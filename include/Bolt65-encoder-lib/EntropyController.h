/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once 
#include "EncodingContext.h"
#include "Scanner.h"
#include "Enums.h"
#include "SyntaxElementInfo.h"
#include "SyntaxMap.h"
#include "CabacEngine.h"
#include "EntropyBuffer.h"

#include <algorithm>
#include <map>

namespace encoder
{
	class EntropyController
	{
	public:
		ContextMemory ctxMem;

		EncodingContext	*enCtx;
		CabacEngine cabacEngine;
		EntropyBuffer buffer;

		map<syntaxElemEnum, SyntaxElementInfo> sMap;

		EntropyController();
		~EntropyController();

		void Init(EncodingContext * _enCtx);

		void parseNALHeader(NALType nalType);
		void parseNonVCL(NALType nalType);
		void parseVCL(NALType nalType, Frame *frame);

		void parseVPS();
		void parseSPS();
		void parsePPS();

		void parseProfileTierLevel();
		void parseStRefPicSet();

		void parseSliceSegment(NALType nalType, Frame *frame);
		void parseSliceHeader(NALType nalType, Frame *frame);
		void parseSliceData(Partition *frame);
		void parseCTU(CU *cu, Partition *partition);
		void parseCU(CU *cu, Partition *partition);
		void parsePU(PU *pu, Partition *partition);
		void parseMVDCoding(PU *pu);
		void parseTransformTree(CU * cu, TU * tu);
		void parseTU(CU * cu, TU * tu);
		void parseResidual(CU * cu, TB *tb);

		void encodeElement(syntaxElemEnum syntaxElemNo, int syntaxElemVal);

		void init();
		void reset();
		void writeTrailingBits();
		void writeStartCode();
		void flushOutputBuffer();
		void refreshCabacEngine();

		void clearModels();
	};
}