/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include "BlockPartition.h"
#include "Frame.h"
#include "EncodingContext.h"
#include "Enums.h"
#include <cmath>
#include "ComUtil.h"
#include <vector>
#include "CUMapper.h"

namespace encoder
{
	class CTBController
	{
	public:
		CTBController();
		~CTBController();

		void Init(EncodingContext * _enCtx);
		void Init(EncodingContext * _enCtx, EncodingContext* _decCtx);
		void CreateCTBTree(Partition * partition);
		void CreateCTBTreeWithDecodedCUMapping(Partition * transcodedPartition, Frame * decodedFrame);

		VarianceMean GetVarianceMean(int16_t * block, int blockSize, int offset, PartitionModeInter partMode);
		void CreateRelations(Partition * partition);
		void CopyModeDecisionFromDecodedCU(CU * cu, CU * dec_cu);

	private:
		EncodingContext* enCtx;
		EncodingContext* decCtx;

		//In case of transcoding 
		float widthCoeff = 0.0;
		float heightCoeff = 0.0;

		void SplitToFitFrame(CU * cu, Partition *partition, int frameWidth, int frameHeight, bool remapTransCU);
		void CreateCTUsOnly(Partition * partition);
		void MapCTUStructure(Partition * transcodedPartition, Frame * decodedFrame);
		void MapOriginalCUToTranscodedCU(CU *transcodedCU, CU *originalCU, int columnStart, int columnEnd, int rowStart, int rowEnd);
		void MapOriginalCUToTranscodedCU(CU *transcodedCU, CU *originalCU);
		void MapToChildren(CU *transcodedCU);
		void mapAllAsWhole(CU *transcodedCU, CU *originalCU);
		void decisionForCUSplit(CU *cu, int partitionWidth, int partitionHeight);
		//void splitCTU(Coding_Quadtree * cqt, EncodingContext * enCtx, CU * ctu);
	};
}
