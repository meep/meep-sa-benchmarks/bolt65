/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include <string.h>
#include <math.h>  
#include<iostream>

using namespace std;

class ResolutionConverter
{
public:
	ResolutionConverter();
	~ResolutionConverter();
	static void ConvertResolution(unsigned char* original, int originalWidth, int originalHeight, unsigned char *transcoded, int transcodedWidth, int transcodedHeight);
	static void ConvertResolutionWeighted(unsigned char* original, int originalWidth, int originalHeight, unsigned char *transcoded, int transcodedWidth, int transcodedHeight);
};