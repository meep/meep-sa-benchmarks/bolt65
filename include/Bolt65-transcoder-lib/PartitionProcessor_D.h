/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "../../include/Bolt65-decoder-lib/CTBController.h"
#include "../../include/Bolt65-decoder-lib/PredictionController.h"
#include "../../include/Bolt65-decoder-lib/EntropyController.h"
#include "TQController.h"
#include "Scanner.h"
#include "InLoopFilterController.h"
#include "ResolutionConverter.h"


namespace transcoder
{
	class PartitionProcessor_D
	{
	public:
		Partition *partition;
		decoder::EntropyController decEc;

		PartitionProcessor_D();
		~PartitionProcessor_D();

		void processPartition();
		void initPartition(Partition * _partition, EncodingContext * decCtx, DPBManager * dpbm, long partitionOffset);

	private:
		decoder::CTBController decCtb;
		decoder::PredictionController decPc;
		TQController tq;
		InLoopFilterController loopc;
		Streamer *streamObject;

	};
}