/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once


#include "EncodingContext.h"
#include "Constants.h"
#include "Enums.h"

#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <algorithm>
#include<cmath>

using namespace std;

class ConfigurationManager
{
public:
	void checkExternalConfig(EncodingContext* enCtx, char* argv[], int argc);
	void fetchConfigurationFromFile(EncodingContext *enCtx);
	void parseConsoleParameters(EncodingContext* enCtx, char* argv[], int argc);
	void assignConfigAttribute(string configName, string configValue, EncodingContext* enCtx);
	void validateContextForEncoder(EncodingContext * enCtx);
	void validateContextForDecoder(EncodingContext * decCtx);
	void validateContextForTranscoder(EncodingContext *enCtx, EncodingContext *decCtx);

	void validateGOPStructure(EncodingContext* enCtx);
	void validateTileConfiguration(EncodingContext* enCtx);
	void validateMotionEstimationConfiguration(EncodingContext* enCtx);
	void validateArchFlagsConfiguration(EncodingContext* enCtx);
	void validateEncodingFlags(EncodingContext* enCtx);
	void validatEncodingParameters(EncodingContext* enCtx);
	void validateBufferSizes(EncodingContext* enCtx);
	void validateIO(EncodingContext* enCtx);
	void validateParallelization(EncodingContext* enCtx);
	void validatePolicy(EncodingContext* enCtx);
	void setFixedValues(EncodingContext* enCtx);

	ConfigurationManager();
	~ConfigurationManager();
};


