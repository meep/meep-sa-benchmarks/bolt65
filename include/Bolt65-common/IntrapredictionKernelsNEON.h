/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __ARM_NEON
#pragma once
#include "IIntrapredictionKernels.h"
#include <arm_neon.h>

class IntrapredictionKernelsNEON :public IIntrapredictionKernels
{
public:
	IntrapredictionKernelsNEON();
	virtual void ThreeTapRefFilter(unsigned char* refSamples, unsigned char* filteredRefSamples, int blockSize);
	virtual void PlanarPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int blockSize);
	virtual void DCPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int cIdx, int blockSize);
	virtual void AngularPrediction(unsigned char* refSamples, unsigned char predMode, unsigned char* predictedBlock, int cIdx, int blockSize);
};
#endif
