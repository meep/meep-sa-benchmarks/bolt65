/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "ComUtil.h"

class SAOFilter
{
public:

	static void BandOffset(unsigned char* block);
	static void EdgeOffset(unsigned char * block, int blockSize, int saoEoClass, int saoOffsetAbs, int saoOffsetSign, int log2OffsetScale);
	SAOFilter();
	~SAOFilter();
};

static const int hPos[2][4] = { {-1,0,-1,1},{1,0,1,-1} };
static const int vPos[2][4] = { {0,-1,-1,-1},{0,1,1,1} };


