/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "TB.h"

class TU
{
public:
	TU *parent;
	bool splitTransformFlag;
	int trafoDepth;
	int index;

	TB tbY;
	TB tbU;
	TB tbV;

	TU *children[4];
	bool hasChildren;

	TU();
	TU(TB tbY, TB tbU, TB tbV);
	~TU();
	void splitTU(int frameWidth);
	void splitTU(int width, int frameWidth);
	void cleanTU();
};

