/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Enums.h"
#include <string>

using namespace std;

class EncodingContext
{
public:
	EncodingContext();
	void clearEncodingContext();
	~EncodingContext();

	ApplicationType appType;

//IO and Encoding configuration
	string inputFilePath;
	string outputFilePath;
	int width;
	int height;
	int frameRate;
	int inputFrameRate; //for transcoder?
	int numOfFrames;

	bool externalConfig;
	string externalConfigFileName;

	//Encoding information
	string GOP;

	int quantizationParameter;
	int initQP;
	int bitNumber;

	int maxCUsize;
	int ctbSize;
	int ctbLog2SizeY;
	int maxTbLog2SizeY;
	int minCbLog2SizeY;
	int minTbLog2SizeY;

	NALType nalType;
	int currentVPSId;
	int currentSPSId;
	int currentPPSId;

	int highestTid;

//FLAGS
	//Encoding options flags
	bool disableDeblockingFilter;
	bool saoEnabled;
	bool useSMP;
	bool useAMP;
	bool useIntraTU;

	//Architectural flags
	bool AVX;
	bool NEON;
	bool SVE;
	bool SVE2;
	bool EPI_RISCV;
	bool EPI_RISCV_AUTO;
	bool MANGO_HW;
	bool MANGO_PEAK;
	bool MANGO_GN;
	bool MANGO_GPU;
	bool MANGO_NUP;
	bool MEEP;

	//Statistics flags
	bool calculatePSNR;
	bool calculateSSIM;
	bool calculateBits;
	bool calculateTime;
	bool showStatsPerFrame;
	bool showStatsPerTile;
	bool statMode;

	bool outputStatsToFile;
	string outputStatsFileName;

	bool outputStatsTileToFile;
	string outputStatsTileFileName;

	bool clusteredTQ;

//MOTION ESTIMATION 
	int dpbSize;
	int searchAlgorithm;
	int searchArea;
	int interpolationAlgorithm;
	int blockMatching;
	int log2MaxPicOrderLst;

	bool useTemporalAMVP;
	bool testAllModes;

//TILES and PARALLELISM
	int threads;

	bool tilesEnabled;
	bool isUniform;
	int numberOfTilesInRow;
	int numberOfTilesInColumn;
	int* rowHeights;
	int* columnWidths;
	int* tileOffsets;
	int tileLoadBalancingAlgorithm;
	int tileLoadBalancingInterval;

	string tilesInColumnAsString;
	string tilesInRowAsString;

	bool useDynamicTiles;
	bool tileChanged;

//MEMORY RELATED OPTIONS (Decoder)
	int inputBufSize;
	int outputBufSize;

//ENTROPY CODING HELPERS
	int num_short_term_ref_pic_sets;
	int cabac_init_present_flag;
	bool cabac_init_flag;
	int max_transform_hierarchy_depth_inter;
	int strong_intra_smoothing_enabled_flag;
	int initType;

//TRANSCODER SPECIFIC FLAGS
	bool reuseData;
	bool reuseIntraMode;
	bool reuseInterMode;
	bool reuseInterpolation;
	bool reusePartMode;
	bool reuseMode;

//POLICY
	bool usePolicy;
	string policyHost;
	int policyPort;

//OTHER
	string command;
	string version;


};



 