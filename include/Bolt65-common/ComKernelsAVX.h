/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __AVX2__
#pragma once 
#include "IComKernels.h"
#include <immintrin.h>


class ComKernelsAVX :public IComKernels
{
public:
	ComKernelsAVX();
	virtual int SAD(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight);
	virtual void SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize);
};
#endif
