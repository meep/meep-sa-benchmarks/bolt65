/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "CU.h"
#include "Frame.h"
class CUMapper {

public:
	CUMapper();
	~CUMapper();

	void MapOriginalCUToTranscodedCU(CU * transcodedCU, CU * originalCU);
	void MapToChildren(CU * transcodedCU);
	void mapAllAsWhole(CU * transcodedCU, CU * originalCU);
	void MapOriginalCUToTranscodedCU(CU * transcodedCU, CU * originalCU, int columnStart, int columnEnd, int rowStart, int rowEnd);

	EncodingContext *enCtx;
	EncodingContext *decCtx;
	int heightCoeff;
	int widthCoeff;

};
