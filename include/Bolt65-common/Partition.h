/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EncodingContext.h"
#include "CU.h"
#include "Enums.h"
#include <ctype.h>
#include<cmath>


class Partition
{
public:

	PartitionType partitionType;
	EncodingContext *enCtx;
	// props
	unsigned char* payload;			//Raw frame data in YUV format
	unsigned char* reconstructed_payload;	//reconstructed picture after prediction, transform and quantization.

	bool* isReconstructed;					//boolean array which signifies is the pixel already reconstructed and available for reference samples.
	bool* isCTUProcessed;
	int numberOfCTUs;
	int numberOfCTUsInRow;
	int numberOfCTUsInColumn;

	int startingCTUInFrame = 0;
	int startingIndexInFrame = 0;

	CU **ctuTree;

	int width;
	int height;
	int  size;
	bool isLastPartitionInFrame;

	char type;
	SliceType sliceType;
	int framePoc;

	//variables for Load Balancing...
	float proccesingTime = 0.0;
	long long bitSize;
	int searchPoints = 0;
	int transformCost = 0;
	int predictionCost = 0;
	int blockCost = 0;
	int blockNonZeroCost = 0;

	int i, currRow, currCol, currRowFrame, currColumnFrame;
	int y_pixel_iterator, u_pixel_iterator, v_pixel_iterator;
	int y_pixel_iterator_frame, u_pixel_iterator_frame, v_pixel_iterator_frame;

	int startingIndexU;
	int startingIndexV;

	int startingIndexUFrame;
	int startingIndexVFrame;

	bool isWidthDividible; 
	bool isHeightDividible;

	int startingTileRow; 
	int startingTileColumn;

	// methods
	Partition();
	Partition(EncodingContext *_enCtx);
	~Partition();


};
