/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Enums.h"
#include "Frame.h"
#include <iostream>

#pragma once
class Scanner
{
public:
	static void GetCoordinatesInBlock2x2(int scanIndex, int &cooX, int &cooY, int scanType);
	static void GetCoordinatesInBlock4x4(int scanIndex, int &cooX, int &cooY, int scanType);
	static void GetCoordinatesInBlock8x8(int scanIndex, int & cooX, int & cooY, int scanType);
	static void GetCoordinatesInDiagonalBlock2x2(int scanIndex, int & cooX, int & cooY);
	static void GetCoordinatesInDiagonalBlock4x4(int scanIndex, int & cooX, int & cooY);
	static void GetCoordinatesInDiagonalBlock8x8(int scanIndex, int & cooX, int & cooY);
	static void GetScanIndexFromCoordinates2x2(int & scanIndex, int cooX, int cooY, int scanType);
	static void GetScanIndexFromCoordinates4x4(int & scanIndex, int cooX, int cooY, int scanType);
	static void GetScanIndexFromCoordinates8x8(int & scanIndex, int cooX, int cooY, int scanType);
	static void GetDiagonalScanIndexFromCoordinates2x2(int & scanIndex, int cooX, int cooY);
	static void GetDiagonalScanIndexFromCoordinates4x4(int & scanIndex, int cooX, int cooY);
	static void GetDiagonalScanIndexFromCoordinates8x8(int & scanIndex, int cooX, int cooY);
	static void FindLastSigCoordinates(TB * transformBlock);
	static void SetSubBlockElements(int16_t * scanData, TBSubblock* tbSubBlock);
	static void GetSubBlockElements(int16_t * scanData, TBSubblock * tbSubBlock);
	static void DiagonalScan4x4(int16_t *data, int16_t *result, TBSubblock* tbSubBlock);
	static void VerticalScan4x4(int16_t * data, int16_t * result, TBSubblock * tbSubBlock);
	static void HorizontalScan4x4(int16_t * data, int16_t * result, TBSubblock * tbSubBlock);
	static void ReverseDiagonalScan4x4(int16_t *result, TBSubblock* tbSubBlock);
	static void ReverseVerticalScan4x4(int16_t * result, TBSubblock* tbSubBlock);
	static void ReverseHorizontalScan4x4(int16_t * result, TBSubblock* tbSubBlock);
	static void ScanBlock(TB * transformBlock, PredMode predictionMode);
	static void ReverseScanBlock(TB * transformBlock, PredMode predictionMode);
	static void DiagonalScanBlock(TB * transformBlock);
	static void VerticalScanBlock(TB * transformBlock);
	static void HorizontalScanBlock(TB * transformBlock);
	static void ReverseDiagonalScanBlock(TB * transformBlock);
	static void ReverseVerticalScanBlock(TB * transformBlock);
	static ScanType GetScanType(TB * transformBlock, PredMode predictionMode);
	static void ReverseHorizontalScanBlock(TB * transformBlock);
	static void FillResult(int16_t *subResult, int16_t *finalResult, int index);
	static void FillResidual(int16_t * subResult, int16_t * residual, int index, int blockSize);
	static void Fetch4x4Block(int16_t *data, int16_t *subBlock, int nextElemToFetch, int numOfElementsInRow);
	static void LastSigCoeffBin(int x, int &prefix, int &suffix);
	static void LastSigCoeffInvBin(int & x, int prefix, int suffix);
	static void PerformScanning(Partition* partition);
	static void PerformScanningForCU(CU * cu);
	static void RecursiveTUScanning(CU * cu, TU * tu);
	static void PerformInverseScanning(Partition* partition);
	static void PerformInverseScanningForCU(CU * cu);
	static void PerformInverseScanningForTU(TU * tu, PredMode predMode);
};

