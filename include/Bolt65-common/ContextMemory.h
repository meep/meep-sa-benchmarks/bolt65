/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
class ContextMemory
{
public:
	ContextMemory();
	void CopySliceType(int _sliceType);
	~ContextMemory();


	unsigned short StatCoeff[4];
	unsigned short cLastAbsLevel;
	unsigned short cLastRiceParam;

	int log2_max_pic_order_cnt_lsb_minus4;
	int sps_temporal_mvp_enabled_flag;
	int pic_width_in_luma_samples;
	int pic_height_in_luma_samples;
	int log2_diff_max_min_luma_coding_block_size;
	int log2_min_luma_coding_block_size_minus3;
	int num_short_term_ref_pic_sets;
	int num_long_term_ref_pics_sps;
	int offset_len_minus1;
	int pcm_sample_bit_depth_luma_minus1;
	int pcm_sample_bit_depth_chroma_minus1;
	int pred_mode_flag;
	int predMode;
	int predModeChroma;
	bool amp_enabled_flag;
	int num_ref_idx_l0_active_minus1;
	int num_ref_idx_l1_active_minus1;
	int chroma_qp_offset_list_len_minus1;
	int last_sig_coeff_x_prefix;
	int last_sig_coeff_y_prefix;
	int coeff_abs_level_greater1_flag;
	int coeff_abs_level_greater2_flag;
	bool persistent_rice_adaptation_enabled_flag;
	bool transform_skip_flag;
	bool cu_transquant_bypass_flag;
	bool extended_precision_processing_flag;
	int cbf_luma;
	int cbf_cb;
	int cbf_cr;
	int parent_cbf_luma;
	int parent_cbf_cb;
	int parent_cbf_cr;
	int five_minus_max_num_merge_cand;
	int merge_flag;
	int cu_skip_flag;
	bool left_cu_skip_flag;
	bool above_cu_skip_flag;
	int rqt_root_cbf;
	

	int CuPredMode;

	// Include in syntax
	int cbSize;
	int nPbW;		// Width and the height of the current luma prediction block
	int nPbH;
	int log2TrafoSize;
	int trafoDepth;
	bool first_time_invoke; // coeff_abs_level_remaining is binarized for first time in sub-block

	// Process values availability and depth of L and A CBs
	bool availableL;
	bool availableA;
	int ctDepthL;
	int ctDepthA;
	int cqtDepth;

	int colorIdx;

	int subBlockCoordinateX;
	int subBlockCoordinateY;

	bool isBorderSubBlock;		// sub-blocks which do not have neighbour sub-block right and below within same TB
	bool subBlockRightCSBF;	// for other not border sub-blocks
	bool subBlockBottomCSBF;

	int transformBlock_xC;	// current coefficient scan location in TB
	int transformBlock_yC;
	int scanIdx;

	//For ALG1 ctxInc derivation needed
	int sub_block_index;
	int inside_sub_block_index;
	bool previous_ALG1_flag;
	int first_time_in_TB;
	int maxScanIdx;
	

	int sliceType;
	int initType;
	bool cabac_init_flag;
};

