/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Enums.h"

class SyntaxElementInfo
{
public:
	BinarizationType binType = NoBinarization;
	unsigned int cMax = 0;
	unsigned int cRiceParam = 0;
	unsigned int k = 0;
	BinarizationAdditionalOperation addOperations[2]{ None,None };

	bool isCABACencoded = false;
	unsigned int ctxTableIdx = 0;
	unsigned int ctxIdx = 0;
	unsigned int ctxIdxP = 0;
	unsigned int syntaxProbModelsIdx =0;
	unsigned int writeBits = 0;

	bool derivateSpecialCtxIdx = false;

	SyntaxElementInfo();

	//Full constructor
	SyntaxElementInfo(BinarizationType bType, unsigned int bcMax, unsigned int bcRice, unsigned int bk, BinarizationAdditionalOperation addOp, bool cabac, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx, bool dSpecialCtxIdx);

	//Constructor where ExpGolomb, Fixed Length or Truncated unary is called without CABAC, only cMax or k is passed, depending on binarization
	SyntaxElementInfo(BinarizationType bType, unsigned int param);

	//Constructor where sytaxValue has to be precalculated
	SyntaxElementInfo(BinarizationType bType, BinarizationAdditionalOperation addOp, unsigned int param);

	//Constructor with Binarization with variable number of bits
	SyntaxElementInfo(BinarizationType bType, BinarizationAdditionalOperation addOp);

	//Construtor for CABAC encoded syntax elements
	SyntaxElementInfo(BinarizationType bType, unsigned int param, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx);
	SyntaxElementInfo(BinarizationType bType, unsigned int cRParam, unsigned int cM, unsigned  int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned  int sPIdx);
	SyntaxElementInfo(BinarizationType bType, unsigned int param, BinarizationAdditionalOperation addOp, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx);
	SyntaxElementInfo(BinarizationType bType, unsigned int cRParam, unsigned int cM, BinarizationAdditionalOperation addOp, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx);
	SyntaxElementInfo(BinarizationType bType, unsigned int param, BinarizationAdditionalOperation addOp, BinarizationAdditionalOperation addOp2, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx);
	SyntaxElementInfo(BinarizationType bType, unsigned int cRParam, unsigned int cM, BinarizationAdditionalOperation addOp, BinarizationAdditionalOperation addOp2, unsigned int cTableIdx, unsigned  int cIdx, unsigned int  cIdxP, unsigned int sPIdx);
	SyntaxElementInfo(BinarizationType bType, BinarizationAdditionalOperation addOp, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx);
	SyntaxElementInfo(BinarizationType bType, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx);

	~SyntaxElementInfo();
};