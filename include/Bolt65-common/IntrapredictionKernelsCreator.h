/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "IIntrapredictionKernels.h"
#include "IntrapredictionKernelsScalar.h"
#include "IntrapredictionKernelsAVX.h"
#include "IntrapredictionKernelsNEON.h"
#include "IntrapredictionKernelsSVE.h"
#include "IntrapredictionKernelsEPIRISCV.h"
#include "EncodingContext.h"

using namespace std;

class IntrapredictionKernelsCreator
{
public:
	~IntrapredictionKernelsCreator();
	static IIntrapredictionKernels* Create(EncodingContext* enCtx);
private:
	IntrapredictionKernelsCreator();

};