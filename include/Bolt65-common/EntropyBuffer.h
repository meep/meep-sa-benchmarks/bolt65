/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Binarizer.h"
#include <vector>
class EntropyBuffer
{
public:
	EntropyBuffer();
	~EntropyBuffer();


	void Init();

	void writeBins(Bins b);

	void writeBits(bool bitValue, unsigned int n);

	void writeBytes(unsigned char byteValue, unsigned int n);

	void byteAlign(bool val);

	void flushBuffer();

	void putIntoBuffer(unsigned char byte);

	unsigned char emulation_buffer[3];
	unsigned short emu_buff_empty;
	unsigned short bitCounter;

	unsigned char currentByte;

	long long Param_Number_of_bits;

	unsigned short byteCounter;

	std::vector<unsigned char> output_buffer;
	
};

