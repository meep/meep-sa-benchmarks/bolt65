/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "EncodingContext.h"
#include "ITransformAndQuantization.h"
#include "TransformAndQuantizationCreator.h"
#include "CU.h"
#include "Partition.h"
#include <cstdint>

class TQController
{
public:
	/*These variables are only used in a case where clusteredTQ is used*/
	int16_t* residualFrame32;
	int16_t* residualFrame16;
	int16_t* residualFrame8;
	int16_t* residualFrame4;

	int16_t* qtFrame32;
	int16_t* qtFrame16;
	int16_t* qtFrame8;
	int16_t* qtFrame4;

	int16_t* iqtFrame32;
	int16_t* iqtFrame16;
	int16_t* iqtFrame8;
	int16_t* iqtFrame4;

	int residual32Size = 0;
	int residual16Size = 0;
	int residual8Size = 0;
	int residual4Size = 0;
	/*END These variables are only used in a case where clusteredTQ is used*/

	TQController();
	~TQController();
	void Init(EncodingContext * _enCtx);
	void Init(EncodingContext* _enCtx, Partition* partition);
	void PerformTransformAndQuantization(int qp, int16_t * blockResidual, unsigned char blockSize, bool * isZeroMatrix, int16_t * tqResidual, int colorIdx, PredMode cuPredMode);
	void PerformInverseTransformAndQuantization(int qp, int16_t * quantizedTransformedResidual, unsigned char blockSize, int16_t * residual, int colorIdx, PredMode cuPredMode);
	void PerformTQandIQT();
	void PrepareDataForAccelerator();
	void Clean();

private:
	EncodingContext * enCtx;
	ITransformAndQuantization *tqKernel;
};

