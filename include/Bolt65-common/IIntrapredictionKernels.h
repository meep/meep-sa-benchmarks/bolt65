﻿/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once 
#include <stdlib.h>
#include "ComUtil.h"
//#include "Intraprediction.h"

using namespace std;

class IIntrapredictionKernels
{
public:
	virtual ~IIntrapredictionKernels() {}
	virtual void ThreeTapRefFilter(unsigned char* refSamples, unsigned char* filteredRefSamples, int blockSize) = 0;
	virtual void PlanarPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int blockSize) = 0;
	virtual void DCPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int cIdx, int blockSize) = 0;
	virtual void AngularPrediction(unsigned char* refSamples, unsigned char predMode, unsigned char* predictedBlock, int cIdx, int blockSize) = 0;
};

static short AngularIntrapredictionModes[33][2] = {
	{32, 0}, //mode 2, index 0
	{26, 0},
	{21, 0},
	{17, 0},
	{13, 0},
	{9, 0},
	{5, 0},
	{2, 0},
	{0, 0},
	{-2, -4096},
	{-5, -1638},
	{-9, -910},
	{-13, -630},
	{-17, -482},
	{-21, -390},
	{-26, -315},
	{-32, -256},
	{-26, -315},
	{-21, -390},
	{-17, -482},
	{-13, -630},
	{-9, -910},
	{-5, -1638},
	{-2, -4096},
	{0,0},
	{2,0},
	{5,0},
	{9,0},
	{13,0},
	{17,0},
	{21,0},
	{26,0},
	{32,0} //mode 34
};