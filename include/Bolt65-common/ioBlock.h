/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include <string>
#include <fstream>
#include <vector>
#include "EntropyBuffer.h"

using namespace std;

#define outputBufferSize	512

class ioBlock
{
public:

	unsigned char output_buffer[outputBufferSize];

	int FWidth;
	int FHeight;
	int FSize;
	int FNumber;


	char* readNextFrame(int nFramesToRead, bool isYUV, bool & eof);
	void openOutputFile(string fileName);
	void writeToFile(int size);
	int writeBufferToFile(EntropyBuffer *buffer);
	void putIntoBuffer(unsigned char byte);

	long long byteCounter;

	streamoff openFile(string path);
	streamoff openFile(string path, int width, int height);
	void closeFiles();

	streamoff openCompressedFile(string path);
	void readCompressedFile(int size);
	unsigned char readByteFromBuffer();
	FILE* inputFilePointer;
	bool readHeader();
	long long inputFileSize;

private:
	ofstream *outputBitstreamFile;
	istream *ifs;

	

};
