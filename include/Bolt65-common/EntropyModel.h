/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once
#include "Enums.h"
#include "ComUtil.h"

class Bins
{
public:
	unsigned long bits;
	int length;

	Bins();
	~Bins();

};

class Value
{
public:
	int value;
	bool possible;

	Value();
	~Value();
};

class ctxStruct4ALG1
{
public:
	unsigned char ctxInc;
	unsigned char ctxSet;
};


class CtxIdx
{
public:
	CtxIdx(syntaxElemEnum syntaxElemNoInit, unsigned const char *ctxTableInit, unsigned const char ctxIdxOffsetInit1, unsigned const char ctxIdxOffsetInit2, unsigned const char ctxIdxOffsetInit3);
	syntaxElemEnum syntaxElementNo;
	unsigned const char *ctxTable;
	unsigned short ctxIdxOffset[3];
};


class ProbabilityModel
{
public:
	ProbabilityModel();
	ProbabilityModel(syntaxElemEnum syntaxElemNo, unsigned int ctxIndex, char slice_type, unsigned int sliceQpy, bool cabac_init_flag);
	unsigned char  pStateIdx;
	bool valMps;
};



class ProbabilityModelSelection
{
public:
	ProbabilityModelSelection();
	~ProbabilityModelSelection();

	void InitializeSelection(unsigned int SliceQpy, bool cabac_init_flag);

	ProbabilityModel **probabilityModel;
};

class EntropyModel
{
public:
	EntropyModel();
	~EntropyModel();
};

static const unsigned short ctxIncTable[57][6] = {
	{ toTerminate, na, na, na, na, na },
	{ toTerminate, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, bypass, na, na, na, na },
	{ 0, bypass, na, na, na, na },
	{ bypass, bypass, bypass, bypass, bypass, na },
	{ bypass, na, na, na, na, na },
	{ bypass, bypass, bypass, bypass, bypass, bypass },
	{ bypass, bypass, bypass, na, na, na },
	{ bypass, bypass, bypass, na, na, na },
	{ nd, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, 1, 2, bypass, na, na },
	{ 0, 1, 3, bypass, na, na },
	{ toTerminate, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ bypass, bypass, na, na, na, na },
	{ bypass, bypass, bypass, bypass, bypass, na },
	{ 0, bypass, bypass, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, bypass, bypass, bypass, na, na },
	{ nd, 4, na, na, na, na },
	{ 0, 1, bypass, bypass, bypass, bypass },
	{ 0, 1, bypass, bypass, bypass, bypass },
	{ 0, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ bypass, bypass, bypass, bypass, bypass, bypass },
	{ bypass, na, na, na, na, na },
	{ 0, 1, 1, 1, 1, bypass },
	{ bypass, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, 0, 0, 0, 0, na },
	{ nd, nd, nd, nd, na, na },
	{ nd, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ 0, na, na, na, na, na },
	{ nd, nd, nd, nd, nd, nd },
	{ nd, nd, nd, nd, nd, nd },
	{ bypass, bypass, bypass, bypass, bypass, bypass },
	{ bypass, bypass, bypass, bypass, bypass, bypass },
	{ nd, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ nd, na, na, na, na, na },
	{ bypass, bypass, bypass, bypass, bypass, bypass },
	{ bypass, na, na, na, na, na }
};


//Context tables for syntax elements
static const unsigned char ctxIdx_sao_merge_flag[3] = { 153, 153, 153 };// Table 9-5
static const unsigned char ctxIdx_sao_type_idx[3] = { 200, 185, 160 };// Table 9-6
static const unsigned char ctxIdx_split_cu_flag[9] = { 139, 141, 157, 107, 139, 126, 107, 139, 126 };// Table 9-7
static const unsigned char ctxIdx_cu_transquant_bypass_flag[3] = { 154, 154, 154 };// Table 9-8
static const unsigned char ctxIdx_cu_skip_flag[6] = { 197, 185, 201, 197, 185, 201 };// Table 9-9
static const unsigned char ctxIdx_pred_mode_flag[2] = { 149, 134 };// Table 9-10
static const unsigned char ctxIdx_part_mode[9] = { 184, 154, 139, 154, 154, 154, 139, 154, 154 };// Table 9-11
static const unsigned char ctxIdx_prev_intra_luma_pred_flag[3] = { 184, 154, 183 };// Table 9-12
static const unsigned char ctxIdx_intra_chroma_pred_mode[3] = { 63, 152, 152 };// Table 9-13
static const unsigned char ctxIdx_rqt_root_cbf[2] = { 79,  79 };// Table 9-14
static const unsigned char ctxIdx_merge_flag[2] = { 110, 154 };// Table 9-15
static const unsigned char ctxIdx_merge_idx[2] = { 122, 137 };// Table 9-16
static const unsigned char ctxIdx_inter_pred_idc[10] = { 95,  79,  63,  31,  31,  95,  79,  63,  31,  31 };// Table 9-17
static const unsigned char ctxIdx_ref_idx[4] = { 153, 153, 153, 153 };// Table 9-18
static const unsigned char ctxIdx_mvp_l_flag[2] = { 168, 168 };// Table 9-19
static const unsigned char ctxIdx_split_transform_flag[9] = { 153, 138, 138, 124, 138,  94, 224, 167, 122 };// Table 9-20
static const unsigned char ctxIdx_cbf_luma[6] = { 111, 141, 153, 111, 153, 111 };// Table 9-21
static const unsigned char ctxIdx_cbf_c[15] = { 94, 138, 182, 154, 149, 107, 167, 154, 149,  92, 167, 154, 154, 154, 154 };// Table 9-22
static const unsigned char ctxIdx_mvd_greater_flag[4] = { 140, 198, 169, 198 };// Table 9-23
static const unsigned char ctxIdx_cu_qp_delta_abs[6] = { 154, 154, 154, 154, 154, 154 };// Table 9-24
static const unsigned char ctxIdx_transform_skip_flag[6] = { 139, 139, 139, 139, 139, 139 };// Table 9-25
static const unsigned char ctxIdx_last_sig_coeff_x_prefix[54] = { 110, 110, 124, 125, 140, 153, 125, 127, 140, 109, 111, 143, 127, 111,  79, 108, 123,  63,
125, 110,  94, 110,  95,  79, 125, 111, 110,  78, 110, 111, 111,  95,  94, 108, 123, 108,
125, 110, 124, 110,  95,  94, 125, 111, 111,  79, 125, 126, 111, 111,  79, 108, 123,  93 };// Table 9-26
static const unsigned char ctxIdx_last_sig_coeff_y_prefix[54] = { 110, 110, 124, 125, 140, 153, 125, 127, 140, 109, 111, 143, 127, 111,  79, 108, 123,  63,
125, 110,  94, 110,  95,  79, 125, 111, 110,  78, 110, 111, 111,  95,  94, 108, 123, 108,
125, 110, 124, 110,  95,  94, 125, 111, 111,  79, 125, 126, 111, 111,  79, 108, 123,  93 };// Table 9-27
static const unsigned char ctxIdx_coded_sub_block_flag[12] = { 91, 171, 134, 141, 121, 140,  61, 154, 121, 140,  61, 154 };// Table 9-28
static const unsigned char ctxIdx_sig_coeff_flag[132] = { 111, 111, 125, 110, 110,  94, 124, 108, 124, 107, 125, 141, 179, 153, 125, 107,
125, 141, 179, 153, 125, 107, 125, 141, 179, 153, 125, 140, 139, 182, 182, 152,
136, 152, 136, 153, 136, 139, 111, 136, 139, 111, 155, 154, 139, 153, 139, 123,
123,  63, 153, 166, 183, 140, 136, 153, 154, 166, 183, 140, 136, 153, 154, 166,
183, 140, 136, 153, 154, 170, 153, 123, 123, 107, 121, 107, 121, 167, 151, 183,
140, 151, 183, 140, 170, 154, 139, 153, 139, 123, 123,  63, 124, 166, 183, 140,
136, 153, 154, 166, 183, 140, 136, 153, 154, 166, 183, 140, 136, 153, 154, 170,
153, 138, 138, 122, 121, 122, 121, 167, 151, 183, 140, 151, 183, 140, 141, 111,
140, 140, 140, 140 };// Table 9-29
//Ctx assignment for 4x4 TB
static const unsigned char ctxIdxMapSIG[15] = { 0,   1,   4,   5,   2,   3,   4,   5,   6,   6,   8,   8,   7,   7,   8 };
static const unsigned char ctxIdx_coeff_abs_level_greater1_flag[72] = { 140,  92, 137, 138, 140, 152, 138, 139, 153,  74, 149,  92, 139, 107, 122, 152,
140, 179, 166, 182, 140, 227, 122, 197, 154, 196, 196, 167, 154, 152, 167, 182,
182, 134, 149, 136, 153, 121, 136, 137, 169, 194, 166, 167, 154, 167, 137, 182,
154, 196, 167, 167, 154, 152, 167, 182, 182, 134, 149, 136, 153, 121, 136, 122,
169, 208, 166, 167, 154, 152, 167, 182 };// Table 9-30
static const unsigned char ctxIdx_coeff_abs_level_greater2_flag[18] = { 138, 153, 136, 167, 152, 152, 107, 167,  91,
122, 107, 167, 107, 167,  91, 107, 107, 167 };// Table 9-31
static const unsigned char ctxIdx_explicit_rdpcm_flag[4] = { 139, 139, 139, 139 };// Table 9-32
static const unsigned char ctxIdx_explicit_rdpcm_dir_flag[4] = { 139, 139, 139, 139 };// Table 9-33
static const unsigned char ctxIdx_cu_chroma_qp_offset_flag[3] = { 154, 154, 154 };	// Table 9-34
static const unsigned char ctxIdx_cu_chroma_qp_offset_idx[3] = { 154, 154, 154 };	// Table 9-35
static const unsigned char ctxIdx_log2_res_scale_abs_plus1[24] = { 154, 154, 154, 154, 154, 154, 154, 154, 154,
154, 154, 154, 154, 154, 154, 154, 154, 154,
154, 154, 154, 154, 154, 154 };// Table 9-36
static const unsigned char ctxIdx_res_scale_sign_flag[6] = { 154, 154, 154, 154, 154, 154 };

//Association of ctxIdx and syntax elements for each initialization Type
static CtxIdx ctxIdxOffset[39] = {// SAO
	CtxIdx(sao_merge_left_flag,ctxIdx_sao_merge_flag,	  0,1,2),
	CtxIdx(sao_merge_up_flag,ctxIdx_sao_merge_flag,	  0,1,2),
	CtxIdx(sao_type_idx_luma,ctxIdx_sao_type_idx,	  0,1,2),
	CtxIdx(sao_type_idx_chroma,ctxIdx_sao_type_idx,	  0,1,2),

	// coding_quadtree
	CtxIdx(split_cu_flag,	ctxIdx_split_cu_flag,	  0,3,6),

	// coding_unit
	CtxIdx(cu_transquant_bypass_flag,	ctxIdx_cu_transquant_bypass_flag,	  0,1,2),
	CtxIdx(cu_skip_flag,	ctxIdx_cu_skip_flag,	255,0,3),
	CtxIdx(pred_mode_flag,	ctxIdx_pred_mode_flag,	255,0,1),
	CtxIdx(part_mode,ctxIdx_part_mode,  0,1,5),
	CtxIdx(prev_intra_luma_pred_flag,	ctxIdx_prev_intra_luma_pred_flag,	  0,1,2),
	CtxIdx(intra_chroma_pred_mode,ctxIdx_intra_chroma_pred_mode,  0,1,2),
	CtxIdx(rqt_root_cbf,	ctxIdx_rqt_root_cbf,	255,0,1),

	// prediction unit
	CtxIdx(merge_flag,ctxIdx_merge_flag,255,0,1),
	CtxIdx(merge_idx,ctxIdx_merge_idx,255,0,1),
	CtxIdx(inter_pred_idc,	ctxIdx_inter_pred_idc,	255,0,5),
	CtxIdx(ref_idx_l0,ctxIdx_ref_idx,255,0,2),
	CtxIdx(ref_idx_l1,ctxIdx_ref_idx,255,0,2),
	CtxIdx(mvp_l0_flag,ctxIdx_mvp_l_flag,255,0,1),
	CtxIdx(mvp_l1_flag,ctxIdx_mvp_l_flag,255,0,1),

	// transform tree
	CtxIdx(split_transform_flag,ctxIdx_split_transform_flag,  0,3,6),
	CtxIdx(cbf_luma,ctxIdx_cbf_luma,  0,2,4),
	CtxIdx(cbf_cb,ctxIdx_cbf_c,  0,4,8),
	CtxIdx(cbf_cr,ctxIdx_cbf_c,  0,4,8),

	// mvd coding
	CtxIdx(abs_mvd_greater0_flag,ctxIdx_mvd_greater_flag,255,0,2),
	CtxIdx(abs_mvd_greater1_flag,ctxIdx_mvd_greater_flag,255,1,3),

	// transform unit
	CtxIdx(cu_qp_delta_abs,	ctxIdx_cu_qp_delta_abs,	  0,2,4),
	CtxIdx(cu_chroma_qp_offset_flag,	ctxIdx_cu_chroma_qp_offset_flag,	  0,1,2),
	CtxIdx(cu_chroma_qp_offset_idx,ctxIdx_cu_chroma_qp_offset_idx,  0,1,2),

	// cross comp pred
	CtxIdx(log2_res_scale_abs_plus1,	ctxIdx_log2_res_scale_abs_plus1,	  0,8,16),
	CtxIdx(res_scale_sign_flag,ctxIdx_res_scale_sign_flag,  0,2,4),

	// residual coding
	CtxIdx(transform_skip_flag,ctxIdx_transform_skip_flag,  0,1,2),	// SPECIAL CASE
	CtxIdx(explicit_rdpcm_flag,ctxIdx_explicit_rdpcm_flag,255,0,1), // SPECIAL CASE
	CtxIdx(explicit_rdpcm_dir_flag,ctxIdx_explicit_rdpcm_dir_flag,255,0,1), // SPECIAL CASE
	CtxIdx(last_sig_coeff_x_prefix,ctxIdx_last_sig_coeff_x_prefix,  0,18,36),
	CtxIdx(last_sig_coeff_y_prefix,ctxIdx_last_sig_coeff_y_prefix,  0,18,36),
	CtxIdx(coded_sub_block_flag,ctxIdx_coded_sub_block_flag,  0,4,8),
	CtxIdx(sig_coeff_flag,	ctxIdx_sig_coeff_flag,	  0,42,84),
	CtxIdx(coeff_abs_level_greater1_flag,ctxIdx_coeff_abs_level_greater1_flag,0,24,48),
	CtxIdx(coeff_abs_level_greater2_flag,ctxIdx_coeff_abs_level_greater2_flag,0,6,12)
};


//Lookup table for R x pLPS multiplication
static unsigned char rangeTabLps[256] = { 128, 176, 208, 240,
128, 167, 197, 227,
128, 158, 187, 216,
123, 150, 178, 205,
116, 142, 169, 195,
111, 135, 160, 185,
105, 128, 152, 175,
100, 122, 144, 166,
95, 116, 137, 158,
90, 110, 130, 150,
85, 104, 123, 142,
81,  99, 117, 135,
77,  94, 111, 128,
73,  89, 105, 122,
69,  85, 100, 116,
66,  80,  95, 110,
62,  76,  90, 104,
59,  72,  86,	99,
56,  69,  81,	94,
53,  65,  77,	89,
51,  62,  73,	85,
48,  59,  69,	80,
46,  56,  66,	76,
43,  53,  63,	72,
41,  50,  59,	69,
39,  48,  56,	65,
37,  45,  54,	62,
35,  43,  51,	59,
33,  41,  48,	56,
32,  39,  46,	53,
30,  37,  43,	50,
29,  35,  41,	48,
27,  33,  39,	45,
26,  31,  37,	43,
24,  30,  35,	41,
23,  28,  33,	39,
22,  27,  32,	37,
21,  26,  30,	35,
20,  24,  29,	33,
19,  23,  27,	31,
18,  22,  26,	30,
17,  21,  25,	28,
16,  20,  23,	27,
15,  19,  22,	25,
14,  18,  21,	24,
14,  17,  20,	23,
13,  16,  19,	22,
12,  15,  18,	21,
12,  14,  17,	20,
11,  14,  16,	19,
11,  13,  15,	18,
10,  12,  15,	17,
10,  12,  14,	16,
9,  11,  13,	15,
9,  11,  12,	14,
8,  10,  12,	14,
8,   9,  11,	13,
7,   9,  11,	12,
7,   9,  10,	12,
7,   8,  10,	11,
6,   8,   9,	11,
6,   7,   9,	10,
6,   7,   8,	 9,
2,   2,   2,	 2 };

//probability state transitions
static unsigned char transIdx[] = { //pStateIdx transIdxLps transIdxMps
	0,	 0,	 1,
	1,	 0,	 2,
	2,	 1,	 3,
	3,	 2,	 4,
	4,	 2,	 5,
	5,	 4,	 6,
	6,	 4,	 7,
	7,	 5,	 8,
	8,	 6,	 9,
	9,	 7,	10,
	10,	 8,	11,
	11,	 9,	12,
	12,	 9,	13,
	13,	11,	14,
	14,	11,	15,
	15,	12,	16,
	16,	13,	17,
	17,	13,	18,
	18,	15,	19,
	19,	15,	20,
	20,	16,	21,
	21,	16,	22,
	22,	18,	23,
	23,	18,	24,
	24,	19,	25,
	25,	19,	26,
	26,	21,	27,
	27,	21,	28,
	28,	22,	29,
	29,	22,	30,
	30,	23,	31,
	31,	24,	32,
	32,	24,	33,
	33,	25,	34,
	34,	26,	35,
	35,	26,	36,
	36,	27,	37,
	37,	27,	38,
	38,	28,	39,
	39,	29,	40,
	40,	29,	41,
	41,	30,	42,
	42,	30,	43,
	43,	30,	44,
	44,	31,	45,
	45,	32,	46,
	46,	32,	47,
	47,	33,	48,
	48,	33,	49,
	49,	33,	50,
	50,	34,	51,
	51,	34,	52,
	52,	35,	53,
	53,	35,	54,
	54,	35,	55,
	55,	36,	56,
	56,	36,	57,
	57,	36,	58,
	58,	37,	59,
	59,	37,	60,
	60,	37,	61,
	61,	38,	62,
	62,	38,	62,
	63,	63,	63 };