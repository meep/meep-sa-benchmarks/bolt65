/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#pragma once

#include <iostream>
#include <thread>
#include <map>
#include <condition_variable>

using namespace std;

class MemoryManager
{
private:
	static unsigned char * buffer;
	static unsigned int bufferSize;
	static unsigned int circularWriteAddress;
	static unsigned int circularReadAddress;
	static long virtualAddress;
	static long lastAddress;
	static FILE * stream;
	static thread readingThread;
	static map<unsigned long, unsigned long> released_blocks;
	static mutex file_access_lock;
	static condition_variable read_cv;
	static mutex read_m;
	static condition_variable sem_cv;
	static mutex sem_m;
	static mutex release_m;
	static int ActiveThreadCount;
	static bool _EOF;
	

	/// <summary>
	/// Reads block of defined size from the defined file stream.
	/// </summary>
	/// <param name="size">Size of the block that was read from the stream.</param>
	static void Read(unsigned int size);

	/// <summary>
	/// Releases memory of next block from the starting virtual address that is registered form releasing.
	/// </summary>
	/// <returns>Signal if the process of memory releasing was done.</returns>
	static bool MemoryReleasing();

	/// <summary>
	/// Register the thread entering critical the critical section of the memory manager. 
	/// </summary>
	static void ThreadCheckIn();

	/// <summary>
	/// Register the thread exiting the critical section of the memory manager.
	/// </summary>
	static void ThreadCheckOut();

	/// <summary>
	/// Confirms that is no active threads in critical section of the memory manager.
	/// </summary>
	/// <returns>Signal if there is no active threads.</returns>
	static bool NoActiveThreads();

	/// <summary>
	/// Synchronize the value of the current starting virtual address.
	/// This variable must be set when there is no active threads in critical section.
	/// </summary>
	static void SyncVirtualAddress(long address);

	/// <summary>
	/// Synchronize the value of the current ending virtual address.
	/// This variable must be set when there is no active threads in critical section.
	/// </summary>
	static void SyncLastAddress(long address);

public:
	
	/// <summary>
	/// Creates buffer with defined size and sets input file stream. Creates new reading thread.
	/// </summary>
	/// <param name="bufferSize">The size of input buffer.</param>
	/// <param name="stream">Pointer to the input file stream.</param>
	static void Init(unsigned int bufferSize, FILE * stream);

	/// <summary>
	/// Get the reference to the one byte from the input buffer. Mapping of virtual address to the physical address in the input buffer is done internally.
	/// </summary>
	/// <param name="address">Virtual address that represent the offset from the beginning of the currently active NAL unit.</param>
	/// <returns>Pointer to one byte from the file stream with defined virtual address.</returns>
	static unsigned char * GetByte(long address);

	/// <summary>
	///	Releases the selected block of memory from the input buffer. This action signalize memory manager that the data from the buffer is not used anymore.
	/// </summary>
	/// <param name="from">Starting virtual address (Included) of the releasing memory block.</param>
	/// <param name="to">Ending virtual address (Included) of the releasing memory block.</param>
	static void Release(long from, long to);

	/// <summary>
	/// Resets starting virtual addres to the zero and adjust the end virtual address.
	/// This operation must be done when there is no active threads in critical section.
	/// </summary>
	static void ResetVirtualAddress();

	/// <summary>
	/// Checks if the given virtual address is located after EOF.
	/// </summary>
	/// <param name="address">Addressing location in bitstream.</param>
	/// <returns>Indication of EOF.</returns>
	static bool IsEOF(long address);

	static void JoinReadThread();

	static void ReleaseBuffer();

	static void Free();
};

