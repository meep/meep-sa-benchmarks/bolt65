# Bolt65

Bolt65 encoding/decoding suite

### **Compiling the application** 

All sources are located in folder src/. 
All headers are located in folder include/. 

The application can be built on Windows and Linux:

* In build/vc2017/Bolt65 there is Bolt65.sln file that can be opened in Visual Studio 2017 (works with Visual C++ and Intel compilers)
* In build/linux there is a Makefile that can be used to build both, encoder and decoder (*make bolt-encoder*, *make bolt-decoder* or *make all*)


### **Running the application** 

The applications (encoder and decoder) can be run several ways: 

	1.	By defining all options in configuration file and passing only the path of the configuration file to the application 

		Example (examples of configuration file can be found in /data/configurations/): 

			./bolt-encoder -c config.cfg

	2. By defining all options through command line separately. All the options are listed below
	
	3. By combining first two methods. In this case any option set in command line will override appropriate option defined in configuration file.


### **Input/Output options**

`--config, -c <filename>`

Use external config file.

---

`--input, -i <filename>`

Input filename, only raw YUV or Y4M supported.

---

`--output, -o <filename>`

Bitstream output file name.

The output will contain raw HEVC bitstream.

---

`--frames, -f <integer>`

Number of frames to be encoded.

---

`--picWidth, -w <integer>`

Input picture width. If input file is Y4M, picture width will be read from input file.

---

`--picHeight, -h <integer>`

Input picture Height. If input file is Y4M, picture height will be read from input file. 

---

`--framerate, -fr <integer>`

Source file frame rate.

---

### **Encoding info options**

`--qp, -q <integer>`

Specify base quantization parameter for Constant QP rate control. Default QP is 24. 

`--gop <string>`

Define GOP structure of output HEVC bitstream. I or P frames. If not defined All INTRA configuration will be used.

### **Algorithms**

## Search algorithms

Default algorithm is Three Step Search with search area of 6 pixels

`--full-integer-search`

Use full integer search in interprediction.

---

`--full-fractional-search`

Use full fractional search in interprediction.

---

`--three-step-search`

Use three step search in interprediction.

---

`--search-area <integer>`

Search area for full integer and fractional search.

---

## Interpolation algorithms

Default interpolation algorithm is standard HEVC 7/8 tap filter for luma and 4 tap filter for chroma. 

`--hevc-interpolation`

Use standard HEVC interpolation.

---

`--hevc-sf5`

Use standard HEVC interpolation with SF = 5. 

---

`--avc-interpolation`

Use interpolation as in previous AVC standard (6-tap filter)

---

`--bilinear-interpolation`

Use bilinear interpolation.

---

`--four-tap-interpolation`

Use 4-tap filter interpolation.

---

## Block matching algorithms

Default block matching algorithm is Sum of Absolute Differences (SAD)

`--sad`

Use Sum of Absoulte Differences

---

`--satd`

Use Sum of Absoulte Transform Differences (Fast Walsh–Hadamard transform algorithm)

---

`--mse`

Using Mean Square Error algorithm.

---

### **Flags**

## In-loop filters 

`--enable-sao-filter, --disable-sao-filter>`

Enable or disable SAO filtering.

---

`--enable-deblocking-filter, --disable-deblocking-filter>`

Enable or disable Deblocking filtering.

## Prediction units

`--enable-smp, --disable-smp`

Enable or disable Symetric Motion Prediction units.

---

`--enable-amp, --disable-amp`

Enable or disable Asymetric Motion Prediction units.

---

`--all-modes`

Both INTRA and INTER prediction will be evaluated for Coding units in P frames.

---

## Architectures

`--avx`

Enable Advances Vector eXtensions (AVX2) if the current architecture supports it. 

---

`--neon`

Enable ARM NEON vector extensions if the current architecture supports it. 

---

`--sve`

Enable ARM SVE vector extensions if the current architecture supports it. 

---

## Other

`--clustered-tq`

Instead of processing block by block, this option enables collecting residual data for the entire frame first, so it can process TQ and ITQ for a larger block of data. This is useful for example when using HW accelerator. However, this option will only be employed for I or B frames where every block is INTER predicted. This is due to the fact that for INTRA predicted blocks, neighbouring blocks have to be reconstructed first. 

---

`--epi-riscv`

Enable EPI RISC-V vector builtins if the current architecture supports it. 

---

### Multithreading

`--threads <integer>`

Define number of threads that will be used. Threads can be utilized only if number of tiles is larger than 1.

---

## Tiles

Tiles can be se uniformly or nonuniformly 

`--tile-rows <integer>`

Specify number of tiles in a row of frame. Height of each row will be calculated uniformly.

`--tile-columns <integer>`

Specify number of tiles in a column of frame. Width of each column will be calculated uniformly.

---

`--tile-rows <integer>;<integer>;<integer>;...`

Specify size of each tile in a row (as number of CTU's).

`--tile-columns <integer>;<integer>;<integer>;...`

Specify size of each tile in a column (as number of CTU's).


### **Debugging and statistics**

`--csv <filename>`

Outputs statistical information to file.

---

`--csv-tiles <filename>`

Outputs statistical information abotu tiles to file.

---

`--calculate-bitrate`

Calculate output video bitrate.

---

`--calculate-psnr`

Calculate output video Peak Signal to Nose Ratio for Y, U and V planes.

---

`--calculate-time`

Calculate time needed for video processing.

---

`--show-stats-per-frame`

Shows selected statistics after each frame

---

`--show-stats-per-tile`

Shows selected statistics after each tile in frame

---



	
