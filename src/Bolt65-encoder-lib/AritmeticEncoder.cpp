/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "AritmeticEncoder.h"


AritmeticEncoder::AritmeticEncoder()
{
}

AritmeticEncoder::~AritmeticEncoder()
{
}

void AritmeticEncoder::initArithmeticCoder(bool isFirstInSliceSegment, EntropyBuffer* _eb)
{
	ivlLow = 0;
	ivlCurrRange = 510;
	firstBitFlag = 1;
	bitsOutstanding = 0;

	eb = _eb;

	if (isFirstInSliceSegment) binCountsInNalInits = 0;
}


void AritmeticEncoder::initArithmeticCoder(bool isFirstInSliceSegment)
{
	ivlLow = 0;
	ivlCurrRange = 510;
	firstBitFlag = 1;
	bitsOutstanding = 0;

	if (isFirstInSliceSegment) binCountsInNalInits = 0;
}

/*-----------------------------------------------------------------------------
Function name:  encodeDecision
Description:	based on observed bin value selects corresponding subinterval as new coding interval and calculates
new probability model(probability estimate and potentially MPS value). Implementation based on
chapter 9.3.5.3 in [1]
Parameters:     probabModel			- pointer to probability model object with its parameters probability state and MPS value
binVal				- binary symbol being encoded
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
Remarks:		MC_todo think abour replacing parameter probabModel with syntax element where probabModel would be sub-class
MC_todo arithetic coder can not be local object in the method since arithmetic encoding is recursive operation. Remark
relates to all subsequent methods which return arithmeticCoder object.
-----------------------------------------------------------------------------*/
void AritmeticEncoder::encodeDecision(ProbabilityModel *probabilityModel, bool binVal)
{
	//MC_todo Think about encodeDecision as recursive method which would process complete bin string and generate entropy code
	unsigned short qRangeIdx;
	unsigned int ivlLpsRange;

	unsigned char pom = probabilityModel->pStateIdx;
	// Calculate R_MPS by substracting calculated R_LPS from initially given interval R
	// R_LPS is derived(table lookup) from probability model state and interval R's quantizier index
	qRangeIdx = (ivlCurrRange >> 6) & 3;
	ivlLpsRange = rangeTabLps[probabilityModel->pStateIdx * 4 + qRangeIdx];
	ivlCurrRange = ivlCurrRange - ivlLpsRange;

	if (binVal != probabilityModel->valMps)										// valLps
	{
		// LPS subinterval chosen as new coding interval and
		// its base is new base
		ivlLow += ivlCurrRange;
		ivlCurrRange = ivlLpsRange;

		// Toogle valMps if valLps was observed and pLPS reached max value in previous bin
		if (probabilityModel->pStateIdx == 0)
			probabilityModel->valMps = !probabilityModel->valMps;

		// Realize LPS transition in updating probability estimation
		probabilityModel->pStateIdx = transIdx[probabilityModel->pStateIdx * 3 + 1];
	}
	else																	// valMps, base stays unchanged, range already set
	{
		// Realize MPS transition in updating probability estimation
		probabilityModel->pStateIdx = transIdx[probabilityModel->pStateIdx * 3 + 2];
	}

	// Renormalization
	renormE();
	binCountsInNalInits++;
}

/*-----------------------------------------------------------------------------
Function name:  encodeBypass
Description:	based on observed bin value selects corresponding subinterval as new coding interval. Since bin value is uniformly
distributed initial range is halved. If joint beginning digits for selected subinterval can be identified they are
sent to output and renormalization iz carried out. If not carry over control is done. In compare to encodeDecision
with invoked renormE algorithm optimization was done here since range division factor is known in advance. Left shift
of lower bound ivlLow is moved to start of arithemtic enoding. Left shift of current range is compensated with range
halving. As consequence renormalization operates with doubled decision thresholds (chapter D, section 2 in [3]).
Implementation acc. 9.3.5.5 in [1].
Parameters:     binVal				- binary symbol being encoded
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
void AritmeticEncoder::encodeBypass(bool binVal)
{
	ivlLow <<= 1;

	//Subinterval selection, base remains unchanged if "0"
	if (binVal != 0)
		ivlLow += ivlCurrRange;

	//Renormalization with carry-over control
	if (ivlLow >= 1024)
	{
		putBit(1);
		ivlLow -= 1024;
	}
	else
	{
		if (ivlLow < 512)
		{
			putBit(0);
		}
		else
		{
			ivlLow -= 512;
			bitsOutstanding++;
		}
	}
	binCountsInNalInits++;
}

/*-----------------------------------------------------------------------------
Function name:  bypassAlignment
Description:	when  cabac_bypass_alignment_enabled_flag (SPS) is set alignment is performed before encoding s and ALRem syntax elements.
Specified in 9.3.5.5 in [1].
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
void AritmeticEncoder::bypassAlignment()
{
	ivlCurrRange = 256;
}

/*-----------------------------------------------------------------------------
Function name:  encodeTerminate
Description:	encoding routine for syntax elements end_of_slice_segment_flag, end_of_subset_one_bit, and pcm_flag. Last is delivered
for CU and first two for CTU syntax structure. If flag is set probability estimation process is abandoned and non-adapting
probability state n = 63 is used. In that case encodeFlush is invoked to properly terminate the arithmetic codeword. Implementation
acc. 9.3.5.6 in [1]
Parameters:     binVal				- binary symbol being encoded
pEncoderBitstream	- pointer to the bitstream where CABAC code is written
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/

void AritmeticEncoder::encodeTerminate(bool binVal)
{
	ivlCurrRange -= 2;
	if (binVal)
	{
		ivlLow += ivlCurrRange;
		encodeFlush();
	}
	else
	{
		renormE();
	}
	binCountsInNalInits++;
}

/*-----------------------------------------------------------------------------
Function name:  encodeFlush
Description:	sets LPS range to table value for probability state n = 63, invokes renormalization and sends 3 most significant symbols
of lower bound to the output, where the last one is set to 1 and represents stop or alignment bit depending on syntax element
which is encoded. Implementation acc. 9.3.5.6 in [1]
Parameters:		--
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
Remarks:		MC_todo writeBits is called with 2 bit word. According specification in chapter 9.3.5.4 it should receive only one bit. To clarify
-----------------------------------------------------------------------------*/
void AritmeticEncoder::encodeFlush()
{
	ivlCurrRange = 2;
	renormE();
	putBit((ivlLow >> 9) & 1);
	eb->writeBits(((ivlLow >> 8) & 1), 1);
	eb->writeBits(true, 1);
}

/*-----------------------------------------------------------------------------
Function name:  byteStuffing
Description:	process is invoked after encoding last CB of last slice segment of a picture and after encapsulation of payload.
Parameters:     rawMinCuBits			-
numBytesInVclNalUnits
picSizeInMinCbsY
binCountsInNalUnits
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
void AritmeticEncoder::byteStuffing(unsigned int rawMinCuBits, unsigned int numBytesInVclNalUnits, unsigned int picSizeInMinCbsY, unsigned int binCountsInNalUnits)
{
	/*
	double k;

	k = ceil((ceil(3 * (32 * binCountsInNalUnits - rawMinCuBits * picSizeInMinCbsY) / 1024) - numBytesInVclNalUnits) / 3);

	if (k > 0)
	{
	do
	{
	ioc->writeBytes(0x00, 2);
	ioc->writeBytes(0x03, 1);

	k--;
	} while (k > 0);
	}
	* */

}

/*-----------------------------------------------------------------------------
Function name:  renormE
Description:	to ensure that finite-precision registers are sufficient to represent lower bound ivlLow and width of selected
sub-interval ivlCurrRange during probability estimation process renormalization operation is carried out. It is
invoked every time after one or more subdivisions when ivlCurrRange falls below limit 2^(b-2) since then all values
in the range share certain beginning digits which are sent to output. Operation background explained in chapter D,
section 3 of [3]. Implementation acc. 9.3.5.4 in [1].
Parameters:     --
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
------------------------------------------ -----------------------------------*/
void AritmeticEncoder::renormE()
{
	while (ivlCurrRange < 256)
	{
		if (ivlLow < 256)
		{
			putBit(0);
		}
		else
		{
			if (ivlLow >= 512)
			{
				ivlLow -= 512;
				putBit(1);
			}
			else
			{
				ivlLow -= 256;
				bitsOutstanding++;
			}
		}
		ivlCurrRange <<= 1;
		ivlLow <<= 1;
	}

}

/*-----------------------------------------------------------------------------
Function name:  putBit
Description:	sends MSB bit value, which is shared among all values in the range, to output and provides carry-over
control for cases when values inside range in previous subdivisions haven't shared beginning digits. Method
is invoked by renormE method. Refer to its description for more informations. Implementation acc. chapter
9.3.5.4 in [1]
Parameters:     val		- value which is to be sent to output
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
------------------------------------------ -----------------------------------*/
void AritmeticEncoder::putBit(bool val)
{
	if (firstBitFlag != 0)
	{
		firstBitFlag = 0;
	}
	else
	{
		eb->writeBits(val, 1);
	}

	// Carry-over control
	while (bitsOutstanding > 0)
	{
		eb->writeBits(!val, 1);
		bitsOutstanding--;

	}
}