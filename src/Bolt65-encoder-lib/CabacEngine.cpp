/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CabacEngine.h"

CabacEngine::CabacEngine()
{
	probMods = nullptr;
}

void CabacEngine::CabacInit(ContextMemory * ctMem, int QP, EntropyBuffer *ec)
{
	ctxMem = ctMem;
	probMods = new ProbabilityModelSelection();

	bool cabac_init_flag = false;
	probMods->InitializeSelection(QP, cabac_init_flag);
	aritmeticEncoder.initArithmeticCoder(true, ec);
}

void CabacEngine::CabacReset(ContextMemory * ctMem, int QP, EntropyBuffer *ec)
{

	bool cabac_init_flag = false;
	probMods->InitializeSelection(QP, cabac_init_flag);
	aritmeticEncoder.initArithmeticCoder(true, ec);
}

CabacEngine::~CabacEngine()
{
}

void CabacEngine::cabacEncode(int binLength, unsigned long binBits, SyntaxElementInfo *info)
{
	for (int i = binLength - 1; i >= 0; i--)
	{
		bool binVal = false;

		if (((binBits >> i) & 1) != 0)
			binVal = true;

		unsigned short ctxIdxTmp = ctxMem->sliceType == P ? info->ctxIdxP : info->ctxIdx;
		unsigned short ctxInc;

		if ((binLength - 1) - i <= 5)
			ctxInc = ctxIncTable[info->ctxTableIdx][(binLength - 1) - i];
		else
			ctxInc = ctxIncTable[info->ctxTableIdx][5];

		if (ctxInc == nd)
		{
			ctxIdxTmp += derivateSpecCtxIdx(info->ctxTableIdx, ((binLength - 1) - i));
			aritmeticEncoder.encodeDecision(&probMods->probabilityModel[info->syntaxProbModelsIdx][ctxIdxTmp], binVal);
		}
		else if (ctxInc == bypass)
		{
			aritmeticEncoder.encodeBypass(binVal);
		}
		else if (ctxInc == toTerminate)
		{
			aritmeticEncoder.encodeTerminate(binVal);
		}
		else
		{
			ctxIdxTmp += ctxInc;
			aritmeticEncoder.encodeDecision(&probMods->probabilityModel[info->syntaxProbModelsIdx][ctxIdxTmp], binVal);
		}
	}
}

unsigned short CabacEngine::derivateSpecCtxIdx(int ctxIncTableIdx, int i)
{
	unsigned short ctxIdx = 0;

	switch (ctxIncTableIdx)
	{
	case 11: // split_cu_flag
		ctxIdx = ctxIdxOffset[4].ctxIdxOffset[0] + derivateCtxInc_split_cu_flag(ctxMem->cqtDepth, ctxMem->ctDepthL,
			ctxMem->ctDepthA, ctxMem->availableL, ctxMem->availableA);
		break;

	case 13: // cu_skip_flag
		ctxIdx = ctxIdxOffset[6].ctxIdxOffset[1] + derivateCtxInc_cu_skip_flag(ctxMem->left_cu_skip_flag, ctxMem->above_cu_skip_flag, ctxMem->availableL, ctxMem->availableA);
		break;

	case 30: // split_transform_flag
		ctxIdx = ctxIdxOffset[19].ctxIdxOffset[0] + 0;	// ctxInc(binIdx = 0) = 5 - log2Trafosize
		break;

	case 31: //cbf_cb
		ctxIdx = ctxIdxOffset[21].ctxIdxOffset[0] + ctxMem->trafoDepth;	// ctxInc(binIdx = 0) = trafoDepth
		break;

	case 32: // cbf_cr
		ctxIdx = ctxIdxOffset[22].ctxIdxOffset[0] + ctxMem->trafoDepth;	// ctxInc(binIdx = 0) = trafoDepth
		break;

	case 33: //cbf_luma
		ctxIdx = ctxIdxOffset[20].ctxIdxOffset[0] + (ctxMem->trafoDepth == 0 ? 1 : 0);
		break;

	case 47: // last_sig_coeff_x_prefix
		ctxIdx = ctxIdxOffset[33].ctxIdxOffset[0] + derivateCtxIncLAST(i, ctxMem->colorIdx, ctxMem->log2TrafoSize);
		break;

	case 48: // last_sig_coeff_y_prefix
		ctxIdx = ctxIdxOffset[34].ctxIdxOffset[0] + derivateCtxIncLAST(i, ctxMem->colorIdx, ctxMem->log2TrafoSize);
		break;

	case 51: // coded_sub_block_flag
		ctxIdx = ctxIdxOffset[35].ctxIdxOffset[0] + derivateCtxIncCSBF(ctxMem->colorIdx, ctxMem->subBlockRightCSBF, ctxMem->subBlockBottomCSBF,
			ctxMem->subBlockCoordinateX, ctxMem->subBlockCoordinateY, ctxMem->log2TrafoSize);
		break;

	case 52: //sig_coeff_flag
		ctxIdx = ctxIdxOffset[36].ctxIdxOffset[0] + derivateCtxIncSIG(ctxMem->colorIdx, ctxMem->transformBlock_xC, ctxMem->transformBlock_yC, ctxMem->scanIdx,
			ctxMem->log2TrafoSize, ctxMem->subBlockRightCSBF, ctxMem->subBlockBottomCSBF, 0, ctxMem->transform_skip_flag, ctxMem->cu_transquant_bypass_flag);
		break;

	case 53: // coeff_abs_level_greater1_flag
		ctxDataALG1 = derivateCtxIncALG1(ctxMem->colorIdx, ctxMem->sub_block_index, ctxMem->inside_sub_block_index, ctxMem->previous_ALG1_flag);
		ctxIdx = ctxIdxOffset[37].ctxIdxOffset[0] + ctxDataALG1.ctxInc;
		break;

	case 54: // coeff_abs_level_greater2_flag
		ctxIdx = ctxIdxOffset[38].ctxIdxOffset[0] + derivateCtxIncALG2(ctxMem->colorIdx, ctxDataALG1.ctxSet);
		break;
	}

	return ctxIdx;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxInc_split_cu_flag
Description:	calculates context increment for syntax elements split_cu_flag based on
availability of neighbouring (left and above) coding blocks and their depth. Possible values
are 0,1,2. Details in chapter 9.3.4.2.2 in [1]
Parameters:     cqtDepth	- depth of currently process CB
ctDepthNbL	- depth of left CB in z-scan order
ctDepthNbA	- depth of above CB in z-scan order
availableL	- availability of left CB in z-scan order
availableA	- availability of above CB in z-scan order
Returns:		ctxInc		- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char CabacEngine::derivateCtxInc_split_cu_flag(unsigned char cqtDepth, unsigned char ctDepthNbL, unsigned char ctDepthNbA, bool availableL, bool availableA)
{
	bool condL;
	bool condA;
	unsigned char ctxInc;

	condL = ctDepthNbL > cqtDepth;
	condA = ctDepthNbA > cqtDepth;

	ctxInc = (condL && availableL) + (condA && availableA);

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxInc_cu_skip_flag
Description:	calculates context increment for syntax elements cu_skip_flag based on
flag values for neighbouring (left and above) coding blocks and their depth. Possible values
are 0,1,2. Details in chapter 9.3.4.2.2 in [1]
Parameters:     cu_skip_flagNbL	- flag value for left neighboring CB
cu_skip_flagNbA	- flag value for above neighbouring CB
availableL		- availability of left CB in z-scan order
availableA		- availability of above CB in z-scan order
Returns:		ctxInc			- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char CabacEngine::derivateCtxInc_cu_skip_flag(bool cu_skip_flagNbL, bool cu_skip_flagNbA, bool availableL, bool availableA)
{
	bool condL;
	bool condA;
	unsigned char ctxInc;

	condL = cu_skip_flagNbL;
	condA = cu_skip_flagNbA;

	ctxInc = (condL && availableL) + (condA && availableA);

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncCSBF
Description:	calculates context increment for syntax element coded_sub_block_flag based on color component index,
and CSBF of transform sub-blocks right and below. Possible values are 0..3. For details see chapter 9.3.4.2.4
in [1]
Parameters:     cIdx			- color component index
csbfRight		- CSBF of the sub-block right to currently processed
csbfBottom		- CSBF of the sub-block bottom to currently processed
xS, yS			- scan location of current sub-block in TB
log2TrafoSize	- transform block size
Returns:		ctxInc			- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char CabacEngine::derivateCtxIncCSBF(unsigned short cIdx, bool csbfRight, bool csbfBottom, unsigned char xS, unsigned char yS, unsigned short log2TrafoSize)
{
	unsigned char csbfCtx = 0;
	unsigned char ctxInc;

	if (xS < ((1 << (log2TrafoSize - 2)) - 1))
		csbfCtx += csbfRight;						// csbf[xS + 1][yS]

	if (yS < ((1 << (log2TrafoSize - 2)) - 1))
		csbfCtx += csbfBottom;						// csbf[xS][yS + 1]

	if (cIdx == 0) {
		ctxInc = min(csbfCtx, (unsigned char)1);
	}
	else
	{
		ctxInc = 2 + min(csbfCtx, (unsigned char)1);
	}

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncLAST
Description:	calculates context increment for syntax elements last_sig_coeff_x_prefix and last_sig_coeff_y_prefix
based on bin position within syntax elements and transform block size. Possible values are 0..17. For
details see chapter 9.3.4.2.3 in [1] and 2nd paragraph in chapter 8.6.4.2 in [2]
Parameters:     binIdx		- bin index
cIdx		- color component index
log2TrafoSize - transform block size
Returns:		ctxInc		- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char CabacEngine::derivateCtxIncLAST(unsigned short binIdx, unsigned short cIdx, unsigned short log2TrafoSize)
{
	unsigned short ctxOffset, ctxShift;
	unsigned char ctxInc;

	if (cIdx == 0)
	{
		ctxOffset = 3 * (log2TrafoSize - 2) + ((log2TrafoSize - 1) >> 2);
		ctxShift = (log2TrafoSize + 1) >> 2;
	}
	else
	{
		ctxOffset = 15;
		ctxShift = log2TrafoSize - 2;
	}
	ctxInc = (binIdx >> ctxShift) + ctxOffset;

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncSIG
Description:	calculates context increment for syntax element sig_coeff_flag based on color component, TB size, scan order
(diag, hor, vert), position of the sub-block within TB (DC, non-DC sub-block) and position based context index
(pattern derived from right and bottom CSBF). Possible values 0..43. For details see chapter 9.3.4.2.5 in [1]
and table 8.11 in [2]
Parameters:     cIdx			- color component index
xC, yC			- current coefficient scan location in TB
scanIdx			- scan order index, 0 = diag, 1 = hor, 2 = ver
log2TrafoSize	- transform block size
csbfRight		- CSBF of the sub-block right to currently processed
csbfBottom		- CSBF of the sub-block bottom to currently processed
transform_skip_context_enabled_flag	- determines if context is fixed value or calculated from parameters defined above
transform_skip_flag					- partialy determines if transformation will be applied to current TB
cu_transquant_bypass_flag			- determines if scaling and transform and in-loop filter are bypassed
Returns:		ctxInc								- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char CabacEngine::derivateCtxIncSIG(unsigned short cIdx, unsigned char xC, unsigned char yC, unsigned char scanIdx, unsigned short log2TrafoSize, bool csbfRight, bool csbfBottom, bool transform_skip_context_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag)
{
	unsigned char sigCtx;
	unsigned char xS;										// index x for sub-block inside TB 
	unsigned char yS;										// index y for sub-block inside TB  
	unsigned char xP;										// index x for level element inside sub-block
	unsigned char yP;										// index y for level element inside sub-block
	unsigned char prevCsbf;
	unsigned char ctxInc;

	if (transform_skip_context_enabled_flag && (transform_skip_flag || cu_transquant_bypass_flag))
	{
		sigCtx = (cIdx == 0) ? 42 : 16;
	}
	else
	{
		if (log2TrafoSize == 2)								// 4x4 TB only
		{
			sigCtx = ctxIdxMapSIG[(yC << 2) + xC];
		}
		else if ((xC + yC) == 0)							// 8x8 - 32x32 TBs,	DC level in DC region, all 
		{
			sigCtx = 0;
		}
		else														// DC and non-DC subblocks
		{
			// 4 x 4 sub-block position based mapping context selection based on CSBF of neighboring subblocks, see Fig 8.19
			xS = xC >> 2;
			yS = yC >> 2;
			prevCsbf = 0;

			if (xS < ((1 << (log2TrafoSize - 2)) - 1))					// not a border CSBF on x axis
				prevCsbf += csbfRight;									// csbf[xS + 1][yS]

			if (yS < ((1 << (log2TrafoSize - 2)) - 1))					// not a border CSBF on y axis
				prevCsbf += (csbfBottom << 1);							// csbf[xS][yS + 1]

			xP = xC & 3;
			yP = yC & 3;

			if (prevCsbf == 0)
			{
				sigCtx = (xP + yP == 0) ? 2 : (xP + yP < 3) ? 1 : 0;	// pattern 1, 
			}
			else if (prevCsbf == 1)
			{
				sigCtx = (yP == 0) ? 2 : (yP == 1) ? 1 : 0;				// pattern 2,
			}
			else if (prevCsbf == 2)
			{
				sigCtx = (xP == 0) ? 2 : (xP == 1) ? 1 : 0;				// pattern 3
			}
			else
			{
				sigCtx = 2;												// pattern 4
			}

			if (cIdx == 0)									// luma
			{
				if ((xS + yS) > 0) sigCtx += 3;				// Differentiate subblock position

				if (log2TrafoSize == 3)
				{
					sigCtx += (scanIdx == 0) ? 9 : 15;			// Differentiate scan order for 8x8 TB
				}
				else
				{
					sigCtx += 21;								// 16x16 and 32x32 TBs							
				}

			}
			else											// chroma
			{
				if (log2TrafoSize == 3)							// 8x8 TB
				{
					sigCtx += 9;
				}
				else
				{
					sigCtx += 12;								// 8x8 TB							
				}
			}
		}
	}
	// Determine starting context index depending on color component
	if (cIdx == 0)
	{
		ctxInc = sigCtx;
	}
	else
	{
		ctxInc = 27 + sigCtx;
	}

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncALG1
Description:	calculates context increment for syntax element coeff_abs_level_greater1_flag based on context set which is assigned to
sub-block and previous flag value(s) in scan order. Further, context set depends on  color component index, sub-block
scan index (scan position within TB), coefficient scan index (scan position within sub-block) and method invocation count
for particular sub-block.  Possible values are 0..23. Detailed description in chapter 9.3.4.2.6 in [1]
Parameters:     cIdx			- color component index
subblockScanIdx	- position of sub-block to which flag belongs along TB reverse scanning pattern
coeffScanIdx	- position of the flag inside sub-block
lastALG1		- previous flag value along reverse scanning pattern of transform sub-block and blocks
Returns:		ctxIncWithCtxSet- context increment for syntax element and ctxSet which is used for ctxInc derivation of ALG2
Time (Cycles):	typ= xx cycl max = xx cycl
Remarks:		Specification acc. chapter 8.6.5.1 (p. 255) in [2] and [1] are different in No. of sets. To check with author MC_todo
-----------------------------------------------------------------------------*/

ctxStruct4ALG1 CabacEngine::derivateCtxIncALG1(unsigned short cIdx, unsigned char subblockScanIdx, unsigned char coeffScanIdx, bool lastALG1)
{
	if (ctxMem->first_time_in_TB == 1)
	{
		firstTime = true;
		activeSubBlockScanIdx = subblockScanIdx;
	}

	if (!firstTime)
	{
		if (activeSubBlockScanIdx != subblockScanIdx)
		{
			firstTime = true;
			activeSubBlockScanIdx = subblockScanIdx;
			greater1CtxPrevSubBlock = greater1Ctx;
		}
	}

	unsigned char lastGreater1Ctx;

	bool lastGreater1Flag;
	ctxStruct4ALG1 ctxIncWithCtxSet = ctxStruct4ALG1();

	if (firstTime)			// Method invoked first time for particular sub-block
	{
		// Assumption is that in reverse scanning pattern indices are incremented and that first processed sub-block has index 0
		// If this will be changed during controller implementation expression in if statement and init value of storage value have 
		// to be modified MC_todo
		// Storage parameter has to be reset when last sub-block in TB is processed MC_todo

		if ((subblockScanIdx == ctxMem->maxScanIdx - 1) || (cIdx > 0))
		{
			ctxIncWithCtxSet.ctxSet = 0;
		}
		else
		{
			ctxIncWithCtxSet.ctxSet = 2;
		}

		// Derivation of lastGreater1Ctx
		if (ctxMem->first_time_in_TB == 1)								// New sub-block is first sub-block processed along reverese scanning pattern in the TB
		{
			lastGreater1Ctx = 1;
			ctxMem->first_time_in_TB = 0;
		}
		else													// Newly started sub-block, but not first scanned in TB
		{
			// greater1Ctx is value derived during last invocation of this method for a previous sub-block
			lastGreater1Ctx = greater1CtxPrevSubBlock;

			// lastALG1 is here coeff_abs_level_greater1_flag that has been used during the last invocation of the method for previous sub-block
			if (lastGreater1Ctx > 0)
			{
				lastGreater1Flag = lastALG1;

				if (lastGreater1Flag == 1)
					lastGreater1Ctx = 0;
				else
					lastGreater1Ctx += 1;
			}
		}
		if (lastGreater1Ctx == 0) ctxIncWithCtxSet.ctxSet++;

		greater1Ctx = 1;
		firstTime = false;
	}
	else													// Method invoked for same sub-block
	{
		ctxIncWithCtxSet.ctxSet = ctxSet;

		// Variables ctxSet and greater1Ctx presevre their last values from previous invocation of this method
		if (greater1Ctx > 0)
		{
			// lastALG1 is here coeff_abs_level_greater1_flag that has been used during the last invocation of the method
			lastGreater1Flag = lastALG1;

			if (lastGreater1Flag == 1)
				greater1Ctx = 0;
			else
				greater1Ctx += 1;
		}
	}

	// Derivation of ctxInc
	ctxIncWithCtxSet.ctxInc = (ctxIncWithCtxSet.ctxSet * 4) + min((unsigned char)3, greater1Ctx);
	if (cIdx > 0)
	{
		ctxIncWithCtxSet.ctxInc += 16;
	}

	ctxSet = ctxIncWithCtxSet.ctxSet;

	return ctxIncWithCtxSet;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncALG2
Description:	calculates context increment for syntax element coeff_abs_level_greater2_flag based on color component index, sub-block
scan index (scan position within TB) and context set calculated for that sub-block in derivateCtxIncALG1 method. Possible values
are 0..5. For details see chapter 9.3.4.2.7 in [1]
Parameters:     cIdx			- color component index
ctxSet			- context set calculated for the sub-block in which processed ALG2 bin is located
Returns:		ctxInc			- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char CabacEngine::derivateCtxIncALG2(unsigned short cIdx, unsigned char ctxSet)
{
	unsigned char ctxInc;

	// MC_todo ctxSet, current context set, is equal to value of the variable ctxSet that has been derived for same subset i within method derivateCtxIncALG1
	// I am 90% sure that subset here relates to ctxSet derived for sub-block i. To be reexamined during controller realization
	ctxInc = ctxSet;

	if (cIdx > 0)
		ctxInc += 4;

	return ctxInc;
}

void  CabacEngine::clean()
{
	if (probMods != nullptr)
		delete probMods;
}