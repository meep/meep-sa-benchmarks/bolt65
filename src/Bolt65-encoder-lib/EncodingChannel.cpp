/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "EncodingChannel.h"

EncodingChannel::EncodingChannel()
{
	STATUS = NOT_STARTED;
}

EncodingChannel::~EncodingChannel()
{
}

void EncodingChannel::StartEncoding(char* argv[], int argc)
{
	Configure(argv, argc);
	OpenIOStreams();
	Initialize();
	Run();
}

void EncodingChannel::Configure(char* argv[], int argc)
{
	enCtx.appType = Encoder;
	cc.checkExternalConfig(&enCtx, argv, argc);

	if (enCtx.externalConfig)
	{
		cc.fetchConfigurationFromFile(&enCtx);
	}
	else
	{
		cout << "No external configuration file." << endl;
	}

	//Console parameters override configuration file 
	cc.parseConsoleParameters(&enCtx, argv, argc);

	cc.validateContextForEncoder(&enCtx);
}

void EncodingChannel::OpenIOStreams()
{
	ic.InitIO(&enCtx);
	STATUS = IO_STREAMS_OPENED;
}

void EncodingChannel::Initialize()
{
	dpbm.Init(enCtx.dpbSize);
	ec.Init(&enCtx);

	//initialize vector implementations for static classes
	ComUtil::initialize(&enCtx);
	Intraprediction::initialize(&enCtx);

	if (enCtx.tilesEnabled)
	{
		numOfPartitions = enCtx.numberOfTilesInColumn * enCtx.numberOfTilesInRow;
		enCtx.tileOffsets = new int[numOfPartitions + 1];
		enCtx.tileOffsets[0] = 0;
	}
	else
	{
		numOfPartitions = 1;
	}

	partitionProcessors = new PartitionProcessor * [numOfPartitions];

	for (int i = 0; i < numOfPartitions; i++)
	{
		partitionProcessors[i] = new PartitionProcessor();
	}

	STATUS = INITIALIZED;
}

void EncodingChannel::Run()
{
	int N_NAL = 0;
	bool isEndOfVideo = false;
	bool isEndOfVideoSequence = false;
	bool error = false;

	enCtx.currentVPSId = -1;
	enCtx.currentSPSId = -1;
	enCtx.currentPPSId = -1;

	bool addAccessUnitDelimiter = false;
	int numberOfFramesToEcode = enCtx.numOfFrames;

	ic.StartPrefetch(numberOfFramesToEcode);

	std::thread* processingBlockThread = new thread[enCtx.threads];

	if (enCtx.tilesEnabled)
		tilesPerThread = new int* [enCtx.threads];

	if (enCtx.usePolicy)
		Policy::initializePolicy(&enCtx);

	stats.outputTileStatsToFile(enCtx.outputStatsTileToFile, enCtx.outputStatsTileFileName);
	stats.startVideoClock(enCtx.calculateTime);

	//NAL_Unit_VCL Frame1;
	while (!isEndOfVideo && (STATUS == INITIALIZED || STATUS == IN_PROGRESS))
	{

		STATUS = IN_PROGRESS;

		if (addNewVPS)
		{
			enCtx.currentVPSId++;
			ec.parseNonVCL(VPS_NUT);
			addNewVPS = false;
		}
		if (addNewSPS)
		{
			enCtx.currentSPSId++;
			ec.parseNonVCL(SPS_NUT);
			addNewSPS = false;
		}

		//Starting new Video Sequence 
		Frame* frame;

		cout << "Starting encoding process" << endl << endl;

		while (!isEndOfVideoSequence)
		{
			//Process one frame
			stats.startFrameClock(enCtx.calculateTime);
			stats.startTestClock();

			char frameType = 'X';
			if (enCtx.GOP == "LD")
			{
				if (N_NAL == 0)
					frameType = 'I';
				else
					frameType = 'P';
			}
			else
			{
				frameType = enCtx.GOP.at(N_NAL % enCtx.GOP.size());
			}

			if (enCtx.showStatsPerFrame)
				cout << "Processing frame " << (char)toupper(frameType) << ": " << N_NAL;

			frame = new Frame(&enCtx, N_NAL, frameType);
			ic.GetNextFrame(frame, error);

			if (!error)
			{
				N_NAL++;

				NALType nalType = NALType::TRAIL_R;
				if (N_NAL == 1)
					nalType = NALType::IDR_W_RADL;

				if (numOfPartitions > 1)
				{
					clock_t partitionStart = clock();
					LoadBalancing::performTileLoadBalancing(frame, tilesPerThread, numOfPartitions, dpbm.dpb->frames[0], &enCtx, &stats, addNewPPS);
					stats.overhead += float(clock() - partitionStart) / CLOCKS_PER_SEC;

					if (enCtx.tilesEnabled)
						frame->splitToTiles();

					for (int i = 0; i < enCtx.threads; i++)
					{
						processingBlockThread[i] = thread(&EncodingChannel::ThreadProcessor, this, i, frame);
					}
					for (int i = 0; i < enCtx.threads; i++)
					{
						processingBlockThread[i].join();
					}
				}
				else
				{
					partitionProcessors[0]->initPartition(frame, &enCtx, &dpbm);
					partitionProcessors[0]->ec.ctxMem.CopySliceType(frame->sliceType);
					partitionProcessors[0]->processPartition();
				}

				if (addNewPPS)
				{
					enCtx.currentPPSId++;
					enCtx.currentPPSId = enCtx.currentPPSId % 64;
					ec.parseNonVCL(PPS_NUT);
					stats.sumBits += ic.writeBufferToFile(&ec.buffer);
					addNewPPS = false;
				}

				ec.parseVCL(nalType, frame);
				stats.sumBits += ic.writeBufferToFile(&ec.buffer);
				for (int i = 0; i < numOfPartitions; i++)
				{
					partitionProcessors[i]->ec.flushOutputBuffer();
					stats.frameBits += ic.writeBufferToFile(&partitionProcessors[i]->ec.buffer);

				}
				stats.sumBits += stats.frameBits;

				stats.addToTestClock();

				numberOfFramesToEcode--;

				dpbm.AddFrameToDPB(frame);

				if (enCtx.statMode)
					stats.calculateFrameStats(frame, &enCtx);

				if (enCtx.usePolicy)
					Policy::getOptimumConfiguration(&enCtx);

				if (enCtx.showStatsPerFrame)
					cout << endl;

				if (enCtx.tilesEnabled && enCtx.useDynamicTiles)
					adaptTiles(N_NAL);

				addNewPPS = checkIfNewPPSIsRequired();
			}
			else
			{
				cout << "End of source video reached. Encoding process finished" << endl;
			}

			isEndOfVideoSequence = checkIsEndOfVideoSequence(numberOfFramesToEcode, error);
		}

		if (enCtx.statMode)
			stats.showVideoStats(&enCtx);

		isEndOfVideo = checkIsEndOfVideo(numberOfFramesToEcode, error);

		addNewVPS = checkIfNewVPSIsRequired();
		addNewSPS = checkIfNewSPSIsRequired();
	}

	STATUS = LAST_FRAME_ENCODED;

	if (processingBlockThread != nullptr)
		delete[] processingBlockThread;

	clearMemory();
}

bool EncodingChannel::checkIsEndOfVideoSequence(int numOfFramesLeft, bool lastFrameProcessed)
{
	return numOfFramesLeft == 0 || lastFrameProcessed;
}

bool EncodingChannel::checkIsEndOfVideo(int numOfFramesLeft, bool lastFrameProcessed)
{
	return numOfFramesLeft == 0 || lastFrameProcessed;
}

bool EncodingChannel::checkIfNewPPSIsRequired()
{
	if (enCtx.tileChanged)
		return true;
	else
		return false;
}

bool EncodingChannel::checkIfNewSPSIsRequired()
{
	return false;
}

bool EncodingChannel::checkIfNewVPSIsRequired()
{
	return false;
}

void EncodingChannel::clearMemory()
{
	if (enCtx.tilesEnabled)
	{
		for (int i = 0; i < enCtx.numberOfTilesInColumn * enCtx.numberOfTilesInRow; i++)
		{
			delete partitionProcessors[i];
		}
		delete[] partitionProcessors;

	}
	else
	{
		delete partitionProcessors[0];
		delete[] partitionProcessors;
	}


	dpbm.ClearDPB();
	ic.closeFiles();
	ec.clearModels();
	enCtx.clearEncodingContext();
	stats.clear(enCtx.outputStatsTileToFile);

	if (enCtx.tilesEnabled)
	{
		for (int i = 0; i < enCtx.threads; i++)
			delete tilesPerThread[i];

		delete[] tilesPerThread;
	}

	ComUtil::clean();
	Intraprediction::clean();
}

void EncodingChannel::adaptTiles(int index)
{
	//Example for testing tile changes between frames (only for 2x2)
	if (index % 2 == 0)
	{
		reinitializeProcessPartitions(9, 4);

		enCtx.isUniform = false;
		enCtx.numberOfTilesInRow = 2;
		enCtx.numberOfTilesInColumn = 2;
		numOfPartitions = enCtx.numberOfTilesInRow * enCtx.numberOfTilesInColumn;

		enCtx.rowHeights = new int[2];
		enCtx.rowHeights[0] = 7;
		enCtx.rowHeights[1] = 10;

		enCtx.columnWidths = new int[2];
		enCtx.columnWidths[0] = 20;
		enCtx.columnWidths[1] = 10;

		enCtx.tileChanged = true;
	}
	else
	{

		reinitializeProcessPartitions(4, 9);

		enCtx.isUniform = false;
		enCtx.numberOfTilesInRow = 3;
		enCtx.numberOfTilesInColumn = 3;
		numOfPartitions = enCtx.numberOfTilesInRow * enCtx.numberOfTilesInColumn;

		enCtx.rowHeights = new int[3];
		enCtx.rowHeights[0] = 8;
		enCtx.rowHeights[1] = 4;
		enCtx.rowHeights[2] = 5;

		enCtx.columnWidths = new int[3];
		enCtx.columnWidths[0] = 10;
		enCtx.columnWidths[1] = 8;
		enCtx.columnWidths[2] = 12;

		enCtx.tileChanged = true;
	}

}

void EncodingChannel::reinitializeProcessPartitions(int oldNumberOfPartitions, int newNumberOfPartitions)
{
	//delete old partitions
	if (enCtx.tilesEnabled)
	{
		for (int i = 0; i < oldNumberOfPartitions; i++)
		{
			delete partitionProcessors[i];
		}
		delete[] partitionProcessors;

	}
	else
	{
		delete partitionProcessors[0];
		delete[] partitionProcessors;
	}

	//create new partitions
	partitionProcessors = new PartitionProcessor * [newNumberOfPartitions];

	for (int i = 0; i < newNumberOfPartitions; i++)
	{
		partitionProcessors[i] = new PartitionProcessor();
	}

}

void EncodingChannel::ThreadProcessor(int threadId, Frame* frame)
{
	int i = 0;
	int tileId = tilesPerThread[threadId][i++];

	while (tileId != -1)
	{
		partitionProcessors[tileId]->initPartition(frame->tiles[tileId], &enCtx, &dpbm);
		partitionProcessors[tileId]->ec.ctxMem.CopySliceType(frame->sliceType);
		partitionProcessors[tileId]->processPartition();
		enCtx.tileOffsets[tileId + 1] = (int)(frame->tiles[tileId]->bitSize / 8);
		tileId = tilesPerThread[threadId][i++];
	}

}
