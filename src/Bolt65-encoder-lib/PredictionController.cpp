/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "PredictionController.h"
#include "BlockPartition.h"

encoder::PredictionController::PredictionController()
{
	enCtx = nullptr;
	dpbm = nullptr;
	partition = nullptr;
	tQController = nullptr;
}

encoder::PredictionController::~PredictionController()
{

}

void encoder::PredictionController::Init(TQController* _tQController, DPBManager* _dpbm, EncodingContext* _enCtx)
{
	tQController = _tQController;
	dpbm = _dpbm;
	enCtx = _enCtx;
}

void encoder::PredictionController::PerformPrediction(Partition* _partition)
{
	partition = _partition;
	
	//Clustered mode is only available in P frames where all blocks are INTER predicted
	//This is because INTRA predicted blocks depend on reconstructed neigbouring blocks
	if (enCtx->clusteredTQ && (partition->type == StringConstants::FRAME_P || partition->type == StringConstants::FRAME_B) && !enCtx->testAllModes)
	{
		GenerateResidualFrame();
		PrepareTQData();
		tQController->PerformTQandIQT();
		FetchTQData();
		ReconstructFrame();
	}
	else
	{
		for (int i = 0; i < partition->numberOfCTUs; i++)
		{
			PredictBlock(partition->ctuTree[i], partition->type);
		}
	}
}

void encoder::PredictionController::PredictBlock(CU* cu, char frameType)
{

	// if is not leaf...
	if (cu->hasChildren)
	{
		if (cu->children[0]->isInFrame)
			PredictBlock(cu->children[0], frameType);

		if (cu->children[1]->isInFrame)
			PredictBlock(cu->children[1], frameType);


		if (cu->children[2]->isInFrame)
			PredictBlock(cu->children[2], frameType);

		if (cu->children[3]->isInFrame)
			PredictBlock(cu->children[3], frameType);
	}
	else // if is leaf
	{
		//In encoding CuPredMode should always be UNKNOWN at this point
		if (cu->CuPredMode == UNKNOWN)
		{
			if (frameType == StringConstants::FRAME_I)
			{
				GenerateResidualBlockIntra(cu);
				ConstructQTLevelsBlockIntra(cu);
				ReconstructBlockIntra(cu);
			}
			else if (frameType == StringConstants::FRAME_P || frameType == StringConstants::FRAME_B)
			{
				if (enCtx->testAllModes)
				{
					GenerateResidualBlockInterIntra(cu);
					ConstructQTLevelsBlockInterIntra(cu);
					ReconstructBlockInterIntra(cu);
				}
				else
				{
					GenerateResidualBlockInter(cu);
					ConstructQTLevelsBlockInter(cu);
					ReconstructBlockInter(cu);
				}

			}

		}
	}
}

void encoder::PredictionController::GenerateResidualBlockTransformUnit(TU* tu, int& intraPredModeY, int& intraPredModeChroma, int qpY, int qpU, int qpV, int _depth)
{
	//LUMA
	unsigned char* blockY = new unsigned char[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
	unsigned char* predictedBlockY = new unsigned char[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
	tu->tbY.residual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];

	unsigned char* refSamplesY = new unsigned char[4 * tu->tbY.transformBlockSize + 1];
	bool* refSamplesYExist = new bool[(4 * tu->tbY.transformBlockSize) + 1];

	//CHROMA
	unsigned char* blockU;
	unsigned char* blockV;
	unsigned char* predictedBlockU;
	unsigned char* predictedBlockV;

	unsigned char* refSamplesU;
	unsigned char* refSamplesV;
	bool* refSamplesUExist;
	bool* refSamplesVExist;

	BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockY, tu->tbY.startingIndexInFrame, tu->tbY.transformBlockSize, tu->tbY.transformBlockSize, enCtx->width, enCtx->height);
	BlockPartition::getReferenceSamples(tu->tbY.startingIndexInFrame, tu->tbY.startingIndex, tu->tbY.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesY, refSamplesYExist);

	//find best candidate from intra prediction modes.
	int blockMatchingValue, bestBlockMatchingValue, bestI;
	blockMatchingValue = 0;
	bestBlockMatchingValue = -1;

	if (tu->parent && tu->parent->tbY.transformBlockSize == 64 && tu->index != 0)
	{
		bestI = tu->parent->children[0]->tbY.intraPredMode;
	}
	else
	{
		for (int i = 0; i < 35; i++)
		{
			Intraprediction::GenerateIntraPredictedBlock(i, refSamplesY, refSamplesYExist, predictedBlockY, 0, tu->tbY.transformBlockSize);
			int blockMatchingValue = ComUtil::CalculateBlockMatchingValue(blockY, predictedBlockY, tu->tbY.transformBlockSize, tu->tbY.transformBlockSize, enCtx);

			if (blockMatchingValue < bestBlockMatchingValue || bestBlockMatchingValue < 0)
			{
				bestBlockMatchingValue = blockMatchingValue;
				bestI = i;
			}
		}
	}
	intraPredModeY = bestI;
	intraPredModeChroma = 4; // this means that CHROMA component is always DERIVED from LUMA.

	tu->tbY.intraPredMode = bestI;

	Intraprediction::GenerateIntraPredictedBlock(intraPredModeY, refSamplesY, refSamplesYExist, predictedBlockY, 0, tu->tbY.transformBlockSize);

	ComUtil::SubResidual(blockY, predictedBlockY, tu->tbY.residual, tu->tbY.transformBlockSize);

	if (tu->tbV.transformBlockSize < 4) // PART NxN, calculate chroma only for first child
	{
		if (tu->index == 0)
		{
			blockU = new unsigned char[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			blockV = new unsigned char[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];

			BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockU, tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.transformBlockSize, tu->parent->tbU.transformBlockSize, enCtx->width, enCtx->height);
			BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockV, tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.transformBlockSize, tu->parent->tbV.transformBlockSize, enCtx->width, enCtx->height);

			refSamplesU = new unsigned char[4 * tu->parent->tbU.transformBlockSize + 1];
			refSamplesV = new unsigned char[4 * tu->parent->tbV.transformBlockSize + 1];
			refSamplesUExist = new bool[(4 * tu->parent->tbU.transformBlockSize) + 1];
			refSamplesVExist = new bool[(4 * tu->parent->tbV.transformBlockSize) + 1];

			tu->parent->tbU.residual = new int16_t[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			tu->parent->tbV.residual = new int16_t[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];

			predictedBlockU = new unsigned char[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			predictedBlockV = new unsigned char[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];

			BlockPartition::getReferenceSamples(tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.startingIndex, tu->parent->tbU.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesU, refSamplesUExist);
			BlockPartition::getReferenceSamples(tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.startingIndex, tu->parent->tbV.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesV, refSamplesVExist);

			Intraprediction::GenerateIntraPredictedBlock(bestI, refSamplesU, refSamplesUExist, predictedBlockU, 1, tu->parent->tbU.transformBlockSize);
			Intraprediction::GenerateIntraPredictedBlock(bestI, refSamplesV, refSamplesVExist, predictedBlockV, 2, tu->parent->tbV.transformBlockSize);

			ComUtil::SubResidual(blockU, predictedBlockU, tu->parent->tbU.residual, tu->parent->tbU.transformBlockSize);
			ComUtil::SubResidual(blockV, predictedBlockV, tu->parent->tbV.residual, tu->parent->tbV.transformBlockSize);

			tu->parent->tbU.intraPredMode = bestI;
			tu->parent->tbV.intraPredMode = bestI;

			delete[] blockU;
			delete[] blockV;
			delete[] refSamplesUExist;
			delete[] refSamplesVExist;
			delete[] refSamplesU;
			delete[] refSamplesV;
			delete[] predictedBlockU;
			delete[] predictedBlockV;
		}
	}
	else
	{
		blockU = new unsigned char[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		blockV = new unsigned char[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockU, tu->tbU.startingIndexInFrame, tu->tbU.transformBlockSize, tu->tbU.transformBlockSize, enCtx->width, enCtx->height);
		BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockV, tu->tbV.startingIndexInFrame, tu->tbV.transformBlockSize, tu->tbU.transformBlockSize, enCtx->width, enCtx->height);

		refSamplesU = new unsigned char[4 * tu->tbU.transformBlockSize + 1];
		refSamplesV = new unsigned char[4 * tu->tbV.transformBlockSize + 1];
		refSamplesUExist = new bool[(4 * tu->tbU.transformBlockSize) + 1];
		refSamplesVExist = new bool[(4 * tu->tbV.transformBlockSize) + 1];

		tu->tbU.residual = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		tu->tbV.residual = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		predictedBlockU = new unsigned char[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		predictedBlockV = new unsigned char[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		BlockPartition::getReferenceSamples(tu->tbU.startingIndexInFrame, tu->tbU.startingIndex, tu->tbU.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesU, refSamplesUExist);
		BlockPartition::getReferenceSamples(tu->tbV.startingIndexInFrame, tu->tbV.startingIndex, tu->tbV.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesV, refSamplesVExist);

		Intraprediction::GenerateIntraPredictedBlock(bestI, refSamplesU, refSamplesUExist, predictedBlockU, 1, tu->tbU.transformBlockSize);
		Intraprediction::GenerateIntraPredictedBlock(bestI, refSamplesV, refSamplesVExist, predictedBlockV, 2, tu->tbV.transformBlockSize);

		ComUtil::SubResidual(blockU, predictedBlockU, tu->tbU.residual, tu->tbU.transformBlockSize);
		ComUtil::SubResidual(blockV, predictedBlockV, tu->tbV.residual, tu->tbV.transformBlockSize);

		tu->tbU.intraPredMode = bestI;
		tu->tbV.intraPredMode = bestI;

		delete[] blockU;
		delete[] blockV;
		delete[] refSamplesUExist;
		delete[] refSamplesVExist;
		delete[] refSamplesU;
		delete[] refSamplesV;
		delete[] predictedBlockU;
		delete[] predictedBlockV;

	}

	delete[] blockY;
	delete[] refSamplesYExist;
	delete[] refSamplesY;
	delete[] predictedBlockY;
}

void encoder::PredictionController::ConstructQTLevelsBlockIntraTransformUnit(TU* tu, int& intraPredModeY, int& intraPredModeChroma, int qpY, int qpU, int qpV, int _depth)
{
	tu->tbY.QTResidual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
	tQController->PerformTransformAndQuantization(qpY, tu->tbY.residual, tu->tbY.transformBlockSize, &tu->tbY.isZeroMatrix, tu->tbY.QTResidual, 0, PredMode::MODE_INTRA);

	if (tu->tbV.transformBlockSize < 4) // PART NxN, calculate chroma only for first child
	{
		if (tu->index == 0)
		{
			tu->parent->tbU.QTResidual = new int16_t[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			tQController->PerformTransformAndQuantization(qpU, tu->parent->tbU.QTResidual, tu->parent->tbU.transformBlockSize, &tu->parent->tbU.isZeroMatrix, tu->tbU.QTResidual, 1, PredMode::MODE_INTRA);

			tu->parent->tbV.QTResidual = new int16_t[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];
			tQController->PerformTransformAndQuantization(qpV, tu->parent->tbV.QTResidual, tu->parent->tbV.transformBlockSize, &tu->parent->tbV.isZeroMatrix, tu->tbV.QTResidual, 2, PredMode::MODE_INTRA);
		}
	}
	else
	{
		tu->tbU.QTResidual = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		tu->tbV.QTResidual = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		tQController->PerformTransformAndQuantization(qpU, tu->tbU.residual, tu->tbU.transformBlockSize, &tu->tbU.isZeroMatrix, tu->tbU.QTResidual, 1, PredMode::MODE_INTRA);
		tQController->PerformTransformAndQuantization(qpV, tu->tbV.residual, tu->tbV.transformBlockSize, &tu->tbV.isZeroMatrix, tu->tbV.QTResidual, 2, PredMode::MODE_INTRA);

		if (!tu->tbU.isZeroMatrix && tu->parent)
			tu->parent->tbU.isZeroMatrix = tu->tbU.isZeroMatrix;
		if (!tu->tbV.isZeroMatrix && tu->parent)
			tu->parent->tbV.isZeroMatrix = tu->tbV.isZeroMatrix;
	}
}

void encoder::PredictionController::ReconstructBlockIntraTransformUnit(TU* tu, int& intraPredModeY, int& intraPredModeChroma, int qpY, int qpU, int qpV, int _depth)
{
	//LUMA
	int16_t* reconstructedResidualBlockY = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
	unsigned char* refSamplesY = new unsigned char[4 * tu->tbY.transformBlockSize + 1];
	bool* refSamplesYExist = new bool[(4 * tu->tbY.transformBlockSize) + 1];
	unsigned char* predictedBlockY = new unsigned char[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
	unsigned char* reconstructedBlockY = new unsigned char[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];

	//CHROMA
	unsigned char* refSamplesU;
	unsigned char* refSamplesV;
	bool* refSamplesUExist;
	bool* refSamplesVExist;
	unsigned char* predictedBlockU;
	unsigned char* predictedBlockV;
	int16_t* reconstructedResidualBlockU;
	int16_t* reconstructedResidualBlockV;
	unsigned char* reconstructedBlockU;
	unsigned char* reconstructedBlockV;

	//REFERENCE SAMPLES LUMA
	BlockPartition::getReferenceSamples(tu->tbY.startingIndexInFrame, tu->tbY.startingIndex, tu->tbY.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesY, refSamplesYExist);
	Intraprediction::GenerateIntraPredictedBlock(tu->tbY.intraPredMode, refSamplesY, refSamplesYExist, predictedBlockY, 0, tu->tbY.transformBlockSize);

	tQController->PerformInverseTransformAndQuantization(qpY, tu->tbY.QTResidual, tu->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTRA);
	ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, tu->tbY.transformBlockSize, tu->tbY.transformBlockSize, 0);
	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockY, tu->tbY.startingIndexInFrame, tu->tbY.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

	if (tu->tbV.transformBlockSize < 4) // PART NxN, calculate chroma only for first child
	{
		if (tu->index == 0)
		{
			refSamplesU = new unsigned char[4 * tu->parent->tbU.transformBlockSize + 1];
			refSamplesV = new unsigned char[4 * tu->parent->tbV.transformBlockSize + 1];

			refSamplesUExist = new bool[(4 * tu->parent->tbU.transformBlockSize) + 1];
			refSamplesVExist = new bool[(4 * tu->parent->tbV.transformBlockSize) + 1];

			predictedBlockU = new unsigned char[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			predictedBlockV = new unsigned char[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];

			reconstructedResidualBlockU = new int16_t[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			reconstructedResidualBlockV = new int16_t[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];

			reconstructedBlockU = new unsigned char[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			reconstructedBlockV = new unsigned char[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];


			BlockPartition::getReferenceSamples(tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.startingIndex, tu->parent->tbU.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesU, refSamplesUExist);
			BlockPartition::getReferenceSamples(tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.startingIndex, tu->parent->tbV.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesV, refSamplesVExist);

			tQController->PerformInverseTransformAndQuantization(qpU, tu->parent->tbU.QTResidual, tu->parent->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTRA);
			ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, tu->parent->tbU.transformBlockSize, tu->parent->tbU.transformBlockSize, 0);
			BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockU, tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

			tQController->PerformInverseTransformAndQuantization(qpV, tu->parent->tbV.QTResidual, tu->parent->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTRA);
			ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, tu->parent->tbV.transformBlockSize, tu->parent->tbV.transformBlockSize, 0);
			BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockV, tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

			delete[] refSamplesUExist;
			delete[] refSamplesVExist;
			delete[] refSamplesU;
			delete[] refSamplesV;
			delete[] predictedBlockU;
			delete[] predictedBlockV;
			delete[] reconstructedBlockU;
			delete[] reconstructedBlockV;
			delete[] reconstructedResidualBlockU;
			delete[] reconstructedResidualBlockV;
		}
	}
	else
	{
		refSamplesU = new unsigned char[4 * tu->tbU.transformBlockSize + 1];
		refSamplesV = new unsigned char[4 * tu->tbV.transformBlockSize + 1];

		refSamplesUExist = new bool[(4 * tu->tbU.transformBlockSize) + 1];
		refSamplesVExist = new bool[(4 * tu->tbV.transformBlockSize) + 1];

		predictedBlockU = new unsigned char[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		predictedBlockV = new unsigned char[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		reconstructedResidualBlockU = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		reconstructedResidualBlockV = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		reconstructedBlockU = new unsigned char[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		reconstructedBlockV = new unsigned char[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		BlockPartition::getReferenceSamples(tu->tbU.startingIndexInFrame, tu->tbU.startingIndex, tu->tbU.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesU, refSamplesUExist);
		BlockPartition::getReferenceSamples(tu->tbV.startingIndexInFrame, tu->tbV.startingIndex, tu->tbV.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesV, refSamplesVExist);

		Intraprediction::GenerateIntraPredictedBlock(tu->tbU.intraPredMode, refSamplesU, refSamplesUExist, predictedBlockU, 1, tu->tbU.transformBlockSize);
		Intraprediction::GenerateIntraPredictedBlock(tu->tbV.intraPredMode, refSamplesV, refSamplesVExist, predictedBlockV, 2, tu->tbV.transformBlockSize);

		tQController->PerformInverseTransformAndQuantization(qpU, tu->tbU.QTResidual, tu->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTRA);
		ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, tu->tbU.transformBlockSize, tu->tbU.transformBlockSize, 0);
		BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockU, tu->tbU.startingIndexInFrame, tu->tbU.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

		tQController->PerformInverseTransformAndQuantization(qpV, tu->tbV.QTResidual, tu->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTRA);
		ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, tu->tbV.transformBlockSize, tu->tbV.transformBlockSize, 0);
		BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockV, tu->tbV.startingIndexInFrame, tu->tbV.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

		delete[] refSamplesUExist;
		delete[] refSamplesVExist;
		delete[] refSamplesU;
		delete[] refSamplesV;
		delete[] predictedBlockU;
		delete[] predictedBlockV;
		delete[] reconstructedBlockU;
		delete[] reconstructedBlockV;
		delete[] reconstructedResidualBlockU;
		delete[] reconstructedResidualBlockV;

	}

	delete[] refSamplesYExist;
	delete[] refSamplesY;
	delete[] predictedBlockY;
	delete[] reconstructedBlockY;
	delete[] reconstructedResidualBlockY;
}

void encoder::PredictionController::GenerateResidualBlockIntra(CU* cu)
{
	cu->CuPredMode = MODE_INTRA;
	cu->PredModeFlag = true;

	Intraprediction::FindBestPredictionIntra(cu, partition, enCtx);

	if (cu->PartMode == INTRA_PART_NxN)
	{
		for (int i = 0; i < 4; i++) //pretpostavljamo da je za intra pu = tu uvijek
		{
			GenerateResidualBlockTransformUnit(cu->transformTree->children[i], cu->IntraPredModeY[i], cu->IntraChromaPredMode, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
		}
	}
	else
	{
		GenerateResidualBlockTransformUnit(cu->transformTree, cu->IntraPredModeY[0], cu->IntraChromaPredMode, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
	}
}

void encoder::PredictionController::ConstructQTLevelsBlockIntra(CU* cu)
{
	if (cu->PartMode == INTRA_PART_NxN)
	{
		for (int i = 0; i < 4; i++) //pretpostavljamo da je za intra pu = tu uvijek
		{
			ConstructQTLevelsBlockIntraTransformUnit(cu->transformTree->children[i], cu->IntraPredModeY[i], cu->IntraChromaPredMode, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
		}
	}
	else
	{
		ConstructQTLevelsBlockIntraTransformUnit(cu->transformTree, cu->IntraPredModeY[0], cu->IntraChromaPredMode, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
	}
	cu->encodeIntraPredDecision(cu->frameWidth);
}

void encoder::PredictionController::ReconstructBlockIntra(CU* cu)
{
	if (cu->PartMode == INTRA_PART_NxN)
	{
		for (int i = 0; i < 4; i++) //pretpostavljamo da je za intra pu = tu uvijek
		{
			ReconstructBlockIntraTransformUnit(cu->transformTree->children[i], cu->IntraPredModeY[i], cu->IntraChromaPredMode, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
		}
	}
	else
	{
		ReconstructBlockIntraTransformUnit(cu->transformTree, cu->IntraPredModeY[0], cu->IntraChromaPredMode, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
	}
}

void encoder::PredictionController::GenerateResidualBlockInter(CU* cu)
{
	cu->CuPredMode = MODE_INTER;
	cu->PredModeFlag = false;

	unsigned char* blockY = new unsigned char[cu->cbY.blockSize * cu->cbY.blockSize];
	unsigned char* blockU = new unsigned char[cu->cbU.blockSize * cu->cbU.blockSize];
	unsigned char* blockV = new unsigned char[cu->cbV.blockSize * cu->cbV.blockSize];

	BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockY, cu->cbY.startingIndexInFrame, cu->cbY.blockSize, cu->cbY.blockSize, enCtx->width, enCtx->height);
	BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockU, cu->cbU.startingIndexInFrame, cu->cbU.blockSize, cu->cbU.blockSize, enCtx->width, enCtx->height);
	BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockV, cu->cbV.startingIndexInFrame, cu->cbV.blockSize, cu->cbV.blockSize, enCtx->width, enCtx->height);

	unsigned char* predictedBlockY = new unsigned char[cu->cbY.blockSize * cu->cbY.blockSize];
	unsigned char* predictedBlockU = new unsigned char[cu->cbU.blockSize * cu->cbU.blockSize];
	unsigned char* predictedBlockV = new unsigned char[cu->cbV.blockSize * cu->cbV.blockSize];

	Interprediction::FindBestPredictionInter(cu, partition, dpbm, enCtx);

	int chosenBmcValue = -1;

	//TODO Check How to avoid new Estimation after the best one was found
	for (int i = 0; i < cu->numOfPUs; ++i)
	{
		unsigned char* blockYPuOriginal = new unsigned char[cu->predictionUnits[i].pbY.width * cu->predictionUnits[i].pbY.height];

		BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockYPuOriginal, cu->predictionUnits[i].pbY.startingIndexInFrame, cu->predictionUnits[i].pbY.width, cu->predictionUnits[i].pbY.height, enCtx->width, enCtx->height);
		Interprediction::EstimateMotion(dpbm->dpb->frames[0], blockYPuOriginal, cu->cbY.blockSize, &cu->predictionUnits[i], enCtx, chosenBmcValue);

		delete[] blockYPuOriginal;

	}

	if (cu->numOfPUs == 2)
	{
		if (cu->predictionUnits[0].MVx1 == cu->predictionUnits[1].MVx1 && cu->predictionUnits[0].MVy1 == cu->predictionUnits[1].MVy1)
			cu->MergePredictionUnits(cu->frameWidth);
	}

	for (int i = 0; i < cu->numOfPUs; ++i)
	{
		/*	cu->predictionUnits[i].MVt1 = 1;
			cu->predictionUnits[i].isProcessed = true;*/

		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockY, enCtx->width, enCtx->height, cu->predictionUnits[i].pbY.width, cu->predictionUnits[i].pbY.height, i, cu->predictionUnits[i].pbY.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbY.blockSize, enCtx->width, Luma);
		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockU, enCtx->width, enCtx->height, cu->predictionUnits[i].pbU.width, cu->predictionUnits[i].pbU.height, i, cu->predictionUnits[i].pbU.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbU.blockSize, enCtx->width / 2, ChromaCb);
		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockV, enCtx->width, enCtx->height, cu->predictionUnits[i].pbV.width, cu->predictionUnits[i].pbV.height, i, cu->predictionUnits[i].pbV.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbV.blockSize, enCtx->width / 2, ChromaCr);

	}

	cu->InitializeTransformTree();
	if (cu->PartMode != PART_2Nx2N)
	{
		cu->transformTree->splitTU(cu->partitionWidth, cu->frameWidth);
	}

	if (cu->transformTree->hasChildren == false)
	{
		cu->transformTree->tbY.residual = new int16_t[cu->transformTree->tbY.transformBlockSize * cu->transformTree->tbY.transformBlockSize];
		cu->transformTree->tbU.residual = new int16_t[cu->transformTree->tbU.transformBlockSize * cu->transformTree->tbU.transformBlockSize];
		cu->transformTree->tbV.residual = new int16_t[cu->transformTree->tbV.transformBlockSize * cu->transformTree->tbV.transformBlockSize];

		ComUtil::SubResidual(blockY, predictedBlockY, cu->transformTree->tbY.residual, cu->cbY.blockSize);
		ComUtil::SubResidual(blockU, predictedBlockU, cu->transformTree->tbU.residual, cu->cbU.blockSize);
		ComUtil::SubResidual(blockV, predictedBlockV, cu->transformTree->tbV.residual, cu->cbV.blockSize);


		/*SubstractResidualTB(blockY, predictedBlockY, cu->cbY.blockSize, &cu->transformTree->tbY);
		SubstractResidualTB(blockU, predictedBlockU, cu->cbU.blockSize, &cu->transformTree->tbU);
		SubstractResidualTB(blockV, predictedBlockV, cu->cbV.blockSize, &cu->transformTree->tbV);*/
	}
	else
	{
		if (cu->transformTree->tbY.transformBlockSize > 8)
		{
			for (int i = 0; i < 4; i++)
			{
				cu->transformTree->children[i]->tbY.residual = new int16_t[cu->transformTree->children[i]->tbY.transformBlockSize * cu->transformTree->children[i]->tbY.transformBlockSize];
				cu->transformTree->children[i]->tbU.residual = new int16_t[cu->transformTree->children[i]->tbU.transformBlockSize * cu->transformTree->children[i]->tbU.transformBlockSize];
				cu->transformTree->children[i]->tbV.residual = new int16_t[cu->transformTree->children[i]->tbV.transformBlockSize * cu->transformTree->children[i]->tbV.transformBlockSize];
			}
			ComUtil::SubResidualWithTransformTree(blockY, predictedBlockY, cu->cbY.blockSize, cu->transformTree, Luma);
			ComUtil::SubResidualWithTransformTree(blockU, predictedBlockU, cu->cbU.blockSize, cu->transformTree, ChromaCb);
			ComUtil::SubResidualWithTransformTree(blockV, predictedBlockV, cu->cbV.blockSize, cu->transformTree, ChromaCr);
		}
		else
		{
			for (int i = 0; i < 4; i++)
			{
				cu->transformTree->children[i]->tbY.residual = new int16_t[cu->transformTree->children[i]->tbY.transformBlockSize * cu->transformTree->children[i]->tbY.transformBlockSize];
			}

			cu->transformTree->tbU.residual = new int16_t[cu->transformTree->tbU.transformBlockSize * cu->transformTree->tbU.transformBlockSize];
			cu->transformTree->tbV.residual = new int16_t[cu->transformTree->tbV.transformBlockSize * cu->transformTree->tbV.transformBlockSize];

			ComUtil::SubResidualWithTransformTree(blockY, predictedBlockY, cu->cbY.blockSize, cu->transformTree, Luma);
			ComUtil::SubResidual(blockU, predictedBlockU, cu->transformTree->tbU.residual, cu->cbU.blockSize);
			ComUtil::SubResidual(blockV, predictedBlockV, cu->transformTree->tbV.residual, cu->cbV.blockSize);
		}
	}


	delete[] blockY;
	delete[] blockU;
	delete[] blockV;
	delete[] predictedBlockY;
	delete[] predictedBlockU;
	delete[] predictedBlockV;
}

void encoder::PredictionController::ConstructQTLevelsBlockInter(CU* cu)
{

	if (cu->transformTree->hasChildren == false)
	{
		cu->transformTree->tbY.QTResidual = new int16_t[cu->transformTree->tbY.transformBlockSize * cu->transformTree->tbY.transformBlockSize];
		cu->transformTree->tbU.QTResidual = new int16_t[cu->transformTree->tbU.transformBlockSize * cu->transformTree->tbU.transformBlockSize];
		cu->transformTree->tbV.QTResidual = new int16_t[cu->transformTree->tbV.transformBlockSize * cu->transformTree->tbV.transformBlockSize];

		tQController->PerformTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->tbY.residual, cu->transformTree->tbY.transformBlockSize, &cu->transformTree->tbY.isZeroMatrix, cu->transformTree->tbY.QTResidual, 0, MODE_INTER);
		tQController->PerformTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->tbU.residual, cu->transformTree->tbU.transformBlockSize, &cu->transformTree->tbU.isZeroMatrix, cu->transformTree->tbU.QTResidual, 1, MODE_INTER);
		tQController->PerformTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->tbV.residual, cu->transformTree->tbV.transformBlockSize, &cu->transformTree->tbV.isZeroMatrix, cu->transformTree->tbV.QTResidual, 2, MODE_INTER);
	}
	else
	{
		if (cu->transformTree->tbY.transformBlockSize > 8)
		{
			int isZeroMatrixY = 1;
			int isZeroMatrixU = 1;
			int isZeroMatrixV = 1;

			for (int i = 0; i < 4; ++i)
			{
				cu->transformTree->children[i]->tbY.QTResidual = new int16_t[cu->transformTree->children[i]->tbY.transformBlockSize * cu->transformTree->children[i]->tbY.transformBlockSize];
				cu->transformTree->children[i]->tbU.QTResidual = new int16_t[cu->transformTree->children[i]->tbU.transformBlockSize * cu->transformTree->children[i]->tbU.transformBlockSize];
				cu->transformTree->children[i]->tbV.QTResidual = new int16_t[cu->transformTree->children[i]->tbV.transformBlockSize * cu->transformTree->children[i]->tbV.transformBlockSize];

				tQController->PerformTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->children[i]->tbY.residual, cu->transformTree->children[i]->tbY.transformBlockSize, &cu->transformTree->children[i]->tbY.isZeroMatrix, cu->transformTree->children[i]->tbY.QTResidual, 0, MODE_INTER);
				tQController->PerformTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->children[i]->tbU.residual, cu->transformTree->children[i]->tbU.transformBlockSize, &cu->transformTree->children[i]->tbU.isZeroMatrix, cu->transformTree->children[i]->tbU.QTResidual, 1, MODE_INTER);
				tQController->PerformTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->children[i]->tbV.residual, cu->transformTree->children[i]->tbV.transformBlockSize, &cu->transformTree->children[i]->tbV.isZeroMatrix, cu->transformTree->children[i]->tbV.QTResidual, 2, MODE_INTER);

				if (isZeroMatrixY)
					if (!cu->transformTree->children[i]->tbY.isZeroMatrix)
						isZeroMatrixY = 0;
				if (isZeroMatrixU)
					if (!cu->transformTree->children[i]->tbU.isZeroMatrix)
						isZeroMatrixU = 0;
				if (isZeroMatrixV)
					if (!cu->transformTree->children[i]->tbV.isZeroMatrix)
						isZeroMatrixV = 0;
			}

			cu->transformTree->tbY.isZeroMatrix = isZeroMatrixY;
			cu->transformTree->tbU.isZeroMatrix = isZeroMatrixU;
			cu->transformTree->tbV.isZeroMatrix = isZeroMatrixV;

		}
		else
		{
			for (int i = 0; i < 4; ++i)
			{
				cu->transformTree->children[i]->tbY.QTResidual = new int16_t[cu->transformTree->children[i]->tbY.transformBlockSize * cu->transformTree->children[i]->tbY.transformBlockSize];
				tQController->PerformTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->children[i]->tbY.residual, cu->transformTree->children[i]->tbY.transformBlockSize, &cu->transformTree->children[i]->tbY.isZeroMatrix, cu->transformTree->children[i]->tbY.QTResidual, 0, MODE_INTER);
			}

			cu->transformTree->tbU.QTResidual = new int16_t[cu->transformTree->tbU.transformBlockSize * cu->transformTree->tbU.transformBlockSize];
			cu->transformTree->tbV.QTResidual = new int16_t[cu->transformTree->tbV.transformBlockSize * cu->transformTree->tbV.transformBlockSize];

			tQController->PerformTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->tbU.residual, cu->transformTree->tbU.transformBlockSize, &cu->transformTree->tbU.isZeroMatrix, cu->transformTree->tbU.QTResidual, 1, MODE_INTER);
			tQController->PerformTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->tbV.residual, cu->transformTree->tbV.transformBlockSize, &cu->transformTree->tbV.isZeroMatrix, cu->transformTree->tbV.QTResidual, 2, MODE_INTER);
		}
	}

}

void encoder::PredictionController::ReconstructBlockInter(CU* cu)
{
	unsigned char* predictedBlockY = new unsigned char[cu->cbY.blockSize * cu->cbY.blockSize];
	unsigned char* predictedBlockU = new unsigned char[cu->cbU.blockSize * cu->cbU.blockSize];
	unsigned char* predictedBlockV = new unsigned char[cu->cbV.blockSize * cu->cbV.blockSize];

	for (int i = 0; i < cu->numOfPUs; ++i)
	{
		cu->predictionUnits[i].MVt1 = 1;
		cu->predictionUnits[i].isProcessed = true;

		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockY, enCtx->width, enCtx->height, cu->predictionUnits[i].pbY.width, cu->predictionUnits[i].pbY.height, i, cu->predictionUnits[i].pbY.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbY.blockSize, enCtx->width, Luma);
		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockU, enCtx->width, enCtx->height, cu->predictionUnits[i].pbU.width, cu->predictionUnits[i].pbU.height, i, cu->predictionUnits[i].pbU.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbU.blockSize, enCtx->width / 2, ChromaCb);
		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockV, enCtx->width, enCtx->height, cu->predictionUnits[i].pbV.width, cu->predictionUnits[i].pbV.height, i, cu->predictionUnits[i].pbV.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbV.blockSize, enCtx->width / 2, ChromaCr);

	}

	int16_t* reconstructedResidualBlockY;
	int16_t* reconstructedResidualBlockU;
	int16_t* reconstructedResidualBlockV;

	unsigned char* reconstructedBlockY = new unsigned char[cu->cbY.blockSize * cu->cbY.blockSize];
	unsigned char* reconstructedBlockU = new unsigned char[cu->cbU.blockSize * cu->cbU.blockSize];
	unsigned char* reconstructedBlockV = new unsigned char[cu->cbV.blockSize * cu->cbV.blockSize];

	if (cu->transformTree->hasChildren == false)
	{
		reconstructedResidualBlockY = new int16_t[cu->transformTree->tbY.transformBlockSize * cu->transformTree->tbY.transformBlockSize];
		reconstructedResidualBlockU = new int16_t[cu->transformTree->tbU.transformBlockSize * cu->transformTree->tbU.transformBlockSize];
		reconstructedResidualBlockV = new int16_t[cu->transformTree->tbV.transformBlockSize * cu->transformTree->tbV.transformBlockSize];

		tQController->PerformInverseTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->tbY.QTResidual, cu->transformTree->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTER);
		tQController->PerformInverseTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->tbU.QTResidual, cu->transformTree->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTER);
		tQController->PerformInverseTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->tbV.QTResidual, cu->transformTree->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTER);

		ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, cu->transformTree->tbY.transformBlockSize, cu->cbY.blockSize, 0);
		ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, cu->transformTree->tbU.transformBlockSize, cu->cbU.blockSize, 0);
		ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, cu->transformTree->tbV.transformBlockSize, cu->cbV.blockSize, 0);
	}
	else
	{
		if (cu->transformTree->tbY.transformBlockSize > 8)
		{

			reconstructedResidualBlockY = new int16_t[cu->transformTree->children[0]->tbY.transformBlockSize * cu->transformTree->children[0]->tbY.transformBlockSize];
			reconstructedResidualBlockU = new int16_t[cu->transformTree->children[0]->tbU.transformBlockSize * cu->transformTree->children[0]->tbU.transformBlockSize];
			reconstructedResidualBlockV = new int16_t[cu->transformTree->children[0]->tbV.transformBlockSize * cu->transformTree->children[0]->tbV.transformBlockSize];

			for (int i = 0; i < 4; ++i)
			{
				tQController->PerformInverseTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->children[i]->tbY.QTResidual, cu->transformTree->children[i]->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTER);
				tQController->PerformInverseTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->children[i]->tbU.QTResidual, cu->transformTree->children[i]->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTER);
				tQController->PerformInverseTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->children[i]->tbV.QTResidual, cu->transformTree->children[i]->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTER);

				ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, cu->transformTree->children[i]->tbY.transformBlockSize, cu->cbY.blockSize, i);
				ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, cu->transformTree->children[i]->tbU.transformBlockSize, cu->cbU.blockSize, i);
				ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, cu->transformTree->children[i]->tbV.transformBlockSize, cu->cbV.blockSize, i);
			}
		}
		else
		{
			reconstructedResidualBlockY = new int16_t[cu->transformTree->children[0]->tbY.transformBlockSize * cu->transformTree->children[0]->tbY.transformBlockSize];
			reconstructedResidualBlockU = new int16_t[cu->transformTree->tbU.transformBlockSize * cu->transformTree->tbU.transformBlockSize];
			reconstructedResidualBlockV = new int16_t[cu->transformTree->tbV.transformBlockSize * cu->transformTree->tbV.transformBlockSize];

			for (int i = 0; i < 4; ++i)
			{
				tQController->PerformInverseTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->children[i]->tbY.QTResidual, cu->transformTree->children[i]->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTER);
				ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, cu->transformTree->children[i]->tbY.transformBlockSize, cu->cbY.blockSize, i);

			}
			tQController->PerformInverseTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->tbU.QTResidual, cu->transformTree->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTER);
			tQController->PerformInverseTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->tbV.QTResidual, cu->transformTree->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTER);
			ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, cu->transformTree->tbU.transformBlockSize, cu->cbU.blockSize, 0);
			ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, cu->transformTree->tbV.transformBlockSize, cu->cbV.blockSize, 0);
		}
	}

	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockY, cu->cbY.startingIndexInFrame, cu->cbY.blockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);
	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockU, cu->cbU.startingIndexInFrame, cu->cbU.blockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);
	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockV, cu->cbV.startingIndexInFrame, cu->cbV.blockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

	Interprediction::encodeInterPredDecision(cu, partition, enCtx->ctbSize, enCtx->useTemporalAMVP);

	delete[] predictedBlockY;
	delete[] predictedBlockU;
	delete[] predictedBlockV;
	delete[] reconstructedBlockY;
	delete[] reconstructedBlockV;
	delete[] reconstructedBlockU;
	delete[] reconstructedResidualBlockY;
	delete[] reconstructedResidualBlockU;
	delete[] reconstructedResidualBlockV;
}

void encoder::PredictionController::GenerateResidualBlockInterIntra(CU* cu)
{
	unsigned char* blockY = new unsigned char[cu->cbY.blockSize * cu->cbY.blockSize];
	unsigned char* predictedBlockY = new unsigned char[cu->cbY.blockSize * cu->cbY.blockSize];
	unsigned char* refSamplesY = new unsigned char[4 * cu->cbY.blockSize + 1];
	bool* refSamplesYExist = new bool[(4 * cu->cbY.blockSize) + 1];

	BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockY, cu->cbY.startingIndexInFrame, cu->cbY.blockSize, cu->cbY.blockSize, enCtx->width, enCtx->height);
	BlockPartition::getReferenceSamples(cu->cbY.startingIndexInFrame, cu->cbY.startingIndex, cu->cbY.blockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesY, refSamplesYExist);


	//Calculate best INTRA mode
	int i, blockMatchingValue, bestBlockMatchingValueINTRA, bestI;
	blockMatchingValue = 0;
	bestBlockMatchingValueINTRA = -1;

	for (i = 0; i < 35; i++)
	{
		// LUMA COMPONENT searches for best candidate
		Intraprediction::GenerateIntraPredictedBlock(i, refSamplesY, refSamplesYExist, predictedBlockY, 0, cu->cbY.blockSize);
		blockMatchingValue = ComUtil::CalculateBlockMatchingValue(blockY, predictedBlockY, cu->cbY.blockSize, cu->cbY.blockSize, enCtx);

		if (blockMatchingValue < bestBlockMatchingValueINTRA || bestBlockMatchingValueINTRA < 0)
		{
			bestBlockMatchingValueINTRA = blockMatchingValue;
			bestI = i;
		}
	}


	delete[] refSamplesY;
	delete[] refSamplesYExist;

	//Calculate best INTER mode
	int bestBlockMatchingValueINTER = Interprediction::FindBestPredictionInter(cu, partition, dpbm, enCtx);

	//This happens when SMP and AMP are bot disabled, so the search is not even performed. THe best part mode in this case has to be PART_2Nx2N
	//Therefore, in this case we have to check it the best for Inter prediction manually
	if (bestBlockMatchingValueINTER == -1)
	{
		cu->PartMode = INTRA_PART_2Nx2N;
		cu->CreatePredictionUnits(cu->frameWidth);
		Interprediction::EstimateMotion(dpbm->dpb->frames[0], blockY, cu->cbY.blockSize, &cu->predictionUnits[0], enCtx, bestBlockMatchingValueINTER);
	}

	delete[] blockY;
	delete[] predictedBlockY;


	//TODO
	//This way FindBestPrediction.. is being called two times, first time with command above and second time within PredictBlockInter() function
	//Check how to aviod this behaviour
	//For now, functionality is priority 

	if (bestBlockMatchingValueINTRA <= bestBlockMatchingValueINTER)
	{
		GenerateResidualBlockIntra(cu);
	}
	else
	{
		GenerateResidualBlockInter(cu);
	}
}

void encoder::PredictionController::ConstructQTLevelsBlockInterIntra(CU* cu)
{
	if (cu->CuPredMode == MODE_INTER)
	{
		ConstructQTLevelsBlockInter(cu);
	}
	else
	{
		ConstructQTLevelsBlockIntra(cu);
	}
}

void encoder::PredictionController::ReconstructBlockInterIntra(CU* cu)
{
	if (cu->CuPredMode == MODE_INTER)
	{
		ReconstructBlockInter(cu);
	}
	else
	{
		ReconstructBlockIntra(cu);
	}
}

void encoder::PredictionController::PrepareTQData()
{
	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		PrepareTQData(partition->ctuTree[i]);
	}
}

void encoder::PredictionController::PrepareTQData(CU* cu)
{
	if (cu->hasChildren)
	{
		if (cu->children[0]->isInFrame)
			PrepareTQData(cu->children[0]);

		if (cu->children[1]->isInFrame)
			PrepareTQData(cu->children[1]);

		if (cu->children[2]->isInFrame)
			PrepareTQData(cu->children[2]);

		if (cu->children[3]->isInFrame)
			PrepareTQData(cu->children[3]);
	}
	else
	{
		//Fill residual arrays 
		if (cu->transformTree->hasChildren)
		{
			PrepareTQData(cu->transformTree->children[0]);
			PrepareTQData(cu->transformTree->children[1]);
			PrepareTQData(cu->transformTree->children[2]);
			PrepareTQData(cu->transformTree->children[3]);
		}
		else
		{
			PrepareTQData(cu->transformTree);
		}
	}


}

void encoder::PredictionController::PrepareTQData(TU* tu)
{
	if (tu->tbY.transformBlockSize == 32)
	{
		tu->tbY.residualIdx = tQController->residual32Size;
		memcpy(&tQController->residualFrame32[tu->tbY.residualIdx], tu->tbY.residual, sizeof(int16_t) * 1024);
		tQController->residual32Size += 1024;

		tu->tbU.residualIdx = tQController->residual16Size;
		memcpy(&tQController->residualFrame16[tu->tbU.residualIdx], tu->tbU.residual, sizeof(int16_t) * 256);
		tQController->residual16Size += 256;

		tu->tbV.residualIdx = tQController->residual16Size;
		memcpy(&tQController->residualFrame16[tu->tbV.residualIdx], tu->tbV.residual, sizeof(int16_t) * 256);
		tQController->residual16Size += 256;
	}
	else if (tu->tbY.transformBlockSize == 16)
	{
		tu->tbY.residualIdx = tQController->residual16Size;
		memcpy(&tQController->residualFrame16[tu->tbY.residualIdx], tu->tbY.residual, sizeof(int16_t) * 256);
		tQController->residual16Size += 256;

		tu->tbU.residualIdx = tQController->residual8Size;
		memcpy(&tQController->residualFrame8[tu->tbU.residualIdx], tu->tbU.residual, sizeof(int16_t) * 64);
		tQController->residual8Size += 64;

		tu->tbV.residualIdx = tQController->residual8Size;
		memcpy(&tQController->residualFrame8[tu->tbV.residualIdx], tu->tbV.residual, sizeof(int16_t) * 64);
		tQController->residual8Size += 64;
	}
	else if (tu->tbY.transformBlockSize == 8)
	{
		tu->tbY.residualIdx = tQController->residual8Size;
		memcpy(&tQController->residualFrame8[tu->tbY.residualIdx], tu->tbY.residual, sizeof(int16_t) * 64);
		tQController->residual8Size += 64;

		tu->tbU.residualIdx = tQController->residual4Size;
		memcpy(&tQController->residualFrame4[tu->tbU.residualIdx], tu->tbU.residual, sizeof(int16_t) * 16);
		tQController->residual4Size += 16;

		tu->tbV.residualIdx = tQController->residual4Size;
		memcpy(&tQController->residualFrame4[tu->tbV.residualIdx], tu->tbV.residual, sizeof(int16_t) * 16);
		tQController->residual4Size += 16;

	}
	else if (tu->tbY.transformBlockSize == 4)
	{
		tu->tbY.residualIdx = tQController->residual4Size;
		memcpy(&tQController->residualFrame4[tu->tbY.residualIdx], tu->tbY.residual, sizeof(int16_t) * 16);
		tQController->residual4Size += 16;

		//If TU is 4x4 it means that its CHROMA COMPONENTS are in its parent, but we calculate that only for the first child
		if (tu->index == 0)
		{
			tu->parent->tbU.residualIdx = tQController->residual4Size;
			memcpy(&tQController->residualFrame4[tu->parent->tbU.residualIdx], tu->parent->tbU.residual, sizeof(int16_t) * 16);
			tQController->residual4Size += 16;

			tu->parent->tbV.residualIdx = tQController->residual4Size;
			memcpy(&tQController->residualFrame4[tu->parent->tbV.residualIdx], tu->parent->tbV.residual, sizeof(int16_t) * 16);
			tQController->residual4Size += 16;
		}
	}
}

void encoder::PredictionController::FetchTQData()
{
	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		FetchTQData(partition->ctuTree[i]);
	}
}

void encoder::PredictionController::FetchTQData(CU* cu)
{
	if (cu->hasChildren)
	{
		if (cu->children[0]->isInFrame)
			FetchTQData(cu->children[0]);

		if (cu->children[1]->isInFrame)
			FetchTQData(cu->children[1]);

		if (cu->children[2]->isInFrame)
			FetchTQData(cu->children[2]);

		if (cu->children[3]->isInFrame)
			FetchTQData(cu->children[3]);
	}
	else
	{
		//Fill residual arrays 
		if (cu->transformTree->hasChildren)
		{
			if (cu->transformTree->tbY.transformBlockSize > 8)
			{
				int isZeroMatrixY = 1;
				int isZeroMatrixU = 1;
				int isZeroMatrixV = 1;

				for (int i = 0; i < 4; i++)
				{
					FetchTQData(cu->transformTree->children[i]);

					if (isZeroMatrixY)
						if (!cu->transformTree->children[i]->tbY.isZeroMatrix)
							isZeroMatrixY = 0;
					if (isZeroMatrixU)
						if (!cu->transformTree->children[i]->tbU.isZeroMatrix)
							isZeroMatrixU = 0;
					if (isZeroMatrixV)
						if (!cu->transformTree->children[i]->tbV.isZeroMatrix)
							isZeroMatrixV = 0;
				}

				cu->transformTree->tbY.isZeroMatrix = isZeroMatrixY;
				cu->transformTree->tbU.isZeroMatrix = isZeroMatrixU;
				cu->transformTree->tbV.isZeroMatrix = isZeroMatrixV;
			}
			else
			{
				FetchTQData(cu->transformTree->children[0]);
				FetchTQData(cu->transformTree->children[1]);
				FetchTQData(cu->transformTree->children[2]);
				FetchTQData(cu->transformTree->children[3]);
			}
		}
		else
		{
			FetchTQData(cu->transformTree);
		}
	}


}

void encoder::PredictionController::FetchTQData(TU* tu)
{
	if (tu->tbY.transformBlockSize == 32)
	{
		tu->tbY.QTResidual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
		tu->tbU.QTResidual = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		tu->tbV.QTResidual = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		memcpy(tu->tbY.QTResidual, &tQController->qtFrame32[tu->tbY.residualIdx], sizeof(int16_t) * 1024);
		memcpy(tu->tbU.QTResidual, &tQController->qtFrame16[tu->tbU.residualIdx], sizeof(int16_t) * 256);
		memcpy(tu->tbV.QTResidual, &tQController->qtFrame16[tu->tbV.residualIdx], sizeof(int16_t) * 256);

		if (tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbY.isZeroMatrix = false;
		else
			tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] = 0;

		if (tu->tbU.QTResidual[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbU.isZeroMatrix = false;
		else
			tu->tbU.QTResidual[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize - 1] = 0;

		if (tu->tbV.QTResidual[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbV.isZeroMatrix = false;
		else
			tu->tbV.QTResidual[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize - 1] = 0;
	}
	else if (tu->tbY.transformBlockSize == 16)
	{
		tu->tbY.QTResidual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
		tu->tbU.QTResidual = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		tu->tbV.QTResidual = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		memcpy(tu->tbY.QTResidual, &tQController->qtFrame16[tu->tbY.residualIdx], sizeof(int16_t) * 256);
		memcpy(tu->tbU.QTResidual, &tQController->qtFrame8[tu->tbU.residualIdx], sizeof(int16_t) * 64);
		memcpy(tu->tbV.QTResidual, &tQController->qtFrame8[tu->tbV.residualIdx], sizeof(int16_t) * 64);

		if (tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbY.isZeroMatrix = false;
		else
			tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] = 0;

		if (tu->tbU.QTResidual[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbU.isZeroMatrix = false;
		else
			tu->tbU.QTResidual[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize - 1] = 0;

		if (tu->tbV.QTResidual[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbV.isZeroMatrix = false;
		else
			tu->tbV.QTResidual[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize - 1] = 0;
	}
	else if (tu->tbY.transformBlockSize == 8)
	{
		tu->tbY.QTResidual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
		tu->tbU.QTResidual = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize];
		tu->tbV.QTResidual = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize];

		memcpy(tu->tbY.QTResidual, &tQController->qtFrame8[tu->tbY.residualIdx], sizeof(int16_t) * 64);
		memcpy(tu->tbU.QTResidual, &tQController->qtFrame4[tu->tbU.residualIdx], sizeof(int16_t) * 16);
		memcpy(tu->tbV.QTResidual, &tQController->qtFrame4[tu->tbV.residualIdx], sizeof(int16_t) * 16);

		if (tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbY.isZeroMatrix = false;
		else
			tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] = 0;

		if (tu->tbU.QTResidual[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbU.isZeroMatrix = false;
		else
			tu->tbU.QTResidual[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize - 1] = 0;

		if (tu->tbV.QTResidual[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbV.isZeroMatrix = false;
		else
			tu->tbV.QTResidual[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize - 1] = 0;

	}
	else if (tu->tbY.transformBlockSize == 4)
	{
		tu->tbY.QTResidual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize];
		memcpy(tu->tbY.QTResidual, &tQController->qtFrame4[tu->tbY.residualIdx], sizeof(int16_t) * 16);

		if (tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] != (int16_t)0x8000)
			tu->tbY.isZeroMatrix = false;
		else
			tu->tbY.QTResidual[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize - 1] = 0;

		//If TU is 4x4 it means that its CHROMA COMPONENTS are in its parent, but we calculate that only for the first child
		if (tu->index == 0)
		{
			tu->parent->tbU.QTResidual = new int16_t[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize];
			tu->parent->tbV.QTResidual = new int16_t[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize];

			memcpy(tu->parent->tbU.QTResidual, &tQController->qtFrame4[tu->parent->tbU.residualIdx], sizeof(int16_t) * 16);
			memcpy(tu->parent->tbV.QTResidual, &tQController->qtFrame4[tu->parent->tbV.residualIdx], sizeof(int16_t) * 16);

			if (tu->parent->tbU.QTResidual[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize - 1] != (int16_t)0x8000)
				tu->parent->tbU.isZeroMatrix = false;
			else
				tu->parent->tbU.QTResidual[tu->parent->tbU.transformBlockSize * tu->parent->tbU.transformBlockSize - 1] = 0;

			if (tu->parent->tbV.QTResidual[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize - 1] != (int16_t)0x8000)
				tu->parent->tbV.isZeroMatrix = false;
			else
				tu->parent->tbV.QTResidual[tu->parent->tbV.transformBlockSize * tu->parent->tbV.transformBlockSize - 1] = 0;
		}
	}
}

void encoder::PredictionController::ReconstructFrame()
{
	for (int i = 0; i < partition->numberOfCTUs; i++)
		ReconstructFrame(partition->ctuTree[i]);
}

void encoder::PredictionController::ReconstructFrame(CU* cu)
{

	if (cu->hasChildren)
	{
		if (cu->children[0]->isInFrame)
			ReconstructFrame(cu->children[0]);

		if (cu->children[1]->isInFrame)
			ReconstructFrame(cu->children[1]);


		if (cu->children[2]->isInFrame)
			ReconstructFrame(cu->children[2]);


		if (cu->children[3]->isInFrame)
			ReconstructFrame(cu->children[3]);
	}
	else
	{
		//We are working this way only for INTER frames
		ReconstructBlockInter(cu);
	}
}

void encoder::PredictionController::GenerateResidualFrame()
{
	for (int i = 0; i < partition->numberOfCTUs; i++)
		GenerateResidualFrame(partition->ctuTree[i]);
}

void encoder::PredictionController::GenerateResidualFrame(CU* cu)
{

	if (cu->hasChildren)
	{
		if (cu->children[0]->isInFrame)
			GenerateResidualFrame(cu->children[0]);

		if (cu->children[1]->isInFrame)
			GenerateResidualFrame(cu->children[1]);


		if (cu->children[2]->isInFrame)
			GenerateResidualFrame(cu->children[2]);


		if (cu->children[3]->isInFrame)
			GenerateResidualFrame(cu->children[3]);
	}
	else
	{
		//We are working this way only for INTER frames
		GenerateResidualBlockInter(cu);
	}
}
