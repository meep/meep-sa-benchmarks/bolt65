/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "PartitionProcessor.h"

encoder::PartitionProcessor::PartitionProcessor()
{
}

encoder::PartitionProcessor::~PartitionProcessor()
{
}

void encoder::PartitionProcessor::processPartition()
{
	clock_t partitionStart = clock();

	ctb.CreateCTBTree(partition);
	ctb.CreateRelations(partition);
	pc.PerformPrediction(partition);
	loopc.PerformDeblockingFilter(partition);
	Scanner::PerformScanning(partition);

	ec.parseSliceData(partition);

	ec.refreshCabacEngine();

	tq.Clean();

	//Clear memory of current EntropyController
	ec.clearModels();

	partition->proccesingTime = float(clock() - partitionStart) / CLOCKS_PER_SEC;
	partition->bitSize = ec.buffer.Param_Number_of_bits;
}

void encoder::PartitionProcessor::initPartition(Partition *_partition, EncodingContext *enCtx, DPBManager *dpbm)
{
	partition = _partition;
	ctb.Init(enCtx);
	tq.Init(enCtx, partition);
	pc.Init(&tq, dpbm, enCtx);
	loopc.Init(enCtx);
	ec.Init(enCtx);
	
}

vector<unsigned char>* encoder::PartitionProcessor::getBuffer()
{
	return &ec.buffer.output_buffer;
}
