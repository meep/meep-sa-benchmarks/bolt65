/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "IOController.h"

encoder::IOController::IOController()
{

}

encoder::IOController::IOController(EncodingContext *enCtx)
{
	ioKernel = ioBlock();

	remove(enCtx->outputFilePath.c_str());
	ioKernel.openOutputFile(enCtx->outputFilePath);
}

encoder::IOController::~IOController()
{

	fetchThread.join();
}

void encoder::IOController::PreFetchFrame()
{
	while (nFrames > 0)
	{
		bool finished = false;
		bool isYuv = inputType == YUV;
		unsigned char* frame_payload = (unsigned char*)ioKernel.readNextFrame(100, isYuv, finished);
		if (finished)
		{
			//Thread has read all frames. Thread will exit.
			nFrames = 0;
			return;
		}
		else
		{
			std::unique_lock<std::mutex> lk(mx);
			inputBufferNotFull.wait(lk, [this]() {return inputBuffer.size() != inputBufSize; });

			inputBuffer.push(frame_payload);
			//cout << "putting frame to buffer:" << 30 - nFrames << endl;
			--nFrames;
			lk.unlock();

			inputBufferNotEmpty.notify_all();
		}
	}
}

void encoder::IOController::InitIO(EncodingContext* enCtx)
{
	inputBufSize = enCtx->inputBufSize;
	outputBufSize = enCtx->outputBufSize;

	std::size_t versionPos = enCtx->outputFilePath.find("<v>");

	if (versionPos != -1)
	{
		enCtx->outputFilePath.erase(versionPos, 3);
		enCtx->outputFilePath.insert(versionPos, enCtx->version);
	}

	remove(enCtx->outputFilePath.c_str());
	ioKernel.openOutputFile(enCtx->outputFilePath);

	if (!enCtx->inputFilePath.empty())
	{
		determineInputFileType(enCtx->inputFilePath);

		frameNumber = 0;
		path = enCtx->inputFilePath;
		const char *cPath = path.c_str();
		inputFilePointer = fopen(cPath, "rb");

		if (inputFilePointer == NULL)
		{
			cout << "Error opening file " << cPath << ". Terminating." << endl;
			exit(EXIT_FAILURE);
		}
		else
		{
			cout << "The file " << cPath << " was opened." << endl;
		}

		fseek(inputFilePointer, 0, SEEK_END);
		ioKernel.inputFileSize = ftell(inputFilePointer);
		rewind(inputFilePointer);

		ioKernel.inputFilePointer = inputFilePointer;


		if (inputType == Y4M)
		{
			cout << "Input file in Y4M format." << endl;

			ioKernel.readHeader();

			if ((enCtx->width != -1 || enCtx->height != -1) && (enCtx->width != ioKernel.FWidth || enCtx->height != ioKernel.FHeight))
			{
				cout << "Ignoring picture width and height set by config.cfg : ";
				cout << "Using picture width: " << ioKernel.FWidth << " and height: " << ioKernel.FHeight << endl;
			}

			enCtx->width = ioKernel.FWidth;
			enCtx->height = ioKernel.FHeight;

			ioKernel.FSize = (ioKernel.FWidth * ioKernel.FHeight) * 3 / 2;
		}

		else if (inputType == YUV)
		{
			cout << "Input file in YUV format." << endl;
			if (enCtx->width == -1 || enCtx->height == -1)
			{
				cout << "No input frame width and height specified." << endl;
				exit(EXIT_FAILURE);
			}
			//inputFile = ioKernel.openFile(path, enCtx->width, enCtx->height);
		}
		ioKernel.FSize = (enCtx->width * enCtx->height) * 3 / 2;
		ioKernel.FWidth = enCtx->width;
		ioKernel.FHeight = enCtx->height;
		if (inputFilePointer == NULL)
		{
			cout << "Error opening file: " << enCtx->inputFilePath << " - File does not exist" << endl;
			exit(EXIT_FAILURE);
		}
	}
}

/*-----------------------------------------------------------------------------
Function name: GetNextFrame
Author/s: Cobrnic, Dragic
Current Version: v1.0
Description: Get next frame will read input buffer instead of reading from file.

Version history: v0.1
Description: Constructor for IOController object which will control reading from
input file with encoded video. It resets position counters for bytes and buffers
which take part in byte stream reading, opens input file and reads first block of
data into output buffer. Two LSB of emulation buffer are loaded as precondition
for read bit engine.



Parameters:
Inputs:
-	name of the input file

Outputs:
-	reset position counters
-	initial load of output and emulation buffer
-----------------------------------------------------------------------------*/
void encoder::IOController::GetNextFrame(Frame* frame, bool& lastFrameProccessed)
{

	//cout << "reading from inputbuffer frame: " << frameNumber << endl;
	//TO DO: we need to check if we are at the end of the file cause we are going to read till exception arises 21.11.2017. Leon

	std::unique_lock<std::mutex> lk(mx);

	inputBufferNotEmpty.wait(lk, [this]() {return inputBuffer.size() > 0; });

	frame->payload = inputBuffer.front();
	inputBuffer.pop();
	frame->frameNumber = frameNumber++;
	if (inputBuffer.size() < inputBufSize / 2 || inputBufSize == 1)
		inputBufferNotFull.notify_one();
	lk.unlock();
}

void encoder::IOController::StartPrefetch(int _nFrames)
{
	cout << "thread starting" << endl;
	nFrames = _nFrames;
	fetchThread = std::thread(&IOController::PreFetchFrame, this);

}

void encoder::IOController::closeFiles()
{
	fclose(inputFilePointer);
	ioKernel.closeFiles();
}

int encoder::IOController::writeBufferToFile(EntropyBuffer *buffer)
{
	return ioKernel.writeBufferToFile(buffer);
}

void encoder::IOController::determineInputFileType(string inputFileName)
{
	int delimiter = (int)inputFileName.find_last_of('.');

	string fileExtension = inputFileName.substr(delimiter + 1);

	if (fileExtension == "y4m")
	{
		inputType = Y4M;
	}
	else if (fileExtension == "yuv")
	{
		inputType = YUV;
	}
	else
	{
		cout << "Unknown input format type: " << fileExtension << endl;
		exit(EXIT_FAILURE);
	}
}
