/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CTBController.h"

encoder::CTBController::CTBController()
{
	decCtx = nullptr;
	enCtx = nullptr;
}

encoder::CTBController::~CTBController()
{
}

void encoder::CTBController::Init(EncodingContext* _enCtx)
{
	enCtx = _enCtx;
}

void encoder::CTBController::Init(EncodingContext* _enCtx, EncodingContext* _decCtx)
{
	enCtx = _enCtx;
	decCtx = _decCtx;

	widthCoeff = (float)decCtx->width / enCtx->width;
	heightCoeff = (float)decCtx->height / enCtx->height;
	if (decCtx->width == 1920 && enCtx->width == 1600)
	{
		//widthCoeff = 1.2;
		//heightCoeff = 1.2;

	}
	else if (decCtx->width == 1920 && enCtx->width == 848)
	{
		//widthCoeff = 2.25;
		//heightCoeff = 2.25;
	}

}

void encoder::CTBController::CreateCTUsOnly(Partition* partition)
{
	int i, currRow, currCol, currRowFrame, currColumnFrame;
	int y_pixel_iterator, u_pixel_iterator, v_pixel_iterator;
	int y_pixel_iterator_frame, u_pixel_iterator_frame, v_pixel_iterator_frame;

	int startingIndexU = partition->width * partition->height;
	int startingIndexV = partition->width * partition->height + (partition->width * partition->height / 4);

	int startingIndexUFrame = enCtx->width * enCtx->height;
	int startingIndexVFrame = enCtx->width * enCtx->height + (enCtx->width * enCtx->height / 4);

	partition->ctuTree = new CU * [partition->numberOfCTUs];

	bool isWidthDividible = partition->width % enCtx->ctbSize == 0;
	bool isHeightDividible = partition->height % enCtx->ctbSize == 0;

	int startingTileRow = (partition->startingIndexInFrame / enCtx->width) / enCtx->ctbSize;
	int startingTileColumn = (partition->startingIndexInFrame % enCtx->width) / enCtx->ctbSize;

	/*THis part is for creating ctb tree, with only CTB's */
	for (i = 0; i < partition->numberOfCTUs; i++)
	{
		currRow = i / partition->numberOfCTUsInRow;
		currCol = i % partition->numberOfCTUsInRow;

		currRowFrame = startingTileRow + currRow;
		currColumnFrame = startingTileColumn + currCol;

		y_pixel_iterator = partition->width * currRow * enCtx->ctbSize + currCol * enCtx->ctbSize;
		u_pixel_iterator = startingIndexU + (partition->width / 2) * currRow * (enCtx->ctbSize / 2) + currCol * (enCtx->ctbSize / 2);
		v_pixel_iterator = startingIndexV + (partition->width / 2) * currRow * (enCtx->ctbSize / 2) + currCol * (enCtx->ctbSize / 2);

		y_pixel_iterator_frame = enCtx->width * currRowFrame * enCtx->ctbSize + currColumnFrame * enCtx->ctbSize;
		u_pixel_iterator_frame = startingIndexUFrame + (enCtx->width / 2) * currRowFrame * (enCtx->ctbSize / 2) + currColumnFrame * (enCtx->ctbSize / 2);
		v_pixel_iterator_frame = startingIndexVFrame + (enCtx->width / 2) * currRowFrame * (enCtx->ctbSize / 2) + currColumnFrame * (enCtx->ctbSize / 2);

		CB ctbY = CB(y_pixel_iterator, y_pixel_iterator_frame, enCtx->ctbSize, enCtx->quantizationParameter, Luma);
		CB ctbU = CB(u_pixel_iterator, u_pixel_iterator_frame, enCtx->ctbSize / 2, enCtx->quantizationParameter, ChromaCb);
		CB ctbV = CB(v_pixel_iterator, v_pixel_iterator_frame, enCtx->ctbSize / 2, enCtx->quantizationParameter, ChromaCr);

		//Find starting index in Frame, in case frame is divided in Tiles

		partition->ctuTree[i] = new CU(enCtx->width, enCtx->height, partition->width, partition->height, ctbY, ctbU, ctbV);
		partition->ctuTree[i]->index = i;
		partition->ctuTree[i]->indexInFrame = (int)ceil((float)enCtx->width / enCtx->ctbSize) * currRowFrame + currColumnFrame;
		partition->ctuTree[i]->ctuTree = partition->ctuTree;

		if (i != 0 && (i + 1) % partition->numberOfCTUsInRow == 0)
			partition->ctuTree[i]->isLastInRow = true;

		if (i > (partition->numberOfCTUs - 1) - partition->numberOfCTUsInRow)
			partition->ctuTree[i]->isLastInColumn = true;

		if ((partition->ctuTree[i]->isLastInRow && !isWidthDividible) || (partition->ctuTree[i]->isLastInColumn && !isHeightDividible))
			partition->ctuTree[i]->requiresFurtherSplitting = true;
	}
}

void encoder::CTBController::CreateCTBTree(Partition* partition)
{

	CreateCTUsOnly(partition);

	//Do simple splitting, where each CTU is split once, and CTUs on the edge are split so that they fit in frame
	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		partition->ctuTree[i]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);

		////This is for further splitting of CTU three to 16x16 or 8x8 CTBs
		//partition->ctuTree[i]->children[0]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
		//partition->ctuTree[i]->children[1]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
		//partition->ctuTree[i]->children[2]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
		//partition->ctuTree[i]->children[3]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);

		if (partition->ctuTree[i]->requiresFurtherSplitting)
			SplitToFitFrame(partition->ctuTree[i], partition, enCtx->width, enCtx->height, false);


		/*int k;
		for (k = 0; k < 4; k++)
		{
			partition->ctuTree[i]->children[0]->children[k]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			partition->ctuTree[i]->children[1]->children[k]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			partition->ctuTree[i]->children[2]->children[k]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			partition->ctuTree[i]->children[3]->children[k]->PerformSplit(partition->width, partition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
		}*/
	}
}

void encoder::CTBController::CreateCTBTreeWithDecodedCUMapping(Partition* transcodedPartition, Frame* decodedFrame)
{
	CreateCTUsOnly(transcodedPartition);

	MapCTUStructure(transcodedPartition, decodedFrame);

	for (int i = 0; i < transcodedPartition->numberOfCTUs; i++)
	{
		/*	int startingIndex = transcodedPartition->ctuTree[i]->cbY.startingIndex * enCtx->tr_width;
			int blockSize = ceilf((float)transcodedPartition->ctuTree[i]->cbY.blockSize * enCtx->tr_width);*/

			//int8_t* block_depth = new int8_t[blockSize*blockSize];
			//int16_t* block_mvx = new int16_t[blockSize*blockSize];


			//BlockPartition::get1dInt8BlockByStartingIndex(decodedFrame->depth_payload, block_depth, startingIndex, blockSize, decodedFrame->width, decodedFrame->height);
			//BlockPartition::get1dInt16BlockByStartingIndex(decodedFrame->mv_x_payload, block_mvx, startingIndex, blockSize, decodedFrame->width, decodedFrame->height);
			//if (transcodedPartition->sliceType == SliceType::P)
			//{
			//	ComUtil::printArrayOfInt16(block_mvx, blockSize, blockSize);
			//	getchar();
			//}



			//float AvgDepthSum = 0.0;
			//float AvgMvx = 0.0;
			//float varianceSum = 0.0;
			//float variance = 0.0;

			//for (int i = 0; i < blockSize * blockSize; ++i)
			//{
			//	AvgMvx += block_mvx[i];
			//}
			//AvgMvx = AvgMvx / (blockSize * blockSize);

			//for (int i = 0; i < blockSize * blockSize; ++i)
			//{
			//	varianceSum += (AvgMvx - block_mvx[i]);
			//	
			//}


			//variance = varianceSum / (blockSize * blockSize);

			//transcodedPartition->ctuTree[i]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);

		transcodedPartition->ctuTree[i]->PerformRecursiveSplit(enCtx->minCbLog2SizeY, enCtx->reuseData, decodedFrame->ctuTree[i], widthCoeff, heightCoeff);
		//MapToChildren(transcodedPartition->ctuTree[i]);
		//VarianceMean vmBlock;
		for (int j = 0; j < 4; ++j)
		{
			//BlockPartition::get1dInt16BlockByStartingIndex(decodedFrame->mv_x_payload, block_mvx, startingIndex, blockSize, decodedFrame->width, decodedFrame->height);
			/*vmBlock = GetVarianceMean(block_mvx, transcodedPartition->ctuTree[i]->children[j]->cbY.blockSize, j, PartitionModeInter::PART_2Nx2N);
			if (vmBlock.variance > 2.0)
				transcodedPartition->ctuTree[j]->children[j]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);*/
		}


		/*if (AvgDepthSum > 1.5)
		{
			///This is for further splitting of CTU three to 16x16 or 8x8 CTBs
			transcodedPartition->ctuTree[i]->children[0]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			transcodedPartition->ctuTree[i]->children[1]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			transcodedPartition->ctuTree[i]->children[2]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			transcodedPartition->ctuTree[i]->children[3]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);

			if (AvgDepthSum > 2.5)
			{


				int k;
				for (k = 0; k < 4; k++)
				{
					transcodedPartition->ctuTree[i]->children[0]->children[k]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
					transcodedPartition->ctuTree[i]->children[1]->children[k]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
					transcodedPartition->ctuTree[i]->children[2]->children[k]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
					transcodedPartition->ctuTree[i]->children[3]->children[k]->PerformSplit(transcodedPartition->width, transcodedPartition->height, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
				}
			}

		}*/


		if (transcodedPartition->ctuTree[i]->requiresFurtherSplitting)
			SplitToFitFrame(transcodedPartition->ctuTree[i], transcodedPartition, enCtx->width, enCtx->height, false);

		//delete[] block_depth;
		//delete[] block_mvx;
		//decisionForCUSplit(transcodedPartition->ctuTree[i], transcodedPartition->width, transcodedPartition->height);
		//CopyModeDecisionFromDecodedCU(transcodedPartition->ctuTree[i], decodedFrame->ctuTree[i]);

	}
}

VarianceMean encoder::CTBController::GetVarianceMean(int16_t* block, int blockSize, int offset, PartitionModeInter partMode)
{
	VarianceMean varianceMean;
	int rowOffset = 0;
	int columnOffset = 0;

	int blockEnd;

	float avg = 0.0;
	float variance = 0.0;
	if (partMode == PartitionModeInter::PART_NxN)
	{
		rowOffset = (offset / 2) * (blockSize / 2);
		columnOffset = (offset % 2) * (blockSize / 2);
		blockEnd = blockSize / 2;
	}
	else
	{
		rowOffset = (offset / 2) * blockSize;
		columnOffset = (offset % 2) * blockSize;
		blockEnd = blockSize;
	}

	for (int i = 0; i < blockEnd; ++i)
	{
		for (int j = 0; j < blockEnd; ++j)
		{
			avg += block[(i + rowOffset) * blockSize + j + columnOffset];
		}
	}

	avg = avg / (blockEnd * blockEnd);

	for (int i = 0; i < blockEnd; ++i)
	{
		for (int j = 0; j < blockEnd; ++j)
		{
			variance += (avg - block[(i + rowOffset) * blockSize + j + columnOffset]);
		}
	}

	varianceMean.mean = avg;
	varianceMean.variance = variance;
	return varianceMean;
}

void encoder::CTBController::CreateRelations(Partition* partition)
{
	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		BlockPartition::assignAllNeighbours(partition->ctuTree, partition->ctuTree[i], partition->width);
	}
}

void encoder::CTBController::CopyModeDecisionFromDecodedCU(CU* cu, CU* dec_cu) {
	/*if (dec_cu->isSplit)
	{
		cu->PerformSplit()
		for (int i = 0; i < 4; ++i)
		{
			CopyModeDecisionFromDecodedCU(cu->children[i], dec_cu->children[i]);
		}
	}
	else
	{
		if(dec_cu->)
	}*/

}

//Create only CTUs of transcoded video and map them to CTUs of decoded video
void encoder::CTBController::MapCTUStructure(Partition* transcodedPartition, Frame* decodedFrame)
{
	for (int i = 0; i < transcodedPartition->numberOfCTUs; i++)
	{
		/*	int numberOfOriginalCTUsTOConsiderInRow = decodedFrame->numberOfCTUsInRow;
			int numberOfOriginalCTUsTOConsiderInColumn = decodedFrame->numberOfCTUsInColumn;*/
		int numberOfOriginalCTUsTOConsiderInRow = (int)ceil(widthCoeff) + 1;
		int numberOfOriginalCTUsTOConsiderInColumn = (int)ceil(heightCoeff) + 1;
		int* originalCTUsToConsider = new int[numberOfOriginalCTUsTOConsiderInRow * numberOfOriginalCTUsTOConsiderInColumn];

		int numberOfCTUsinFrameRow = (int)ceil((double)enCtx->width / enCtx->ctbSize);

		float projectedOriginalCTUColumn = (float)(transcodedPartition->ctuTree[i]->indexInFrame % numberOfCTUsinFrameRow) * widthCoeff;
		float projectedOriginalCTURow = (float)(transcodedPartition->ctuTree[i]->indexInFrame / numberOfCTUsinFrameRow) * heightCoeff;

		int CTUiterator = ((int)projectedOriginalCTURow) * decodedFrame->numberOfCTUsInRow + ((int)projectedOriginalCTUColumn);

		//Find all CTUs in original frame that are mapped to transcoded frame
		//This is done to avoid searching entire CTU tree in original frame..
		for (int k = 0; k < numberOfOriginalCTUsTOConsiderInColumn; k++)
		{
			for (int l = 0; l < numberOfOriginalCTUsTOConsiderInRow; l++)
			{
				if (CTUiterator < decodedFrame->numberOfCTUs)
					originalCTUsToConsider[k * numberOfOriginalCTUsTOConsiderInRow + l] = CTUiterator;
				else
					originalCTUsToConsider[k * numberOfOriginalCTUsTOConsiderInRow + l] = -1;

				CTUiterator++;
			}
			CTUiterator = CTUiterator - numberOfOriginalCTUsTOConsiderInRow + decodedFrame->numberOfCTUsInRow;
		}

		int projectedOriginalCTUPixelColumn = (int)((transcodedPartition->ctuTree[i]->cbY.startingIndexInFrame % enCtx->width) * widthCoeff);
		int projectedOriginalCTUPixelRow = (int)((transcodedPartition->ctuTree[i]->cbY.startingIndexInFrame / enCtx->width) * heightCoeff);
		int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(enCtx->ctbSize * widthCoeff);
		int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(enCtx->ctbSize * heightCoeff);

		//Search through all CTUs that are found in previous step and mapped them (or their children to CTUs of transcoded frame
		for (int z = 0; z < numberOfOriginalCTUsTOConsiderInColumn * numberOfOriginalCTUsTOConsiderInRow; z++)
		{
			if (originalCTUsToConsider[z] != -1)
				MapOriginalCUToTranscodedCU(transcodedPartition->ctuTree[i], decodedFrame->ctuTree[originalCTUsToConsider[z]], projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
		}

		//When mapping is done categorize CUs
		//Not needed for CTUS that will be 64x64

		//categorize(transcodedPartition->ctuTree[i]);

		delete[] originalCTUsToConsider;
	}
}

//check if original CTU is between the startIndexInFrame and endIndexInFrame and assign it to transcoded CU
//also calculate weight of mapped CU 
void encoder::CTBController::MapOriginalCUToTranscodedCU(CU* transcodedCU, CU* originalCU, int columnStart, int columnEnd, int rowStart, int rowEnd)
{
	//At this point originalCU should be in frame, but this is just in case
	if (originalCU->isInFrame)
	{
		int originalCUPixelColumn = originalCU->cbY.startingIndexInFrame % decCtx->width;
		int originalCuPixelRow = originalCU->cbY.startingIndexInFrame / decCtx->width;

		//In this case entire CU froom original frame is consisted in transcoded frame
		if (originalCUPixelColumn >= columnStart && originalCuPixelRow >= rowStart &&
			originalCUPixelColumn + originalCU->cbY.blockSize <= columnEnd && originalCuPixelRow + originalCU->cbY.blockSize <= rowEnd)
		{
			//Put all children to CU 
			mapAllAsWhole(transcodedCU, originalCU);
		}
		//Four cases where CU is totaly outside of transcoded CU
		else if (!((originalCUPixelColumn + originalCU->cbY.blockSize <= columnStart) || (originalCUPixelColumn >= columnEnd)
			|| (originalCuPixelRow + originalCU->cbY.blockSize <= rowStart) || (originalCuPixelRow >= rowEnd)))
		{
			if (originalCU->hasChildren)
			{
				for (int i = 0; i < 4; i++)
				{
					if (originalCU->children[i]->isInFrame)
						MapOriginalCUToTranscodedCU(transcodedCU, originalCU->children[i], columnStart, columnEnd, rowStart, rowEnd);
				}
			}
			else
			{
				float coeff = 0.0;
				float widthCuCoeff = 0.0;
				float heightCuCoeff = 0.0;

				if (originalCUPixelColumn >= columnStart && columnEnd - originalCUPixelColumn >= originalCU->cbY.blockSize)
					widthCuCoeff = 1;
				else if (originalCUPixelColumn < columnStart && columnEnd - originalCUPixelColumn >= originalCU->cbY.blockSize)
					widthCuCoeff = (float)(originalCUPixelColumn + originalCU->cbY.blockSize - columnStart) / originalCU->cbY.blockSize;
				else if (columnStart > originalCUPixelColumn && columnEnd < (originalCUPixelColumn + originalCU->cbY.blockSize))
					widthCuCoeff = (float)(columnEnd - columnStart) / originalCU->cbY.blockSize;
				else
					widthCuCoeff = (float)(columnEnd - originalCUPixelColumn) / originalCU->cbY.blockSize;


				if (originalCuPixelRow >= rowStart && rowEnd - originalCuPixelRow >= originalCU->cbY.blockSize)
					heightCuCoeff = 1;
				else if (originalCuPixelRow < rowStart && rowEnd - originalCuPixelRow >= originalCU->cbY.blockSize)
					heightCuCoeff = (float)(originalCuPixelRow + originalCU->cbY.blockSize - rowStart) / originalCU->cbY.blockSize;
				else if (rowStart > originalCuPixelRow && rowEnd < (originalCuPixelRow + originalCU->cbY.blockSize))
					heightCuCoeff = (float)(rowEnd - rowStart) / originalCU->cbY.blockSize;
				else
					heightCuCoeff = (float)(rowEnd - originalCuPixelRow) / originalCU->cbY.blockSize;

				double coeffwidth = (double)originalCU->frameWidth / transcodedCU->frameWidth;


				RatioCU rc;
				rc.PercentageOfWhole = widthCuCoeff * heightCuCoeff;
				rc.RatioInCU = (rc.PercentageOfWhole * originalCU->cbY.blockSize * originalCU->cbY.blockSize) / (transcodedCU->cbY.blockSize * transcodedCU->cbY.blockSize * coeffwidth * coeffwidth);

				transcodedCU->transMappedCUs.emplace_back(make_pair(rc, originalCU));


			}
		}


	}


}

//This function is used for single Mapping, where calculating projected Row and Column is done once
//In case where for one CU we want to find all mapped decoded CUs it is faster to use MapOriginalCUToTranscodedCU(CU*,CU*,int,int,int,int) to avoid calculating projected values every time
void encoder::CTBController::MapOriginalCUToTranscodedCU(CU* transcodedCU, CU* originalCU)
{
	int projectedOriginalCTUPixelColumn = (int)((transcodedCU->cbY.startingIndexInFrame % enCtx->width) * widthCoeff);
	int projectedOriginalCTUPixelRow = (int)((transcodedCU->cbY.startingIndexInFrame / enCtx->width) * heightCoeff);
	int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(enCtx->ctbSize * widthCoeff);
	int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(enCtx->ctbSize * heightCoeff);

	MapOriginalCUToTranscodedCU(transcodedCU, originalCU, projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
}

void encoder::CTBController::MapToChildren(CU* transcodedCU)
{
	//although at this point CU should have children
	if (transcodedCU->hasChildren)
	{
		//remap transcoded CUs to children
		for (int i = 0; i < 4; i++)
		{
			int projectedOriginalCTUPixelColumn = (int)((transcodedCU->children[i]->cbY.startingIndexInFrame % enCtx->width) * widthCoeff);
			int projectedOriginalCTUPixelRow = (int)((transcodedCU->children[i]->cbY.startingIndexInFrame / enCtx->width) * heightCoeff);
			int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(transcodedCU->children[i]->cbY.blockSize * widthCoeff);
			int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(transcodedCU->children[i]->cbY.blockSize * heightCoeff);

			for (int j = 0; j < transcodedCU->transMappedCUs.size(); j++)
			{
				MapOriginalCUToTranscodedCU(transcodedCU->children[i], transcodedCU->transMappedCUs.at(j).second, projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
			}

			//When mapping is done categorize children CUs
			//Categorization::categorize(transcodedCU->children[i], enCtx);
		}

		for (int i = 0; i < 4; ++i)
			MapToChildren(transcodedCU->children[i]);
	}
}

void encoder::CTBController::mapAllAsWhole(CU* transcodedCU, CU* originalCU)
{
	if (originalCU->hasChildren)
	{
		for (int i = 0; i < 4; i++)
			mapAllAsWhole(transcodedCU, originalCU->children[i]);
	}
	else
	{
		if (originalCU->isInFrame)
		{
			double coeffwidth = (double)originalCU->frameWidth / transcodedCU->frameWidth;

			RatioCU rc;
			rc.RatioInCU = (originalCU->cbY.blockSize * originalCU->cbY.blockSize) / (transcodedCU->cbY.blockSize * transcodedCU->cbY.blockSize * coeffwidth * coeffwidth);
			rc.PercentageOfWhole = 1.0;
			transcodedCU->transMappedCUs.emplace_back(std::make_pair(rc, originalCU));
		}
	}
}

void encoder::CTBController::decisionForCUSplit(CU* cu, int partitionWidth, int partitionHeight)
{
	//This is for CUs that are at the edge of the frame and has to be split to fit in frame, there is no decision algorithm for this case, we HAVE to split it 
	if (cu->requiresFurtherSplitting)
	{
		cu->PerformSplit(partitionWidth, partitionHeight, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
		MapToChildren(cu);
	}
	else
	{
		//decision is made for LCU...FOr now, always split CTU
		if (cu->cbY.blockSize == enCtx->ctbSize)
		{
			cu->PerformSplit(partitionWidth, partitionHeight, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
			MapToChildren(cu);
		}
		//This is decision for other CUs that are not LCU
		else
		{
			int treshold = 25;

			if (cu->transMappedCUs.size() > treshold)
			{
				cu->PerformSplit(partitionWidth, partitionHeight, enCtx->width, enCtx->height, enCtx->minCbLog2SizeY);
				MapToChildren(cu);
			}
		}
	}

	//If we have decided to split the CU than go for decision about its children
	if (cu->isSplit)
	{
		for (int i = 0; i < 4; i++)
			decisionForCUSplit(cu->children[i], partitionWidth, partitionHeight);
	}
}

void encoder::CTBController::SplitToFitFrame(CU* cu, Partition* partition, int frameWidth, int frameHeight, bool remapTransCU)
{
	int x = 0;

	if (cu->requiresFurtherSplitting)
	{
		if (!cu->hasChildren)
		{
			cu->PerformSplit(partition->width, partition->height, frameWidth, frameHeight);
			if (remapTransCU)
				MapToChildren(cu);

			int j;
			for (j = 0; j < 4; j++)
			{
				if (cu->children[j]->requiresFurtherSplitting)
					SplitToFitFrame(cu->children[j], partition, frameWidth, frameHeight, remapTransCU);
			}
		}
		else
		{
			int i;
			for (i = 0; i < 4; i++)
			{
				SplitToFitFrame(cu->children[i], partition, frameWidth, frameHeight, remapTransCU);
			}
		}
	}



}
