/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "EntropyController.h"

encoder::EntropyController::EntropyController()
{
	sMap = SyntaxMap::InitializeSyntaxMap();
}

encoder::EntropyController::~EntropyController()
{
}

void encoder::EntropyController::parseNALHeader(NALType nalType)
{
	encodeElement(forbidden_zero_bit, 0);
	encodeElement(nal_unit_type, nalType);
	encodeElement(nuh_layer_id, 0);
	encodeElement(nuh_temporal_id_plus1, 1);
}

void encoder::EntropyController::parseNonVCL(NALType nalType)
{
	writeStartCode();

	parseNALHeader(nalType);

	if (nalType == VPS_NUT)
		parseVPS();
	else if (nalType == SPS_NUT)
		parseSPS();
	else if (nalType == PPS_NUT)
		parsePPS();

}

void encoder::EntropyController::parseVCL(NALType nalType, Frame * frame)
{
	reset();
	writeStartCode();

	parseNALHeader(nalType);
	parseSliceSegment(nalType, frame);
}

void encoder::EntropyController::parseVPS()
{
	encodeElement(vps_video_parameter_set_id, enCtx->currentVPSId);
	encodeElement(vps_base_layer_internal_flag, 1);
	encodeElement(vps_base_layer_available_flag, 1);
	encodeElement(vps_max_layers_minus1, 0);
	encodeElement(vps_max_sub_layers_minus1, enCtx->highestTid);
	encodeElement(vps_temporal_id_nesting_flag, 1);
	encodeElement(vps_reserved_0xffff_16bits, 0xFFFF);

	parseProfileTierLevel();

	encodeElement(vps_sub_layer_ordering_info_present_flag, 0);

	//for (int i = (vps->vps_sub_layer_ordering_info_present_flag != 0 ? 0 : vps->vps_max_sub_layers_minus1); i <= vps->vps_max_sub_layers_minus1; i++)
	int VpsSubLayerOrderingInfoPresentFlag = 0;
	for (int i = (VpsSubLayerOrderingInfoPresentFlag != 0 ? 0 : enCtx->highestTid); i <= enCtx->highestTid; i++)
	{
		encodeElement(vps_max_dec_pic_buffering_minus1, 1);
		encodeElement(vps_max_num_reorder_pics, 0);
		encodeElement(vps_max_latency_increase_plus1, 0);
	}

	int VpsNumLayerSetsMinus1 = 0;
	int VpsMaxLayerId = 0;

	encodeElement(vps_max_layer_id, 0);
	encodeElement(vps_num_layer_sets_minus1, VpsNumLayerSetsMinus1);

	for (int i = 1; i <= VpsNumLayerSetsMinus1; i++)
	{
		for (int j = 0; j <= VpsMaxLayerId; j++)
		{
			encodeElement(layer_id_included_flag, 0);
		}
	}

	int VpsTimingInfoPresentFlag = 1;
	encodeElement(vps_timing_info_present_flag, VpsTimingInfoPresentFlag);

	if (VpsTimingInfoPresentFlag != 0)
	{
		encodeElement(vps_num_units_in_tick, 1);
		encodeElement(vps_time_scale, enCtx->frameRate);
		encodeElement(vps_poc_proportional_to_timing_flag, 0);

		int VpsPocProportionalToTimingFlag = 0;
		if (VpsPocProportionalToTimingFlag != 0)
		{
			encodeElement(vps_num_ticks_poc_diff_one_minus1, 0);
		}

		int VpsNumHrdParameters = 0;
		encodeElement(vps_num_hrd_parameters, VpsNumHrdParameters);

		for (int i = 0; i < VpsNumHrdParameters; i++)
		{
			encodeElement(hrd_layer_set_idx, 0);

			if (i > 0)
				encodeElement(cprms_present_flag, 0);

		}
	}

	encodeElement(vps_extension_flag, 0);

	// RBSP trailing bits
	writeTrailingBits();
}

void encoder::EntropyController::parseSPS()
{
	encodeElement(sps_video_parameter_set_id, enCtx->currentVPSId);
	encodeElement(sps_max_sub_layers_minus1, 0);
	encodeElement(sps_temporal_id_nesting_flag, 1);

	parseProfileTierLevel();

	int ChromaFormatIdc = 1;
	int ConformanceWindowFlag = 0;
	encodeElement(sps_seq_parameter_set_id, enCtx->currentSPSId);
	encodeElement(chroma_format_idc, ChromaFormatIdc);

	if (ChromaFormatIdc == 3)
		encodeElement(separate_colour_plane_flag, 0);

	encodeElement(pic_width_in_luma_samples, enCtx->width);
	encodeElement(pic_height_in_luma_samples, enCtx->height);
	encodeElement(conformance_window_flag, ConformanceWindowFlag);

	if (ConformanceWindowFlag)
	{
		encodeElement(conf_win_left_offset, 0);
		encodeElement(conf_win_right_offset, 0);
		encodeElement(conf_win_top_offset, 0);
		encodeElement(conf_win_bottom_offset, 0);
	}

	encodeElement(bit_depth_luma_minus8, enCtx->bitNumber - 8);
	encodeElement(bit_depth_chroma_minus8, enCtx->bitNumber - 8);
	encodeElement(log2_max_pic_order_cnt_lsb_minus4, enCtx->log2MaxPicOrderLst - 4);
	encodeElement(sps_sub_layer_ordering_info_present_flag, 0);

	encodeElement(sps_max_dec_pic_buffering_minus1, 1);
	encodeElement(sps_max_num_reorder_pics, 0);
	encodeElement(sps_max_latency_increase_plus1, 0);

	encodeElement(log2_min_luma_coding_block_size_minus3, enCtx->minCbLog2SizeY - 3);
	encodeElement(log2_diff_max_min_luma_coding_block_size, enCtx->ctbLog2SizeY - enCtx->minCbLog2SizeY);
	encodeElement(log2_min_luma_transform_block_size_minus2, enCtx->minTbLog2SizeY - 2);
	encodeElement(log2_diff_max_min_luma_transform_block_size, enCtx->maxTbLog2SizeY - enCtx->minTbLog2SizeY);
	encodeElement(max_transform_hierarchy_depth_inter, 0);
	encodeElement(max_transform_hierarchy_depth_intra, 0);
	encodeElement(scaling_list_enabled_flag, 0);
	encodeElement(amp_enabled_flag, enCtx->useAMP);
	encodeElement(sample_adaptive_offset_enabled_flag, enCtx->saoEnabled);
	encodeElement(pcm_enabled_flag, 0);

	int NumShortTermRefPicSets = 1;
	encodeElement(num_short_term_ref_pic_sets, NumShortTermRefPicSets);

	int i;
	for (i = 0; i < NumShortTermRefPicSets; i++)
	{
		parseStRefPicSet();
	}

	encodeElement(long_term_ref_pics_present_flag, 0);

	encodeElement(sps_temporal_mvp_enabled_flag, enCtx->useTemporalAMVP);
	encodeElement(strong_intra_smoothing_enabled_flag, 0);
	encodeElement(vui_parameters_present_flag, 0);

	encodeElement(sps_extension_present_flag, 0);

	ctxMem.sps_temporal_mvp_enabled_flag = enCtx->useTemporalAMVP;

	// RBSP trailing bits
	writeTrailingBits();
}

void encoder::EntropyController::parsePPS()
{
	encodeElement(pps_pic_parameter_set_id, enCtx->currentPPSId);
	encodeElement(pps_seq_parameter_set_id, enCtx->currentSPSId);
	encodeElement(dependent_slice_segments_enabled_flag, 0);
	encodeElement(output_flag_present_flag, 0);
	encodeElement(num_extra_slice_header_bits, 0);
	encodeElement(sign_data_hiding_enabled_flag, 0);
	encodeElement(cabac_init_present_flag, 0);
	encodeElement(num_ref_idx_l0_default_active_minus1, 0);
	encodeElement(num_ref_idx_l1_default_active_minus1, 0);
	encodeElement(init_qp_minus26, enCtx->quantizationParameter - 26);
	encodeElement(constrained_intra_pred_flag, 0);
	encodeElement(transform_skip_enabled_flag, 0);

	int CuQpDeltaEnabledFlag = 0;
	encodeElement(cu_qp_delta_enabled_flag, CuQpDeltaEnabledFlag);

	if (CuQpDeltaEnabledFlag != 0)
		encodeElement(diff_cu_qp_delta_depth, 0);

	int TilesEnabledFlag = enCtx->tilesEnabled;
	encodeElement(pps_cb_qp_offset, 0);
	encodeElement(pps_cr_qp_offset, 0);
	encodeElement(pps_slice_chroma_qp_offsets_present_flag, 0);
	encodeElement(weighted_pred_flag, 0);
	encodeElement(weighted_bipred_flag, 0);
	encodeElement(transquant_bypass_enabled_flag, 0);
	encodeElement(tiles_enabled_flag, TilesEnabledFlag);
	encodeElement(entropy_coding_sync_enabled_flag, 0);

	if (TilesEnabledFlag != 0)
	{
		int NumTilesColumnsMinus1 = enCtx->numberOfTilesInColumn - 1;
		int NumTileRowsMinus1 = enCtx->numberOfTilesInRow - 1;

		encodeElement(num_tile_columns_minus1, NumTilesColumnsMinus1);
		encodeElement(num_tile_rows_minus1, NumTileRowsMinus1);

		int UniformSpacingFlag = enCtx->isUniform;
		encodeElement(uniform_spacing_flag, UniformSpacingFlag);

		if (UniformSpacingFlag == 0)
		{
			for (int i = 0; i < NumTilesColumnsMinus1; i++)
				encodeElement(column_width_minus1, enCtx->columnWidths[i] - 1);
			for (int i = 0; i < NumTileRowsMinus1; i++)
				encodeElement(row_height_minus1, enCtx->rowHeights[i] - 1);
		}

		encodeElement(loop_filter_across_tiles_enabled_flag, 0);
	}

	encodeElement(pps_loop_filter_across_slices_enabled_flag, 0);

	int DeblockingFilterControlPresentFlag = 1;
	encodeElement(deblocking_filter_control_present_flag, DeblockingFilterControlPresentFlag);

	if (DeblockingFilterControlPresentFlag != 0)
	{
		encodeElement(deblocking_filter_override_enabled_flag, 0);
		encodeElement(pps_deblocking_filter_disabled_flag, enCtx->disableDeblockingFilter);

		if (enCtx->disableDeblockingFilter == 0)
		{
			encodeElement(pps_beta_offset_div2, 0);
			encodeElement(pps_tc_offset_div2, 0);
		}
	}

	int PpsScalingListDataPresentFlag = 0;
	encodeElement(pps_scaling_list_data_present_flag, PpsScalingListDataPresentFlag);

	if (PpsScalingListDataPresentFlag != 0)  // TODO: Scaling list data
	{
	}

	encodeElement(lists_modification_present_flag, 0);
	encodeElement(log2_parallel_merge_level_minus2, 0);
	encodeElement(slice_segment_header_extension_present_flag, 0);
	encodeElement(pps_extension_present_flag, 0);

	// RBSP trailing bits
	writeTrailingBits();
}

void encoder::EntropyController::parseProfileTierLevel()
{
	int GeneralProfileIdc = 0;
	int GeneralProfileCompatibilityFlag[32]{ 0 };
	encodeElement(general_profile_space, 0);
	encodeElement(general_tier_flag, 0);
	encodeElement(general_profile_idc, GeneralProfileIdc);

	for (int j = 0; j < 32; j++)
	{
		if (j == GeneralProfileIdc)
			GeneralProfileCompatibilityFlag[j] = 1;
		else
			GeneralProfileCompatibilityFlag[j] = 0;

		encodeElement(general_profile_compatibility_flag, GeneralProfileCompatibilityFlag[j]);
	}

	encodeElement(general_progressive_source_flag, 1);
	encodeElement(general_interlaced_source_flag, 0);
	encodeElement(general_non_packed_constraint_flag, 1);
	encodeElement(general_frame_only_constraint_flag, 1);

	if ((GeneralProfileIdc >= 4 && GeneralProfileIdc <= 7) ||
		GeneralProfileCompatibilityFlag[4] != 0 ||
		GeneralProfileCompatibilityFlag[5] != 0 ||
		GeneralProfileCompatibilityFlag[6] != 0 ||
		GeneralProfileCompatibilityFlag[7] != 0)
	{
		encodeElement(general_max_12bit_constraint_flag, 0);
		encodeElement(general_max_10bit_constraint_flag, 0);
		encodeElement(general_max_8bit_constraint_flag, 0);
		encodeElement(general_max_422chroma_constraint_flag, 0);
		encodeElement(general_max_420chroma_constraint_flag, 0);
		encodeElement(general_max_monochrome_constraint_flag, 0);
		encodeElement(general_intra_constraint_flag, 0);
		encodeElement(general_one_picture_only_constraint_flag, 0);
		encodeElement(general_lower_bit_rate_constraint_flag, 0);
		encodeElement(general_reserved_zero_34bits, 0);
	}
	else
	{
		encodeElement(general_reserved_zero_43bits, 0);
		if ((GeneralProfileIdc >= 1 && GeneralProfileIdc <= 5) ||
			GeneralProfileCompatibilityFlag[1] != 0 ||
			GeneralProfileCompatibilityFlag[2] != 0 ||
			GeneralProfileCompatibilityFlag[3] != 0 ||
			GeneralProfileCompatibilityFlag[4] != 0 ||
			GeneralProfileCompatibilityFlag[5] != 0)
			encodeElement(general_inbld_flag, 0);
		else
			encodeElement(general_reserved_zero_bit, 0);
	}

	encodeElement(general_level_idc, 123);
}

void encoder::EntropyController::parseStRefPicSet()
{
	int NumNegativePics = 1;
	int NumPositivePics = 0;
	encodeElement(num_negative_pics, NumNegativePics);
	encodeElement(num_positive_pics, NumPositivePics);

	int i;
	for (i = 0; i < NumNegativePics; i++)
	{
		encodeElement(delta_poc_s0_minus1, 0);
		encodeElement(used_by_curr_pic_s0_flag, 1);
	}

	for (i = 0; i < NumPositivePics; i++)
	{
		encodeElement(delta_poc_s1_minus1, 0);
		encodeElement(used_by_curr_pic_s1_flag, 1);
	}
}

void encoder::EntropyController::parseSliceSegment(NALType nalType, Frame * frame)
{
	parseSliceHeader(nalType, frame);

}

void encoder::EntropyController::parseSliceHeader(NALType nalType, Frame * frame)
{
	encodeElement(first_slice_segment_in_pic_flag, 1);

	if (nalType >= BLA_W__LP && nalType <= RSV_IRAP_VCL22)
		encodeElement(no_output_of_prior_pics_flag, 0);

	encodeElement(slice_pic_parameter_set_id, enCtx->currentPPSId);

	int DependentSliceSegmentFlag = 0;

	if (DependentSliceSegmentFlag == 0)
	{
		ctxMem.sliceType = frame->sliceType;
		encodeElement(slice_type, frame->sliceType);

		if (nalType != IDR_W_RADL && nalType != IDR_N_LP)
		{
			encodeElement(slice_pic_order_cnt_lsb, frame->poc % (1 << (enCtx->log2MaxPicOrderLst)));
			encodeElement(short_term_ref_pic_set_sps_flag, 1);

			if (ctxMem.sps_temporal_mvp_enabled_flag == 1)
				encodeElement(slice_temporal_mvp_enabled_flag, enCtx->useTemporalAMVP);
		}

		if (frame->sliceType == P || frame->sliceType == B)
		{
			encodeElement(num_ref_idx_active_override_flag, 0);
			encodeElement(five_minus_max_num_merge_cand, 0);
		}
	}
	encodeElement(slice_qp_delta, enCtx->quantizationParameter - enCtx->initQP);

	if (enCtx->tilesEnabled)
	{
		int numOfTiles = enCtx->numberOfTilesInColumn*enCtx->numberOfTilesInRow;

		if (numOfTiles > 1)
		{
			encodeElement(num_entry_point_offsets, numOfTiles - 1);

			int max = 0;
			for (int i = 0; i < numOfTiles; i++)
			{
				if (enCtx->tileOffsets[i] > max)
					max = enCtx->tileOffsets[i];
			}
			int numberOfBits = (int)ceil(log2(max));
			encodeElement(offset_len_minus1, numberOfBits - 1);

			for (int i = 0; i < numOfTiles - 1; i++)
				encodeElement(entry_point_offset_minus1, enCtx->tileOffsets[i + 1] - 1);
		}
	}

	writeTrailingBits();
}

void encoder::EntropyController::parseSliceData(Partition * partition)
{
	ctxMem.amp_enabled_flag = enCtx->useAMP;
	//Param_Number_of_bits = 0;
	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		parseCTU(partition->ctuTree[i], partition);

		bool isLastInFrame = partition->isLastPartitionInFrame && i == (partition->numberOfCTUs - 1);
		encodeElement(end_of_slice_segment_flag, isLastInFrame);

		if (partition->partitionType == TILE && !isLastInFrame && i == partition->numberOfCTUs - 1)
			encodeElement(end_of_subset_one_bit, 1);
	}
}

void encoder::EntropyController::parseCTU(CU * cu, Partition* partition)
{
	ctxMem.availableL = cu->isLeftCodingUnitAvailable(partition->width) ? 1 : 0;
	ctxMem.availableA = cu->isAboveCodingUnitAvailable(partition->width) ? 1 : 0;

	//TO DO
	ctxMem.ctDepthL = cu->getLeftCodingUnit()->depth;
	ctxMem.ctDepthA = cu->getAboveCodingUnit()->depth;
	ctxMem.cqtDepth = cu->depth;

	ctxMem.cbSize = cu->cbY.blockSize;
	ctxMem.nPbW = cu->cbY.blockSize;
	ctxMem.nPbH = cu->cbY.blockSize;

	if (!cu->requiresFurtherSplitting && cu->cbY.blockSize > 8)
		encodeElement(split_cu_flag, cu->isSplit);

	if (cu->isSplit != 0)
	{
		if (cu->children[0]->isInFrame)
			parseCTU(cu->children[0], partition);

		if (cu->children[1]->isInFrame)
			parseCTU(cu->children[1], partition);

		if (cu->children[2]->isInFrame)
			parseCTU(cu->children[2], partition);

		if (cu->children[3]->isInFrame)
			parseCTU(cu->children[3], partition);
	}
	else
	{
		parseCU(cu, partition);
	}
}

void encoder::EntropyController::parseCU(CU * cu, Partition *partition)
{
	ctxMem.left_cu_skip_flag = cu->isLeftCodingUnitAvailable(partition->width) ? cu->getLeftCodingUnit()->CuSkipFlag : 0;
	ctxMem.above_cu_skip_flag = cu->isAboveCodingUnitAvailable(partition->width) ? cu->getAboveCodingUnit()->CuSkipFlag : 0;

	if (ctxMem.sliceType != I)
		encodeElement(cu_skip_flag, cu->CuSkipFlag);

	ctxMem.cu_skip_flag = cu->CuSkipFlag;

	if (cu->CuSkipFlag == 1)
	{
		parsePU(&cu->predictionUnits[0], partition);
	}
	else
	{
		ctxMem.pred_mode_flag = cu->PredModeFlag;

		if (ctxMem.sliceType != I)
			encodeElement(pred_mode_flag, cu->PredModeFlag);

		PredMode CuPredMode = cu->PredModeFlag == 0 ? MODE_INTER : MODE_INTRA;


		if (CuPredMode != MODE_INTRA || ctxMem.cbSize == 8)
		{
			ctxMem.amp_enabled_flag = enCtx->useAMP;
			encodeElement(part_mode, cu->PartMode);
		}


		if (CuPredMode == MODE_INTRA) // LDOC TO DO
		{
			if (cu->PartMode == PART_2Nx2N)
			{
				encodeElement(prev_intra_luma_pred_flag, cu->PrevIntraLumaPredFlag[0]);

				if (cu->PrevIntraLumaPredFlag[0] == true)
					encodeElement(mpm_idx, cu->MpmIdx[0]);
				else
					encodeElement(rem_intra_luma_pred_mode, cu->RemIntraLumaPredMode[0]);
			}
			else
			{
				for (int i = 0; i < 4; ++i)
				{
					encodeElement(prev_intra_luma_pred_flag, cu->PrevIntraLumaPredFlag[i]);
				}
				for (int i = 0; i < 4; ++i)
				{
					if (cu->PrevIntraLumaPredFlag[i] == true)
						encodeElement(mpm_idx, cu->MpmIdx[i]);
					else
						encodeElement(rem_intra_luma_pred_mode, cu->RemIntraLumaPredMode[i]);
				}
			}

			encodeElement(intra_chroma_pred_mode, cu->IntraChromaPredMode);
		}
		else
		{
			if (cu->PartMode == PartitionModeInter::PART_2Nx2N)
			{
				parsePU(&cu->predictionUnits[0], partition);
			}
			else if (cu->PartMode == PartitionModeInter::PART_2NxN)
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
			}
			else if (cu->PartMode == PartitionModeInter::PART_Nx2N)
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
			}
			else if (cu->PartMode == PartitionModeInter::PART_2NxnU)
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
			}
			else if (cu->PartMode == PartitionModeInter::PART_2NxnD)
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
			}
			else if (cu->PartMode == PartitionModeInter::PART_nLx2N)
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
			}
			else if (cu->PartMode == PartitionModeInter::PART_nRx2N)
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
			}
			else // PART NxN
			{
				parsePU(&cu->predictionUnits[0], partition);
				parsePU(&cu->predictionUnits[1], partition);
				parsePU(&cu->predictionUnits[2], partition);
				parsePU(&cu->predictionUnits[3], partition);
			}
		}

		if (CuPredMode != MODE_INTRA)
			ctxMem.merge_flag = cu->predictionUnits[0].predictionMode == Merge || cu->predictionUnits[0].predictionMode == Skip ? 1 : 0;

		int RqtRootCbf = (cu->transformTree->tbY.isZeroMatrix && cu->transformTree->tbU.isZeroMatrix && cu->transformTree->tbV.isZeroMatrix) && !cu->transformTree->hasChildren && CuPredMode == MODE_INTER ? 0 : 1;
		if (CuPredMode != MODE_INTRA && !(cu->PartMode == PartitionModeInter::PART_2Nx2N && ctxMem.merge_flag == 1))
			encodeElement(rqt_root_cbf, RqtRootCbf);

		ctxMem.rqt_root_cbf = RqtRootCbf;
		ctxMem.CuPredMode = CuPredMode;

		if (RqtRootCbf == 1)
			parseTransformTree(cu, cu->transformTree);

	}
}

void encoder::EntropyController::parsePU(PU * pu, Partition *partition)
{
	if (ctxMem.cu_skip_flag == 1)
	{
		// TODO: Check if maximal number of merge candidates is greater than 1
		encodeElement(merge_idx, pu->candidateIndex);
	}
	else
	{
		int MergeFlag = pu->predictionMode == Merge || pu->predictionMode == Skip ? 1 : 0;
		encodeElement(merge_flag, MergeFlag);

		if (MergeFlag == 1)
		{
			// TODO: Check if maximal number of merge candidates is greater than 1
			encodeElement(merge_idx, pu->candidateIndex);
		}
		else
		{
			if (ctxMem.num_ref_idx_l0_active_minus1 > 0)
				encodeElement(ref_idx_l0, 0);

			parseMVDCoding(pu);
			encodeElement(mvp_l0_flag, pu->candidateIndex);
		}
	}
}

void encoder::EntropyController::parseMVDCoding(PU * pu)
{
	int absX = abs(pu->MDabsx1);
	int absY = abs(pu->MDabsy1);

	int AbsMvdGreater0Flag0 = absX > 0 ? 1 : 0;
	int AbsMvdGreater0Flag1 = absY > 0 ? 1 : 0;
	int AbsMvdGreater1Flag0 = absX > 1 ? 1 : 0;
	int AbsMvdGreater1Flag1 = absY > 1 ? 1 : 0;

	encodeElement(abs_mvd_greater0_flag, AbsMvdGreater0Flag0);
	encodeElement(abs_mvd_greater0_flag, AbsMvdGreater0Flag1);

	if (AbsMvdGreater0Flag0 == 1)
		encodeElement(abs_mvd_greater1_flag, AbsMvdGreater1Flag0);

	if (AbsMvdGreater0Flag1 == 1)
		encodeElement(abs_mvd_greater1_flag, AbsMvdGreater1Flag1);

	if (AbsMvdGreater0Flag0 == 1)
	{
		if (AbsMvdGreater1Flag0 == 1)
		{
			encodeElement(abs_mvd_minus2, absX - 2);
		}
		encodeElement(mvd_sign_flag, pu->MDsignx1 == -1 ? 1 : 0);
	}

	if (AbsMvdGreater0Flag1 == 1)
	{
		if (AbsMvdGreater1Flag1 == 1)
		{
			encodeElement(abs_mvd_minus2, absY - 2);
		}
		encodeElement(mvd_sign_flag, pu->MDsigny1 == -1 ? 1 : 0);
	}
}

void encoder::EntropyController::parseTransformTree(CU *cu, TU * tu)
{

	int ChromaArrayType = 1;
	if ((tu->tbY.transformBlockSize > 4 && ChromaArrayType != 0) || ChromaArrayType == 3)
	{
		if (tu->trafoDepth == 0 || (tu->parent && !tu->parent->tbU.isZeroMatrix))
		{
			ctxMem.trafoDepth = tu->trafoDepth;
			encodeElement(cbf_cb, !tu->tbU.isZeroMatrix);

			if (ChromaArrayType == 2 && (!split_transform_flag || tu->tbY.transformBlockSize == 8))
			{
				// kad budemo imali chroma array type 2 imat �emo i ovo.
			}
		}

		if (tu->trafoDepth == 0 || (tu->parent && !tu->parent->tbV.isZeroMatrix))
		{
			ctxMem.trafoDepth = tu->trafoDepth;
			encodeElement(cbf_cr, !tu->tbV.isZeroMatrix);

			if (ChromaArrayType == 2 && (!split_transform_flag || tu->tbY.transformBlockSize == 8))
			{
				// kad budemo imali chroma array type 2 imat �emo i ovo.
			}
		}
	}

	if (tu->hasChildren)
	{
		for (int i = 0; i < 4; ++i)
			parseTransformTree(cu, tu->children[i]);

		if (tu->tbY.transformBlockSize == 8)
		{
			if (!tu->tbU.isZeroMatrix)
			{
				ctxMem.predMode = cu->IntraPredModeChroma;
				ctxMem.colorIdx = 1;
				ctxMem.log2TrafoSize = ComUtil::logarithm2(tu->tbU.transformBlockSize);
				tu->tbU.scanType = Scanner::GetScanType(&tu->tbU, cu->CuPredMode);
				ctxMem.scanIdx = tu->tbU.scanType;

				parseResidual(cu, &tu->tbU);
			}

			if (!tu->tbV.isZeroMatrix)
			{
				ctxMem.predMode = cu->IntraPredModeChroma;
				ctxMem.colorIdx = 2;
				ctxMem.log2TrafoSize = ComUtil::logarithm2(tu->tbV.transformBlockSize);
				tu->tbV.scanType = Scanner::GetScanType(&tu->tbV, cu->CuPredMode);
				ctxMem.scanIdx = tu->tbV.scanType;

				parseResidual(cu, &tu->tbV);
			}
		}
	}
	else
	{
		if (ctxMem.CuPredMode == MODE_INTRA || !tu->tbU.isZeroMatrix || !tu->tbV.isZeroMatrix || tu->trafoDepth != 0)
		{
			ctxMem.trafoDepth = tu->trafoDepth;
			encodeElement(cbf_luma, !tu->tbY.isZeroMatrix);
		}



		ctxMem.cbf_luma = !tu->tbY.isZeroMatrix;
		ctxMem.cbf_cb = !tu->tbU.isZeroMatrix;
		ctxMem.cbf_cr = !tu->tbV.isZeroMatrix;

		parseTU(cu, tu);
	}
}

void encoder::EntropyController::parseTU(CU *cu, TU * tu)
{
	int lumaLog2trafoSize = ComUtil::logarithm2(tu->tbY.transformBlockSize);

	if (ctxMem.cbf_luma == 1)
	{
		ctxMem.colorIdx = 0;
		ctxMem.log2TrafoSize = lumaLog2trafoSize;
		tu->tbY.scanType = Scanner::GetScanType(&tu->tbY, cu->CuPredMode);
		ctxMem.scanIdx = tu->tbY.scanType;
		parseResidual(cu, &tu->tbY);
	}

	if (ctxMem.cbf_cb == 1)
	{
		ctxMem.colorIdx = 1;
		ctxMem.log2TrafoSize = lumaLog2trafoSize - 1;
		tu->tbU.scanType = Scanner::GetScanType(&tu->tbU, cu->CuPredMode);
		ctxMem.scanIdx = tu->tbU.scanType;
		parseResidual(cu, &tu->tbU);
	}

	if (ctxMem.cbf_cr == 1)
	{
		ctxMem.colorIdx = 2;
		ctxMem.log2TrafoSize = lumaLog2trafoSize - 1;
		tu->tbV.scanType = Scanner::GetScanType(&tu->tbV, cu->CuPredMode);
		ctxMem.scanIdx = tu->tbV.scanType;
		parseResidual(cu, &tu->tbV);
	}
}

void encoder::EntropyController::parseResidual(CU * cu, TB * tb)
{
	int lastSigCoeffXPrefix, lastSigCoeffXSuffix, lastSigCoeffYPrefix, lastSigCoeffYSuffix;

	Scanner::LastSigCoeffBin(tb->lastSigCoeffX, lastSigCoeffXPrefix, lastSigCoeffXSuffix);
	Scanner::LastSigCoeffBin(tb->lastSigCoeffY, lastSigCoeffYPrefix, lastSigCoeffYSuffix);

	encodeElement(last_sig_coeff_x_prefix, lastSigCoeffXPrefix);
	encodeElement(last_sig_coeff_y_prefix, lastSigCoeffYPrefix);

	if (lastSigCoeffXPrefix > 3)
		encodeElement(last_sig_coeff_x_suffix, lastSigCoeffXSuffix);

	if (lastSigCoeffYPrefix > 3)
		encodeElement(last_sig_coeff_y_suffix, lastSigCoeffYSuffix);

	int sizeOfSubblockMatrix = tb->transformBlockSize / 4;
	int numberOfSubblocksInTU = sizeOfSubblockMatrix * sizeOfSubblockMatrix;

	ctxMem.maxScanIdx = numberOfSubblocksInTU;

	bool flag = true;
	bool firstOne = false;

	ctxMem.first_time_in_TB = 1;

	for (int i = tb->indexOfSigSubblock; i < numberOfSubblocksInTU; i++)
	{
		ctxMem.sub_block_index = i;

		if (sizeOfSubblockMatrix == 8)
			Scanner::GetCoordinatesInBlock8x8(i, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, tb->scanType);
		else if (sizeOfSubblockMatrix == 4)
			Scanner::GetCoordinatesInBlock4x4(i, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, tb->scanType);
		else if (sizeOfSubblockMatrix == 2)
			Scanner::GetCoordinatesInBlock2x2(i, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, tb->scanType);
		else
		{
			ctxMem.subBlockCoordinateX = 0;
			ctxMem.subBlockCoordinateY = 0;
		}

		int iR;
		int iB;

		if (sizeOfSubblockMatrix == 8)
		{
			Scanner::GetScanIndexFromCoordinates8x8(iR, ctxMem.subBlockCoordinateX + 1, ctxMem.subBlockCoordinateY, tb->scanType);
			Scanner::GetScanIndexFromCoordinates8x8(iB, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY + 1, tb->scanType);
		}
		else if (sizeOfSubblockMatrix == 4)
		{
			Scanner::GetScanIndexFromCoordinates4x4(iR, ctxMem.subBlockCoordinateX + 1, ctxMem.subBlockCoordinateY, tb->scanType);
			Scanner::GetScanIndexFromCoordinates4x4(iB, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY + 1, tb->scanType);
		}
		else if (sizeOfSubblockMatrix == 2)
		{
			Scanner::GetScanIndexFromCoordinates2x2(iR, ctxMem.subBlockCoordinateX + 1, ctxMem.subBlockCoordinateY, tb->scanType);
			Scanner::GetScanIndexFromCoordinates2x2(iB, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY + 1, tb->scanType);
		}
		else
		{
			iR = 0;
			iB = 0;
		}

		if ((ctxMem.subBlockCoordinateX == sizeOfSubblockMatrix) || (ctxMem.subBlockCoordinateY == sizeOfSubblockMatrix))
			ctxMem.isBorderSubBlock = true;
		else
		{
			ctxMem.isBorderSubBlock = false;

			if (iR != -1)
				ctxMem.subBlockRightCSBF = tb->subBlocks[iR].codedSubBlockFlag == 1;

			if (iB != -1)
				ctxMem.subBlockBottomCSBF = tb->subBlocks[iB].codedSubBlockFlag == 1;
		}

		if (i > tb->indexOfSigSubblock && i < numberOfSubblocksInTU - 1)
		{
			encodeElement(coded_sub_block_flag, tb->subBlocks[i].codedSubBlockFlag);
		}
		int nullCounter = 0;
		if (tb->subBlocks[i].codedSubBlockFlag != 0 || (i == numberOfSubblocksInTU - 1 && tb->indexOfSigSubblock != numberOfSubblocksInTU - 1))
			for (int j = 0; j < 16; j++)
			{
				if (tb->subBlocks[i].SigCoeffFlag[j] == 1)
					firstOne = true;

				if (firstOne)
				{
					int pom_sub_x = 0;
					int pom_sub_y = 0;
					Scanner::GetCoordinatesInBlock4x4(j, pom_sub_x, pom_sub_y, tb->scanType);

					ctxMem.transformBlock_xC = pom_sub_x + (ctxMem.subBlockCoordinateX * 4);
					ctxMem.transformBlock_yC = pom_sub_y + (ctxMem.subBlockCoordinateY * 4);

					ctxMem.scanIdx = tb->scanType;

					if (flag == true)
						flag = false;
					else
					{

						if ((nullCounter < 15) || (i == (numberOfSubblocksInTU - 1)))
							encodeElement(sig_coeff_flag, tb->subBlocks[i].SigCoeffFlag[j]);

						if (tb->subBlocks[i].SigCoeffFlag[j] == 0)
							nullCounter++;
					}
				}
			}

		for (int j = 0; j < 16; j++)
		{
			if (tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] != -1)
			{
				ctxMem.inside_sub_block_index = j;
				encodeElement(coeff_abs_level_greater1_flag, tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j]);
				ctxMem.previous_ALG1_flag = tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] == 1;
			}
		}

		for (int j = 0; j < 16; j++)
		{
			if (tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j] != -1)
				encodeElement(coeff_abs_level_greater2_flag, tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j]);
		}

		for (int j = 0; j < 16; j++)
		{
			if (tb->subBlocks[i].CoeffSignFlag[j] != -1)
				encodeElement(coeff_sign_flag, tb->subBlocks[i].CoeffSignFlag[j]);
		}

		ctxMem.first_time_invoke = true; // coeff_abs_level_remaining is binarized for first time in sub-block
		for (int j = 0; j < 16; j++)
		{
			if (tb->subBlocks[i].CoeffAbsLevelRemaining[j] != -1)
			{
				ctxMem.coeff_abs_level_greater1_flag = tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] == -1 ? 0 : tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j];
				ctxMem.coeff_abs_level_greater2_flag = tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j] == -1 ? 0 : tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j];
				encodeElement(coeff_abs_level_remaining, tb->subBlocks[i].CoeffAbsLevelRemaining[j]);
			}
		}
	}
}

void encoder::EntropyController::encodeElement(syntaxElemEnum syntaxElemNo, int syntaxElemVal)
{
	SyntaxElementInfo info = sMap.find(syntaxElemNo)->second;

	if (info.binType == NoBinarization)
	{
		buffer.writeBits(0, info.writeBits);
	}
	else
	{
		Bins b = Binarizer::binarizeElement(syntaxElemNo, syntaxElemVal, &info, &ctxMem);
		if (info.isCABACencoded)
			cabacEngine.cabacEncode(b.length, b.bits, &info);
		else
			buffer.writeBins(b);
	}
}

void encoder::EntropyController::init()
{
	buffer.Init();
	cabacEngine.CabacInit(&ctxMem, enCtx->quantizationParameter, &buffer);
}

void encoder::EntropyController::reset()
{
	buffer.Init();
	cabacEngine.CabacReset(&ctxMem, enCtx->quantizationParameter, &buffer);
}

void encoder::EntropyController::writeTrailingBits()
{
	buffer.writeBits(true, 1);
	buffer.byteAlign(false);
	buffer.flushBuffer();
}

void encoder::EntropyController::writeStartCode()
{
	buffer.writeBytes(0x00, 3);
	buffer.writeBytes(0x80, 1);
}

void encoder::EntropyController::flushOutputBuffer()
{
	buffer.byteAlign(false);
	buffer.flushBuffer();
}

void encoder::EntropyController::refreshCabacEngine()
{
	//cabacEngine.aritmeticEncoder.encodeTerminate(1);
	flushOutputBuffer();
	ctxMem.StatCoeff[0] = 0;
	ctxMem.StatCoeff[1] = 0;
	ctxMem.StatCoeff[2] = 0;
	ctxMem.StatCoeff[3] = 0;

	bool cabac_init_flag = false;

	cabacEngine.probMods->InitializeSelection(enCtx->quantizationParameter, cabac_init_flag);
	cabacEngine.aritmeticEncoder.initArithmeticCoder(false);

}

void encoder::EntropyController::clearModels()
{
	cabacEngine.clean();
}

void encoder::EntropyController::Init(EncodingContext * _enCtx)
{
	//ctxMem = ContextMemory();
	enCtx = _enCtx;
	init();
}




