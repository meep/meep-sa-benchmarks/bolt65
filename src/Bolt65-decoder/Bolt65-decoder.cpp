/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "DecodingChannel.h"
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		DecodingChannel ch1;
		ch1.Configure(argv, argc);
		ch1.OpenIOStreams();
		ch1.Initialize();
		ch1.Run();
	}
	else
	{
		cout << "Error in passing of arguments." << endl;
		exit(EXIT_FAILURE);
	}

	return 0;

}
