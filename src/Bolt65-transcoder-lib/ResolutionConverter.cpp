/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ResolutionConverter.h"

ResolutionConverter::ResolutionConverter()
{

}

ResolutionConverter::~ResolutionConverter()
{

}

void ResolutionConverter::ConvertResolution(unsigned char* original, int originalWidth, int originalHeight, unsigned char* transcoded, int transcodedWidth, int transcodedHeight)
{
	if (originalWidth == transcodedWidth && originalHeight == transcodedHeight)
	{
		memcpy(transcoded, original, originalWidth * originalHeight * 3 / 2);
	}
	else if (originalWidth > transcodedWidth && originalHeight > transcodedHeight)
	{
		//DOWNSIZING RESOLUTION
		int lumaSizeOriginal = originalWidth * originalHeight;
		int lumaSizeTranscoded = transcodedWidth * transcodedHeight;
		int chromaSizeOriginal = lumaSizeOriginal / 4;
		int chromaSizeTranscoded = lumaSizeTranscoded / 4;

		float widthCoeff = (float)originalWidth / transcodedWidth;
		float heightCoeff = (float)originalHeight / transcodedHeight;
		float widthStep = 0.0;
		float heightStep = 0.0;

		int i, j;

		//Luma component
		for (i = 0; i < transcodedHeight; i++)
		{
			for (j = 0; j < transcodedWidth; j++)
			{
				int fullWidthPixel = (int)ceil(widthStep);
				int fullHeightPixel = (int)ceil(heightStep);

				transcoded[i * transcodedWidth + j] = original[fullHeightPixel * originalWidth + fullWidthPixel];

				if (i < transcodedHeight / 2 && j < transcodedWidth / 2)
				{
					transcoded[lumaSizeTranscoded + i * transcodedWidth / 2 + j] = original[lumaSizeOriginal + fullHeightPixel * originalWidth / 2 + fullWidthPixel];
					transcoded[lumaSizeTranscoded + chromaSizeTranscoded + i * transcodedWidth / 2 + j] = original[lumaSizeOriginal + chromaSizeOriginal + fullHeightPixel * originalWidth / 2 + fullWidthPixel];
				}

				widthStep += widthCoeff;
			}
			heightStep += heightCoeff;
			widthStep = 0.0;
		}
		heightStep = 0.0;
	}
	else if (originalWidth < transcodedWidth && originalHeight < transcodedHeight)
	{
		//UPSIZING
	}
	else
	{
		cout << "Wrong transcoding rations, check sizes of source and destination videos" << endl;
		exit(EXIT_FAILURE);
	}

}

void ResolutionConverter::ConvertResolutionWeighted(unsigned char* original, int originalWidth, int originalHeight, unsigned char* transcoded, int transcodedWidth, int transcodedHeight)
{
	if (originalWidth == transcodedWidth && originalHeight == transcodedHeight)
	{
		memcpy(transcoded, original, originalWidth * originalHeight * 3 / 2);
	}
	else if (originalWidth > transcodedWidth && originalHeight > transcodedHeight)
	{
		//DOWNSIZING RESOLUTION
		int lumaSizeOriginal = originalWidth * originalHeight;
		int lumaSizeTranscoded = transcodedWidth * transcodedHeight;
		int chromaSizeOriginal = lumaSizeOriginal / 4;
		int chromaSizeTranscoded = lumaSizeTranscoded / 4;

		float widthCoeff = (float)originalWidth / transcodedWidth;
		float heightCoeff = (float)originalHeight / transcodedHeight;
		float widthStep = 0.0;
		float heightStep = 0.0;

		int i, j;

		//Luma component
		//transcoded[i*transcodedWidth + j]
		for (i = 0; i < transcodedHeight; i++)
		{
			float nextHeightStep = heightStep + heightCoeff;
			float currentHeightStep = heightStep;
			widthStep = 0.0;

			for (j = 0; j < transcodedWidth; j++)
			{
				float sum = 0.0;
				float coeff = 0.0;
				float nextWidthStep = widthStep + widthCoeff;
				float currentWidthStep = widthStep;


				while (heightStep < nextHeightStep)
				{
					while (widthStep < nextWidthStep)
					{

						float pixelWidthCoeff = ceil(widthStep) - widthStep;
						if (pixelWidthCoeff == 0.00)
							pixelWidthCoeff = nextWidthStep - widthStep > 1 ? 1 : nextWidthStep - widthStep;

						float pixelHeightCoeff = ceil(heightStep) - heightStep;
						if (pixelHeightCoeff == 0.00)
							pixelHeightCoeff = nextHeightStep - heightStep > 1 ? 1 : nextHeightStep - heightStep;

						float pixelCoeff = pixelWidthCoeff * pixelHeightCoeff;

						int pOriginalHeight = (int)heightStep;
						int pOriginalWidth = (int)widthStep;

						sum += pixelCoeff * original[pOriginalHeight * originalWidth + pOriginalWidth];
						coeff += pixelCoeff;


						if (ceil(widthStep) - widthStep != 0.00)
						{
							widthStep = ceil(widthStep);
						}
						else
						{
							if (widthStep + 1 < nextWidthStep)
								widthStep += 1;
							else
								widthStep = nextWidthStep;
						}
					}

					if (ceil(heightStep) - heightStep != 0.00)
					{
						heightStep = ceil(heightStep);
					}
					else
					{
						if (heightStep + 1 < nextHeightStep)
						{
							heightStep += 1;
							widthStep = currentWidthStep;
						}
						else
						{
							heightStep = nextHeightStep;
						}
					}
				}

				float valueY = sum / coeff;
				transcoded[i * transcodedWidth + j] = (unsigned char)round(valueY);
				heightStep = currentHeightStep;
			}
			heightStep += heightCoeff;

		}

		widthStep = 0.0;
		heightStep = 0.0;

		//Chroma components
		for (i = 0; i < transcodedHeight / 2; i++)
		{
			float nextHeightStep = heightStep + heightCoeff;
			float currentHeightStep = heightStep;
			widthStep = 0.0;

			for (j = 0; j < transcodedWidth / 2; j++)
			{
				float sumU = 0.0;
				float sumV = 0.0;
				float coeff = 0.0;
				float nextWidthStep = widthStep + widthCoeff;
				float currentWidthStep = widthStep;


				while (heightStep < nextHeightStep)
				{
					while (widthStep < nextWidthStep)
					{

						float pixelWidthCoeff = ceil(widthStep) - widthStep;
						if (pixelWidthCoeff == 0.00)
							pixelWidthCoeff = nextWidthStep - widthStep > 1 ? 1 : nextWidthStep - widthStep;

						float pixelHeightCoeff = ceil(heightStep) - heightStep;
						if (pixelHeightCoeff == 0.00)
							pixelHeightCoeff = nextHeightStep - heightStep > 1 ? 1 : nextHeightStep - heightStep;

						float pixelCoeff = pixelWidthCoeff * pixelHeightCoeff;

						int pOriginalHeight = (int)heightStep;
						int pOriginalWidth = (int)widthStep;

						sumU += pixelCoeff * original[lumaSizeOriginal + pOriginalHeight * originalWidth / 2 + pOriginalWidth];
						sumV += pixelCoeff * original[lumaSizeOriginal + chromaSizeOriginal + pOriginalHeight * originalWidth / 2 + pOriginalWidth];
						coeff += pixelCoeff;


						if (ceil(widthStep) - widthStep != 0.00)
						{
							widthStep = ceil(widthStep);
						}
						else
						{
							if (widthStep + 1 < nextWidthStep)
								widthStep += 1;
							else
								widthStep = nextWidthStep;
						}
					}

					if (ceil(heightStep) - heightStep != 0.00)
					{
						heightStep = ceil(heightStep);
					}
					else
					{
						if (heightStep + 1 < nextHeightStep)
						{
							heightStep += 1;
							widthStep = currentWidthStep;
						}
						else
						{
							heightStep = nextHeightStep;
						}
					}
				}

				float valueU = sumU / coeff;
				float valueV = sumV / coeff;
				transcoded[lumaSizeTranscoded + i * transcodedWidth / 2 + j] = (unsigned char)round(valueU);
				transcoded[lumaSizeTranscoded + chromaSizeTranscoded + i * transcodedWidth / 2 + j] = (unsigned char)round(valueV);
				heightStep = currentHeightStep;
			}
			heightStep += heightCoeff;

		}


	}
	else if (originalWidth < transcodedWidth && originalHeight < transcodedHeight)
	{
		//UPSIZING
	}
	else
	{
		cout << "Wrong transcoding rations, check sizes of source and destination videos" << endl;
		exit(EXIT_FAILURE);
	}
}
