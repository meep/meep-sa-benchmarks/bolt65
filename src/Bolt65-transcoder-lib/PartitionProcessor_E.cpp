/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "PartitionProcessor_E.h"

transcoder::PartitionProcessor_E::PartitionProcessor_E()
{
}

transcoder::PartitionProcessor_E::~PartitionProcessor_E()
{
}

void transcoder::PartitionProcessor_E::processPartition()
{
	clock_t partitionStart = clock();

	if (reuseDecodedData)
	{
		ctb.CreateCTBTreeWithDecodedCUMapping(partition, (Frame*)decodedFrame);
	}
	else
	{
		ctb.CreateCTBTree(partition);
	}

	ctb.CreateRelations(partition);
	
	pc.PerformPrediction(partition);
	loopc.PerformDeblockingFilter(partition);
	Scanner::PerformScanning(partition);

	ec.parseSliceData(partition);

	ec.refreshCabacEngine();

	//Clear memory of current EntropyController
	ec.clearModels();

	partition->proccesingTime = float(clock() - partitionStart) / CLOCKS_PER_SEC;
	partition->bitSize = ec.buffer.Param_Number_of_bits;
}

void transcoder::PartitionProcessor_E::initPartition(Partition *_partition, Partition * _decodedFrame, EncodingContext *enCtx, EncodingContext *decCtx, DPBManager *dpbm)
{
	partition = _partition;
	decodedFrame = _decodedFrame;
	ctb.Init(enCtx, decCtx);
	tq.Init(enCtx);
	pc.Init(&tq, dpbm, enCtx);
	loopc.Init(enCtx);
	ec.Init(enCtx);
	reuseDecodedData = enCtx->reuseData;

}

vector<unsigned char>* transcoder::PartitionProcessor_E::getBuffer()
{
	return &ec.buffer.output_buffer;
}
