/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "TranscodingChannel.h"

TranscodingChannel::TranscodingChannel()
{
	STATUS = TRANSCODER_NOT_STARTED;
	decNumOfPartitions = -1;
	encNumOfPartitions = -1;
	decPartitionProcessors = nullptr;
	encPartitionProcessors = nullptr;
	decTilesPerThread = nullptr;
	encTilesPerThread = nullptr;
	streamObject = nullptr;
}

void TranscodingChannel::StartTranscoding(char* argv[], int argc)
{
	Configure(argv, argc);
	OpenIOStreams();
	Initialize();
	Run();
}

void TranscodingChannel::Configure(char* argv[], int argc)
{
	enCtx.appType = Encoder;
	decCtx.appType = Decoder;

	cc.checkExternalConfig(&enCtx,argv, argc);
	cc.checkExternalConfig(&decCtx, argv, argc); 

	if (enCtx.externalConfig)
	{
		cc.fetchConfigurationFromFile(&enCtx);
		cc.fetchConfigurationFromFile(&decCtx); 
	}
	else
	{
		cout << "No external configuration file." << endl;
	}

	//Console parameters override configuration file 
	cc.parseConsoleParameters(&enCtx,argv, argc);
	cc.parseConsoleParameters(&decCtx, argv, argc);

	cc.validateContextForTranscoder(&enCtx, &decCtx);
}

void TranscodingChannel::OpenIOStreams()
{
	transcoderIO.InitIO(&enCtx, &decCtx);
	STATUS = TRANSCODER_IO_STREAMS_OPENED;
}

void TranscodingChannel::Initialize()
{
	streamObject = new Streamer(0);

	decoderEntropyC.Init(streamObject, &decCtx);
	encoderEntropyC.Init(&enCtx);

	dpbmEncoder.Init(enCtx.dpbSize);
	dpbmDecoder.Init(decCtx.dpbSize);

	//initialize vector implementations for static classes
	ComUtil::initialize(&enCtx);

	STATUS = TRANSCODER_INITIALIZED;
}

void TranscodingChannel::Run()
{
	Frame * frameDecoded;
	Frame * frameEncoded;
	int frameNumberDecoded = 0;
	int frameNumberEncoded = 0;

	if (enCtx.tilesEnabled)
		encTilesPerThread = new int*[enCtx.threads];

	enCtx.currentVPSId = -1;
	enCtx.currentSPSId = -1;
	enCtx.currentPPSId = -1;

	stats.startVideoClock(enCtx.calculateTime);
	cout << "Starting transcoding process" << endl << endl;

	float frameRateReduxCoeff = 0.0;
	bool isFrameEncoded = true;

	while (!decoderEntropyC.IsVideoDecoded())
	{
		decoderEntropyC.DecodeNextNAL();
		decoderEntropyC.ReleaseMemory();

		if (decCtx.nalType == NALType::VPS_NUT)
		{
			enCtx.currentVPSId++;
			encoderEntropyC.parseNonVCL(VPS_NUT);
			addNewVPS = false;
		}
		if (decCtx.nalType == NALType::SPS_NUT)
		{
			enCtx.currentSPSId++;
			encoderEntropyC.parseNonVCL(SPS_NUT);
			addNewSPS = false;
		}
		if (decCtx.nalType == NALType::PPS_NUT)
		{
			enCtx.currentPPSId++;
			encoderEntropyC.parseNonVCL(PPS_NUT);
			addNewPPS = false;
		}


		if (decCtx.nalType == NALType::IDR_W_RADL || decCtx.nalType == NALType::TRAIL_R)
		{
			stats.startFrameClock(enCtx.calculateTime);
			frameRateReduxCoeff = (float)decCtx.frameRate / (float)enCtx.frameRate;

			char frameTypeEncoded = enCtx.GOP.at(frameNumberEncoded% enCtx.GOP.size());

			frameDecoded = new Frame(&decCtx, frameNumberDecoded, (SliceType)decoderEntropyC.ctxMem.sliceType);
			frameEncoded = new Frame(&enCtx, frameNumberEncoded, frameTypeEncoded);

			if (frameNumberDecoded == 0)
				printTranscodingInfo();

			if (enCtx.showStatsPerFrame)
				cout << "Processing frame : " << frameNumberDecoded;

			frameNumberDecoded++;
			isFrameEncoded = frameDecoded->poc % (int)frameRateReduxCoeff == 0;

			decodeFrame(frameDecoded, frameNumberDecoded);

			frameEncoded->payload = new unsigned char[frameEncoded->size]{ 0 };

			ResolutionConverter::ConvertResolutionWeighted(frameDecoded->reconstructed_payload, frameDecoded->width, frameDecoded->height, frameEncoded->payload, frameEncoded->width, frameEncoded->height);

			if (isFrameEncoded)
			{
				frameNumberEncoded++;
				encodeFrame(frameEncoded, frameDecoded, frameNumberEncoded);
			}
			else
			{
				delete frameEncoded;
			}

			if (enCtx.statMode)
				stats.showFrameTime(&enCtx);

			if (enCtx.showStatsPerFrame)
				cout << endl;

			if (frameNumberDecoded == decCtx.numOfFrames)
				break;

		}

	}

	if (enCtx.statMode)
		stats.showVideoStatsTranscoding(&enCtx, &decCtx);

	clearMemory();
}

TranscodingChannel::~TranscodingChannel()
{
}

void TranscodingChannel::decodeFrame(Frame *decFrame, int decFrameNum)
{
	stats.startDecodingClock(enCtx.calculateTime);

	if (!decProcessesInitialized)
		InitializeDecProcesses();

	if (decCtx.tilesEnabled)
	{
		std::thread *processingBlockThread = new thread[decCtx.threads];
		decFrame->splitToTiles();

		/*Simplest Load balancing for decoding for now, can be initialized only on first frame, since we do not change number of partitions*/
		if (decFrameNum == 1)
			decTilesPerThread = new int*[decCtx.threads];

		LoadBalancing::simplestTilePerThreadLB(decFrame, decTilesPerThread, decCtx.threads, decNumOfPartitions, dpbmDecoder.dpb->frames[0], &stats);

		for (int i = 0; i < decCtx.threads; i++)
		{
			processingBlockThread[i] = thread(&TranscodingChannel::ThreadDecProcessor, this, i, decFrame);
		}
		for (int i = 0; i < decCtx.threads; i++)
		{
			processingBlockThread[i].join();
		}
		delete[] processingBlockThread;

		//Construct CTU tree for frame from all tiles. It is neccessary because encoded frame can be split in tiles in different manner
		decFrame->scanTileCTUs();
	}
	else
	{
		long frameOffset = decoderEntropyC.GetBaseOffset();
		decPartitionProcessors[0]->initPartition(decFrame, &decCtx, &dpbmDecoder, frameOffset);
		decPartitionProcessors[0]->decEc.ctxMem.CopySliceType(decFrame->sliceType);
		decPartitionProcessors[0]->processPartition();
	}

	MemoryManager::Free();
	dpbmDecoder.AddFrameToDPB(decFrame);
	decoderEntropyC.EndFrame();

	stats.addToDecodingClock();
}

void TranscodingChannel::encodeFrame(Frame *encFrame, Frame *decFrame, int encFrameNum)
{
	stats.startEncodingClock(enCtx.calculateTime);

	if (!encProcessesInitialized)
		InitializeEncProcesses();

	if (encNumOfPartitions > 1 || enCtx.tileLoadBalancingAlgorithm == TranscodingLB)
	{
		std::thread *processingBlockThread = new thread[enCtx.threads];


		LoadBalancing::simplestTilePerThreadLB(encFrame, encTilesPerThread, enCtx.threads, encNumOfPartitions, dpbmEncoder.dpb->frames[0], &stats);

		if (enCtx.tilesEnabled)
			encFrame->splitToTiles();

		for (int i = 0; i < enCtx.threads; i++)
		{
			processingBlockThread[i] = thread(&TranscodingChannel::ThreadEncProcessor, this, i, encFrame, decFrame);
		}
		for (int i = 0; i < enCtx.threads; i++)
		{
			processingBlockThread[i].join();
		}
		delete[] processingBlockThread;
	}
	else
	{
		encPartitionProcessors[0]->initPartition(encFrame, decFrame, &enCtx, &decCtx, &dpbmEncoder);
		encPartitionProcessors[0]->ec.ctxMem.CopySliceType(encFrame->sliceType);
		encPartitionProcessors[0]->processPartition();
	}

	enCtx.nalType = NALType::TRAIL_R;
	if (encFrameNum == 1)
		enCtx.nalType = NALType::IDR_W_RADL;

	encoderEntropyC.parseVCL(enCtx.nalType, encFrame);
	stats.sumBits += transcoderIO.writeBufferToFile(&encoderEntropyC.buffer);
	for (int i = 0; i < encNumOfPartitions; i++)
	{
		encPartitionProcessors[i]->ec.flushOutputBuffer();
		stats.frameBits += transcoderIO.writeBufferToFile(&encPartitionProcessors[i]->ec.buffer);

	}
	stats.sumBits += stats.frameBits;
	dpbmEncoder.AddFrameToDPB(encFrame);

	stats.addToEncodingClock();

}

void TranscodingChannel::ThreadDecProcessor(int threadId, Frame * frame)
{
	int i = 0;
	int tileId = decTilesPerThread[threadId][i++];

	while (tileId != -1)
	{
		long frameOffset = decoderEntropyC.GetBaseOffset();
		int tileOffset = frameOffset;
		for (int j = 0; j < tileId; j++)
			tileOffset += decCtx.tileOffsets[j + 1];

		decPartitionProcessors[tileId]->initPartition(frame->tiles[tileId], &decCtx, &dpbmDecoder, tileOffset);
		decPartitionProcessors[tileId]->decEc.ctxMem.CopySliceType(frame->sliceType);
		decPartitionProcessors[tileId]->processPartition();

		tileId = decTilesPerThread[threadId][i++];
	}
}

void TranscodingChannel::ThreadEncProcessor(int threadId, Frame * frame, Frame *decFrame)
{
	int i = 0;
	int tileId = encTilesPerThread[threadId][i++];

	while (tileId != -1)
	{
		encPartitionProcessors[tileId]->initPartition(frame->tiles[tileId], decFrame, &enCtx, &decCtx, &dpbmEncoder);
		encPartitionProcessors[tileId]->ec.ctxMem.CopySliceType(frame->sliceType);
		encPartitionProcessors[tileId]->processPartition();
		enCtx.tileOffsets[tileId + 1] = (int)(frame->tiles[tileId]->bitSize / 8);
		tileId = encTilesPerThread[threadId][i++];
	}
}

void TranscodingChannel::clearMemory()
{
	dpbmEncoder.ClearDPB();
	dpbmDecoder.ClearDPB();

	transcoderIO.closeFiles();

	decoderEntropyC.clearModels();
	encoderEntropyC.clearModels();

	enCtx.clearEncodingContext();
	decCtx.clearEncodingContext();

	stats.clear(enCtx.outputStatsTileToFile);

	delete streamObject;

	/*delete partitionProcessors[0];
	delete[] partitionProcessors;*/

	if (enCtx.tilesEnabled)
	{
		for (int i = 0; i < enCtx.numberOfTilesInColumn * enCtx.numberOfTilesInRow; i++)
		{
			delete encPartitionProcessors[i];
		}
		delete[] encPartitionProcessors;

	}
	else
	{
		delete encPartitionProcessors[0];
		delete[] encPartitionProcessors;
	}

	if (decCtx.tilesEnabled)
	{
		for (int i = 0; i < decCtx.numberOfTilesInColumn * decCtx.numberOfTilesInRow; i++)
		{
			delete decPartitionProcessors[i];
		}
		delete[] decPartitionProcessors;

	}
	else
	{
		delete decPartitionProcessors[0];
		delete[] decPartitionProcessors;
	}

	if (decCtx.tilesEnabled)
	{
		for (int i = 0; i < decCtx.threads; i++)
			delete decTilesPerThread[i];

		delete[] decTilesPerThread;
	}

	if (enCtx.tilesEnabled)
	{
		for (int i = 0; i < enCtx.threads; i++)
			delete encTilesPerThread[i];

		delete[] encTilesPerThread;
	}

	ComUtil::clean();

}

void TranscodingChannel::printTranscodingInfo()
{
	cout << endl << "TRANSCODING INFO: " << endl;
	int numOfChanges = 0;

	if (enCtx.quantizationParameter != decCtx.quantizationParameter)
	{
		cout << "-	Changing quantization parameter from " << decCtx.quantizationParameter << " in original bitstream to " << enCtx.quantizationParameter << endl;
		numOfChanges++;
	}
	if (enCtx.width != decCtx.width || enCtx.height != decCtx.height)
	{
		cout << "-	Changing resolution from " << decCtx.width << "x" << decCtx.height << " in original bitstream to " << enCtx.width << "x" << enCtx.height << endl;

		float widthRatio = (float)decCtx.width / enCtx.width;
		float heightRatio = (float)decCtx.height / enCtx.height;

		if (widthRatio != heightRatio)
			cout << "	WARNING: width and height ratios are not the same for original and transcoded video. Transcoding will continue." << endl;

		numOfChanges++;
	}
	if (enCtx.frameRate != decCtx.frameRate)
	{
		if (enCtx.frameRate > decCtx.frameRate)
		{
			cout << "-	Trying to transcode to higher frame rate. Transcoding ABORTED!" << endl;
			exit(EXIT_FAILURE);
		}
		else
		{
			cout << "-	Changing frame rate from " << decCtx.frameRate << "fps in original bitstream to " << enCtx.frameRate << "fps" << endl;
			numOfChanges++;
		}
	}

	if (numOfChanges == 0)
	{
		cout << "There is no difference between original and transcoded video in terms of resolution, QP and frame rate" << endl;
	}
	cout << endl;

}

void TranscodingChannel::InitializeEncProcesses()
{
	if (enCtx.tilesEnabled)
	{
		encNumOfPartitions = enCtx.numberOfTilesInColumn * enCtx.numberOfTilesInRow;
	}
	else
	{
		encNumOfPartitions = 1;
	}

	encPartitionProcessors = new transcoder::PartitionProcessor_E *[encNumOfPartitions];

	for (int i = 0; i < encNumOfPartitions; i++)
	{
		encPartitionProcessors[i] = new transcoder::PartitionProcessor_E();
	}

	enCtx.tileOffsets = new int[encNumOfPartitions + 1];
	enCtx.tileOffsets[0] = 0;

	encProcessesInitialized = true;
}

void TranscodingChannel::InitializeDecProcesses()
{
	if (decCtx.tilesEnabled)
	{
		decNumOfPartitions = decCtx.numberOfTilesInColumn * decCtx.numberOfTilesInRow;
	}
	else
	{
		decNumOfPartitions = 1;
	}

	decPartitionProcessors = new transcoder::PartitionProcessor_D *[decNumOfPartitions];

	for (int i = 0; i < decNumOfPartitions; i++)
	{
		decPartitionProcessors[i] = new transcoder::PartitionProcessor_D();
	}

	decProcessesInitialized = true;
}

