/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/


#include "../../include/Bolt65-transcoder-lib/IOController.h"


transcoder::IOController::IOController()
{
}

transcoder::IOController::IOController(EncodingContext* enCtx, EncodingContext* decCtx)
{
	ioKernel = ioBlock();

	remove(enCtx->outputFilePath.c_str());
	ioKernel.openOutputFile(enCtx->outputFilePath);
}

transcoder::IOController::~IOController()
{
}

void transcoder::IOController::InitIO(EncodingContext * enCtx, EncodingContext *decCtx)
{

	inputBufSize = enCtx->inputBufSize;
	outputBufSize = enCtx->outputBufSize;

	if (!enCtx->inputFilePath.empty())
	{
		determineInputFileType(enCtx->inputFilePath);

		inputFilePointer = fopen(enCtx->inputFilePath.c_str(), "rb");

		if (inputFilePointer == NULL)
		{
			cout << "Error opening input file: " << enCtx->inputFilePath << endl;
			exit(EXIT_FAILURE);
		}
		else
		{
			cout << "The file " << enCtx->inputFilePath.c_str() << " was opened." << endl;
		}
		MemoryManager::Init(enCtx->inputBufSize, inputFilePointer);
	}

	if (!enCtx->outputFilePath.empty())
	{
		remove(enCtx->outputFilePath.c_str());
		ioKernel.openOutputFile(enCtx->outputFilePath);
	}
}

int transcoder::IOController::writeBufferToFile(EntropyBuffer * buffer)
{
	return ioKernel.writeBufferToFile(buffer);
}

void transcoder::IOController::closeFiles()
{
	fclose(inputFilePointer);
	ioKernel.closeFiles();

	MemoryManager::ReleaseBuffer();
}

void transcoder::IOController::determineInputFileType(string inputFileName)
{
	int delimiter = (int)inputFileName.find_last_of('.');

	string fileExtension = inputFileName.substr(delimiter + 1);

	if (fileExtension != "hevc")
	{
		cout << "Unknown input format type: " << fileExtension << endl;
		exit(EXIT_FAILURE);
	}
}
