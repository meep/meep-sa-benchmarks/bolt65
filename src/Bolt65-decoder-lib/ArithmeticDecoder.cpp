/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ArithmeticDecoder.h"

ArithmeticDecoder::ArithmeticDecoder(Streamer* streamObject)
{
	this->streamObject = streamObject;
	binVal = false;
	ivlCurrRange = -1;
	ivlOffset = -1;
}

ArithmeticDecoder::ArithmeticDecoder()
{
	this->streamObject = NULL;
	binVal = false;
	ivlCurrRange = -1;
	ivlOffset = -1;
}

ArithmeticDecoder ::~ArithmeticDecoder()
{
}

void ArithmeticDecoder::initArithmeticDecoder()
{
	ivlCurrRange = 510;
	unsigned short value;
	streamObject->GetBits(&value, 9);

	ivlOffset = value; // TODO: initialize ivlOffset with data read from the bitstream, see chapter 9.3.2.5 in th3e standard for details
}

void ArithmeticDecoder::decodeDecision(ProbabilityModel* probabilityModel)
{
	unsigned short	qRangeIdx;
	unsigned int	ivlLpsRange;

	// Calculate R_MPS by substracting calculated R_LPS from initially given interval R
	// R_LPS is derived(table lookup) from probability model state and interval R's quantizier index
	qRangeIdx = (ivlCurrRange >> 6) & 3;
	ivlLpsRange = rangeTabLps[probabilityModel->pStateIdx * 4 + qRangeIdx];
	ivlCurrRange = ivlCurrRange - ivlLpsRange;

	if (ivlOffset >= ivlCurrRange)					//valLps
	{
		binVal = !probabilityModel->valMps;
		ivlOffset -= ivlCurrRange;
		ivlCurrRange = ivlLpsRange;

		// Toogle valMps if valLps was observed and pLPS reached max value in previous bin
		if (probabilityModel->pStateIdx == 0)
			probabilityModel->valMps = 1 - probabilityModel->valMps;

		// Realize LPS transition in updating probability estimation
		probabilityModel->pStateIdx = transIdx[probabilityModel->pStateIdx * 3 + 1];

	}
	else											//valMps
	{
		binVal = probabilityModel->valMps;
		// Realize MPS transition in updating probability estimation
		probabilityModel->pStateIdx = transIdx[probabilityModel->pStateIdx * 3 + 2];
	}
	renormD();
}

void ArithmeticDecoder::renormD()
{
	while (ivlCurrRange < 256)
	{
		ivlCurrRange <<= 1;
		ivlOffset <<= 1;
		ivlOffset |= (int)streamObject->GetBit();
	}
}

void ArithmeticDecoder::decodeBypass()
{
	ivlOffset <<= 1;
	ivlOffset |= (int)streamObject->GetBit();

	if (ivlOffset >= ivlCurrRange)
	{
		binVal = 1;
		ivlOffset -= ivlCurrRange;
	}
	else
	{
		binVal = 0;
	}
}

void ArithmeticDecoder::decodeTerminate()
{
	ivlCurrRange -= 2;
	if (ivlOffset >= ivlCurrRange)
	{
		binVal = 1;
	}
	else
	{
		binVal = 0;
		renormD();
	}
}
