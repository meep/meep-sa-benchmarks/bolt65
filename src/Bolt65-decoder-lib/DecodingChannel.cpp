/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "DecodingChannel.h"

DecodingChannel::DecodingChannel()
{
	STATUS = DEC_NOT_STARTED;
}

DecodingChannel::~DecodingChannel()
{
}

void DecodingChannel::StartDecoding(char* argv[], int argc)
{
	Configure(argv, argc);
	OpenIOStreams();
	Initialize();
	Run();
}

void DecodingChannel::Configure(char* argv[], int argc)
{
	decCtx.appType = Decoder;
	cc.checkExternalConfig(&decCtx, argv, argc);

	if (decCtx.externalConfig)
	{
		cc.fetchConfigurationFromFile(&decCtx);
	}
	else
	{
		cout << "No external configuration file." << endl;
	}

	//Console parameters override configuration file 
	cc.parseConsoleParameters(&decCtx, argv, argc);

	cc.validateContextForDecoder(&decCtx);
}

void DecodingChannel::OpenIOStreams()
{
	decIc.InitIO(&decCtx);
	STATUS = DEC_IO_STREAMS_OPENED;
}

void DecodingChannel::Initialize()
{
	streamObject = new Streamer(0);
	decEc.Init(streamObject, &decCtx);
	dpbm.Init(decCtx.dpbSize);

	//initialize vector implementations for static classes
	ComUtil::initialize(&decCtx);


	STATUS = DEC_INITIALIZED;
}

void DecodingChannel::Run()
{
	std::thread* processingBlockThread = new thread[decCtx.threads];
	Frame* frame;
	int frameNumber = 0;

	stats.startVideoClock(decCtx.calculateTime);
	cout << "Starting decoding process" << endl << endl;

	while (!decEc.IsVideoDecoded())
	{
		decEc.DecodeNextNAL();
		decEc.ReleaseMemory();

		if (decCtx.nalType == NALType::IDR_W_RADL || decCtx.nalType == NALType::TRAIL_R || decCtx.nalType == NALType::CRA)
		{
			stats.startFrameClock(decCtx.calculateTime);

			if (!processesInitialized)
				InitializeProcesses();

			frame = new Frame(&decCtx, frameNumber, (SliceType)decEc.ctxMem.sliceType);

			if (decCtx.showStatsPerFrame)
				cout << "Processing frame : " << frameNumber;

			frameNumber++;

			if (decCtx.tilesEnabled)
			{
				frame->splitToTiles();

				/*Simplest Load balancing for decoding for now, can be initialized only on first frame, since we do not change number of partitions*/
				//TODO skip if only one thread is available
				if (frameNumber == 1)
				{
					tilesPerThread = new int* [decCtx.threads];
					int* counter = new int[decCtx.threads] { 0 };

					for (int i = 0; i < decCtx.threads; i++)
					{
						tilesPerThread[i] = new int[numOfPartitions + 1];
						for (int j = 0; j < numOfPartitions + 1; j++)
						{
							tilesPerThread[i][j] = -1;
						}
					}
					for (int j = 0; j < numOfPartitions; j++)
					{
						int threadId = j % decCtx.threads;
						tilesPerThread[threadId][counter[threadId]++] = j;
					}
					delete[] counter;
				}
				/*End of load balancing*/

				for (int i = 0; i < decCtx.threads; i++)
				{
					processingBlockThread[i] = thread(&DecodingChannel::ThreadProcessor, this, i, frame);
				}
				for (int i = 0; i < decCtx.threads; i++)
				{
					processingBlockThread[i].join();
				}

			}
			else
			{
				long frameOffset = decEc.GetBaseOffset();
				partitionProcessors[0]->initPartition(frame, &decCtx, &dpbm, frameOffset);
				partitionProcessors[0]->decEc.ctxMem.CopySliceType(frame->sliceType);
				partitionProcessors[0]->processPartition();
			}

			decIc.writeToOutputFile((char*)frame->reconstructed_payload, frame->size);

			MemoryManager::Free();
			dpbm.AddFrameToDPB(frame);
			decEc.EndFrame();

			if (decCtx.statMode)
				stats.calculateFrameStats(frame, &decCtx);

			if (decCtx.showStatsPerFrame)
				cout << endl;

			if (frameNumber == decCtx.numOfFrames)
				break;
		}
	}


	if (decCtx.statMode)
		stats.showVideoStats(&decCtx);

	if (processingBlockThread != nullptr)
		delete[] processingBlockThread;

	clearMemory();
}

void DecodingChannel::ThreadProcessor(int threadId, Frame* frame)
{
	int i = 0;
	int tileId = tilesPerThread[threadId][i++];

	while (tileId != -1)
	{
		long frameOffset = decEc.GetBaseOffset();
		int tileOffset = frameOffset;
		for (int j = 0; j < tileId; j++)
			tileOffset += decCtx.tileOffsets[j + 1];

		partitionProcessors[tileId]->initPartition(frame->tiles[tileId], &decCtx, &dpbm, tileOffset);
		partitionProcessors[tileId]->decEc.ctxMem.CopySliceType(frame->sliceType);
		partitionProcessors[tileId]->processPartition();

		tileId = tilesPerThread[threadId][i++];
	}
}

void DecodingChannel::InitializeProcesses()
{
	if (decCtx.tilesEnabled)
	{
		numOfPartitions = decCtx.numberOfTilesInColumn * decCtx.numberOfTilesInRow;
	}
	else
	{
		numOfPartitions = 1;
	}

	partitionProcessors = new PartitionProcessor * [numOfPartitions];

	for (int i = 0; i < numOfPartitions; i++)
	{
		partitionProcessors[i] = new PartitionProcessor();
	}

	processesInitialized = true;
}

void DecodingChannel::clearMemory()
{

	if (decCtx.tilesEnabled)
	{
		for (int i = 0; i < decCtx.numberOfTilesInColumn * decCtx.numberOfTilesInRow; i++)
		{
			delete partitionProcessors[i];
		}
		delete[] partitionProcessors;

	}
	else
	{
		delete partitionProcessors[0];
		delete[] partitionProcessors;
	}

	dpbm.ClearDPB();
	decIc.closeFiles();
	decEc.clearModels();
	delete streamObject;

	if (decCtx.tilesEnabled)
	{
		for (int i = 0; i < decCtx.threads; i++)
			delete tilesPerThread[i];

		delete[] tilesPerThread;
	}

	ComUtil::clean();
}
