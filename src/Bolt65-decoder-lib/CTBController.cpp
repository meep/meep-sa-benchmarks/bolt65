/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CTBController.h"

decoder::CTBController::CTBController()
{
}

void decoder::CTBController::Init(EncodingContext* _decCtx)
{
	decCtx = _decCtx;
	//blockPartition = BlockPartition();
}

void decoder::CTBController::ReconstructCTBTree(Partition * partition)
{

	int i, currRow, currCol, currRowFrame, currColumnFrame;
	int y_pixel_iterator, u_pixel_iterator, v_pixel_iterator;
	int y_pixel_iterator_frame, u_pixel_iterator_frame, v_pixel_iterator_frame;

	int startingIndexU = partition->width*partition->height;
	int startingIndexV = partition->width*partition->height + (partition->width*partition->height / 4);

	int startingIndexUFrame = decCtx->width*decCtx->height;
	int startingIndexVFrame = decCtx->width*decCtx->height + (decCtx->width*decCtx->height / 4);

	bool isWidthDividible = partition->width % decCtx->ctbSize == 0;
	bool isHeightDividible = partition->height % decCtx->ctbSize == 0;

	int startingTileRow = (partition->startingIndexInFrame / decCtx->width) / decCtx->ctbSize;
	int startingTileColumn = (partition->startingIndexInFrame % decCtx->width) / decCtx->ctbSize;

	for (i = 0; i < partition->numberOfCTUs; i++)
	{
		currRow = i / partition->numberOfCTUsInRow;
		currCol = i % partition->numberOfCTUsInRow;

		currRowFrame = startingTileRow + currRow;
		currColumnFrame = startingTileColumn + currCol;

		y_pixel_iterator = partition->width*currRow*decCtx->ctbSize + currCol * decCtx->ctbSize;
		u_pixel_iterator = startingIndexU + (partition->width / 2)*currRow*(decCtx->ctbSize / 2) + currCol * (decCtx->ctbSize / 2);
		v_pixel_iterator = startingIndexV + (partition->width / 2)*currRow*(decCtx->ctbSize / 2) + currCol * (decCtx->ctbSize / 2);

		y_pixel_iterator_frame = decCtx->width*currRowFrame*decCtx->ctbSize + currColumnFrame * decCtx->ctbSize;
		u_pixel_iterator_frame = startingIndexUFrame + (decCtx->width / 2)*currRowFrame*(decCtx->ctbSize / 2) + currColumnFrame * (decCtx->ctbSize / 2);
		v_pixel_iterator_frame = startingIndexVFrame + (decCtx->width / 2)*currRowFrame*(decCtx->ctbSize / 2) + currColumnFrame * (decCtx->ctbSize / 2);

		partition->ctuTree[i]->cbY = CB(y_pixel_iterator, y_pixel_iterator_frame, decCtx->ctbSize, decCtx->quantizationParameter, Luma);
		partition->ctuTree[i]->cbU = CB(u_pixel_iterator, u_pixel_iterator_frame, decCtx->ctbSize / 2, decCtx->quantizationParameter, ChromaCb);
		partition->ctuTree[i]->cbV = CB(v_pixel_iterator, v_pixel_iterator_frame, decCtx->ctbSize / 2, decCtx->quantizationParameter, ChromaCr);

		partition->ctuTree[i]->index = i;
		partition->ctuTree[i]->indexInFrame = (decCtx->width / decCtx->ctbSize)*currRowFrame + currColumnFrame;

		if (i != 0 && (i + 1) % partition->numberOfCTUsInRow == 0)
			partition->ctuTree[i]->isLastInRow = true;

		if (i > (partition->numberOfCTUs - 1) - partition->numberOfCTUsInRow)
			partition->ctuTree[i]->isLastInColumn = true;

		if ((partition->ctuTree[i]->isLastInRow && !isWidthDividible) || (partition->ctuTree[i]->isLastInColumn && !isHeightDividible))
			partition->ctuTree[i]->requiresFurtherSplitting = true;

		if (partition->ctuTree[i]->hasChildren)
			ReconstructChildren(partition->ctuTree[i], partition->width, partition->height, decCtx->width, decCtx->height);

		ReconstructTransformTree(partition->ctuTree[i], partition->width, partition->height, decCtx->width, decCtx->height);
	}

}

decoder::CTBController::~CTBController()
{
}

void decoder::CTBController::ReconstructChildren(CU* cu, int width, int height, int frameWidth, int frameHeight)
{
	int blockSize = cu->cbY.blockSize;
	int lumaQP = cu->cbY.cbQP;
	int i;
	for (i = 0; i < 4; i++)
	{
		int lumaPixelOffset = (i / 2)* (blockSize / 2 * width) + (i % 2)*blockSize / 2;
		int chromaPixelOffset = (i / 2)*(blockSize / 4 * width / 2) + (i % 2)*blockSize / 4;

		int lumaPixelOffsetFrame = (i / 2)* (blockSize / 2 * frameWidth) + (i % 2)*blockSize / 2;
		int chromaPixelOffsetFrame = (i / 2)*(blockSize / 4 * frameWidth / 2) + (i % 2)*blockSize / 4;

		cu->children[i]->cbY = CB(cu->cbY.startingIndex + lumaPixelOffset, cu->cbY.startingIndexInFrame + lumaPixelOffsetFrame, blockSize / 2, lumaQP, Luma);
		cu->children[i]->cbU = CB(cu->cbU.startingIndex + chromaPixelOffset, cu->cbU.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCb);
		cu->children[i]->cbV = CB(cu->cbV.startingIndex + chromaPixelOffset, cu->cbV.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCr);

		cu->children[i]->depth = cu->depth + 1;
		cu->children[i]->parent = cu;
		cu->children[i]->hasParent = true;
		cu->children[i]->index = i;
		//cu->CreatePredictionUnits();
		ReconstructTransformTree(cu->children[i], width, height, frameWidth, frameHeight);

		if (cu->children[i]->isSplit)
			ReconstructChildren(cu->children[i], width, height, frameWidth, frameHeight);
	}

	//Set all flags
	//setFlags(cu, width, height);

}

void decoder::CTBController::ReconstructTransformTree(CU *cu, int width, int height, int frameWidth, int frameHeight)
{
	if (cu->hasTransformTree)
	{
		//set root TU values
		cu->transformTree->tbY.startingIndex = cu->cbY.startingIndex;
		cu->transformTree->tbU.startingIndex = cu->cbU.startingIndex;
		cu->transformTree->tbV.startingIndex = cu->cbV.startingIndex;

		cu->transformTree->tbY.startingIndexInFrame = cu->cbY.startingIndexInFrame;
		cu->transformTree->tbU.startingIndexInFrame = cu->cbU.startingIndexInFrame;
		cu->transformTree->tbV.startingIndexInFrame = cu->cbV.startingIndexInFrame;

		ReconnstructTransformUnit(cu->transformTree, width, height, frameWidth, frameHeight);
	}
}

void decoder::CTBController::ReconnstructTransformUnit(TU * tu, int width, int height, int frameWidth, int frameHeight)
{
	int blockSize = tu->tbY.transformBlockSize;
	if (tu->hasChildren)
	{
		for (int i = 0; i < 4; i++)
		{
			int lumaPixelOffset = (i / 2)* (blockSize / 2 * width) + (i % 2)*blockSize / 2;
			int chromaPixelOffset = (i / 2)*(blockSize / 4 * width / 2) + (i % 2)*blockSize / 4;

			int lumaPixelOffsetFrame = (i / 2)* (blockSize / 2 * frameWidth) + (i % 2)*blockSize / 2;
			int chromaPixelOffsetFrame = (i / 2)*(blockSize / 4 * frameWidth / 2) + (i % 2)*blockSize / 4;

			tu->children[i]->tbY.startingIndex = tu->tbY.startingIndex + lumaPixelOffset;
			tu->children[i]->tbU.startingIndex = tu->tbU.startingIndex + chromaPixelOffset;
			tu->children[i]->tbV.startingIndex = tu->tbV.startingIndex + chromaPixelOffset;

			tu->children[i]->tbY.startingIndexInFrame = tu->tbY.startingIndexInFrame + lumaPixelOffsetFrame;
			tu->children[i]->tbU.startingIndexInFrame = tu->tbU.startingIndexInFrame + chromaPixelOffsetFrame;
			tu->children[i]->tbV.startingIndexInFrame = tu->tbV.startingIndexInFrame + chromaPixelOffsetFrame;

			if (tu->children[i]->hasChildren)
				ReconnstructTransformUnit(tu->children[i], width, height, frameWidth, frameHeight);
		}
	}
}

void decoder::CTBController::CreateRelations(Partition * partition)
{
	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		//BlockPartition::assignAllNeighbours(partition->ctuTree, partition->ctuTree[i], partition->width);
	}
}

void decoder::CTBController::setFlags(CU *cu, int frameWidth, int frameHeight)
{
	//Check if children are inside of Frame, only if CU that is splitted is last in row or in last column
	bool isWidthDividible = frameWidth % (cu->cbY.blockSize / 2) == 0;
	bool isHeightDividible = frameHeight % (cu->cbY.blockSize / 2) == 0;

	if (cu->isLastInRow)
	{
		int rightChildStartingIndex = cu->children[1]->cbY.startingIndex;
		int rightChildBlockSize = cu->children[1]->cbY.blockSize;
		bool areAllChildrenInFrame = rightChildStartingIndex % frameWidth >= frameWidth - rightChildBlockSize;

		if (!areAllChildrenInFrame)
		{
			cu->children[1]->isInFrame = false;
			cu->children[3]->isInFrame = false;
			cu->children[0]->isLastInRow = true;
			cu->children[2]->isLastInRow = true;

			if (!isWidthDividible)
			{
				cu->children[0]->requiresFurtherSplitting = true;
				cu->children[2]->requiresFurtherSplitting = true;
			}
		}
		else
		{
			cu->children[1]->isLastInRow = true;
			cu->children[3]->isLastInRow = true;

			if (!isWidthDividible)
			{
				cu->children[1]->requiresFurtherSplitting = true;
				cu->children[3]->requiresFurtherSplitting = true;
			}
		}
	}

	if (cu->isLastInColumn)
	{
		int LowerChildStartingIndex = cu->children[2]->cbY.startingIndex;
		bool areAllChildrenInFrame = LowerChildStartingIndex < frameHeight*frameWidth;

		if (!areAllChildrenInFrame)
		{
			cu->children[2]->isInFrame = false;
			cu->children[3]->isInFrame = false;
			cu->children[0]->isLastInColumn = true;
			cu->children[1]->isLastInColumn = true;

			if (!isHeightDividible)
			{
				cu->children[0]->requiresFurtherSplitting = true;
				cu->children[1]->requiresFurtherSplitting = true;
			}
		}
		else
		{
			cu->children[2]->isLastInColumn = true;
			cu->children[3]->isLastInColumn = true;

			if (!isHeightDividible)
			{
				cu->children[2]->requiresFurtherSplitting = true;
				cu->children[3]->requiresFurtherSplitting = true;
			}
		}
	}
}