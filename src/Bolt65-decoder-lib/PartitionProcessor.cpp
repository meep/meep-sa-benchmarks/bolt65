/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "PartitionProcessor.h"

decoder::PartitionProcessor::PartitionProcessor()
{
}

decoder::PartitionProcessor::~PartitionProcessor()
{
}

void decoder::PartitionProcessor::processPartition()
{
	decEc.DecodeSliceSegmentData(partition);
	decPc.PerformInversePrediction(partition);
	loopc.PerformDeblockingFilter(partition);

	partition->bitSize = streamObject->paramNumberOfBits;

	decEc.ReleaseMemory();
	decEc.clearModels();
	delete streamObject;
}

void decoder::PartitionProcessor::initPartition(Partition * _partition, EncodingContext * decCtx, DPBManager * dpbm, long partitionOffset)
{
	streamObject = new Streamer(partitionOffset);

	partition = _partition;
	decCtb.Init(decCtx);
	tq.Init(decCtx);
	decPc.Init(&tq, dpbm, decCtx);
	loopc.Init(decCtx);
	decEc.Init(streamObject, decCtx);
}
