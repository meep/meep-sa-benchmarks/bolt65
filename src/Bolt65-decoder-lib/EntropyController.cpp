/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "EntropyController.h"

decoder::EntropyController::EntropyController()
{
	decCtx = nullptr;
	ctxDataALG1 = ctxStruct4ALG1();
	probMods = nullptr;
	streamObject = nullptr;
}

decoder::EntropyController::~EntropyController()
{
	streamObject->CleanUp();
}

void decoder::EntropyController::Init(Streamer * streamObject, EncodingContext *_decCtx)
{
	decCtx = _decCtx;
	//sMap = SyntaxMap::InitializeSyntaxMap();
	this->streamObject = streamObject;
	ctxMem = ContextMemory();
	probMods = new ProbabilityModelSelection();
	aritDecod = ArithmeticDecoder(streamObject);
}

void decoder::EntropyController::DecodeNextNAL()
{
	DecodeHeader();

	switch (decCtx->nalType)
	{
	case NALType::VPS_NUT:
		DecodeVPS();
		break;
	case NALType::PPS_NUT:
		DecodePPS();
		break;
	case NALType::SPS_NUT:
		DecodeSPS();
		break;
	case NALType::IDR_W_RADL:
		DecodeSliceSegment();
		break;
	case NALType::TRAIL_R:
		DecodeSliceSegment();
		break;
	case NALType::CRA:
		DecodeSliceSegment();
		break;
	default:
		break;
	}

}

int decoder::EntropyController::DecodeElement(syntaxElemEnum syntaxElemNo)
{
	// TODO: Optimization of the decoding process
	/*SyntaxElementInfo info = sMap.find(syntaxElemNo)->second;

	if (info.binType == NoBinarization)
	{
		for (int i = 0; i < info.writeBits; i++) streamObject->GetBit();
	}
	else
	{

	}

	return 0;*/
	return 0;
}

void decoder::EntropyController::DecodeHeader()
{
	FromBitstream(syntaxElemEnum::forbidden_zero_bit);
	decCtx->nalType = (NALType)FromBitstream(syntaxElemEnum::nal_unit_type);
	FromBitstream(syntaxElemEnum::nuh_layer_id);
	FromBitstream(syntaxElemEnum::nuh_temporal_id_plus1);
}

void decoder::EntropyController::DecodeVPS()
{
	FromBitstream(syntaxElemEnum::vps_video_parameter_set_id);
	FromBitstream(syntaxElemEnum::vps_base_layer_internal_flag);
	FromBitstream(syntaxElemEnum::vps_base_layer_available_flag);
	FromBitstream(syntaxElemEnum::vps_max_layers_minus1);
	int vps_max_sub_layers_minus1 = FromBitstream(syntaxElemEnum::vps_max_sub_layers_minus1);
	decCtx->highestTid = vps_max_sub_layers_minus1;

	FromBitstream(syntaxElemEnum::vps_temporal_id_nesting_flag);
	FromBitstream(syntaxElemEnum::vps_reserved_0xffff_16bits);

	DecodeProfileTierLevel();

	int vps_sub_layer_ordering_info_present_flag = FromBitstream(syntaxElemEnum::vps_sub_layer_ordering_info_present_flag);

	for (int i = (vps_sub_layer_ordering_info_present_flag != 0 ? 0 : vps_max_sub_layers_minus1); i <= vps_max_sub_layers_minus1; i++)
	{
		FromBitstream(syntaxElemEnum::vps_max_dec_pic_buffering_minus1);
		FromBitstream(syntaxElemEnum::vps_max_num_reorder_pics);
		FromBitstream(syntaxElemEnum::vps_max_latency_increase_plus1);
	}

	int vps_max_layer_id = FromBitstream(syntaxElemEnum::vps_max_layer_id);
	int vps_num_layer_sets_minus1 = FromBitstream(syntaxElemEnum::vps_num_layer_sets_minus1);

	for (int i = 1; i <= vps_num_layer_sets_minus1; i++)
	{
		for (int j = 0; j <= vps_max_layer_id; j++)
		{
			FromBitstream(syntaxElemEnum::layer_id_included_flag);
		}
	}

	int vps_timing_info_present_flag = FromBitstream(syntaxElemEnum::vps_timing_info_present_flag);

	if (vps_timing_info_present_flag != 0)
	{
		int tick = FromBitstream(syntaxElemEnum::vps_num_units_in_tick);
		int scale = FromBitstream(syntaxElemEnum::vps_time_scale);
		decCtx->frameRate = scale / tick;

		int vps_poc_proportional_to_timing_flag = FromBitstream(syntaxElemEnum::vps_poc_proportional_to_timing_flag);

		if (vps_poc_proportional_to_timing_flag != 0)
		{
			FromBitstream(syntaxElemEnum::vps_num_ticks_poc_diff_one_minus1);
		}

		int vps_num_hrd_parameters = FromBitstream(syntaxElemEnum::vps_num_hrd_parameters);

		for (int i = 0; i < vps_num_hrd_parameters; i++)
		{
			FromBitstream(syntaxElemEnum::hrd_layer_set_idx);

			if (i > 0)
				FromBitstream(syntaxElemEnum::cprms_present_flag);

		}
	}

	FromBitstream(syntaxElemEnum::vps_extension_flag);

	streamObject->GetBit(); // Read the RBSP stop bit
	streamObject->ByteAlign(); // Reat trailing bits
}

void decoder::EntropyController::DecodeSPS()
{
	FromBitstream(syntaxElemEnum::sps_video_parameter_set_id);
	FromBitstream(syntaxElemEnum::sps_max_sub_layers_minus1);
	FromBitstream(syntaxElemEnum::sps_temporal_id_nesting_flag);

	DecodeProfileTierLevel();

	FromBitstream(syntaxElemEnum::sps_seq_parameter_set_id);
	int chroma_format_idc = FromBitstream(syntaxElemEnum::chroma_format_idc);

	if (chroma_format_idc == 3)
		FromBitstream(syntaxElemEnum::separate_colour_plane_flag);

	decCtx->width = FromBitstream(syntaxElemEnum::pic_width_in_luma_samples);
	decCtx->height = FromBitstream(syntaxElemEnum::pic_height_in_luma_samples);
	int conformance_window_flag = FromBitstream(syntaxElemEnum::conformance_window_flag);

	if (conformance_window_flag)
	{
		FromBitstream(syntaxElemEnum::conf_win_left_offset);
		FromBitstream(syntaxElemEnum::conf_win_right_offset);
		FromBitstream(syntaxElemEnum::conf_win_top_offset);
		FromBitstream(syntaxElemEnum::conf_win_bottom_offset);
	}

	decCtx->bitNumber = FromBitstream(syntaxElemEnum::bit_depth_luma_minus8) + 8;
	decCtx->dpbSize = 1;

	FromBitstream(syntaxElemEnum::bit_depth_chroma_minus8);
	decCtx->log2MaxPicOrderLst = FromBitstream(syntaxElemEnum::log2_max_pic_order_cnt_lsb_minus4) + 4;
	ctxMem.log2_max_pic_order_cnt_lsb_minus4 = decCtx->log2MaxPicOrderLst - 4;

	FromBitstream(syntaxElemEnum::sps_sub_layer_ordering_info_present_flag);

	FromBitstream(syntaxElemEnum::sps_max_dec_pic_buffering_minus1);
	FromBitstream(syntaxElemEnum::sps_max_num_reorder_pics);
	FromBitstream(syntaxElemEnum::sps_max_latency_increase_plus1);





	decCtx->minCbLog2SizeY = FromBitstream(syntaxElemEnum::log2_min_luma_coding_block_size_minus3) + 3;
	decCtx->ctbLog2SizeY = FromBitstream(syntaxElemEnum::log2_diff_max_min_luma_coding_block_size) + decCtx->minCbLog2SizeY;
	decCtx->ctbSize = 1 << decCtx->ctbLog2SizeY;

	decCtx->minTbLog2SizeY = FromBitstream(syntaxElemEnum::log2_min_luma_transform_block_size_minus2) + 2;
	decCtx->maxTbLog2SizeY = FromBitstream(syntaxElemEnum::log2_diff_max_min_luma_transform_block_size) + decCtx->minTbLog2SizeY;
	decCtx->max_transform_hierarchy_depth_inter = FromBitstream(syntaxElemEnum::max_transform_hierarchy_depth_inter);

	FromBitstream(syntaxElemEnum::max_transform_hierarchy_depth_intra);
	FromBitstream(syntaxElemEnum::scaling_list_enabled_flag);

	decCtx->useAMP = FromBitstream(syntaxElemEnum::amp_enabled_flag) == 1;
	FromBitstream(syntaxElemEnum::sample_adaptive_offset_enabled_flag);
	FromBitstream(syntaxElemEnum::pcm_enabled_flag);

	decCtx->num_short_term_ref_pic_sets = FromBitstream(syntaxElemEnum::num_short_term_ref_pic_sets);


	for (int i = 0; i < decCtx->num_short_term_ref_pic_sets; i++)
	{
		DecodeStRefPicSet(i);
	}
	//if (decCtx->num_short_term_ref_pic_sets > 0)
	//{
	//	DecodeStRefPicSet(decCtx->num_short_term_ref_pic_sets);
	//}

	FromBitstream(syntaxElemEnum::long_term_ref_pics_present_flag);

	decCtx->useTemporalAMVP = FromBitstream(syntaxElemEnum::sps_temporal_mvp_enabled_flag);
	ctxMem.sps_temporal_mvp_enabled_flag = decCtx->useTemporalAMVP;
	decCtx->strong_intra_smoothing_enabled_flag = FromBitstream(syntaxElemEnum::strong_intra_smoothing_enabled_flag);
	int vuiPresent = FromBitstream(syntaxElemEnum::vui_parameters_present_flag);

	if (vuiPresent == 1)
		DecodeVUI();

	FromBitstream(syntaxElemEnum::sps_extension_present_flag);

	streamObject->GetBit(); // Read the RBSP stop bit
	streamObject->ByteAlign(); // Reat trailing bits
}

void decoder::EntropyController::DecodePPS()
{
	FromBitstream(syntaxElemEnum::pps_pic_parameter_set_id);
	FromBitstream(syntaxElemEnum::pps_seq_parameter_set_id);
	FromBitstream(syntaxElemEnum::dependent_slice_segments_enabled_flag);
	FromBitstream(syntaxElemEnum::output_flag_present_flag);
	FromBitstream(syntaxElemEnum::num_extra_slice_header_bits);
	FromBitstream(syntaxElemEnum::sign_data_hiding_enabled_flag);
	decCtx->cabac_init_present_flag = FromBitstream(syntaxElemEnum::cabac_init_present_flag);
	FromBitstream(syntaxElemEnum::num_ref_idx_l0_default_active_minus1);
	FromBitstream(syntaxElemEnum::num_ref_idx_l1_default_active_minus1);
	int init_qp_minus26 = FromBitstream(syntaxElemEnum::init_qp_minus26);

	decCtx->initQP = init_qp_minus26 + 26;
	decCtx->quantizationParameter = init_qp_minus26 + 26;

	FromBitstream(syntaxElemEnum::constrained_intra_pred_flag);
	FromBitstream(syntaxElemEnum::transform_skip_enabled_flag);
	int cu_qp_delta_enabled_flag = FromBitstream(syntaxElemEnum::cu_qp_delta_enabled_flag);

	if (cu_qp_delta_enabled_flag != 0)
		FromBitstream(syntaxElemEnum::diff_cu_qp_delta_depth);

	FromBitstream(syntaxElemEnum::pps_cb_qp_offset);
	FromBitstream(syntaxElemEnum::pps_cr_qp_offset);
	FromBitstream(syntaxElemEnum::pps_slice_chroma_qp_offsets_present_flag);
	FromBitstream(syntaxElemEnum::weighted_pred_flag);
	FromBitstream(syntaxElemEnum::weighted_bipred_flag);
	FromBitstream(syntaxElemEnum::transquant_bypass_enabled_flag);

	decCtx->tilesEnabled = FromBitstream(syntaxElemEnum::tiles_enabled_flag);
	FromBitstream(syntaxElemEnum::entropy_coding_sync_enabled_flag);

	if (decCtx->tilesEnabled)
	{
		decCtx->numberOfTilesInColumn = FromBitstream(syntaxElemEnum::num_tile_columns_minus1) + 1;
		decCtx->numberOfTilesInRow = FromBitstream(syntaxElemEnum::num_tile_rows_minus1) + 1;

		decCtx->isUniform = FromBitstream(syntaxElemEnum::uniform_spacing_flag);

		decCtx->rowHeights = new int[decCtx->numberOfTilesInRow];
		decCtx->columnWidths = new int[decCtx->numberOfTilesInColumn];

		if (!decCtx->isUniform)
		{
			int numberOfCTUsInRow = (int)ceil((double)decCtx->width / decCtx->ctbSize);;
			int numberOfCTUsInColumn = (int)ceil((double)decCtx->height / decCtx->ctbSize);
			int sumInRows = 0;
			int sumInColumns = 0;

			for (int i = 0; i < decCtx->numberOfTilesInColumn - 1; i++)
			{
				decCtx->columnWidths[i] = FromBitstream(syntaxElemEnum::column_width_minus1) + 1;
				sumInColumns += decCtx->columnWidths[i];
			}
			for (int i = 0; i < decCtx->numberOfTilesInRow - 1; i++)
			{
				decCtx->rowHeights[i] = FromBitstream(syntaxElemEnum::row_height_minus1) + 1;
				sumInRows += decCtx->rowHeights[i];
			}
			decCtx->columnWidths[decCtx->numberOfTilesInColumn - 1] = numberOfCTUsInRow - sumInColumns;
			decCtx->rowHeights[decCtx->numberOfTilesInRow - 1] = numberOfCTUsInColumn - sumInRows;

		}

		FromBitstream(syntaxElemEnum::loop_filter_across_tiles_enabled_flag);
	}

	FromBitstream(syntaxElemEnum::pps_loop_filter_across_slices_enabled_flag);
	int deblocking_filter_control_present_flag = FromBitstream(syntaxElemEnum::deblocking_filter_control_present_flag);

	decCtx->disableDeblockingFilter = true;

	if (deblocking_filter_control_present_flag != 0)
	{
		FromBitstream(syntaxElemEnum::deblocking_filter_override_enabled_flag);
		decCtx->disableDeblockingFilter = FromBitstream(syntaxElemEnum::pps_deblocking_filter_disabled_flag);

		if (decCtx->disableDeblockingFilter == 0)
		{
			FromBitstream(syntaxElemEnum::pps_beta_offset_div2);
			FromBitstream(syntaxElemEnum::pps_tc_offset_div2);
		}
	}

	int pps_scaling_list_data_present_flag = FromBitstream(syntaxElemEnum::pps_scaling_list_data_present_flag);

	if (pps_scaling_list_data_present_flag != 0)  // TODO: Scaling list data
	{
	}

	FromBitstream(syntaxElemEnum::lists_modification_present_flag);
	FromBitstream(syntaxElemEnum::log2_parallel_merge_level_minus2);
	FromBitstream(syntaxElemEnum::slice_segment_header_extension_present_flag);
	FromBitstream(syntaxElemEnum::pps_extension_present_flag);

	streamObject->GetBit(); // Read the RBSP stop bit
	streamObject->ByteAlign(); // Reat trailing bits
}

void decoder::EntropyController::DecodeVUI()
{
	int aspectRatioInfoPresent = FromBitstream(syntaxElemEnum::aspect_ratio_idc);
	if (aspectRatioInfoPresent == 1)
	{
		//TODO 
	}

	int overscanInfoPresentFlag = FromBitstream(syntaxElemEnum::overscan_info_present_flag);
	if (overscanInfoPresentFlag == 1)
	{
		//TODO
	}
	int videoSignalTypePresent = FromBitstream(syntaxElemEnum::video_signal_type_present_flag);
	if (videoSignalTypePresent == 1)
	{
		//TODO
	}
	int chromaLocInfoPresent = FromBitstream(syntaxElemEnum::chroma_loc_info_present_flag);
	if (chromaLocInfoPresent == 1)
	{
		//TODO
	}
	int neutralChromaIndication = FromBitstream(syntaxElemEnum::neutral_chroma_indication_flag);
	int fieldSeqFlag = FromBitstream(syntaxElemEnum::field_seq_flag);
	int frameFieldInfoPresent = FromBitstream(syntaxElemEnum::frame_field_info_present_flag);
	int defaultDisplayWndow = FromBitstream(syntaxElemEnum::default_display_window_flag);
	if (defaultDisplayWndow == 1)
	{
		//TODO
	}
	int vuiTimingInfoPresent = FromBitstream(syntaxElemEnum::vui_timing_info_present_flag);
	if (vuiTimingInfoPresent == 1)
	{
		int tick = FromBitstream(syntaxElemEnum::vui_num_units_in_tick);
		int scale = FromBitstream(syntaxElemEnum::vui_time_scale);

		decCtx->frameRate = scale / tick;

		int vuiPocProportionalToTiming = FromBitstream(syntaxElemEnum::vui_poc_proportional_to_timing_flag);
		if (vuiPocProportionalToTiming == 1)
			FromBitstream(syntaxElemEnum::vui_num_ticks_poc_diff_one_minus1);

		int vuiHrdPresent = FromBitstream(syntaxElemEnum::vui_hrd_parameters_present_flag);
		if (vuiHrdPresent == 1)
		{
			//TODO DecodeHrdParameters()
		}
		FromBitstream(syntaxElemEnum::bitstream_restriction_flag);

	}
	int bitstreamRestricfion = FromBitstream(syntaxElemEnum::bitstream_restriction_flag);
	if (bitstreamRestricfion == 1)
	{

	}

}

void decoder::EntropyController::DecodeStRefPicSet(int numOfShortRefPicSets)
{
	//TODO this part should be refactored ST_Ref_Pic_Set is part of old decoder
	//ST_Ref_Pic_Set * st_ref_pic_set = new ST_Ref_Pic_Set[numOfShortRefPicSets]();
	int inter_ref_pic_set_prediction_flag_var = 0;
	if (numOfShortRefPicSets != 0)
	{
		inter_ref_pic_set_prediction_flag_var = FromBitstream(syntaxElemEnum::inter_ref_pic_set_prediction_flag);
	}

	if (inter_ref_pic_set_prediction_flag_var == 1)
	{
		if (inter_ref_pic_set_prediction_flag_var == decCtx->num_short_term_ref_pic_sets)
		{
			FromBitstream(syntaxElemEnum::delta_idx_minus1);
		}

		FromBitstream(syntaxElemEnum::delta_rps_sign);
		FromBitstream(syntaxElemEnum::abs_delta_rps_minus1);
		//more to do...
	}
	else
	{
		int num_negative_pics = FromBitstream(syntaxElemEnum::num_negative_pics);
		int num_positive_pics = FromBitstream(syntaxElemEnum::num_positive_pics);
		for (int j = 0; j < num_negative_pics; j++)
		{
			FromBitstream(syntaxElemEnum::delta_poc_s0_minus1);
			FromBitstream(syntaxElemEnum::used_by_curr_pic_s0_flag);
		}

		for (int j = 0; j < num_positive_pics; j++)
		{
			FromBitstream(syntaxElemEnum::delta_poc_s1_minus1);
			FromBitstream(syntaxElemEnum::used_by_curr_pic_s1_flag);
		}
	}

	//delete[] st_ref_pic_set;
}

void decoder::EntropyController::DecodeProfileTierLevel()
{
	FromBitstream(syntaxElemEnum::general_profile_space);
	FromBitstream(syntaxElemEnum::general_tier_flag);
	int general_profile_idc = FromBitstream(syntaxElemEnum::general_profile_idc);

	int general_profile_compatibility_flag[32]{ 0 };

	for (int j = 0; j < 32; j++)
	{
		general_profile_compatibility_flag[j] = FromBitstream(syntaxElemEnum::general_profile_compatibility_flag);
	}

	FromBitstream(syntaxElemEnum::general_progressive_source_flag);
	FromBitstream(syntaxElemEnum::general_interlaced_source_flag);
	FromBitstream(syntaxElemEnum::general_non_packed_constraint_flag);
	FromBitstream(syntaxElemEnum::general_frame_only_constraint_flag);

	if ((general_profile_idc >= 4 && general_profile_idc <= 7) ||
		general_profile_compatibility_flag[4] != 0 ||
		general_profile_compatibility_flag[5] != 0 ||
		general_profile_compatibility_flag[6] != 0 ||
		general_profile_compatibility_flag[7] != 0)
	{
		FromBitstream(syntaxElemEnum::general_max_12bit_constraint_flag);
		FromBitstream(syntaxElemEnum::general_max_10bit_constraint_flag);
		FromBitstream(syntaxElemEnum::general_max_8bit_constraint_flag);
		FromBitstream(syntaxElemEnum::general_max_422chroma_constraint_flag);
		FromBitstream(syntaxElemEnum::general_max_420chroma_constraint_flag);
		FromBitstream(syntaxElemEnum::general_max_monochrome_constraint_flag);
		FromBitstream(syntaxElemEnum::general_intra_constraint_flag);
		FromBitstream(syntaxElemEnum::general_one_picture_only_constraint_flag);
		FromBitstream(syntaxElemEnum::general_lower_bit_rate_constraint_flag);
		FromBitstream(syntaxElemEnum::general_reserved_zero_34bits);
	}
	else
	{
		FromBitstream(syntaxElemEnum::general_reserved_zero_43bits);
		if ((general_profile_idc >= 1 && general_profile_idc <= 5) ||
			general_profile_compatibility_flag[1] != 0 ||
			general_profile_compatibility_flag[2] != 0 ||
			general_profile_compatibility_flag[3] != 0 ||
			general_profile_compatibility_flag[4] != 0 ||
			general_profile_compatibility_flag[5] != 0)
			FromBitstream(syntaxElemEnum::general_inbld_flag);
		else
			FromBitstream(syntaxElemEnum::general_reserved_zero_bit);
	}

	FromBitstream(syntaxElemEnum::general_level_idc);

	for (int i = 0; i < decCtx->highestTid; i++)
	{
		FromBitstream(syntaxElemEnum::sub_layer_profile_present_flag);
		FromBitstream(syntaxElemEnum::sub_layer_level_present_flag);
	}
	if (decCtx->highestTid > 0)
	{
		for (int i = decCtx->highestTid; i < 8; i++)
		{
			FromBitstream(syntaxElemEnum::reserved_zero_2bits);
		}
	}
}

void decoder::EntropyController::DecodeSliceSegment()
{
	DecodeSliceSegmentHeader();
}

void decoder::EntropyController::DecodeSliceSegmentHeader()
{
	ctxMem.initType = 0;
	decCtx->initType = 0;
	FromBitstream(syntaxElemEnum::first_slice_segment_in_pic_flag);

	if (decCtx->nalType >= BLA_W__LP && decCtx->nalType <= RSV_IRAP_VCL22)
		FromBitstream(syntaxElemEnum::no_output_of_prior_pics_flag);

	FromBitstream(syntaxElemEnum::slice_pic_parameter_set_id);

	// This part depends on dependent_slice_segment_flag, but that flag is not in bitstream in out case 
	int dependent_slice_segment_flag = 0;

	if (dependent_slice_segment_flag == 0)
	{
		int slice_type = FromBitstream(syntaxElemEnum::slice_type);
		ctxMem.sliceType = (SliceType)slice_type;

		if (decCtx->nalType != IDR_W_RADL && decCtx->nalType != IDR_N_LP)
		{
			FromBitstream(syntaxElemEnum::slice_pic_order_cnt_lsb);
			int shortTermPresentInSPS = FromBitstream(syntaxElemEnum::short_term_ref_pic_set_sps_flag);

			if (shortTermPresentInSPS == 0)
			{
				DecodeStRefPicSet(decCtx->num_short_term_ref_pic_sets);
			}
			else if (decCtx->num_short_term_ref_pic_sets > 1)
				FromBitstream(syntaxElemEnum::short_term_ref_pic_set_idx);

			if (ctxMem.sps_temporal_mvp_enabled_flag == 1)
				FromBitstream(syntaxElemEnum::slice_temporal_mvp_enabled_flag);

			if (slice_type == P || slice_type == B)
			{
				int numRefIdxActiveOverride = FromBitstream(syntaxElemEnum::num_ref_idx_active_override_flag);

				if (numRefIdxActiveOverride == 1)
					FromBitstream(syntaxElemEnum::num_ref_idx_l0_active_minus1);

				if (decCtx->cabac_init_present_flag == 1)
				{
					decCtx->cabac_init_flag = FromBitstream(syntaxElemEnum::cabac_init_flag) == 1;
					ctxMem.cabac_init_flag = decCtx->cabac_init_flag;

					if (slice_type == P && ctxMem.cabac_init_flag == 1)
					{
						decCtx->initType = 2;
						ctxMem.initType = 2;
					}

					else
					{
						ctxMem.initType = 1;
						decCtx->initType = 1;
					}

				}
				else
				{
					ctxMem.initType = 1;
					decCtx->initType = 1;
				}


				int MaxNumMergeCandidate = FromBitstream(syntaxElemEnum::five_minus_max_num_merge_cand) + 5;
			}
		}
	}
	probMods->InitializeSelection(decCtx->quantizationParameter, decCtx->cabac_init_flag);

	int slice_qp_delta = FromBitstream(syntaxElemEnum::slice_qp_delta);
	decCtx->quantizationParameter = decCtx->initQP + slice_qp_delta;

	if (decCtx->tilesEnabled)
	{

		int numEntryPoints = FromBitstream(syntaxElemEnum::num_entry_point_offsets);

		int numOfTiles = decCtx->numberOfTilesInColumn*decCtx->numberOfTilesInRow;

		//Number of tiles can change from frame to frame
		//TODO: Check if another approach, where we check if the number of tiles has been changed is better? 
		if (decCtx->tileOffsets != nullptr)
			delete decCtx->tileOffsets;

		decCtx->tileOffsets = new int[numOfTiles];
		decCtx->tileOffsets[0] = 0;

		int length = FromBitstream(syntaxElemEnum::offset_len_minus1);
		for (int i = 0; i < numOfTiles - 1; i++)
		{
			decCtx->tileOffsets[i + 1] = FromBitstream(syntaxElemEnum::entry_point_offset_minus1) + 1;
		}

	}

	streamObject->GetBit(); // Read the RBSP stop bit
	streamObject->ByteAlign(); // Reat trailing bits
}

void decoder::EntropyController::DecodeCTU(Partition *partition, CUContext * cuCtx, int i)
{
	ctxMem.pic_width_in_luma_samples = partition->enCtx->width;

	stack <int> *pathL = new stack <int>();
	stack <int> *pathA = new stack <int>();

	pathL->push(0);
	pathA->push(0);

	ctxMem.ctDepthL = cuCtx->calculateDepthLeft(pathL, false);

	if (ctxMem.ctDepthL == -1)
		ctxMem.availableL = 0;
	else
		ctxMem.availableL = 1;

	ctxMem.ctDepthA = cuCtx->calculateDepthAbove(pathA, false);

	if (ctxMem.ctDepthA == -1)
		ctxMem.availableA = 0;
	else
		ctxMem.availableA = 1;

	//entropyCtrl->ctxMem.availableL = ctuIndex % (enCtx->width / (1 << enCtx->ctbLog2SizeY)) == 0 ? 0 : 1;
	//entropyCtrl->ctxMem.availableA = ctuIndex / (enCtx->width / (1 << enCtx->ctbLog2SizeY)) == 0 ? 0 : 1;

	bool splitInf = false;

	int x = (i % partition->numberOfCTUsInRow) * decCtx->ctbSize;
	int y = (i / partition->numberOfCTUsInRow) * decCtx->ctbSize;

	if ((x + decCtx->ctbSize) > partition->width)
		splitInf = true;

	if ((y + decCtx->ctbSize) > partition->height)
		splitInf = true;

	delete pathL;
	delete pathA;

	partition->ctuTree[i] = new CU(decCtx->width, decCtx->height, partition->width, partition->height);
	partition->ctuTree[i]->ctuTree = partition->ctuTree;
	partition->currRow = i / partition->numberOfCTUsInRow;
	partition->currCol = i % partition->numberOfCTUsInRow;

	partition->currRowFrame = partition->startingTileRow + partition->currRow;
	partition->currColumnFrame = partition->startingTileColumn + partition->currCol;

	partition->y_pixel_iterator = partition->width*partition->currRow*decCtx->ctbSize + partition->currCol * decCtx->ctbSize;
	partition->u_pixel_iterator = partition->startingIndexU + (partition->width / 2)*partition->currRow*(decCtx->ctbSize / 2) + partition->currCol * (decCtx->ctbSize / 2);
	partition->v_pixel_iterator = partition->startingIndexV + (partition->width / 2)*partition->currRow*(decCtx->ctbSize / 2) + partition->currCol * (decCtx->ctbSize / 2);

	partition->y_pixel_iterator_frame = decCtx->width*partition->currRowFrame*decCtx->ctbSize + partition->currColumnFrame * decCtx->ctbSize;
	partition->u_pixel_iterator_frame = partition->startingIndexUFrame + (decCtx->width / 2)*partition->currRowFrame*(decCtx->ctbSize / 2) + partition->currColumnFrame * (decCtx->ctbSize / 2);
	partition->v_pixel_iterator_frame = partition->startingIndexVFrame + (decCtx->width / 2)*partition->currRowFrame*(decCtx->ctbSize / 2) + partition->currColumnFrame * (decCtx->ctbSize / 2);

	partition->ctuTree[i]->cbY = CB(partition->y_pixel_iterator, partition->y_pixel_iterator_frame, decCtx->ctbSize, decCtx->quantizationParameter, Luma);
	partition->ctuTree[i]->cbU = CB(partition->u_pixel_iterator, partition->u_pixel_iterator_frame, decCtx->ctbSize / 2, decCtx->quantizationParameter, ChromaCb);
	partition->ctuTree[i]->cbV = CB(partition->v_pixel_iterator, partition->v_pixel_iterator_frame, decCtx->ctbSize / 2, decCtx->quantizationParameter, ChromaCr);

	partition->ctuTree[i]->index = i;
	partition->ctuTree[i]->indexInFrame = (decCtx->width / decCtx->ctbSize)*partition->currRowFrame + partition->currColumnFrame;

	if (i != 0 && (i + 1) % partition->numberOfCTUsInRow == 0)
		partition->ctuTree[i]->isLastInRow = true;

	if (i > (partition->numberOfCTUs - 1) - partition->numberOfCTUsInRow)
		partition->ctuTree[i]->isLastInColumn = true;

	if ((partition->ctuTree[i]->isLastInRow && !partition->isWidthDividible) || (partition->ctuTree[i]->isLastInColumn && !partition->isHeightDividible))
		partition->ctuTree[i]->requiresFurtherSplitting = true;

	DecodeQuadtree(partition->ctuTree[i], 0, cuCtx, x, y, splitInf, partition->width, partition->height);

}

void decoder::EntropyController::DecodeQuadtree(CU *cu, int depth, CUContext * cuCtx, int x, int y, bool splitInf, int partitionWidth, int partitionHeight)
{

	int blockSize = cu->cbY.blockSize;
	int lumaQP = cu->cbY.cbQP;
	int frameWidth = decCtx->width;
	int frameHeight = decCtx->height;


	ctxMem.cbSize = decCtx->ctbSize >> depth;
	ctxMem.nPbW = decCtx->ctbSize >> depth;
	ctxMem.nPbH = decCtx->ctbSize >> depth;

	long long numberOfBitsBefore = streamObject->paramNumberOfBits;
	ctxMem.cqtDepth = depth;
	cu->depth = depth;

	if (!splitInf && ctxMem.cbSize != 8)
		cu->isSplit = FromBitstream(syntaxElemEnum::split_cu_flag);
	else if (ctxMem.cbSize != 8)
		cu->isSplit = true;
	else
		cu->isSplit = false;

	if (cu->isSplit == true)
	{
		int newX = x;
		int newY = y;
		bool splitInf = false;
		CUContext* cqtGen;
		cu->hasChildren = true;

		for (int i = 0; i < 4; ++i)
		{
			cu->children[i] = new CU(cu->frameWidth, cu->frameHeight, cu->partitionWidth, cu->partitionHeight);
			cu->children[i]->ctuTree = cu->ctuTree;
			int lumaPixelOffset = (i / 2)* (blockSize / 2 * partitionWidth) + (i % 2)*blockSize / 2;
			int chromaPixelOffset = (i / 2)*(blockSize / 4 * partitionWidth / 2) + (i % 2)*blockSize / 4;

			int lumaPixelOffsetFrame = (i / 2)* (blockSize / 2 * frameWidth) + (i % 2)*blockSize / 2;
			int chromaPixelOffsetFrame = (i / 2)*(blockSize / 4 * frameWidth / 2) + (i % 2)*blockSize / 4;

			cu->children[i]->cbY = CB(cu->cbY.startingIndex + lumaPixelOffset, cu->cbY.startingIndexInFrame + lumaPixelOffsetFrame, blockSize / 2, lumaQP, Luma);
			cu->children[i]->cbU = CB(cu->cbU.startingIndex + chromaPixelOffset, cu->cbU.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCb);
			cu->children[i]->cbV = CB(cu->cbV.startingIndex + chromaPixelOffset, cu->cbV.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCr);

			cu->children[i]->depth = cu->depth + 1;
			cu->children[i]->parent = cu;
			cu->children[i]->hasParent = true;
			cu->children[i]->index = i;
		}

		bool isWidthDividible = partitionWidth % (cu->cbY.blockSize / 2) == 0;
		bool isHeightDividible = partitionHeight % (cu->cbY.blockSize / 2) == 0;

		if (cu->isLastInRow)
		{
			int rightChildStartingIndex = cu->children[1]->cbY.startingIndex;
			int rightChildBlockSize = cu->children[1]->cbY.blockSize;
			bool areAllChildrenInFrame = rightChildStartingIndex % partitionWidth >= partitionWidth - rightChildBlockSize;

			if (!areAllChildrenInFrame)
			{
				cu->children[1]->isInFrame = false;
				cu->children[3]->isInFrame = false;
				cu->children[0]->isLastInRow = true;
				cu->children[2]->isLastInRow = true;

				if (!isWidthDividible)
				{
					cu->children[0]->requiresFurtherSplitting = true;
					cu->children[2]->requiresFurtherSplitting = true;
				}
			}
			else
			{
				cu->children[1]->isLastInRow = true;
				cu->children[3]->isLastInRow = true;

				if (!isWidthDividible)
				{
					cu->children[1]->requiresFurtherSplitting = true;
					cu->children[3]->requiresFurtherSplitting = true;
				}
			}
		}

		if (cu->isLastInColumn)
		{
			int LowerChildStartingIndex = cu->children[2]->cbY.startingIndex;
			bool areAllChildrenInFrame = LowerChildStartingIndex < partitionHeight * partitionWidth;

			if (!areAllChildrenInFrame)
			{
				cu->children[2]->isInFrame = false;
				cu->children[3]->isInFrame = false;
				cu->children[0]->isLastInColumn = true;
				cu->children[1]->isLastInColumn = true;

				if (!isHeightDividible)
				{
					cu->children[0]->requiresFurtherSplitting = true;
					cu->children[1]->requiresFurtherSplitting = true;
				}
			}
			else
			{
				cu->children[2]->isLastInColumn = true;
				cu->children[3]->isLastInColumn = true;

				if (!isHeightDividible)
				{
					cu->children[2]->requiresFurtherSplitting = true;
					cu->children[3]->requiresFurtherSplitting = true;
				}
			}
		}

		if ((newX < partitionWidth) && (newY < partitionHeight))
		{

			stack <int> *pathL = new stack <int>();
			stack <int> *pathA = new stack <int>();

			cqtGen = new CUContext(nullptr, nullptr, cuCtx, depth + 1, 0);

			cuCtx->cqt[0] = cqtGen;

			ctxMem.ctDepthL = cqtGen->calculateDepthLeft(pathL, false);

			if (ctxMem.ctDepthL == -1)
				ctxMem.availableL = 0;
			else
				ctxMem.availableL = 1;


			ctxMem.ctDepthA = cqtGen->calculateDepthAbove(pathA, false);

			if (ctxMem.ctDepthA == -1)
				ctxMem.availableA = 0;
			else
				ctxMem.availableA = 1;

			if ((newX + (decCtx->ctbSize >> (depth + 1))) > partitionWidth)
				splitInf = true;

			if ((newY + (decCtx->ctbSize >> (depth + 1))) > partitionHeight)
				splitInf = true;

			DecodeQuadtree(cu->children[0], depth + 1, cqtGen, newX, newY, splitInf, partitionWidth, partitionHeight);

			delete pathA;
			delete pathL;


			//entropyCtrl->ctxMem.availableL = 1;
		}
		else
		{
			cu->children[0]->isInFrame = false;
		}

		newX = x + (decCtx->ctbSize >> (depth + 1));
		newY = y;

		if ((newX < partitionWidth) && (newY < partitionHeight))
		{

			stack <int> *pathL = new stack <int>();
			stack <int> *pathA = new stack <int>();

			splitInf = false;
			cqtGen = new CUContext(nullptr, nullptr, cuCtx, depth + 1, 1);

			cuCtx->cqt[1] = cqtGen;

			ctxMem.ctDepthL = cqtGen->calculateDepthLeft(pathL, false);

			if (ctxMem.ctDepthL == -1)
				ctxMem.availableL = 0;
			else
				ctxMem.availableL = 1;

			ctxMem.ctDepthA = cqtGen->calculateDepthAbove(pathA, false);

			if (ctxMem.ctDepthA == -1)
				ctxMem.availableA = 0;
			else
				ctxMem.availableA = 1;

			if ((newX + (decCtx->ctbSize >> (depth + 1))) > partitionWidth)
				splitInf = true;

			if ((newY + (decCtx->ctbSize >> (depth + 1))) > partitionHeight)
				splitInf = true;

			DecodeQuadtree(cu->children[1], depth + 1, cqtGen, newX, newY, splitInf, partitionWidth, partitionHeight);

			delete pathA;
			delete pathL;

			//entropyCtrl->ctxMem.availableL = avlLTmp;
			//entropyCtrl->ctxMem.availableA = 1;
		}
		else
		{
			cu->children[1]->isInFrame = false;
		}

		newX = x;
		newY = y + (decCtx->ctbSize >> (depth + 1));

		if ((newX < partitionWidth) && (newY < partitionHeight))
		{
			stack <int> *pathL = new stack <int>();
			stack <int> *pathA = new stack <int>();

			splitInf = false;
			cqtGen = new CUContext(nullptr, nullptr, cuCtx, depth + 1, 2);

			cuCtx->cqt[2] = cqtGen;
			ctxMem.ctDepthL = cqtGen->calculateDepthLeft(pathL, false);

			if (ctxMem.ctDepthL == -1)
				ctxMem.availableL = 0;
			else
				ctxMem.availableL = 1;

			ctxMem.ctDepthA = cqtGen->calculateDepthAbove(pathA, false);

			if (ctxMem.ctDepthA == -1)
				ctxMem.availableA = 0;
			else
				ctxMem.availableA = 1;

			if ((newX + (decCtx->ctbSize >> (depth + 1))) > partitionWidth)
				splitInf = true;

			if ((newY + (decCtx->ctbSize >> (depth + 1))) > partitionHeight)
				splitInf = true;

			DecodeQuadtree(cu->children[2], depth + 1, cqtGen, newX, newY, splitInf, partitionWidth, partitionHeight);

			delete pathA;
			delete pathL;

		}
		else
		{
			cu->children[2]->isInFrame = false;
		}
		//entropyCtrl->ctxMem.availableL = 1;

		newX = x + (decCtx->ctbSize >> (depth + 1));
		newY = y + (decCtx->ctbSize >> (depth + 1));

		if ((newX < partitionWidth) && (newY < partitionHeight))
		{
			splitInf = false;
			cqtGen = new CUContext(nullptr, nullptr, cuCtx, depth + 1, 3);

			stack <int> *pathL = new stack <int>();
			stack <int> *pathA = new stack <int>();

			cuCtx->cqt[3] = cqtGen;

			ctxMem.ctDepthL = cqtGen->calculateDepthLeft(pathL, false);

			if (ctxMem.ctDepthL == -1)
				ctxMem.availableL = 0;
			else
				ctxMem.availableL = 1;


			ctxMem.ctDepthA = cqtGen->calculateDepthAbove(pathA, false);

			if (ctxMem.ctDepthA == -1)
				ctxMem.availableA = 0;
			else
				ctxMem.availableA = 1;

			if ((newX + (decCtx->ctbSize >> (depth + 1))) > partitionWidth)
				splitInf = true;

			if ((newY + (decCtx->ctbSize >> (depth + 1))) > partitionHeight)
				splitInf = true;

			DecodeQuadtree(cu->children[3], depth + 1, cqtGen, newX, newY, splitInf, partitionWidth, partitionHeight);

			delete pathA;
			delete pathL;

			//entropyCtrl->ctxMem.availableL = avlLTmp;
			//entropyCtrl->ctxMem.availableA = avlATmp;
		}
		else
		{
			cu->children[3]->isInFrame = false;
		}
	}
	else
	{
		cu->hasChildren = false;
		DecodeCU(cu, cuCtx, partitionWidth);
	}

	cu->numberOfBits = (int)(streamObject->paramNumberOfBits - numberOfBitsBefore);
}

void decoder::EntropyController::DecodeCU(CU * cu, CUContext * cuCtx, int partitionWidth)
{
	/*stack <int> *pathL = new stack <int>();
	stack <int> *pathA = new stack <int>();

	ctxMem.left_cu_skip_flag = cuCtx->calculateCuSkipLeft(pathL, false);
	ctxMem.above_cu_skip_flag = cuCtx->calculateCuSkipAbove(pathA, false);

	delete pathL;
	delete pathA;*/

	//cout << cu->getCUPathID() << endl;

	ctxMem.availableL = cu->isLeftCodingUnitAvailable(cu->partitionWidth);
	ctxMem.availableA = cu->isAboveCodingUnitAvailable(cu->partitionWidth);

	if (ctxMem.availableL)
		ctxMem.left_cu_skip_flag = cu->getLeftCodingUnit()->CuSkipFlag;
	if (ctxMem.availableA)
		ctxMem.above_cu_skip_flag = cu->getAboveCodingUnit()->CuSkipFlag;

	if (ctxMem.sliceType != I)
		cu->CuSkipFlag = FromBitstream(syntaxElemEnum::cu_skip_flag);

	ctxMem.cu_skip_flag = cu->CuSkipFlag;
	cuCtx->cu_skip_flag = cu->CuSkipFlag;

	if (cu->CuSkipFlag == true)
	{
		//In skip mode there is no transform at all
		cu->CreatePredictionUnits(ctxMem.cbSize);
		cu->CuPredMode = MODE_INTER;

		//Just to set default values
		//TODO: Refactor this, since transform tree is not needed in Skip mode
		cu->InitializeTransformTree(ctxMem.cbSize);

		DecodePU(&cu->predictionUnits[0]);
	}
	else
	{
		if (ctxMem.sliceType != I)
			cu->PredModeFlag = FromBitstream(syntaxElemEnum::pred_mode_flag);
		else
		{
			cu->PredModeFlag = 1;
			ctxMem.pred_mode_flag = 1;
		}


		cu->CuPredMode = cu->PredModeFlag == 0 ? MODE_INTER : MODE_INTRA;

		if (cu->PredModeFlag != MODE_INTRA || ctxMem.cbSize == 8)
			cu->PartMode = FromBitstream(syntaxElemEnum::part_mode);


		if (cu->CuPredMode == MODE_INTRA)
		{
			if (cu->PartMode == PART_2Nx2N)
			{
				cu->PrevIntraLumaPredFlag[0] = FromBitstream(syntaxElemEnum::prev_intra_luma_pred_flag);

				if (cu->PrevIntraLumaPredFlag[0] == true)
					cu->MpmIdx[0] = FromBitstream(syntaxElemEnum::mpm_idx);
				else
					cu->RemIntraLumaPredMode[0] = FromBitstream(syntaxElemEnum::rem_intra_luma_pred_mode);
			}
			else // PART NxN
			{
				for (int i = 0; i < 4; ++i)
				{
					cu->PrevIntraLumaPredFlag[i] = FromBitstream(syntaxElemEnum::prev_intra_luma_pred_flag);
				}
				for (int i = 0; i < 4; ++i)
				{
					if (cu->PrevIntraLumaPredFlag[i] == true)
						cu->MpmIdx[i] = FromBitstream(syntaxElemEnum::mpm_idx);
					else
						cu->RemIntraLumaPredMode[i] = FromBitstream(syntaxElemEnum::rem_intra_luma_pred_mode);
				}
			}

			cu->IntraChromaPredMode = FromBitstream(syntaxElemEnum::intra_chroma_pred_mode);

			cu->decodeIntraPredDecision(ctxMem.pic_width_in_luma_samples);

			ctxMem.predMode = cu->IntraPredModeY[0];

		}
		else
		{
			cu->CreatePredictionUnits(decCtx->width);
			if (cu->PartMode == PartitionModeInter::PART_2Nx2N)
			{
				DecodePU(&cu->predictionUnits[0]);
			}
			else if (cu->PartMode == PartitionModeInter::PART_2NxN)
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
			}
			else if (cu->PartMode == PartitionModeInter::PART_Nx2N)
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
			}
			else if (cu->PartMode == PartitionModeInter::PART_2NxnU)
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
			}
			else if (cu->PartMode == PartitionModeInter::PART_2NxnD)
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
			}
			else if (cu->PartMode == PartitionModeInter::PART_nLx2N)
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
			}
			else if (cu->PartMode == PartitionModeInter::PART_nRx2N)
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
			}
			else // PART NxN
			{
				DecodePU(&cu->predictionUnits[0]);
				DecodePU(&cu->predictionUnits[1]);
				DecodePU(&cu->predictionUnits[2]);
				DecodePU(&cu->predictionUnits[3]);
			}
		}

		if (cu->CuPredMode != MODE_INTRA && !(cu->PartMode == PART_2Nx2N && ctxMem.merge_flag == 1))
			ctxMem.rqt_root_cbf = FromBitstream(syntaxElemEnum::rqt_root_cbf);
		else
			ctxMem.rqt_root_cbf = 1;

		ctxMem.CuPredMode = cu->PredModeFlag;

		cu->InitializeTransformTree(ctxMem.cbSize);
		cuCtx->intraSplitFlag = cu->getIntraSplitFlag();

		if (ctxMem.rqt_root_cbf == 1)
		{
			//if (cu->CuPredMode == MODE_INTRA)
			//{
			//	stack <int> *pathL = new stack <int>();
			//	int leftPredMode = cuCtx->getPredModeLeft(pathL, false);
			//	delete pathL;

			//	stack <int> *pathA = new stack <int>();
			//	int abovePredMode = cuCtx->getPredModeAbove(pathA, false);
			//	delete pathA;

			//	bool isCuOnUpperEdge = cuCtx->isCUUpperEdgeOfCTU();

			//	//LDOC TO DO
			//	ctxMem.predMode = cuCtx->getIntraPredictionModeLuma(cu->PrevIntraLumaPredFlag[0], cu->MpmIdx[0], cu->RemIntraLumaPredMode[0], leftPredMode, abovePredMode, isCuOnUpperEdge);
//	ctxMem.predModeChroma = cuCtx->getIntraPredictionModeChroma(cu->IntraChromaPredMode, ctxMem.predMode);
//	cuCtx->predMode = ctxMem.predMode;
//	cu->IntraPredModeY[0] = ctxMem.predMode;
//}
			DecodeTransformTree(cu->transformTree, cuCtx, cu, partitionWidth);
		}
		else
		{
			cu->transformTree->tbY.isZeroMatrix = true;
			cu->transformTree->tbU.isZeroMatrix = true;
			cu->transformTree->tbV.isZeroMatrix = true;
		}
	}
}

void decoder::EntropyController::DecodePU(PU *pu)
{
	if (ctxMem.cu_skip_flag == 1)
	{
		// TODO: Check if maximal number of merge candidates is greater than 1
		pu->candidateIndex = FromBitstream(syntaxElemEnum::merge_idx);
		pu->predictionMode = Skip;
	}
	else
	{
		int merge_flag = FromBitstream(syntaxElemEnum::merge_flag);
		ctxMem.merge_flag = merge_flag;


		if (merge_flag == 1)
		{
			// TODO: Check if maximal number of merge candidates is greater than 1
			if (ctxMem.cu_skip_flag)
				pu->predictionMode = Skip;
			else
			{
				pu->predictionMode = Merge;
				pu->candidateIndex = FromBitstream(syntaxElemEnum::merge_idx);
			}
		}
		else
		{
			pu->predictionMode = AMVP;

			if (ctxMem.num_ref_idx_l0_active_minus1 > 0)
				FromBitstream(syntaxElemEnum::ref_idx_l0);

			DecodeMVD(pu);

			pu->candidateIndex = FromBitstream(syntaxElemEnum::mvp_l0_flag);
		}
	}
}

void decoder::EntropyController::DecodeMVD(PU * pu)
{
	pu->MDabsx1 = 0;
	pu->MDabsy1 = 0;
	pu->MDsignx1 = 0;
	pu->MDsigny1 = 0;

	pu->MDabsx1 += FromBitstream(syntaxElemEnum::abs_mvd_greater0_flag);
	pu->MDabsy1 += FromBitstream(syntaxElemEnum::abs_mvd_greater0_flag);

	if (pu->MDabsx1 == 1)
		pu->MDabsx1 += FromBitstream(syntaxElemEnum::abs_mvd_greater1_flag);
	if (pu->MDabsy1 == 1)
		pu->MDabsy1 += FromBitstream(syntaxElemEnum::abs_mvd_greater1_flag);

	if (pu->MDabsx1 > 0)
	{
		if (pu->MDabsx1 == 2)
			pu->MDabsx1 += FromBitstream(syntaxElemEnum::abs_mvd_minus2);

		pu->MDsignx1 = -FromBitstream(syntaxElemEnum::mvd_sign_flag);
	}

	if (pu->MDabsy1 > 0)
	{
		if (pu->MDabsy1 == 2)
			pu->MDabsy1 += FromBitstream(syntaxElemEnum::abs_mvd_minus2);

		pu->MDsigny1 = -FromBitstream(syntaxElemEnum::mvd_sign_flag);
	}
	int c = 0;
}

void decoder::EntropyController::DecodeTransformTree(TU * tu, CUContext * cuCtx, CU *cu, int partitionWidth)
{
	bool split_transform_flag = false;

	if (ctxMem.cbSize == 64 && tu->trafoDepth == 0)
		split_transform_flag = true;
	else if (cuCtx->intraSplitFlag && tu->trafoDepth == 0)
		split_transform_flag = true;
	else if (cu->getPartOfInterSplitFlag() && tu->trafoDepth == 0 && decCtx->max_transform_hierarchy_depth_inter == 0)
		split_transform_flag = true;

	int ChromaArrayType = 1;

	if ((tu->tbY.transformBlockSize > 4 && ChromaArrayType != 0) || ChromaArrayType == 3)
	{
		if (tu->trafoDepth == 0 || (tu->parent && !tu->parent->tbU.isZeroMatrix))
		{
			ctxMem.trafoDepth = tu->trafoDepth;
			tu->tbU.isZeroMatrix = FromBitstream(syntaxElemEnum::cbf_cb) == 0; // if cbf_cb is equal 1 then isZeroMatrix is 0;

			if (ChromaArrayType == 2 && (!split_transform_flag || tu->tbY.transformBlockSize == 8))
			{
				// kad budemo imali chroma array type 2 imat �emo i ovo.
			}
		}

		if (tu->trafoDepth == 0 || (tu->parent && !tu->parent->tbV.isZeroMatrix))
		{
			ctxMem.trafoDepth = tu->trafoDepth;
			tu->tbV.isZeroMatrix = FromBitstream(syntaxElemEnum::cbf_cr) == 0; // if cbf_ce is equal 1 then isZeroMatrix is 0;

			if (ChromaArrayType == 2 && (!split_transform_flag || tu->tbY.transformBlockSize == 8))
			{
				// kad budemo imali chroma array type 2 imat �emo i ovo.
			}
		}
	}

	if (split_transform_flag)
	{
		tu->splitTU(partitionWidth, ctxMem.pic_width_in_luma_samples);

		DecodeTransformTree(tu->children[0], cuCtx, cu, partitionWidth);
		DecodeTransformTree(tu->children[1], cuCtx, cu, partitionWidth);
		DecodeTransformTree(tu->children[2], cuCtx, cu, partitionWidth);
		DecodeTransformTree(tu->children[3], cuCtx, cu, partitionWidth);

		//If TU is 4x4, chroma blocks are 2x2 which is invalid...Now decode TB for chroma
		if (tu->tbY.transformBlockSize == 8)
		{

			if (!tu->tbU.isZeroMatrix)
			{
				ctxMem.predMode = cu->IntraPredModeChroma;
				ctxMem.colorIdx = 1;
				ctxMem.log2TrafoSize = ComUtil::logarithm2(tu->tbU.transformBlockSize);

				CUContext::refreshScanIndex(&ctxMem);
				DecodeTB(&tu->tbU, cuCtx);
			}

			if (!tu->tbV.isZeroMatrix)
			{
				ctxMem.predMode = cu->IntraPredModeChroma;
				ctxMem.colorIdx = 2;
				ctxMem.log2TrafoSize = ComUtil::logarithm2(tu->tbV.transformBlockSize);

				CUContext::refreshScanIndex(&ctxMem);
				DecodeTB(&tu->tbV, cuCtx);
			}
		}
	}
	else
	{
		if (ctxMem.CuPredMode == MODE_INTRA || !tu->tbU.isZeroMatrix || !tu->tbV.isZeroMatrix || tu->trafoDepth != 0)
		{
			ctxMem.trafoDepth = tu->trafoDepth;
			tu->tbY.isZeroMatrix = FromBitstream(syntaxElemEnum::cbf_luma) == 0;
		}
		else
		{
			tu->tbY.isZeroMatrix = false;
		}


		ctxMem.cbf_luma = !tu->tbY.isZeroMatrix;
		ctxMem.cbf_cb = !tu->tbU.isZeroMatrix;
		ctxMem.cbf_cr = !tu->tbV.isZeroMatrix;

		DecodeTU(cuCtx, tu, cu);

	}
	//ctxMem.trafoDepth = tu->trafoDepth;

	//int parent_cbf_luma = 1, parent_cbf_cb = 1, parent_cbf_cr = 1;
	//int cbf_luma = 1, cbf_cb = 1, cbf_cr = 1;

	////If depth of transform tree is larger then 0 and parent TU has cbf_cb equal to 0, then cbf_cb for child is ommited
	//if (ctxMem.trafoDepth > 0 && ctxMem.parent_cbf_cb == 0)
	//	cbf_cb = 0;
	//else
	//	cbf_cb = FromBitstream(syntaxElemEnum::cbf_cb);

	//if (ctxMem.trafoDepth > 0 && ctxMem.parent_cbf_cr == 0)
	//	cbf_cr = 0;
	//else
	//	cbf_cr = FromBitstream(syntaxElemEnum::cbf_cr);


	//if (split_transform_flag)
	//{
	//	ctxMem.parent_cbf_luma = cbf_luma;
	//	ctxMem.parent_cbf_cr = cbf_cr;
	//	ctxMem.parent_cbf_cb = cbf_cb;

	//	DecodeTransformTree(tu->children[0], cuCtx);
	//	DecodeTransformTree(tu->children[1], cuCtx);
	//	DecodeTransformTree(tu->children[2], cuCtx);
	//	DecodeTransformTree(tu->children[3], cuCtx);
	/*}
	else
	{
		if (ctxMem.CuPredMode == MODE_INTRA || (cbf_cb == 1 || cbf_cr == 1) || tu->trafoDepth != 0)
			cbf_luma = FromBitstream(syntaxElemEnum::cbf_luma);

		ctxMem.cbf_luma = cbf_luma;
		ctxMem.cbf_cr = cbf_cr;
		ctxMem.cbf_cb = cbf_cb;

		DecodeTU(cuCtx, tu);
	}*/
}

void decoder::EntropyController::DecodeTU(CUContext * cuCtx, TU * tu, CU *cu)
{
	int lumaLog2TrafoSize = decCtx->ctbLog2SizeY - cuCtx->cuDepth - tu->trafoDepth;

	/*tu->tbY.isZeroMatrix = true;
	tu->tbU.isZeroMatrix = true;
	tu->tbV.isZeroMatrix = true;

	tu->tbY.transformBlockSize = (1 << lumaLog2TrafoSize);
	tu->tbU.transformBlockSize = (1 << (lumaLog2TrafoSize - 1));
	tu->tbV.transformBlockSize = (1 << (lumaLog2TrafoSize - 1));*/


	if (!tu->tbY.isZeroMatrix)
	{
		ctxMem.colorIdx = 0;
		ctxMem.log2TrafoSize = lumaLog2TrafoSize;
		ctxMem.predMode = cu->IntraPredModeY[tu->index];

		CUContext::refreshScanIndex(&ctxMem);
		DecodeTB(&tu->tbY, cuCtx);
	}

	if (!tu->tbU.isZeroMatrix && tu->tbU.transformBlockSize > 2)
	{
		ctxMem.colorIdx = 1;
		ctxMem.log2TrafoSize = lumaLog2TrafoSize - 1;
		ctxMem.predMode = cu->IntraPredModeChroma;

		CUContext::refreshScanIndex(&ctxMem);
		DecodeTB(&tu->tbU, cuCtx);
	}

	if (!tu->tbV.isZeroMatrix && tu->tbV.transformBlockSize > 2)
	{
		ctxMem.colorIdx = 2;
		ctxMem.log2TrafoSize = lumaLog2TrafoSize - 1;
		ctxMem.predMode = cu->IntraPredModeChroma;

		CUContext::refreshScanIndex(&ctxMem);
		DecodeTB(&tu->tbV, cuCtx);
	}
}

void decoder::EntropyController::DecodeTB(TB * tb, CUContext * cuCtx)
{
	//if it comes to this point it is not all null!!
	int lastSigCoeffX, lastSigCoeffY;
	tb->isZeroMatrix = false;
	tb->intraPredMode = ctxMem.predMode;

	int ctx_indexOfSigSubBlock = 0;

	int last_sig_coeff_x_prefix = 0;
	int last_sig_coeff_y_prefix = 0;
	int last_sig_coeff_x_suffix = 0;
	int last_sig_coeff_y_suffix = 0;

	last_sig_coeff_x_prefix = FromBitstream(syntaxElemEnum::last_sig_coeff_x_prefix);
	last_sig_coeff_y_prefix = FromBitstream(syntaxElemEnum::last_sig_coeff_y_prefix);

	if (last_sig_coeff_x_prefix > 3)
		last_sig_coeff_x_suffix = FromBitstream(syntaxElemEnum::last_sig_coeff_x_suffix);

	if (last_sig_coeff_y_prefix > 3)
		last_sig_coeff_y_suffix = FromBitstream(syntaxElemEnum::last_sig_coeff_y_suffix);

	int sizeOfSubblockMatrix = (1 << ctxMem.log2TrafoSize) / 4;
	tb->numOfSubblocks = sizeOfSubblockMatrix * sizeOfSubblockMatrix;


	ctxMem.maxScanIdx = tb->numOfSubblocks;

	tb->subBlocks = new TBSubblock[tb->numOfSubblocks];

	Scanner::LastSigCoeffInvBin(lastSigCoeffX, last_sig_coeff_x_prefix, last_sig_coeff_x_suffix);
	Scanner::LastSigCoeffInvBin(lastSigCoeffY, last_sig_coeff_y_prefix, last_sig_coeff_y_suffix);

	tb->lastSigCoeffX = lastSigCoeffX;
	tb->lastSigCoeffY = lastSigCoeffY;

	if (ctxMem.scanIdx == VerticalScan)
	{
		int tmp = lastSigCoeffX;
		lastSigCoeffX = lastSigCoeffY;
		lastSigCoeffY = tmp;
	}

	int subBlockIndex = 0;
	int startScanIndex = 0;

	ctxMem.subBlockCoordinateX = lastSigCoeffX / 4;
	ctxMem.subBlockCoordinateY = lastSigCoeffY / 4;

	if (sizeOfSubblockMatrix == 8)
		Scanner::GetScanIndexFromCoordinates8x8(subBlockIndex, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
	else if (sizeOfSubblockMatrix == 4)
		Scanner::GetScanIndexFromCoordinates4x4(subBlockIndex, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
	else if (sizeOfSubblockMatrix == 2)
		Scanner::GetScanIndexFromCoordinates2x2(subBlockIndex, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
	else
	{
		subBlockIndex = 0;
	}

	Scanner::GetScanIndexFromCoordinates4x4(startScanIndex, lastSigCoeffX % 4, lastSigCoeffY % 4, ctxMem.scanIdx);

	bool firstSubblock = true;
	bool firstPixel = true;
	bool firstPixelSign = true;

	ctxMem.first_time_in_TB = 1;

	for (int i = subBlockIndex; i < tb->numOfSubblocks; i++)
	{
		ctxMem.sub_block_index = i;

		int startingIndex = 0;
		int sigCoeffIndex = 0;

		int iR = 0;
		int iB = 0;

		if (sizeOfSubblockMatrix == 8)
			Scanner::GetCoordinatesInBlock8x8(i, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
		else if (sizeOfSubblockMatrix == 4)
			Scanner::GetCoordinatesInBlock4x4(i, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
		else if (sizeOfSubblockMatrix == 2)
			Scanner::GetCoordinatesInBlock2x2(i, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
		else
		{
			ctxMem.subBlockCoordinateX = 0;
			ctxMem.subBlockCoordinateY = 0;
		}

		if (sizeOfSubblockMatrix == 8)
		{
			Scanner::GetScanIndexFromCoordinates8x8(iR, ctxMem.subBlockCoordinateX + 1, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
			Scanner::GetScanIndexFromCoordinates8x8(iB, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY + 1, ctxMem.scanIdx);
		}
		else if (sizeOfSubblockMatrix == 4)
		{
			Scanner::GetScanIndexFromCoordinates4x4(iR, ctxMem.subBlockCoordinateX + 1, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
			Scanner::GetScanIndexFromCoordinates4x4(iB, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY + 1, ctxMem.scanIdx);
		}
		else if (sizeOfSubblockMatrix == 2)
		{
			Scanner::GetScanIndexFromCoordinates2x2(iR, ctxMem.subBlockCoordinateX + 1, ctxMem.subBlockCoordinateY, ctxMem.scanIdx);
			Scanner::GetScanIndexFromCoordinates2x2(iB, ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY + 1, ctxMem.scanIdx);
		}
		else
		{
			iR = 0;
			iB = 0;
		}


		if ((ctxMem.subBlockCoordinateX == sizeOfSubblockMatrix) || (ctxMem.subBlockCoordinateY == sizeOfSubblockMatrix))
			ctxMem.isBorderSubBlock = true;
		else
		{
			ctxMem.isBorderSubBlock = false;

			if (iR != -1)
				ctxMem.subBlockRightCSBF = tb->subBlocks[iR].codedSubBlockFlag != 1 ? 0 : 1;

			if (iB != -1)
				ctxMem.subBlockBottomCSBF = tb->subBlocks[iB].codedSubBlockFlag != 1 ? 0 : 1;
		}

		if (i > subBlockIndex && i < tb->numOfSubblocks - 1)
		{
			tb->subBlocks[i].codedSubBlockFlag = FromBitstream(syntaxElemEnum::coded_sub_block_flag);
		}
		else if (i == tb->numOfSubblocks - 1 || i == subBlockIndex)
		{
			tb->subBlocks[i].codedSubBlockFlag = 1;
		}

		if (firstSubblock)
		{
			startingIndex = startScanIndex;
			sigCoeffIndex = startingIndex + 1;
			tb->subBlocks[i].SigCoeffFlag[startingIndex] = 1;
		}

		if (tb->subBlocks[i].codedSubBlockFlag == 1 || (i == tb->numOfSubblocks - 1 && ctx_indexOfSigSubBlock != tb->numOfSubblocks - 1))
		{
			bool allZero = true;

			for (int j = sigCoeffIndex; j < 16; j++)
			{
				int pom_sub_x = 0;
				int pom_sub_y = 0;
				Scanner::GetCoordinatesInBlock4x4(j, pom_sub_x, pom_sub_y, ctxMem.scanIdx);
				ctxMem.transformBlock_xC = pom_sub_x + (ctxMem.subBlockCoordinateX * 4);
				ctxMem.transformBlock_yC = pom_sub_y + (ctxMem.subBlockCoordinateY * 4);

				if (!(allZero && j == 15) || firstSubblock || (i == (tb->numOfSubblocks - 1)))
					tb->subBlocks[i].SigCoeffFlag[j] = FromBitstream(syntaxElemEnum::sig_coeff_flag);
				else if (allZero && j == 15)
					tb->subBlocks[i].SigCoeffFlag[j] = 1;

				if (tb->subBlocks[i].SigCoeffFlag[j] == 1)
					allZero = false;
			}

			if (firstSubblock)
				firstSubblock = false;

			int greater1Counter = 0;
			int greater1Index = 16;

			for (int j = startingIndex; j < 16; j++)
			{
				ctxMem.inside_sub_block_index = j;

				if (firstPixel)
				{
					firstPixel = false;
					tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] = FromBitstream(syntaxElemEnum::coeff_abs_level_greater1_flag);
					greater1Counter++;
					ctxMem.previous_ALG1_flag = tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j];
				}
				else if (tb->subBlocks[i].SigCoeffFlag[j] == 1)
				{
					tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] = FromBitstream(syntaxElemEnum::coeff_abs_level_greater1_flag);
					greater1Counter++;
					ctxMem.previous_ALG1_flag = tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j];
				}

				if (greater1Counter == 8)
				{
					greater1Index = j;
					break;
				}
			}

			int greater2Index = 16;

			for (int j = startingIndex; j < 16; j++)
			{
				if (tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] == 1 || (j > greater1Index && greater2Index < 16))
				{
					tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j] = FromBitstream(syntaxElemEnum::coeff_abs_level_greater2_flag);
					greater2Index = j;
					break;
				}
			}


			for (int j = startingIndex; j < 16; j++)
			{
				if (firstPixelSign)
				{
					firstPixelSign = false;
					tb->subBlocks[i].CoeffSignFlag[j] = FromBitstream(syntaxElemEnum::coeff_sign_flag);
				}
				else if (tb->subBlocks[i].SigCoeffFlag[j] == 1)
				{
					tb->subBlocks[i].CoeffSignFlag[j] = FromBitstream(syntaxElemEnum::coeff_sign_flag);
				}
			}

			ctxMem.first_time_invoke = true; // coeff_abs_level_remaining is binarized for first time in sub-block

			for (int j = startingIndex; j < 16; j++)
			{
				if (greater2Index < j || tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j] == 1 || (greater1Counter == 8 && greater2Index == 16))
				{
					if (tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] == 1 || (j > greater1Index && tb->subBlocks[i].SigCoeffFlag[j] == 1))
					{
						ctxMem.coeff_abs_level_greater1_flag = tb->subBlocks[i].CoeffAbsLevelGreater1Flag[j] != 1 ? 0 : 1;
						ctxMem.coeff_abs_level_greater2_flag = tb->subBlocks[i].CoeffAbsLevelGreater2Flag[j] != 1 ? 0 : 1;
						tb->subBlocks[i].CoeffAbsLevelRemaining[j] = FromBitstream(syntaxElemEnum::coeff_abs_level_remaining);
					}
				}
			}
		}
	}

	tb->QTResidual = new int16_t[tb->transformBlockSize * tb->transformBlockSize]{ 0 };
	if (ctxMem.scanIdx == DiagonalScan)
	{
		Scanner::ReverseDiagonalScanBlock(tb);
	}
	else if (ctxMem.scanIdx == HorizontalScan)
	{
		Scanner::ReverseHorizontalScanBlock(tb);
	}
	else
	{
		Scanner::ReverseVerticalScanBlock(tb);
	}
}

void decoder::EntropyController::DecodeSliceSegmentData(Partition * partition)
{

	ctxMem.initType = decCtx->initType;
	ctxMem.amp_enabled_flag = decCtx->useAMP;

	int ctuSize = 1 << decCtx->ctbLog2SizeY;
	int end_of_slice_segment_flag = 0;

	partition->ctuTree = new CU*[partition->numberOfCTUs];

	aritDecod.initArithmeticDecoder();
	probMods->InitializeSelection(decCtx->quantizationParameter, decCtx->cabac_init_flag);

	queue <CUContext*> lastRow;
	CUContext * leftCU = nullptr;

	for (int i = 0; i < partition->numberOfCTUs; i++)
	{
		CUContext * above = nullptr;

		if (lastRow.size() >= partition->numberOfCTUsInRow)
		{
			above = lastRow.front();
			lastRow.pop();
		}

		CUContext * cuCtx = new CUContext(above, leftCU, nullptr, 0, 0);

		//int x = (i % numOfCTUsInRow) * enCtx->ctbSize;
		//int y = (i / numOfCTUsInRow) * enCtx->ctbSize;
		DecodeCTU(partition, cuCtx, i);
		end_of_slice_segment_flag = FromBitstream(syntaxElemEnum::end_of_slice_segment_flag);

		if (i % partition->numberOfCTUsInRow != partition->numberOfCTUsInRow - 1)
			leftCU = cuCtx;
		else
			leftCU = nullptr;

		if (above != nullptr)
		{
			above->deleteAllNodes();
			delete above;
		}

		lastRow.push(cuCtx);

		if (end_of_slice_segment_flag == 1)
		{
			break;
		}
	}

	while (lastRow.size() > 0)
	{
		CUContext* tmp = lastRow.front();
		lastRow.pop();
		tmp->deleteAllNodes();
		delete tmp;
	}

	if (end_of_slice_segment_flag == 1)
		RBSPSliceSegmentTrailingBits();
	else
		streamObject->ByteAlign();

}

void decoder::EntropyController::RBSPSliceSegmentTrailingBits()
{
	//streamObject->GetBit();
	streamObject->ByteAlign();
	streamObject->ReloadBuffers();
}

int decoder::EntropyController::FromBitstream(syntaxElemEnum syntaxElemNo)
{
	Value resultValue = Value();
	Bins b = Bins();

	b.bits = 0;
	b.length = 0;

	bool CABAC_flag = false;
	int ctxIncTableIdx = 0;
	int syntaxProbModelsIdx = 0;
	unsigned short ctxIdx = 0;

	switch (syntaxElemNo)
	{
	case vps_max_dec_pic_buffering_minus1:case vps_max_num_reorder_pics:case vps_max_latency_increase_plus1:case vps_num_layer_sets_minus1:case vps_num_ticks_poc_diff_one_minus1:
	case vps_num_hrd_parameters:case hrd_layer_set_idx:case sps_seq_parameter_set_id:case chroma_format_idc:case conf_win_left_offset:
	case conf_win_right_offset:case conf_win_top_offset:case conf_win_bottom_offset:case bit_depth_luma_minus8:case bit_depth_chroma_minus8:
	case sps_max_dec_pic_buffering_minus1:case sps_max_num_reorder_pics:case sps_max_latency_increase_plus1:case log2_min_luma_transform_block_size_minus2:
	case log2_diff_max_min_luma_transform_block_size:case max_transform_hierarchy_depth_inter:case max_transform_hierarchy_depth_intra:
	case log2_min_pcm_luma_coding_block_size_minus3:case log2_diff_max_min_pcm_luma_coding_block_size:case pps_pic_parameter_set_id:
	case pps_seq_parameter_set_id:case num_ref_idx_l0_default_active_minus1:case num_ref_idx_l1_default_active_minus1:case diff_cu_qp_delta_depth:
	case num_tile_columns_minus1:case num_tile_rows_minus1:case column_width_minus1:case row_height_minus1:case log2_parallel_merge_level_minus2:
	case log2_max_transform_skip_block_size_minus2:case diff_cu_chroma_qp_offset_depth:case log2_sao_offset_scale_luma:case log2_sao_offset_scale_chroma:
	case scaling_list_pred_matrix_id_delta:case slice_pic_parameter_set_id:case slice_type:case num_long_term_sps:case num_long_term_pics:
	case delta_poc_msb_cycle_lt:case collocated_ref_idx:case five_minus_max_num_merge_cand:case num_entry_point_offsets:
	case slice_segment_header_extension_length:case luma_log2_weight_denom:case delta_idx_minus1:case abs_delta_rps_minus1:case num_negative_pics:
	case num_positive_pics:case delta_poc_s0_minus1:case delta_poc_s1_minus1: // ue(v)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		break;

	case init_qp_minus26:case pps_cb_qp_offset:case pps_cr_qp_offset:case pps_beta_offset_div2:case pps_tc_offset_div2:case cb_qp_offset_list:case cr_qp_offset_list:
	case scaling_list_dc_coef_minus8:case scaling_list_delta_coef:case slice_qp_delta:case slice_cb_qp_offset:case slice_cr_qp_offset:case slice_beta_offset_div2:
	case slice_tc_offset_div2:case delta_chroma_log2_weight_denom:case delta_luma_weight_l0:case luma_offset_l0:case delta_chroma_weight_l0:case delta_chroma_offset_l0:
	case delta_luma_weight_l1:case luma_offset_l1:case delta_chroma_weight_l1:case delta_chroma_offset_l1: // se(v)
	{
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}

		if (resultValue.value % 2 == 1)
			resultValue.value = (resultValue.value / 2) + 1;
		else
			resultValue.value = -(resultValue.value / 2);
	}
	break;

	case vps_num_units_in_tick:case vps_time_scale:case vui_num_units_in_tick:case vui_time_scale: // u(32)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, UINT32_MAX); // 32-bits
		}
		break;

	case vps_reserved_0xffff_16bits: // u(16)
	case cabac_zero_word: // f(16)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << 16) - 1); // 16-bits
		}
		break;

	case emulation_prevention_three_byte:case ff_byte: // f(8)
	case general_level_idc:case sub_layer_level_idc:case last_payload_type_byte:case last_payload_size_byte:case slice_segment_header_extension_data_byte: // u(8)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 255); // 8-bits
		}
		break;

	case nal_unit_type:case nuh_layer_id:case vps_max_layers_minus1:case vps_max_layer_id: // u(6)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 63); // 6-bits
		}
		break;

	case sps_extension_5bits:case pps_extension_5bits:case general_profile_idc:case sub_layer_profile_idc: // u(5)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 31); // 5-bits
		}
		break;

	case vps_video_parameter_set_id:case sps_video_parameter_set_id: // u(4)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 15); // 4-bits
		}
		break;

	case nuh_temporal_id_plus1: case vps_max_sub_layers_minus1:case sps_max_sub_layers_minus1:case num_extra_slice_header_bits:case pic_type: // u(3)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 7); // 3-bits
		}
		break;

	case general_profile_space:case reserved_zero_2bits:case sub_layer_profile_space:case colour_plane_id:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 3); // 2-bits
		}
		break;

	case general_reserved_zero_34bits:case sub_layer_reserved_zero_34bits:
		for (int i = 0; i < 34; i++) streamObject->GetBit();
		break;

	case general_reserved_zero_43bits:case sub_layer_reserved_zero_43bits:
		for (int i = 0; i < 43; i++) streamObject->GetBit();
		break;

		// Cases with context variable refresh--------------------------------------------------------------------
	case log2_max_pic_order_cnt_lsb_minus4:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.log2_max_pic_order_cnt_lsb_minus4 = resultValue.value;
		break;

	case pic_width_in_luma_samples:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.pic_width_in_luma_samples = resultValue.value;
		break;

	case pic_height_in_luma_samples:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.pic_height_in_luma_samples = resultValue.value;
		break;

	case log2_diff_max_min_luma_coding_block_size:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.log2_diff_max_min_luma_coding_block_size = resultValue.value;
		break;

	case log2_min_luma_coding_block_size_minus3:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.log2_min_luma_coding_block_size_minus3 = resultValue.value;
		break;

	case num_short_term_ref_pic_sets:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.num_short_term_ref_pic_sets = resultValue.value;
		break;

	case num_long_term_ref_pics_sps:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.num_long_term_ref_pics_sps = resultValue.value;
		break;

	case offset_len_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.offset_len_minus1 = resultValue.value;
		break;

	case pcm_sample_bit_depth_luma_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 15); // 4-bits
		}
		ctxMem.pcm_sample_bit_depth_luma_minus1 = resultValue.value;
		break;

	case pcm_sample_bit_depth_chroma_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 15); // 4-bits
		}
		ctxMem.pcm_sample_bit_depth_chroma_minus1 = resultValue.value;
		break;

	case amp_enabled_flag:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 1);
		}
		ctxMem.amp_enabled_flag = resultValue.value;
		break;

	case num_ref_idx_l0_active_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.num_ref_idx_l0_active_minus1 = resultValue.value;
		break;

	case num_ref_idx_l1_active_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.num_ref_idx_l1_active_minus1 = resultValue.value;
		break;

	case chroma_qp_offset_list_len_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_EGk(b, 0);
		}
		ctxMem.chroma_qp_offset_list_len_minus1 = resultValue.value;
		break;

	case persistent_rice_adaptation_enabled_flag:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 1);
		}
		ctxMem.persistent_rice_adaptation_enabled_flag = resultValue.value;
		break;

	case extended_precision_processing_flag:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 1);
		}
		ctxMem.extended_precision_processing_flag = resultValue.value;
		break;

		// Cases with variable number of bits --------------------------------------------------------------------
	case lt_ref_pic_poc_lsb_sps:case slice_pic_order_cnt_lsb:case poc_lsb_lt:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << (ctxMem.log2_max_pic_order_cnt_lsb_minus4 + 4)) - 1);
		}
		break;

	case slice_segment_address:
	{
		int MinCbLog2SizeY = ctxMem.log2_min_luma_coding_block_size_minus3 + 3;
		int CtbLog2SizeY = MinCbLog2SizeY + ctxMem.log2_diff_max_min_luma_coding_block_size;
		int CtbSizeY = 1 << CtbLog2SizeY;
		int PicWidthInCtbsY = (int)ceil(ctxMem.pic_width_in_luma_samples / CtbSizeY);
		int PicHeightInCtbsY = (int)ceil(ctxMem.pic_height_in_luma_samples / CtbSizeY);
		int PicSizeInCtbsY = PicWidthInCtbsY * PicHeightInCtbsY;
		int numOfBits = (int)ceil(log2(PicSizeInCtbsY));
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << numOfBits) - 1);
		}
	}
	break;

	case short_term_ref_pic_set_idx:
	{
		int numOfBits = (int)ceil(log2(ctxMem.num_short_term_ref_pic_sets));
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << numOfBits) - 1);
		}
	}
	break;

	case lt_idx_sps:
	{
		int numOfBits = (int)ceil(log2(ctxMem.num_long_term_ref_pics_sps));
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << numOfBits) - 1);
		}
	}
	break;

	case entry_point_offset_minus1:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << (ctxMem.offset_len_minus1 + 1)) - 1);
		}
		break;

	case list_entry_l0:case list_entry_l1:
		// TODO: inter prediction
		break;

	case pcm_sample_luma:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << (ctxMem.pcm_sample_bit_depth_luma_minus1 + 1)) - 1);
		}
		break;

	case pcm_sample_chroma:
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, (1 << (ctxMem.pcm_sample_bit_depth_chroma_minus1 + 1)) - 1);
		}
		break;

		// CABAC Syntax element ----------------------------------------------------------------------------------
	case end_of_slice_segment_flag:
		ctxIncTableIdx = 0;
		CABAC_flag = true;
		break;

	case end_of_subset_one_bit:
		ctxIncTableIdx = 1;
		CABAC_flag = true;
		break;

	case sao_merge_left_flag:

		ctxIdx = ctxMem.initType;

		ctxIncTableIdx = 2;
		syntaxProbModelsIdx = 0;
		CABAC_flag = true;
		break;

	case sao_merge_up_flag:
		ctxIdx = ctxMem.initType;

		ctxIncTableIdx = 3;
		syntaxProbModelsIdx = 0;
		CABAC_flag = true;
		break;

	case sao_type_idx_luma:
		ctxIdx = ctxMem.initType;
		ctxIncTableIdx = 4;
		syntaxProbModelsIdx = 1;
		CABAC_flag = true;
		break;

	case sao_type_idx_chroma:
		ctxIdx = ctxMem.initType;
		ctxIncTableIdx = 5;
		syntaxProbModelsIdx = 1;
		CABAC_flag = true;
		break;

	case sao_offset_abs:
		ctxIncTableIdx = 6;
		CABAC_flag = true;
		break;

	case sao_offset_sign:
		ctxIncTableIdx = 7;
		CABAC_flag = true;
		break;

	case sao_band_position:
		ctxIncTableIdx = 8;
		CABAC_flag = true;
		break;

	case sao_eo_class_luma:
		ctxIncTableIdx = 9;
		CABAC_flag = true;
		break;

	case sao_eo_class_chroma:
		ctxIncTableIdx = 10;
		CABAC_flag = true;
		break;

	case split_cu_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 3;
		if (ctxMem.initType == 2)
			ctxIdx = 6;

		ctxIncTableIdx = 11;
		syntaxProbModelsIdx = 2;
		CABAC_flag = true;
		break;

	case cu_transquant_bypass_flag:

		ctxIdx = ctxMem.initType;


		ctxIncTableIdx = 12;
		syntaxProbModelsIdx = 3;
		CABAC_flag = true;
		break;

	case cu_skip_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 3;
		ctxIncTableIdx = 13;
		syntaxProbModelsIdx = 4;
		CABAC_flag = true;
		break;

	case pred_mode_flag:
		ctxIncTableIdx = 14;
		syntaxProbModelsIdx = 5;
		CABAC_flag = true;
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 1;
		break;

	case part_mode:
	{
		if (ctxMem.initType == 1)
			ctxIdx = 1;
		if (ctxMem.initType == 2)
			ctxIdx = 5;

		bool CuPredMode = true;

		if (ctxMem.pred_mode_flag == 0)
			CuPredMode = false;

		ctxIncTableIdx = 16;
		syntaxProbModelsIdx = 6;
		CABAC_flag = true;
	}
	break;

	case pcm_flag:
		ctxIncTableIdx = 17;
		CABAC_flag = true;
		break;

	case prev_intra_luma_pred_flag:
		ctxIdx = ctxMem.initType;

		ctxIncTableIdx = 18;
		syntaxProbModelsIdx = 7;
		CABAC_flag = true;
		break;

	case mpm_idx:
		ctxIncTableIdx = 19;
		CABAC_flag = true;
		break;

	case rem_intra_luma_pred_mode:
		ctxIncTableIdx = 20;
		CABAC_flag = true;
		break;

	case intra_chroma_pred_mode:
		ctxIdx = ctxMem.initType;

		ctxIncTableIdx = 21;
		syntaxProbModelsIdx = 8;
		CABAC_flag = true;
		break;

	case rqt_root_cbf:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 1;
		ctxIncTableIdx = 22;
		syntaxProbModelsIdx = 9;
		CABAC_flag = true;
		break;

	case merge_idx:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 1;
		ctxIncTableIdx = 24;
		syntaxProbModelsIdx = 11;
		CABAC_flag = true;
		break;

	case merge_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 1;
		ctxIncTableIdx = 23;
		syntaxProbModelsIdx = 10;
		CABAC_flag = true;
		break;

	case inter_pred_idc:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 5;
		ctxIncTableIdx = 25;
		syntaxProbModelsIdx = 12;
		CABAC_flag = true;
		break;

	case ref_idx_l0:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 2;
		ctxIncTableIdx = 26;
		syntaxProbModelsIdx = 13;
		CABAC_flag = true;
		break;

	case mvp_l0_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 1;
		ctxIncTableIdx = 28;
		syntaxProbModelsIdx = 14;
		CABAC_flag = true;
		break;

	case ref_idx_l1:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 2;
		ctxIncTableIdx = 27;
		syntaxProbModelsIdx = 13;
		CABAC_flag = true;
		break;

	case mvp_l1_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 1;
		ctxIncTableIdx = 29;
		syntaxProbModelsIdx = 14;
		CABAC_flag = true;
		break;

	case split_transform_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 3;
		if (ctxMem.initType == 2)
			ctxIdx = 6;

		syntaxProbModelsIdx = 15;
		ctxIncTableIdx = 30;
		CABAC_flag = true;
		break;

	case cbf_cb:
		if (ctxMem.initType == 1)
			ctxIdx = 4;
		if (ctxMem.initType == 2)
			ctxIdx = 8;

		syntaxProbModelsIdx = 17;
		ctxIncTableIdx = 31;
		CABAC_flag = true;
		break;

	case cbf_cr:
		if (ctxMem.initType == 1)
			ctxIdx = 4;
		if (ctxMem.initType == 2)
			ctxIdx = 8;

		syntaxProbModelsIdx = 17;
		ctxIncTableIdx = 32;
		CABAC_flag = true;
		break;

	case cbf_luma:
		if (ctxMem.initType == 1)
			ctxIdx = 2;
		if (ctxMem.initType == 2)
			ctxIdx = 4;

		syntaxProbModelsIdx = 16;
		ctxIncTableIdx = 33;
		CABAC_flag = true;
		break;

	case abs_mvd_greater0_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 0;
		if (ctxMem.initType == 2)
			ctxIdx = 2;
		ctxIncTableIdx = 34;
		syntaxProbModelsIdx = 18;
		CABAC_flag = true;
		break;

	case abs_mvd_greater1_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 1;
		if (ctxMem.initType == 2)
			ctxIdx = 3;
		ctxIncTableIdx = 35;
		syntaxProbModelsIdx = 19;
		ctxIdx = 1; //
		CABAC_flag = true;
		break;

	case abs_mvd_minus2:
		ctxIncTableIdx = 36;
		CABAC_flag = true;
		break;

	case mvd_sign_flag:
		ctxIncTableIdx = 37;
		CABAC_flag = true;
		break;

	case cu_qp_delta_abs:
		if (ctxMem.initType == 1)
			ctxIdx = 2;
		if (ctxMem.initType == 2)
			ctxIdx = 4;

		syntaxProbModelsIdx = 20;
		ctxIncTableIdx = 38;
		CABAC_flag = true;
		break;

	case cu_qp_delta_sign_flag:
		ctxIncTableIdx = 39;
		CABAC_flag = true;
		break;

	case cu_chroma_qp_offset_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 1;
		if (ctxMem.initType == 2)
			ctxIdx = 2;

		syntaxProbModelsIdx = 21;
		ctxIncTableIdx = 40;
		CABAC_flag = true;
		break;

	case cu_chroma_qp_offset_idx:
		if (ctxMem.initType == 1)
			ctxIdx = 1;
		if (ctxMem.initType == 2)
			ctxIdx = 2;

		syntaxProbModelsIdx = 22;
		ctxIncTableIdx = 41;
		CABAC_flag = true;
		break;

	case transform_skip_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 1;
		if (ctxMem.initType == 2)
			ctxIdx = 2;

		syntaxProbModelsIdx = 25;
		ctxIncTableIdx = 44;
		CABAC_flag = true;
		break;

	case explicit_rdpcm_flag:
		syntaxProbModelsIdx = 26;
		ctxIncTableIdx = 45;
		CABAC_flag = true;
		break;

	case explicit_rdpcm_dir_flag:
		syntaxProbModelsIdx = 27;
		ctxIncTableIdx = 46;
		CABAC_flag = true;
		break;

	case last_sig_coeff_x_prefix:
		if (ctxMem.initType == 1)
			ctxIdx = 18;
		if (ctxMem.initType == 2)
			ctxIdx = 36;

		syntaxProbModelsIdx = 28;
		ctxIncTableIdx = 47;
		CABAC_flag = true;
		break;

	case last_sig_coeff_y_prefix:
		if (ctxMem.initType == 1)
			ctxIdx = 18;
		if (ctxMem.initType == 2)
			ctxIdx = 36;

		syntaxProbModelsIdx = 29;
		ctxIncTableIdx = 48;
		CABAC_flag = true;
		break;

	case last_sig_coeff_x_suffix:
		ctxIncTableIdx = 49;
		CABAC_flag = true;
		break;

	case last_sig_coeff_y_suffix:
		ctxIncTableIdx = 50;
		CABAC_flag = true;
		break;

	case coded_sub_block_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 4;
		if (ctxMem.initType == 2)
			ctxIdx = 8;

		syntaxProbModelsIdx = 30;
		ctxIncTableIdx = 51;
		CABAC_flag = true;
		break;

	case sig_coeff_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 42;
		if (ctxMem.initType == 2)
			ctxIdx = 84;

		syntaxProbModelsIdx = 31;
		ctxIncTableIdx = 52;
		CABAC_flag = true;
		break;

	case coeff_abs_level_greater1_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 24;
		if (ctxMem.initType == 2)
			ctxIdx = 48;

		syntaxProbModelsIdx = 32;
		ctxIncTableIdx = 53;
		CABAC_flag = true;
		break;

	case coeff_abs_level_greater2_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 6;
		if (ctxMem.initType == 2)
			ctxIdx = 12;

		syntaxProbModelsIdx = 33;
		ctxIncTableIdx = 54;
		CABAC_flag = true;
		break;

	case coeff_sign_flag:
		ctxIncTableIdx = 56;
		CABAC_flag = true;
		break;

	case coeff_abs_level_remaining:
	{
		int baseLevel = 1 + ctxMem.coeff_abs_level_greater1_flag + ctxMem.coeff_abs_level_greater2_flag;
		ctxIncTableIdx = 55;
		CABAC_flag = true;
	}
	break;

	case log2_res_scale_abs_plus1:
		if (ctxMem.initType == 1)
			ctxIdx = 8;
		if (ctxMem.initType == 2)
			ctxIdx = 16;

		ctxIncTableIdx = 42;
		CABAC_flag = true;
		break;

	case res_scale_sign_flag:
		if (ctxMem.initType == 1)
			ctxIdx = 2;
		if (ctxMem.initType == 2)
			ctxIdx = 4;

		ctxIncTableIdx = 43;
		CABAC_flag = true;
		break;

	default: // f(1), u(1)
		while (!resultValue.possible)
		{
			b = Binarizer::put(streamObject->GetBit(), b);
			resultValue = Binarizer::invBin_FL(b, 1);
		}

		break;
	}

	if (CABAC_flag)
	{
		for (int i = 0; !resultValue.possible; i++)
		{
			bool binVal = false;

			unsigned short ctxIdxTmp = ctxIdx;
			unsigned short ctxInc;
			if (i <= 5)
				ctxInc = ctxIncTable[ctxIncTableIdx][i];
			else
				ctxInc = ctxIncTable[ctxIncTableIdx][5];

			switch (ctxInc)
			{
			case nd:
				// Special cases of derivation of ctxIndex
				ctxIdxTmp += derivateSpecCtxIdx(ctxIncTableIdx, i);
				aritDecod.decodeDecision(&probMods->probabilityModel[syntaxProbModelsIdx][ctxIdxTmp]);
				break;

			case bypass:
				// Bypass encoding
				aritDecod.decodeBypass();
				break;

			case toTerminate:
				// Terminate encoding
				aritDecod.decodeTerminate();
				break;

			case na:
				// Forbbiden case: do nothing
				break;

			default:
				ctxIdxTmp += ctxInc;
				aritDecod.decodeDecision(&probMods->probabilityModel[syntaxProbModelsIdx][ctxIdxTmp]);
				break;
			}

			b = Binarizer::put(aritDecod.binVal, b);

			switch (syntaxElemNo)
			{
			case end_of_slice_segment_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case end_of_subset_one_bit:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case sao_merge_left_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case sao_merge_up_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case sao_type_idx_luma:
				resultValue = Binarizer::invBin_TRk(b, 0, 2);
				break;

			case sao_type_idx_chroma:
				resultValue = Binarizer::invBin_TRk(b, 0, 2);
				break;

			case sao_offset_abs:
				resultValue = Binarizer::invBin_TRk(b, 0, (1 << (min(bitDepthY, 10) - 5)) - 1);
				break;

			case sao_offset_sign:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case sao_band_position:
				resultValue = Binarizer::invBin_FL(b, 31);
				break;

			case sao_eo_class_luma:
				resultValue = Binarizer::invBin_FL(b, 3);
				break;

			case sao_eo_class_chroma:
				resultValue = Binarizer::invBin_FL(b, 3);
				break;

			case split_cu_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cu_transquant_bypass_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				if (resultValue.possible)
				{
					ctxMem.cu_transquant_bypass_flag = resultValue.value;
				}

				break;

			case cu_skip_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case pred_mode_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				if (resultValue.possible)
				{
					ctxMem.pred_mode_flag = resultValue.value;
				}

				break;

			case part_mode:
			{
				bool CuPredMode = true;

				if (ctxMem.pred_mode_flag == 0)
					CuPredMode = false;

				resultValue = Binarizer::invBin_part_mode(b, CuPredMode, ctxMem.cbSize, ctxMem.log2_min_luma_coding_block_size_minus3 + 3, ctxMem.amp_enabled_flag);
			}
			break;

			case pcm_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case prev_intra_luma_pred_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case mpm_idx:
				resultValue = Binarizer::invBin_TRk(b, 0, 2);
				break;

			case rem_intra_luma_pred_mode:
				resultValue = Binarizer::invBin_FL(b, 31);
				break;

			case intra_chroma_pred_mode:
				resultValue = Binarizer::invBin_intra_chroma_pred_mode(b);
				break;

			case rqt_root_cbf:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case merge_idx:
			{
				int IvDiMcEnabledFlag = 0;
				int TexMcEnabledFlag = 0;
				int VspMcEnabledFlag = 0;

				int NumExtraMergeCand = (IvDiMcEnabledFlag + TexMcEnabledFlag + VspMcEnabledFlag) > 0 ? 1 : 0;
				int MaxNumMergeCand = 5 + NumExtraMergeCand - ctxMem.five_minus_max_num_merge_cand;

				resultValue = Binarizer::invBin_TRk(b, 0, MaxNumMergeCand - 1);
			}
			break;

			case merge_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case inter_pred_idc:
				resultValue = Binarizer::invBin_inter_pred_idc(b, ctxMem.nPbW, ctxMem.nPbH);
				break;

			case ref_idx_l0:
				resultValue = Binarizer::invBin_TRk(b, 0, ctxMem.num_ref_idx_l0_active_minus1);
				break;

			case mvp_l0_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case ref_idx_l1:
				resultValue = Binarizer::invBin_TRk(b, 0, ctxMem.num_ref_idx_l1_active_minus1);
				break;

			case mvp_l1_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case split_transform_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cbf_cb:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cbf_cr:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cbf_luma:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case abs_mvd_greater0_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case abs_mvd_greater1_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case abs_mvd_minus2:
				resultValue = Binarizer::invBin_EGk(b, 1);
				break;

			case mvd_sign_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cu_qp_delta_abs:
				resultValue = Binarizer::invBin_cu_qp_delta_abs(b);
				break;

			case cu_qp_delta_sign_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cu_chroma_qp_offset_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case cu_chroma_qp_offset_idx:
				resultValue = Binarizer::invBin_TRk(b, 0, ctxMem.chroma_qp_offset_list_len_minus1);
				break;

			case transform_skip_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				if (resultValue.possible)
				{
					ctxMem.transform_skip_flag = resultValue.value;
				}

				break;

			case explicit_rdpcm_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case explicit_rdpcm_dir_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case last_sig_coeff_x_prefix:
				resultValue = Binarizer::invBin_TRk(b, 0, (ctxMem.log2TrafoSize << 1) - 1);
				if (resultValue.possible)
				{
					ctxMem.last_sig_coeff_x_prefix = resultValue.value;
				}

				break;

			case last_sig_coeff_y_prefix:
				resultValue = Binarizer::invBin_TRk(b, 0, (ctxMem.log2TrafoSize << 1) - 1);
				if (resultValue.possible)
				{
					ctxMem.last_sig_coeff_y_prefix = resultValue.value;
				}

				break;

			case last_sig_coeff_x_suffix:
				resultValue = Binarizer::invBin_FL(b, (1 << ((ctxMem.last_sig_coeff_x_prefix >> 1) - 1)) - 1);
				break;

			case last_sig_coeff_y_suffix:
				resultValue = Binarizer::invBin_FL(b, (1 << ((ctxMem.last_sig_coeff_y_prefix >> 1) - 1)) - 1);
				break;

			case coded_sub_block_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case sig_coeff_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case coeff_abs_level_greater1_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case coeff_abs_level_greater2_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case coeff_sign_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;

			case coeff_abs_level_remaining:
			{
				int baseLevel = 1 + ctxMem.coeff_abs_level_greater1_flag + ctxMem.coeff_abs_level_greater2_flag;
				resultValue = Binarizer::invBin_coeff_abs_level_remaining(b, baseLevel, ctxMem.colorIdx, ctxMem.persistent_rice_adaptation_enabled_flag, ctxMem.transform_skip_flag,
					ctxMem.cu_transquant_bypass_flag, ctxMem.extended_precision_processing_flag, ctxMem.first_time_invoke, ctxMem.StatCoeff,
					ctxMem.cLastAbsLevel, ctxMem.cLastRiceParam, bitDepthY, bitDepthC);

				if (resultValue.possible)
					ctxMem.first_time_invoke = false;
			}
			break;

			case log2_res_scale_abs_plus1:
				resultValue = Binarizer::invBin_TRk(b, 0, 4);
				break;

			case res_scale_sign_flag:
				resultValue = Binarizer::invBin_FL(b, 1);
				break;
			}
		}
	}

	if (Stringify::enableSyntaxLog)
		Stringify::syntaxDecodingLog << syntaxElemString[syntaxElemNo] << ": " << resultValue.value << endl;

	return resultValue.value;
}

unsigned short decoder::EntropyController::derivateSpecCtxIdx(int ctxIncTableIdx, int i)
{
	unsigned short ctxIdx = 0;

	switch (ctxIncTableIdx)
	{
	case 11: // split_cu_flag
		ctxIdx = ctxIdxOffset[4].ctxIdxOffset[0] + derivateCtxInc_split_cu_flag(ctxMem.cqtDepth, ctxMem.ctDepthL,
			ctxMem.ctDepthA, ctxMem.availableL, ctxMem.availableA);
		break;

	case 13: // cu_skip_flag
		ctxIdx = ctxIdxOffset[6].ctxIdxOffset[1] + derivateCtxInc_cu_skip_flag(ctxMem.left_cu_skip_flag, ctxMem.above_cu_skip_flag, ctxMem.availableL, ctxMem.availableA);
		break;

	case 30: // split_transform_flag
		ctxIdx = ctxIdxOffset[19].ctxIdxOffset[0] + 0;	// ctxInc(binIdx = 0) = 5 - log2Trafosize
		break;

	case 31: //cbf_cb
		ctxIdx = ctxIdxOffset[21].ctxIdxOffset[0] + ctxMem.trafoDepth;	// ctxInc(binIdx = 0) = trafoDepth
		break;

	case 32: // cbf_cr
		ctxIdx = ctxIdxOffset[22].ctxIdxOffset[0] + ctxMem.trafoDepth;	// ctxInc(binIdx = 0) = trafoDepth
		break;

	case 33: //cbf_luma
		ctxIdx = ctxIdxOffset[20].ctxIdxOffset[0] + (ctxMem.trafoDepth == 0 ? 1 : 0);
		break;

	case 47: // last_sig_coeff_x_prefix
		ctxIdx = ctxIdxOffset[33].ctxIdxOffset[0] + derivateCtxIncLAST(i, ctxMem.colorIdx, ctxMem.log2TrafoSize);
		break;

	case 48: // last_sig_coeff_y_prefix
		ctxIdx = ctxIdxOffset[34].ctxIdxOffset[0] + derivateCtxIncLAST(i, ctxMem.colorIdx, ctxMem.log2TrafoSize);
		break;

	case 51: // coded_sub_block_flag
		ctxIdx = ctxIdxOffset[35].ctxIdxOffset[0] + derivateCtxIncCSBF(ctxMem.colorIdx, ctxMem.subBlockRightCSBF, ctxMem.subBlockBottomCSBF,
			ctxMem.subBlockCoordinateX, ctxMem.subBlockCoordinateY, ctxMem.log2TrafoSize);
		break;

	case 52: //sig_coeff_flag
		ctxIdx = ctxIdxOffset[36].ctxIdxOffset[0] + derivateCtxIncSIG(ctxMem.colorIdx, ctxMem.transformBlock_xC, ctxMem.transformBlock_yC, ctxMem.scanIdx,
			ctxMem.log2TrafoSize, ctxMem.subBlockRightCSBF, ctxMem.subBlockBottomCSBF, 0, ctxMem.transform_skip_flag, ctxMem.cu_transquant_bypass_flag);
		break;

	case 53: // coeff_abs_level_greater1_flag
		ctxDataALG1 = derivateCtxIncALG1(ctxMem.colorIdx, ctxMem.sub_block_index, ctxMem.inside_sub_block_index, ctxMem.previous_ALG1_flag);
		ctxIdx = ctxIdxOffset[37].ctxIdxOffset[0] + ctxDataALG1.ctxInc;
		break;

	case 54: // coeff_abs_level_greater2_flag
		ctxIdx = ctxIdxOffset[38].ctxIdxOffset[0] + derivateCtxIncALG2(ctxMem.colorIdx, ctxDataALG1.ctxSet);
		break;
	}

	return ctxIdx;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxInc_split_cu_flag
Description:	calculates context increment for syntax elements split_cu_flag based on
availability of neighbouring (left and above) coding blocks and their depth. Possible values
are 0,1,2. Details in chapter 9.3.4.2.2 in [1]
Parameters:     cqtDepth	- depth of currently process CB
ctDepthNbL	- depth of left CB in z-scan order
ctDepthNbA	- depth of above CB in z-scan order
availableL	- availability of left CB in z-scan order
availableA	- availability of above CB in z-scan order
Returns:		ctxInc		- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char decoder::EntropyController::derivateCtxInc_split_cu_flag(unsigned char cqtDepth, unsigned char ctDepthNbL, unsigned char ctDepthNbA, bool availableL, bool availableA)
{
	bool condL;
	bool condA;
	unsigned char ctxInc;

	condL = ctDepthNbL > cqtDepth;
	condA = ctDepthNbA > cqtDepth;

	ctxInc = (condL && availableL) + (condA && availableA);

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxInc_cu_skip_flag
Description:	calculates context increment for syntax elements cu_skip_flag based on
flag values for neighbouring (left and above) coding blocks and their depth. Possible values
are 0,1,2. Details in chapter 9.3.4.2.2 in [1]
Parameters:     cu_skip_flagNbL	- flag value for left neighboring CB
cu_skip_flagNbA	- flag value for above neighbouring CB
availableL		- availability of left CB in z-scan order
availableA		- availability of above CB in z-scan order
Returns:		ctxInc			- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char decoder::EntropyController::derivateCtxInc_cu_skip_flag(bool cu_skip_flagNbL, bool cu_skip_flagNbA, bool availableL, bool availableA)
{
	bool condL;
	bool condA;
	unsigned char ctxInc;

	condL = cu_skip_flagNbL;
	condA = cu_skip_flagNbA;

	ctxInc = (condL && availableL) + (condA && availableA);

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncCSBF
Description:	calculates context increment for syntax element coded_sub_block_flag based on color component index,
and CSBF of transform sub-blocks right and below. Possible values are 0..3. For details see chapter 9.3.4.2.4
in [1]
Parameters:     cIdx			- color component index
csbfRight		- CSBF of the sub-block right to currently processed
csbfBottom		- CSBF of the sub-block bottom to currently processed
xS, yS			- scan location of current sub-block in TB
log2TrafoSize	- transform block size
Returns:		ctxInc			- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char decoder::EntropyController::derivateCtxIncCSBF(unsigned short cIdx, bool csbfRight, bool csbfBottom, unsigned char xS, unsigned char yS, unsigned short log2TrafoSize)
{
	unsigned char csbfCtx = 0;
	unsigned char ctxInc;

	if (xS < ((1 << (log2TrafoSize - 2)) - 1))
		csbfCtx += csbfRight;						// csbf[xS + 1][yS]

	if (yS < ((1 << (log2TrafoSize - 2)) - 1))
		csbfCtx += csbfBottom;						// csbf[xS][yS + 1]

	if (cIdx == 0) {
		ctxInc = min(csbfCtx, (unsigned char)1);
	}
	else
	{
		ctxInc = 2 + min(csbfCtx, (unsigned char)1);
	}

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncLAST
Description:	calculates context increment for syntax elements last_sig_coeff_x_prefix and last_sig_coeff_y_prefix
based on bin position within syntax elements and transform block size. Possible values are 0..17. For
details see chapter 9.3.4.2.3 in [1] and 2nd paragraph in chapter 8.6.4.2 in [2]
Parameters:     binIdx		- bin index
cIdx		- color component index
log2TrafoSize - transform block size
Returns:		ctxInc		- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char decoder::EntropyController::derivateCtxIncLAST(unsigned short binIdx, unsigned short cIdx, unsigned short log2TrafoSize)
{
	unsigned short ctxOffset, ctxShift;
	unsigned char ctxInc;

	if (cIdx == 0)
	{
		ctxOffset = 3 * (log2TrafoSize - 2) + ((log2TrafoSize - 1) >> 2);
		ctxShift = (log2TrafoSize + 1) >> 2;
	}
	else
	{
		ctxOffset = 15;
		ctxShift = log2TrafoSize - 2;
	}
	ctxInc = (binIdx >> ctxShift) + ctxOffset;

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncSIG
Description:	calculates context increment for syntax element sig_coeff_flag based on color component, TB size, scan order
(diag, hor, vert), position of the sub-block within TB (DC, non-DC sub-block) and position based context index
(pattern derived from right and bottom CSBF). Possible values 0..43. For details see chapter 9.3.4.2.5 in [1]
and table 8.11 in [2]
Parameters:     cIdx			- color component index
xC, yC			- current coefficient scan location in TB
scanIdx			- scan order index, 0 = diag, 1 = hor, 2 = ver
log2TrafoSize	- transform block size
csbfRight		- CSBF of the sub-block right to currently processed
csbfBottom		- CSBF of the sub-block bottom to currently processed
transform_skip_context_enabled_flag	- determines if context is fixed value or calculated from parameters defined above
transform_skip_flag					- partialy determines if transformation will be applied to current TB
cu_transquant_bypass_flag			- determines if scaling and transform and in-loop filter are bypassed
Returns:		ctxInc								- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char decoder::EntropyController::derivateCtxIncSIG(unsigned short cIdx, unsigned char xC, unsigned char yC, unsigned char scanIdx, unsigned short log2TrafoSize, bool csbfRight, bool csbfBottom, bool transform_skip_context_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag)
{
	unsigned char sigCtx;
	unsigned char xS;										// index x for sub-block inside TB 
	unsigned char yS;										// index y for sub-block inside TB  
	unsigned char xP;										// index x for level element inside sub-block
	unsigned char yP;										// index y for level element inside sub-block
	unsigned char prevCsbf;
	unsigned char ctxInc;

	if (transform_skip_context_enabled_flag && (transform_skip_flag || cu_transquant_bypass_flag))
	{
		sigCtx = (cIdx == 0) ? 42 : 16;
	}
	else
	{
		if (log2TrafoSize == 2)								// 4x4 TB only
		{
			sigCtx = ctxIdxMapSIG[(yC << 2) + xC];
		}
		else if ((xC + yC) == 0)							// 8x8 - 32x32 TBs,	DC level in DC region, all 
		{
			sigCtx = 0;
		}
		else														// DC and non-DC subblocks
		{
			// 4 x 4 sub-block position based mapping context selection based on CSBF of neighboring subblocks, see Fig 8.19
			xS = xC >> 2;
			yS = yC >> 2;
			prevCsbf = 0;

			if (xS < ((1 << (log2TrafoSize - 2)) - 1))					// not a border CSBF on x axis
				prevCsbf += csbfRight;									// csbf[xS + 1][yS]

			if (yS < ((1 << (log2TrafoSize - 2)) - 1))					// not a border CSBF on y axis
				prevCsbf += (csbfBottom << 1);							// csbf[xS][yS + 1]

			xP = xC & 3;
			yP = yC & 3;

			if (prevCsbf == 0)
			{
				sigCtx = (xP + yP == 0) ? 2 : (xP + yP < 3) ? 1 : 0;	// pattern 1, 
			}
			else if (prevCsbf == 1)
			{
				sigCtx = (yP == 0) ? 2 : (yP == 1) ? 1 : 0;				// pattern 2,
			}
			else if (prevCsbf == 2)
			{
				sigCtx = (xP == 0) ? 2 : (xP == 1) ? 1 : 0;				// pattern 3
			}
			else
			{
				sigCtx = 2;												// pattern 4
			}

			if (cIdx == 0)									// luma
			{
				if ((xS + yS) > 0) sigCtx += 3;				// Differentiate subblock position

				if (log2TrafoSize == 3)
				{
					sigCtx += (scanIdx == 0) ? 9 : 15;			// Differentiate scan order for 8x8 TB
				}
				else
				{
					sigCtx += 21;								// 16x16 and 32x32 TBs							
				}

			}
			else											// chroma
			{
				if (log2TrafoSize == 3)							// 8x8 TB
				{
					sigCtx += 9;
				}
				else
				{
					sigCtx += 12;								// 8x8 TB							
				}
			}
		}
	}
	// Determine starting context index depending on color component
	if (cIdx == 0)
	{
		ctxInc = sigCtx;
	}
	else
	{
		ctxInc = 27 + sigCtx;
	}

	return ctxInc;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncALG1
Description:	calculates context increment for syntax element coeff_abs_level_greater1_flag based on context set which is assigned to
sub-block and previous flag value(s) in scan order. Further, context set depends on  color component index, sub-block
scan index (scan position within TB), coefficient scan index (scan position within sub-block) and method invocation count
for particular sub-block.  Possible values are 0..23. Detailed description in chapter 9.3.4.2.6 in [1]
Parameters:     cIdx			- color component index
subblockScanIdx	- position of sub-block to which flag belongs along TB reverse scanning pattern
coeffScanIdx	- position of the flag inside sub-block
lastALG1		- previous flag value along reverse scanning pattern of transform sub-block and blocks
Returns:		ctxIncWithCtxSet- context increment for syntax element and ctxSet which is used for ctxInc derivation of ALG2
Time (Cycles):	typ= xx cycl max = xx cycl
Remarks:		Specification acc. chapter 8.6.5.1 (p. 255) in [2] and [1] are different in No. of sets. To check with author MC_todo
-----------------------------------------------------------------------------*/

ctxStruct4ALG1 decoder::EntropyController::derivateCtxIncALG1(unsigned short cIdx, unsigned char subblockScanIdx, unsigned char coeffScanIdx, bool lastALG1)
{
	if (ctxMem.first_time_in_TB == 1)
	{
		firstTime = true;
		activeSubBlockScanIdx = subblockScanIdx;
	}

	if (!firstTime)
	{
		if (activeSubBlockScanIdx != subblockScanIdx)
		{
			firstTime = true;
			activeSubBlockScanIdx = subblockScanIdx;
			greater1CtxPrevSubBlock = greater1Ctx;
		}
	}

	unsigned char lastGreater1Ctx;

	bool lastGreater1Flag;
	ctxStruct4ALG1 ctxIncWithCtxSet = ctxStruct4ALG1();

	if (firstTime)			// Method invoked first time for particular sub-block
	{
		// Assumption is that in reverse scanning pattern indices are incremented and that first processed sub-block has index 0
		// If this will be changed during controller implementation expression in if statement and init value of storage value have 
		// to be modified MC_todo
		// Storage parameter has to be reset when last sub-block in TB is processed MC_todo

		if ((subblockScanIdx == ctxMem.maxScanIdx - 1) || (cIdx > 0))
		{
			ctxIncWithCtxSet.ctxSet = 0;
		}
		else
		{
			ctxIncWithCtxSet.ctxSet = 2;
		}

		// Derivation of lastGreater1Ctx
		if (ctxMem.first_time_in_TB == 1)								// New sub-block is first sub-block processed along reverese scanning pattern in the TB
		{
			lastGreater1Ctx = 1;
			ctxMem.first_time_in_TB = 0;
		}
		else													// Newly started sub-block, but not first scanned in TB
		{
			// greater1Ctx is value derived during last invocation of this method for a previous sub-block
			lastGreater1Ctx = greater1CtxPrevSubBlock;

			// lastALG1 is here coeff_abs_level_greater1_flag that has been used during the last invocation of the method for previous sub-block
			if (lastGreater1Ctx > 0)
			{
				lastGreater1Flag = lastALG1;

				if (lastGreater1Flag == 1)
					lastGreater1Ctx = 0;
				else
					lastGreater1Ctx += 1;
			}
		}
		if (lastGreater1Ctx == 0) ctxIncWithCtxSet.ctxSet++;

		greater1Ctx = 1;
		firstTime = false;
	}
	else													// Method invoked for same sub-block
	{
		ctxIncWithCtxSet.ctxSet = ctxSet;

		// Variables ctxSet and greater1Ctx presevre their last values from previous invocation of this method
		if (greater1Ctx > 0)
		{
			// lastALG1 is here coeff_abs_level_greater1_flag that has been used during the last invocation of the method
			lastGreater1Flag = lastALG1;

			if (lastGreater1Flag == 1)
				greater1Ctx = 0;
			else
				greater1Ctx += 1;
		}
	}

	// Derivation of ctxInc
	ctxIncWithCtxSet.ctxInc = (ctxIncWithCtxSet.ctxSet * 4) + min((unsigned char)3, greater1Ctx);
	if (cIdx > 0)
	{
		ctxIncWithCtxSet.ctxInc += 16;
	}

	ctxSet = ctxIncWithCtxSet.ctxSet;

	return ctxIncWithCtxSet;
}

/*-----------------------------------------------------------------------------
Function name:  derivateCtxIncALG2
Description:	calculates context increment for syntax element coeff_abs_level_greater2_flag based on color component index, sub-block
scan index (scan position within TB) and context set calculated for that sub-block in derivateCtxIncALG1 method. Possible values
are 0..5. For details see chapter 9.3.4.2.7 in [1]
Parameters:     cIdx			- color component index
ctxSet			- context set calculated for the sub-block in which processed ALG2 bin is located
Returns:		ctxInc			- context increment for syntax element
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
unsigned char decoder::EntropyController::derivateCtxIncALG2(unsigned short cIdx, unsigned char ctxSet)
{
	unsigned char ctxInc;

	// MC_todo ctxSet, current context set, is equal to value of the variable ctxSet that has been derived for same subset i within method derivateCtxIncALG1
	// I am 90% sure that subset here relates to ctxSet derived for sub-block i. To be reexamined during controller realization
	ctxInc = ctxSet;

	if (cIdx > 0)
		ctxInc += 4;

	return ctxInc;
}

bool decoder::EntropyController::IsVideoDecoded()
{
	return streamObject->IsEND();
}

void decoder::EntropyController::ReleaseMemory()
{
	streamObject->Release();
}

long decoder::EntropyController::GetBaseOffset()
{
	return streamObject->GetEndAddress();
}

void decoder::EntropyController::EndFrame()
{
	streamObject->Reset();
}

void decoder::EntropyController::clearModels()
{
	if (probMods != nullptr)
		delete probMods;
}
