/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Streamer.h"

Streamer::Streamer()
{
	bitCounter = 0;
	emuBuffEmpty = 2;
	paramNumberOfBits = 0;

	address = 0;
	startAddress = 0;
	endAddress = 0;
	END = false;

	emulationByte_2 = GetNextByte();
	emulationByte_1 = GetNextByte();
	emulationByte_0 = GetNextByte();
}

Streamer::Streamer(long startingAddress)
{
	bitCounter = 0;
	emuBuffEmpty = 2;
	paramNumberOfBits = 0;

	address = startingAddress;
	startAddress = startingAddress;
	endAddress = startingAddress;
	END = false;

	emulationByte_2 = GetNextByte();
	emulationByte_1 = GetNextByte();
	emulationByte_0 = GetNextByte();
}

bool Streamer::GetBit()
{
	unsigned char mask = 1;
	bool bit;

	// Check start code prefix and emulation prevention byte is in emulation buffer
	if (bitCounter == 0)
	{
		// Shift emulation buffer 1 byte left
		emulationByte_3 = emulationByte_2;
		emulationByte_2 = emulationByte_1;
		emulationByte_1 = emulationByte_0;
		emulationByte_0 = GetNextByte();
		endAddress++;

		if (!END)
		{
			int code = IsStartCodePrefix();

			// For byte pattern 0x00 0x00 0x03 0x00 0x01 neglect returned code 2
			if (emuBuffEmpty > 2)
			{
				emuBuffEmpty--;
				code = 0;
			}

			if (code == 1)
			{
				emulationByte_3 = GetNextByte();
				emulationByte_2 = GetNextByte();
				emulationByte_1 = GetNextByte();
				emulationByte_0 = GetNextByte();
				endAddress += 4;
			}

			if (code == 2)
			{
				emulationByte_3 = emulationByte_0;
				emulationByte_2 = GetNextByte();
				emulationByte_1 = GetNextByte();
				emulationByte_0 = GetNextByte();
				endAddress += 3;
			}

			if (code == 3)
			{
				emulationByte_1 = emulationByte_0;
				emulationByte_0 = GetNextByte();
				emuBuffEmpty = 3;
				endAddress++;
			}
		}
	}

	mask <<= (7 - bitCounter);
	bit = mask & *emulationByte_3;
	bitCounter++;
	paramNumberOfBits++;

	// Move to next emulationBuffer byte 
	if (bitCounter == 8)
	{
		bitCounter = 0;
	}

	return bit;
}

void Streamer::GetBits(unsigned short * bitString, unsigned int n)
{
	unsigned short mask = 1;

	*bitString = 0;

	for (int i = n - 1; i >= 0; i--)
	{
		mask <<= i;
		if (GetBit())
		{
			*bitString |= mask;
		}
		else
		{
			*bitString &= (~mask);
		}
		mask = 1;
	}
}

void Streamer::ByteAlign()
{
	while (bitCounter != 0)
		GetBit();
}

int Streamer::IsStartCodePrefix()
{
	if ((*emulationByte_3 == 0x00) && (*emulationByte_2 == 0x00) && (*emulationByte_1 == 0x00) &&
		(*emulationByte_0 == 0x01))
		return 1;

	if ((*emulationByte_3 == 0x00) && (*emulationByte_2 == 0x00) && (*emulationByte_1 == 0x01))
		return 2;

	if ((*emulationByte_3 == 0x00) && (*emulationByte_2 == 0x00) && (*emulationByte_1 == 0x03))
		return 3;

	return 0;
}

unsigned char * Streamer::GetNextByte()
{
	if (MemoryManager::IsEOF(address))
	{
		END = true;
		return 0;
	}

	unsigned char * byte = MemoryManager::GetByte(address);
	address++;
	return byte;
}

int Streamer::Release()
{
	int diff = endAddress - startAddress;

	if (diff > 0)
	{
		MemoryManager::Release(startAddress, endAddress);

		startAddress = endAddress;
	}

	return diff;
}

bool Streamer::IsEND()
{
	if (MemoryManager::IsEOF(address))
	{
		END = true;
	}

	return END;
}

void Streamer::CleanUp()
{
	MemoryManager::JoinReadThread();
}

long Streamer::GetEndAddress()
{
	return endAddress;
}

void Streamer::Reset()
{
	MemoryManager::ResetVirtualAddress();
	bitCounter = 0;
	emuBuffEmpty = 2;

	address = 0;
	startAddress = 0;
	endAddress = 0;

	emulationByte_2 = GetNextByte();
	emulationByte_1 = GetNextByte();
	emulationByte_0 = GetNextByte();
}

void Streamer::ReloadBuffers()
{
	for (int i = 0; i < emuBuffEmpty; i++)
		GetNextByte();

	endAddress += emuBuffEmpty;
}

