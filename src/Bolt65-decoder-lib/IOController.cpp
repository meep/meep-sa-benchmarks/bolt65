/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "IOController.h"

decoder::IOController::IOController()
{

}

decoder::IOController::~IOController()
{
}

void decoder::IOController::InitIO(EncodingContext * decCtx)
{
	if (!decCtx->inputFilePath.empty())
	{
		inputBufSize = decCtx->inputBufSize;
		outputBufSize = decCtx->outputBufSize;

		determineInputFileType(decCtx->inputFilePath);

		inputFilePointer = fopen(decCtx->inputFilePath.c_str(), "rb");

		if (inputFilePointer == NULL)
		{
			cout << "Error opening input file: " << decCtx->inputFilePath << endl;
			exit(EXIT_FAILURE);
		}

		outputFile.open(decCtx->outputFilePath.c_str(), ofstream::binary);

		MemoryManager::Init(decCtx->inputBufSize, inputFilePointer);
	}
}

void decoder::IOController::determineInputFileType(string inputFileName)
{
	int delimiter = (int)inputFileName.find_last_of('.');

	string fileExtension = inputFileName.substr(delimiter + 1);

	if (fileExtension != "hevc")
	{
		cout << "Unknown input format type: " << fileExtension << endl;
		exit(EXIT_FAILURE);
	}
}

void decoder::IOController::writeToOutputFile(char* data, int dataSize)
{
	outputFile.write(data, dataSize);
}

void decoder::IOController::closeFiles()
{
	outputFile.close();
	fclose(inputFilePointer);

	MemoryManager::ReleaseBuffer();
}