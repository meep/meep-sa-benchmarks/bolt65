/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CUContext.h"

CUContext::CUContext()
{
	aboveCU = nullptr;
	leftCU = nullptr;
	parent = nullptr;

	cuDepth = -1;
	index = -1;
	predMode = -1;
	cu_skip_flag = 0;

	intraSplitFlag = -1;

	cqt = new CUContext * [4] { nullptr };
}

CUContext::CUContext(CUContext* above_cu, CUContext* left_cu, CUContext* _parent, int _ctuDepth, int _cuIndex)
{
	aboveCU = above_cu;
	leftCU = left_cu;
	parent = _parent;

	cuDepth = _ctuDepth;
	index = _cuIndex;
	predMode = -1;
	cu_skip_flag = 0;

	intraSplitFlag = -1;

	cqt = new CUContext*[4]{ nullptr };
}

CUContext::~CUContext()
{

}

int CUContext::calculateDepthAbove(stack <int> *path, bool downfall)
{
	if (!downfall)
	{
		if (parent != nullptr)
			path->push(index);

		if (index == 0 || index == 1)
		{
			if (parent != nullptr)
				return parent->calculateDepthAbove(path, false);
			else if (aboveCU != nullptr)
				return aboveCU->calculateDepthAbove(path, true);
			else
				return -1;
		}
		else
		{
			return parent->calculateDepthAbove(path, true);
		}
	}
	else
	{
		if (path->size() > 0)
		{
			int downIndex = path->top();
			path->pop();

			if (downIndex == 0)
			{
				if (cqt[2] != nullptr)
					return cqt[2]->calculateDepthAbove(path, true);

				return cuDepth;
			}
			else if (downIndex == 1)
			{
				if (cqt[3] != nullptr)
					return cqt[3]->calculateDepthAbove(path, true);

				return cuDepth;
			}
			else if (downIndex == 2)
			{
				if (cqt[0] != nullptr)
					return cqt[0]->calculateDepthAbove(path, true);

				return cuDepth;
			}
			else if (downIndex == 3)
			{
				if (cqt[1] != nullptr)
					return cqt[1]->calculateDepthAbove(path, true);

				return cuDepth;
			}
			else
				return -1;
		}
		else
		{
			if (cqt[2] != nullptr)
				return cqt[2]->calculateDepthAbove(path, true);

			return cuDepth;
		}

	}
}

int CUContext::calculateDepthLeft(stack <int> *path, bool downfall)
{
	if (!downfall)
	{
		if (parent != nullptr)
			path->push(index);

		if (index == 0 || index == 2)
		{

			if (parent != nullptr)
				return parent->calculateDepthLeft(path, false);
			else if (leftCU != nullptr)
				return leftCU->calculateDepthLeft(path, true);
			else
				return -1;
		}
		else
		{
			return parent->calculateDepthLeft(path, true);
		}
	}
	else
	{
		if (path->size() > 0)
		{
			int downIndex = path->top();
			path->pop();

			if (downIndex == 0)
			{
				if (cqt[1] != nullptr)
					return cqt[1]->calculateDepthLeft(path, true);

				return cuDepth;
			}
			else if (downIndex == 2)
			{
				if (cqt[3] != nullptr)
					return cqt[3]->calculateDepthLeft(path, true);

				return cuDepth;
			}
			else if (downIndex == 1)
			{
				if (cqt[1] != nullptr)
					return cqt[0]->calculateDepthLeft(path, true);

				return cuDepth;
			}
			else if (downIndex == 3)
			{
				if (cqt[2] != nullptr)
					return cqt[2]->calculateDepthLeft(path, true);

				return cuDepth;
			}
			else
				return -1;
		}
		else
		{
			if (cqt[1] != nullptr)
				return cqt[1]->calculateDepthLeft(path, true);

			return cuDepth;
		}

	}
}

int CUContext::calculateCuSkipLeft(stack <int> *path, bool downfall)
{
	if (!downfall)
	{
		if (parent != nullptr)
			path->push(index);

		if (index == 0 || index == 2)
		{

			if (parent != nullptr)
				return parent->calculateCuSkipLeft(path, false);
			else if (leftCU != nullptr)
				return leftCU->calculateCuSkipLeft(path, true);
			else
				return 0;
		}
		else
		{
			return parent->calculateCuSkipLeft(path, true);
		}
	}
	else
	{
		if (path->size() > 0)
		{
			int downIndex = path->top();
			path->pop();

			if (downIndex == 0)
			{
				if (cqt[1] != nullptr)
					return cqt[1]->calculateCuSkipLeft(path, true);

				return cu_skip_flag;
			}
			else if (downIndex == 2)
			{
				if (cqt[3] != nullptr)
					return cqt[3]->calculateCuSkipLeft(path, true);

				return cu_skip_flag;
			}
			else if (downIndex == 1)
			{
				if (cqt[1] != nullptr)
					return cqt[0]->calculateCuSkipLeft(path, true);

				return cu_skip_flag;
			}
			else if (downIndex == 3)
			{
				if (cqt[2] != nullptr)
					return cqt[2]->calculateCuSkipLeft(path, true);

				return cu_skip_flag;
			}
		}
		else
		{
			if (cqt[1] != nullptr)
				return cqt[1]->calculateCuSkipLeft(path, true);

			return cu_skip_flag;
		}

	}
}

int CUContext::calculateCuSkipAbove(stack <int> *path, bool downfall)
{
	if (!downfall)
	{
		if (parent != nullptr)
			path->push(index);

		if (index == 0 || index == 1)
		{
			if (parent != nullptr)
				return parent->calculateCuSkipAbove(path, false);
			else if (aboveCU != nullptr)
				return aboveCU->calculateCuSkipAbove(path, true);
			else
				return 0;
		}
		else
		{
			return parent->calculateCuSkipAbove(path, true);
		}
	}
	else
	{
		if (path->size() > 0)
		{
			int downIndex = path->top();
			path->pop();

			if (downIndex == 0)
			{
				if (cqt[2] != nullptr)
					return cqt[2]->calculateCuSkipAbove(path, true);

				return cu_skip_flag;
			}
			else if (downIndex == 1)
			{
				if (cqt[3] != nullptr)
					return cqt[3]->calculateCuSkipAbove(path, true);

				return cu_skip_flag;
			}
			else if (downIndex == 2)
			{
				if (cqt[0] != nullptr)
					return cqt[0]->calculateCuSkipAbove(path, true);

				return cu_skip_flag;
			}
			else if (downIndex == 3)
			{
				if (cqt[1] != nullptr)
					return cqt[1]->calculateCuSkipAbove(path, true);

				return cu_skip_flag;
			}
		}
		else
		{
			if (cqt[2] != nullptr)
				return cqt[2]->calculateCuSkipAbove(path, true);

			return cu_skip_flag;
		}

	}
}

int CUContext::getPredModeAbove(stack <int> *path, bool downfall)
{
	if (!downfall)
	{
		if (parent != nullptr)
			path->push(index);

		if (index == 0 || index == 1)
		{
			if (parent != nullptr)
				return parent->getPredModeAbove(path, false);
			else if (aboveCU != nullptr)
				return aboveCU->getPredModeAbove(path, true);
			else
				return -1;
		}
		else
		{
			return parent->getPredModeAbove(path, true);
		}
	}
	else
	{
		if (path->size() > 0)
		{
			int downIndex = path->top();
			path->pop();

			if (downIndex == 0)
			{
				if (cqt[2] != nullptr)
					return cqt[2]->getPredModeAbove(path, true);

				return predMode;
			}
			else if (downIndex == 1)
			{
				if (cqt[3] != nullptr)
					return cqt[3]->getPredModeAbove(path, true);

				return predMode;
			}
			else if (downIndex == 2)
			{
				if (cqt[0] != nullptr)
					return cqt[0]->getPredModeAbove(path, true);

				return predMode;
			}
			else if (downIndex == 3)
			{
				if (cqt[1] != nullptr)
					return cqt[1]->getPredModeAbove(path, true);

				return predMode;
			}
		}
		else
		{
			if (cqt[2] != nullptr)
				return cqt[2]->getPredModeAbove(path, true);

			return predMode;
		}

	}
}

int CUContext::getPredModeLeft(stack <int> *path, bool downfall)
{
	if (!downfall)
	{
		if (parent != nullptr)
			path->push(index);

		if (index == 0 || index == 2)
		{

			if (parent != nullptr)
				return parent->getPredModeLeft(path, false);
			else if (leftCU != nullptr)
				return leftCU->getPredModeLeft(path, true);
			else
				return -1;
		}
		else
		{
			return parent->getPredModeLeft(path, true);
		}
	}
	else
	{
		if (path->size() > 0)
		{
			int downIndex = path->top();
			path->pop();

			if (downIndex == 0)
			{
				if (cqt[1] != nullptr)
					return cqt[1]->getPredModeLeft(path, true);

				return predMode;
			}
			else if (downIndex == 2)
			{
				if (cqt[3] != nullptr)
					return cqt[3]->getPredModeLeft(path, true);

				return predMode;
			}
			else if (downIndex == 1)
			{
				if (cqt[1] != nullptr)
					return cqt[0]->getPredModeLeft(path, true);

				return predMode;
			}
			else if (downIndex == 3)
			{
				if (cqt[2] != nullptr)
					return cqt[2]->getPredModeLeft(path, true);

				return predMode;
			}
		}
		else
		{
			if (cqt[1] != nullptr)
				return cqt[1]->getPredModeLeft(path, true);

			return predMode;
		}

	}
}

bool CUContext::isCUUpperEdgeOfCTU()
{
	if (index == 0 || index == 1)
	{
		if (parent != nullptr)
			return parent->isCUUpperEdgeOfCTU();
		else
			return true;
	}
	else
	{
		return false;
	}

}

int CUContext::getIntraPredictionModeLuma(bool PrevIntraLumaPredFlag, int MpmIdx, int RemIntraLumaPredMode, int leftPredMode, int abovePredMode, bool isCuOnUpperEdge)
{
	int candModeList[3];

	bool leftIsAvailable = leftPredMode != -1;
	bool aboveIsAvailable = abovePredMode != -1;
	int candModeA;
	int candModeB;

	if (aboveIsAvailable)
	{
		if (isCuOnUpperEdge)
			candModeB = 1;
		else
			candModeB = abovePredMode;
	}
	else
	{
		candModeB = 1;
	}

	if (leftIsAvailable)
		candModeA = leftPredMode;
	else
		candModeA = 1;


	if (candModeA == candModeB)
	{
		if (candModeA < 2)
		{
			candModeList[0] = 0;
			candModeList[1] = 1;
			candModeList[2] = 26;
		}
		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = 2 + ((candModeA + 29) % 32);
			candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
		}
	}

	else
	{
		candModeList[0] = candModeA;
		candModeList[1] = candModeB;

		if (candModeList[0] != 0 && candModeList[1] != 0)
		{
			candModeList[2] = 0;
		}
		else
		{
			if (candModeList[0] != 1 && candModeList[1] != 1)
			{
				candModeList[2] = 1;
			}
			else
			{
				candModeList[2] = 26;
			}
		}
	}


	if (PrevIntraLumaPredFlag)
	{
		return candModeList[MpmIdx];
	}
	else
	{
		int intraLuma = RemIntraLumaPredMode;

		int tmp;
		if (candModeList[0] > candModeList[1])
		{
			tmp = candModeList[1];
			candModeList[1] = candModeList[0];
			candModeList[0] = tmp;
		}

		if (candModeList[0] > candModeList[2])
		{
			tmp = candModeList[2];
			candModeList[2] = candModeList[0];
			candModeList[0] = tmp;
		}

		if (candModeList[1] > candModeList[2])
		{
			tmp = candModeList[2];
			candModeList[2] = candModeList[1];
			candModeList[1] = tmp;
		}

		if (RemIntraLumaPredMode + 3 > candModeList[2])
			intraLuma = intraLuma + 3;
		else
			if (RemIntraLumaPredMode + 2 > candModeList[1])
				intraLuma = intraLuma + 2;
			else
				if (RemIntraLumaPredMode + 1 > candModeList[0])
					intraLuma = intraLuma + 1;


		return intraLuma;
	}

}

int CUContext::getIntraPredictionModeChroma(int intra_chroma_pred_mode, int IntraPredModeY)
{
	if (intra_chroma_pred_mode == 0)
	{
		if (IntraPredModeY == 0)
			return 34;
		else
			return 0;
	}
	else if (intra_chroma_pred_mode == 1)
	{
		if (IntraPredModeY == 26)
			return 34;
		else
			return 26;
	}
	else if (intra_chroma_pred_mode == 2)
	{
		if (IntraPredModeY == 10)
			return 34;
		else
			return 10;
	}
	else if (intra_chroma_pred_mode == 3)
	{
		if (IntraPredModeY == 1)
			return 34;
		else
			return 1;
	}
	else
		return IntraPredModeY;
}

void CUContext::refreshScanIndex(ContextMemory * ctxMem)
{
	if (ctxMem->pred_mode_flag == 1)
	{
		if ((ctxMem->log2TrafoSize == 3 && ctxMem->colorIdx == 0) || ctxMem->log2TrafoSize == 2)
		{
			if (ctxMem->predMode < 6 ||
				(ctxMem->predMode > 14 && ctxMem->predMode < 22) ||
				(ctxMem->predMode > 30 && ctxMem->predMode < 35))
			{
				ctxMem->scanIdx = DiagonalScan;
			}
			else if (ctxMem->predMode > 21 && ctxMem->predMode < 31)
			{
				ctxMem->scanIdx = HorizontalScan;
			}
			else if (ctxMem->predMode > 5 && ctxMem->predMode < 15)
			{
				ctxMem->scanIdx = VerticalScan;
			}
		}
		else
		{
			ctxMem->scanIdx = DiagonalScan;
		}
	}
	else
	{
		ctxMem->scanIdx = DiagonalScan;
	}
}

void CUContext::deleteAllNodes()
{
	if (cqt[0] != nullptr)
	{
		cqt[0]->deleteAllNodes();
		delete cqt[0];
	}

	if (cqt[1] != nullptr)
	{
		cqt[1]->deleteAllNodes();
		delete cqt[1];
	}

	if (cqt[2] != nullptr)
	{
		cqt[2]->deleteAllNodes();
		delete cqt[2];
	}

	if (cqt[3] != nullptr)
	{
		cqt[3]->deleteAllNodes();
		delete cqt[3];
	}

	delete cqt;
}