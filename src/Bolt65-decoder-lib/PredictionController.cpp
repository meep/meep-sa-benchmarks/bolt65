/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "PredictionController.h"

decoder::PredictionController::PredictionController()
{
	decCtx = nullptr;
	partition = nullptr;
	dpbm = nullptr;
	decTQController = nullptr;
}

decoder::PredictionController::~PredictionController()
{
}

void decoder::PredictionController::Init(TQController* _decTQController, DPBManager* _dpbm, EncodingContext* _decCtx)
{
	decTQController = _decTQController;
	dpbm = _dpbm;
	decCtx = _decCtx;
}

void decoder::PredictionController::PerformInversePrediction(Partition* _partition)
{

	int i;
	partition = _partition;

	for (i = 0; i < partition->numberOfCTUs; i++)
	{
		InversePredictBlock(partition->ctuTree[i]);
	}


}

void decoder::PredictionController::decodeAMVPPredDecision(PU* pu, CU* cu)
{
	int MVPx[2], MVPy[2], MVPt[2];

	pu->MVt1 = 1;
	DeriveCandidatesForAMVP(MVPx, MVPy, MVPt, pu);

	int signX = pu->MDsignx1 == 0 ? 1 : -1;
	int signY = pu->MDsigny1 == 0 ? 1 : -1;

	pu->MVx1 = MVPx[pu->candidateIndex] + signX * pu->MDabsx1;
	pu->MVy1 = MVPy[pu->candidateIndex] + signY * pu->MDabsy1;

}

void decoder::PredictionController::decodeMergePredDecision(PU* pu, CU* cu)
{
	int MVPx[10], MVPy[10], MVPt[10];

	pu->MVt1 = 1;
	DeriveCandidatesForMerge(MVPx, MVPy, MVPt, cu, pu);

	pu->MVx1 = MVPx[pu->candidateIndex];
	pu->MVy1 = MVPy[pu->candidateIndex];

}

void decoder::PredictionController::InversePredictBlock(CU* cu)
{

	// if is not leaf...
	if (cu->hasChildren)
	{
		if (cu->children[0]->isInFrame)
			InversePredictBlock(cu->children[0]);

		if (cu->children[1]->isInFrame)
			InversePredictBlock(cu->children[1]);

		if (cu->children[2]->isInFrame)
			InversePredictBlock(cu->children[2]);

		if (cu->children[3]->isInFrame)
			InversePredictBlock(cu->children[3]);
	}
	else // if is leaf
	{
		if (cu->CuPredMode == MODE_INTRA)
		{
			if (cu->PartMode == INTRA_PART_2Nx2N) //possible implicit split when 64x64 ctu is used
			{
				if (cu->transformTree && cu->transformTree->hasChildren)
				{
					for (int i = 0; i < 4; i++) //pretpostavljamo da je za intra pu = tu uvijek
					{
						InversePredictBlockIntraTransformUnit(cu->transformTree->children[i], cu->IntraPredModeY[0], cu->IntraPredModeChroma, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
					}
				}
				else
				{
					InversePredictBlockIntraTransformUnit(cu->transformTree, cu->IntraPredModeY[0], cu->IntraPredModeChroma, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
				}
			}
			else //PART NxN
			{
				for (int i = 0; i < 4; i++) //pretpostavljamo da je za intra pu = tu uvijek
				{
					InversePredictBlockIntraTransformUnit(cu->transformTree->children[i], cu->IntraPredModeY[i], cu->IntraPredModeChroma, cu->cbY.cbQP, cu->cbU.cbQP, cu->cbV.cbQP, cu->depth);
				}
			}
		}

		if (cu->CuPredMode == MODE_INTER)
		{
			InversePredictBlockInter(cu);
		}
	}
}

void decoder::PredictionController::InversePredictBlockIntraTransformUnit(TU* tu, int intraPredModeY, int intraPredModeChroma, int qpY, int qpU, int qpV, int _depth)
{

	unsigned char refSamplesU[4 * 32 + 1];
	unsigned char refSamplesV[4 * 32 + 1];
	bool refSamplesUExist[4 * 32 + 1];
	bool refSamplesVExist[4 * 32 + 1];
	unsigned char predictedBlockU[32 * 32];
	unsigned char predictedBlockV[32 * 32];


	unsigned char refSamplesY[4 * 32 + 1];
	bool refSamplesYExist[4 * 32 + 1];
	unsigned char predictedBlockY[32 * 32];
	int16_t reconstructedResidualBlockY[32 * 32];
	int16_t reconstructedResidualBlockU[32 * 32];
	int16_t reconstructedResidualBlockV[32 * 32];
	unsigned char reconstructedBlockY[32 * 32];
	unsigned char reconstructedBlockU[32 * 32];
	unsigned char reconstructedBlockV[32 * 32];

	BlockPartition::getReferenceSamples(tu->tbY.startingIndexInFrame, tu->tbY.startingIndex, tu->tbY.transformBlockSize, decCtx->width, decCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesY, refSamplesYExist);

	Intraprediction::GenerateIntraPredictedBlock(intraPredModeY, refSamplesY, refSamplesYExist, predictedBlockY, 0, tu->tbY.transformBlockSize);

	if (tu->tbY.isZeroMatrix)
	{
		BlockPartition::fill1dBlockByStartingIndex(predictedBlockY, tu->tbY.startingIndexInFrame, tu->tbY.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
	}
	else
	{
		decTQController->PerformInverseTransformAndQuantization(qpY, tu->tbY.QTResidual, tu->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTRA);
		ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, tu->tbY.transformBlockSize, tu->tbY.transformBlockSize, 0);
		BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockY, tu->tbY.startingIndexInFrame, tu->tbY.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
	}


	if (tu->tbV.transformBlockSize < 4) // PART NxN, calculate chroma only for first child
	{
		if (tu->index == 0)
		{

			BlockPartition::getReferenceSamples(tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.startingIndex, tu->parent->tbU.transformBlockSize, decCtx->width, decCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesU, refSamplesUExist);
			BlockPartition::getReferenceSamples(tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.startingIndex, tu->parent->tbV.transformBlockSize, decCtx->width, decCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesV, refSamplesVExist);


			Intraprediction::GenerateIntraPredictedBlock(intraPredModeChroma, refSamplesU, refSamplesUExist, predictedBlockU, 1, tu->parent->tbU.transformBlockSize);
			Intraprediction::GenerateIntraPredictedBlock(intraPredModeChroma, refSamplesV, refSamplesVExist, predictedBlockV, 2, tu->parent->tbV.transformBlockSize);

			if (tu->parent->tbU.isZeroMatrix)
			{
				BlockPartition::fill1dBlockByStartingIndex(predictedBlockU, tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
			}
			else
			{
				decTQController->PerformInverseTransformAndQuantization(qpU, tu->parent->tbU.QTResidual, tu->parent->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTRA);
				ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, tu->parent->tbU.transformBlockSize, tu->parent->tbU.transformBlockSize, 0);
				BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockU, tu->parent->tbU.startingIndexInFrame, tu->parent->tbU.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
			}

			if (tu->parent->tbV.isZeroMatrix)
			{
				BlockPartition::fill1dBlockByStartingIndex(predictedBlockV, tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
			}
			else
			{
				decTQController->PerformInverseTransformAndQuantization(qpV, tu->parent->tbV.QTResidual, tu->parent->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTRA);
				ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, tu->parent->tbV.transformBlockSize, tu->parent->tbV.transformBlockSize, 0);
				BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockV, tu->parent->tbV.startingIndexInFrame, tu->parent->tbV.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
			}
		}

	}
	else
	{
		BlockPartition::getReferenceSamples(tu->tbU.startingIndexInFrame, tu->tbU.startingIndex, tu->tbU.transformBlockSize, decCtx->width, decCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesU, refSamplesUExist);
		BlockPartition::getReferenceSamples(tu->tbV.startingIndexInFrame, tu->tbV.startingIndex, tu->tbV.transformBlockSize, decCtx->width, decCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesV, refSamplesVExist);


		Intraprediction::GenerateIntraPredictedBlock(intraPredModeChroma, refSamplesU, refSamplesUExist, predictedBlockU, 1, tu->tbU.transformBlockSize);
		Intraprediction::GenerateIntraPredictedBlock(intraPredModeChroma, refSamplesV, refSamplesVExist, predictedBlockV, 2, tu->tbV.transformBlockSize);

		if (tu->tbU.isZeroMatrix)
		{
			BlockPartition::fill1dBlockByStartingIndex(predictedBlockU, tu->tbU.startingIndexInFrame, tu->tbU.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
		}
		else
		{
			decTQController->PerformInverseTransformAndQuantization(qpU, tu->tbU.QTResidual, tu->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTRA);
			ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, tu->tbU.transformBlockSize, tu->tbU.transformBlockSize, 0);
			BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockU, tu->tbU.startingIndexInFrame, tu->tbU.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
		}


		if (tu->tbV.isZeroMatrix)
		{
			BlockPartition::fill1dBlockByStartingIndex(predictedBlockV, tu->tbV.startingIndexInFrame, tu->tbV.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
		}
		else
		{
			decTQController->PerformInverseTransformAndQuantization(qpV, tu->tbV.QTResidual, tu->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTRA);
			ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, tu->tbV.transformBlockSize, tu->tbV.transformBlockSize, 0);
			BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockV, tu->tbV.startingIndexInFrame, tu->tbV.transformBlockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
		}
	}
}

void decoder::PredictionController::InversePredictBlockInter(CU* cu)
{

	unsigned char reconstructedBlockY[64 * 64];
	unsigned char reconstructedBlockU[32 * 32];
	unsigned char reconstructedBlockV[32 * 32];

	unsigned char predictedBlockY[64 * 64];
	unsigned char predictedBlockU[32 * 32];
	unsigned char predictedBlockV[32 * 32];

	for (int i = 0; i < cu->numOfPUs; ++i)
	{
		cu->predictionUnits[i].isProcessed = true;

		if (cu->predictionUnits[i].predictionMode == AMVP)
			decodeAMVPPredDecision(&cu->predictionUnits[i], cu);
		else
			decodeMergePredDecision(&cu->predictionUnits[i], cu);

		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockY, decCtx->width, decCtx->height, cu->predictionUnits[i].pbY.width, cu->predictionUnits[i].pbY.height, i, cu->predictionUnits[i].pbY.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbY.blockSize, decCtx->width, Luma);
		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockU, decCtx->width, decCtx->height, cu->predictionUnits[i].pbU.width, cu->predictionUnits[i].pbU.height, i, cu->predictionUnits[i].pbU.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbU.blockSize, decCtx->width / 2, ChromaCb);
		Interprediction::GenerateInterPredictedBlock(dpbm->dpb->frames[0]->reconstructed_payload, predictedBlockV, decCtx->width, decCtx->height, cu->predictionUnits[i].pbV.width, cu->predictionUnits[i].pbV.height, i, cu->predictionUnits[i].pbV.startingIndexInFrame, cu->predictionUnits[i].MVx1, cu->predictionUnits[i].MVy1, cu->cbV.blockSize, decCtx->width / 2, ChromaCr);
	}


	//inverse transform block
	//case when root tu is not split.
	if (cu->transformTree->hasChildren == false)
	{
		int16_t reconstructedResidualBlockY[64 * 64]{ 0 };
		int16_t reconstructedResidualBlockU[32 * 32]{ 0 };
		int16_t reconstructedResidualBlockV[32 * 32]{ 0 };

		if (!cu->transformTree->tbY.isZeroMatrix)
			decTQController->PerformInverseTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->tbY.QTResidual, cu->transformTree->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTER);
		if (!cu->transformTree->tbU.isZeroMatrix)
			decTQController->PerformInverseTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->tbU.QTResidual, cu->transformTree->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTER);
		if (!cu->transformTree->tbV.isZeroMatrix)
			decTQController->PerformInverseTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->tbV.QTResidual, cu->transformTree->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTER);
		ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, cu->transformTree->tbY.transformBlockSize, cu->cbY.blockSize, 0);
		ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, cu->transformTree->tbU.transformBlockSize, cu->cbU.blockSize, 0);
		ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, cu->transformTree->tbV.transformBlockSize, cu->cbV.blockSize, 0);
	}

	else if (cu->transformTree->hasChildren == true)
	{
		//2 cases here
		//first when children luma size is greater or equal to 8
		if (cu->transformTree->tbY.transformBlockSize > 8)
		{

			for (int i = 0; i < 4; ++i)
			{

				int16_t reconstructedResidualBlockY[32 * 32]{ 0 };
				int16_t reconstructedResidualBlockU[16 * 16]{ 0 };
				int16_t reconstructedResidualBlockV[16 * 16]{ 0 };

				if (!cu->transformTree->children[i]->tbY.isZeroMatrix)
					decTQController->PerformInverseTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->children[i]->tbY.QTResidual, cu->transformTree->children[i]->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTER);
				if (!cu->transformTree->children[i]->tbU.isZeroMatrix)
					decTQController->PerformInverseTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->children[i]->tbU.QTResidual, cu->transformTree->children[i]->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTER);
				if (!cu->transformTree->children[i]->tbV.isZeroMatrix)
					decTQController->PerformInverseTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->children[i]->tbV.QTResidual, cu->transformTree->children[i]->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTER);

				ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, cu->transformTree->children[i]->tbY.transformBlockSize, cu->cbY.blockSize, i);
				ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, cu->transformTree->children[i]->tbU.transformBlockSize, cu->cbU.blockSize, i);
				ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, cu->transformTree->children[i]->tbV.transformBlockSize, cu->cbV.blockSize, i);
			}
		}
		else
		{



			for (int i = 0; i < 4; ++i)
			{
				int16_t reconstructedResidualBlockY[8 * 8]{ 0 };
				if (!cu->transformTree->children[i]->tbY.isZeroMatrix)
					decTQController->PerformInverseTransformAndQuantization(cu->cbY.cbQP, cu->transformTree->children[i]->tbY.QTResidual, cu->transformTree->children[i]->tbY.transformBlockSize, reconstructedResidualBlockY, 0, MODE_INTER);
				ComUtil::AddResidual(reconstructedResidualBlockY, predictedBlockY, reconstructedBlockY, cu->transformTree->children[i]->tbY.transformBlockSize, cu->cbY.blockSize, i);

			}
			int16_t reconstructedResidualBlockU[8 * 8]{ 0 };
			int16_t reconstructedResidualBlockV[8 * 8]{ 0 };
			if (!cu->transformTree->tbU.isZeroMatrix)
				decTQController->PerformInverseTransformAndQuantization(cu->cbU.cbQP, cu->transformTree->tbU.QTResidual, cu->transformTree->tbU.transformBlockSize, reconstructedResidualBlockU, 1, MODE_INTER);
			if (!cu->transformTree->tbV.isZeroMatrix)
				decTQController->PerformInverseTransformAndQuantization(cu->cbV.cbQP, cu->transformTree->tbV.QTResidual, cu->transformTree->tbV.transformBlockSize, reconstructedResidualBlockV, 2, MODE_INTER);
			ComUtil::AddResidual(reconstructedResidualBlockU, predictedBlockU, reconstructedBlockU, cu->transformTree->tbU.transformBlockSize, cu->cbU.blockSize, 0);
			ComUtil::AddResidual(reconstructedResidualBlockV, predictedBlockV, reconstructedBlockV, cu->transformTree->tbV.transformBlockSize, cu->cbV.blockSize, 0);
		}
	}

	//inverse predict block

	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockY, cu->cbY.startingIndexInFrame, cu->cbY.blockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockU, cu->cbU.startingIndexInFrame, cu->cbU.blockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);
	BlockPartition::fill1dBlockByStartingIndex(reconstructedBlockV, cu->cbV.startingIndexInFrame, cu->cbV.blockSize, partition->reconstructed_payload, partition->isReconstructed, decCtx->width, decCtx->height);

}

void decoder::PredictionController::DeriveCandidatesForAMVP(int* MvpX, int* MvpY, int* MvpT, PU* pu)
{
	bool isA0Available;
	bool isA1Available;
	bool isB0Available;
	bool isB1Available;
	bool isB2Available;

	int puUpLeftIdx = pu->pbY.startingIndex;
	int puUpRightIdx = pu->pbY.startingIndex + pu->pbY.width - 1;
	int puBottomLeftIdx = pu->pbY.startingIndex + pu->pbY.height * partition->width;

	int puUpLeftIdxRow = puUpLeftIdx / partition->width;
	int puUpLeftIdxColumn = puUpLeftIdx % partition->width;

	int puUpRightIdxRow = puUpRightIdx / partition->width;
	int puUpRightIdxColumn = (puUpRightIdx % partition->width);

	int puBottomLeftIdxIdxRow = (puBottomLeftIdx / partition->width) - 1;
	int puBottomLeftIdxColumn = puBottomLeftIdx % partition->width;

	PU puA0;
	PU puA1;
	PU puB0;
	PU puB1;
	PU puB2;


	isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA0);
	isA1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA1);
	isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, decCtx->ctbSize, puB0);
	isB1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn, partition->width, partition->height, decCtx->ctbSize, puB1);
	isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puB2);

	bool AisAvailable = false;
	bool BisAvailable = false;
	bool CisAvailable = false;
	int Ax, Ay, At, Bx, By, Bt;

	//TODO Scaling is done only if both pictures are in short term ref pics

	//A0, A1 available?
	if (isA0Available || isA1Available)
	{
		//First pass, find non-scaled MV's
		if (isA0Available && pu->MVt1 == puA0.MVt1)
		{
			Ax = puA0.MVx1;
			Ay = puA0.MVy1;
			At = puA0.MVt1;
			AisAvailable = true;
		}
		else if (isA1Available && pu->MVt1 == puA1.MVt1)
		{
			Ax = puA1.MVx1;
			Ay = puA1.MVy1;
			At = puA1.MVt1;
			AisAvailable = true;
		}

		//Second pass, find scaled MV's
		else if (isA0Available && pu->MVt1 != puA0.MVt1)
		{
			int td = ComUtil::clip3(-128, 127, puA0.MVt1);
			int tb = ComUtil::clip3(-128, 127, pu->MVt1);

			ScaleCandidate(puA0.MVx1, puA0.MVy1, tb, td, Ax, Ay);
			At = puA0.MVt1;
			AisAvailable = true;
		}
		else if (isA1Available && pu->MVt1 != puA1.MVt1)
		{
			int td = ComUtil::clip3(-128, 127, puA1.MVt1);
			int tb = ComUtil::clip3(-128, 127, pu->MVt1);

			ScaleCandidate(puA1.MVx1, puA1.MVy1, tb, td, Ax, Ay);
			At = puA1.MVt1;
			AisAvailable = true;
		}
	}

	//B0,B1, B2 available
	if (isB0Available || isB1Available || isB2Available)
	{
		int BTempFound = false;
		int BxTemp, ByTemp, BtTemp, BTemp;

		//find non-scaled MV
		if (isB0Available && pu->MVt1 == puB0.MVt1)
		{
			BxTemp = puB0.MVx1;
			ByTemp = puB0.MVy1;
			BtTemp = puB0.MVt1;
			BTempFound = true;
			BTemp = 0;
		}
		else if (isB1Available && pu->MVt1 == puB1.MVt1)
		{
			BxTemp = puB1.MVx1;
			ByTemp = puB1.MVy1;
			BtTemp = puB1.MVt1;
			BTempFound = true;
			BTemp = 1;
		}
		else if (isB2Available && pu->MVt1 == puB2.MVt1)
		{
			BxTemp = puB2.MVx1;
			ByTemp = puB2.MVy1;
			BtTemp = puB2.MVt1;
			BTempFound = true;
			BTemp = 2;
		}

		//first non-scaled MV found
		if (BTempFound)
		{
			//A0, A1 available
			if (!AisAvailable)
			{
				//set A
				Ax = BxTemp;
				Ay = ByTemp;
				At = BtTemp;
				AisAvailable = true;


			}
			//A0 or A1 available, set non-scaled as B
			else
			{
				Bx = BxTemp;
				By = ByTemp;
				Bt = BtTemp;
				BisAvailable = true;
			}
		}
		//non-scaled MV not found
		else
		{
			if (!AisAvailable)
			{
				if (isB0Available && pu->MVt1 != puB0.MVt1)
				{
					int td = ComUtil::clip3(-128, 127, puB0.MVt1);
					int tb = ComUtil::clip3(-128, 127, pu->MVt1);

					ScaleCandidate(puB0.MVx1, puB0.MVy1, tb, td, Bx, By);
					Bt = puB0.MVt1;
					BisAvailable = true;
				}
				else if (isB1Available && pu->MVt1 != puB1.MVt1)
				{
					int td = ComUtil::clip3(-128, 127, puB1.MVt1);
					int tb = ComUtil::clip3(-128, 127, pu->MVt1);

					ScaleCandidate(puB1.MVx1, puB1.MVy1, tb, td, Bx, By);
					Bt = puB1.MVt1;
					BisAvailable = true;
				}
				else if (isB2Available && pu->MVt1 != puB2.MVt1)
				{
					int td = ComUtil::clip3(-128, 127, puB2.MVt1);
					int tb = ComUtil::clip3(-128, 127, pu->MVt1);

					ScaleCandidate(puB2.MVx1, puB2.MVy1, tb, td, Bx, By);
					Bt = puB2.MVt1;
					BisAvailable = true;
				}
			}
		}
	}


	//End spatial candidates
	bool temporalNeeded = !((AisAvailable && BisAvailable) && !(Ax == Bx && Ay == By && At == Bt));

	//Temporal candidates (can be switched off)
	if (decCtx->useTemporalAMVP && temporalNeeded)
	{
		bool isC0Available = false;
		bool isC1Available = true;

		////CU* C0 = BlockPartition::getRefRightDownCodingUnitByIndex(dpbm->dpb->frames[0]->ctuTree, pu, isC0Available, partition->width, partition->height);
		////CU* C1 = BlockPartition::getRefCenterCodingUnit(dpbm->dpb->frames[0]->ctuTree, cu);

		//isC0Available = isC0Available && C0->CuPredMode == MODE_INTER;
		//isC1Available = C1->CuPredMode == MODE_INTER;

		//if (isC0Available)
		//{
		//	int td = ComUtil::clip3(-128, 127, C0->predictionUnits[0].MVt1);
		//	int tb = ComUtil::clip3(-128, 127, pu->MVt1);

		//	ScaleCandidate(C0->predictionUnits[0].MVx1, C0->predictionUnits[0].MVy1, tb, td, Cx, Cy);
		//	Ct = C0->predictionUnits[0].MVt1;
		//	CisAvailable = true;
		//}
		//else if (isC1Available)
		//{
		//	int td = ComUtil::clip3(-128, 127, C1->predictionUnits[0].MVt1);
		//	int tb = ComUtil::clip3(-128, 127, pu->MVt1);

		//	ScaleCandidate(C1->predictionUnits[0].MVx1, C1->predictionUnits[0].MVy1, tb, td, Cx, Cy);
		//	Ct = C1->predictionUnits[0].MVt1;
		//	CisAvailable = true;
		//}
	}
	// End temporal candidates



	int i = 0;
	//If candidates are not found as spatial or temporal candidates fill them with zero vectors
	if (AisAvailable)
	{
		MvpX[i] = Ax;
		MvpY[i] = Ay;
		MvpT[i] = At;
		i++;
		if (BisAvailable && !(Ax == Bx && Ay == By && At == Bt))
		{
			MvpX[i] = Bx;
			MvpY[i] = By;
			MvpT[i] = Bt;
			i++;
		}
	}
	else if (BisAvailable)
	{
		MvpX[i] = Bx;
		MvpY[i] = By;
		MvpT[i] = Bt;
		i++;
	}
	if (i < 2 && CisAvailable)
	{
		/*MvpX[i] = Cx;
		MvpY[i] = Cy;
		MvpT[i] = Ct;
		i++;*/
	}
	while (i < 2)
	{
		MvpX[i] = 0;
		MvpY[i] = 0;
		MvpT[i] = pu->MVt1;
		i++;
	}


}

void decoder::PredictionController::DeriveCandidatesForMerge(int* MergeCandListX, int* MergeCandListY, int* MergeCandListT, CU* cu, PU* pu)
{
	int i = 0;

	int numOfMergeCandidates = 5;

	//defined in standard
	int maxSpatialCandidates = 4;

	bool isA0Available;
	bool isA1Available;
	bool isB0Available;
	bool isB1Available;
	bool isB2Available;

	int puUpLeftIdx = pu->pbY.startingIndex;
	int puUpRightIdx = pu->pbY.startingIndex + pu->pbY.width - 1;
	int puBottomLeftIdx = pu->pbY.startingIndex + pu->pbY.height * partition->width;

	int puUpLeftIdxRow = puUpLeftIdx / partition->width;
	int puUpLeftIdxColumn = puUpLeftIdx % partition->width;

	int puUpRightIdxRow = puUpRightIdx / partition->width;
	int puUpRightIdxColumn = (puUpRightIdx % partition->width);

	int puBottomLeftIdxIdxRow = (puBottomLeftIdx / partition->width) - 1;
	int puBottomLeftIdxColumn = puBottomLeftIdx % partition->width;

	PU puA0;
	PU puA1;
	PU puB0;
	PU puB1;
	PU puB2;


	if ((cu->PartMode == PART_2NxN || cu->PartMode == PART_2NxnU || cu->PartMode == PART_2NxnD) && pu->index == 1)
	{
		isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA0);
		isA1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA1);
		isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, decCtx->ctbSize, puB0);
		isB1Available = false;
		isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puB2);
	}

	else if ((cu->PartMode == PART_Nx2N || cu->PartMode == PART_nLx2N || cu->PartMode == PART_nRx2N) && pu->index == 1)
	{
		isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA0);
		isA1Available = false;
		isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, decCtx->ctbSize, puB0);
		isB1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn, partition->width, partition->height, decCtx->ctbSize, puB1);
		isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puB2);

	}
	else {

		isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA0);
		isA1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow, puBottomLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puA1);
		isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, decCtx->ctbSize, puB0);
		isB1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn, partition->width, partition->height, decCtx->ctbSize, puB1);
		isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, decCtx->ctbSize, puB2);
	}




	//First spatial candidate
	if (isA1Available && i < numOfMergeCandidates)
	{
		MergeCandListX[i] = puA1.MVx1;
		MergeCandListY[i] = puA1.MVy1;
		MergeCandListT[i] = puA1.MVt1;
		i++;
	}
	//Second spatial candidate
	if (isB1Available && i < numOfMergeCandidates)
	{
		if (!(isA1Available && puB1.MVx1 == puA1.MVx1
			&& puB1.MVy1 == puA1.MVy1))
		{
			MergeCandListX[i] = puB1.MVx1;
			MergeCandListY[i] = puB1.MVy1;
			MergeCandListT[i] = puB1.MVt1;
			i++;
		}
	}

	//Third spatial candidate
	//B0 checks B1
	if (isB0Available && i < numOfMergeCandidates)
	{
		if (!(isB1Available && puB0.MVx1 == puB1.MVx1
			&& puB0.MVy1 == puB1.MVy1))
		{
			MergeCandListX[i] = puB0.MVx1;
			MergeCandListY[i] = puB0.MVy1;
			MergeCandListT[i] = puB0.MVt1;
			i++;
		}
	}

	//Fourth spatial candidate
	//A0 checks A1
	if (isA0Available && i < numOfMergeCandidates)
	{
		if (!(isA1Available && puA0.MVx1 == puA1.MVx1
			&& puA0.MVy1 == puA1.MVy1))
		{
			MergeCandListX[i] = puA0.MVx1;
			MergeCandListY[i] = puA0.MVy1;
			MergeCandListT[i] = puA0.MVt1;
			i++;
		}
	}

	//Fifth spatial candidate
	//B2 checks A1 and B1
	if (isB2Available && i < numOfMergeCandidates && i < maxSpatialCandidates)
	{
		if (!((isB1Available && puB2.MVx1 == puB1.MVx1
			&& puB2.MVy1 == puB1.MVy1) ||
			(isA1Available && puB2.MVx1 == puA1.MVx1
				&& puB2.MVy1 == puA1.MVy1)))

		{
			MergeCandListX[i] = puB2.MVx1;
			MergeCandListY[i] = puB2.MVy1;
			MergeCandListT[i] = puB2.MVt1;
			i++;
		}
	}

	if (decCtx->useTemporalAMVP && i < numOfMergeCandidates)
	{
		/*CU* C0 = BlockPartition::getRefRightDownCodingUnitByIndex(dpbm.dpb.frames[0].ctuTree, cu, isC0Available, decCtx.width, decCtx.height);
		CU* C1 = BlockPartition::getRefCenterCodingUnit(dpbm.dpb.frames[0].ctuTree, cu);

		isC0Available = isB2Available && C0.CuPredMode == MODE_INTER && C0.predictionUnits[0].isProcessed;
		isC1Available = C1.CuPredMode == MODE_INTER && C1.predictionUnits[0].isProcessed;

		if ((isC0Available || isC1Available))
		{
			if (isC0Available)
			{
				MergeCandListX[i] = C0.predictionUnits[0].MVx1;
				MergeCandListY[i] = C0.predictionUnits[0].MVy1;
				MergeCandListT[i] = C0.predictionUnits[0].MVt1;
			}
			else
			{
				MergeCandListX[i] = C1.predictionUnits[0].MVx1;
				MergeCandListY[i] = C1.predictionUnits[0].MVy1;
				MergeCandListT[i] = C1.predictionUnits[0].MVt1;
			}
			i++;
		}*/
	}

	while (i < numOfMergeCandidates)
	{
		//TODO Add combined bi-predictive candidates foe B slices 

		//Zero motion candidates
		MergeCandListX[i] = 0;
		MergeCandListY[i] = 0;
		MergeCandListT[i] = pu->MVt1;
		i++;
	}
}

void decoder::PredictionController::ScaleCandidate(int deltaX, int deltaY, int tb, int td, int& scaledDeltaX, int& scaledDeltaY)
{
	int tx = (16384 + abs(td >> 1)) / td;
	int scaleFactor = ComUtil::clip3(-4096, 4095, (tb * tx + 32) >> 6);

	int dsX, dsY;

	dsX = deltaX * scaleFactor;
	dsY = deltaY * scaleFactor;

	scaledDeltaX = ComUtil::clip3(-32678, 32676, ComUtil::sign(dsX) * ((abs(dsX) + 127) >> 8));
	scaledDeltaY = ComUtil::clip3(-32678, 32676, ComUtil::sign(dsY) * ((abs(dsY) + 127) >> 8));

}





