/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "MemoryManager.h"

unsigned char * MemoryManager::buffer = NULL;
unsigned int MemoryManager::bufferSize = 0;
unsigned int MemoryManager::circularWriteAddress = 0;
unsigned int MemoryManager::circularReadAddress = 0;
long MemoryManager::virtualAddress = 0;
long MemoryManager::lastAddress = 0;
FILE * MemoryManager::stream = NULL;
thread MemoryManager::readingThread = thread();
map<unsigned long, unsigned long> MemoryManager::released_blocks = map<unsigned long, unsigned long>();
mutex MemoryManager::file_access_lock;
condition_variable MemoryManager::read_cv;
mutex MemoryManager::read_m;
condition_variable MemoryManager::sem_cv;
mutex MemoryManager::sem_m;
mutex MemoryManager::release_m;
int MemoryManager::ActiveThreadCount = 0;
bool MemoryManager::_EOF = false;


void MemoryManager::Init(unsigned int bufferSize, FILE * stream)
{
	buffer = new unsigned char[bufferSize];
	MemoryManager::bufferSize = bufferSize;
	MemoryManager::stream = stream;

	circularWriteAddress = 0;
	virtualAddress = 0;
	lastAddress = 0;

	ThreadCheckIn();

	// Initial read to fill the input buffer
	readingThread = thread(&Read, bufferSize);
	readingThread.join();

	virtualAddress = 0;
}

unsigned char * MemoryManager::GetByte(long address)
{
	// Waiting for reading thread to load the new block of data
	/* TODO: Handle the deadlock situation when all worker threads are waiting for reading thread
	which can not read the data because the buffer memory is not released yet. */
	while (address > lastAddress)
	{
		unique_lock<mutex> lk(read_m);
		read_cv.wait(lk);
	}

	ThreadCheckIn();
	// Calculate the offset from the first virtual address in buffer
	int virtualOffset = address - virtualAddress;
	ThreadCheckOut();

	if (virtualOffset < 0)
		throw; // TODO: Throw exception when external proccess request address that already was read

	if (virtualOffset >= (int)(2 * bufferSize))
		throw;

	int circularBufferPointer = circularReadAddress + virtualOffset;

	// Calculate in the circular architecture
	if (circularBufferPointer >= (int)bufferSize)
		circularBufferPointer -= bufferSize;

	return &buffer[circularBufferPointer];
}

void MemoryManager::Release(long from, long to)
{
	if (virtualAddress > from)
	{
		// TODO: Throw exception if from address is less than first virtual address in buffer
		// ReleasingOutOfRangeException
		return;
	}

	if (lastAddress + 2 < to)
	{
		// TODO: Throw exception if to address is greater than last virtual address in buffer
		return;
	}

	// Multiple Streamer threads accessing the released_blocks map (mutex lock)
	release_m.lock();
	released_blocks.insert(pair<unsigned long, unsigned long>(from, to));
	release_m.unlock();
}

void MemoryManager::Free()
{
	ThreadCheckIn();
	while (MemoryReleasing())
		ThreadCheckIn();
	ThreadCheckOut();
}

void MemoryManager::ResetVirtualAddress()
{
	unique_lock<mutex> lk(sem_m);
	sem_cv.wait(lk, NoActiveThreads);

	lastAddress -= virtualAddress;
	virtualAddress = 0;
	sem_cv.notify_one();
}

void MemoryManager::Read(unsigned int size)
{
	if (_EOF)
	{
		ThreadCheckOut();
		lock_guard<mutex> lk(read_m);
		read_cv.notify_all();

		return;
	}

	lock_guard<mutex> falk(file_access_lock);

	if (size > bufferSize)
	{
		cout << "Memory manager: Requested read size (";
		cout << size;
		cout << " bytes) is greater than input buffer size (";
		cout << bufferSize;
		cout << " bytes). Reading process is terminated." << endl;
		return;
	}

	unsigned int freeMemorySize = bufferSize - (lastAddress - virtualAddress);

	if (size > freeMemorySize)
	{
		cout << "Memory manager: Requested read size (";
		cout << size;
		cout << " bytes) is greater than the available released memory in input buffer (";
		cout << freeMemorySize;
		cout << " bytes). Reading process will stop after when there is no more space in buffer." << endl;

		size = freeMemorySize;
	}

	long addressOffset = 0;

	if (circularWriteAddress + size <= bufferSize)
	{
		long rSize = (long)fread((MemoryManager::buffer + circularWriteAddress), 1, size, MemoryManager::stream);
		circularWriteAddress += rSize;

		_EOF = feof(MemoryManager::stream);

		if (circularWriteAddress == bufferSize)
			circularWriteAddress = 0;

		addressOffset = rSize;
	}
	else
	{
		unsigned int remaining = (circularWriteAddress + size) - bufferSize;
		long rSize_1 = (long)fread((MemoryManager::buffer + circularWriteAddress), 1, size - remaining, MemoryManager::stream);
		_EOF = feof(MemoryManager::stream);

		addressOffset = rSize_1;

		if (!_EOF)
		{
			long rSize_2 = (long)fread((MemoryManager::buffer), 1, remaining, MemoryManager::stream);
			circularWriteAddress = rSize_2;
			addressOffset += rSize_2;
			_EOF = feof(MemoryManager::stream);
		}
		else
		{
			circularWriteAddress += rSize_1;
		}
	}

	ThreadCheckOut();
	SyncLastAddress(lastAddress + addressOffset);

	lock_guard<mutex> lk(read_m);
	read_cv.notify_all();
}

bool MemoryManager::MemoryReleasing()
{
	if (released_blocks.empty())
		return false;

	unsigned long nextReleased = released_blocks.begin()->first;

	if (nextReleased == virtualAddress)
	{
		ThreadCheckOut();
		SyncVirtualAddress(released_blocks.begin()->second);

		if (readingThread.joinable())
			readingThread.join();

		ThreadCheckIn();
		unsigned int size = virtualAddress - nextReleased;

		released_blocks.erase(nextReleased);

		readingThread = thread(&Read, size);
		return true;
	}
	else
	{
		return false;
	}
}

void MemoryManager::ThreadCheckIn()
{
	lock_guard<mutex> lk(sem_m);
	ActiveThreadCount++;
}

void MemoryManager::ThreadCheckOut()
{
	lock_guard<mutex> lk(sem_m);
	ActiveThreadCount--;
	sem_cv.notify_one();
}

bool MemoryManager::NoActiveThreads()
{
	return ActiveThreadCount == 0;
}

void MemoryManager::SyncVirtualAddress(long address)
{
	unique_lock<mutex> lk(sem_m);
	sem_cv.wait(lk, NoActiveThreads);

	long diff = address - virtualAddress;

	virtualAddress = address;
	circularReadAddress += (int)diff;

	if (circularReadAddress >= bufferSize)
		circularReadAddress -= bufferSize;

	sem_cv.notify_one();
}

void MemoryManager::SyncLastAddress(long address)
{
	unique_lock<mutex> lk(sem_m);
	sem_cv.wait(lk, NoActiveThreads);

	lastAddress = address;
	sem_cv.notify_one();
}

bool MemoryManager::IsEOF(long address)
{
	if (address >= lastAddress && _EOF)
		return true;
	else
		return false;
}

void MemoryManager::JoinReadThread()
{
	if (readingThread.joinable())
		readingThread.join();
}

void MemoryManager::ReleaseBuffer()
{
	delete[] buffer;
}
