/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "TQController.h"

TQController::TQController()
{
	enCtx = nullptr;
	tqKernel = nullptr;

	residualFrame4 = nullptr;
	residualFrame8 = nullptr;
	residualFrame16 = nullptr;
	residualFrame32 = nullptr;

	qtFrame4 = nullptr;
	qtFrame8 = nullptr;
	qtFrame16 = nullptr;
	qtFrame32 = nullptr;

	iqtFrame4 = nullptr;
	iqtFrame8 = nullptr;
	iqtFrame16 = nullptr;
	iqtFrame32 = nullptr;

}

TQController::~TQController()
{
	if (tqKernel != nullptr)
		delete tqKernel;
}

void TQController::Init(EncodingContext* _enCtx)
{
	enCtx = _enCtx;
	tqKernel = TransformAndQuantizationCreator::Create(enCtx);
}

void TQController::Init(EncodingContext* _enCtx, Partition* partition)
{
	enCtx = _enCtx;
	tqKernel = TransformAndQuantizationCreator::Create(enCtx);

	if (enCtx->clusteredTQ)
	{

		residualFrame32 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		residualFrame16 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		residualFrame8 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		residualFrame4 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };

		qtFrame32 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		qtFrame16 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		qtFrame8 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		qtFrame4 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };

		iqtFrame32 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		iqtFrame16 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		iqtFrame8 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };
		iqtFrame4 = new int16_t[partition->width * partition->height * 3 / 2]{ 0 };

		residual32Size = 0;
		residual16Size = 0;
		residual8Size = 0;
		residual4Size = 0;
	}
}

void TQController::PerformTransformAndQuantization(int qp, int16_t* blockResidual, unsigned char blockSize, bool* isZeroMatrix, int16_t* tqResidual, int colorIdx, PredMode cuPredMode)
{
	int16_t* transformedResidual = new int16_t[blockSize * blockSize];

	tqKernel->forward2DTransform(blockResidual, enCtx->bitNumber, blockSize, transformedResidual, colorIdx, cuPredMode);
	tqKernel->quantization(transformedResidual, qp, enCtx->bitNumber, blockSize, isZeroMatrix, tqResidual);

	delete[] transformedResidual;
}

void TQController::PerformInverseTransformAndQuantization(int qp, int16_t* quantizedTransformedResidual, unsigned char blockSize, int16_t* residual, int colorIdx, PredMode cuPredMode)
{
	int16_t* transformedResidual = new int16_t[blockSize * blockSize];

	tqKernel->dequantization(quantizedTransformedResidual, qp, enCtx->bitNumber, blockSize, transformedResidual);
	tqKernel->forward2DInverseTransform(transformedResidual, enCtx->bitNumber, blockSize, residual, colorIdx, cuPredMode);

	delete[] transformedResidual;
}

void TQController::PerformTQandIQT()
{
	if (enCtx->MEEP)
	{
		//Since the TQ/ITQ HEVC accelerator is designed to receive transposed matrices, we first have to transpose all the matrices
		PrepareDataForAccelerator();
	}
	tqKernel->performTQandIQTOnCluster(enCtx->initQP, enCtx->bitNumber, residualFrame32, residual32Size, 32, qtFrame32, 0, MODE_INTER, iqtFrame32);
	tqKernel->performTQandIQTOnCluster(enCtx->initQP, enCtx->bitNumber, residualFrame16, residual16Size, 16, qtFrame16, 0, MODE_INTER, iqtFrame16);
	tqKernel->performTQandIQTOnCluster(enCtx->initQP, enCtx->bitNumber, residualFrame8, residual8Size, 8, qtFrame8, 0, MODE_INTER, iqtFrame8);
	tqKernel->performTQandIQTOnCluster(enCtx->initQP, enCtx->bitNumber, residualFrame4, residual4Size, 4, qtFrame4, 0, MODE_INTER, iqtFrame4);
}

void TQController::PrepareDataForAccelerator()
{
	ComUtil::TransposeInputResidual(residualFrame32, residual32Size, 32);
	ComUtil::TransposeInputResidual(residualFrame16, residual16Size, 16);
	ComUtil::TransposeInputResidual(residualFrame8, residual8Size, 8);
	ComUtil::TransposeInputResidual(residualFrame4, residual4Size, 4);
}

void TQController::Clean()
{
	if (residualFrame32 != nullptr)
		delete[] residualFrame32;
	if (residualFrame16 != nullptr)
		delete[] residualFrame16;
	if (residualFrame8 != nullptr)
		delete[] residualFrame8;
	if (residualFrame4 != nullptr)
		delete[] residualFrame4;

	if (qtFrame32 != nullptr)
		delete[] qtFrame32;
	if (qtFrame16 != nullptr)
		delete[] qtFrame16;
	if (qtFrame8 != nullptr)
		delete[] qtFrame8;
	if (qtFrame4 != nullptr)
		delete[] qtFrame4;

	if (iqtFrame32 != nullptr)
		delete[] iqtFrame32;
	if (iqtFrame16 != nullptr)
		delete[] iqtFrame16;
	if (iqtFrame8 != nullptr)
		delete[] iqtFrame8;
	if (iqtFrame4 != nullptr)
		delete[] iqtFrame4;
}