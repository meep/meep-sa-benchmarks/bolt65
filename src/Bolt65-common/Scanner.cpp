/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Scanner.h"
#include "Scanner.h"

void Scanner::GetCoordinatesInBlock2x2(int scanIndex, int &cooX, int &cooY, int scanType)
{
	if (scanType == DiagonalScan)
	{
		GetCoordinatesInDiagonalBlock2x2(scanIndex, cooX, cooY);
	}
	else if (scanType == VerticalScan)
	{
		cooX = 1 - scanIndex / 2;
		cooY = 1 - scanIndex % 2;
	}
	else if (scanType == HorizontalScan)
	{
		cooX = 1 - scanIndex % 2;
		cooY = 1 - scanIndex / 2;
	}
}

void Scanner::GetCoordinatesInBlock4x4(int scanIndex, int &cooX, int &cooY, int scanType)
{
	if (scanType == DiagonalScan)
	{
		GetCoordinatesInDiagonalBlock4x4(scanIndex, cooX, cooY);
	}
	else if (scanType == VerticalScan)
	{
		cooX = 3 - scanIndex / 4;
		cooY = 3 - scanIndex % 4;
	}
	else if (scanType == HorizontalScan)
	{
		cooX = 3 - scanIndex % 4;
		cooY = 3 - scanIndex / 4;
	}
}

void Scanner::GetCoordinatesInBlock8x8(int scanIndex, int &cooX, int &cooY, int scanType)
{
	if (scanType == DiagonalScan)
	{
		GetCoordinatesInDiagonalBlock8x8(scanIndex, cooX, cooY);
	}
	else if (scanType == VerticalScan)
	{
		cooX = 7 - scanIndex / 8;
		cooY = 7 - scanIndex % 8;
	}
	else if (scanType == HorizontalScan)
	{
		cooX = 7 - scanIndex % 8;
		cooY = 7 - scanIndex / 8;
	}
}

void Scanner::GetCoordinatesInDiagonalBlock2x2(int scanIndex, int &cooX, int &cooY)
{
	switch (scanIndex)
	{
	case 0:
		cooX = 1;
		cooY = 1;
		break;
	case 1:
		cooX = 1;
		cooY = 0;
		break;
	case 2:
		cooX = 0;
		cooY = 1;
		break;
	case 3:
		cooX = 0;
		cooY = 0;
		break;
	default:
		cooX = -1;
		cooY = -1;
		break;
	}
}

void Scanner::GetCoordinatesInDiagonalBlock4x4(int scanIndex, int &cooX, int &cooY)
{
	switch (scanIndex)
	{
	case 0:
		cooX = 3;
		cooY = 3;
		break;
	case 1:
		cooX = 3;
		cooY = 2;
		break;
	case 2:
		cooX = 2;
		cooY = 3;
		break;
	case 3:
		cooX = 3;
		cooY = 1;
		break;
	case 4:
		cooX = 2;
		cooY = 2;
		break;
	case 5:
		cooX = 1;
		cooY = 3;
		break;
	case 6:
		cooX = 3;
		cooY = 0;
		break;
	case 7:
		cooX = 2;
		cooY = 1;
		break;
	case 8:
		cooX = 1;
		cooY = 2;
		break;
	case 9:
		cooX = 0;
		cooY = 3;
		break;
	case 10:
		cooX = 2;
		cooY = 0;
		break;
	case 11:
		cooX = 1;
		cooY = 1;
		break;
	case 12:
		cooX = 0;
		cooY = 2;
		break;
	case 13:
		cooX = 1;
		cooY = 0;
		break;
	case 14:
		cooX = 0;
		cooY = 1;
		break;
	case 15:
		cooX = 0;
		cooY = 0;
		break;
	default:
		cooX = -1;
		cooY = -1;
		break;
	}
}

void Scanner::GetCoordinatesInDiagonalBlock8x8(int scanIndex, int &cooX, int &cooY)
{
	switch (scanIndex)
	{
	case 0:
		cooX = 7;
		cooY = 7;
		break;
	case 1:
		cooX = 7;
		cooY = 6;
		break;
	case 2:
		cooX = 6;
		cooY = 7;
		break;
	case 3:
		cooX = 7;
		cooY = 5;
		break;
	case 4:
		cooX = 6;
		cooY = 6;
		break;
	case 5:
		cooX = 5;
		cooY = 7;
		break;
	case 6:
		cooX = 7;
		cooY = 4;
		break;
	case 7:
		cooX = 6;
		cooY = 5;
		break;
	case 8:
		cooX = 5;
		cooY = 6;
		break;
	case 9:
		cooX = 4;
		cooY = 7;
		break;
	case 10:
		cooX = 7;
		cooY = 3;
		break;
	case 11:
		cooX = 6;
		cooY = 4;
		break;
	case 12:
		cooX = 5;
		cooY = 5;
		break;
	case 13:
		cooX = 4;
		cooY = 6;
		break;
	case 14:
		cooX = 3;
		cooY = 7;
		break;
	case 15:
		cooX = 7;
		cooY = 2;
		break;
	case 16:
		cooX = 6;
		cooY = 3;
		break;
	case 17:
		cooX = 5;
		cooY = 4;
		break;
	case 18:
		cooX = 4;
		cooY = 5;
		break;
	case 19:
		cooX = 3;
		cooY = 6;
		break;
	case 20:
		cooX = 2;
		cooY = 7;
		break;
	case 21:
		cooX = 7;
		cooY = 1;
		break;
	case 22:
		cooX = 6;
		cooY = 2;
		break;
	case 23:
		cooX = 5;
		cooY = 3;
		break;
	case 24:
		cooX = 4;
		cooY = 4;
		break;
	case 25:
		cooX = 3;
		cooY = 5;
		break;
	case 26:
		cooX = 2;
		cooY = 6;
		break;
	case 27:
		cooX = 1;
		cooY = 7;
		break;
	case 28:
		cooX = 7;
		cooY = 0;
		break;
	case 29:
		cooX = 6;
		cooY = 1;
		break;
	case 30:
		cooX = 5;
		cooY = 2;
		break;
	case 31:
		cooX = 4;
		cooY = 3;
		break;
	case 32:
		cooX = 3;
		cooY = 4;
		break;
	case 33:
		cooX = 2;
		cooY = 5;
		break;
	case 34:
		cooX = 1;
		cooY = 6;
		break;
	case 35:
		cooX = 0;
		cooY = 7;
		break;
	case 36:
		cooX = 6;
		cooY = 0;
		break;
	case 37:
		cooX = 5;
		cooY = 1;
		break;
	case 38:
		cooX = 4;
		cooY = 2;
		break;
	case 39:
		cooX = 3;
		cooY = 3;
		break;
	case 40:
		cooX = 2;
		cooY = 4;
		break;
	case 41:
		cooX = 1;
		cooY = 5;
		break;
	case 42:
		cooX = 0;
		cooY = 6;
		break;
	case 43:
		cooX = 5;
		cooY = 0;
		break;
	case 44:
		cooX = 4;
		cooY = 1;
		break;
	case 45:
		cooX = 3;
		cooY = 2;
		break;
	case 46:
		cooX = 2;
		cooY = 3;
		break;
	case 47:
		cooX = 1;
		cooY = 4;
		break;
	case 48:
		cooX = 0;
		cooY = 5;
		break;
	case 49:
		cooX = 4;
		cooY = 0;
		break;
	case 50:
		cooX = 3;
		cooY = 1;
		break;
	case 51:
		cooX = 2;
		cooY = 2;
		break;
	case 52:
		cooX = 1;
		cooY = 3;
		break;
	case 53:
		cooX = 0;
		cooY = 4;
		break;
	case 54:
		cooX = 3;
		cooY = 0;
		break;
	case 55:
		cooX = 2;
		cooY = 1;
		break;
	case 56:
		cooX = 1;
		cooY = 2;
		break;
	case 57:
		cooX = 0;
		cooY = 3;
		break;
	case 58:
		cooX = 2;
		cooY = 0;
		break;
	case 59:
		cooX = 1;
		cooY = 1;
		break;
	case 60:
		cooX = 0;
		cooY = 2;
		break;
	case 61:
		cooX = 1;
		cooY = 0;
		break;
	case 62:
		cooX = 0;
		cooY = 1;
		break;
	case 63:
		cooX = 0;
		cooY = 0;
		break;
	default:
		cooX = -1;
		cooY = -1;
		break;
	}
}

void Scanner::GetScanIndexFromCoordinates2x2(int &scanIndex, int cooX, int cooY, int scanType)
{
	if (cooX < 0 || cooX>1 || cooY < 0 || cooY>1)
	{
		scanIndex = -1;
	}
	else if (scanType == DiagonalScan)
	{
		GetDiagonalScanIndexFromCoordinates2x2(scanIndex, cooX, cooY);
	}
	else if (scanType == VerticalScan)
	{
		scanIndex = (1 - cooX) * 2 + (1 - cooY);
	}
	else if (scanType == HorizontalScan)
	{
		scanIndex = 3 - (2 * cooY + cooX);
	}
}

void Scanner::GetScanIndexFromCoordinates4x4(int &scanIndex, int cooX, int cooY, int scanType)
{
	if (cooX < 0 || cooX>3 || cooY < 0 || cooY>3)
	{
		scanIndex = -1;
	}
	else if (scanType == DiagonalScan)
	{
		GetDiagonalScanIndexFromCoordinates4x4(scanIndex, cooX, cooY);
	}
	else if (scanType == VerticalScan)
	{
		scanIndex = (3 - cooX) * 4 + (3 - cooY);
	}
	else if (scanType == HorizontalScan)
	{
		scanIndex = 15 - (4 * cooY + cooX);
	}
}

void Scanner::GetScanIndexFromCoordinates8x8(int &scanIndex, int cooX, int cooY, int scanType)
{
	if (cooX < 0 || cooX>7 || cooY < 0 || cooY>7)
	{
		scanIndex = -1;
	}
	else if (scanType == DiagonalScan)
	{
		GetDiagonalScanIndexFromCoordinates8x8(scanIndex, cooX, cooY);
	}
	else if (scanType == VerticalScan)
	{
		scanIndex = (7 - cooX) * 8 + (7 - cooY);
	}
	else if (scanType == HorizontalScan)
	{
		scanIndex = 63 - (8 * cooY + cooX);
	}
}

void Scanner::GetDiagonalScanIndexFromCoordinates2x2(int &scanIndex, int cooX, int cooY)
{
	if (cooX == 1 && cooY == 1)
	{
		scanIndex = 0;
	}
	else if (cooX == 1 && cooY == 0)
	{
		scanIndex = 1;
	}

	else if (cooX == 0 && cooY == 1)
	{
		scanIndex = 2;
	}
	else if (cooX == 0 && cooY == 0)
	{
		scanIndex = 3;
	}
	else
	{
		scanIndex = -1;
	}

}

void Scanner::GetDiagonalScanIndexFromCoordinates4x4(int &scanIndex, int cooX, int cooY)
{

	if (cooX == 3 && cooY == 3)		scanIndex = 0;
	else if (cooX == 3 && cooY == 2) scanIndex = 1;
	else if (cooX == 2 && cooY == 3) scanIndex = 2;
	else if (cooX == 3 && cooY == 1) scanIndex = 3;
	else if (cooX == 2 && cooY == 2)scanIndex = 4;
	else if (cooX == 1 && cooY == 3)scanIndex = 5;
	else if (cooX == 3 && cooY == 0)scanIndex = 6;
	else if (cooX == 2 && cooY == 1)scanIndex = 7;
	else if (cooX == 1 && cooY == 2)scanIndex = 8;
	else if (cooX == 0 && cooY == 3)scanIndex = 9;
	else if (cooX == 2 && cooY == 0)scanIndex = 10;
	else if (cooX == 1 && cooY == 1)scanIndex = 11;
	else if (cooX == 0 && cooY == 2)scanIndex = 12;
	else if (cooX == 1 && cooY == 0)scanIndex = 13;
	else if (cooX == 0 && cooY == 1)scanIndex = 14;
	else if (cooX == 0 && cooY == 0)scanIndex = 15;
	else scanIndex = -1;

}

void Scanner::GetDiagonalScanIndexFromCoordinates8x8(int &scanIndex, int cooX, int cooY)
{

	if (cooX == 7 && cooY == 7)		scanIndex = 0;
	else if (cooX == 7 && cooY == 6) scanIndex = 1;
	else if (cooX == 6 && cooY == 7) scanIndex = 2;
	else if (cooX == 7 && cooY == 5) scanIndex = 3;
	else if (cooX == 6 && cooY == 6)scanIndex = 4;
	else if (cooX == 5 && cooY == 7)scanIndex = 5;
	else if (cooX == 7 && cooY == 4)scanIndex = 6;
	else if (cooX == 6 && cooY == 5)scanIndex = 7;
	else if (cooX == 5 && cooY == 6)scanIndex = 8;
	else if (cooX == 4 && cooY == 7)scanIndex = 9;
	else if (cooX == 7 && cooY == 3)scanIndex = 10;
	else if (cooX == 6 && cooY == 4)scanIndex = 11;
	else if (cooX == 5 && cooY == 5)scanIndex = 12;
	else if (cooX == 4 && cooY == 6)scanIndex = 13;
	else if (cooX == 3 && cooY == 7)scanIndex = 14;
	else if (cooX == 7 && cooY == 2)scanIndex = 15;
	else if (cooX == 6 && cooY == 3)scanIndex = 16;
	else if (cooX == 5 && cooY == 4)scanIndex = 17;
	else if (cooX == 4 && cooY == 5)scanIndex = 18;
	else if (cooX == 3 && cooY == 6)scanIndex = 19;
	else if (cooX == 2 && cooY == 7)scanIndex = 20;
	else if (cooX == 7 && cooY == 1)scanIndex = 21;
	else if (cooX == 6 && cooY == 2)scanIndex = 22;
	else if (cooX == 5 && cooY == 3)scanIndex = 23;
	else if (cooX == 4 && cooY == 4)scanIndex = 24;
	else if (cooX == 3 && cooY == 5)scanIndex = 25;
	else if (cooX == 2 && cooY == 6)scanIndex = 26;
	else if (cooX == 1 && cooY == 7)scanIndex = 27;
	else if (cooX == 7 && cooY == 0)scanIndex = 28;
	else if (cooX == 6 && cooY == 1)scanIndex = 29;
	else if (cooX == 5 && cooY == 2)scanIndex = 30;
	else if (cooX == 4 && cooY == 3)scanIndex = 31;
	else if (cooX == 3 && cooY == 4)scanIndex = 32;
	else if (cooX == 2 && cooY == 5)scanIndex = 33;
	else if (cooX == 1 && cooY == 6)scanIndex = 34;
	else if (cooX == 0 && cooY == 7)scanIndex = 35;
	else if (cooX == 6 && cooY == 0)scanIndex = 36;
	else if (cooX == 5 && cooY == 1)scanIndex = 37;
	else if (cooX == 4 && cooY == 2)scanIndex = 38;
	else if (cooX == 3 && cooY == 3)scanIndex = 39;
	else if (cooX == 2 && cooY == 4)scanIndex = 40;
	else if (cooX == 1 && cooY == 5)scanIndex = 41;
	else if (cooX == 0 && cooY == 6)scanIndex = 42;
	else if (cooX == 5 && cooY == 0)scanIndex = 43;
	else if (cooX == 4 && cooY == 1)scanIndex = 44;
	else if (cooX == 3 && cooY == 2)scanIndex = 45;
	else if (cooX == 2 && cooY == 3)scanIndex = 46;
	else if (cooX == 1 && cooY == 4)scanIndex = 47;
	else if (cooX == 0 && cooY == 5)scanIndex = 48;
	else if (cooX == 4 && cooY == 0)scanIndex = 49;
	else if (cooX == 3 && cooY == 1)scanIndex = 50;
	else if (cooX == 2 && cooY == 2)scanIndex = 51;
	else if (cooX == 1 && cooY == 3)scanIndex = 52;
	else if (cooX == 0 && cooY == 4)scanIndex = 53;
	else if (cooX == 3 && cooY == 0)scanIndex = 54;
	else if (cooX == 2 && cooY == 1)scanIndex = 55;
	else if (cooX == 1 && cooY == 2)scanIndex = 56;
	else if (cooX == 0 && cooY == 3)scanIndex = 57;
	else if (cooX == 2 && cooY == 0)scanIndex = 58;
	else if (cooX == 1 && cooY == 1)scanIndex = 59;
	else if (cooX == 0 && cooY == 2)scanIndex = 60;
	else if (cooX == 1 && cooY == 0)scanIndex = 61;
	else if (cooX == 0 && cooY == 1)scanIndex = 62;
	else if (cooX == 0 && cooY == 0)scanIndex = 63;
	else scanIndex = -1;
}

void Scanner::FindLastSigCoordinates(TB *transformBlock)
{
	int indexOfSubblock = transformBlock->indexOfSigSubblock;
	int indexInSubblock = transformBlock->subBlocks[indexOfSubblock].scanSigIndex;

	int coordinateX = -1, coordinateY = -1;
	int subblockX = -1, subblockY = -1;

	GetCoordinatesInBlock4x4(indexInSubblock, subblockX, subblockY, transformBlock->scanType);

	switch (transformBlock->transformBlockSize)
	{
	case 32:
		GetCoordinatesInBlock8x8(indexOfSubblock, coordinateX, coordinateY, transformBlock->scanType);
		break;
	case 16:
		GetCoordinatesInBlock4x4(indexOfSubblock, coordinateX, coordinateY, transformBlock->scanType);
		break;
	case 8:
		GetCoordinatesInBlock2x2(indexOfSubblock, coordinateX, coordinateY, transformBlock->scanType);
		break;
	case 4:
		coordinateX = 0;
		coordinateY = 0;
	default:
		break;
	}


	transformBlock->lastSigCoeffX = coordinateX * 4 + subblockX;
	transformBlock->lastSigCoeffY = coordinateY * 4 + subblockY;
}

void Scanner::SetSubBlockElements(int16_t* scanData, TBSubblock* tbSubBlock)
{
	int i;

	bool sigFound = false;
	int alg1Counter = 8;//only 8 flags can be transmitted and set for ALG1
	int alg2Counter = 1;//only 1 flag can be transmitted and set for ALG1

	tbSubBlock->scanSigIndex = -1;

	for (i = 0; i < 16; i++)
	{
		if (scanData[i] != 0 && !sigFound)
		{
			tbSubBlock->scanSigIndex = i;
			sigFound = true;
		}
		/*	else if (!sigFound)
		{
		continue;
		}*/

		/*SET sig coeff flag*/
		if (scanData[i] == 0)
		{
			tbSubBlock->SigCoeffFlag[i] = 0;
		}
		else
		{
			tbSubBlock->SigCoeffFlag[i] = 1;
		}
		/*END SET sig coeff flag*/

		/*SET coeff abs level greater1 flag*/
		if (tbSubBlock->SigCoeffFlag[i] == 1 && alg1Counter > 0)
		{
			alg1Counter--;
			if (abs(scanData[i]) > 1)
			{
				tbSubBlock->CoeffAbsLevelGreater1Flag[i] = 1;
			}
			else
			{
				tbSubBlock->CoeffAbsLevelGreater1Flag[i] = 0;
			}
		}
		/*END SET coeff abs level greater1 flag*/

		/*SET coeff abs level greater2 flag*/
		if (tbSubBlock->CoeffAbsLevelGreater1Flag[i] == 1 && alg2Counter > 0)
		{
			alg2Counter--;
			if (abs(scanData[i]) > 2)
			{
				tbSubBlock->CoeffAbsLevelGreater2Flag[i] = 1;
			}
			else
			{
				tbSubBlock->CoeffAbsLevelGreater2Flag[i] = 0;
			}
		}
		/*END SET coeff abs level greater2 flag*/

		/*SET coeff abs level remaining*/
		if (tbSubBlock->CoeffAbsLevelGreater2Flag[i] == 1
			|| (tbSubBlock->CoeffAbsLevelGreater2Flag[i] == -1 && tbSubBlock->CoeffAbsLevelGreater1Flag[i] == 1)
			|| (tbSubBlock->CoeffAbsLevelGreater2Flag[i] == -1 && tbSubBlock->CoeffAbsLevelGreater1Flag[i] == -1 && tbSubBlock->SigCoeffFlag[i] == 1))
		{
			//Since value is -1 when flag is not set we have to do this
			int SIG = tbSubBlock->SigCoeffFlag[i] == 1 ? 1 : 0;
			int ALG1 = tbSubBlock->CoeffAbsLevelGreater1Flag[i] == 1 ? 1 : 0;
			int ALG2 = tbSubBlock->CoeffAbsLevelGreater2Flag[i] == 1 ? 1 : 0;

			tbSubBlock->CoeffAbsLevelRemaining[i] = abs(scanData[i]) - SIG - ALG1 - ALG2;
		}
		/*END SET coeff abs level remaining*/

		/*SET coeff abs level remaining*/
		if (tbSubBlock->SigCoeffFlag[i] == 1)
		{
			if (scanData[i] < 0)
			{
				tbSubBlock->CoeffSignFlag[i] = 1;
			}
			else
			{
				tbSubBlock->CoeffSignFlag[i] = 0;
			}
		}

		/*END SET coeff abs level remaining*/
	}

	//Are all coeff == 0
	if (tbSubBlock->scanSigIndex == -1)
	{
		tbSubBlock->codedSubBlockFlag = 0;
	}
	else
	{
		tbSubBlock->codedSubBlockFlag = 1;
	}
}

void Scanner::GetSubBlockElements(int16_t* scanData, TBSubblock* tbSubBlock)
{
	int i;

	if (tbSubBlock->codedSubBlockFlag == 0)
	{
		for (i = 0; i < 16; i++)
		{
			scanData[i] = 0;
		}
		return;
	}
	else
	{
		for (i = 0; i < 16; i++)
		{
			int base = 1;

			if (tbSubBlock->SigCoeffFlag[i] <= 0)
			{
				scanData[i] = 0;
				continue;
			}

			if (tbSubBlock->CoeffAbsLevelGreater1Flag[i] == 0)
				scanData[i] = 1;
			else if (tbSubBlock->CoeffAbsLevelGreater1Flag[i] > 0)
				base++;

			if (tbSubBlock->CoeffAbsLevelGreater2Flag[i] > 0)
				base++;

			if (scanData[i] != 1)
			{
				int remaining = tbSubBlock->CoeffAbsLevelRemaining[i] == -1 ? 0 : tbSubBlock->CoeffAbsLevelRemaining[i];
				scanData[i] = remaining + base;
			}

			if (tbSubBlock->CoeffSignFlag[i] == 1)
				scanData[i] = -scanData[i];

		}
	}

}

void Scanner::DiagonalScan4x4(int16_t* data, int16_t* result, TBSubblock* tbSubBlock)
{
	result[0] = data[15];
	result[1] = data[11];
	result[2] = data[14];
	result[3] = data[7];
	result[4] = data[10];
	result[5] = data[13];
	result[6] = data[3];
	result[7] = data[6];
	result[8] = data[9];
	result[9] = data[12];
	result[10] = data[2];
	result[11] = data[5];
	result[12] = data[8];
	result[13] = data[1];
	result[14] = data[4];
	result[15] = data[0];

	SetSubBlockElements(result, tbSubBlock);

}

void Scanner::VerticalScan4x4(int16_t* data, int16_t* result, TBSubblock* tbSubBlock)
{
	result[0] = data[15];
	result[1] = data[11];
	result[2] = data[7];
	result[3] = data[3];
	result[4] = data[14];
	result[5] = data[10];
	result[6] = data[6];
	result[7] = data[2];
	result[8] = data[13];
	result[9] = data[9];
	result[10] = data[5];
	result[11] = data[1];
	result[12] = data[12];
	result[13] = data[8];
	result[14] = data[4];
	result[15] = data[0];

	SetSubBlockElements(result, tbSubBlock);

}

void Scanner::HorizontalScan4x4(int16_t* data, int16_t* result, TBSubblock* tbSubBlock)
{
	result[0] = data[15];
	result[1] = data[14];
	result[2] = data[13];
	result[3] = data[12];
	result[4] = data[11];
	result[5] = data[10];
	result[6] = data[9];
	result[7] = data[8];
	result[8] = data[7];
	result[9] = data[6];
	result[10] = data[5];
	result[11] = data[4];
	result[12] = data[3];
	result[13] = data[2];
	result[14] = data[1];
	result[15] = data[0];

	SetSubBlockElements(result, tbSubBlock);

}

void Scanner::ReverseDiagonalScan4x4(int16_t * result, TBSubblock* tbSubBlock)
{
	int16_t data[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	GetSubBlockElements(data, tbSubBlock);

	result[0] = data[15];
	result[1] = data[13];
	result[2] = data[10];
	result[3] = data[6];
	result[4] = data[14];
	result[5] = data[11];
	result[6] = data[7];
	result[7] = data[3];
	result[8] = data[12];
	result[9] = data[8];
	result[10] = data[4];
	result[11] = data[1];
	result[12] = data[9];
	result[13] = data[5];
	result[14] = data[2];
	result[15] = data[0];
}

void Scanner::ReverseVerticalScan4x4(int16_t * result, TBSubblock* tbSubBlock)
{
	int16_t data[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	GetSubBlockElements(data, tbSubBlock);

	result[0] = data[15];
	result[1] = data[11];
	result[2] = data[7];
	result[3] = data[3];
	result[4] = data[14];
	result[5] = data[10];
	result[6] = data[6];
	result[7] = data[2];
	result[8] = data[13];
	result[9] = data[9];
	result[10] = data[5];
	result[11] = data[1];
	result[12] = data[12];
	result[13] = data[8];
	result[14] = data[4];
	result[15] = data[0];
}

void Scanner::ReverseHorizontalScan4x4(int16_t * result, TBSubblock* tbSubBlock)
{
	int16_t data[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	GetSubBlockElements(data, tbSubBlock);

	result[0] = data[15];
	result[1] = data[14];
	result[2] = data[13];
	result[3] = data[12];
	result[4] = data[11];
	result[5] = data[10];
	result[6] = data[9];
	result[7] = data[8];
	result[8] = data[7];
	result[9] = data[6];
	result[10] = data[5];
	result[11] = data[4];
	result[12] = data[3];
	result[13] = data[2];
	result[14] = data[1];
	result[15] = data[0];
}

void Scanner::ScanBlock(TB *transformBlock, PredMode predictionMode)
{
	if (predictionMode == MODE_INTRA)
	{
		if ((transformBlock->transformBlockSize == 8 && transformBlock->colorIdx == 0) || transformBlock->transformBlockSize == 4)
		{
			if (transformBlock->intraPredMode < 6 ||
				(transformBlock->intraPredMode > 14 && transformBlock->intraPredMode < 22) ||
				(transformBlock->intraPredMode > 30 && transformBlock->intraPredMode < 35))
			{
				DiagonalScanBlock(transformBlock);
			}
			else if (transformBlock->intraPredMode > 21 && transformBlock->intraPredMode < 31)
			{
				HorizontalScanBlock(transformBlock);
			}
			else if (transformBlock->intraPredMode > 5 && transformBlock->intraPredMode < 15)
			{
				VerticalScanBlock(transformBlock);
			}
		}
		else
		{
			DiagonalScanBlock(transformBlock);
		}
	}
	else
	{
		DiagonalScanBlock(transformBlock);
	}
}

void Scanner::ReverseScanBlock(TB *transformBlock, PredMode predictionMode)
{
	if (predictionMode == MODE_INTRA)
	{
		if ((transformBlock->transformBlockSize == 8 && transformBlock->colorIdx == 0) || transformBlock->transformBlockSize == 4)
		{
			if (transformBlock->intraPredMode < 6 ||
				(transformBlock->intraPredMode > 14 && transformBlock->intraPredMode < 22) ||
				(transformBlock->intraPredMode > 30 && transformBlock->intraPredMode < 35))
			{
				ReverseDiagonalScanBlock(transformBlock);
			}
			else if (transformBlock->intraPredMode > 21 && transformBlock->intraPredMode < 31)
			{
				ReverseHorizontalScanBlock(transformBlock);
			}
			else if (transformBlock->intraPredMode > 5 && transformBlock->intraPredMode < 15)
			{
				ReverseVerticalScanBlock(transformBlock);
			}
		}
		else
		{
			ReverseDiagonalScanBlock(transformBlock);
		}
	}
	else
	{
		ReverseDiagonalScanBlock(transformBlock);
	}
}

void Scanner::DiagonalScanBlock(TB *transformBlock)
{
	int level = 1;
	int elementsLeftForLevelUp = 1;
	bool startDecreasing = false;
	bool foundNonZeroSubblock = false;
	int sigIndexInSubBlock = -1;

	int numOfElemInRow = (transformBlock->transformBlockSize / 4);
	int numOfElements = numOfElemInRow * numOfElemInRow;
	int nextElemToFetch = numOfElements - 1;
	int nextElemOffset = numOfElemInRow;
	int16_t subBlock[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };;
	int16_t subResult[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };;

	int16_t* data = transformBlock->QTResidual;
	transformBlock->numOfSubblocks = numOfElements;
	transformBlock->subBlocks = new TBSubblock[numOfElements];
	transformBlock->scanType = DiagonalScan;

	int i = 0;
	for (i = 0; i < numOfElements; i++)
	{

		Fetch4x4Block(data, subBlock, nextElemToFetch, numOfElemInRow);
		DiagonalScan4x4(subBlock, subResult, &transformBlock->subBlocks[i]);

		if (!foundNonZeroSubblock && transformBlock->subBlocks[i].codedSubBlockFlag == 1)
		{
			foundNonZeroSubblock = true;
			transformBlock->indexOfSigSubblock = i;
			sigIndexInSubBlock = transformBlock->subBlocks[i].scanSigIndex;
		}

		/*Calculate next 4x4 block in scan order*/
		elementsLeftForLevelUp--;
		if (elementsLeftForLevelUp == 0)
		{
			nextElemToFetch -= nextElemOffset;
			if (!startDecreasing)
			{
				level++;
				elementsLeftForLevelUp = level;
				if (level == numOfElemInRow)
				{
					startDecreasing = true;
				}
				else
				{
					nextElemOffset = nextElemOffset + numOfElemInRow - 1;
				}
			}
			else
			{
				level--;
				elementsLeftForLevelUp = level;
				nextElemOffset = nextElemOffset - numOfElemInRow + 1;
			}
		}
		else
		{
			nextElemToFetch += numOfElemInRow - 1;
		}
		/*END Calculate next 4x4 block in scan order*/
	}

	FindLastSigCoordinates(transformBlock);
}

void Scanner::VerticalScanBlock(TB *transformBlock)
{
	bool foundNonZeroSubblock = false;
	int sigIndexInSubBlock = -1;

	int numOfElemInRow = (transformBlock->transformBlockSize / 4);
	int numOfElements = numOfElemInRow * numOfElemInRow;
	int nextElemToFetch = numOfElements - 1;
	int16_t subBlock[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };;
	int16_t subResult[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };;

	int16_t* data = transformBlock->QTResidual;
	transformBlock->numOfSubblocks = numOfElements;
	transformBlock->subBlocks = new TBSubblock[numOfElements];
	transformBlock->scanType = VerticalScan;

	int i = 0;
	int z = 1;
	for (i = 0; i < numOfElements; i++)
	{

		Fetch4x4Block(data, subBlock, nextElemToFetch, numOfElemInRow);
		VerticalScan4x4(subBlock, subResult, &transformBlock->subBlocks[i]);

		if (!foundNonZeroSubblock &&  transformBlock->subBlocks[i].codedSubBlockFlag == 1)
		{
			foundNonZeroSubblock = true;
			transformBlock->indexOfSigSubblock = i;
			sigIndexInSubBlock = transformBlock->subBlocks[i].scanSigIndex;
		}

		if (nextElemToFetch < numOfElemInRow)
		{
			nextElemToFetch = numOfElements - 1 - z;
			z++;
		}
		else
		{
			nextElemToFetch = nextElemToFetch - numOfElemInRow;
		}
	}

	FindLastSigCoordinates(transformBlock);

	int temp = transformBlock->lastSigCoeffX;
	transformBlock->lastSigCoeffX = transformBlock->lastSigCoeffY;
	transformBlock->lastSigCoeffY = temp;

}

void Scanner::HorizontalScanBlock(TB *transformBlock)
{
	bool foundNonZeroSubblock = false;
	int sigIndexInSubBlock = -1;

	int numOfElemInRow = (transformBlock->transformBlockSize / 4);
	int numOfElements = numOfElemInRow * numOfElemInRow;
	int nextElemToFetch = numOfElements - 1;
	int16_t subBlock[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int16_t subResult[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

	int16_t* data = transformBlock->QTResidual;
	transformBlock->numOfSubblocks = numOfElements;
	transformBlock->subBlocks = new TBSubblock[numOfElements];
	transformBlock->scanType = HorizontalScan;

	int i = 0;
	for (i = 0; i < numOfElements; i++)
	{


		Fetch4x4Block(data, subBlock, nextElemToFetch, numOfElemInRow);
		HorizontalScan4x4(subBlock, subResult, &transformBlock->subBlocks[i]);


		if (!foundNonZeroSubblock && transformBlock->subBlocks[i].codedSubBlockFlag == 1)
		{
			foundNonZeroSubblock = true;
			transformBlock->indexOfSigSubblock = i;
			sigIndexInSubBlock = transformBlock->subBlocks[i].scanSigIndex;
		}

		nextElemToFetch--;
	}

	FindLastSigCoordinates(transformBlock);
}

void Scanner::ReverseDiagonalScanBlock(TB *transformBlock)
{
	int resultIndex = 0;
	int numOfElemInRow = (transformBlock->transformBlockSize / 4);
	int numOfElements = transformBlock->numOfSubblocks;
	int nextElemToFetch = 0;
	int elementsBefore = 0;
	int16_t subResult[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

	transformBlock->scanType = DiagonalScan;

	int i = 0;
	for (i = 0; i < numOfElements; i++)
	{

		/*Calculate next 4x4 block in scan order*/
		int column = i % numOfElemInRow;
		int row = i / numOfElemInRow;

		elementsBefore = (numOfElemInRow - column) * (numOfElemInRow - row) - 1;

		int tempColumn = column + 1;
		int tempRow = row - 1;
		while (tempRow >= 0 && tempColumn < numOfElemInRow)
		{
			elementsBefore += numOfElemInRow - tempColumn;
			tempColumn++;
			tempRow--;
		}

		tempColumn = column - 1;
		tempRow = row + 2;
		while (tempColumn >= 0 && tempRow < numOfElemInRow)
		{
			elementsBefore += numOfElemInRow - tempRow;
			tempColumn--;
			tempRow++;
		}

		nextElemToFetch = elementsBefore;
		/*END Calculate next 4x4 block in scan order*/

		resultIndex = (i % numOfElemInRow) * 4 + (i / numOfElemInRow) * transformBlock->transformBlockSize * 4;

		ReverseDiagonalScan4x4(subResult, &transformBlock->subBlocks[nextElemToFetch]);
		FillResidual(subResult, transformBlock->QTResidual, resultIndex, transformBlock->transformBlockSize);


	}
}

void Scanner::ReverseVerticalScanBlock(TB *transformBlock)
{
	int resultIndex = 0;
	int numOfElemInRow = (transformBlock->transformBlockSize / 4);
	int numOfElements = transformBlock->numOfSubblocks;
	int nextElemToFetch = numOfElements - 1;
	int16_t subResult[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

	transformBlock->scanType = VerticalScan;

	int z = 1;
	int i = 0;
	for (i = 0; i < numOfElements; i++)
	{
		resultIndex = (i % numOfElemInRow) * 4 + (i / numOfElemInRow) * transformBlock->transformBlockSize * 4;

		ReverseVerticalScan4x4(subResult, &transformBlock->subBlocks[nextElemToFetch]);
		FillResidual(subResult, transformBlock->QTResidual, resultIndex, transformBlock->transformBlockSize);

		if (nextElemToFetch < numOfElemInRow)
		{
			nextElemToFetch = numOfElements - 1 - z;
			z++;
		}
		else
		{
			nextElemToFetch = nextElemToFetch - numOfElemInRow;
		}
	}
}

ScanType Scanner::GetScanType(TB *transformBlock, PredMode predictionMode)
{
	if (predictionMode == MODE_INTRA)
	{
		if ((transformBlock->transformBlockSize == 8 && transformBlock->colorIdx == 0) || transformBlock->transformBlockSize == 4)
		{
			if (transformBlock->intraPredMode < 6 ||
				(transformBlock->intraPredMode > 14 && transformBlock->intraPredMode < 22) ||
				(transformBlock->intraPredMode > 30 && transformBlock->intraPredMode < 35))
			{
				return DiagonalScan;
			}
			else if (transformBlock->intraPredMode > 21 && transformBlock->intraPredMode < 31)
			{
				return HorizontalScan;
			}
			else if (transformBlock->intraPredMode > 5 && transformBlock->intraPredMode < 15)
			{
				return VerticalScan;
			}
			else
			{
				return UnknownScan;
			}
		}
		else
		{
			return DiagonalScan;
		}
	}
	else
	{
		return DiagonalScan;
	}
}

void Scanner::ReverseHorizontalScanBlock(TB *transformBlock)
{
	int resultIndex = 0;
	int numOfElemInRow = (transformBlock->transformBlockSize / 4);
	int numOfElements = transformBlock->numOfSubblocks;
	int16_t nextElemToFetch = numOfElements - 1;
	int16_t subResult[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

	transformBlock->scanType = HorizontalScan;

	int i = 0;
	for (i = 0; i < numOfElements; i++)
	{
		resultIndex = (i % numOfElemInRow) * 4 + (i / numOfElemInRow) * transformBlock->transformBlockSize * 4;

		ReverseHorizontalScan4x4(subResult, &transformBlock->subBlocks[nextElemToFetch]);
		FillResidual(subResult, transformBlock->QTResidual, resultIndex, transformBlock->transformBlockSize);

		nextElemToFetch--;
	}
}

void Scanner::FillResult(int16_t * subResult, int16_t * finalResult, int index)
{
	int i, j = 0;
	for (i = index; i < index + 16; i++)
	{
		finalResult[i] = subResult[j];
		j++;
	}
}

void Scanner::FillResidual(int16_t * subResult, int16_t * residual, int index, int blockSize)
{
	int i, j = 0;
	for (i = 0; i < 16; i++)
	{
		residual[index] = subResult[j];
		j++;

		if ((i + 1) % 4 == 0)
			index = index - 3 + blockSize;
		else
			index++;

	}
}

void Scanner::Fetch4x4Block(int16_t * data, int16_t * subBlock, int nextElemToFetch, int numOfElementsInRow)
{
	int numOfValuesInRow = numOfElementsInRow * 4;
	int startingIndex = (nextElemToFetch / numOfElementsInRow)*numOfValuesInRow * 4 + (nextElemToFetch % numOfElementsInRow) * 4;

	int i, j, z = 0;
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			subBlock[z] = data[startingIndex];
			z++;
			startingIndex++;
		}
		startingIndex = startingIndex + numOfValuesInRow - 4;
	}
}

void Scanner::LastSigCoeffBin(int x, int & prefix, int & suffix)
{
	if (x == 0)
	{
		prefix = 0;
	}
	else if (x == 1)
	{
		prefix = 1;
	}
	else if (x == 2)
	{
		prefix = 2;
	}
	else if (x == 3)
	{
		prefix = 3;
	}
	else if (x == 4)
	{
		prefix = 4;
		suffix = 0;
	}
	else if (x == 5)
	{
		prefix = 4;
		suffix = 1;
	}
	else if (x == 6)
	{
		prefix = 5;
		suffix = 0;
	}

	else if (x == 7)
	{
		prefix = 5;
		suffix = 1;
	}

	else if (x == 8)
	{
		prefix = 6;
		suffix = 0;
	}

	else if (x == 9)
	{
		prefix = 6;
		suffix = 1;
	}

	else if (x == 10)
	{
		prefix = 6;
		suffix = 2;
	}

	else if (x == 11)
	{
		prefix = 6;
		suffix = 3;
	}

	else if (x == 12)
	{
		prefix = 7;
		suffix = 0;
	}
	else if (x == 13)
	{
		prefix = 7;
		suffix = 1;
	}
	else if (x == 14)
	{
		prefix = 7;
		suffix = 2;
	}
	else if (x == 15)
	{
		prefix = 7;
		suffix = 3;
	}
	else if (x == 16)
	{
		prefix = 8;
		suffix = 0;
	}
	else if (x == 17)
	{
		prefix = 8;
		suffix = 1;
	}
	else if (x == 18)
	{
		prefix = 8;
		suffix = 2;
	}
	else if (x == 19)
	{
		prefix = 8;
		suffix = 3;
	}

	else if (x == 20)
	{
		prefix = 8;
		suffix = 4;
	}
	else if (x == 21)
	{
		prefix = 8;
		suffix = 5;
	}
	else if (x == 22)
	{
		prefix = 8;
		suffix = 6;
	}
	else if (x == 23)
	{
		prefix = 8;
		suffix = 7;
	}
	else if (x == 24)
	{
		prefix = 9;
		suffix = 0;
	}
	else if (x == 25)
	{
		prefix = 9;
		suffix = 1;
	}
	else if (x == 26)
	{
		prefix = 9;
		suffix = 2;
	}
	else if (x == 27)
	{
		prefix = 9;
		suffix = 3;
	}
	else if (x == 28)
	{
		prefix = 9;
		suffix = 4;
	}
	else if (x == 29)
	{
		prefix = 9;
		suffix = 5;
	}
	else if (x == 30)
	{
		prefix = 9;
		suffix = 6;
	}
	else if (x == 31)
	{
		prefix = 9;
		suffix = 7;
	}

}

void Scanner::LastSigCoeffInvBin(int &x, int prefix, int suffix)
{
	if (prefix == 0)
	{
		x = 0;
	}
	else if (prefix == 1)
	{
		x = 1;
	}
	else if (prefix == 2)
	{
		x = 2;
	}
	else if (prefix == 3)
	{
		x = 3;
	}
	else if (prefix == 4 && suffix == 0)
	{
		x = 4;
	}
	else if (prefix == 4 && suffix == 1)
	{
		x = 5;
	}
	else if (prefix == 5 && suffix == 0)
	{
		x = 6;
	}
	else if (prefix == 5 && suffix == 1)
	{
		x = 7;
	}
	else if (prefix == 6 && suffix == 0)
	{
		x = 8;
	}
	else if (prefix == 6 && suffix == 1)
	{
		x = 9;
	}
	else if (prefix == 6 && suffix == 2)
	{
		x = 10;
	}
	else if (prefix == 6 && suffix == 3)
	{
		x = 11;
	}
	else if (prefix == 7 && suffix == 0)
	{
		x = 12;
	}
	else if (prefix == 7 && suffix == 1)
	{
		x = 13;
	}
	else if (prefix == 7 && suffix == 2)
	{
		x = 14;
	}
	else if (prefix == 7 && suffix == 3)
	{
		x = 15;
	}
	else if (prefix == 8 && suffix == 0)
	{
		x = 16;
	}
	else if (prefix == 8 && suffix == 1)
	{
		x = 17;
	}
	else if (prefix == 8 && suffix == 2)
	{
		x = 18;
	}
	else if (prefix == 8 && suffix == 3)
	{
		x = 19;
	}
	else if (prefix == 8 && suffix == 4)
	{
		x = 20;
	}
	else if (prefix == 8 && suffix == 5)
	{
		x = 21;
	}
	else if (prefix == 8 && suffix == 6)
	{
		x = 22;
	}
	else if (prefix == 8 && suffix == 7)
	{
		x = 23;
	}
	else if (prefix == 9 && suffix == 0)
	{
		x = 24;
	}
	else if (prefix == 9 && suffix == 1)
	{
		x = 25;
	}
	else if (prefix == 9 && suffix == 2)
	{
		x = 26;
	}
	else if (prefix == 9 && suffix == 3)
	{
		x = 27;
	}
	else if (prefix == 9 && suffix == 4)
	{
		x = 28;
	}
	else if (prefix == 9 && suffix == 5)
	{
		x = 29;
	}
	else if (prefix == 9 && suffix == 6)
	{
		x = 30;
	}
	else if (prefix == 9 && suffix == 7)
	{
		x = 31;
	}

}

void Scanner::PerformScanning(Partition * partition)
{
	int i;
	for (i = 0; i < partition->numberOfCTUs; i++)
	{
		PerformScanningForCU(partition->ctuTree[i]);
	}
}

void Scanner::PerformScanningForCU(CU * cu)
{
	if (cu->isSplit)
	{
		if (cu->children[0]->isInFrame)
			PerformScanningForCU(cu->children[0]);

		if (cu->children[1]->isInFrame)
			PerformScanningForCU(cu->children[1]);

		if (cu->children[2]->isInFrame)
			PerformScanningForCU(cu->children[2]);

		if (cu->children[3]->isInFrame)
			PerformScanningForCU(cu->children[3]);
	}
	else
	{
		RecursiveTUScanning(cu, cu->transformTree);
	}
}

void Scanner::RecursiveTUScanning(CU *cu, TU *tu)
{
	if (tu->hasChildren)
	{
		for (int i = 0; i < 4; ++i)
			RecursiveTUScanning(cu, tu->children[i]);
	}
	else
	{

		if (!tu->tbY.isZeroMatrix)
			ScanBlock(&tu->tbY, cu->CuPredMode);

		if (tu->tbY.transformBlockSize == 4)
		{
			if (!tu->parent->tbU.isZeroMatrix)
				ScanBlock(&tu->parent->tbU, cu->CuPredMode);
		}
		else
		{
			if (!tu->tbU.isZeroMatrix)
				ScanBlock(&tu->tbU, cu->CuPredMode);
		}

		if (tu->tbY.transformBlockSize == 4)
		{
			if (!tu->parent->tbV.isZeroMatrix)
				ScanBlock(&tu->parent->tbV, cu->CuPredMode);
		}
		else
		{
			if (!tu->tbV.isZeroMatrix)
				ScanBlock(&tu->tbV, cu->CuPredMode);
		}

	}
}

void Scanner::PerformInverseScanning(Partition * partition)
{
	int i;
	for (i = 0; i < partition->numberOfCTUs; i++)
	{
		PerformInverseScanningForCU(partition->ctuTree[i]);
	}
}

void Scanner::PerformInverseScanningForCU(CU * cu)
{
	if (cu->isSplit)
	{
		if (cu->children[0]->isInFrame)
			PerformInverseScanningForCU(cu->children[0]);

		if (cu->children[1]->isInFrame)
			PerformInverseScanningForCU(cu->children[1]);

		if (cu->children[2]->isInFrame)
			PerformInverseScanningForCU(cu->children[2]);

		if (cu->children[3]->isInFrame)
			PerformInverseScanningForCU(cu->children[3]);
	}
	else
	{
		PerformInverseScanningForTU(cu->transformTree, cu->CuPredMode);
	}
}

void Scanner::PerformInverseScanningForTU(TU * tu, PredMode predMode)
{
	if (tu->hasChildren)
	{
		PerformInverseScanningForTU(tu->children[0], predMode);
		PerformInverseScanningForTU(tu->children[1], predMode);
		PerformInverseScanningForTU(tu->children[2], predMode);
		PerformInverseScanningForTU(tu->children[3], predMode);
	}
	else
	{
		tu->tbY.QTResidual = new int16_t[tu->tbY.transformBlockSize * tu->tbY.transformBlockSize]{ 0 };
		tu->tbU.QTResidual = new int16_t[tu->tbU.transformBlockSize * tu->tbU.transformBlockSize]{ 0 };
		tu->tbV.QTResidual = new int16_t[tu->tbV.transformBlockSize * tu->tbV.transformBlockSize]{ 0 };

		if (!tu->tbY.isZeroMatrix)
			ReverseScanBlock(&tu->tbY, predMode);
		if (!tu->tbU.isZeroMatrix)
			ReverseScanBlock(&tu->tbU, predMode);
		if (!tu->tbV.isZeroMatrix)
			ReverseScanBlock(&tu->tbV, predMode);
	}
}
