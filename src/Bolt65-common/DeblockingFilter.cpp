/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "DeblockingFilter.h"
#include <cmath>




void DeblockingFilter::DeblockingFilterBlock(unsigned char * blockDataP, unsigned char * blockDataQ, int blockSize, int QP, DeblockinFilterType deblockingType)
{
	int numberOfSegments = blockSize / 4;
	int beta = Beta[QP];
	int tc = Tc[QP];
	int i;

	for (i = 0; i < numberOfSegments; i++)
	{
		unsigned char p[4][4], q[4][4];
		if (deblockingType == VERTICAL_DEB)
		{
			getRightSampleSegmentByIndex(blockDataP, blockSize, i, p);
			getLeftSampleSegmentByIndex(blockDataQ, blockSize, i, q);
		}
		else if (deblockingType == HORIZONTAL_DEB)
		{
			getBottomSampleSegmentByIndex(blockDataP, blockSize, i, p);
			getTopSampleSegmentByIndex(blockDataQ, blockSize, i, q);
		}


		bool doFilter = abs(p[0][1] - 2 * p[0][2] + p[0][3]) + abs(p[3][1] - 2 * p[3][2] + p[3][3]) + abs(q[0][2] - 2 * q[0][1] + q[0][0]) + abs(q[3][2] - 2 * q[3][1] + q[3][0]) < beta;

		if (doFilter)
		{

			bool firstCondition0 = 2 * (abs(p[0][1] - 2 * p[0][2] + p[0][3]) + abs(q[0][2] - 2 * q[0][1] + q[0][0])) < (beta >> 2);
			bool secondCondition0 = abs(p[0][0] - p[0][3]) + abs(q[0][0] - q[0][3]) < (beta >> 3);
			bool thirdCondition0 = abs(p[0][3] - q[0][0]) < ((5 * tc + 1) >> 1);

			bool firstCondition3 = 2 * (abs(p[3][1] - 2 * p[3][2] + p[3][3]) + abs(q[3][2] - 2 * q[3][1] + q[3][0])) < (beta >> 2);
			bool secondCondition3 = abs(p[3][0] - p[3][3]) + abs(q[3][0] - q[3][3]) < (beta >> 3);
			bool thirdCondition3 = abs(p[3][3] - q[3][0]) < ((5 * tc + 1) >> 1);

			bool strongFilter = firstCondition0 && secondCondition0 && thirdCondition0 && firstCondition3 && secondCondition3 && thirdCondition3;

			if (strongFilter)
				DeblockingFilter::StrongFiltering(p, q, QP);
			else
				DeblockingFilter::NormalFiltering(p, q, QP);

			if (deblockingType == VERTICAL_DEB)
			{
				setRightSampleSegmentByIndex(blockDataP, blockSize, i, p);
				setLeftSampleSegmentByIndex(blockDataQ, blockSize, i, q);
			}
			else if (deblockingType == HORIZONTAL_DEB)
			{
				setBottomSampleSegmentByIndex(blockDataP, blockSize, i, p);
				setTopSampleSegmentByIndex(blockDataQ, blockSize, i, q);
			}
		}
	}
}

void DeblockingFilter::DeblockingFilterBlockChroma(unsigned char * blockDataP, unsigned char * blockDataQ, int blockSize, int QP, DeblockinFilterType deblockingType)
{
	int numberOfSegments = blockSize / 4;
	int beta = Beta[QP];
	int tc = Tc[QP];
	int i;

	for (i = 0; i < numberOfSegments; i++)
	{
		unsigned char p[4][4], q[4][4];
		if (deblockingType == VERTICAL_DEB)
		{
			getRightSampleSegmentByIndex(blockDataP, blockSize, i, p);
			getLeftSampleSegmentByIndex(blockDataQ, blockSize, i, q);
		}
		else if (deblockingType == HORIZONTAL_DEB)
		{
			getBottomSampleSegmentByIndex(blockDataP, blockSize, i, p);
			getTopSampleSegmentByIndex(blockDataQ, blockSize, i, q);
		}

		DeblockingFilter::ChromaFiltering(p, q, QP);

		if (deblockingType == VERTICAL_DEB)
		{
			setRightSampleSegmentByIndex(blockDataP, blockSize, i, p);
			setLeftSampleSegmentByIndex(blockDataQ, blockSize, i, q);
		}
		else if (deblockingType == HORIZONTAL_DEB)
		{
			setBottomSampleSegmentByIndex(blockDataP, blockSize, i, p);
			setTopSampleSegmentByIndex(blockDataQ, blockSize, i, q);
		}
	}
}

void DeblockingFilter::NormalFiltering(unsigned char p[4][4], unsigned char q[4][4], int QP)
{
	bool changeSecondP = false;
	bool changeSecondQ = false;

	int beta = Beta[QP];
	int tc = Tc[QP];

	if (abs(p[0][1] - 2 * p[0][2] + p[0][3]) + abs(p[3][1] - 2 * p[3][2] + p[3][3]) < (beta + (beta >> 1)) >> 3)
		changeSecondP = true;

	if (abs(q[0][2] - 2 * q[0][1] + q[0][0]) + abs(q[3][2] - 2 * q[3][1] + q[3][0]) < (beta + (beta >> 1)) >> 3)
		changeSecondQ = true;

	int i;
	for (i = 0; i < 4; i++)
	{
		int delta = (9 * (q[i][0] - p[i][3]) - 3 * (q[i][1] - p[i][2]) + 8) >> 4;
		int delta0 = ComUtil::clip3(-tc, tc, delta);

		int oldP0, oldQ0;
		//Apply filtering on row only if following is true
		if (abs(delta) < 10 * tc)
		{
			//remember old values of p0 and q0 for p1 and q1 calcualations
			oldP0 = p[i][3];
			oldQ0 = q[i][0];

			p[i][3] = ComUtil::clip3(0, 255, p[i][3] + delta0);
			q[i][0] = ComUtil::clip3(0, 255, q[i][0] - delta0);

			if (changeSecondP)
			{
				int deltaP1 = ComUtil::clip3(-(tc >> 1), tc >> 1, ((((p[i][1] + oldP0 + 1) >> 1) - p[i][2] + delta0) >> 1));
				p[i][2] = ComUtil::clip3(0, 255, p[i][2] + deltaP1);
			}
			if (changeSecondQ)
			{
				int deltaQ1 = ComUtil::clip3(-(tc >> 1), tc >> 1, ((((q[i][2] + oldQ0 + 1) >> 1) - q[i][1] - delta0) >> 1));
				q[i][1] = ComUtil::clip3(0, 255, q[i][1] + deltaQ1);
			}
		}
	}
}

void DeblockingFilter::StrongFiltering(unsigned char p[4][4], unsigned char q[4][4], int QP)
{
	int tc = Tc[QP];
	int i;
	for (i = 0; i < 4; i++)
	{
		int p0 = p[i][3];
		int p1 = p[i][2];
		int p2 = p[i][1];
		int p3 = p[i][0];
		int q0 = q[i][0];
		int q1 = q[i][1];
		int q2 = q[i][2];
		int q3 = q[i][3];

		p[i][3] = ComUtil::clip3(p0 - 2 * tc, p0 + 2 * tc, (p2 + 2 * p1 + 2 * p0 + 2 * q0 + q1 + 4) >> 3);
		p[i][2] = ComUtil::clip3(p1 - 2 * tc, p1 + 2 * tc, (p2 + p1 + p0 + q0 + 2) >> 2);
		p[i][1] = ComUtil::clip3(p2 - 2 * tc, p2 + 2 * tc, (2 * p3 + 3 * p2 + p1 + p0 + q0 + 4) >> 3);

		q[i][0] = ComUtil::clip3(q0 - 2 * tc, q0 + 2 * tc, (q2 + 2 * q1 + 2 * q0 + 2 * p0 + p1 + 4) >> 3);
		q[i][1] = ComUtil::clip3(q1 - 2 * tc, q1 + 2 * tc, (q2 + q1 + q0 + p0 + 2) >> 2);
		q[i][2] = ComUtil::clip3(q2 - 2 * tc, q2 + 2 * tc, (2 * q3 + 3 * q2 + q1 + q0 + p0 + 4) >> 3);
	}
}

void DeblockingFilter::ChromaFiltering(unsigned char p[4][4], unsigned char q[4][4], int QP)
{
	int tc = Tc[QP];

	int i;
	for (i = 0; i < 4; i++)
	{
		int delta = (((q[i][0] - p[i][3]) << 2) + p[i][2] - q[i][1] + 4) >> 3;
		int delta0 = ComUtil::clip3(-tc, tc, delta);

		p[i][3] = ComUtil::clip3(0, 255, p[i][3] + delta0);
		q[i][0] = ComUtil::clip3(0, 255, q[i][0] - delta0);
	}
}

void DeblockingFilter::getLeftSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	getFirstFourBoundaryPixelsByRow(data, blockSize, index * 4, segment[0]);
	getFirstFourBoundaryPixelsByRow(data, blockSize, index * 4 + 1, segment[1]);
	getFirstFourBoundaryPixelsByRow(data, blockSize, index * 4 + 2, segment[2]);
	getFirstFourBoundaryPixelsByRow(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::getRightSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	getLastFourBoundaryPixelsByRow(data, blockSize, index * 4, segment[0]);
	getLastFourBoundaryPixelsByRow(data, blockSize, index * 4 + 1, segment[1]);
	getLastFourBoundaryPixelsByRow(data, blockSize, index * 4 + 2, segment[2]);
	getLastFourBoundaryPixelsByRow(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::getTopSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	getFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4, segment[0]);
	getFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 1, segment[1]);
	getFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 2, segment[2]);
	getFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::getBottomSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	getLastFourBoundaryPixelsByColumn(data, blockSize, index * 4, segment[0]);
	getLastFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 1, segment[1]);
	getLastFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 2, segment[2]);
	getLastFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::setLeftSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	setFirstFourBoundaryPixelsByRow(data, blockSize, index * 4, segment[0]);
	setFirstFourBoundaryPixelsByRow(data, blockSize, index * 4 + 1, segment[1]);
	setFirstFourBoundaryPixelsByRow(data, blockSize, index * 4 + 2, segment[2]);
	setFirstFourBoundaryPixelsByRow(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::setRightSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	setLastFourBoundaryPixelsByRow(data, blockSize, index * 4, segment[0]);
	setLastFourBoundaryPixelsByRow(data, blockSize, index * 4 + 1, segment[1]);
	setLastFourBoundaryPixelsByRow(data, blockSize, index * 4 + 2, segment[2]);
	setLastFourBoundaryPixelsByRow(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::setTopSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	setFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4, segment[0]);
	setFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 1, segment[1]);
	setFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 2, segment[2]);
	setFirstFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::setBottomSampleSegmentByIndex(unsigned char * data, int blockSize, int index, unsigned char segment[4][4])
{
	setLastFourBoundaryPixelsByColumn(data, blockSize, index * 4, segment[0]);
	setLastFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 1, segment[1]);
	setLastFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 2, segment[2]);
	setLastFourBoundaryPixelsByColumn(data, blockSize, index * 4 + 3, segment[3]);
}

void DeblockingFilter::getLastFourBoundaryPixelsByRow(unsigned char * data, int blockSize, int row, unsigned char boundary[4])
{
	int offset = (row + 1)* blockSize - 4;

	boundary[0] = data[offset];
	boundary[1] = data[offset + 1];
	boundary[2] = data[offset + 2];
	boundary[3] = data[offset + 3];
}

void DeblockingFilter::getFirstFourBoundaryPixelsByRow(unsigned char * data, int blockSize, int row, unsigned char boundary[4])
{
	int offset = row*blockSize;

	boundary[0] = data[offset];
	boundary[1] = data[offset + 1];
	boundary[2] = data[offset + 2];
	boundary[3] = data[offset + 3];
}

void DeblockingFilter::getFirstFourBoundaryPixelsByColumn(unsigned char * data, int blockSize, int column, unsigned char boundary[4])
{
	boundary[0] = data[column];
	boundary[1] = data[column + 1 * blockSize];
	boundary[2] = data[column + 2 * blockSize];
	boundary[3] = data[column + 3 * blockSize];
}

void DeblockingFilter::getLastFourBoundaryPixelsByColumn(unsigned char * data, int blockSize, int column, unsigned char boundary[4])
{
	int offset = blockSize*(blockSize - 4) + column;

	boundary[0] = data[offset];
	boundary[1] = data[offset + 1 * blockSize];
	boundary[2] = data[offset + 2 * blockSize];
	boundary[3] = data[offset + 3 * blockSize];
}

void DeblockingFilter::setLastFourBoundaryPixelsByRow(unsigned char * data, int blockSize, int row, unsigned char boundary[4])
{
	int offset = (row + 1)* blockSize - 4;

	data[offset] = boundary[0];
	data[offset + 1] = boundary[1];
	data[offset + 2] = boundary[2];
	data[offset + 3] = boundary[3];
}

void DeblockingFilter::setFirstFourBoundaryPixelsByRow(unsigned char * data, int blockSize, int row, unsigned char boundary[4])
{
	int offset = row*blockSize;

	data[offset] = boundary[0];
	data[offset + 1] = boundary[1];
	data[offset + 2] = boundary[2];
	data[offset + 3] = boundary[3];
}

void DeblockingFilter::setFirstFourBoundaryPixelsByColumn(unsigned char * data, int blockSize, int column, unsigned char boundary[4])
{
	data[column] = boundary[0];
	data[column + 1 * blockSize] = boundary[1];
	data[column + 2 * blockSize] = boundary[2];
	data[column + 3 * blockSize] = boundary[3];
}

void DeblockingFilter::setLastFourBoundaryPixelsByColumn(unsigned char * data, int blockSize, int column, unsigned char boundary[4])
{
	int offset = blockSize*(blockSize - 4) + column;

	data[offset] = boundary[0];
	data[offset + 1 * blockSize] = boundary[1];
	data[offset + 2 * blockSize] = boundary[2];
	data[offset + 3 * blockSize] = boundary[3];
}

DeblockingFilter::DeblockingFilter()
{
}

DeblockingFilter::~DeblockingFilter()
{
}
