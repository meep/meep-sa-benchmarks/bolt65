/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Interprediction.h"

void Interprediction::EstimateMotion(Frame* referenceFrame, unsigned char* block, int cuBlockSize, PU *pu, EncodingContext *enCtx, int &chosenBmcValue)
{

	if (enCtx->searchAlgorithm == FullIntSearch)
	{
		FullIntegerAreaSearch(referenceFrame->reconstructed_payload, block, cuBlockSize, pu->index, pu->pbY.width, pu->pbY.height, pu->pbY.startingIndexInFrame, enCtx->searchArea, pu->MVx1, pu->MVy1, enCtx, chosenBmcValue);
	}
	else if (enCtx->searchAlgorithm == FullFractionalSearch)
	{
		FullAreaSearch(referenceFrame->reconstructed_payload, block, cuBlockSize, pu->index, pu->pbY.width, pu->pbY.height, pu->pbY.startingIndexInFrame, enCtx->searchArea, pu->MVx1, pu->MVy1, enCtx, chosenBmcValue);
	}
	else if (enCtx->searchAlgorithm == TSS)
	{
		ThreeStepSearch(referenceFrame->reconstructed_payload, block, cuBlockSize, pu->index, pu->pbY.width, pu->pbY.height, pu->pbY.startingIndexInFrame, enCtx->searchArea, pu->MVx1, pu->MVy1, enCtx, chosenBmcValue);
	}
	/*if (enCtx->searchAlgorithm == FullIntSearch)
	{
		FullIntegerAreaSearch(referenceFrame->reconstructed_payload, block, cu->cbY.blockSize, cu->cbY.startingIndexInFrame, enCtx->searchArea, cu->predictionUnits[0].MVx1, cu->predictionUnits[0].MVy1, enCtx, chosenBmcValue);
	}
	else if (enCtx->searchAlgorithm == FullFractionalSearch)
	{
		FullAreaSearch(referenceFrame->reconstructed_payload, block, cu->cbY.blockSize, cu->cbY.startingIndexInFrame, enCtx->searchArea, cu->predictionUnits[0].MVx1, cu->predictionUnits[0].MVy1, enCtx, chosenBmcValue);
	}
	else if (enCtx->searchAlgorithm == ZeroVector)
	{
		NoSearchSame(cu->predictionUnits[0].MVx1, cu->predictionUnits[0].MVy1);
	}
	else if (enCtx->searchAlgorithm == TSS)
	{*/
	
	//}
	//else if (enCtx->searchAlgorithm == TakeLast)
	//{
	//	TakeFromLastFrame(referenceFrame, block, cu, enCtx);
	//}
	////Only available in transcoding mode
	//else if (enCtx->searchAlgorithm == TRANSWeightedMV)
	//{
	//	ReuseDataWeightedMVs(cu, widthCoeff, heightCoeff, cu->predictionUnits[0].MVx1, cu->predictionUnits[0].MVy1);
	//}
}

void Interprediction::FullIntegerSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue)
{
	deltaX = 0;
	deltaY = 0;

	unsigned char* predicted = new unsigned char[puWidth*puHeight];
	int predictionStartingIndex = 0;
	int predictionStartingIndexOfBest = 0;

	bool isEnd = false;
	int bestBmcValue = -1;

	while (!isEnd)
	{
		FetchBlockFromReferenceFrame(referenceFrame, enCtx->width, predicted, predictionStartingIndex, puWidth, puHeight);
		int bmcValue = ComUtil::CalculateBlockMatchingValue(block, predicted, puWidth, puHeight, enCtx);

		if (bmcValue < bestBmcValue || bestBmcValue == -1)
		{
			bestBmcValue = bmcValue;
			predictionStartingIndexOfBest = predictionStartingIndex;
		}

		predictionStartingIndex++;

		if (((predictionStartingIndex%enCtx->width) + puWidth) > enCtx->width)
			predictionStartingIndex = predictionStartingIndex + puWidth - 1;

		if (((predictionStartingIndex / enCtx->width) + puHeight) > enCtx->height)
			isEnd = true;
	}

	int integerX = (predictionStartingIndexOfBest%enCtx->width) - (startingIndex%enCtx->width);
	int integerY = (predictionStartingIndexOfBest / enCtx->width) - (startingIndex / enCtx->width);

	//In this algorithm only integer positions are considered, so last two bits are zero
	deltaX = ComUtil::sign(integerX)*(abs(integerX) << 2);
	deltaY = ComUtil::sign(integerY)*(abs(integerY) << 2);

	chosenBmcValue = bestBmcValue;

	delete[] predicted;

}

void Interprediction::FullIntegerAreaSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue)
{
	deltaX = 0;
	deltaY = 0;

	unsigned char* predicted = new unsigned char[puWidth*puHeight];

	bool isEnd = false;
	int bestBmcValue = -1;

	//find first blcok for search
	int predictionStartingIndex = startingIndex;
	int predictionStartingIndexOfBest = 0;
	int i;
	for (i = 0; i < areaSize; i++)
	{
		//go as left as possible in search area
		if (predictionStartingIndex % enCtx->width != 0)
			predictionStartingIndex--;
		else
			break;
	}

	for (i = 0; i < areaSize; i++)
	{
		//go as top as possible in search area
		if (predictionStartingIndex > enCtx->width)
			predictionStartingIndex -= enCtx->width;
		else
			break;
	}

	int predictionStart = predictionStartingIndex;
	int rowCounter = 0;
	int horizontalLimit = ComUtil::clip3(0, enCtx->width, startingIndex%enCtx->width + areaSize);
	int verticalLimit = ComUtil::clip3(0, enCtx->height, startingIndex / enCtx->width + areaSize);

	while (!isEnd)
	{
		FetchBlockFromReferenceFrame(referenceFrame, enCtx->width, predicted, predictionStartingIndex, puWidth, puHeight);
		int bmcValue = ComUtil::CalculateBlockMatchingValue(block, predicted, puWidth, puHeight, enCtx);

		if (bmcValue < bestBmcValue || bestBmcValue == -1)
		{
			bestBmcValue = bmcValue;
			predictionStartingIndexOfBest = predictionStartingIndex;
		}

		predictionStartingIndex++;

		if (((predictionStartingIndex%enCtx->width) + puWidth) > enCtx->width || (predictionStartingIndex%enCtx->width) > horizontalLimit)
		{
			rowCounter++;
			predictionStartingIndex = predictionStart + rowCounter * enCtx->width;
		}

		if (((predictionStartingIndex / enCtx->width) + puHeight) > enCtx->height || (predictionStartingIndex / enCtx->width) > verticalLimit)
			isEnd = true;
	}

	int integerX = (predictionStartingIndexOfBest%enCtx->width) - (startingIndex%enCtx->width);
	int integerY = (predictionStartingIndexOfBest / enCtx->width) - (startingIndex / enCtx->width);

	//In this algorithm only integer positions are considered, so last two bits are zero
	deltaX = ComUtil::sign(integerX)*(abs(integerX) << 2);
	deltaY = ComUtil::sign(integerY)*(abs(integerY) << 2);

	chosenBmcValue = bestBmcValue;

	delete[] predicted;


}

void Interprediction::FullAreaSearch(unsigned char * referenceFrame, unsigned char * block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext * enCtx, int & chosenBmcValue)
{
	deltaX = 0;
	deltaY = 0;

	bool isEnd = false;
	int bestBmcValue = -1;

	//find first blcok for search
	int predictionStartingIndex = startingIndex;
	int i;
	for (i = 0; i < areaSize; i++)
	{
		//go as left as possible in search area
		if (predictionStartingIndex % enCtx->width != 0)
			predictionStartingIndex--;
		else
			break;
	}

	for (i = 0; i < areaSize; i++)
	{
		//go as top as possible in search area
		if (predictionStartingIndex > enCtx->width)
			predictionStartingIndex -= enCtx->width;
		else
			break;
	}

	int predictionStart = predictionStartingIndex;
	int rowCounter = 0;
	int horizontalLimit = ComUtil::clip3(0, enCtx->width, startingIndex%enCtx->width + areaSize);
	int verticalLimit = ComUtil::clip3(0, enCtx->height, startingIndex / enCtx->width + areaSize);

	while (!isEnd)
	{
		SearchAllFractions(referenceFrame, block, predictionStartingIndex, startingIndex, puWidth, puHeight, bestBmcValue, deltaX, deltaY, enCtx);

		predictionStartingIndex++;

		if (((predictionStartingIndex%enCtx->width) + puWidth) > enCtx->width || (predictionStartingIndex%enCtx->width) > horizontalLimit)
		{
			rowCounter++;
			predictionStartingIndex = predictionStart + rowCounter * enCtx->width;
		}

		if (((predictionStartingIndex / enCtx->width) + puHeight) > enCtx->height || (predictionStartingIndex / enCtx->width) > verticalLimit)
			isEnd = true;
	}

	chosenBmcValue = bestBmcValue;
}

void Interprediction::ThreeStepSearch(unsigned char * referenceFrame, unsigned char *block, int cuBlockSize, int puIndex, int puWidth, int puHeight, int startingIndex, int areaSize, int & deltaX, int & deltaY, EncodingContext *enCtx, int &chosenBmcValue)
{
	deltaX = 0;
	deltaY = 0;

	unsigned char* predicted = new unsigned char[puWidth*puHeight];

	int bestBmcValue = -1;

	//find first block for search
	int stepCentralStartingIndex = startingIndex;
	int predictionStartingIndexOfBest = 0;
	int i;

	int divisionStep = 2;
	int step = (int)ceil((double)areaSize / divisionStep);
	int stepCounter = 0;

	while (step >= 1)
	{
		int centralRow = stepCentralStartingIndex / enCtx->width;
		int centralColumn = stepCentralStartingIndex % enCtx->width;
		stepCounter++;

		for (i = 0; i < 9; i++)
		{
			//Find 9 candidates for each step
			//If the row and column are 1, 1 then that is a central block, thats why -1 + ...
			int rowOffset = (-1 + (i / 3))*step;
			int columnOffset = (-1 + (i % 3))*step;

			//check if predicted frame is inside frame 
			if (centralRow + rowOffset >= 0 && centralRow + rowOffset + puHeight < enCtx->height && centralColumn + columnOffset >= 0 && centralColumn + columnOffset + puWidth < enCtx->width)
			{
				int predictionStartingIndex = stepCentralStartingIndex + columnOffset + rowOffset * enCtx->width;
				FetchBlockFromReferenceFrame(referenceFrame, enCtx->width, predicted, predictionStartingIndex, puWidth, puHeight);
				int BmcValue = ComUtil::CalculateBlockMatchingValue(block, predicted, puWidth, puHeight, enCtx);


				//PU factors --> 4x4 = 1  8x8 = 4  16x16 = 16  32x32 = 64
				//predictionCost += (blockWidth * blockHeight) / 16;


				if (BmcValue < bestBmcValue || bestBmcValue == -1)
				{
					bestBmcValue = BmcValue;
					predictionStartingIndexOfBest = predictionStartingIndex;
				}
			}
		}

		divisionStep = divisionStep * 2;
		stepCentralStartingIndex = predictionStartingIndexOfBest;

		if (step > 1 && stepCounter < 3)
			step = (int)ceil((double)areaSize / divisionStep);
		else
			break;

	}

	int integerX = (predictionStartingIndexOfBest%enCtx->width) - (startingIndex%enCtx->width);
	int integerY = (predictionStartingIndexOfBest / enCtx->width) - (startingIndex / enCtx->width);

	//In this algorithm only integer positions are considered, so last two bits are zero
	deltaX = ComUtil::sign(integerX)*(abs(integerX) << 2);
	deltaY = ComUtil::sign(integerY)*(abs(integerY) << 2);

	chosenBmcValue = bestBmcValue;

	delete[] predicted;

}

void Interprediction::FetchBlockFromReferenceFrame(unsigned char * data, int frameWidth, unsigned char * predictedBlock, int startingIndex, int blockWidth, int blockHeight)
{
	/*
	Theoretically, if block of memory is larger then using memcpy should be faster.
	http://nadeausoftware.com/articles/2012/05/c_c_tip_how_copy_memory_quickly
	*/
	int data_index = startingIndex;
	for (int i = 0; i < blockHeight; i++)
	{
		for (int j = 0; j < blockWidth; j++)
		{
			int index = data_index + frameWidth * i + j;
			predictedBlock[i*blockWidth + j] = data[index];
		}
	}


}

void Interprediction::ReuseDataWeightedMVs(PU * pu, float widthCoeff, float heightCoeff)
{
	double sumX = 0;
	double sumY = 0;
	double interArea = 0;
	for (int i = 0; i < pu->transMappedPUs.size(); i++)
	{

		
		sumX += pu->transMappedPUs.at(i).second->MVx1 *pu->transMappedPUs.at(i).first.RatioInCU * (1 / widthCoeff);
		sumY += pu->transMappedPUs.at(i).second->MVy1 * pu->transMappedPUs.at(i).first.RatioInCU * (1 / heightCoeff);
		interArea += pu->transMappedPUs.at(i).first.RatioInCU;

	}

		pu->MVx1 = (int)round(sumX / interArea);
		pu->MVy1 = (int)round(sumY / interArea);
	
}

void Interprediction::ReuseMajorMVs(PU * pu, float widthCoeff, float heightCoeff)
{

	double percent = -1.0;
	double sum = 0.0;
	int index = -1;
	for (int i = 0; i < pu->transMappedPUs.size(); ++i)
	{
		sum += pu->transMappedPUs[i].first.RatioInCU;

		if (pu->transMappedPUs[i].first.RatioInCU > percent) {
			percent = pu->transMappedPUs[i].first.RatioInCU;
			index = i;
		}

	}

	pu->MVx1 = (int)round(pu->transMappedPUs.at(index).second->MVx1*(1 / widthCoeff));
	pu->MVy1 = (int)round(pu->transMappedPUs.at(index).second->MVy1*(1 / heightCoeff));

}

void Interprediction::SearchAllFractions(unsigned char * referenceFrame, unsigned char * block, int predictionStartingIndex, int startingIndex, int puWidth, int puHeight, int &bestBmcValue, int &deltaX, int &deltaY, EncodingContext *enCtx)
{
	int i, j;

	int dX = ((predictionStartingIndex%enCtx->width) - (startingIndex%enCtx->width)) << 2;
	int dY = ((predictionStartingIndex / enCtx->width) - (startingIndex / enCtx->width)) << 2;

	unsigned char* predicted = new unsigned char[puWidth*puHeight];

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			int BmcValue;

			if (i == 0 && j == 0)
			{
				FetchBlockFromReferenceFrame(referenceFrame, enCtx->width, predicted, predictionStartingIndex, puWidth, puHeight);
				BmcValue = ComUtil::CalculateBlockMatchingValue(block, predicted, puWidth, puHeight, enCtx);

				if (BmcValue < bestBmcValue || bestBmcValue == -1)
				{
					bestBmcValue = BmcValue;
					deltaX = dX;
					deltaY = dY;
				}
			}
			else
			{
				int absX = abs(dX + i);
				int absY = abs(dY + j);

				bool isXNegative = dX < 0 ? true : false;
				bool isYNegative = dY < 0 ? true : false;

				if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::HEVCStandard)
				{
					Interpolation::Interpolate(puHeight, puWidth, referenceFrame, predicted, startingIndex, enCtx->width, enCtx->height, Luma, absX, absY, isXNegative, isYNegative);
				}
				else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::HEVCSF5)
				{
					Interpolation::InterpolateSF5(puHeight, puWidth, referenceFrame, predicted, startingIndex, enCtx->width, enCtx->height, absX, absY, isXNegative, isYNegative);
				}
				else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::AVCStandard)
				{
					Interpolation::AVCStandardInterpolate(puHeight, puWidth, referenceFrame, predicted, startingIndex, enCtx->width, enCtx->height, absX, absY, isXNegative, isYNegative);
				}
				else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::Bilinear)
				{
					Interpolation::InterpolateBilinear(puHeight, puWidth, referenceFrame, predicted, startingIndex, enCtx->width, enCtx->height, absX, absY, isXNegative, isYNegative);
				}
				else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::FourTap)
				{
					Interpolation::InterpolateFourTap(puHeight, puWidth, referenceFrame, predicted, startingIndex, enCtx->width, enCtx->height, absX, absY, isXNegative, isYNegative);
				}

				BmcValue = ComUtil::CalculateBlockMatchingValue(block, predicted, puWidth, puHeight, enCtx);

				if (BmcValue < bestBmcValue || bestBmcValue == -1)
				{
					bestBmcValue = BmcValue;
					deltaX = absX * (isXNegative ? -1 : 1);
					deltaY = absY * (isYNegative ? -1 : 1);
				}
			}

		}
	}

	delete[] predicted;
}

void Interprediction::GenerateInterPredictedBlock(unsigned char* referenceReconstructedPayload, unsigned char* predictedBlock, int frameWidth, int frameHeight, int puWidth, int puHeight, int puOffset, int originalStartingIndex, int deltaX, int deltaY, int cuBlocksize, int width, ColorIdx colorIdx)
{
	//This will be more complex function when weighted prediction and bi-prediction is introduced
	//TODO Check for Cb and Cr
	int yFraction, xFraction, yInteger, xInteger;

	int rowOffset, colOffset;

	if (puWidth == cuBlocksize)
	{
		rowOffset = puOffset % 2 * (cuBlocksize - puHeight);
		colOffset = 0;
	}
	else
	{
		rowOffset = puOffset / 2 * (cuBlocksize - puHeight);
		colOffset = puOffset % 2 * (cuBlocksize - puWidth);
	}


	if (colorIdx == Luma)
	{

		yFraction = deltaY & 3;
		xFraction = deltaX & 3;
		xInteger = deltaX >> 2;
		yInteger = deltaY >> 2;
	}
	else
	{
		yFraction = deltaY & 7;
		xFraction = deltaX & 7;
		xInteger = deltaX >> 3;
		yInteger = deltaY >> 3;
	}

	bool isdXNeg = deltaX < 0 ? true : false;
	bool isdYNeg = deltaY < 0 ? true : false;
	int absdX = abs(deltaX);
	int absdY = abs(deltaY);


	if (xFraction == 0 && yFraction == 0)
	{
		BlockPartition::get1dBlockByStartingIndexWithOffset((char*)referenceReconstructedPayload, predictedBlock, xInteger, yInteger, originalStartingIndex, cuBlocksize, frameWidth, frameHeight, puWidth, puHeight, puOffset);
	}
	else if (xFraction != 0 || yFraction != 0)
	{
		unsigned char* predictedBlockTmp = new unsigned char[puHeight*puWidth]{ 0 };
		//unsigned char* predictedBlockTmp2 = new unsigned char[puHeight*puWidth]{ 0 };
		//Interpolation::Interpolate(puHeight, puWidth, (unsigned char*)referenceReconstructedPayload, predictedBlockTmp, originalStartingIndex, enCtx->width, enCtx->height, colorIdx, absdX, absdY, isdXNeg, isdYNeg);
		Interpolation::InterpolateNew(puHeight, puWidth, (unsigned char*)referenceReconstructedPayload, predictedBlockTmp, originalStartingIndex, frameWidth, frameHeight, colorIdx, deltaX, deltaY);
		for (int i = 0; i < puHeight; ++i)
		{
			for (int j = 0; j < puWidth; ++j)
			{
				/*	if (predictedBlockTmp[i*puWidth + j] != predictedBlockTmp2[i*puWidth + j])
						cout << "kurac" << endl;*/

				predictedBlock[(i + rowOffset) * cuBlocksize + j + colOffset] = predictedBlockTmp[i*puWidth + j];
			}
		}

		delete[] predictedBlockTmp;
		//delete[] predictedBlockTmp2;
	}
}

void Interprediction::DeriveCandidatesForAMVP(int * MvpX, int * MvpY, int * MvpT, PU * pu, Partition *partition, int ctbSize, bool useTemporalCandidates)
{
	bool isA0Available;
	bool isA1Available;
	bool isB0Available;
	bool isB1Available;
	bool isB2Available;

	int puUpLeftIdx = pu->pbY.startingIndex;
	int puUpRightIdx = pu->pbY.startingIndex + pu->pbY.width - 1;
	int puBottomLeftIdx = pu->pbY.startingIndex + pu->pbY.height*partition->width;

	int puUpLeftIdxRow = puUpLeftIdx / partition->width;
	int puUpLeftIdxColumn = puUpLeftIdx % partition->width;

	int puUpRightIdxRow = puUpRightIdx / partition->width;
	int puUpRightIdxColumn = (puUpRightIdx % partition->width);

	int puBottomLeftIdxIdxRow = (puBottomLeftIdx / partition->width) - 1;
	int puBottomLeftIdxColumn = puBottomLeftIdx % partition->width;

	PU puA0;
	PU puA1;
	PU puB0;
	PU puB1;
	PU puB2;


	isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA0);
	isA1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA1);
	isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, ctbSize, puB0);
	isB1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn, partition->width, partition->height, ctbSize, puB1);
	isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puB2);

	bool AisAvailable = false;
	bool BisAvailable = false;
	bool CisAvailable = false;
	int Ax, Ay, At, Bx, By, Bt;

	//TODO Scaling is done only if both pictures are in short term ref pics

	//A0, A1 available?
	if (isA0Available || isA1Available)
	{
		//First pass, find non-scaled MV's
		if (isA0Available && pu->MVt1 == puA0.MVt1)
		{
			Ax = puA0.MVx1;
			Ay = puA0.MVy1;
			At = puA0.MVt1;
			AisAvailable = true;
		}
		else if (isA1Available && pu->MVt1 == puA1.MVt1)
		{
			Ax = puA1.MVx1;
			Ay = puA1.MVy1;
			At = puA1.MVt1;
			AisAvailable = true;
		}

		//Second pass, find scaled MV's
		else if (isA0Available && pu->MVt1 != puA0.MVt1)
		{
			int td = ComUtil::clip3(-128, 127, puA0.MVt1);
			int tb = ComUtil::clip3(-128, 127, pu->MVt1);

			ScaleCandidate(puA0.MVx1, puA0.MVy1, tb, td, Ax, Ay);
			At = puA0.MVt1;
			AisAvailable = true;
		}
		else if (isA1Available  && pu->MVt1 != puA1.MVt1)
		{
			int td = ComUtil::clip3(-128, 127, puA1.MVt1);
			int tb = ComUtil::clip3(-128, 127, pu->MVt1);

			ScaleCandidate(puA1.MVx1, puA1.MVy1, tb, td, Ax, Ay);
			At = puA1.MVt1;
			AisAvailable = true;
		}
	}

	//B0,B1, B2 available
	if (isB0Available || isB1Available || isB2Available)
	{
		int BTempFound = false;
		int BxTemp, ByTemp, BtTemp, BTemp;

		//find non-scaled MV
		if (isB0Available && pu->MVt1 == puB0.MVt1)
		{
			BxTemp = puB0.MVx1;
			ByTemp = puB0.MVy1;
			BtTemp = puB0.MVt1;
			BTempFound = true;
			BTemp = 0;
		}
		else if (isB1Available && pu->MVt1 == puB1.MVt1)
		{
			BxTemp = puB1.MVx1;
			ByTemp = puB1.MVy1;
			BtTemp = puB1.MVt1;
			BTempFound = true;
			BTemp = 1;
		}
		else if (isB2Available && pu->MVt1 == puB2.MVt1)
		{
			BxTemp = puB2.MVx1;
			ByTemp = puB2.MVy1;
			BtTemp = puB2.MVt1;
			BTempFound = true;
			BTemp = 2;
		}

		//first non-scaled MV found
		if (BTempFound)
		{
			//A0, A1 available
			if (!AisAvailable)
			{
				//set A
				Ax = BxTemp;
				Ay = ByTemp;
				At = BtTemp;
				AisAvailable = true;


			}
			//A0 or A1 available, set non-scaled as B
			else
			{
				Bx = BxTemp;
				By = ByTemp;
				Bt = BtTemp;
				BisAvailable = true;
			}
		}
		//non-scaled MV not found
		else
		{
			if (!AisAvailable)
			{
				if (isB0Available && pu->MVt1 != puB0.MVt1)
				{
					int td = ComUtil::clip3(-128, 127, puB0.MVt1);
					int tb = ComUtil::clip3(-128, 127, pu->MVt1);

					ScaleCandidate(puB0.MVx1, puB0.MVy1, tb, td, Bx, By);
					Bt = puB0.MVt1;
					BisAvailable = true;
				}
				else if (isB1Available  && pu->MVt1 != puB1.MVt1)
				{
					int td = ComUtil::clip3(-128, 127, puB1.MVt1);
					int tb = ComUtil::clip3(-128, 127, pu->MVt1);

					ScaleCandidate(puB1.MVx1, puB1.MVy1, tb, td, Bx, By);
					Bt = puB1.MVt1;
					BisAvailable = true;
				}
				else if (isB2Available  && pu->MVt1 != puB2.MVt1)
				{
					int td = ComUtil::clip3(-128, 127, puB2.MVt1);
					int tb = ComUtil::clip3(-128, 127, pu->MVt1);

					ScaleCandidate(puB2.MVx1, puB2.MVy1, tb, td, Bx, By);
					Bt = puB2.MVt1;
					BisAvailable = true;
				}
			}
		}
	}


	//End spatial candidates
	bool temporalNeeded = !((AisAvailable && BisAvailable) && !(Ax == Bx && Ay == By && At == Bt));

	//Temporal candidates (can be switched off)
	if (useTemporalCandidates && temporalNeeded)
	{
		bool isC0Available = false;
		bool isC1Available = true;

		////CU* C0 = BlockPartition::getRefRightDownCodingUnitByIndex(dpbm->dpb->frames[0]->ctuTree, pu, isC0Available, partition->width, partition->height);
		////CU* C1 = BlockPartition::getRefCenterCodingUnit(dpbm->dpb->frames[0]->ctuTree, cu);

		//isC0Available = isC0Available && C0->CuPredMode == MODE_INTER;
		//isC1Available = C1->CuPredMode == MODE_INTER;

		//if (isC0Available)
		//{
		//	int td = ComUtil::clip3(-128, 127, C0->predictionUnits[0].MVt1);
		//	int tb = ComUtil::clip3(-128, 127, pu->MVt1);

		//	ScaleCandidate(C0->predictionUnits[0].MVx1, C0->predictionUnits[0].MVy1, tb, td, Cx, Cy);
		//	Ct = C0->predictionUnits[0].MVt1;
		//	CisAvailable = true;
		//}
		//else if (isC1Available)
		//{
		//	int td = ComUtil::clip3(-128, 127, C1->predictionUnits[0].MVt1);
		//	int tb = ComUtil::clip3(-128, 127, pu->MVt1);

		//	ScaleCandidate(C1->predictionUnits[0].MVx1, C1->predictionUnits[0].MVy1, tb, td, Cx, Cy);
		//	Ct = C1->predictionUnits[0].MVt1;
		//	CisAvailable = true;
		//}
	}
	// End temporal candidates



	int i = 0;
	//If candidates are not found as spatial or temporal candidates fill them with zero vectors
	if (AisAvailable)
	{
		MvpX[i] = Ax;
		MvpY[i] = Ay;
		MvpT[i] = At;
		i++;
		if (BisAvailable && !(Ax == Bx && Ay == By && At == Bt))
		{
			MvpX[i] = Bx;
			MvpY[i] = By;
			MvpT[i] = Bt;
			i++;
		}
	}
	else if (BisAvailable)
	{
		MvpX[i] = Bx;
		MvpY[i] = By;
		MvpT[i] = Bt;
		i++;
	}
	if (i < 2 && CisAvailable)
	{
		/*MvpX[i] = Cx;
		MvpY[i] = Cy;
		MvpT[i] = Ct;
		i++;*/
	}
	while (i < 2)
	{
		MvpX[i] = 0;
		MvpY[i] = 0;
		MvpT[i] = pu->MVt1;
		i++;
	}


}

void Interprediction::DeriveCandidatesForMerge(int * MergeCandListX, int * MergeCandListY, int * MergeCandListT, CU *cu, PU * pu, Partition *partition, int ctbSize, bool useTemporalCandidates)
{
	int i = 0;

	int numOfMergeCandidates = 5;

	//defined in standard
	int maxSpatialCandidates = 4;

	bool isA0Available;
	bool isA1Available;
	bool isB0Available;
	bool isB1Available;
	bool isB2Available;

	int puUpLeftIdx = pu->pbY.startingIndex;
	int puUpRightIdx = pu->pbY.startingIndex + pu->pbY.width - 1;
	int puBottomLeftIdx = pu->pbY.startingIndex + pu->pbY.height*partition->width;

	int puUpLeftIdxRow = puUpLeftIdx / partition->width;
	int puUpLeftIdxColumn = puUpLeftIdx % partition->width;

	int puUpRightIdxRow = puUpRightIdx / partition->width;
	int puUpRightIdxColumn = (puUpRightIdx % partition->width);

	int puBottomLeftIdxIdxRow = (puBottomLeftIdx / partition->width) - 1;
	int puBottomLeftIdxColumn = puBottomLeftIdx % partition->width;

	PU puA0;
	PU puA1;
	PU puB0;
	PU puB1;
	PU puB2;


	if ((cu->PartMode == PART_2NxN || cu->PartMode == PART_2NxnU || cu->PartMode == PART_2NxnD) && pu->index == 1)
	{
		isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA0);
		isA1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA1);
		isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, ctbSize, puB0);
		isB1Available = false;
		isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puB2);
	}

	else if ((cu->PartMode == PART_Nx2N || cu->PartMode == PART_nLx2N || cu->PartMode == PART_nRx2N) && pu->index == 1)
	{
		isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA0);
		isA1Available = false;
		isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, ctbSize, puB0);
		isB1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn, partition->width, partition->height, ctbSize, puB1);
		isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puB2);

	}
	else {

		isA0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow + 1, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA0);
		isA1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puBottomLeftIdxIdxRow, puBottomLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puA1);
		isB0Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn + 1, partition->width, partition->height, ctbSize, puB0);
		isB1Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpRightIdxRow - 1, puUpRightIdxColumn, partition->width, partition->height, ctbSize, puB1);
		isB2Available = BlockPartition::getCandidateBy2DCoordinates(partition->ctuTree, puUpLeftIdxRow - 1, puUpLeftIdxColumn - 1, partition->width, partition->height, ctbSize, puB2);
	}




	//First spatial candidate
	if (isA1Available && i < numOfMergeCandidates)
	{
		MergeCandListX[i] = puA1.MVx1;
		MergeCandListY[i] = puA1.MVy1;
		MergeCandListT[i] = puA1.MVt1;
		i++;
	}
	//Second spatial candidate
	if (isB1Available && i < numOfMergeCandidates)
	{
		if (!(isA1Available && puB1.MVx1 == puA1.MVx1
			&& puB1.MVy1 == puA1.MVy1))
		{
			MergeCandListX[i] = puB1.MVx1;
			MergeCandListY[i] = puB1.MVy1;
			MergeCandListT[i] = puB1.MVt1;
			i++;
		}
	}

	//Third spatial candidate
	//B0 checks B1
	if (isB0Available && i < numOfMergeCandidates)
	{
		if (!(isB1Available && puB0.MVx1 == puB1.MVx1
			&& puB0.MVy1 == puB1.MVy1))
		{
			MergeCandListX[i] = puB0.MVx1;
			MergeCandListY[i] = puB0.MVy1;
			MergeCandListT[i] = puB0.MVt1;
			i++;
		}
	}

	//Fourth spatial candidate
	//A0 checks A1
	if (isA0Available && i < numOfMergeCandidates)
	{
		if (!(isA1Available && puA0.MVx1 == puA1.MVx1
			&& puA0.MVy1 == puA1.MVy1))
		{
			MergeCandListX[i] = puA0.MVx1;
			MergeCandListY[i] = puA0.MVy1;
			MergeCandListT[i] = puA0.MVt1;
			i++;
		}
	}

	//Fifth spatial candidate
	//B2 checks A1 and B1
	if (isB2Available && i < numOfMergeCandidates && i < maxSpatialCandidates)
	{
		if (!((isB1Available && puB2.MVx1 == puB1.MVx1
			&& puB2.MVy1 == puB1.MVy1) ||
			(isA1Available && puB2.MVx1 == puA1.MVx1
				&& puB2.MVy1 == puA1.MVy1)))

		{
			MergeCandListX[i] = puB2.MVx1;
			MergeCandListY[i] = puB2.MVy1;
			MergeCandListT[i] = puB2.MVt1;
			i++;
		}
	}

	if (useTemporalCandidates && i < numOfMergeCandidates)
	{
		/*CU* C0 = BlockPartition::getRefRightDownCodingUnitByIndex(dpbm.dpb.frames[0].ctuTree, cu, isC0Available, decCtx.width, decCtx.height);
		CU* C1 = BlockPartition::getRefCenterCodingUnit(dpbm.dpb.frames[0].ctuTree, cu);

		isC0Available = isB2Available && C0.CuPredMode == MODE_INTER && C0.predictionUnits[0].isProcessed;
		isC1Available = C1.CuPredMode == MODE_INTER && C1.predictionUnits[0].isProcessed;

		if ((isC0Available || isC1Available))
		{
		if (isC0Available)
		{
		MergeCandListX[i] = C0.predictionUnits[0].MVx1;
		MergeCandListY[i] = C0.predictionUnits[0].MVy1;
		MergeCandListT[i] = C0.predictionUnits[0].MVt1;
		}
		else
		{
		MergeCandListX[i] = C1.predictionUnits[0].MVx1;
		MergeCandListY[i] = C1.predictionUnits[0].MVy1;
		MergeCandListT[i] = C1.predictionUnits[0].MVt1;
		}
		i++;
		}*/
	}

	while (i < numOfMergeCandidates)
	{
		//TODO Add combined bi-predictive candidates foe B slices 

		//Zero motion candidates
		MergeCandListX[i] = 0;
		MergeCandListY[i] = 0;
		MergeCandListT[i] = pu->MVt1;
		i++;
	}
}

void Interprediction::ScaleCandidate(int deltaX, int deltaY, int tb, int td, int &scaledDeltaX, int &scaledDeltaY)
{
	int tx = (16384 + abs(td >> 1)) / td;
	int scaleFactor = ComUtil::clip3(-4096, 4095, (tb*tx + 32) >> 6);

	int dsX, dsY;

	dsX = deltaX * scaleFactor;
	dsY = deltaY * scaleFactor;

	scaledDeltaX = ComUtil::clip3(-32678, 32676, ComUtil::sign(dsX)*((abs(dsX) + 127) >> 8));
	scaledDeltaY = ComUtil::clip3(-32678, 32676, ComUtil::sign(dsY)*((abs(dsY) + 127) >> 8));

}

void Interprediction::SortCandModeList(int* candModeList)
{
	int tmp;
	if (candModeList[0] > candModeList[1])
	{
		tmp = candModeList[1];
		candModeList[1] = candModeList[0];
		candModeList[0] = tmp;
	}

	if (candModeList[0] > candModeList[2])
	{
		tmp = candModeList[2];
		candModeList[2] = candModeList[0];
		candModeList[0] = tmp;
	}

	if (candModeList[1] > candModeList[2])
	{
		tmp = candModeList[2];
		candModeList[2] = candModeList[1];
		candModeList[1] = tmp;
	}
}

void Interprediction::encodeInterPredDecision(CU *cu, Partition *partition, int ctbSize, bool useTemporalCandidates)
{
	for (int i = 0; i < cu->numOfPUs; ++i)
	{
		//first try to encode with Merge, since it is more coding efficient 
		encodeMerge(cu, &cu->predictionUnits[i], partition, ctbSize, useTemporalCandidates);

		//If it could not be encoded in Merge then encode it with AMVP
		if (cu->predictionUnits[i].predictionMode == InterpredictionMode::Unknown)
		{
			encodeAMVP(&cu->predictionUnits[i], partition, ctbSize, useTemporalCandidates);
		}
	}
}

//TODO re-check algorithm when ref lists and short term and long term pictures are constructed
void Interprediction::encodeAMVP(PU *pu, Partition *partition, int ctbSize, bool useTemporalCandidates)
{
	pu->predictionMode = AMVP;

	int MVPx[2], MVPy[2], MVPt[2];

	DeriveCandidatesForAMVP(MVPx, MVPy, MVPt, pu, partition, ctbSize, useTemporalCandidates);


	//Set flags for syntax
	//Select better candidate and calculate MVD
	int FirstDiff = abs(pu->MVx1 - MVPx[0]) + abs(pu->MVy1 - MVPy[0]);
	int SecondDiff = abs(pu->MVx1 - MVPx[1]) + abs(pu->MVy1 - MVPy[1]);

	if (FirstDiff <= SecondDiff)
	{
		pu->candidateIndex = 0;
		pu->MDabsx1 = abs(pu->MVx1 - MVPx[0]);
		pu->MDabsy1 = abs(pu->MVy1 - MVPy[0]);

		pu->MDsignx1 = ComUtil::sign(pu->MVx1 - MVPx[0]);
		pu->MDsigny1 = ComUtil::sign(pu->MVy1 - MVPy[0]);
	}
	else
	{
		pu->candidateIndex = 1;
		pu->MDabsx1 = abs(pu->MVx1 - MVPx[1]);
		pu->MDabsy1 = abs(pu->MVy1 - MVPy[1]);

		pu->MDsignx1 = ComUtil::sign(pu->MVx1 - MVPx[1]);
		pu->MDsigny1 = ComUtil::sign(pu->MVy1 - MVPy[1]);
	}

}

void Interprediction::encodeMerge(CU * cu, PU *pu, Partition *partition, int ctbSize, bool useTemporalCandidates)
{
	//Number of merge candidates is hard-coded to be 5
	int MergeCandListX[10], MergeCandListY[10], MergeCandListT[10];
	DeriveCandidatesForMerge(MergeCandListX, MergeCandListY, MergeCandListT, cu, pu, partition, ctbSize, useTemporalCandidates);

	//Check if CU can be encoded as a Merge
	for (int i = 0; i < 5; i++)
	{
		if (pu->MVx1 == MergeCandListX[i] && pu->MVy1 == MergeCandListY[i])
		{
			pu->predictionMode = Merge;
			pu->candidateIndex = i;

			//If it is possible to Merge, then check if SKIP mode is possible
			if (cu->transformTree->tbY.isZeroMatrix && cu->transformTree->tbU.isZeroMatrix && cu->transformTree->tbV.isZeroMatrix && !cu->transformTree->hasChildren)
			{
				cu->CuSkipFlag = true;
				pu->predictionMode = Skip;
			}

			break;
		}
	}
}

int Interprediction::FindBestPredictionInter(CU * cu, Partition *partition, DPBManager *dpbm, EncodingContext *enCtx)
{
	//Check the best PU mode - first check 2Nx2N

	vector<PartitionModeInter> modesToTest;

	modesToTest.push_back(PART_2Nx2N);

	if (enCtx->useSMP)
	{
		modesToTest.push_back(PART_Nx2N);
		modesToTest.push_back(PART_2NxN);

		//if (cu->cbY.blockSize > 8)
		//{
		//	modesToTest.push_back(PART_NxN); //Cannot apear in Inter prediction for now...Only if the minimum block is limited in VPS/SPS/PPS
		//}
	}
	if (enCtx->useAMP && cu->cbY.blockSize > 8)
	{
		modesToTest.push_back(PART_2NxnU);
		modesToTest.push_back(PART_2NxnD);
		modesToTest.push_back(PART_nLx2N);
		modesToTest.push_back(PART_nRx2N);
	}

	PartitionModeInter bestMode = PART_2Nx2N;
	int chosenBmcValue = -1;
	int bestBmcValue = -1;

	//If both AMP and SMP are disabled, only PART_2Nx"N is available, so bestMode stays PART_2Nx2N
	if (modesToTest.size() > 1)
	{
		for (PartitionModeInter mode : modesToTest)
		{
			cu->PartMode = mode;
			cu->CreatePredictionUnits(cu->frameWidth);

			int predictionBmc = 0;

			for (int i = 0; i < cu->numOfPUs; ++i)
			{
				unsigned char *blockYPuOriginal = new unsigned char[cu->predictionUnits[i].pbY.width *cu->predictionUnits[i].pbY.height];

				BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockYPuOriginal, cu->predictionUnits[i].pbY.startingIndexInFrame, cu->predictionUnits[i].pbY.width, cu->predictionUnits[i].pbY.height, enCtx->width, enCtx->height);
				EstimateMotion(dpbm->dpb->frames[0], blockYPuOriginal, cu->cbY.blockSize, &cu->predictionUnits[i], enCtx, chosenBmcValue);

				predictionBmc += chosenBmcValue;
				delete[] blockYPuOriginal;
			}

			cu->ClearPredictionUnits();

			if (predictionBmc < bestBmcValue || bestBmcValue < 0)
			{
				bestBmcValue = predictionBmc;
				bestMode = mode;
			}
		}
	}

	cu->PartMode = bestMode;
	cu->CreatePredictionUnits(cu->frameWidth);

	return bestBmcValue;
}

Interprediction::Interprediction()
{
}

Interprediction::~Interprediction()
{
}
