/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CU.h"
#include "BlockPartition.h"

/*-----------------------------------------------------------------------------
Function name:PerformSplit
Author/s: Piljic
Current Version: v0.2

Version history:
	-	v0.1 - Splitting CU to four children
	-	v0.2 - Additionally sets flags that indicate if newly created CU's are in Frame or not
	-	v0.3 - Removed lumaBlockOffset, since CB does not have reconstructedStartingIndex anymore

Description: Splits one CU to four child CU's

Parameters:
Inputs:
	-	frameWidth
	-	frameHeight

Outputs:
	-	fills this CU with children[0-4]
-----------------------------------------------------------------------------*/

std::string CU::getCUPathID()
{
	std::string result = "";

	CU* currCu = this;

	while (currCu != nullptr)
	{
		if (result.empty())
			result = to_string(currCu->index);
		else
			result = to_string(currCu->index) + "->" + result;

		currCu = currCu->parent;
	}

	return result;

}

void CU::PerformSplit(int frameWidth, int frameHeight)
{
	this->isSplit = true;
	this->SplitCuFlag = true;
	this->hasChildren = true;
	int lumaQP = this->cbY.cbQP;

	int i;
	int blockSize = this->cbY.blockSize;
	for (i = 0; i < 4; i++)
	{
		int lumaPixelOffset = (i / 2) * (blockSize / 2 * frameWidth) + (i % 2) * blockSize / 2;
		int chromaPixelOffset = (i / 2) * (blockSize / 4 * frameWidth / 2) + (i % 2) * blockSize / 4;

		/*int lumaBlockOffset = i* (blockSize / 2)*(blockSize / 2);
		int chromeBlockOffset = i * (blockSize / 4)*(blockSize / 4);*/

		CB luma = CB(this->cbY.startingIndex + lumaPixelOffset, blockSize / 2, lumaQP);
		CB cb = CB(this->cbU.startingIndex + chromaPixelOffset, blockSize / 4, lumaQP);
		CB cr = CB(this->cbV.startingIndex + chromaPixelOffset, blockSize / 4, lumaQP);

		CU* cu = new CU(frameWidth, frameHeight, partitionWidth, partitionHeight, luma, cb, cr);
		cu->depth = depth + 1;
		cu->parent = this;
		cu->hasParent = true;
		cu->index = i;
		cu->ctuTree = ctuTree;
		//cu->CreatePredictionUnits();

		if (this->hasParent)
			cu->ctuParent = this->ctuParent;
		else
			cu->ctuParent = this;

		this->children[i] = cu;
	}

	setFlagsForFurtherSplitting(frameWidth, frameHeight);
}

void CU::PerformSplit(int width, int height, int frameWidth, int frameHeight)
{
	this->isSplit = true;
	this->SplitCuFlag = true;
	this->hasChildren = true;
	int lumaQP = this->cbY.cbQP;

	int i;
	int blockSize = this->cbY.blockSize;
	for (i = 0; i < 4; i++)
	{
		int lumaPixelOffset = (i / 2) * (blockSize / 2 * width) + (i % 2) * blockSize / 2;
		int chromaPixelOffset = (i / 2) * (blockSize / 4 * width / 2) + (i % 2) * blockSize / 4;

		int lumaPixelOffsetFrame = (i / 2) * (blockSize / 2 * frameWidth) + (i % 2) * blockSize / 2;
		int chromaPixelOffsetFrame = (i / 2) * (blockSize / 4 * frameWidth / 2) + (i % 2) * blockSize / 4;

		CB luma = CB(this->cbY.startingIndex + lumaPixelOffset, this->cbY.startingIndexInFrame + lumaPixelOffsetFrame, blockSize / 2, lumaQP, Luma);
		CB cb = CB(this->cbU.startingIndex + chromaPixelOffset, this->cbU.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCb);
		CB cr = CB(this->cbV.startingIndex + chromaPixelOffset, this->cbV.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCr);

		CU* cu = new CU(frameWidth, frameHeight, partitionWidth, partitionHeight, luma, cb, cr);
		cu->depth = depth + 1;
		cu->parent = this;
		cu->hasParent = true;
		cu->index = i;
		cu->ctuTree = ctuTree;
		//cu->CreatePredictionUnits();

		if (this->hasParent)
			cu->ctuParent = this->ctuParent;
		else
			cu->ctuParent = this;

		this->children[i] = cu;
	}

	setFlagsForFurtherSplitting(width, height);
}

void CU::PerformSplit(int frameWidth, int frameHeight, int minCbLog2SizeY)
{
	//disable spliting smallest CU 
	if (this->cbY.blockSize <= (1 << minCbLog2SizeY))
		return;

	this->PerformSplit(frameWidth, frameHeight);
}

void CU::PerformSplit(int width, int height, int frameWidth, int frameHeight, int minCbLog2SizeY)
{
	//disable spliting smallest CU 
	if (this->cbY.blockSize <= (1 << minCbLog2SizeY))
		return;

	this->PerformSplit(width, height, frameWidth, frameHeight);
}

void CU::PerformRecursiveSplit(int minCbLog2SizeY, bool reuseData, CU* cuReuse, float widthCoeff, float heightCoeff)
{

	if (reuseData)
	{
		double avgDepth = 0.0;
		double totalRatio = 0.0;

		int numOfPUs = 0;
		for (int i = 0; i < this->transMappedCUs.size(); i++)
		{
			totalRatio += transMappedCUs[i].first.RatioInCU;
			avgDepth += transMappedCUs[i].first.RatioInCU * transMappedCUs[i].second->depth;
			numOfPUs += transMappedCUs[i].second->numOfPUs;
		}
		avgDepth += log2(widthCoeff);
		//int_avgDepth = avgDepth - 0.5;
		//avgDepth = avgDepth / transMappedCUs.size();
		if (depth == 0 || (floor(avgDepth) > depth && cbY.blockSize > 8 || this->requiresFurtherSplitting)) {
			this->isSplit = true;
			this->SplitCuFlag = true;
			this->hasChildren = true;
			int lumaQP = this->cbY.cbQP;

			int i;
			int blockSize = this->cbY.blockSize;
			for (i = 0; i < 4; i++)
			{
				int lumaPixelOffset = (i / 2) * (blockSize / 2 * partitionWidth) + (i % 2) * blockSize / 2;
				int chromaPixelOffset = (i / 2) * (blockSize / 4 * partitionWidth / 2) + (i % 2) * blockSize / 4;

				int lumaPixelOffsetFrame = (i / 2) * (blockSize / 2 * frameWidth) + (i % 2) * blockSize / 2;
				int chromaPixelOffsetFrame = (i / 2) * (blockSize / 4 * frameWidth / 2) + (i % 2) * blockSize / 4;

				CB luma = CB(this->cbY.startingIndex + lumaPixelOffset, this->cbY.startingIndexInFrame + lumaPixelOffsetFrame, blockSize / 2, lumaQP, Luma);
				CB cb = CB(this->cbU.startingIndex + chromaPixelOffset, this->cbU.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCb);
				CB cr = CB(this->cbV.startingIndex + chromaPixelOffset, this->cbV.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, lumaQP, ChromaCr);

				CU* cu = new CU(frameWidth, frameHeight, partitionWidth, partitionHeight, luma, cb, cr);

				cu->depth = depth + 1;
				cu->parent = this;
				cu->hasParent = true;
				cu->index = i;
				cu->ctuTree = ctuTree;

				if (this->hasParent)
					cu->ctuParent = this->ctuParent;
				else
					cu->ctuParent = this;

				this->children[i] = cu;

			}

			setFlagsForFurtherSplitting(partitionWidth, partitionHeight);

			MapToChildren(this, widthCoeff, heightCoeff);



			for (i = 0; i < 4; i++)
			{
				this->children[i]->PerformRecursiveSplit(minCbLog2SizeY, true, nullptr, widthCoeff, heightCoeff);
			}

		}
	}




}
/*-----------------------------------------------------------------------------
Function name:setFlagsForFurtherSplitting
Author/s: Piljic
Current Version: v0.1

Description: Sets the flags of children CU's created after Split (called in PerformSplit, but extracted as separate method for better maintainability

Parameters:
Inputs:
	-	frameWidth
	-	frameHeight

Outputs:
	-	sets the flags IsInFrame, IsLastInRow, IsLastInColumn

-----------------------------------------------------------------------------*/
void CU::setFlagsForFurtherSplitting(int frameWidth, int frameHeight)
{
	//Check if children are inside of Frame, only if CU that is splitted is last in row or in last column
	bool isWidthDividible = frameWidth % (cbY.blockSize / 2) == 0;
	bool isHeightDividible = frameHeight % (cbY.blockSize / 2) == 0;

	if (isLastInRow)
	{
		int rightChildStartingIndex = children[1]->cbY.startingIndex;
		int rightChildBlockSize = children[1]->cbY.blockSize;
		bool areAllChildrenInFrame = rightChildStartingIndex % frameWidth >= frameWidth - rightChildBlockSize;

		if (!areAllChildrenInFrame)
		{
			children[1]->isInFrame = false;
			children[3]->isInFrame = false;
			children[0]->isLastInRow = true;
			children[2]->isLastInRow = true;

			if (!isWidthDividible)
			{
				children[0]->requiresFurtherSplitting = true;
				children[2]->requiresFurtherSplitting = true;
			}
		}
		else
		{
			children[1]->isLastInRow = true;
			children[3]->isLastInRow = true;

			if (!isWidthDividible)
			{
				children[1]->requiresFurtherSplitting = true;
				children[3]->requiresFurtherSplitting = true;
			}
		}
	}

	if (isLastInColumn)
	{
		int LowerChildStartingIndex = children[2]->cbY.startingIndex;
		bool areAllChildrenInFrame = LowerChildStartingIndex < frameHeight* frameWidth;

		if (!areAllChildrenInFrame)
		{
			children[2]->isInFrame = false;
			children[3]->isInFrame = false;
			children[0]->isLastInColumn = true;
			children[1]->isLastInColumn = true;

			if (!isHeightDividible)
			{
				children[0]->requiresFurtherSplitting = true;
				children[1]->requiresFurtherSplitting = true;
			}
		}
		else
		{
			children[2]->isLastInColumn = true;
			children[3]->isLastInColumn = true;

			if (!isHeightDividible)
			{
				children[2]->requiresFurtherSplitting = true;
				children[3]->requiresFurtherSplitting = true;
			}
		}
	}
}

/*-----------------------------------------------------------------------------
Function name:InitializeTransformTree
Author/s: Piljic
Current Version: v0.1

Version history:
	-	v0.1 - Does not do anything smart, just initializes TU tree of this CU

Description: Creates Transform tree on CU as a root

Parameters:
Inputs:
	-	residual - CU's residual needed for decisions when creating transform tree

Outputs:
	-	initializes transform tree

Comments:In this version (v0.1) transformation is done on CU level, meaning that CU=TU.
		Thereby, this function does not do anything smart, it just initalizes TU
-----------------------------------------------------------------------------*/

void CU::InitializeTransformTree()
{
	//TODO This is initialization of TU where TU==CU
	//Additional logic will have to be implemented in order to create proper transfer tree

	hasTransformTree = true;

	TB tbY = TB(0, this->cbY.blockSize, 0);
	TB tbU = TB(0, this->cbU.blockSize, 1);
	TB tbV = TB(0, this->cbV.blockSize, 2);

	this->transformTree = new TU(tbY, tbU, tbV);

	if (this->CuPredMode == MODE_INTRA)
	{
		//LDOC TO DO
		this->transformTree->tbY.intraPredMode = IntraPredModeY[0];
		this->transformTree->tbU.intraPredMode = IntraPredModeY[0];
		this->transformTree->tbV.intraPredMode = IntraPredModeY[0];
	}
	this->transformTree->trafoDepth = 0;//when CU==TU trafoDepth = 0
	this->transformTree->parent = nullptr;
	*this->transformTree->children = nullptr;
	this->transformTree->splitTransformFlag = false;
}

void CU::InitializeTransformTree(int lumaSize, int frameWidth, int frameHeight)
{
	//TODO This is initialization of TU where TU==CU
	//Additional logic will have to be implemented in order to create proper transfer tree

	TB tbY = TB(cbY.startingIndexInFrame, lumaSize, 0);
	TB tbU = TB(cbU.startingIndexInFrame, lumaSize / 2, 1);
	TB tbV = TB(cbV.startingIndexInFrame, lumaSize / 2, 2);
	tbY.startingIndex = cbY.startingIndex;
	tbU.startingIndex = cbU.startingIndex;
	tbV.startingIndex = cbV.startingIndex;
	transformTree = new TU(tbY, tbU, tbV);
	transformTree->index = 0;
	hasTransformTree = true;

	transformTree->trafoDepth = 0;//when CU==TU trafoDepth = 0
	transformTree->parent = nullptr;
	*transformTree->children = nullptr;
	this->transformTree->hasChildren = false;
	this->transformTree->splitTransformFlag = false;
}

void CU::InitializeTransformTree(int lumaSize)
{

	TB tbY = TB(cbY.startingIndexInFrame, lumaSize, 0);
	TB tbU = TB(cbU.startingIndexInFrame, lumaSize / 2, 1);
	TB tbV = TB(cbV.startingIndexInFrame, lumaSize / 2, 2);
	tbY.startingIndex = cbY.startingIndex;
	tbU.startingIndex = cbU.startingIndex;
	tbV.startingIndex = cbV.startingIndex;
	transformTree = new TU(tbY, tbU, tbV);
	hasTransformTree = true;
	transformTree->index = 0;

	transformTree->trafoDepth = 0;//when CU==TU trafoDepth = 0
	transformTree->parent = nullptr;
	*transformTree->children = nullptr;
	this->transformTree->hasChildren = false;
	this->transformTree->splitTransformFlag = false;
}


/*-----------------------------------------------------------------------------
Function name:CreatePredictionUnits
Author/s: Piljic
Current Version: v0.1

Version history:
-	v0.1 - Creates one prediction units (CU==PU)

Description: Creates Precition Units needed for prediction

Parameters:
Inputs:
-	residual - CU's residual needed for decisions when creating transform tree

Outputs:
-	one, two or four prediction units

Comments:In this version (v0.1) only one PU is created (PU==CU)
-----------------------------------------------------------------------------*/
void CU::CreatePredictionUnits(int frameWidth)
{
	if (this->PartMode == PartitionModeInter::PART_2Nx2N)
	{
		numOfPUs = 1;
		predictionUnits = new PU[numOfPUs];

		PB pbY = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize);
		PB pbU = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize);
		PB pbV = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize);

		pbY.startingIndex = cbY.startingIndex;
		pbU.startingIndex = cbU.startingIndex;
		pbV.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY, pbU, pbV);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;
	}
	else if (this->PartMode == PartitionModeInter::PART_2NxN)
	{
		numOfPUs = 2;
		predictionUnits = new PU[numOfPUs];

		// PARTITION 0
		PB pbY0 = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize / 2);
		PB pbU0 = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize / 2);
		PB pbV0 = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize / 2);

		pbY0.startingIndex = cbY.startingIndex;
		pbU0.startingIndex = cbU.startingIndex;
		pbV0.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY0, pbU0, pbV0);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;

		// PARTITION 1
		PB pbY1 = PB((cbY.blockSize / 2 * frameWidth) + this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize / 2);
		PB pbU1 = PB((cbU.blockSize / 2 * frameWidth / 2) + this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize / 2);
		PB pbV1 = PB((cbV.blockSize / 2 * frameWidth / 2) + this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize / 2);

		pbY1.startingIndex = (cbY.blockSize / 2 * partitionWidth) + cbY.startingIndex; // TO DO
		pbU1.startingIndex = (cbY.blockSize / 2 * partitionWidth / 2) + cbU.startingIndex;
		pbV1.startingIndex = (cbY.blockSize / 2 * partitionWidth / 2) + cbV.startingIndex;

		predictionUnits[1] = PU(pbY1, pbU1, pbV1);

		predictionUnits[1].MVx1 = 0;
		predictionUnits[1].MVy1 = 0;
		predictionUnits[1].MVt1 = 0;

		predictionUnits[1].MVx2 = 0;
		predictionUnits[1].MVy2 = 0;
		predictionUnits[1].MVt2 = 0;

		predictionUnits[1].index = 1;

	}
	else if (this->PartMode == PartitionModeInter::PART_Nx2N)
	{
		numOfPUs = 2;
		predictionUnits = new PU[numOfPUs];

		// PARTITION 0
		PB pbY0 = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize / 2, this->cbY.blockSize);
		PB pbU0 = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize / 2, this->cbU.blockSize);
		PB pbV0 = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize / 2, this->cbV.blockSize);

		pbY0.startingIndex = cbY.startingIndex;
		pbU0.startingIndex = cbU.startingIndex;
		pbV0.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY0, pbU0, pbV0);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;

		// PARTITION 1
		PB pbY1 = PB(cbY.blockSize / 2 + this->cbY.startingIndexInFrame, this->cbY.blockSize / 2, this->cbY.blockSize);
		PB pbU1 = PB(cbU.blockSize / 2 + this->cbU.startingIndexInFrame, this->cbU.blockSize / 2, this->cbU.blockSize);
		PB pbV1 = PB(cbV.blockSize / 2 + this->cbV.startingIndexInFrame, this->cbV.blockSize / 2, this->cbV.blockSize);

		pbY1.startingIndex = cbY.blockSize / 2 + cbY.startingIndex; // TO DO
		pbU1.startingIndex = cbY.blockSize / 2 + cbU.startingIndex;
		pbV1.startingIndex = cbY.blockSize / 2 + cbV.startingIndex;

		predictionUnits[1] = PU(pbY1, pbU1, pbV1);

		predictionUnits[1].MVx1 = 0;
		predictionUnits[1].MVy1 = 0;
		predictionUnits[1].MVt1 = 0;

		predictionUnits[1].MVx2 = 0;
		predictionUnits[1].MVy2 = 0;
		predictionUnits[1].MVt2 = 0;

		predictionUnits[1].index = 1;

	}
	else if (this->PartMode == PartitionModeInter::PART_2NxnU)
	{
		numOfPUs = 2;
		predictionUnits = new PU[numOfPUs];

		// PARTITION 0
		PB pbY0 = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize / 4);
		PB pbU0 = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize / 4);
		PB pbV0 = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize / 4);

		pbY0.startingIndex = cbY.startingIndex;
		pbU0.startingIndex = cbU.startingIndex;
		pbV0.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY0, pbU0, pbV0);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;

		// PARTITION 1
		PB pbY1 = PB((cbY.blockSize / 4 * frameWidth) + this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize * 3 / 4);
		PB pbU1 = PB((cbU.blockSize / 4 * frameWidth / 2) + this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize * 3 / 4);
		PB pbV1 = PB((cbV.blockSize / 4 * frameWidth / 2) + this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize * 3 / 4);

		pbY1.startingIndex = (cbY.blockSize / 4 * partitionWidth) + cbY.startingIndex; // TO DO
		pbU1.startingIndex = (cbY.blockSize / 4 * partitionWidth / 2) + cbU.startingIndex;
		pbV1.startingIndex = (cbY.blockSize / 4 * partitionWidth / 2) + cbV.startingIndex;

		predictionUnits[1] = PU(pbY1, pbU1, pbV1);

		predictionUnits[1].MVx1 = 0;
		predictionUnits[1].MVy1 = 0;
		predictionUnits[1].MVt1 = 0;

		predictionUnits[1].MVx2 = 0;
		predictionUnits[1].MVy2 = 0;
		predictionUnits[1].MVt2 = 0;

		predictionUnits[1].index = 1;
	}
	else if (this->PartMode == PartitionModeInter::PART_2NxnD)
	{
		numOfPUs = 2;
		predictionUnits = new PU[numOfPUs];

		// PARTITION 0
		PB pbY0 = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize * 3 / 4);
		PB pbU0 = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize * 3 / 4);
		PB pbV0 = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize * 3 / 4);

		pbY0.startingIndex = cbY.startingIndex;
		pbU0.startingIndex = cbU.startingIndex;
		pbV0.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY0, pbU0, pbV0);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;

		// PARTITION 1
		PB pbY1 = PB((cbY.blockSize * 3 / 4 * frameWidth) + this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize / 4);
		PB pbU1 = PB((cbU.blockSize * 3 / 4 * frameWidth / 2) + this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize / 4);
		PB pbV1 = PB((cbV.blockSize * 3 / 4 * frameWidth / 2) + this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize / 4);

		pbY1.startingIndex = (cbY.blockSize * 3 / 4 * partitionWidth) + cbY.startingIndex; // TO DO
		pbU1.startingIndex = (cbY.blockSize * 3 / 4 * partitionWidth / 2) + cbU.startingIndex;
		pbV1.startingIndex = (cbY.blockSize * 3 / 4 * partitionWidth / 2) + cbV.startingIndex;

		predictionUnits[1] = PU(pbY1, pbU1, pbV1);

		predictionUnits[1].MVx1 = 0;
		predictionUnits[1].MVy1 = 0;
		predictionUnits[1].MVt1 = 0;

		predictionUnits[1].MVx2 = 0;
		predictionUnits[1].MVy2 = 0;
		predictionUnits[1].MVt2 = 0;

		predictionUnits[1].index = 1;
	}
	else if (this->PartMode == PartitionModeInter::PART_nLx2N)
	{
		numOfPUs = 2;
		predictionUnits = new PU[numOfPUs];

		// PARTITION 0
		PB pbY0 = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize / 4, this->cbY.blockSize);
		PB pbU0 = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize / 4, this->cbU.blockSize);
		PB pbV0 = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize / 4, this->cbV.blockSize);

		pbY0.startingIndex = cbY.startingIndex;
		pbU0.startingIndex = cbU.startingIndex;
		pbV0.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY0, pbU0, pbV0);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;

		// PARTITION 1
		PB pbY1 = PB(cbY.blockSize / 4 + this->cbY.startingIndexInFrame, this->cbY.blockSize * 3 / 4, this->cbY.blockSize);
		PB pbU1 = PB(cbU.blockSize / 4 + this->cbU.startingIndexInFrame, this->cbU.blockSize * 3 / 4, this->cbU.blockSize);
		PB pbV1 = PB(cbV.blockSize / 4 + this->cbV.startingIndexInFrame, this->cbV.blockSize * 3 / 4, this->cbV.blockSize);

		pbY1.startingIndex = cbY.blockSize / 4 + cbY.startingIndex; // TO DO
		pbU1.startingIndex = cbY.blockSize / 4 + cbU.startingIndex;
		pbV1.startingIndex = cbY.blockSize / 4 + cbV.startingIndex;

		predictionUnits[1] = PU(pbY1, pbU1, pbV1);

		predictionUnits[1].MVx1 = 0;
		predictionUnits[1].MVy1 = 0;
		predictionUnits[1].MVt1 = 0;

		predictionUnits[1].MVx2 = 0;
		predictionUnits[1].MVy2 = 0;
		predictionUnits[1].MVt2 = 0;

		predictionUnits[1].index = 1;
	}
	else if (this->PartMode == PartitionModeInter::PART_nRx2N)
	{
		numOfPUs = 2;
		predictionUnits = new PU[numOfPUs];

		// PARTITION 0
		PB pbY0 = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize * 3 / 4, this->cbY.blockSize);
		PB pbU0 = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize * 3 / 4, this->cbU.blockSize);
		PB pbV0 = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize * 3 / 4, this->cbV.blockSize);

		pbY0.startingIndex = cbY.startingIndex;
		pbU0.startingIndex = cbU.startingIndex;
		pbV0.startingIndex = cbV.startingIndex;

		predictionUnits[0] = PU(pbY0, pbU0, pbV0);

		predictionUnits[0].MVx1 = 0;
		predictionUnits[0].MVy1 = 0;
		predictionUnits[0].MVt1 = 0;

		predictionUnits[0].MVx2 = 0;
		predictionUnits[0].MVy2 = 0;
		predictionUnits[0].MVt2 = 0;

		predictionUnits[0].index = 0;

		// PARTITION 1
		PB pbY1 = PB(cbY.blockSize * 3 / 4 + this->cbY.startingIndexInFrame, this->cbY.blockSize / 4, this->cbY.blockSize);
		PB pbU1 = PB(cbU.blockSize * 3 / 4 + this->cbU.startingIndexInFrame, this->cbU.blockSize / 4, this->cbU.blockSize);
		PB pbV1 = PB(cbV.blockSize * 3 / 4 + this->cbV.startingIndexInFrame, this->cbV.blockSize / 4, this->cbV.blockSize);

		pbY1.startingIndex = cbY.blockSize * 3 / 4 + cbY.startingIndex; // TO DO
		pbU1.startingIndex = cbY.blockSize * 3 / 4 + cbU.startingIndex;
		pbV1.startingIndex = cbY.blockSize * 3 / 4 + cbV.startingIndex;

		predictionUnits[1] = PU(pbY1, pbU1, pbV1);

		predictionUnits[1].MVx1 = 0;
		predictionUnits[1].MVy1 = 0;
		predictionUnits[1].MVt1 = 0;

		predictionUnits[1].MVx2 = 0;
		predictionUnits[1].MVy2 = 0;
		predictionUnits[1].MVt2 = 0;

		predictionUnits[1].index = 1;
	}
	else // PART NxN
	{
		numOfPUs = 4;
		predictionUnits = new PU[numOfPUs];

		for (int i = 0; i < numOfPUs; ++i)
		{
			int lumaPixelOffset = (i / 2) * (this->cbY.blockSize / 2 * frameWidth) + (i % 2) * this->cbY.blockSize / 2;
			int chromaPixelOffset = (i / 2) * (this->cbY.blockSize / 4 * frameWidth / 2) + (i % 2) * this->cbY.blockSize / 4;

			int lumaPixelOffsetPartition = (i / 2) * (this->cbY.blockSize / 2 * partitionWidth) + (i % 2) * this->cbY.blockSize / 2;
			int chromaPixelOffsetPartition = (i / 2) * (this->cbY.blockSize / 4 * partitionWidth / 2) + (i % 2) * this->cbY.blockSize / 4;

			PB pbY0 = PB(this->cbY.startingIndexInFrame + lumaPixelOffset, this->cbY.blockSize / 2, this->cbY.blockSize / 2);
			PB pbU0 = PB(this->cbU.startingIndexInFrame + chromaPixelOffset, this->cbU.blockSize / 2, this->cbU.blockSize / 2);
			PB pbV0 = PB(this->cbV.startingIndexInFrame + chromaPixelOffset, this->cbV.blockSize / 2, this->cbV.blockSize / 2);

			pbY0.startingIndex = cbY.startingIndex + lumaPixelOffsetPartition;
			pbU0.startingIndex = cbU.startingIndex + chromaPixelOffsetPartition;
			pbV0.startingIndex = cbV.startingIndex + chromaPixelOffsetPartition;

			predictionUnits[i] = PU(pbY0, pbU0, pbV0);

			predictionUnits[i].MVx1 = 0;
			predictionUnits[i].MVy1 = 0;
			predictionUnits[i].MVt1 = 0;

			predictionUnits[i].MVx2 = 0;
			predictionUnits[i].MVy2 = 0;
			predictionUnits[i].MVt2 = 0;

			predictionUnits[i].index = i;
		}
	}

}

void CU::ClearPredictionUnits()
{
	this->PartMode = PartitionModeInter::PART_INTER_UNDEFINED;
	numOfPUs = 0;
	delete[] predictionUnits;
}

void CU::MergePredictionUnits(int frameWidth)
{
	numOfPUs = 1;
	this->PartMode = PartitionModeInter::PART_2Nx2N;

	PB pbY = PB(this->cbY.startingIndexInFrame, this->cbY.blockSize, this->cbY.blockSize);
	PB pbU = PB(this->cbU.startingIndexInFrame, this->cbU.blockSize, this->cbU.blockSize);
	PB pbV = PB(this->cbV.startingIndexInFrame, this->cbV.blockSize, this->cbV.blockSize);

	pbY.startingIndex = cbY.startingIndex;
	pbU.startingIndex = cbU.startingIndex;
	pbV.startingIndex = cbV.startingIndex;

	predictionUnits[0].pbY = pbY;
	predictionUnits[0].pbU = pbU;
	predictionUnits[0].pbV = pbV;

}


CU::CU(int _frameWidth, int _frameHeight, int _partitionWidth, int _partitionHeight)
{
	frameWidth = _frameWidth;
	frameHeight = _frameHeight;
	partitionWidth = _partitionWidth;
	partitionHeight = _partitionHeight;

	isSplit = false;
	isLastInRow = false;
	isLastInColumn = false;
	isInFrame = true;
	requiresFurtherSplitting = false;
	SplitCuFlag = false;

	CuTransquantByPassFlag = false; //not present
	CuSkipFlag = false; //not present
	PredModeFlag = true; //not present
	CuPredMode = UNKNOWN;
	PartMode = 0;
	IntraSplitFlag = 0;
	//PMode = PART_2Nx2N;
	PcmFlag = 0;
	//RqtRootCbf = 1;


	isLeftCached = false;
	isAboveCached = false;
	isLeftAvailable = false;
	isAboveAvailable = false;
	leftCU = nullptr;
	aboveCU = nullptr;

	*children = nullptr;
	parent = nullptr;
	ctuParent = this;
	predictionUnits = nullptr;
	ctuTree = nullptr;

	hasParent = false;
	hasChildren = false;
	depth = 0;

	numOfPUs = 0;
	predictionUnits = nullptr;

	hasTransformTree = false;
	transformTree = nullptr;

	indexInFrame = -1;
	id = idClass++;
}

CU::CU(int _frameWidth, int _frameHeight, int _partitionWidth, int _partitionHeight, CB _cbY, CB _cbU, CB _cbV)
{
	cbY = _cbY;
	cbU = _cbU;
	cbV = _cbV;

	isSplit = false;
	isLastInRow = false;
	isLastInColumn = false;
	isInFrame = true;
	requiresFurtherSplitting = false;
	SplitCuFlag = false;

	frameWidth = _frameWidth;
	frameHeight = _frameHeight;

	partitionWidth = _partitionWidth;
	partitionHeight = _partitionHeight;

	CuTransquantByPassFlag = false; //not present
	CuSkipFlag = false; //not present
	PredModeFlag = true; //not present
	CuPredMode = UNKNOWN;
	PartMode = 0;
	IntraSplitFlag = 0;
	//PMode = PART_2Nx2N;
	PcmFlag = 0;
	//RqtRootCbf = 1;


	isLeftCached = false;
	isAboveCached = false;
	isLeftAvailable = false;
	isAboveAvailable = false;
	leftCU = nullptr;
	aboveCU = nullptr;

	*children = nullptr;
	parent = nullptr;
	ctuParent = this;
	predictionUnits = nullptr;

	hasParent = false;
	hasChildren = false;
	depth = 0;

	numOfPUs = 0;
	predictionUnits = nullptr;

	hasTransformTree = false;
	transformTree = nullptr;

	indexInFrame = -1;
	id = idClass++;
}

int CU::idClass = 0;

CU::~CU()
{
	if (*children != nullptr)
	{
		delete children[0];
		delete children[1];
		delete children[2];
		delete children[3];
	}

	if (predictionUnits != nullptr)
		delete[] predictionUnits;

	if (hasTransformTree)
		delete transformTree;

}

bool CU::isLeftCodingUnitAvailable(int frameWidth)
{
	if (isLeftCached)
		return isLeftAvailable;
	else
	{
		leftCU = BlockPartition::getLeftCodingUnit(ctuTree, this, isLeftAvailable, frameWidth);
		isLeftCached = true;
		return isLeftAvailable;
	}

}

bool CU::isAboveCodingUnitAvailable(int frameWidth)
{
	if (isAboveCached)
		return isAboveAvailable;
	else
	{
		aboveCU = BlockPartition::getAboveCodingUnit(ctuTree, this, isAboveAvailable, frameWidth);
		isAboveCached = true;
		return isAboveAvailable;
	}

}

int CU::getMajorCUFromTransmappedCUs() {
	if (isMajorMappedCUCached)
		return majorMappedCUIndex;
	else
	{
		double percent = -1.0;
		double sum = 0.0;
		double coeff = (double)transMappedCUs[0].second->frameWidth / frameWidth;
		int index = -1;
		for (int i = 0; i < transMappedCUs.size(); ++i)
		{
			sum += transMappedCUs[i].first.RatioInCU;

			if (transMappedCUs[i].first.RatioInCU > percent) {
				percent = transMappedCUs[i].first.RatioInCU;
				index = i;
			}

			if (percent > 1.0 - sum)
			{
				majorMappedCUIndex = index;
				isMajorMappedCUCached = true;
				return index;
			}
		}
		majorMappedCUIndex = index;
		isMajorMappedCUCached = true;
		return index;
	}
}

void CU::RejectPredictionMode()
{
	if (predictionUnits != nullptr)
		delete[] predictionUnits;

	predictionUnits = nullptr;

	numOfPUs = 0;


}

void CU::sort() // sort vector
{
	for (int i = 1; i < transMappedCUs.size(); i++)
	{
		for (int j = 0; j < transMappedCUs.size() - i; j++)
		{
			if (transMappedCUs.at(j).first.RatioInCU < transMappedCUs.at(j + 1).first.RatioInCU)
			{
				std::pair<RatioCU, CU*> temp = transMappedCUs.at(j);
				transMappedCUs.at(j) = transMappedCUs.at(j + 1);
				transMappedCUs.at(j + 1) = temp;
			}
		}
	}
}

CU* CU::getLeftCodingUnit()
{
	return leftCU;
}

CU* CU::getAboveCodingUnit()
{
	return aboveCU;
}

bool CU::getPartOfInterSplitFlag()
{
	if (CuPredMode == MODE_INTER && PartMode != PartitionModeInter::PART_2Nx2N)
		return true;
	else
		return false;
}

int CU::getIntraSplitFlag()
{
	if (CuPredMode == MODE_INTRA && PartMode == PartitionModeIntra::INTRA_PART_NxN)
		return 1;
	else
		return 0;
}

void CU::decodeIntraPredDecision(int width)
{
	if (PartMode == PartitionModeIntra::INTRA_PART_2Nx2N)
	{
		int candModeList[3];
		DeriveCandidatesForIntraPredictionPARTNxN(candModeList, 0, partitionWidth);

		if (PrevIntraLumaPredFlag[0])
		{
			IntraPredModeY[0] = candModeList[MpmIdx[0]];
		}
		else
		{
			int intraLuma = RemIntraLumaPredMode[0];
			SortCandModeList(candModeList);

			if (RemIntraLumaPredMode[0] + 3 > candModeList[2])
				intraLuma = intraLuma + 3;
			else
				if (RemIntraLumaPredMode[0] + 2 > candModeList[1])
					intraLuma = intraLuma + 2;
				else
					if (RemIntraLumaPredMode[0] + 1 > candModeList[0])
						intraLuma = intraLuma + 1;


			IntraPredModeY[0] = intraLuma;
		}
	}
	else //PART NxN
	{
		int candModeList[3];
		for (int i = 0; i < 4; ++i)
		{
			DeriveCandidatesForIntraPredictionPARTNxN(candModeList, i, partitionWidth);

			if (PrevIntraLumaPredFlag[i])
			{
				IntraPredModeY[i] = candModeList[MpmIdx[i]];
			}
			else
			{
				int intraLuma = RemIntraLumaPredMode[i];
				SortCandModeList(candModeList);

				if (RemIntraLumaPredMode[i] + 3 > candModeList[2])
					intraLuma = intraLuma + 3;
				else
					if (RemIntraLumaPredMode[i] + 2 > candModeList[1])
						intraLuma = intraLuma + 2;
					else
						if (RemIntraLumaPredMode[i] + 1 > candModeList[0])
							intraLuma = intraLuma + 1;


				IntraPredModeY[i] = intraLuma;
			}
		}
	}

	getIntraPredictionModeChroma();

}

void CU::encodeIntraPredDecision(int width)
{
	if (PartMode == PartitionModeIntra::INTRA_PART_2Nx2N)
	{
		int candModeList[3];
		DeriveCandidatesForIntraPredictionPARTNxN(candModeList, 0, partitionWidth);

		if (IntraPredModeY[0] == candModeList[0])
		{
			PrevIntraLumaPredFlag[0] = true;
			MpmIdx[0] = 0;
		}
		else
			if (IntraPredModeY[0] == candModeList[1])
			{
				PrevIntraLumaPredFlag[0] = true;
				MpmIdx[0] = 1;
			}
			else
				if (IntraPredModeY[0] == candModeList[2])
				{
					PrevIntraLumaPredFlag[0] = true;
					MpmIdx[0] = 2;
				}
				else
				{
					PrevIntraLumaPredFlag[0] = false;
					RemIntraLumaPredMode[0] = IntraPredModeY[0];
					SortCandModeList(candModeList);

					if (candModeList[2] < RemIntraLumaPredMode[0])
						RemIntraLumaPredMode[0] = RemIntraLumaPredMode[0] - 3;
					else
						if (candModeList[1] < RemIntraLumaPredMode[0])
							RemIntraLumaPredMode[0] = RemIntraLumaPredMode[0] - 2;
						else
							if (candModeList[0] < RemIntraLumaPredMode[0])
								RemIntraLumaPredMode[0] = RemIntraLumaPredMode[0] - 1;
				}
	}
	else
	{
		for (int i = 0; i < 4; ++i)
		{
			int candModeList[3];
			DeriveCandidatesForIntraPredictionPARTNxN(candModeList, i, partitionWidth);

			if (IntraPredModeY[i] == candModeList[0])
			{
				PrevIntraLumaPredFlag[i] = true;
				MpmIdx[i] = 0;
			}
			else
				if (IntraPredModeY[i] == candModeList[1])
				{
					PrevIntraLumaPredFlag[i] = true;
					MpmIdx[i] = 1;
				}
				else
					if (IntraPredModeY[i] == candModeList[2])
					{
						PrevIntraLumaPredFlag[i] = true;
						MpmIdx[i] = 2;
					}
					else
					{
						PrevIntraLumaPredFlag[i] = false;
						RemIntraLumaPredMode[i] = IntraPredModeY[i];
						SortCandModeList(candModeList);

						if (candModeList[2] < RemIntraLumaPredMode[i])
							RemIntraLumaPredMode[i] = RemIntraLumaPredMode[i] - 3;
						else
							if (candModeList[1] < RemIntraLumaPredMode[i])
								RemIntraLumaPredMode[i] = RemIntraLumaPredMode[i] - 2;
							else
								if (candModeList[0] < RemIntraLumaPredMode[i])
									RemIntraLumaPredMode[i] = RemIntraLumaPredMode[i] - 1;
					}
		}
	}

}

void CU::DeriveCandidatesForIntraPrediction(int* candModeList, int width)
{
	bool leftIsAvailable = isLeftCodingUnitAvailable(width);
	bool aboveIsAvailable = isAboveCodingUnitAvailable(width);
	CU* A = nullptr;
	CU* B = nullptr;

	if (leftIsAvailable)
		A = getLeftCodingUnit();
	if (aboveIsAvailable)
		B = getAboveCodingUnit();

	int candModeA;
	int candModeB;

	if (aboveIsAvailable)
	{
		if (B->CuPredMode != MODE_INTRA || B->PcmFlag == true || BlockPartition::isCornerUpperInCTU(this))
		{
			candModeB = 1;
		}
		else
		{
			candModeB = B->IntraPredModeY[0];
		}


	}
	else
	{
		candModeB = 1;
	}

	if (leftIsAvailable)
	{
		if (A->CuPredMode != MODE_INTRA || A->PcmFlag == true)
		{
			candModeA = 1;
		}
		else
		{
			candModeA = A->IntraPredModeY[0];
		}
	}
	else
	{
		candModeA = 1;
	}


	if (candModeA == candModeB)
	{
		if (candModeA < 2)
		{
			candModeList[0] = 0;
			candModeList[1] = 1;
			candModeList[2] = 26;
		}
		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = 2 + ((candModeA + 29) % 32);
			candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
		}
	}

	else
	{
		candModeList[0] = candModeA;
		candModeList[1] = candModeB;

		if (candModeList[0] != 0 && candModeList[1] != 0)
		{
			candModeList[2] = 0;
		}
		else
		{
			if (candModeList[0] != 1 && candModeList[1] != 1)
			{
				candModeList[2] = 1;
			}
			else
			{
				candModeList[2] = 26;
			}
		}
	}
}



void CU::DeriveCandidatesForIntraPredictionPARTNxN(int* candModeList, int part, int width)
{
	if (part == 0)
	{
		bool leftIsAvailable = isLeftCodingUnitAvailable(width);
		bool aboveIsAvailable = isAboveCodingUnitAvailable(width);
		CU* A = nullptr;
		CU* B = nullptr;

		if (leftIsAvailable)
			A = getLeftCodingUnit();
		if (aboveIsAvailable)
			B = getAboveCodingUnit();

		int candModeA;
		int candModeB;

		if (aboveIsAvailable)
		{
			if (B->CuPredMode != MODE_INTRA || B->PcmFlag == true || BlockPartition::isCornerUpperInCTU(this))
			{
				candModeB = 1;
			}
			else
			{
				if (B->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeB = B->IntraPredModeY[2];
				}
				else
					candModeB = B->IntraPredModeY[0];
			}


		}
		else
		{
			candModeB = 1;
		}

		if (leftIsAvailable)
		{
			if (A->CuPredMode != MODE_INTRA || A->PcmFlag == true)
			{
				candModeA = 1;
			}
			else
			{
				if (A->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeA = A->IntraPredModeY[1];
				}
				else
					candModeA = A->IntraPredModeY[0];
			}
		}
		else
		{
			candModeA = 1;
		}


		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}

	}
	else if (part == 1)
	{
		bool leftIsAvailable = true;
		bool aboveIsAvailable = isAboveCodingUnitAvailable(width);
		CU* B = nullptr;

		if (aboveIsAvailable)
			B = getAboveCodingUnit();

		int candModeA;
		int candModeB;

		if (aboveIsAvailable)
		{
			if (B->CuPredMode != MODE_INTRA || B->PcmFlag == true || BlockPartition::isCornerUpperInCTU(this))
			{
				candModeB = 1;
			}
			else
			{
				if (B->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeB = B->IntraPredModeY[3];
				}
				else
					candModeB = B->IntraPredModeY[0];
			}


		}
		else
		{
			candModeB = 1;
		}

		candModeA = IntraPredModeY[0];

		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}
	}
	else if (part == 2)
	{
		bool leftIsAvailable = isLeftCodingUnitAvailable(width);
		bool aboveIsAvailable = true;
		CU* A = nullptr;

		if (leftIsAvailable)
			A = getLeftCodingUnit();


		int candModeA;
		int candModeB;


		candModeB = IntraPredModeY[0];


		if (leftIsAvailable)
		{
			if (A->CuPredMode != MODE_INTRA || A->PcmFlag == true)
			{
				candModeA = 1;
			}
			else
			{
				if (A->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeA = A->IntraPredModeY[3];
				}
				else
					candModeA = A->IntraPredModeY[0];
			}
		}
		else
		{
			candModeA = 1;
		}


		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}
	}
	else
	{
		int candModeA;
		int candModeB;

		candModeB = IntraPredModeY[1];

		candModeA = IntraPredModeY[2];

		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}
	}



}
void CU::SortCandModeList(int* candModeList)
{
	int tmp;
	if (candModeList[0] > candModeList[1])
	{
		tmp = candModeList[1];
		candModeList[1] = candModeList[0];
		candModeList[0] = tmp;
	}

	if (candModeList[0] > candModeList[2])
	{
		tmp = candModeList[2];
		candModeList[2] = candModeList[0];
		candModeList[0] = tmp;
	}

	if (candModeList[1] > candModeList[2])
	{
		tmp = candModeList[2];
		candModeList[2] = candModeList[1];
		candModeList[1] = tmp;
	}
}

void CU::getIntraPredictionModeChroma()
{
	if (IntraChromaPredMode == 0)
	{
		if (IntraPredModeY[0] == 0)
			IntraPredModeChroma = 34;
		else
			IntraPredModeChroma = 0;
	}
	else if (IntraChromaPredMode == 1)
	{
		if (IntraPredModeY[0] == 26)
			IntraPredModeChroma = 34;
		else
			IntraPredModeChroma = 26;
	}
	else if (IntraChromaPredMode == 2)
	{
		if (IntraPredModeY[0] == 10)
			IntraPredModeChroma = 34;
		else
			IntraPredModeChroma = 10;
	}
	else if (IntraChromaPredMode == 3)
	{
		if (IntraPredModeY[0] == 1)
			IntraPredModeChroma = 34;
		else
			IntraPredModeChroma = 1;
	}
	else
		IntraPredModeChroma = IntraPredModeY[0];
}


void CU::MapPartModeToPU(float widthCoeff, float heightCoeff)
{

	for (int i = 0; i < numOfPUs; i++)
	{
		//remap transcoded CUs to children
		int projectedOriginalCTUPixelColumn = (int)((predictionUnits[i].pbY.startingIndexInFrame % frameWidth) * widthCoeff);
		int projectedOriginalCTUPixelRow = (int)((predictionUnits[i].pbY.startingIndexInFrame / frameWidth) * heightCoeff);
		int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(predictionUnits[i].pbY.width * widthCoeff);
		int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(predictionUnits[i].pbY.height * heightCoeff);

		for (int j = 0; j < transMappedCUs.size(); j++)
		{
			for (int k = 0; k < transMappedCUs[j].second->numOfPUs; k++)
			{
				MapPUtoOriginalPU(&predictionUnits[i], &transMappedCUs.at(j).second->predictionUnits[k], projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd, transMappedCUs[0].second->frameWidth);
			}

		}

		//When mapping is done categorize children CUs
		//Categorization::categorize(transcodedCU->children[i], enCtx);

		predictionUnits[i].sort();
	}


}



void CU::MapToChildren(CU* transcodedCU, float widthCoeff, float heightCoeff)
{
	//although at this point CU should have children
	if (transcodedCU->hasChildren)
	{
		//remap transcoded CUs to children
		for (int i = 0; i < 4; i++)
		{
			int projectedOriginalCTUPixelColumn = (int)((transcodedCU->children[i]->cbY.startingIndexInFrame % frameWidth) * widthCoeff);
			int projectedOriginalCTUPixelRow = (int)((transcodedCU->children[i]->cbY.startingIndexInFrame / frameWidth) * heightCoeff);
			int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(transcodedCU->children[i]->cbY.blockSize * widthCoeff);
			int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(transcodedCU->children[i]->cbY.blockSize * heightCoeff);

			for (int j = 0; j < transcodedCU->transMappedCUs.size(); j++)
			{
				MapOriginalCUToTranscodedCU(transcodedCU->children[i], transcodedCU->transMappedCUs.at(j).second, projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
			}

			//When mapping is done categorize children CUs
			//Categorization::categorize(transcodedCU->children[i], enCtx);
		}

		for (int i = 0; i < 4; ++i)
			MapToChildren(transcodedCU->children[i], widthCoeff, heightCoeff);
	}
}

//check if original CTU is between the startIndexInFrame and endIndexInFrame and assign it to transcoded CU
//also calculate weight of mapped CU 
void CU::MapOriginalCUToTranscodedCU(CU* transcodedCU, CU* originalCU, int columnStart, int columnEnd, int rowStart, int rowEnd)
{
	//At this point originalCU should be in frame, but this is just in case
	if (originalCU->isInFrame)
	{
		int originalCUPixelColumn = originalCU->cbY.startingIndexInFrame % originalCU->frameWidth;
		int originalCuPixelRow = originalCU->cbY.startingIndexInFrame / originalCU->frameWidth;

		//In this case entire CU from original frame is consisted in transcoded frame
		if (originalCUPixelColumn >= columnStart && originalCuPixelRow >= rowStart &&
			originalCUPixelColumn + originalCU->cbY.blockSize <= columnEnd && originalCuPixelRow + originalCU->cbY.blockSize <= rowEnd)
		{
			//Put all children to CU 
			mapAllAsWhole(transcodedCU, originalCU);
		}
		//Four cases where CU is totaly outside of transcoded CU
		else if (!((originalCUPixelColumn + originalCU->cbY.blockSize <= columnStart) || (originalCUPixelColumn >= columnEnd)
			|| (originalCuPixelRow + originalCU->cbY.blockSize <= rowStart) || (originalCuPixelRow >= rowEnd)))
		{
			if (originalCU->hasChildren)
			{
				for (int i = 0; i < 4; i++)
				{
					if (originalCU->children[i]->isInFrame)
						MapOriginalCUToTranscodedCU(transcodedCU, originalCU->children[i], columnStart, columnEnd, rowStart, rowEnd);
				}
			}
			else
			{
				float coeff = 0.0;
				float widthCuCoeff = 0.0;
				float heightCuCoeff = 0.0;

				if (originalCUPixelColumn >= columnStart && columnEnd - originalCUPixelColumn >= originalCU->cbY.blockSize)
					widthCuCoeff = 1;
				else if (originalCUPixelColumn < columnStart && columnEnd - originalCUPixelColumn >= originalCU->cbY.blockSize)
					widthCuCoeff = (float)(originalCUPixelColumn + originalCU->cbY.blockSize - columnStart) / originalCU->cbY.blockSize;
				else if (columnStart > originalCUPixelColumn && columnEnd < (originalCUPixelColumn + originalCU->cbY.blockSize))
					widthCuCoeff = (float)(columnEnd - columnStart) / originalCU->cbY.blockSize;
				else
					widthCuCoeff = (float)(columnEnd - originalCUPixelColumn) / originalCU->cbY.blockSize;


				if (originalCuPixelRow >= rowStart && rowEnd - originalCuPixelRow >= originalCU->cbY.blockSize)
					heightCuCoeff = 1;
				else if (originalCuPixelRow < rowStart && rowEnd - originalCuPixelRow >= originalCU->cbY.blockSize)
					heightCuCoeff = (float)(originalCuPixelRow + originalCU->cbY.blockSize - rowStart) / originalCU->cbY.blockSize;
				else if (rowStart > originalCuPixelRow && rowEnd < (originalCuPixelRow + originalCU->cbY.blockSize))
					heightCuCoeff = (float)(rowEnd - rowStart) / originalCU->cbY.blockSize;
				else
					heightCuCoeff = (float)(rowEnd - originalCuPixelRow) / originalCU->cbY.blockSize;



				double coeffwidth = (double)originalCU->frameWidth / frameWidth;
				RatioCU rc;
				rc.PercentageOfWhole = widthCuCoeff * heightCuCoeff;
				rc.RatioInCU = (rc.PercentageOfWhole * originalCU->cbY.blockSize * originalCU->cbY.blockSize) / (transcodedCU->cbY.blockSize * transcodedCU->cbY.blockSize * coeffwidth * coeffwidth);

				transcodedCU->transMappedCUs.emplace_back(make_pair(rc, originalCU));
			}
		}
	}
}

void CU::MapPUtoOriginalPU(PU* predictionUnit, PU* originalPU, int columnStart, int columnEnd, int rowStart, int rowEnd, int originalFrameWidth)
{
	int originalCUPixelColumn = originalPU->pbY.startingIndexInFrame % originalFrameWidth;
	int originalCuPixelRow = originalPU->pbY.startingIndexInFrame / originalFrameWidth;

	//In this case entire CU from original frame is consisted in transcoded frame
	if (originalCUPixelColumn >= columnStart && originalCuPixelRow >= rowStart &&
		originalCUPixelColumn + originalPU->pbY.width <= columnEnd && originalCuPixelRow + originalPU->pbY.height <= rowEnd)
	{
		double coeffwidth = (double)originalFrameWidth / frameWidth;
		RatioCU rc;
		rc.RatioInCU = (originalPU->pbY.width * originalPU->pbY.height) / (predictionUnit->pbY.width * predictionUnit->pbY.height * coeffwidth * coeffwidth);
		rc.PercentageOfWhole = 1.0;

		predictionUnit->transMappedPUs.emplace_back(std::make_pair(rc, originalPU));
	}
	//Four cases where CU is totaly outside of transcoded CU
	else if (!((originalCUPixelColumn + originalPU->pbY.width <= columnStart) || (originalCUPixelColumn >= columnEnd)
		|| (originalCuPixelRow + originalPU->pbY.height <= rowStart) || (originalCuPixelRow >= rowEnd)))
	{

		float coeff = 0.0;
		float widthCuCoeff = 0.0;
		float heightCuCoeff = 0.0;

		if (originalCUPixelColumn >= columnStart && columnEnd - originalCUPixelColumn >= originalPU->pbY.width)
			widthCuCoeff = 1;
		else if (originalCUPixelColumn < columnStart && columnEnd - originalCUPixelColumn >= originalPU->pbY.width)
			widthCuCoeff = (float)(originalCUPixelColumn + originalPU->pbY.width - columnStart) / originalPU->pbY.width;
		else if (columnStart > originalCUPixelColumn && columnEnd < (originalCUPixelColumn + originalPU->pbY.width))
			widthCuCoeff = (float)(columnEnd - columnStart) / originalPU->pbY.width;
		else
			widthCuCoeff = (float)(columnEnd - originalCUPixelColumn) / originalPU->pbY.width;


		if (originalCuPixelRow >= rowStart && rowEnd - originalCuPixelRow >= originalPU->pbY.height)
			heightCuCoeff = 1;
		else if (originalCuPixelRow < rowStart && rowEnd - originalCuPixelRow >= originalPU->pbY.height)
			heightCuCoeff = (float)(originalCuPixelRow + originalPU->pbY.height - rowStart) / originalPU->pbY.height;
		else if (rowStart > originalCuPixelRow && rowEnd < (originalCuPixelRow + originalPU->pbY.height))
			heightCuCoeff = (float)(rowEnd - rowStart) / originalPU->pbY.height;
		else
			heightCuCoeff = (float)(rowEnd - originalCuPixelRow) / originalPU->pbY.height;



		double coeffwidth = (double)originalFrameWidth / frameWidth;
		RatioCU rc;
		rc.PercentageOfWhole = widthCuCoeff * heightCuCoeff;
		rc.RatioInCU = (rc.PercentageOfWhole * originalPU->pbY.width * originalPU->pbY.height) / (predictionUnit->pbY.width * predictionUnit->pbY.height * coeffwidth * coeffwidth);

		predictionUnit->transMappedPUs.emplace_back(make_pair(rc, originalPU));

	}
}

void CU::MapIntraToOriginalIntra(float widthCoeff, float heightCoeff)
{
	int columnOffset;
	int rowOffset;
	for (int i = 0; i < 4; i++)
	{
		columnOffset = i % 2 * cbY.blockSize / 2;
		rowOffset = i / 2 * cbY.blockSize / 2;



		//remap transcoded CUs to children
		int projectedOriginalCTUPixelColumn = (int)(((cbY.startingIndexInFrame % frameWidth) + columnOffset) * widthCoeff);
		int projectedOriginalCTUPixelRow = (int)(((predictionUnits[i].pbY.startingIndexInFrame / frameWidth) + rowOffset) * heightCoeff);
		int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(cbY.blockSize / 2 * widthCoeff);
		int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(cbY.blockSize / 2 * heightCoeff);

		for (int j = 0; j < transMappedCUs.size(); j++)
		{
			if (transMappedCUs[j].second->CuPredMode == PredMode::MODE_INTRA)
			{
				if (transMappedCUs[j].second->PartMode == INTRA_PART_NxN)
				{
					for (int k = 0; k < 4; k++)
					{
						//sad tu treba za svaki tu, raspodjelu napravit.
					}

				}
			}
			for (int k = 0; k < transMappedCUs[j].second->numOfPUs; k++)
			{
				MapPUtoOriginalPU(&predictionUnits[i], &transMappedCUs.at(j).second->predictionUnits[k], projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd, transMappedCUs[0].second->frameWidth);
			}

		}

		//When mapping is done categorize children CUs
		//Categorization::categorize(transcodedCU->children[i], enCtx);

	}
}

//This function is used for single Mapping, where calculating projected Row and Column is done once
//In case where for one CU we want to find all mapped decoded CUs it is faster to use MapOriginalCUToTranscodedCU(CU*,CU*,int,int,int,int) to avoid calculating projected values every time
void CU::MapOriginalCUToTranscodedCU(CU* transcodedCU, CU* originalCU)
{
	float widthCoeff = (float)originalCU->frameWidth / transcodedCU->frameWidth;
	float heightCoeff = (float)originalCU->frameHeight / transcodedCU->frameHeight;

	int projectedOriginalCTUPixelColumn = (int)((transcodedCU->cbY.startingIndexInFrame % frameWidth) * widthCoeff);
	int projectedOriginalCTUPixelRow = (int)((transcodedCU->cbY.startingIndexInFrame / frameWidth) * heightCoeff);
	int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + (int)(transcodedCU->ctuParent->cbY.blockSize * widthCoeff);
	int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + (int)(transcodedCU->ctuParent->cbY.blockSize * heightCoeff);

	MapOriginalCUToTranscodedCU(transcodedCU, originalCU, projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
}

void CU::mapAllAsWhole(CU* transcodedCU, CU* originalCU)
{
	if (originalCU->hasChildren)
	{
		for (int i = 0; i < 4; i++)
			mapAllAsWhole(transcodedCU, originalCU->children[i]);
	}
	else
	{
		if (originalCU->isInFrame)
		{
			double coeffwidth = (double)originalCU->frameWidth / frameWidth;
			RatioCU rc;
			rc.RatioInCU = (originalCU->cbY.blockSize * originalCU->cbY.blockSize) / (transcodedCU->cbY.blockSize * transcodedCU->cbY.blockSize * coeffwidth * coeffwidth);
			rc.PercentageOfWhole = 1.0;
			transcodedCU->transMappedCUs.emplace_back(std::make_pair(rc, originalCU));
		}

	}
}

//bool CU::isNeighbourAvailable(Neighbour n)
//{
//	if (n == Left)
//	{
//		if (isLeftCached)
//			return isLeftAvailable;
//		else
//		{
//			BlockPartition::getLeftCodingUnit()
//		}
//	}
//	else if (n == Above)
//	{
//
//	}
//}

#pragma region Old methods 

//unsigned char * CU::GetRightPixelsBlockDomain(unsigned char* reconstructed_data, int numberOfPixels, int offset_index)
//{
//	unsigned char* result = new unsigned char[numberOfPixels];
//	BlockPartition bp;
//
//	if (!this->HasChildren())
//	{
//		unsigned char * payload = bp.get1dReconstructedBlockByStartingIndex(reconstructed_data, this->cbY.reconstructedStartingIndex, this->cbY.blockSize);
//		return bp.extractPixelsOnRight(payload, this->cbY.blockSize, numberOfPixels, offset_index);
//	}
//	else
//	{
//		unsigned char* resultFirst;
//		unsigned char* resultSecond;
//		int numberOfPixelsFirst;
//		int numberOfPixelsSecond;
//		int offsetIndexFirst;
//		int offsetIndexSecond;
//
//		int numberOfPartsPerBlock = (this->cbY.blockSize / numberOfPixels) / 2;
//
//		if (numberOfPartsPerBlock == 0)
//		{
//			numberOfPixelsFirst = numberOfPixels / 2;
//			numberOfPixelsSecond = numberOfPixels / 2;
//			offsetIndexFirst = 0;
//			offsetIndexSecond = 0;
//		}
//		else
//		{
//			if (offset_index < numberOfPartsPerBlock)
//			{
//				if (numberOfPixels > this->cbY.blockSize / 2)
//				{
//					numberOfPixelsFirst = numberOfPixels / 2;
//					numberOfPixelsSecond = numberOfPixels / 2;
//					offsetIndexFirst = offset_index;
//					offsetIndexSecond = 0;
//				}
//				else
//				{
//					numberOfPixelsFirst = numberOfPixels;
//					numberOfPixelsSecond = 0;
//					offsetIndexFirst = offset_index;
//					offsetIndexSecond = 0;
//				}
//			}
//			else
//			{
//				numberOfPixelsFirst = 0;
//				numberOfPixelsSecond = numberOfPixels;
//				offsetIndexFirst = 0;
//				offsetIndexSecond = offset_index%numberOfPartsPerBlock;
//			}
//		}
//
//
//		CU *cu1 = (CU*)this->children[1];
//		CU *cu3 = (CU*)this->children[3];
//
//		resultFirst = cu1->GetRightPixelsBlockDomain(reconstructed_data, numberOfPixelsFirst, offsetIndexFirst);
//		resultSecond = cu3->GetRightPixelsBlockDomain(reconstructed_data, numberOfPixelsSecond, offsetIndexSecond);
//
//		int i = 0, f = 0, s = 0;
//		for (i = 0; i < numberOfPixels; i++)
//		{
//			if (i < numberOfPixelsFirst)
//			{
//				result[i] = resultFirst[f];
//				f++;
//			}
//			else
//			{
//				result[i] = resultSecond[s];
//				s++;
//			}
//		}
//	}
//
//	return result;
//}
//
//unsigned char * CU::GetBottomPixelsBlockDomain(unsigned char* reconstructed_data, int numberOfPixels, int offset_index)
//{
//	unsigned char* result = new unsigned char[numberOfPixels];
//	BlockPartition bp;
//
//	if (!this->HasChildren())
//	{
//		unsigned char * payload = bp.get1dReconstructedBlockByStartingIndex(reconstructed_data, this->cbY.reconstructedStartingIndex, this->cbY.blockSize);
//		return bp.extractPixelsOnBottom(payload, this->cbY.blockSize, numberOfPixels, offset_index);
//	}
//	else
//	{
//		unsigned char* resultFirst;
//		unsigned char* resultSecond;
//		int numberOfPixelsFirst;
//		int numberOfPixelsSecond;
//		int offsetIndexFirst;
//		int offsetIndexSecond;
//
//		int numberOfPartsPerBlock = (this->cbY.blockSize / numberOfPixels) / 2;
//
//		if (numberOfPartsPerBlock == 0)
//		{
//			numberOfPixelsFirst = numberOfPixels / 2;
//			numberOfPixelsSecond = numberOfPixels / 2;
//			offsetIndexFirst = 0;
//			offsetIndexSecond = 0;
//		}
//		else
//		{
//			if (offset_index < numberOfPartsPerBlock)
//			{
//				if (numberOfPixels > this->cbY.blockSize / 2)
//				{
//					numberOfPixelsFirst = numberOfPixels / 2;
//					numberOfPixelsSecond = numberOfPixels / 2;
//					offsetIndexFirst = offset_index;
//					offsetIndexSecond = 0;
//				}
//				else
//				{
//					numberOfPixelsFirst = numberOfPixels;
//					numberOfPixelsSecond = 0;
//					offsetIndexFirst = offset_index;
//					offsetIndexSecond = 0;
//				}
//			}
//			else
//			{
//				numberOfPixelsFirst = 0;
//				numberOfPixelsSecond = numberOfPixels;
//				offsetIndexFirst = 0;
//				offsetIndexSecond = offset_index%numberOfPartsPerBlock;
//			}
//		}
//
//
//		CU *cu2 = (CU*)this->children[2];
//		CU *cu3 = (CU*)this->children[3];
//
//		resultFirst = cu2->GetBottomPixelsBlockDomain(reconstructed_data, numberOfPixelsFirst, offsetIndexFirst);
//		resultSecond = cu3->GetBottomPixelsBlockDomain(reconstructed_data, numberOfPixelsSecond, offsetIndexSecond);
//
//		int i = 0, f = 0, s = 0;
//		for (i = 0; i < numberOfPixels; i++)
//		{
//			if (i < numberOfPixelsFirst)
//			{
//				result[i] = resultFirst[f];
//				f++;
//			}
//			else
//			{
//				result[i] = resultSecond[s];
//				s++;
//			}
//		}
//	}
//
//	return result;
//}

#pragma endregion