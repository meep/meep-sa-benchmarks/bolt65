/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "EntropyBuffer.h"



EntropyBuffer::EntropyBuffer()
{
	
}


EntropyBuffer::~EntropyBuffer()
{
}

void EntropyBuffer::Init()
{
	byteCounter = 0;
	currentByte = 0;
	bitCounter = 0;
	emu_buff_empty = 2;

	Param_Number_of_bits = 0;
}

/*-----------------------------------------------------------------------------
Function name:  writeBins
Description:	write bin string to buffer.
Parameters:     b		- bin string
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
------------------------------------------ -----------------------------------*/
void EntropyBuffer::writeBins(Bins b)
{
	long mask = 1;
	for (int i = b.length - 1; i >= 0; i--)
	{
		mask = 1;
		mask <<= i;

		if ((b.bits & mask) != 0)
			writeBits(true, 1);
		else
			writeBits(false, 1);

	}
}
/*-----------------------------------------------------------------------------
Function name:  writeBits
Description:	write bit value to output n times. Output i.e. bitstream is implemented as buffer where
bit(s) is(are) written to position in byte, determined by index.
Parameters:     bitValue	- value which is sent to output
n			- number of times that bitValue is sent to bitstream
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
Remarks:		MC_todo To double check if shift direction and counter handling follow buffer's bit and byte order. I am not sure if it does.
Clarify with Alen if this method was successfuly tested.
------------------------------------------ -----------------------------------*/
void EntropyBuffer::writeBits(bool bitValue, unsigned int n)
{
	for (unsigned int i = 0; i < n; i++)
	{
		Param_Number_of_bits++;
		currentByte <<= 1;

		if (bitValue == true)					// If bitValue is true then make OR assignment
		{
			currentByte += 1;
		}

		bitCounter += 1;						// Increment bitCounter to point next bit in currentByte

		if (bitCounter == 8)					// If bitCounter is 8 then is end of byte
		{
			bitCounter = 0;						// Reset bitCounter

			if (emu_buff_empty > 0)
			{
				emulation_buffer[emu_buff_empty] = currentByte;
				emu_buff_empty--;
			}
			else
			{
				emulation_buffer[0] = currentByte;

				if ((emulation_buffer[2] == 0x00) && (emulation_buffer[1] == 0x00) &&
					((emulation_buffer[0] == 0x00) ||
					(emulation_buffer[0] == 0x01) ||
						(emulation_buffer[0] == 0x02) ||
						(emulation_buffer[0] == 0x03)))
				{
					putIntoBuffer(emulation_buffer[2]);
					putIntoBuffer(emulation_buffer[1]);
					putIntoBuffer(0x03);
					Param_Number_of_bits += 8;

					if (emulation_buffer[0] == 0x00)
					{
						emulation_buffer[2] = emulation_buffer[0];
						emu_buff_empty = 1;
					}
					else
					{
						putIntoBuffer(emulation_buffer[0]);
						emu_buff_empty = 2;
					}

				}
				else
				{
					putIntoBuffer(emulation_buffer[2]);	// Place byte in buffer
					emulation_buffer[2] = emulation_buffer[1];
					emulation_buffer[1] = emulation_buffer[0];
				}

				currentByte = 0;					// Reset currentByte
			}

		}

	}
}


/*-----------------------------------------------------------------------------
Function name:  writeBytes
Description:	write byte value to output n times. Output i.e. bitstream is implemented as buffer where
byte(s) is(are) written to position in buffer, determined by index.
Parameters:     byteValue	- byte which is sent to output
n			- number of times that byteValue is sent to bitstream
Returns:		--
Time (Cycles):	typ= xx cycl max = xx cycl
Remarks:		MC_todo To double check if shift direction and counter handling follow buffer's bit and byte order. I am not sure if it does.
Clarify with Alen if this method was successfuly tested.
------------------------------------------ -----------------------------------*/
void EntropyBuffer::writeBytes(unsigned char byteValue, unsigned int n)

{
	unsigned char mask;								// Mask to index position in buffer's byte
	unsigned char mask2;							// Mask to index position in byte to be written
	for (unsigned int j = 0; j < n; j++)
	{
		for (int i = 7; i >= 0; i--)
		{
			mask = 1;								// Making OR mask
			mask <<= bitCounter;
			mask2 = 1;
			mask2 <<= i;

			if ((byteValue & mask2) == mask2)		// If bitValue is true then make OR assignment
			{
				currentByte |= mask;
			}

			bitCounter++;							// Increment bitCounter to point next bit in currentByte

			if (bitCounter == 8)					// If bitCounter is 8 then is end of byte
			{
				bitCounter = 0;						// Reset bitCounter
				putIntoBuffer(currentByte);// Place byte in buffer
				currentByte = 0;					// Reset currentByte
			}

		}
	}
}

void EntropyBuffer::byteAlign(bool val)
{
	// Align the last byte with defined value

	if (bitCounter == 0)				// Check if bitstream is already byte aligned
		return;

	writeBits(val, 8 - bitCounter);		// Alignment
}

void EntropyBuffer::flushBuffer()
{
	for (int i = 2; i > emu_buff_empty; i--)
	{
		putIntoBuffer(emulation_buffer[i]);
	}

	emu_buff_empty = 2;

	/*ioKernel.writeToFile(ioKernel.byteCounter); // Write to file		
	ioKernel.byteCounter = 0;					// Reset buffer pointer */
}

void EntropyBuffer::putIntoBuffer(unsigned char byte)
{
	output_buffer.push_back(byte);
	++byteCounter;					// Update byteCounter

	/*if (byteCounter == outputBufferSize)		// Check if byteCounter is pointing to the end of buffer
	{
		byteCounter = 0;				// Reset byteCounter and flush buffer to selected file
		writeToFile(outputBufferSize);
	}*/
}
