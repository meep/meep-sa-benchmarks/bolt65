/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CB.h"

CB::CB(int starting_index, int block_size)
{
	startingIndex = starting_index;
	blockSize = block_size;
}

CB::CB(int starting_index, int block_size, int qp)
{
	startingIndex = starting_index;
	blockSize = block_size;
	cbQP = qp;
}

CB::CB(int starting_index, int block_size, int lumaQp, ColorIdx colorIdx)
{
	startingIndex = starting_index;
	blockSize = block_size;

	if (colorIdx == Luma)
	{
		cbQP = lumaQp;
	}
	else
	{
		cbQP = QPc[lumaQp];
	}
}

CB::CB(int starting_index, int starting_index_in_frame, int block_size, int lumaQp, ColorIdx colorIdx)
{
	startingIndex = starting_index;
	startingIndexInFrame = starting_index_in_frame;
	blockSize = block_size;

	if (colorIdx == Luma)
	{
		cbQP = lumaQp;
	}
	else
	{
		cbQP = QPc[lumaQp];
	}
}

CB::CB()
{
}


CB::~CB()
{
}
