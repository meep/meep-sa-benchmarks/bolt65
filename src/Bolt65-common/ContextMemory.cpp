/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ContextMemory.h"



ContextMemory::ContextMemory()
{
	StatCoeff[0] = 0;
	StatCoeff[1] = 0;
	StatCoeff[2] = 0;
	StatCoeff[3] = 0;


	cLastAbsLevel = 0;
	cLastRiceParam = 0;

	log2_max_pic_order_cnt_lsb_minus4 = 0;
	pic_width_in_luma_samples = 0;
	pic_height_in_luma_samples = 0;
	log2_diff_max_min_luma_coding_block_size = 0;
	log2_min_luma_coding_block_size_minus3 = 0;
	num_short_term_ref_pic_sets = 0;
	num_long_term_ref_pics_sps = 0;
	offset_len_minus1 = 0;
	pcm_sample_bit_depth_luma_minus1 = 0;
	pcm_sample_bit_depth_chroma_minus1 = 0;
	pred_mode_flag = 1;
	amp_enabled_flag = 0;
	num_ref_idx_l0_active_minus1 = 0;
	num_ref_idx_l1_active_minus1 = 0;
	chroma_qp_offset_list_len_minus1 = 0;
	last_sig_coeff_x_prefix = 0;
	last_sig_coeff_y_prefix = 0;
	coeff_abs_level_greater1_flag = 0;
	coeff_abs_level_greater2_flag = 0;
	persistent_rice_adaptation_enabled_flag = 0;
	transform_skip_flag = 0;
	cu_transquant_bypass_flag = 0;
	extended_precision_processing_flag = 0;
	cbf_luma = 0;
	cbf_cb = 0;
	cbf_cr = 0;
	parent_cbf_luma = 0;
	parent_cbf_cb = 0;
	parent_cbf_cr = 0;
	five_minus_max_num_merge_cand = 0;
	merge_flag = 0;
	left_cu_skip_flag = 0;
	above_cu_skip_flag = 0;
	cu_skip_flag = 0;
	rqt_root_cbf = 0;

	CuPredMode = 0;

	cbSize = 0;
	nPbW = 0;
	nPbH = 0;
	log2TrafoSize = 0;
	trafoDepth = 0;
	first_time_invoke = false;

	availableL = 0;
	availableA = 0;
	ctDepthL = 0;
	ctDepthA = 0;
	cqtDepth = 0;

	colorIdx = 0;

	subBlockCoordinateX = 0;
	subBlockCoordinateY = 0;

	isBorderSubBlock = false;
	subBlockRightCSBF = 0;
	subBlockBottomCSBF = 0;

	transformBlock_xC = 0;
	transformBlock_yC = 0;

	sub_block_index = 0;
	inside_sub_block_index = 0;
	previous_ALG1_flag = 0;
	first_time_in_TB = 1;
	maxScanIdx = 64;

	sliceType = 2;
}


void ContextMemory::CopySliceType(int _sliceType)
{
	sliceType = _sliceType;
}


ContextMemory::~ContextMemory()
{
}
