
/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/
#include "IntrapredictionKernelsCreator.h"

IntrapredictionKernelsCreator::IntrapredictionKernelsCreator()
{
}

IntrapredictionKernelsCreator::~IntrapredictionKernelsCreator()
{
}

IIntrapredictionKernels* IntrapredictionKernelsCreator::Create(EncodingContext* enCtx) 
{
#ifdef __EPI_RISCV
	if (enCtx->EPI_RISCV)
	{
		return new IntrapredictionKernelsEPIRISCV();
	}
	else
	{
		return new IntrapredictionKernelsScalar();
	}
#endif
#ifdef __AVX2__
	if (enCtx->AVX)
	{
		return new IntrapredictionKernelsAVX();
	}
	else
	{
		return new IntrapredictionKernelsScalar();
	}
#elif defined(__ARM_NEON) || defined(__ARM_FEATURE_SVE)
	//SVE has higher priority
	if (enCtx->SVE)
	{
#ifdef __ARM_FEATURE_SVE
		return new IntrapredictionKernelsSVE();
#else
		return new IntrapredictionKernelsScalar();
#endif
	}
	else if (enCtx->NEON)
	{
#ifdef __ARM_NEON
		return new IntrapredictionKernelsNEON();
#else
		return new IntrapredictionKernelsScalar();
#endif
	}
	else
	{
		return new IntrapredictionKernelsScalar();
	}
#else
	return new IntrapredictionKernelsScalar();
#endif 
}
