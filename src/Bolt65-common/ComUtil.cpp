/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ComUtil.h"

ComUtil::ComUtil()
{
}

void ComUtil::initialize(EncodingContext* enCtx)
{
	comKernels = ComKernelsCreator::Create(enCtx);
}

void ComUtil::clean()
{
	delete comKernels;
}

ComUtil::~ComUtil()
{
}

int ComUtil::MSE(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight)
{
	double result = 0;

	for (int i = 0; i < puHeight; ++i)
	{
		for (int j = 0; j < puWidth; ++j)
		{
			result += ((int)block_1[i * puWidth + j] - (int)block_2[i * puWidth + j]) * ((int)block_1[i * puWidth + j] - (int)block_2[i * puWidth + j]);
		}
	}

	result = result / (puWidth * puHeight);

	return (int)result;
}

/*
This could be done in three for loops for generic calculation of SATD, but for speed this method of manual calculation of coeffs is chosen
Haddamard is only possible for vectors/matrices that have number of elements at power of two (i.e. 2,4,8,16).
Therefore, SATD cannot be used if AMP or SMP are used
*/
int ComUtil::SATD(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight)
{
	int i, satd = 0;
	int blockSize = puWidth * puHeight;

	int* difference = new int[blockSize] {0};
	int* transformed = new int[blockSize] {0};

	for (i = 0; i < blockSize; i++)
		difference[i] = (int)block_1[i] - (int)block_2[i];

	if (puHeight == 4)
	{
		for (i = 0; i < puWidth; i++)
			FastHaddamardColumn4(difference, transformed, puWidth, i);
	}

	if (puHeight == 8)
	{
		for (i = 0; i < puWidth; i++)
			FastHaddamardColumn8(difference, transformed, puWidth, i);
	}

	if (puHeight == 16)
	{
		for (i = 0; i < puWidth; i++)
			FastHaddamardColumn16(difference, transformed, puWidth, i);
	}

	if (puHeight == 32)
	{
		for (i = 0; i < puWidth; i++)
			FastHaddamardColumn32(difference, transformed, puWidth, i);
	}

	for (i = 0; i < blockSize; i++)
		satd += abs(transformed[i]);

	delete[] difference;
	delete[] transformed;

	return satd;
}

void ComUtil::FastHaddamardColumn4(int* block, int* transformed, int blockSize, int index)
{
	int firstStep[4];

	firstStep[0] = block[blockSize * 0 + index] + block[blockSize * 2 + index];
	firstStep[1] = block[blockSize * 1 + index] + block[blockSize * 3 + index];
	firstStep[2] = -block[blockSize * 2 + index] + block[blockSize * 0 + index];
	firstStep[3] = -block[blockSize * 3 + index] + block[blockSize * 1 + index];

	transformed[blockSize * 0 + index] = firstStep[0] + firstStep[1];
	transformed[blockSize * 1 + index] = -firstStep[1] + firstStep[0];
	transformed[blockSize * 2 + index] = firstStep[2] + firstStep[3];
	transformed[blockSize * 3 + index] = -firstStep[3] + firstStep[2];

}

void ComUtil::FastHaddamardColumn8(int* block, int* transformed, int blockSize, int index)
{
	int firstStep[8], secondStep[8];

	firstStep[0] = block[blockSize * 0 + index] + block[blockSize * 4 + index];
	firstStep[1] = block[blockSize * 1 + index] + block[blockSize * 5 + index];
	firstStep[2] = block[blockSize * 2 + index] + block[blockSize * 6 + index];
	firstStep[3] = block[blockSize * 3 + index] + block[blockSize * 7 + index];
	firstStep[4] = -block[blockSize * 4 + index] + block[blockSize * 0 + index];
	firstStep[5] = -block[blockSize * 5 + index] + block[blockSize * 1 + index];
	firstStep[6] = -block[blockSize * 6 + index] + block[blockSize * 2 + index];
	firstStep[7] = -block[blockSize * 7 + index] + block[blockSize * 3 + index];

	secondStep[0] = firstStep[0] + firstStep[2];
	secondStep[1] = firstStep[1] + firstStep[3];
	secondStep[2] = -firstStep[2] + firstStep[0];
	secondStep[3] = -firstStep[3] + firstStep[1];
	secondStep[4] = firstStep[4] + firstStep[6];
	secondStep[5] = firstStep[5] + firstStep[7];
	secondStep[6] = -firstStep[6] + firstStep[4];
	secondStep[7] = -firstStep[7] + firstStep[5];

	transformed[blockSize * 0 + index] = secondStep[0] + secondStep[1];
	transformed[blockSize * 1 + index] = -secondStep[1] + secondStep[0];
	transformed[blockSize * 2 + index] = secondStep[2] + secondStep[3];
	transformed[blockSize * 3 + index] = -secondStep[3] + secondStep[2];
	transformed[blockSize * 4 + index] = secondStep[4] + secondStep[5];
	transformed[blockSize * 5 + index] = -secondStep[5] + secondStep[4];
	transformed[blockSize * 6 + index] = secondStep[6] + secondStep[7];
	transformed[blockSize * 7 + index] = -secondStep[7] + secondStep[6];
}

void ComUtil::FastHaddamardColumn16(int* block, int* transformed, int blockSize, int index)
{
	int firstStep[16], secondStep[16], thirdStep[16];


	firstStep[0] = block[blockSize * 0 + index] + block[blockSize * 8 + index];
	firstStep[1] = block[blockSize * 1 + index] + block[blockSize * 9 + index];
	firstStep[2] = block[blockSize * 2 + index] + block[blockSize * 10 + index];
	firstStep[3] = block[blockSize * 3 + index] + block[blockSize * 11 + index];
	firstStep[4] = block[blockSize * 4 + index] + block[blockSize * 12 + index];
	firstStep[5] = block[blockSize * 5 + index] + block[blockSize * 13 + index];
	firstStep[6] = block[blockSize * 6 + index] + block[blockSize * 14 + index];
	firstStep[7] = block[blockSize * 7 + index] + block[blockSize * 15 + index];
	firstStep[8] = -block[blockSize * 8 + index] + block[blockSize * 0 + index];
	firstStep[9] = -block[blockSize * 9 + index] + block[blockSize * 1 + index];
	firstStep[10] = -block[blockSize * 10 + index] + block[blockSize * 2 + index];
	firstStep[11] = -block[blockSize * 11 + index] + block[blockSize * 3 + index];
	firstStep[12] = -block[blockSize * 12 + index] + block[blockSize * 4 + index];
	firstStep[13] = -block[blockSize * 13 + index] + block[blockSize * 5 + index];
	firstStep[14] = -block[blockSize * 14 + index] + block[blockSize * 6 + index];
	firstStep[15] = -block[blockSize * 15 + index] + block[blockSize * 7 + index];

	secondStep[0] = firstStep[0] + firstStep[4];
	secondStep[1] = firstStep[1] + firstStep[5];
	secondStep[2] = firstStep[2] + firstStep[6];
	secondStep[3] = firstStep[3] + firstStep[7];
	secondStep[4] = -firstStep[4] + firstStep[0];
	secondStep[5] = -firstStep[5] + firstStep[1];
	secondStep[6] = -firstStep[6] + firstStep[2];
	secondStep[7] = -firstStep[7] + firstStep[3];
	secondStep[8] = firstStep[8] + firstStep[12];
	secondStep[9] = firstStep[9] + firstStep[13];
	secondStep[10] = firstStep[10] + firstStep[14];
	secondStep[11] = firstStep[11] + firstStep[15];
	secondStep[12] = -firstStep[12] + firstStep[8];
	secondStep[13] = -firstStep[13] + firstStep[9];
	secondStep[14] = -firstStep[14] + firstStep[10];
	secondStep[15] = -firstStep[15] + firstStep[11];

	thirdStep[0] = secondStep[0] + secondStep[2];
	thirdStep[1] = secondStep[1] + secondStep[3];
	thirdStep[2] = -secondStep[2] + secondStep[0];
	thirdStep[3] = -secondStep[3] + secondStep[1];
	thirdStep[4] = secondStep[4] + secondStep[6];
	thirdStep[5] = secondStep[5] + secondStep[7];
	thirdStep[6] = -secondStep[6] + secondStep[4];
	thirdStep[7] = -secondStep[7] + secondStep[5];
	thirdStep[8] = secondStep[8] + secondStep[10];
	thirdStep[9] = secondStep[9] + secondStep[11];
	thirdStep[10] = -secondStep[10] + secondStep[8];
	thirdStep[11] = -secondStep[11] + secondStep[9];
	thirdStep[12] = secondStep[12] + secondStep[14];
	thirdStep[13] = secondStep[13] + secondStep[15];
	thirdStep[14] = -secondStep[14] + secondStep[12];
	thirdStep[15] = -secondStep[15] + secondStep[13];

	transformed[blockSize * 0 + index] = thirdStep[0] + thirdStep[1];
	transformed[blockSize * 1 + index] = -thirdStep[1] + thirdStep[0];
	transformed[blockSize * 2 + index] = thirdStep[2] + thirdStep[3];
	transformed[blockSize * 3 + index] = -thirdStep[3] + thirdStep[2];
	transformed[blockSize * 4 + index] = thirdStep[4] + thirdStep[5];
	transformed[blockSize * 5 + index] = -thirdStep[5] + thirdStep[4];
	transformed[blockSize * 6 + index] = thirdStep[6] + thirdStep[7];
	transformed[blockSize * 7 + index] = -thirdStep[7] + thirdStep[6];
	transformed[blockSize * 8 + index] = thirdStep[8] + thirdStep[9];
	transformed[blockSize * 9 + index] = -thirdStep[9] + thirdStep[8];
	transformed[blockSize * 10 + index] = thirdStep[10] + thirdStep[11];
	transformed[blockSize * 11 + index] = -thirdStep[11] + thirdStep[10];
	transformed[blockSize * 12 + index] = thirdStep[12] + thirdStep[13];
	transformed[blockSize * 13 + index] = -thirdStep[13] + thirdStep[12];
	transformed[blockSize * 14 + index] = thirdStep[14] + thirdStep[15];
	transformed[blockSize * 15 + index] = -thirdStep[15] + thirdStep[14];

}

void ComUtil::FastHaddamardColumn32(int* block, int* transformed, int blockSize, int index)
{
	int firstStep[32], secondStep[32], thirdStep[32], fourthStep[32];


	firstStep[0] = block[blockSize * 0 + index] + block[blockSize * 16 + index];
	firstStep[1] = block[blockSize * 1 + index] + block[blockSize * 17 + index];
	firstStep[2] = block[blockSize * 2 + index] + block[blockSize * 18 + index];
	firstStep[3] = block[blockSize * 3 + index] + block[blockSize * 19 + index];
	firstStep[4] = block[blockSize * 4 + index] + block[blockSize * 20 + index];
	firstStep[5] = block[blockSize * 5 + index] + block[blockSize * 21 + index];
	firstStep[6] = block[blockSize * 6 + index] + block[blockSize * 22 + index];
	firstStep[7] = block[blockSize * 7 + index] + block[blockSize * 23 + index];
	firstStep[8] = block[blockSize * 8 + index] + block[blockSize * 24 + index];
	firstStep[9] = block[blockSize * 9 + index] + block[blockSize * 25 + index];
	firstStep[10] = block[blockSize * 10 + index] + block[blockSize * 26 + index];
	firstStep[11] = block[blockSize * 11 + index] + block[blockSize * 27 + index];
	firstStep[12] = block[blockSize * 12 + index] + block[blockSize * 28 + index];
	firstStep[13] = block[blockSize * 13 + index] + block[blockSize * 29 + index];
	firstStep[14] = block[blockSize * 14 + index] + block[blockSize * 30 + index];
	firstStep[15] = block[blockSize * 15 + index] + block[blockSize * 31 + index];
	firstStep[16] = -block[blockSize * 16 + index] + block[blockSize * 0 + index];
	firstStep[17] = -block[blockSize * 17 + index] + block[blockSize * 1 + index];
	firstStep[18] = -block[blockSize * 18 + index] + block[blockSize * 2 + index];
	firstStep[19] = -block[blockSize * 19 + index] + block[blockSize * 3 + index];
	firstStep[20] = -block[blockSize * 20 + index] + block[blockSize * 4 + index];
	firstStep[21] = -block[blockSize * 21 + index] + block[blockSize * 5 + index];
	firstStep[22] = -block[blockSize * 22 + index] + block[blockSize * 6 + index];
	firstStep[23] = -block[blockSize * 23 + index] + block[blockSize * 7 + index];
	firstStep[24] = -block[blockSize * 24 + index] + block[blockSize * 8 + index];
	firstStep[25] = -block[blockSize * 25 + index] + block[blockSize * 9 + index];
	firstStep[26] = -block[blockSize * 26 + index] + block[blockSize * 10 + index];
	firstStep[27] = -block[blockSize * 27 + index] + block[blockSize * 11 + index];
	firstStep[28] = -block[blockSize * 28 + index] + block[blockSize * 12 + index];
	firstStep[29] = -block[blockSize * 29 + index] + block[blockSize * 13 + index];
	firstStep[30] = -block[blockSize * 30 + index] + block[blockSize * 14 + index];
	firstStep[31] = -block[blockSize * 31 + index] + block[blockSize * 15 + index];

	secondStep[0] = firstStep[0] + firstStep[8];
	secondStep[1] = firstStep[1] + firstStep[9];
	secondStep[2] = firstStep[2] + firstStep[10];
	secondStep[3] = firstStep[3] + firstStep[11];
	secondStep[4] = firstStep[4] + firstStep[12];
	secondStep[5] = firstStep[5] + firstStep[13];
	secondStep[6] = firstStep[6] + firstStep[14];
	secondStep[7] = firstStep[7] + firstStep[15];
	secondStep[8] = -firstStep[8] + firstStep[0];
	secondStep[9] = -firstStep[9] + firstStep[1];
	secondStep[10] = -firstStep[10] + firstStep[2];
	secondStep[11] = -firstStep[11] + firstStep[3];
	secondStep[12] = -firstStep[12] + firstStep[4];
	secondStep[13] = -firstStep[13] + firstStep[5];
	secondStep[14] = -firstStep[14] + firstStep[6];
	secondStep[15] = -firstStep[15] + firstStep[7];
	secondStep[16] = firstStep[16] + firstStep[24];
	secondStep[17] = firstStep[17] + firstStep[25];
	secondStep[18] = firstStep[18] + firstStep[26];
	secondStep[19] = firstStep[19] + firstStep[27];
	secondStep[20] = firstStep[20] + firstStep[28];
	secondStep[21] = firstStep[21] + firstStep[29];
	secondStep[22] = firstStep[22] + firstStep[30];
	secondStep[23] = firstStep[23] + firstStep[31];
	secondStep[24] = -firstStep[24] + firstStep[16];
	secondStep[25] = -firstStep[25] + firstStep[17];
	secondStep[26] = -firstStep[26] + firstStep[18];
	secondStep[27] = -firstStep[27] + firstStep[19];
	secondStep[28] = -firstStep[28] + firstStep[20];
	secondStep[29] = -firstStep[29] + firstStep[21];
	secondStep[30] = -firstStep[30] + firstStep[22];
	secondStep[31] = -firstStep[31] + firstStep[23];

	thirdStep[0] = secondStep[0] + secondStep[4];
	thirdStep[1] = secondStep[1] + secondStep[5];
	thirdStep[2] = secondStep[2] + secondStep[6];
	thirdStep[3] = secondStep[3] + secondStep[7];
	thirdStep[4] = -secondStep[4] + secondStep[0];
	thirdStep[5] = -secondStep[5] + secondStep[1];
	thirdStep[6] = -secondStep[6] + secondStep[2];
	thirdStep[7] = -secondStep[7] + secondStep[3];
	thirdStep[8] = secondStep[8] + secondStep[12];
	thirdStep[9] = secondStep[9] + secondStep[13];
	thirdStep[10] = secondStep[10] + secondStep[14];
	thirdStep[11] = secondStep[11] + secondStep[15];
	thirdStep[12] = -secondStep[12] + secondStep[8];
	thirdStep[13] = -secondStep[13] + secondStep[9];
	thirdStep[14] = -secondStep[14] + secondStep[10];
	thirdStep[15] = -secondStep[15] + secondStep[11];
	thirdStep[16] = secondStep[16] + secondStep[20];
	thirdStep[17] = secondStep[17] + secondStep[21];
	thirdStep[18] = secondStep[18] + secondStep[22];
	thirdStep[19] = secondStep[19] + secondStep[23];
	thirdStep[20] = -secondStep[20] + secondStep[16];
	thirdStep[21] = -secondStep[21] + secondStep[17];
	thirdStep[22] = -secondStep[22] + secondStep[18];
	thirdStep[23] = -secondStep[23] + secondStep[19];
	thirdStep[24] = secondStep[24] + secondStep[28];
	thirdStep[25] = secondStep[25] + secondStep[29];
	thirdStep[26] = secondStep[26] + secondStep[30];
	thirdStep[27] = secondStep[27] + secondStep[31];
	thirdStep[28] = -secondStep[28] + secondStep[24];
	thirdStep[29] = -secondStep[29] + secondStep[25];
	thirdStep[30] = -secondStep[30] + secondStep[26];
	thirdStep[31] = -secondStep[31] + secondStep[27];

	fourthStep[0] = thirdStep[0] + thirdStep[2];
	fourthStep[1] = thirdStep[1] + thirdStep[3];
	fourthStep[2] = -thirdStep[2] + thirdStep[0];
	fourthStep[3] = -thirdStep[3] + thirdStep[1];
	fourthStep[4] = thirdStep[4] + thirdStep[6];
	fourthStep[5] = thirdStep[5] + thirdStep[7];
	fourthStep[6] = -thirdStep[6] + thirdStep[4];
	fourthStep[7] = -thirdStep[7] + thirdStep[5];
	fourthStep[8] = thirdStep[8] + thirdStep[10];
	fourthStep[9] = thirdStep[9] + thirdStep[11];
	fourthStep[10] = -thirdStep[10] + thirdStep[8];
	fourthStep[11] = -thirdStep[11] + thirdStep[9];
	fourthStep[12] = thirdStep[12] + thirdStep[14];
	fourthStep[13] = thirdStep[13] + thirdStep[15];
	fourthStep[14] = -thirdStep[14] + thirdStep[12];
	fourthStep[15] = -thirdStep[15] + thirdStep[13];
	fourthStep[16] = thirdStep[16] + thirdStep[18];
	fourthStep[17] = thirdStep[17] + thirdStep[19];
	fourthStep[18] = -thirdStep[18] + thirdStep[16];
	fourthStep[19] = -thirdStep[19] + thirdStep[17];
	fourthStep[20] = thirdStep[20] + thirdStep[22];
	fourthStep[21] = thirdStep[21] + thirdStep[23];
	fourthStep[22] = -thirdStep[22] + thirdStep[20];
	fourthStep[23] = -thirdStep[23] + thirdStep[21];
	fourthStep[24] = thirdStep[24] + thirdStep[26];
	fourthStep[25] = thirdStep[25] + thirdStep[27];
	fourthStep[26] = -thirdStep[26] + thirdStep[24];
	fourthStep[27] = -thirdStep[27] + thirdStep[25];
	fourthStep[28] = thirdStep[28] + thirdStep[30];
	fourthStep[29] = thirdStep[29] + thirdStep[31];
	fourthStep[30] = -thirdStep[30] + thirdStep[28];
	fourthStep[31] = -thirdStep[31] + thirdStep[29];

	transformed[blockSize * 0 + index] = fourthStep[0] + fourthStep[1];
	transformed[blockSize * 1 + index] = -fourthStep[1] + fourthStep[0];
	transformed[blockSize * 2 + index] = fourthStep[2] + fourthStep[3];
	transformed[blockSize * 3 + index] = -fourthStep[3] + fourthStep[2];
	transformed[blockSize * 4 + index] = fourthStep[4] + fourthStep[5];
	transformed[blockSize * 5 + index] = -fourthStep[5] + fourthStep[4];
	transformed[blockSize * 6 + index] = fourthStep[6] + fourthStep[7];
	transformed[blockSize * 7 + index] = -fourthStep[7] + fourthStep[6];
	transformed[blockSize * 8 + index] = fourthStep[8] + fourthStep[9];
	transformed[blockSize * 9 + index] = -fourthStep[9] + fourthStep[8];
	transformed[blockSize * 10 + index] = fourthStep[10] + fourthStep[11];
	transformed[blockSize * 11 + index] = -fourthStep[11] + fourthStep[10];
	transformed[blockSize * 12 + index] = fourthStep[12] + fourthStep[13];
	transformed[blockSize * 13 + index] = -fourthStep[13] + fourthStep[12];
	transformed[blockSize * 14 + index] = fourthStep[14] + fourthStep[15];
	transformed[blockSize * 15 + index] = -fourthStep[15] + fourthStep[14];
	transformed[blockSize * 16 + index] = fourthStep[16] + fourthStep[17];
	transformed[blockSize * 17 + index] = -fourthStep[17] + fourthStep[16];
	transformed[blockSize * 18 + index] = fourthStep[18] + fourthStep[19];
	transformed[blockSize * 19 + index] = -fourthStep[19] + fourthStep[18];
	transformed[blockSize * 20 + index] = fourthStep[20] + fourthStep[21];
	transformed[blockSize * 21 + index] = -fourthStep[21] + fourthStep[20];
	transformed[blockSize * 22 + index] = fourthStep[22] + fourthStep[23];
	transformed[blockSize * 23 + index] = -fourthStep[23] + fourthStep[22];
	transformed[blockSize * 24 + index] = fourthStep[24] + fourthStep[25];
	transformed[blockSize * 25 + index] = -fourthStep[25] + fourthStep[24];
	transformed[blockSize * 26 + index] = fourthStep[26] + fourthStep[27];
	transformed[blockSize * 27 + index] = -fourthStep[27] + fourthStep[26];
	transformed[blockSize * 28 + index] = fourthStep[28] + fourthStep[29];
	transformed[blockSize * 29 + index] = -fourthStep[29] + fourthStep[28];
	transformed[blockSize * 30 + index] = fourthStep[30] + fourthStep[31];
	transformed[blockSize * 31 + index] = -fourthStep[31] + fourthStep[30];

}

int ComUtil::CalculateBlockMatchingValue(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight, EncodingContext* enCtx)
{
	if (enCtx->blockMatching == BlockMatchingCriteria::SAD)
	{
		return comKernels->SAD(block_1, block_2, puWidth, puHeight);
	}
	if (enCtx->blockMatching == BlockMatchingCriteria::SATD)
	{
		return SATD(block_1, block_2, puWidth, puHeight);
	}
	if (enCtx->blockMatching == BlockMatchingCriteria::MSE)
	{
		return MSE(block_1, block_2, puWidth, puHeight);
	}
	else
	{
		return 0;
	}
}

void ComUtil::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
		comKernels->SubResidual(rawBlock, predictedBlock, residualBlock, blockSize);
}

//This method is used when inter predicted block TUs, so that each TU has its appropriate residual
void ComUtil::SubResidualWithTransformTree(unsigned char* rawBlock, unsigned char* predictedBlock, int blockSize, TU* parentTU, int colorIdx)
{
	int columnOffset, rowOffset, childId, childIndex;
	int transformBlockSize = blockSize / 2;

	for (int i = 0; i < blockSize * blockSize; i++)
	{
		rowOffset = i / blockSize;
		columnOffset = i % blockSize;

		childId = (rowOffset / transformBlockSize) * 2 + (columnOffset / transformBlockSize);
		childIndex = (rowOffset % transformBlockSize) * transformBlockSize + (columnOffset % transformBlockSize);

		if (colorIdx == Luma)
			parentTU->children[childId]->tbY.residual[childIndex] = rawBlock[i] - predictedBlock[i];
		else if (colorIdx == ChromaCb)
			parentTU->children[childId]->tbU.residual[childIndex] = rawBlock[i] - predictedBlock[i];
		else if (colorIdx == ChromaCr)
			parentTU->children[childId]->tbV.residual[childIndex] = rawBlock[i] - predictedBlock[i];
	}
}
/*

Piljic:

version 2.0 useing Clip3 method for clipping (instead of calculating every time
Suggestion: Pass blockSize as parametes, since there is no dinamic allocation of any kind
*/
void ComUtil::AddResidual(int16_t* residualBlock, unsigned char* predictedBlock, unsigned char* reconstructedBlock, int blockSize, int resultBlockSize, int idx)
{
	int rowOffset = 0;
	int columnOffset = 0;
	int blockSizeLoop = 0;
	//in case when we combine several residuals in one big reconstructed block
	if (resultBlockSize > blockSize)
	{
		blockSizeLoop = resultBlockSize / 2;
		rowOffset = idx / 2 * blockSizeLoop;
		columnOffset = idx % 2 * blockSizeLoop;
	}
	else
	{
		blockSizeLoop = blockSize;
	}

	int i, j;
	for (i = 0; i < blockSizeLoop; i++)
	{
		for (j = 0; j < blockSizeLoop; j++)
		{
			reconstructedBlock[((i + rowOffset) * resultBlockSize) + columnOffset + j] = clip3(0, 255, residualBlock[i * blockSize + j] + predictedBlock[((i + rowOffset) * resultBlockSize) + columnOffset + j]);
		}

	}
}

/*-----------------------------------------------------------------------------
Function name:  clip3
Description:	For arguments whose value is outside range <a,b> function returns closer
boundary value from these two. When value is inside the range the value itself
is returned
Parameters:     a	- lower boundary
b	- upper boundary
x	- input value
Returns:		clipped output for x
Time (Cycles):	typ= xx cycl max = xx cycl
-----------------------------------------------------------------------------*/
int ComUtil::clip3(const int a, const int b, int x)
{
	if (x < a)
		return a;
	else if (x > b)
		return b;
	else
		return x;
}
int ComUtil::sign(const int a)
{
	if (a < 0)
		return -1;
	if (a > 0)
		return 1;

	return 0;
}

int ComUtil::powerOf8(int n)
{
	if (n == 0)
		return 1;
	else if (n == 1)
		return 8;
	else if (n == 2)
		return 64;
	else if (n == 3)
		return 512;
}
int ComUtil::logarithm2(unsigned int a)
{
	if (a == 2)
		return 1;
	else if (a == 4)
		return 2;
	else if (a == 8)
		return 3;
	else if (a == 16)
		return  4;
	else if (a == 32)
		return  5;
	else if (a == 64)
		return  6;
	else if (a == 128)
		return  7;
	else if (a == 256)
		return  8;
	else if (a == 512)
		return  9;
	else if (a == 1024)
		return  10;
	else if (a == 2048)
		return  11;
	else if (a == 4096)
		return  12;
	else if (a == 8192)
		return  13;
	else if (a == 16384)
		return  14;
	else if (a == 32768)
		return  15;
	else if (a == 65536)
		return  16;
	else if (a == 131072)
		return  17;
	else if (a == 262144)
		return  18;
	else if (a == 524288)
		return  19;
	else if (a == 1048576)
		return  20;
	else if (a == 2097152)
		return  21;
	else if (a == 4194304)
		return  22;
	else if (a == 8388608)
		return  23;
	else if (a == 16777216)
		return  24;
	else if (a == 33554432)
		return  25;
	else if (a == 67108864)
		return  26;
	else if (a == 134217728)
		return  27;
	else if (a == 268435456)
		return  28;
	else if (a == 536870912)
		return  29;
	else if (a == 1073741824)
		return  30;
	else if (a == 2147483648)
		return  31;
	else if (a == 4294967296)
		return  32;
	else
		return -1;

}


float ComUtil::calculatePSNR(unsigned char* original, unsigned char* reconstructed, int size, int bitNumber)
{
	int i;
	int sadSum = 0;
	for (i = 0; i < size; i++)
	{
		sadSum += (original[i] - reconstructed[i]) * (original[i] - reconstructed[i]); //pow(2)
	}
	float mse = (float)sadSum / size;
	int max = (1 << bitNumber) - 1;

	if (mse > 0)
		return 10 * log10((max * max) / mse);
	else
		return -1;
}
void ComUtil::calculatePSNR_YUV(unsigned char* original, unsigned char* reconstructed, int frameWidth, int frameHeight, int bitNumber, float& psnr_y, float& psnr_u, float& psnr_v)
{
	int i;
	int sadSumY = 0;
	int sadSumU = 0;
	int sadSumV = 0;

	int sizeLuma = frameHeight * frameWidth;
	int sizeChroma = sizeLuma / 4;
	int frameSize = sizeLuma + sizeChroma + sizeChroma;
	int startingU = sizeLuma;
	int startingV = sizeLuma + sizeChroma;

	for (i = 0; i < frameSize; i++)
	{
		int error = (original[i] - reconstructed[i]) * (original[i] - reconstructed[i]);

		if (i < startingU)
			sadSumY += error; //pow(2)
		else if (i >= startingU && i < startingV)
			sadSumU += error;
		else
			sadSumV += error;

	}
	float mseY = (float)sadSumY / sizeLuma;
	float mseU = (float)sadSumU / sizeChroma;
	float mseV = (float)sadSumV / sizeChroma;

	int max = (1 << bitNumber) - 1;
	int max2 = max * max;

	if (mseY > 0)
		psnr_y = 10 * log10(max2 / mseY);
	else
		psnr_y = -1;

	if (mseU > 0)
		psnr_u = 10 * log10(max2 / mseU);
	else
		psnr_u = -1;

	if (mseV > 0)
		psnr_v = 10 * log10(max2 / mseV);
	else
		psnr_v = -1;
}


float ComUtil::calculateSSIM(unsigned char* original, unsigned char* reconstructed, int width, int height, int bitNumber)
{
	//int size = width*height;
	//float sumX = 0;
	//float sumY = 0;
	//float sumX2 = 0;
	//float sumY2 = 0;
	//float sumXY = 0;

	//int L = (1 << bitNumber) - 1;

	//float c1 = (0.01 * L)*(0.01 * L);
	//float c2 = (0.03 * L)*(0.03 * L);

	//int i;
	//for (i = 0; i < size; i++)
	//{
	//	float w = 1 - weight2[(i%width) % 11][(i / width) % 11];
	//	sumX += w *original[i];
	//	sumY += w * reconstructed[i];
	//	sumX2 += w * original[i] * original[i];
	//	sumY2 += w * reconstructed[i] * reconstructed[i];
	//	sumXY += w * original[i] * reconstructed[i];
	//}

	//float avgX = (float)sumX / size;
	//float avgY = (float)sumY / size;

	////for (i = 0; i < size; i++)
	////{
	////	double w = 1 - weight2[(i%width) % 11][(i / width) % 11];
	////	sumX2 += w*(original[i] - avgX) * (original[i] - avgX);
	////	sumY2 += w*(reconstructed[i] - avgY) * (reconstructed[i] - avgY);
	////	sumXY += w*(original[i] - avgX) * (reconstructed[i] - avgY);
	////}


	//float avgX2 = (float)sumX2 / size;
	//float avgY2 = (float)sumY2 / size;
	//float avgXY = (float)sumXY / size;

	//float varianceX = avgX2 - (avgX*avgX);
	//float varianceY = avgY2 - (avgY*avgY);
	//float varianceXY = avgXY - (avgX *avgY);

	//float result = (float)(((2 * avgX*avgY) + c1)*(2 * varianceXY + c2)) / (((avgX*avgX) + (avgY*avgY) + c1)*(varianceX + varianceY + c2));

	//return result;

	return 0.0;
}
void ComUtil::calculateSSIM_YUV(unsigned char* original, unsigned char* reconstructed, int frameWidth, int frameHeight, int bitNumber, float& ssim_y, float& ssim_u, float& ssim_v)
{
	int sizeLuma = frameHeight * frameWidth;
	int sizeChroma = sizeLuma / 4;
	int frameSize = sizeLuma + sizeChroma + sizeChroma;
	int startingU = sizeLuma;
	int startingV = sizeLuma + sizeChroma;

	unsigned char* originalY = new unsigned char[sizeLuma];
	unsigned char* reconstructedY = new unsigned char[sizeLuma];
	unsigned char* originalU = new unsigned char[sizeChroma];
	unsigned char* reconstructedU = new unsigned char[sizeChroma];
	unsigned char* originalV = new unsigned char[sizeChroma];
	unsigned char* reconstructedV = new unsigned char[sizeChroma];

	int i;
	for (i = 0; i < frameSize; i++)
	{
		if (i < startingU)
		{
			originalY[i] = original[i];
			reconstructedY[i] = reconstructed[i];
		}
		else if (i >= startingU && i < startingV)
		{
			originalU[i - startingU] = original[i];
			reconstructedU[i - startingU] = reconstructed[i];
		}
		else
		{
			originalV[i - startingV] = original[i];
			reconstructedV[i - startingV] = reconstructed[i];
		}
	}

	ssim_y = calculateSSIM(originalY, reconstructedY, frameWidth, frameHeight, bitNumber);
	ssim_u = calculateSSIM(originalU, reconstructedU, frameWidth / 2, frameHeight / 2, bitNumber);
	ssim_v = calculateSSIM(originalV, reconstructedV, frameWidth / 2, frameHeight / 2, bitNumber);

	delete[] originalY;
	delete[] reconstructedY;
	delete[] originalU;
	delete[] reconstructedU;
	delete[] originalV;
	delete[] reconstructedV;


	/*int i;
	int sumYOriginal = 0;
	int sumYReconstructed = 0;
	int sumUOriginal = 0;
	int sumUReconstructed = 0;
	int sumVOriginal = 0;
	int sumVReconstructed = 0;

	long long sumYOriginalSquared = 0;
	long long sumYReconstructedSquared = 0;
	long long sumUOriginalSquared = 0;
	long long sumUReconstructedSquared = 0;
	long long sumVOriginalSquared = 0;
	long long sumVReconstructedSquared = 0;

	long long sumYOrigRec = 0;
	long long sumUOrigRec = 0;
	long long sumVOrigRec = 0;

	int sizeLuma = frameHeight*frameWidth;
	int sizeChroma = sizeLuma / 4;
	int frameSize = sizeLuma + sizeChroma + sizeChroma;
	int startingU = sizeLuma;
	int startingV = sizeLuma + sizeChroma;

	for (i = 0; i < frameSize; i++)
	{
		if (i < startingU)
		{
			sumYOriginal += original[i];
			sumYReconstructed += reconstructed[i];
			sumYOriginalSquared += original[i] * original[i];
			sumYReconstructedSquared += reconstructed[i] * reconstructed[i];
			sumYOrigRec += original[i] * reconstructed[i];
		}
		else if (i >= startingU && i < startingV)
		{
			sumUOriginal += original[i];
			sumUReconstructed += reconstructed[i];
			sumUOriginalSquared += original[i] * original[i];
			sumUReconstructedSquared += reconstructed[i] * reconstructed[i];
			sumUOrigRec += original[i] * reconstructed[i];
		}
		else
		{
			sumVOriginal += original[i];
			sumVReconstructed += reconstructed[i];
			sumVOriginalSquared += original[i] * original[i];
			sumVReconstructedSquared += reconstructed[i] * reconstructed[i];
			sumUOrigRec += original[i] * reconstructed[i];
		}
	}

	float avgYOriginal = sumYOriginal / sizeLuma;
	float avgYReconstructed = sumYReconstructed / sizeLuma;
	float avgUOriginal = sumUOriginal / sizeChroma;
	float avgUReconstructed = sumUReconstructed / sizeChroma;
	float avgVOriginal = sumVOriginal / sizeChroma;
	float avgVReconstructed = sumVReconstructed / sizeChroma;

	float avgYOriginalSquared = sumYOriginalSquared / sizeLuma;
	float avgYReconstructedSquared = sumYReconstructedSquared / sizeLuma;
	float avgUOriginalSquared = sumUOriginalSquared / sizeChroma;
	float avgUReconstructedSquared = sumUReconstructedSquared / sizeChroma;
	float avgVOriginalSquared = sumVOriginalSquared / sizeChroma;
	float avgVReconstructedSquared = sumVReconstructedSquared / sizeChroma;

	float avgYOrigRec = sumYOrigRec / sizeLuma;
	float avgUOrigRec = sumUOrigRec / sizeChroma;
	float avgVOrigRec = sumVOrigRec / sizeChroma;

	float varianceYOriginal = avgYOriginalSquared - (avgYOriginal*avgYOriginal);
	float varianceYReconstructed = avgYReconstructedSquared - (avgYReconstructed*avgYReconstructed);
	float varianceUOriginal = avgUOriginalSquared - (avgUOriginal*avgUOriginal);
	float varianceUReconstructed = avgUReconstructedSquared - (avgUReconstructed*avgUReconstructed);
	float varianceVOriginal = avgVOriginalSquared - (avgVOriginal*avgVOriginal);
	float varianceVReconstructed = avgVReconstructedSquared - (avgVReconstructed*avgVReconstructed);

	float varianceYOrigRec = avgYOrigRec - (avgYOriginal*avgYReconstructed);
	float varianceUOrigRec = avgYOrigRec - (avgUOriginal*avgUReconstructed);
	float varianceVOrigRec = avgYOrigRec - (avgVOriginal*avgVReconstructed);

	float c1 = (0.01 * 255)*(0.01 * 255);
	float c2 = (0.03 * 255)*(0.03 * 255);

	ssim_y = ((2 * avgYOriginal*avgYReconstructed + c1)*(2 * sqrt(varianceYOrigRec) + c2)) /
		(((avgYOriginal*avgYOriginal) + (avgYReconstructed*avgYReconstructed) + c1)*(varianceYOriginal + varianceYReconstructed + c2));

	ssim_u = ((2 * avgUOriginal*avgUReconstructed + c1)*(2 * sqrt(varianceUOrigRec) + c2)) /
		(((avgUOriginal*avgYOriginal) + (avgUReconstructed*avgUReconstructed) + c1)*(varianceUOriginal + varianceUReconstructed + c2));

	ssim_v = ((2 * avgVOriginal*avgVReconstructed + c1)*(2 * sqrt(varianceVOrigRec) + c2)) /
		(((avgVOriginal*avgVOriginal) + (avgVReconstructed*avgVReconstructed) + c1)*(varianceVOriginal + varianceVReconstructed + c2));
*/

}

bool ComUtil::doesFileExist(std::string filename) {
	std::ifstream ifile(filename);
	return (bool)ifile;
}

void ComUtil::printArrayOfInt16(int16_t* array, int rowSize, int columnSize)
{
	for (int i = 0; i < rowSize; ++i)
	{
		for (int j = 0; j < columnSize; ++j)
		{
			std::cout << " ";
			std::cout << std::setw(4) << array[i * rowSize + j];
		}
		std::cout << std::endl;

	}
}

int ComUtil::getFrameIndexByPartitionIndex(int partitionWidth, int partitionHeight, int partitionStartingIndex, PartitionType partitionType,  EncodingContext* enCtx, int startingIndex)
{

	if (partitionType == FRAME)
		return startingIndex;

	int frameLumaSize = enCtx->width * enCtx->height;
	int frameChromaSize = frameLumaSize / 4;
	int tileLumaSize = partitionWidth * partitionHeight;
	int tileChromaSize = tileLumaSize / 4;

	int tileWidth = partitionStartingIndex % enCtx->width;
	int tileHeight = partitionStartingIndex / enCtx->width;

	ColorIdx color;

	if (startingIndex < tileLumaSize)
		color = Luma;
	else if (startingIndex >= tileLumaSize && startingIndex < (tileLumaSize + tileChromaSize))
		color = ChromaCb;
	else
		color = ChromaCr;

	int siPartitionWidth, siPartitionHeight;

	if (color == Luma)
	{
		siPartitionWidth = startingIndex % partitionWidth;
		siPartitionHeight = startingIndex / partitionWidth;

		return ((tileHeight + siPartitionHeight) * enCtx->width) + tileWidth + siPartitionWidth;
	}
	else if (color == ChromaCb)
	{
		siPartitionWidth = (startingIndex - tileLumaSize) % (partitionWidth / 2);
		siPartitionHeight = (startingIndex - tileLumaSize) / (partitionWidth / 2);

		return frameLumaSize + (((tileHeight/2) + siPartitionHeight) * (enCtx->width / 2)) + (tileWidth / 2) + siPartitionWidth;
	}
	else
	{
		siPartitionWidth = (startingIndex - tileLumaSize - tileChromaSize) % (partitionWidth / 2);
		siPartitionHeight = (startingIndex - tileLumaSize - tileChromaSize) / (partitionWidth / 2);

		return frameLumaSize + frameChromaSize + (((tileHeight / 2) + siPartitionHeight) * (enCtx->width / 2)) + (tileWidth / 2) + siPartitionWidth;
	}
}

void ComUtil::TransposeMatrix(int16_t* input, int blockSize)
{
	int16_t* transpose = new int16_t[blockSize * blockSize];

	for (int i = 0; i < blockSize; i++)
	{
		for (int j = 0; j < blockSize; j++)
		{
			transpose[i * blockSize + j] = input[j * blockSize + i];
		}
	}

	for (int i = 0; i < blockSize * blockSize; i++)
	{
		input[i] = transpose[i];
	}

	delete[] transpose;
}

void ComUtil::TransposeInputResidual(int16_t* residual, int residualSize, int blockSize)
{
	int cnt = 0;

	while (cnt != residualSize)
	{
		TransposeMatrix(&residual[cnt], blockSize);
		cnt += blockSize * blockSize;
	}
}
