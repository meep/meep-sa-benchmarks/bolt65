/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Statistics.h"


void Statistics::calculateTileLBEfficiency(float* threadData, int numOfThreads)
{
	float max = 0.0;
	float sum = 0.0;
	float min = threadData[0];
	for (int i = 0; i < numOfThreads; i++)
	{
		if (threadData[i] < min)
			min = threadData[i];
		if (threadData[i] > max)
			max = threadData[i];

		sum += threadData[i];
	}
	efficiencyIdeal += ((max / (sum / numOfThreads)) - 1) * 100;
	efficiency += ((max / min) - 1) * 100;
	efficiencyCounter++;

}

void Statistics::showVideoStats(EncodingContext* enCtx)
{
	cout << "Video statistics:" << endl;
	long long avgBits = 0;
	double avgPSNRY = 0.0;
	double avgPSNRU = 0.0;
	double avgPSNRV = 0.0;
	double clocks = 0.0;

	if (enCtx->calculateBits)
	{
		avgBits = sumBits / enCtx->numOfFrames;
		cout << "Average bitrate: " << avgBits * enCtx->frameRate / 1000 << " kbps " << endl;
	}
	if (enCtx->calculatePSNR)
	{
		avgPSNRY = sumPSNRY / enCtx->numOfFrames;
		avgPSNRU = sumPSNRU / enCtx->numOfFrames;
		avgPSNRV = sumPSNRV / enCtx->numOfFrames;
		cout << setprecision(5) << "PSNR per frame:  PSNR Y: " << avgPSNRY << " db  PSNR U: " << avgPSNRU << " db  PSNR V: " << avgPSNRV << " db" << endl;
	}

	if (enCtx->calculateTime)
	{
		clocks = double(clock() - begin_video);
		std::cout << setprecision(5) << " Processing time: " << clocks / CLOCKS_PER_SEC << " s " << endl;


		if (showTestTileStats)
		{
			std::cout << endl << "Tile load balancing test variables" << endl;
			std::cout << setprecision(5) << " Overhead: " << overhead << " s " << endl;
			std::cout << setprecision(5) << " Average max/min thread processing time: " << efficiency / efficiencyCounter << " % " << endl;
			std::cout << setprecision(5) << " Average max/ideal thread processing time: " << efficiencyIdeal / efficiencyCounter << " % " << endl;
		}
	}

	if (enCtx->outputStatsToFile)
	{
		//fetch current date and time
		std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
		std::time_t end_time = std::chrono::system_clock::to_time_t(end);

		//converting time to custom format without \n
		char timeStamp[80];
		strftime(timeStamp, 80, "%d/%m/%y %X ", localtime(&end_time));

		//TO DO: bit size needs to be converted to average bitrate per second
		ofstream outputStats;
		outputStats.open(enCtx->outputStatsFileName);
		outputStats << "Command, Date/Time, Elapsed Time, Test Time, FPS, Bitrate kbps, Y PSNR, U PSNR, V PSNR, Global PSNR, SSIM, SSIM (dB), I count, I ave-QP, I kbps, I-PSNR Y, I-PSNR U, I-PSNR V, I-SSIM (dB), P count, P ave-QP, P kbps, P-PSNR Y, P-PSNR U, P-PSNR V, P-SSIM (dB), B count, B ave-QP, B kbps, B-PSNR Y, B-PSNR U, B-PSNR V, B-SSIM (dB), MaxCLL, MaxFALL, Version\n";
		outputStats << enCtx->command << "," << timeStamp << "," << clocks / CLOCKS_PER_SEC << "," << testSum << "," << enCtx->numOfFrames / (clocks / CLOCKS_PER_SEC) << "," << avgBits * enCtx->frameRate / 1000 << "," << avgPSNRY << "," << avgPSNRU << "," << avgPSNRV << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << enCtx->version << "\n";
		outputStats.close();
	}

}

void Statistics::showVideoStatsTranscoding(EncodingContext* enCtx, EncodingContext* decCtx)
{
	cout << "Video statistics:" << endl;
	long long avgBits = 0;
	double avgPSNRY = 0.0;
	double avgPSNRU = 0.0;
	double avgPSNRV = 0.0;
	double clocks = 0.0;

	if (enCtx->calculateBits)
	{
		avgBits = sumBits / enCtx->numOfFrames;
		cout << "Average bitrate: " << avgBits * enCtx->frameRate / 1000 << " kbps " << endl;
	}
	if (enCtx->calculatePSNR)
	{
		avgPSNRY = sumPSNRY / enCtx->numOfFrames;
		avgPSNRU = sumPSNRU / enCtx->numOfFrames;
		avgPSNRV = sumPSNRV / enCtx->numOfFrames;
		cout << setprecision(5) << "PSNR per frame:  PSNR Y: " << avgPSNRY << " db  PSNR U: " << avgPSNRU << " db  PSNR V: " << avgPSNRV << " db" << endl;
	}

	if (enCtx->calculateTime)
	{
		clocks = double(clock() - begin_video);
		std::cout << setprecision(5) << " Processing time: " << clocks / CLOCKS_PER_SEC << " s " << endl;
		std::cout << setprecision(5) << " Processing time -> encoder part: " << encSum << " s " << endl;
		std::cout << setprecision(5) << " Processing time -> decoder part: " << decSum << " s " << endl;

	}

	if (enCtx->outputStatsToFile)
	{
		//fetch current date and time
		std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
		std::time_t end_time = std::chrono::system_clock::to_time_t(end);

		//converting time to custom format without \n
		char timeStamp[80];
		strftime(timeStamp, 80, "%d/%m/%y %X ", localtime(&end_time));

		//TO DO: bit size needs to be converted to average bitrate per second
		ofstream outputStats;
		outputStats.open(enCtx->outputStatsFileName);
		outputStats << "Command, Date/Time, Elapsed Time, Test Time, Elapsed time (encoder), Elapsed time (decoder), Encoding FPS, Bitrate kbps, Y PSNR, U PSNR, V PSNR, Global PSNR, SSIM, SSIM (dB), I count, I ave-QP, I kbps, I-PSNR Y, I-PSNR U, I-PSNR V, I-SSIM (dB), P count, P ave-QP, P kbps, P-PSNR Y, P-PSNR U, P-PSNR V, P-SSIM (dB), B count, B ave-QP, B kbps, B-PSNR Y, B-PSNR U, B-PSNR V, B-SSIM (dB), MaxCLL, MaxFALL, Version\n";
		outputStats << enCtx->command << "," << timeStamp << "," << clocks / CLOCKS_PER_SEC << "," << testSum << "," << encSum << "," << decSum << "," << (float)enCtx->numOfFrames / (encSum) << "," << avgBits * enCtx->frameRate / 1000 << "," << avgPSNRY << "," << avgPSNRU << "," << avgPSNRV << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << "/" << "," << enCtx->version << "\n";
		outputStats.close();
	}

}

void Statistics::showVideoTime(EncodingContext* enCtx)
{
	double clocks;
	if (enCtx->calculateTime)
	{
		clocks = double(clock() - begin_video);
		std::cout << setprecision(5) << " Processing time: " << clocks / CLOCKS_PER_SEC << " s " << endl;


		if (showTestTileStats)
		{
			std::cout << endl << "Tile load balancing test variables" << endl;
			std::cout << setprecision(5) << " Overhead: " << overhead << " s " << endl;
			std::cout << setprecision(5) << " Average max/min thread processing time: " << efficiency / efficiencyCounter << " % " << endl;
			std::cout << setprecision(5) << " Average max/ideal thread processing time: " << efficiencyIdeal / efficiencyCounter << " % " << endl;
		}
	}
}

/*-----------------------------------------------------------------------------
Function name: showFrameStats
Author/s: Igor, Leon
Current Version:
Version 1.1 - Added support for outputing average fps processing time to csv file.
Description:	Function for displaying and outputing statistics to console and csv file.

Parameters:
Inputs: clock_t beginTime	-	Time when process has started
Frame *frame		-	Frame with data for calculating PSNR and SSIM

Outputs: /

Returns: Void

Comments: SSIM doesn't work properly at this point.

-----------------------------------------------------------------------------*/

void Statistics::calculateFrameStats(Frame* frame, EncodingContext* enCtx)
{
	if (enCtx->calculateBits)
	{
		if (enCtx->showStatsPerFrame)
			cout << " [ " << frameBits / 8 / 1024 << " kB (" << frameBits << " bits) ] ";

		//sumBits += ec.buffer.Param_Number_of_bits;

		//currentPerformance.bitrate = frameBits / 8 / 1024;
		frameBits = 0;
	}

	if (enCtx->calculatePSNR)
	{
		float psnrY, psnrU, psnrV;
		ComUtil::calculatePSNR_YUV(frame->payload, frame->reconstructed_payload, frame->width, frame->height, frame->enCtx->bitNumber, psnrY, psnrU, psnrV);
		//float psnr = ComUtil::calculatePSNR(frame->frame_payload, frame->reconstructed_payload, frame->size, frame->enCtx->bitNumber);
		if (enCtx->showStatsPerFrame)
			cout << setprecision(5) << " [ PSNR Y: " << psnrY << " db  PSNR U: " << psnrU << " db  PSNR V: " << psnrV << " db ]";

		sumPSNRY += psnrY;
		sumPSNRU += psnrU;
		sumPSNRV += psnrV;

		/*currentPerformance.PSNRY = psnrY;
		currentPerformance.PSNRU = psnrU;
		currentPerformance.PSNRV = psnrV;*/
	}

	if (enCtx->calculateSSIM)
	{
		float ssimY, ssimU, ssimV, ssimYUV;

		ComUtil::calculateSSIM_YUV(frame->payload, frame->reconstructed_payload, frame->width, frame->height, frame->enCtx->bitNumber, ssimY, ssimU, ssimV);

		ssimYUV = (4 * ssimY + ssimU + ssimV) / 6;

		if (enCtx->showStatsPerFrame)
			cout << setprecision(5) << " [ SSIM Y: " << ssimY << " SSIM U: " << ssimU << " SSIM V: " << ssimV << " SSIM YUV " << ssimYUV << " ]";

		//sumPSNRY += psnrY;
		//sumPSNRU += psnrU;
		//sumPSNRV += psnrV;
	}

	if (enCtx->calculateTime)
	{
		float clocks = float(clock() - begin_frame);
		if (enCtx->showStatsPerFrame)
		{
			std::cout << setprecision(5) << " [ Processing time: " << clocks / CLOCKS_PER_SEC << " s ]";
			//currentPerformance.timePerFrame = clocks / CLOCKS_PER_SEC;
		}
		if (enCtx->tilesEnabled)
		{
			if (enCtx->showStatsPerTile)
			{
				std::cout << endl << "Performance per tiles: " << endl;
				for (int t = 0; t < enCtx->numberOfTilesInColumn * enCtx->numberOfTilesInRow; t++)
					std::cout << setprecision(7) << " [ Processing time of tile " << t << " : " << frame->tiles[t]->proccesingTime << " s ]" << endl;
			}

			if (enCtx->outputStatsTileToFile)
			{
				if (frame->poc == 0 && !outputStatsTilesExist)
				{
					//Write header
					outputStatsTiles << "Video, QP, Tiles, FPS, FrameNum, NumOfTiles, FramePT, FrameType";
					for (int t = 0; t < 32; t++)
						outputStatsTiles << ", Tile" << t << "PT";

					outputStatsTiles << endl;
				}

				//calculate frame processing time 
				outputStatsTiles << enCtx->inputFilePath << ", " << enCtx->quantizationParameter << ", " << enCtx->numberOfTilesInColumn << "X" << enCtx->numberOfTilesInRow << ", " << enCtx->frameRate << ", " << frame->poc << ", " << enCtx->numberOfTilesInColumn * enCtx->numberOfTilesInRow << ", " << setprecision(5) << clocks << ", " << frame->type;

				for (int t = 0; t < enCtx->numberOfTilesInColumn * enCtx->numberOfTilesInRow; t++)
					outputStatsTiles << setprecision(7) << ", " << frame->tiles[t]->proccesingTime;

				for (int t = 0; t < 32 - enCtx->numberOfTilesInColumn * enCtx->numberOfTilesInRow; t++)
					outputStatsTiles << ", " << 0;

				outputStatsTiles << endl;

			}
		}
	}



}

void Statistics::showFrameTime(EncodingContext* enCtx)
{
	if (enCtx->calculateTime)
	{
		float clocks = float(clock() - begin_frame);
		if (enCtx->showStatsPerFrame)
		{
			std::cout << setprecision(5) << " [ Processing time: " << clocks / CLOCKS_PER_SEC << " s ]";
			//currentPerformance.timePerFrame = clocks / CLOCKS_PER_SEC;
		}
	}
}

void Statistics::outputTileStatsToFile(bool performOutput, string outputFileName)
{
	outputStatsTilesExist = ComUtil::doesFileExist(outputFileName);
	outputStatsTiles.open(outputFileName, std::ios_base::app);
}

void Statistics::startVideoClock(bool calculate)
{
	if (calculate)
		begin_video = clock();
}

void Statistics::startFrameClock(bool calculate)
{
	if (calculate)
		begin_frame = clock();
}

void Statistics::startEncodingClock(bool calculate)
{
	if (calculate)
		begin_enc = clock();
}

void Statistics::startDecodingClock(bool calculate)
{
	if (calculate)
		begin_dec = clock();
}

void Statistics::addToEncodingClock()
{
	encSum += float(clock() - begin_enc) / CLOCKS_PER_SEC;
}

void Statistics::addToDecodingClock()
{
	decSum += float(clock() - begin_dec) / CLOCKS_PER_SEC;
}

void Statistics::startTestClock()
{
	begin_test = clock();
}

void Statistics::addToTestClock()
{
	testSum += float(clock() - begin_frame) / CLOCKS_PER_SEC;
}

void Statistics::clear(bool outputStatsTileToFile)
{
	if (outputStatsTileToFile)
		outputStatsTiles.close();
}

Statistics::Statistics()
{

}

Statistics::~Statistics()
{
}