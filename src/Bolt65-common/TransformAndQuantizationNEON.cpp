/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __ARM_NEON
#include "TransformAndQuantizationNEON.h"

TransformAndQuantizationNEON::TransformAndQuantizationNEON()
{
}

void TransformAndQuantizationNEON::multiply(int16_t *A, int16_t *B, int N, int16_t *C, int bdShift)
{
	int shiftOffset = 1 << (bdShift - 1);
	int32x4_t bdShift_right = vmovq_n_s32(-bdShift);

	if (N == 4)
	{
		int16x4_t mB[4];
		int32x4_t mC[4];

		int16x8_t doubleRow;

		for (int i = 0; i < N; i = i + 2)
		{
			doubleRow = vld1q_s16(B + i * N);
			mB[i] = vget_low_s16(doubleRow);
			mB[i + 1] = vget_high_s16(doubleRow);
		}

		int32x4_t tmp;
		int16x4_t mA;

		for (int i = 0; i < N; i++)
		{
			mC[i] = vmovq_n_s32(0);
			for (int k = 0; k < N; k++)
			{
				mA = vget_low_s16(vmovq_n_s16(A[i*N + k]));
				tmp = vmull_s16(mA, mB[k]);
				mC[i] = vaddq_s32(tmp, mC[i]);
			}

			tmp = vmovq_n_s32(shiftOffset);
			mC[i] = vaddq_s32(mC[i], tmp);
			mC[i] = vshlq_s32(mC[i], bdShift_right);
			vst1_s16(C + (i * 4), vqmovn_s32(mC[i]));
		}
	}
	else if (N == 8)
	{
		int16x8_t mB[8];
		int16x4_t mB_half[16];
		int32x4_t mC_half[16];

		for (int i = 0; i < N; i++)
		{
			mB[i] = vld1q_s16(B + i * N);
			mB_half[i] = vget_low_s16(mB[i]);
			mB_half[i + N] = vget_high_s16(mB[i]);
		}

		int32x4_t tmp;
		int16x4_t mA;

		for (int i = 0; i < N; i++)
		{
			mC_half[i] = vmovq_n_s32(0);
			mC_half[i + N] = vmovq_n_s32(0);

			for (int k = 0; k < N; k++)
			{
				mA = vget_low_s16(vmovq_n_s16(A[i*N + k]));

				tmp = vmull_s16(mA, mB_half[k]);
				mC_half[i] = vaddq_s32(tmp, mC_half[i]);

				tmp = vmull_s16(mA, mB_half[k + N]);
				mC_half[i + N] = vaddq_s32(tmp, mC_half[i + N]);

			}

			tmp = vmovq_n_s32(shiftOffset);
			mC_half[i] = vaddq_s32(mC_half[i], tmp);
			mC_half[i] = vshlq_s32(mC_half[i], bdShift_right);
			vst1_s16(C + (i*N), vqmovn_s32(mC_half[i]));

			mC_half[i + N] = vaddq_s32(mC_half[i + N], tmp);
			mC_half[i + N] = vshlq_s32(mC_half[i + N], bdShift_right);
			vst1_s16(C + (i*N + (N / 2)), vqmovn_s32(mC_half[i + N]));
		}

	}

	else if (N == 16)
	{
		int16x8_t mB[32];
		int16x4_t mB_quarter[64];
		int32x4_t mC_quarter[64];

		for (int i = 0; i < N; i++)
		{
			mB[i] = vld1q_s16(B + i * N);
			mB[i + N] = vld1q_s16(B + i * N + (N / 2));

			mB_quarter[i] = vget_low_s16(mB[i]);
			mB_quarter[i + N] = vget_high_s16(mB[i]);
			mB_quarter[i + 2 * N] = vget_low_s16(mB[i + N]);
			mB_quarter[i + 3 * N] = vget_high_s16(mB[i + N]);
		}

		int32x4_t tmp;
		int16x4_t mA;

		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N / 4; j++)
				mC_quarter[i + j * N] = vmovq_n_s32(0);

			for (int k = 0; k < N; k++)
			{
				mA = vget_low_s16(vmovq_n_s16(A[i*N + k]));

				for (int j = 0; j < N / 4; j++)
				{
					tmp = vmull_s16(mA, mB_quarter[k + j * N]);
					mC_quarter[i + j * N] = vaddq_s32(tmp, mC_quarter[i + j * N]);
				}
			}

			tmp = vmovq_n_s32(shiftOffset);

			for (int j = 0; j < N / 4; j++)
			{
				mC_quarter[i + j * N] = vaddq_s32(mC_quarter[i + j * N], tmp);
				mC_quarter[i + j * N] = vshlq_s32(mC_quarter[i + j * N], bdShift_right);
				vst1_s16(C + (i*N) + ((j*N) / 4), vqmovn_s32(mC_quarter[i + j * N]));
			}
		}

	}

	else if (N == 32)
	{
		int16x8_t mB[128];
		int16x4_t mB_eight[256];
		int32x4_t mC_eight[256];

		for (int i = 0; i < N; i++)
		{
			mB[i] = vld1q_s16(B + i * N);
			mB[i + N] = vld1q_s16(B + i * N + (N / 4));
			mB[i + 2 * N] = vld1q_s16(B + i * N + (N / 2));
			mB[i + 3 * N] = vld1q_s16(B + i * N + (3 * N / 4));

			mB_eight[i] = vget_low_s16(mB[i]);
			mB_eight[i + N] = vget_high_s16(mB[i]);
			mB_eight[i + 2 * N] = vget_low_s16(mB[i + N]);
			mB_eight[i + 3 * N] = vget_high_s16(mB[i + N]);
			mB_eight[i + 4 * N] = vget_low_s16(mB[i + 2 * N]);
			mB_eight[i + 5 * N] = vget_high_s16(mB[i + 2 * N]);
			mB_eight[i + 6 * N] = vget_low_s16(mB[i + 3 * N]);
			mB_eight[i + 7 * N] = vget_high_s16(mB[i + 3 * N]);
		}

		int32x4_t tmp;
		int16x4_t mA;

		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N / 4; j++)
				mC_eight[i + j * N] = vmovq_n_s32(0);

			for (int k = 0; k < N; k++)
			{
				mA = vget_low_s16(vmovq_n_s16(A[i*N + k]));

				for (int j = 0; j < N / 4; j++)
				{
					tmp = vmull_s16(mA, mB_eight[k + j * N]);
					mC_eight[i + j * N] = vaddq_s32(tmp, mC_eight[i + j * N]);
				}
			}

			tmp = vmovq_n_s32(shiftOffset);

			for (int j = 0; j < N / 4; j++)
			{
				mC_eight[i + j * N] = vaddq_s32(mC_eight[i + j * N], tmp);
				mC_eight[i + j * N] = vshlq_s32(mC_eight[i + j * N], bdShift_right);
				vst1_s16(C + (i*N) + (j*N / 8), vqmovn_s32(mC_eight[i + j * N]));
			}
		}

	}

}

void TransformAndQuantizationNEON::quantization(int16_t *A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;
	int mScalingFactor = 16;
	*is_zero_matrix = true;

	int16x8_t data;
	int16x4_t data_low, data_high;
	int32x4_t abs_low, abs_high;
	int32x4_t qdata_low, qdata_high;
	int32x4_t addone_low_mask, addone_high_mask;

	int32x4_t mask_zero = vmovq_n_s32(-1);
	int32x4_t mask_one = vmovq_n_s32(1);

	int32x4_t sign_low_mask = vmovq_n_s32(0);
	int32x4_t sign_high_mask = vmovq_n_s32(0);

	int16x4_t mult = vget_low_s16(vmovq_n_s16(f[QP % 6]));
	int32x4_t shiftOffset = vmovq_n_s32(1 << (bdShift - 1));
	int32x4_t bdShift_right = vmovq_n_s32(-((QP / 6) + bdShift));

	int32_t sum = 0;

	for (int i = 0; i < N*N; i = i + 8)
	{
		data = vld1q_s16(A + i);
		data_low = vget_low_s16(data);
		data_high = vget_high_s16(data);

		qdata_low = vmull_s16(data_low, mult);
		qdata_high = vmull_s16(data_high, mult);

		//Save the signs of vector elements for end result
		//Since there is no NEON function to negate certain elements of vector, we have to remebmer which elements are negative
		//To do so we remember sign_mask and addone in order to do 2'complement by doing (Vector XOR signmask) + addone
		sign_low_mask = vreinterpretq_s32_u32(vcltzq_s32(qdata_low));
		sign_high_mask = vreinterpretq_s32_u32(vcltzq_s32(qdata_high));
		addone_low_mask = vandq_s32(sign_low_mask, mask_one);
		addone_high_mask = vandq_s32(sign_high_mask, mask_one);

		qdata_low = vabsq_s32(qdata_low);
		qdata_high = vabsq_s32(qdata_high);

		qdata_low = vaddq_s32(qdata_low, shiftOffset);
		qdata_high = vaddq_s32(qdata_high, shiftOffset);

		qdata_low = vshlq_s32(qdata_low, bdShift_right);
		qdata_high = vshlq_s32(qdata_high, bdShift_right);

		sum = vaddvq_u32(vtstq_s32(qdata_low, mask_zero)) + vaddvq_u32(vtstq_s32(qdata_high, mask_zero));
		if (sum != 0)
			*is_zero_matrix = false;

		//Negate elements of vector based on the masks
		qdata_low = vaddq_s32(veorq_s32(qdata_low, sign_low_mask), addone_low_mask);
		qdata_high = vaddq_s32(veorq_s32(qdata_high, sign_high_mask), addone_high_mask);

		vst1_s16(C + i, vqmovn_s32(qdata_low));
		vst1_s16(C + i + 4, vqmovn_s32(qdata_high));

	}
}

void TransformAndQuantizationNEON::dequantization(int16_t * A, int QP, int bitDepth, int N, int16_t * C)
{
	int M = ComUtil::logarithm2(N);

	int log2TransformRange = 15;
	int bdShift = bitDepth + M + 10 - log2TransformRange;
	int mScalingFactor = 16;

	int16x8_t data;
	int16x4_t data_low, data_high;
	int32x4_t original_low, original_high;

	int16x4_t mult = vget_low_s16(vmovq_n_s16(mScalingFactor));
	int32x4_t mult2 = vmovq_n_s32(g[QP % 6]);
	mult2 = vshlq_s32(mult2, vmovq_n_s32(QP / 6));
	int32x4_t shiftOffset = vmovq_n_s32(1 << (bdShift - 1));
	int32x4_t bdShift_right = vmovq_n_s32(-bdShift);

	for (int i = 0; i < N*N; i = i + 8)
	{
		data = vld1q_s16(A + i);
		data_low = vget_low_s16(data);
		data_high = vget_high_s16(data);

		original_low = vmull_s16(data_low, mult);
		original_high = vmull_s16(data_high, mult);

		original_low = vmulq_s32(original_low, mult2);
		original_high = vmulq_s32(original_high, mult2);

		original_low = vaddq_s32(original_low, shiftOffset);
		original_high = vaddq_s32(original_high, shiftOffset);

		original_low = vshlq_s32(original_low, bdShift_right);
		original_high = vshlq_s32(original_high, bdShift_right);

		vst1_s16(C + i, vqmovn_s32(original_low));
		vst1_s16(C + i + 4, vqmovn_s32(original_high));
	}
}

#endif
