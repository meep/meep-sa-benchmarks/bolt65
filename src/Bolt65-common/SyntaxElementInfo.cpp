/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "SyntaxElementInfo.h"

SyntaxElementInfo::SyntaxElementInfo()
{
	binType = FixedLength;
	cMax = 1;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int bcMax, unsigned int bcRice, unsigned int bk, BinarizationAdditionalOperation addOp, bool cabac, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx, bool dSpecialCtxIdx)
{
	binType = bType;
	cMax = bcMax;
	cRiceParam = bcRice;
	k = bk;
	addOperations[0] = addOp;

	isCABACencoded = cabac;
	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;

	derivateSpecialCtxIdx = dSpecialCtxIdx;

}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int param)
{
	binType = bType;

	if (binType == TruncatedUnary || binType == FixedLength)
		cMax = param;
	else if (binType == ExpGolomb)
		k = param;
	else if (binType == NoBinarization)
		writeBits = param;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, BinarizationAdditionalOperation addOp, unsigned int param)
{
	binType = bType;
	addOperations[0] = addOp;

	if (addOp != PrecalculateParameter)
	{
		if (binType == TruncatedUnary || binType == FixedLength)
			cMax = param;
		else if (binType == ExpGolomb)
			k = param;
	}
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, BinarizationAdditionalOperation addOp)
{
	binType = bType;
	addOperations[0] = addOp;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int param, unsigned int cTableIdx, unsigned  int cIdx, unsigned int  cIdxP, unsigned int sPIdx)
{
	binType = bType;


	if (binType == TruncatedUnary || binType == FixedLength)
		cMax = param;
	else if (binType == ExpGolomb)
		k = param;

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int cRParam, unsigned int cM, unsigned int cTableIdx, unsigned int cIdx, unsigned int  cIdxP, unsigned int sPIdx)
{
	binType = bType;

	cRiceParam = cRParam;
	cMax = cM;

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int param, BinarizationAdditionalOperation addOp, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx)
{
	binType = bType;
	addOperations[0] = addOp;

	if (addOp != PrecalculateParameter)
	{
		if (binType == TruncatedUnary || binType == FixedLength)
			cMax = param;
		else if (binType == ExpGolomb)
			k = param;
	}

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int cRParam, unsigned int cM, BinarizationAdditionalOperation addOp, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx)
{
	binType = bType;
	addOperations[0] = addOp;

	cRiceParam = cRParam;
	cMax = cM;

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int param, BinarizationAdditionalOperation addOp, BinarizationAdditionalOperation addOp2, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx)
{
	binType = bType;
	addOperations[0] = addOp;
	addOperations[1] = addOp2;

	if (addOp != PrecalculateParameter || addOp2 != PrecalculateParameter)
	{
		if (binType == TruncatedUnary || binType == FixedLength)
			cMax = param;
		else if (binType == ExpGolomb)
			k = param;
	}

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int cRParam, unsigned int cM, BinarizationAdditionalOperation addOp, BinarizationAdditionalOperation addOp2, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx)
{
	binType = bType;
	addOperations[0] = addOp;
	addOperations[1] = addOp2;

	cRiceParam = cRParam;
	cMax = cM;

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, BinarizationAdditionalOperation addOp, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx)
{
	binType = bType;

	addOperations[0] = addOp;

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::SyntaxElementInfo(BinarizationType bType, unsigned int cTableIdx, unsigned int cIdx, unsigned int cIdxP, unsigned int sPIdx)
{
	binType = bType;

	isCABACencoded = true;

	ctxTableIdx = cTableIdx;
	ctxIdx = cIdx;
	ctxIdxP = cIdxP;
	syntaxProbModelsIdx = sPIdx;
}

SyntaxElementInfo::~SyntaxElementInfo()
{
}
