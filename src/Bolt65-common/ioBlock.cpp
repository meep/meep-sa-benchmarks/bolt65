/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/


#include "ioBlock.h"
#include "EntropyBuffer.h"
#include <iostream>

using namespace std;

streamoff ioBlock::openFile(string path)
{
	// Opening new file stream from filepath
	ifs = new ifstream(path, ios::binary | ios::in);

	if (readHeader())
	{
		FSize = (FWidth * FHeight) * 3 / 2;

		streamoff current_pos = ifs->tellg();
		ifs->seekg(0, ifs->end);
		streamoff file_size = ifs->tellg();

		ifs->seekg(current_pos);

		return file_size;
	}

	return -1;
}

streamoff ioBlock::openFile(string path, int width, int height)
{
	// Opening new file stream from filepath
	ifs = new ifstream(path, ios::binary | ios::in);
	FWidth = width;
	FHeight = height;

	if (!ifs)
		return -1;

	FSize = (FWidth * FHeight) * 3 / 2;

	streamoff file_size = ifs->tellg();

	ifs->seekg(0);

	return file_size;
}

void ioBlock::closeFiles()
{
	//delete ifs;
	delete outputBitstreamFile;
}

// Function for parsing the header of input file
bool ioBlock::readHeader()
{
	//if (!ifs)
		//return false;

	string format_check("");

	char c = fgetc(inputFilePointer);

	while ((c != ' ') && (c != '\n'))
	{
		// Parsing magic word YUV4MPEG2
		format_check += c;
		c = fgetc(inputFilePointer);
	}

	// Check format
	if (format_check.compare("YUV4MPEG2") != 0)
		return false;

	string parsing_str("");

	// Parsing header parameters
	while (c != '\n')
	{
		c = fgetc(inputFilePointer);

		switch (c)
		{
		case 'W':		// Width parameter
			parsing_str = "";
			c = fgetc(inputFilePointer);

			while (c != ' ' && c != '\n')
			{
				parsing_str += c;
				c = fgetc(inputFilePointer);
			}

			FWidth = stoi(parsing_str);
			break;

		case 'H':		// Height parameter
			parsing_str = "";
			c = fgetc(inputFilePointer);

			while (c != ' ' && c != '\n')
			{
				parsing_str += c;
				c = fgetc(inputFilePointer);
			}

			FHeight = stoi(parsing_str);
			break;

		default:		// Other data
			while (c != ' ' && c != '\n')
			{
				c = fgetc(inputFilePointer);
			}
			break;
		}
	}

	return true;

}

// Function for reading next frame of inputed video senquence
char* ioBlock::readNextFrame(int nFramesToRead, bool isYUV, bool &eof)
{
	char* picture = new char[FSize];
	char c = ' ';
	size_t result;

	if (!isYUV)
	{
		while (c != '\n')
		{
			c = fgetc(inputFilePointer);
		}
	}

	result = fread(picture, 1, FSize, inputFilePointer);
	if (result != FSize) { eof = true; }

	


	//fseek(inputFilePointer, 9, SEEK_SET);

	/*for (int i = 0; i < FSize; i++)
	{
		picture[i] = ifs->get();
		if (!ifs->good())
		{
			
		}
	}*/

	return picture;
}

void ioBlock::openOutputFile(std::string fileName)
{
	outputBitstreamFile = new ofstream(fileName, std::ios::out | std::ios::app | std::ios::binary);

}

void ioBlock::writeToFile(int size)
{

	if (outputBitstreamFile->is_open())
		outputBitstreamFile->write((char *)&output_buffer, size);
}


//procedure for writing buffer to file. after the buffer is written, buffer is cleared.
int ioBlock::writeBufferToFile(EntropyBuffer *buffer)
{
	if (outputBitstreamFile->is_open())
	{
		outputBitstreamFile->write((char*)(&buffer->output_buffer.at(0)), buffer->output_buffer.size());
	}

	//clearing buffer after writing to file.

	int bits = (int)buffer->Param_Number_of_bits;
	buffer->output_buffer.clear();
	buffer->Param_Number_of_bits = 0;

	return bits;

}


/*void ioBlock::putIntoBuffer(unsigned char byte)
{
	output_buffer[byteCounter] = byte;
	byteCounter += 1;					// Update byteCounter

	if (byteCounter == outputBufferSize)		// Check if byteCounter is pointing to the end of buffer
	{
		byteCounter = 0;				// Reset byteCounter and flush buffer to selected file
		writeToFile(outputBufferSize);
	}
}*/

/*-----------------------------------------------------------------------------
Function name: openCompressedFile
Author/s: Cobrnic
Current Version: v0.1

Version history:

Description: Open file and set position to the start

Parameters:
Inputs:
-	file name

Outputs:
-	size of the file or -1 if file couldn't be opened
-----------------------------------------------------------------------------*/
streamoff ioBlock::openCompressedFile(string path)
{
	// Opening new file stream from filepath
	ifs = new ifstream(path, ios::binary | ios::in | ios::ate);

	if (!ifs)
		return -1;

	streamoff file_size = ifs->tellg();
	ifs->seekg(0);
	byteCounter = 0;

	return file_size;
}

/*-----------------------------------------------------------------------------
Function name: readCompressedFile
Author/s: Cobrnic
Current Version: v0.1

Version history:

Description: Read data from file into output buffer

Parameters:
Inputs:
-	size of the data block to be read

Outputs:
-	filled output buffer

Comments: 
//TODO: It has to be alighned on decoding channel level how to report EOF. Currently local flag in this method indicates it.
-----------------------------------------------------------------------------*/
void ioBlock::readCompressedFile(int size)
{
	bool isEof;

	if (ifs->good())
		ifs->read((char *)&output_buffer, size);
	
	if (ifs->eof())
		isEof = true;
}

/*-----------------------------------------------------------------------------
Function name: readByteFromBuffer
Author/s: Cobrnic
Current Version: v0.1

Version history:

Description: Read one byte from output buffer and set position to next byte. Reload
output buffer with new data block if it is full

Parameters:
Inputs:
Outputs:
-	byte read from current position in output buffer
-----------------------------------------------------------------------------*/

unsigned char ioBlock::readByteFromBuffer()
{
	unsigned char byte = output_buffer[byteCounter];
	byteCounter++;

	if (byteCounter == outputBufferSize)
	{
		byteCounter = 0;
		readCompressedFile(outputBufferSize);
	}
	return byte;
}

