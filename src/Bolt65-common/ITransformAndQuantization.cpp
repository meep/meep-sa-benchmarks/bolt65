/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ITransformAndQuantization.h"

void ITransformAndQuantization::forward2DTransform(int16_t * A, int NB, int N, int16_t * C, int colorIdx, PredMode cuPredMode)
{
	int log2TransformRange = 15;
	int bitDepth = 8;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int M = ComUtil::logarithm2(N);

	//TODO: Calculate bdShift as follows: bdShift = Max( 20 − bitDepth, extended_precision_processing_flag ? 11 : 0 )
	int bdShift = (NB + M - 9);

	int16_t* D = new int16_t[N*N];

	switch (N) {
	case 32:
		multiply(D32, A, N, D, bdShift);
		break;

	case 16:
		multiply(D16, A, N, D, bdShift);
		break;

	case 8:
		multiply(D8, A, N, D, bdShift);
		break;

	case 4:
		if (colorIdx == 0 && cuPredMode == MODE_INTRA)
			multiply(DST, A, N, D, bdShift);
		else
			multiply(D4, A, N, D, bdShift);

		break;

	default:
		break;
	}

	bdShift = M + 6;

	switch (N) {
	case 32:
		multiply(D, D32T, N, C, bdShift);
		break;

	case 16:
		multiply(D, D16T, N, C, bdShift);
		break;

	case 8:
		multiply(D, D8T, N, C, bdShift);
		break;

	case 4:
		if (colorIdx == 0 && cuPredMode == MODE_INTRA)
			multiply(D, DSTT, N, C, bdShift);
		else
			multiply(D, D4T, N, C, bdShift);
		break;

	default:
		break;
	}

	delete[] D;
}

void ITransformAndQuantization::forward2DInverseTransform(int16_t * A, int NB, int N, int16_t * C, int colorIdx, PredMode cuPredMode)
{
	int log2TransformRange = 15;
	int bitDepth = 8;
	int coeffMin = -32768;
	int coeffMax = 32767;

	//TODO Calculate bdShift as follows: bdShift = Max( 20 − bitDepth, extended_precision_processing_flag ? 11 : 0 )
	int bdShift = 7;

	int16_t *D = new int16_t[N*N];
	switch (N) {
	case 32:
		multiply(D32T, A, N, D, bdShift);
		break;

	case 16:
		multiply(D16T, A, N, D, bdShift);
		break;

	case 8:
		multiply(D8T, A, N, D, bdShift);
		break;

	case 4:
		if (colorIdx == 0 && cuPredMode == MODE_INTRA)
			multiply(DSTT, A, N, D, bdShift);
		else
			multiply(D4T, A, N, D, bdShift);
		break;

	default:
		break;
	}

	bdShift = 12;
	switch (N) {
	case 32:
		multiply(D, D32, N, C, bdShift);
		break;

	case 16:
		multiply(D, D16, N, C, bdShift);
		break;

	case 8:
		multiply(D, D8, N, C, bdShift);
		break;

	case 4:
		if (colorIdx == 0 && cuPredMode == MODE_INTRA)
			multiply(D, DST, N, C, bdShift);
		else
			multiply(D, D4, N, C, bdShift);
		break;

	default:
		break;
	}

	delete[] D;
}

void ITransformAndQuantization::performTQandIQTOnCluster(int QP, int bitNumber, int16_t* residualBlock, int residualSize, int blockSize, int16_t* qtBlock, int colorIdx, PredMode predMode, int16_t* iqtBlock)
{
	bool isZeroMatrix = false;
	int counter = 0;
	int size = blockSize * blockSize;
	int16_t* temp = new int16_t[blockSize * blockSize];

	while (counter != residualSize)
	{
		forward2DTransform(&residualBlock[counter], bitNumber, blockSize, temp, colorIdx, predMode);
		quantization(temp, QP, bitNumber, blockSize, &isZeroMatrix, &qtBlock[counter]);

		//Set last element to FFFF as is case for the accelerator
		if (isZeroMatrix)
			qtBlock[counter + size -1] = (int16_t)0x8000;
		else
		{
			dequantization(&qtBlock[counter], QP, bitNumber, blockSize, temp);
			forward2DInverseTransform(temp, bitNumber, blockSize, &iqtBlock[counter], colorIdx, predMode);
		}

		counter += size;
	}

	delete[] temp;

}
