/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __EPI_RISCV
#include "ComKernelsEPIRISCV.h"


#ifdef __EPI_ONLY_LMUL1
ComKernelsEPIRISCV::ComKernelsEPIRISCV()
{
	// Underuse __epi_e8.
	vl = __builtin_epi_vsetvlmax(__epi_e16, __epi_m1);
}


static inline __epi_4xi16 __builtin_epi_vwsubu_4xi16(__epi_8xi8 a, __epi_8xi8 b,
	long vl) {
	// Make room for a LMUL2 register.
	register __epi_4xi16 vdest_lo asm("v16");
	register __epi_4xi16 vdest_hi asm("v17");

	__asm__("vsetvli x0, %[gvl], e8,m1\n"
		"vwsubu.vv %[vdest], %[va], %[vb]\n"
		: [vdest] "=&v"(vdest_lo), "=&v"(vdest_hi)
		: [gvl] "r"(vl), [va] "v"(a), [vb] "v"(b));

	return vdest_lo;
}

// Widening reduction
static inline __epi_2xi32 __builtin_epi_vwredsum_2xi32(__epi_4xi16 a, long vl) {
	register __epi_2xi32 vdest;
	register __epi_2xi32 vneutral = __builtin_epi_vbroadcast_2xi32(0, vl);

	// vwredsumu.vs  vd, vs2, vs1, vm   # vd[0] =  sum( vs1[0] , zext(vs2[*]) )
	__asm__(
		"vsetvli x0, %[gvl], e16,m1\n"
		"vwredsumu.vs %[vdest], %[va], %[vneutral]\n"
		: [vdest] "=v"(vdest)
		: [gvl] "r"(vl), [va] "v"(a), [vneutral] "v"(vneutral));

	return vdest;
}

int ComKernelsEPIRISCV::SAD(unsigned char* original, unsigned char* predicted, int puWidth, int puHeight)
{

	int matrixSize = puWidth * puHeight;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	__epi_4xi16 zero_mask = __builtin_epi_vbroadcast_4xi16(0, maxElements);

	int sum = 0;

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_8xi8 original_v = __builtin_epi_vload_unsigned_8xi8(original + i, maxElements);
		__epi_8xi8 predicted_v = __builtin_epi_vload_unsigned_8xi8(predicted + i, maxElements);

		__epi_4xi16 difference = __builtin_epi_vwsubu_4xi16(original_v, predicted_v, maxElements);

		//get absolute value (This is different than in quantization, because here we don't need a mask of negatives for later)
		//First calculate -v and then perform max(-v,v) to get absolute value
		__epi_4xi16 difference_reversed = __builtin_epi_vsub_4xi16(zero_mask, difference, maxElements);
		difference = __builtin_epi_vmax_4xi16(difference, difference_reversed, maxElements);

		//Widening result to 32 int. In cases with longer vector lenghts, sum of absolute differences can overflow 16-bit range
		//sum += __builtin_epi_vextract_4xi16(__builtin_epi_vredsum_4xi16(difference, zero_mask, maxElements), 0);
		sum += __builtin_epi_vextract_2xi32(__builtin_epi_vwredsum_2xi32(difference, maxElements), 0);
	}

	return sum;
}

void ComKernelsEPIRISCV::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	int matrixSize = blockSize * blockSize;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_8xi8 raw_v = __builtin_epi_vload_unsigned_8xi8((unsigned char*)rawBlock + i, maxElements);
		__epi_8xi8 predicted_v = __builtin_epi_vload_unsigned_8xi8((unsigned char*)predictedBlock + i, maxElements);

		__epi_4xi16 residual_v = __builtin_epi_vwsubu_4xi16(raw_v, predicted_v, maxElements);

		__builtin_epi_vstore_4xi16(residualBlock + i, residual_v, maxElements);
	}
}

#else
ComKernelsEPIRISCV::ComKernelsEPIRISCV()
{
	vl = __builtin_epi_vsetvl(64, __epi_e8, __epi_m1);
}

int ComKernelsEPIRISCV::SAD(unsigned char* original, unsigned char* predicted, int puWidth, int puHeight)
{
	int matrixSize = puWidth * puHeight;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	__epi_8xi16 zero_mask = __builtin_epi_vbroadcast_8xi16(0, maxElements);

	int sum = 0;

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_8xi8 original_v = __builtin_epi_vload_unsigned_8xi8(original + i, maxElements);
		__epi_8xi8 predicted_v = __builtin_epi_vload_unsigned_8xi8(predicted + i, maxElements);

		__epi_8xi16 difference = __builtin_epi_vwsubu_8xi16(original_v, predicted_v, maxElements);

		//get absolute value (This is different than in quantization, because here we don't need a mask of negatives for later)
		//First calculate -v and then perform max(-v,v) to get absolute value
		__epi_8xi16 difference_reversed = __builtin_epi_vsub_8xi16(zero_mask, difference, maxElements);
		difference = __builtin_epi_vmax_8xi16(difference, difference_reversed, maxElements);

		sum += __builtin_epi_vextract_8xi16(__builtin_epi_vredsum_8xi16(difference, zero_mask, maxElements), 0);
	}

	return sum;
}

void ComKernelsEPIRISCV::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	vl = __builtin_epi_vsetvl(64, __epi_e8, __epi_m1);

	int matrixSize = blockSize * blockSize;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_8xi8 raw_v = __builtin_epi_vload_unsigned_8xi8((unsigned char*)rawBlock + i, maxElements);
		__epi_8xi8 predicted_v = __builtin_epi_vload_unsigned_8xi8((unsigned char*)predictedBlock + i, maxElements);

		__epi_8xi16 residual_v = __builtin_epi_vwsubu_8xi16(raw_v, predicted_v, maxElements);

		__builtin_epi_vstore_8xi16(residualBlock + i, residual_v, maxElements);
	}
}

#endif

#endif
