/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "CUMapper.h"

CUMapper::CUMapper()
{
		
}

CUMapper::~CUMapper()
{

}

void CUMapper::MapToChildren(CU * transcodedCU)
{
	//although at this point CU should have children
	if (transcodedCU->hasChildren)
	{
		//remap transcoded CUs to children
		for (int i = 0; i < 4; i++)
		{
			int projectedOriginalCTUPixelColumn = (transcodedCU->children[i]->cbY.startingIndexInFrame % enCtx->width)*widthCoeff;
			int projectedOriginalCTUPixelRow = (transcodedCU->children[i]->cbY.startingIndexInFrame / enCtx->width)*heightCoeff;
			int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + transcodedCU->children[i]->cbY.blockSize*widthCoeff;
			int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + transcodedCU->children[i]->cbY.blockSize*heightCoeff;

			for (int j = 0; j < transcodedCU->transMappedCUs.size(); j++)
			{
				MapOriginalCUToTranscodedCU(transcodedCU->children[i], transcodedCU->transMappedCUs.at(j).second, projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
			}

			//When mapping is done categorize children CUs
			//Categorization::categorize(transcodedCU->children[i], enCtx);
		}

		for (int i = 0; i < 4; ++i)
			MapToChildren(transcodedCU->children[i]);
	}
}

//check if original CTU is between the startIndexInFrame and endIndexInFrame and assign it to transcoded CU
//also calculate weight of mapped CU 
void CUMapper::MapOriginalCUToTranscodedCU(CU * transcodedCU, CU * originalCU, int columnStart, int columnEnd, int rowStart, int rowEnd)
{
	//At this point originalCU should be in frame, but this is just in case
	if (originalCU->isInFrame)
	{
		int originalCUPixelColumn = originalCU->cbY.startingIndexInFrame %decCtx->width;
		int originalCuPixelRow = originalCU->cbY.startingIndexInFrame / decCtx->width;

		//In this case entire CU froom original frame is consisted in transcoded frame
		if (originalCUPixelColumn >= columnStart && originalCuPixelRow >= rowStart &&
			originalCUPixelColumn + originalCU->cbY.blockSize <= columnEnd && originalCuPixelRow + originalCU->cbY.blockSize <= rowEnd)
		{
			//Put all children to CU 
			mapAllAsWhole(transcodedCU, originalCU);
		}
		//Four cases where CU is totaly outside of transcoded CU
		else if (!((originalCUPixelColumn + originalCU->cbY.blockSize <= columnStart) || (originalCUPixelColumn >= columnEnd)
			|| (originalCuPixelRow + originalCU->cbY.blockSize <= rowStart) || (originalCuPixelRow >= rowEnd)))
		{
			if (originalCU->hasChildren)
			{
				for (int i = 0; i < 4; i++)
				{
					if (originalCU->children[i]->isInFrame)
						MapOriginalCUToTranscodedCU(transcodedCU, originalCU->children[i], columnStart, columnEnd, rowStart, rowEnd);
				}
			}
			else
			{
				float coeff = 0.0;
				float widthCuCoeff = 0.0;
				float heightCuCoeff = 0.0;

				if (originalCUPixelColumn >= columnStart && columnEnd - originalCUPixelColumn >= originalCU->cbY.blockSize)
					widthCuCoeff = 1;
				else if (originalCUPixelColumn < columnStart && columnEnd - originalCUPixelColumn >= originalCU->cbY.blockSize)
					widthCuCoeff = (float)(originalCUPixelColumn + originalCU->cbY.blockSize - columnStart) / originalCU->cbY.blockSize;
				else if (columnStart > originalCUPixelColumn && columnEnd < (originalCUPixelColumn + originalCU->cbY.blockSize))
					widthCuCoeff = (float)(columnEnd - columnStart) / originalCU->cbY.blockSize;
				else
					widthCuCoeff = (float)(columnEnd - originalCUPixelColumn) / originalCU->cbY.blockSize;


				if (originalCuPixelRow >= rowStart && rowEnd - originalCuPixelRow >= originalCU->cbY.blockSize)
					heightCuCoeff = 1;
				else if (originalCuPixelRow < rowStart && rowEnd - originalCuPixelRow >= originalCU->cbY.blockSize)
					heightCuCoeff = (float)(originalCuPixelRow + originalCU->cbY.blockSize - rowStart) / originalCU->cbY.blockSize;
				else if (rowStart > originalCuPixelRow && rowEnd < (originalCuPixelRow + originalCU->cbY.blockSize))
					heightCuCoeff = (float)(rowEnd - rowStart) / originalCU->cbY.blockSize;
				else
					heightCuCoeff = (float)(rowEnd - originalCuPixelRow) / originalCU->cbY.blockSize;

				coeff = widthCuCoeff * heightCuCoeff;

				RatioCU ratio;
				ratio.RatioInCU = coeff;
				transcodedCU->transMappedCUs.push_back(std::make_pair(ratio, originalCU));
			}
		}


	}


}

//This function is used for single Mapping, where calculating projected Row and Column is done once
//In case where for one CU we want to find all mapped decoded CUs it is faster to use MapOriginalCUToTranscodedCU(CU*,CU*,int,int,int,int) to avoid calculating projected values every time
void CUMapper::MapOriginalCUToTranscodedCU(CU * transcodedCU, CU * originalCU)
{
	int projectedOriginalCTUPixelColumn = (transcodedCU->cbY.startingIndexInFrame % enCtx->width)*widthCoeff;
	int projectedOriginalCTUPixelRow = (transcodedCU->cbY.startingIndexInFrame / enCtx->width)*heightCoeff;
	int projectedOriginalCTUPixelColumnEnd = projectedOriginalCTUPixelColumn + enCtx->ctbSize*widthCoeff;
	int projectedOriginalCTUPixelRowEnd = projectedOriginalCTUPixelRow + enCtx->ctbSize*heightCoeff;

	MapOriginalCUToTranscodedCU(transcodedCU, originalCU, projectedOriginalCTUPixelColumn, projectedOriginalCTUPixelColumnEnd, projectedOriginalCTUPixelRow, projectedOriginalCTUPixelRowEnd);
}

void CUMapper::mapAllAsWhole(CU * transcodedCU, CU * originalCU)
{
	if (originalCU->hasChildren)
	{
		for (int i = 0; i < 4; i++)
			mapAllAsWhole(transcodedCU, originalCU->children[i]);
	}
	else
	{
		if (originalCU->isInFrame)
		{
			RatioCU ratio;
			ratio.RatioInCU = 1.0;
			transcodedCU->transMappedCUs.push_back(std::make_pair(ratio, originalCU));
		}
	}
}