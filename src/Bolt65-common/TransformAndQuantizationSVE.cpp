/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __ARM_FEATURE_SVE
#include "TransformAndQuantizationSVE.h"

TransformAndQuantizationSVE::TransformAndQuantizationSVE()
{
}

//This function works with 32 bit integers by loading/Storing 16-bits to 32-bit registers 
//Since there is no signed expansion from 32-bit to 16-bits and vice versa, a number of instructions needed for this casting, would probably quash the performance of doing it this way
//This problem could be facilitated if there is multiply function that recieves two 16-bit operands and gets 32-bit results. However there is no such function in SVE
//SVE functions closest to this behaviour are: 
//svint32_t svmullt[_s32](svint16_t op1, svint16_t op2)
//svint32_t svmullb[_s32](svint16_t op1, svint16_t op2)
//However these functions cannot be compiled and the functionality may not be as expected, since it states: 
//These functions extract the odd-indexed (or even-indexed) elements of the integer inputs and multiply them, giving a result with twice the width

void TransformAndQuantizationSVE::multiply(int16_t *A, int16_t *B, int N, int16_t *C, int bdShift)
{
	size_t svewidth = svcntb();
	svint32_t *r1_32 = (svint32_t *)alloca(N * svewidth);
	svint32_t *r2_32 = (svint32_t *)alloca(N * svewidth);
#define mB(i) (*(svint32_t*)(((char*)r1_32) + (i)*svewidth))
#define mC(i) (*(svint32_t*)(((char*)r2_32) + (i)*svewidth))

	int shiftOffset = 1 << (bdShift - 1);

	//This part determines into how many parts we have to divide calculation, depending on the size of the vector
	//In SVE vector size can be different, depending on the architecture
	//NOTE: This works only for vector sizes that can be written as 2^N
	double parts = ceil((N * sizeof(int32_t)) / (double)svewidth);
	int step = svewidth / sizeof(int32_t);
	int offset = 0;

	svbool_t predicate = svwhilelt_b32_s32(0, N);

	for (int p = 0; p < parts; p++)
	{
		for (int i = 0; i < N; i++)
		{
			mB(i) = svld1sh_s32(predicate, B + (i*N) + offset);
			mC(i) = svdup_n_s32(0);
		}

		svint32_t mA;
		svint32_t temp;

		for (int i = 0; i < N; i++)
		{
			for (int k = 0; k < N; k++)
			{
				mA = svdup_n_s32(A[i*N + k]);
				temp = svmul_s32_x(predicate, mB(k), mA);
				mC(i) = svadd_s32_x(predicate, temp, mC(i));
			}
			temp = svdup_n_s32(shiftOffset);

			mC(i) = svadd_s32_x(predicate, mC(i), temp);
			mC(i) = svasr_n_s32_x(predicate, mC(i), bdShift);

			//Calling svsaturates16_s32_x is not necessary in this case since it has to be 
			svst1h_s32(predicate, C + (i*N) + offset, mC(i));
		}
		offset += step;
	}

	return;
}

void TransformAndQuantizationSVE::quantization(int16_t * A, int QP, int NB, int N, bool * is_zero_matrix, int16_t * C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;
	int mScalingFactor = 16;
	*is_zero_matrix = true;
	int matrixSize = N * N;

	size_t svewidth = svcntb();
	int maxNum = svewidth / sizeof(int32_t);
	if (matrixSize < maxNum)
		maxNum = matrixSize;

	svbool_t predicate = svwhilelt_b32_s32(0, maxNum);
	svint32_t data, absdata;

	svint32_t mult = svdup_n_s32(f[QP % 6]);
	svint32_t shiftOffset = svdup_n_s32(1 << (bdShift - 1));

	for (int i = 0; i < matrixSize; i = i + maxNum)
	{
		data = svld1sh_s32(predicate, A + i);

		absdata = svabs_s32_x(predicate, data);

		absdata = svmul_s32_x(predicate, absdata, mult);
		absdata = svadd_s32_x(predicate, absdata, shiftOffset);
		absdata = svasr_n_s32_x(predicate, absdata, ((QP / 6) + bdShift));

		data = svneg_s32_m(absdata, svcmplt_n_s32(predicate, data, 0), absdata);

		if (svptest_any(predicate, svcmpne_n_s32(predicate, data, 0)))
			*is_zero_matrix = false;

		//Probably not necessary for quantization since the number is unlikely to be out of 16-bit scope (TODO: check to be sure)
		data = svsaturates16_s32_x(predicate, data);

		svst1h_s32(predicate, C + i, data);
	}
}
void TransformAndQuantizationSVE::dequantization(int16_t * A, int QP, int bitDepth, int N, int16_t * C)
{
	int M = ComUtil::logarithm2(N);
	int log2TransformRange = 15;
	int bdShift = bitDepth + M + 10 - log2TransformRange;
	int mScalingFactor = 16;
	int matrixSize = N * N;

	size_t svewidth = svcntb();
	int maxNum = svewidth / sizeof(int32_t);
	if (matrixSize < maxNum)
		maxNum = matrixSize;

	svbool_t predicate = svwhilelt_b32_s32(0, maxNum);
	svint32_t data;

	svint32_t mult = svdup_n_s32(mScalingFactor);
	svint32_t mult2 = svdup_n_s32(g[QP % 6]);
	mult2 = svlsl_n_s32_x(predicate, mult2, QP / 6);
	svint32_t shiftOffset = svdup_n_s32(1 << (bdShift - 1));

	for (int i = 0; i < matrixSize; i = i + maxNum)
	{
		data = svld1sh_s32(predicate, A + i);

		data = svmul_s32_x(predicate, data, mult);
		data = svmul_s32_x(predicate, data, mult2);
		data = svadd_s32_x(predicate, data, shiftOffset);
		data = svasr_n_s32_x(predicate, data, bdShift);

		data = svsaturates16_s32_x(predicate, data);

		svst1h_s32(predicate, C + i, data);
	}
}

inline svint32_t TransformAndQuantizationSVE::svsaturates16_s32_x(svbool_t m, svint32_t a) { // saturate a 32 bits signed value to a 16 bit ssigned value ; SVE2 will have better ways to do this
	svint32_t r;
	svbool_t om;
	om = svcmpge_n_s32(m, a, 32767);
	r = svdup_n_s32_m(a, om, 32767);
	om = svcmple_n_s32(m, a, -32768);
	r = svdup_n_s32_m(r, om, -32768);
	return r;
}
#endif
