/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "EntropyModel.h"

EntropyModel::EntropyModel()
{
}


EntropyModel::~EntropyModel()
{
}

Bins::Bins()
{
	length = 0;
	bits = 0;

}

Bins::~Bins()
{
}

Value::Value()
{
	value = 0;
	possible = false;
}

Value::~Value()
{
}

CtxIdx::CtxIdx(syntaxElemEnum syntaxElemNoInit, unsigned const char *ctxTableInit, unsigned const char ctxIdxOffsetInit1, unsigned const char ctxIdxOffsetInit2, unsigned const char ctxIdxOffsetInit3) : syntaxElementNo(syntaxElemNoInit), ctxTable(ctxTableInit), ctxIdxOffset()
{
	ctxIdxOffset[0] = ctxIdxOffsetInit1;
	ctxIdxOffset[1] = ctxIdxOffsetInit2;
	ctxIdxOffset[2] = ctxIdxOffsetInit3;
}


ProbabilityModel::ProbabilityModel()
{
}

ProbabilityModel::ProbabilityModel(syntaxElemEnum syntaxElemNo, unsigned int ctxIndex, char slice_type, unsigned int sliceQpy, bool cabac_init_flag)
{
	unsigned int initType;
	unsigned char initValue;
	unsigned int slopeIdx;
	unsigned int offsetIdx;
	int preCtxState;

	int m;
	int n;

	int i;

	if (slice_type == 'I')
		initType = 0;
	else if (slice_type == 'P')
		initType = cabac_init_flag ? 2 : 1;
	else
		initType = cabac_init_flag ? 1 : 2;

	// Using defined context index offset which is static value for every syntax element and
	// calculated context increment (dynamic value for most syntax elements) initial value is
	// retrieved from the table

	for (i = 0; i < 39; i++)
	{
		if (ctxIdxOffset[i].syntaxElementNo == syntaxElemNo)
		{
			initValue = ctxIdxOffset[i].ctxTable[ctxIndex];
		}

	}

	slopeIdx = initValue >> 4;
	offsetIdx = initValue & 15;

	m = slopeIdx * 5 - 45;
	n = (offsetIdx << 3) - 16;

	preCtxState = ComUtil::clip3(1, 126, ((m * ComUtil::clip3(0, 51, sliceQpy)) >> 4) + n); //MC_todo it can happen that m is negative number and number subject to right shift is negative. It is not clear if specified right shift considers negative sign
	valMps = (preCtxState <= 63) ? 0 : 1;
	pStateIdx = valMps ? (preCtxState - 64) : (63 - preCtxState);
}






ProbabilityModelSelection::ProbabilityModelSelection()
{
	probabilityModel = new ProbabilityModel*[34];
	probabilityModel[0] = new ProbabilityModel[3];
	probabilityModel[1] = new ProbabilityModel[3];
	probabilityModel[2] = new ProbabilityModel[9];
	probabilityModel[3] = new ProbabilityModel[3];
	probabilityModel[4] = new ProbabilityModel[6];
	probabilityModel[5] = new ProbabilityModel[2];
	probabilityModel[6] = new ProbabilityModel[9];
	probabilityModel[7] = new ProbabilityModel[3];
	probabilityModel[8] = new ProbabilityModel[3];
	probabilityModel[9] = new ProbabilityModel[2];
	probabilityModel[10] = new ProbabilityModel[2];
	probabilityModel[11] = new ProbabilityModel[2];
	probabilityModel[12] = new ProbabilityModel[10];
	probabilityModel[13] = new ProbabilityModel[4];
	probabilityModel[14] = new ProbabilityModel[2];
	probabilityModel[15] = new ProbabilityModel[9];
	probabilityModel[16] = new ProbabilityModel[6];
	probabilityModel[17] = new ProbabilityModel[15];
	probabilityModel[18] = new ProbabilityModel[4];
	probabilityModel[19] = new ProbabilityModel[4];
	probabilityModel[20] = new ProbabilityModel[6];
	probabilityModel[21] = new ProbabilityModel[3];
	probabilityModel[22] = new ProbabilityModel[3];
	probabilityModel[23] = new ProbabilityModel[24];
	probabilityModel[24] = new ProbabilityModel[6];
	probabilityModel[25] = new ProbabilityModel[6];
	probabilityModel[26] = new ProbabilityModel[4];
	probabilityModel[27] = new ProbabilityModel[4];
	probabilityModel[28] = new ProbabilityModel[54];
	probabilityModel[29] = new ProbabilityModel[54];
	probabilityModel[30] = new ProbabilityModel[12];
	probabilityModel[31] = new ProbabilityModel[132];
	probabilityModel[32] = new ProbabilityModel[72];
	probabilityModel[33] = new ProbabilityModel[18];
}

ProbabilityModelSelection::~ProbabilityModelSelection()
{
		for (int i = 0; i < 34; i++)
		{
			delete[] probabilityModel[i];
		}
		delete[] probabilityModel;
}

void ProbabilityModelSelection::InitializeSelection(unsigned int SliceQpy, bool cabac_init_flag)
{
	for (int i = 0; i <= 2; i++)
		probabilityModel[0][i] = ProbabilityModel(sao_merge_left_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 2; i++)
		probabilityModel[1][i] = ProbabilityModel(sao_type_idx_luma, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 8; i++)
		probabilityModel[2][i] = ProbabilityModel(split_cu_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 2; i++)
		probabilityModel[3][i] = ProbabilityModel(cu_transquant_bypass_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 5; i++)
		probabilityModel[4][i] = ProbabilityModel(cu_skip_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 1; i++)
		probabilityModel[5][i] = ProbabilityModel(pred_mode_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 8; i++)
		probabilityModel[6][i] = ProbabilityModel(part_mode, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 2; i++)
		probabilityModel[7][i] = ProbabilityModel(prev_intra_luma_pred_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 2; i++)
		probabilityModel[8][i] = ProbabilityModel(intra_chroma_pred_mode, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 1; i++)
		probabilityModel[9][i] = ProbabilityModel(rqt_root_cbf, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 1; i++)
		probabilityModel[10][i] = ProbabilityModel(merge_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 1; i++)
		probabilityModel[11][i] = ProbabilityModel(merge_idx, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 9; i++)
		probabilityModel[12][i] = ProbabilityModel(inter_pred_idc, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 3; i++)
		probabilityModel[13][i] = ProbabilityModel(ref_idx_l0, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 1; i++)
		probabilityModel[14][i] = ProbabilityModel(mvp_l0_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 8; i++)
		probabilityModel[15][i] = ProbabilityModel(split_transform_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 5; i++)
		probabilityModel[16][i] = ProbabilityModel(cbf_luma, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 14; i++)
		probabilityModel[17][i] = ProbabilityModel(cbf_cb, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 3; i++)
		probabilityModel[18][i] = ProbabilityModel(abs_mvd_greater0_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 3; i++)
		probabilityModel[19][i] = ProbabilityModel(abs_mvd_greater1_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 5; i++)
		probabilityModel[20][i] = ProbabilityModel(cu_qp_delta_abs, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 2; i++)
		probabilityModel[21][i] = ProbabilityModel(cu_chroma_qp_offset_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 2; i++)
		probabilityModel[22][i] = ProbabilityModel(cu_chroma_qp_offset_idx, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 23; i++)
		probabilityModel[23][i] = ProbabilityModel(log2_res_scale_abs_plus1, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 5; i++)
		probabilityModel[24][i] = ProbabilityModel(res_scale_sign_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 5; i++)
		probabilityModel[25][i] = ProbabilityModel(transform_skip_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 3; i++)
		probabilityModel[26][i] = ProbabilityModel(explicit_rdpcm_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 3; i++)
		probabilityModel[27][i] = ProbabilityModel(explicit_rdpcm_dir_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 53; i++)
		probabilityModel[28][i] = ProbabilityModel(last_sig_coeff_x_prefix, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 53; i++)
		probabilityModel[29][i] = ProbabilityModel(last_sig_coeff_y_prefix, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 11; i++)
		probabilityModel[30][i] = ProbabilityModel(coded_sub_block_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 131; i++)
		probabilityModel[31][i] = ProbabilityModel(sig_coeff_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 71; i++)
		probabilityModel[32][i] = ProbabilityModel(coeff_abs_level_greater1_flag, i, 'I', SliceQpy, cabac_init_flag);

	for (int i = 0; i <= 17; i++)
		probabilityModel[33][i] = ProbabilityModel(coeff_abs_level_greater2_flag, i, 'I', SliceQpy, cabac_init_flag);
}

