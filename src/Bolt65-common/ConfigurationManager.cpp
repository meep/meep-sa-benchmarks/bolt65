/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ConfigurationManager.h"

void ConfigurationManager::checkExternalConfig(EncodingContext* enCtx, char* argv[], int argc)
{
	for (int i = 1; i < argc; i++)
	{
		if (argv[i] == std::string("--config") || argv[i] == std::string("-c"))
		{
			enCtx->externalConfig = true;
			enCtx->externalConfigFileName = argv[++i];
		}
	}
}

void ConfigurationManager::fetchConfigurationFromFile(EncodingContext* enCtx)
{
	string configFilepath = enCtx->externalConfigFileName;
	ifstream configFile(configFilepath);

	if (!configFile)
	{
		cout << "Error reading config file. File " << configFilepath << " does not exist" << endl;
		exit(EXIT_FAILURE);
	}
	else
	{
		cout << "Using external config file: " << configFilepath << endl;
	}

	string configLine;
	while (getline(configFile, configLine))
	{
		//remove all whitespaces
		//configLine.erase(remove_if(configLine.begin(), configLine.end(), isspace), configLine.end());
		replace(configLine.begin(), configLine.end(), '\t', ' ');
		replace(configLine.begin(), configLine.end(), '\r', ' ');

		string configName = configLine.substr(0, configLine.find(":"));
		string configValue = configLine.substr(configLine.find(":") + 1);

		int first, last;
		first = (int)configName.find_first_not_of(' ');
		last = (int)configName.find_last_not_of(' ');

		if (first == -1 || last == -1)
			continue;

		configName = configName.substr(first, last - first + 1);

		if (configName[0] == '#')
			continue;

		first = (int)configValue.find_first_not_of(' ');
		last = (int)configValue.find_last_not_of(' ');

		if (first == -1 || last == -1)
			continue;

		configValue = configValue.substr(first, last - first + 1);

		assignConfigAttribute(configName, configValue, enCtx);

	}

	configFile.close();

}

void ConfigurationManager::parseConsoleParameters(EncodingContext* enCtx, char* argv[], int argc)
{
	for (int i = 1; i < argc; i++)
	{
		enCtx->command = enCtx->command + " " + argv[i];
	}
	for (int i = 1; i < argc; i++)
	{
		if (argv[i] == std::string("--csv"))
		{
			enCtx->outputStatsToFile = true;
			enCtx->outputStatsFileName = argv[++i];
		}
		else if (argv[i] == std::string("--csv-tiles"))
		{
			enCtx->outputStatsTileToFile = true;
			enCtx->outputStatsTileFileName = argv[++i];
		}
		else if (argv[i] == std::string("--frames") || argv[i] == std::string("-f"))
			enCtx->numOfFrames = atoi(argv[++i]);
		else if (argv[i] == std::string("--fps"))
			enCtx->frameRate = atoi(argv[++i]);
		else if (argv[i] == std::string("--picWidth") || argv[i] == std::string("-w"))
			enCtx->width = atoi(argv[++i]);
		else if (argv[i] == std::string("--picHeight") || argv[i] == std::string("-h"))
			enCtx->height = atoi(argv[++i]);
		else if (argv[i] == std::string("--input") || argv[i] == std::string("-i"))
			enCtx->inputFilePath = argv[++i];
		else if (argv[i] == std::string("--output") || argv[i] == std::string("-o"))
			enCtx->outputFilePath = argv[++i];
		else if (argv[i] == std::string("--framerate") || argv[i] == std::string("-fr"))
			enCtx->frameRate = atoi(argv[++i]);
		else if (argv[i] == std::string("--qp") || argv[i] == std::string("-q"))
			enCtx->quantizationParameter = atoi(argv[++i]);
		else if (argv[i] == std::string("--bitnumber"))
			enCtx->bitNumber = atoi(argv[++i]);
		else if (argv[i] == std::string("--ctb-size"))
			enCtx->ctbLog2SizeY = atoi(argv[++i]);
		else if (argv[i] == std::string("--enable-deblocking-filter"))
			enCtx->disableDeblockingFilter = false;
		else if (argv[i] == std::string("--disable-deblocking-filter"))
			enCtx->disableDeblockingFilter = true;
		else if (argv[i] == std::string("--enable-sao-filter"))
			enCtx->saoEnabled = true;
		else if (argv[i] == std::string("--disable-sao-filter"))
			enCtx->saoEnabled = false;
		else if (argv[i] == std::string("--enable-smp"))
			enCtx->useSMP = true;
		else if (argv[i] == std::string("--disable-smp"))
			enCtx->useSMP = false;
		else if (argv[i] == std::string("--enable-amp"))
			enCtx->useAMP = true;
		else if (argv[i] == std::string("--disable-amp"))
			enCtx->useAMP = false;
		else if (argv[i] == std::string("--enable-intra-tu"))
			enCtx->useIntraTU = true;
		else if (argv[i] == std::string("--disable-intra-tu"))
			enCtx->useIntraTU = false;
		else if (argv[i] == std::string("--full-integer-search"))
			enCtx->searchAlgorithm = SearchAlgorithm::FullIntSearch;
		else if (argv[i] == std::string("--full-fractional-search"))
			enCtx->searchAlgorithm = SearchAlgorithm::FullFractionalSearch;
		else if (argv[i] == std::string("--three-step-search"))
			enCtx->searchAlgorithm = SearchAlgorithm::TSS;
		else if (argv[i] == std::string("--no-search"))
			enCtx->searchAlgorithm = SearchAlgorithm::ZeroVector;
		else if (argv[i] == std::string("--hevc-interpolation"))
			enCtx->interpolationAlgorithm = InterpolationAlgorithm::HEVCStandard;
		else if (argv[i] == std::string("--hevc-sf5"))
			enCtx->interpolationAlgorithm = InterpolationAlgorithm::HEVCSF5;
		else if (argv[i] == std::string("--avc-interpolation"))
			enCtx->interpolationAlgorithm = InterpolationAlgorithm::AVCStandard;
		else if (argv[i] == std::string("--bilinear-interpolation"))
			enCtx->interpolationAlgorithm = InterpolationAlgorithm::Bilinear;
		else if (argv[i] == std::string("--four-tap-interpolation"))
			enCtx->interpolationAlgorithm = InterpolationAlgorithm::FourTap;
		else if (argv[i] == std::string("--sad"))
			enCtx->blockMatching = BlockMatchingCriteria::SAD;
		else if (argv[i] == std::string("--satd"))
			enCtx->blockMatching = BlockMatchingCriteria::SATD;
		else if (argv[i] == std::string("--mse"))
			enCtx->blockMatching = BlockMatchingCriteria::MSE;
		else if (argv[i] == std::string("--search-area"))
			enCtx->searchArea = atoi(argv[++i]);
		else if (argv[i] == std::string("--calculate-psnr"))
			enCtx->calculatePSNR = true;
		else if (argv[i] == std::string("--calculate-bitrate"))
			enCtx->calculateBits = true;
		else if (argv[i] == std::string("--calculate-time"))
			enCtx->calculateTime = true;
		else if (argv[i] == std::string("--show-stats-per-frame"))
			enCtx->showStatsPerFrame = true;
		else if (argv[i] == std::string("--show-stats-per-tile"))
			enCtx->showStatsPerTile = true;
		else if (argv[i] == std::string("--gop"))
			enCtx->GOP = argv[++i];
		else if (argv[i] == std::string("--avx"))
			enCtx->AVX = true;
		else if (argv[i] == std::string("--neon"))
			enCtx->NEON = true;
		else if (argv[i] == std::string("--sve"))
			enCtx->SVE = true;
		else if (argv[i] == std::string("--epi-riscv"))
			enCtx->EPI_RISCV = true;
		else if (argv[i] == std::string("--epi-riscv-auto"))
			enCtx->EPI_RISCV_AUTO = true;
		else if (argv[i] == std::string("--mango-hw"))
			enCtx->MANGO_HW = true;
		else if (argv[i] == std::string("--mango-nup"))
			enCtx->MANGO_NUP = true;
		else if (argv[i] == std::string("--mango-peak"))
			enCtx->MANGO_PEAK = true;
		else if (argv[i] == std::string("--mango-gn"))
			enCtx->MANGO_GN = true;
		else if (argv[i] == std::string("--mango-gpu"))
			enCtx->MANGO_GPU = true;
		else if (argv[i] == std::string("--meep"))
			enCtx->MEEP = true;
		else if (argv[i] == std::string("--threads"))
			enCtx->threads = atoi(argv[++i]);
		else if (argv[i] == std::string("--input-buffer-size"))
			enCtx->inputBufSize = atoi(argv[++i]);
		else if (argv[i] == std::string("--output-buffer-size"))
			enCtx->outputBufSize = atoi(argv[++i]);
		else if (argv[i] == std::string("--dynamic-tiles"))
			enCtx->useDynamicTiles = true;
		else if (argv[i] == std::string("--tile-rows"))
		{
			enCtx->tilesEnabled = true;
			enCtx->tilesInRowAsString = argv[++i];
		}
		else if (argv[i] == std::string("--tile-columns"))
		{
			enCtx->tilesEnabled = true;
			enCtx->tilesInColumnAsString = argv[++i];
		}
		else if (argv[i] == std::string("--tile-lb-simple"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::Simple;
		else if (argv[i] == std::string("--tile-lb-max-min"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::MaxAndMin;
		else if (argv[i] == std::string("--tile-lb-sort-snake"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::SortSnake;
		else if (argv[i] == std::string("--tile-lb-sort-snake-bits"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::SortSnakeBits;
		else if (argv[i] == std::string("--tile-lb-shift-bits"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::ShiftBordersBits;
		else if (argv[i] == std::string("--tile-lb-shift-time"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::ShiftBordersTimes;
		else if (argv[i] == std::string("--tile-lb-shift-bits-search"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::ShiftBordersBitsSeachPoints;
		else if (argv[i] == std::string("--tile-lb-article"))
			enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::ArticleAlgorithm;
		else if (argv[i] == std::string("--tile-lb-interval"))
			enCtx->tileLoadBalancingInterval = atoi(argv[++i]);
		else if (argv[i] == std::string("--all-modes"))
			enCtx->testAllModes = true;
		else if (argv[i] == std::string("--config") || argv[i] == std::string("-c"))
		{
			enCtx->externalConfig = true;
			enCtx->externalConfigFileName = argv[++i];
		}
		else if (argv[i] == std::string("--clustered-tq"))
			enCtx->clusteredTQ = true;
		else
			cout << "WARNING: Unknown console parameter: " << argv[i] << endl;
	}
}

void ConfigurationManager::assignConfigAttribute(string configName, string configValue, EncodingContext* enCtx)
{
	if (configName == StringConstants::InputFile)
		enCtx->inputFilePath = configValue;
	else if (configName == StringConstants::OutputFile)
		enCtx->outputFilePath = configValue;
	else if (configName == StringConstants::NumberOfFrames)
		enCtx->numOfFrames = stoi(configValue);
	else if (configName == StringConstants::QP)
		enCtx->quantizationParameter = stoi(configValue);
	else if (configName == StringConstants::BitNumber)
		enCtx->bitNumber = stoi(configValue);
	else if (configName == StringConstants::HighestTid)
		enCtx->highestTid = stoi(configValue);
	else if (configName == StringConstants::CtbLog2SizeY)
		enCtx->ctbLog2SizeY = stoi(configValue);
	else if (configName == StringConstants::MinCbLog2SizeY)
		enCtx->minCbLog2SizeY = stoi(configValue);
	else if (configName == StringConstants::MinTbLog2SizeY)
		enCtx->minTbLog2SizeY = stoi(configValue);
	else if (configName == StringConstants::MaxTbLog2SizeY)
		enCtx->maxTbLog2SizeY = stoi(configValue);
	else if (configName == StringConstants::InputPictureWidth)
		enCtx->width = stoi(configValue);
	else if (configName == StringConstants::InputPictureHeight)
		enCtx->height = stoi(configValue);
	else if (configName == StringConstants::FrameRate)
		enCtx->frameRate = stoi(configValue);
	else if (configName == StringConstants::DPBSize)
		enCtx->dpbSize = stoi(configValue);
	else if (configName == StringConstants::GOP)
		enCtx->GOP = configValue;
	else if (configName == StringConstants::AMVPUseTemporalCandidates)
		enCtx->useTemporalAMVP = stoi(configValue) != 0;
	else if (configName == StringConstants::DeblockingFilter)
		enCtx->disableDeblockingFilter = stoi(configValue) == 0;
	else if (configName == StringConstants::SAOFilter)
		enCtx->saoEnabled = stoi(configValue) != 0;
	else if (configName == StringConstants::AMPEnabled)
		enCtx->useAMP = stoi(configValue) != 0;
	else if (configName == StringConstants::SMPEnabled)
		enCtx->useSMP = stoi(configValue) != 0;
	else if (configName == StringConstants::IntraTUEnabled)
		enCtx->useIntraTU = stoi(configValue) != 0;
	else if (configName == StringConstants::Log2MaxPicOrderLst)
		enCtx->log2MaxPicOrderLst = stoi(configValue);
	else if (configName == StringConstants::SearchAlgorithm)
		enCtx->searchAlgorithm = stoi(configValue);
	else if (configName == StringConstants::InterpolateAlgorithm)
		enCtx->interpolationAlgorithm = stoi(configValue);
	else if (configName == StringConstants::BlockMatching)
		enCtx->blockMatching = stoi(configValue);
	else if (configName == StringConstants::SearchArea)
		enCtx->searchArea = stoi(configValue);
	else if (configName == StringConstants::CalculatePSNR)
		enCtx->calculatePSNR = stoi(configValue) != 0;
	else if (configName == StringConstants::CalculateSSIM)
		enCtx->calculateSSIM = stoi(configValue) != 0;
	else if (configName == StringConstants::CalculateBitsPerFrame)
		enCtx->calculateBits = stoi(configValue) != 0;
	else if (configName == StringConstants::CalculateProcessingTime)
		enCtx->calculateTime = stoi(configValue) != 0;
	else if (configName == StringConstants::ShowStatisticsPerFrame)
		enCtx->showStatsPerFrame = stoi(configValue) != 0;
	else if (configName == StringConstants::ShowStatisticsPerTile)
		enCtx->showStatsPerTile = stoi(configValue) != 0;
	else if (configName == StringConstants::StatMode)
		enCtx->statMode = stoi(configValue) != 0;
	else if (configName == StringConstants::InputBufferSize)
		enCtx->inputBufSize = stoi(configValue);
	else if (configName == StringConstants::OutputBufferSize)
		enCtx->outputBufSize = stoi(configValue);
	if (configName == StringConstants::UsePolicy)
		enCtx->usePolicy = stoi(configValue) != 0;
	if (configName == StringConstants::PolicySocketHost)
		enCtx->policyHost = configValue;
	if (configName == StringConstants::PolicySocketPortNumber)
		enCtx->policyPort = stoi(configValue);
	if (configName == StringConstants::CSV)
	{
		enCtx->outputStatsToFile = true;
		enCtx->outputStatsFileName = configValue;
	}
	if (configName == StringConstants::CSVTiles)
	{
		enCtx->outputStatsTileToFile = true;
		enCtx->outputStatsTileFileName = configValue;
	}
	if (configName == StringConstants::AVX)
		enCtx->AVX = stoi(configValue) != 0;
	if (configName == StringConstants::NEON)
		enCtx->NEON = stoi(configValue) != 0;
	if (configName == StringConstants::SVE)
		enCtx->SVE = stoi(configValue) != 0;
	if (configName == StringConstants::SVE2)
		enCtx->SVE2 = stoi(configValue) != 0;
	if (configName == StringConstants::EPI_RISCV)
		enCtx->EPI_RISCV = stoi(configValue) != 0;
	if (configName == StringConstants::EPI_RISCV_AUTO)
		enCtx->EPI_RISCV_AUTO = stoi(configValue) != 0;
	if (configName == StringConstants::MANGO_HW)
		enCtx->MANGO_HW = stoi(configValue) != 0;
	if (configName == StringConstants::MANGO_NUP)
		enCtx->MANGO_NUP = stoi(configValue) != 0;
	if (configName == StringConstants::MANGO_PEAK)
		enCtx->MANGO_PEAK = stoi(configValue) != 0;
	if (configName == StringConstants::MANGO_GN)
		enCtx->MANGO_GN = stoi(configValue) != 0;
	if (configName == StringConstants::MANGO_GPU)
		enCtx->MANGO_GPU = stoi(configValue) != 0;
	if (configName == StringConstants::MEEP)
		enCtx->MEEP = stoi(configValue) != 0;
	if (configName == StringConstants::Threads)
		enCtx->threads = stoi(configValue);
	else if (configName == StringConstants::TilesEnabled)
		enCtx->tilesEnabled = stoi(configValue) != 0;
	else if (configName == StringConstants::DynamicTiles)
		enCtx->useDynamicTiles = stoi(configValue) != 0;
	else if (configName == StringConstants::TilesInColumn)
		enCtx->tilesInColumnAsString = configValue;
	else if (configName == StringConstants::TilesInRow)
		enCtx->tilesInRowAsString = configValue;
	else if (configName == StringConstants::TileLoadBalancingAlgorithm)
		enCtx->tileLoadBalancingAlgorithm = stoi(configValue);
	else if (configName == StringConstants::TileLoadBalancingInterval)
		enCtx->tileLoadBalancingInterval = stoi(configValue);
	else if (configName == StringConstants::ReuseDecodedData)
		enCtx->reuseData = stoi(configValue) != 0;
	else if (configName == StringConstants::TestAllModes)
		enCtx->testAllModes = stoi(configValue) != 0;
	else if (configName == StringConstants::InputFrameRate)
		enCtx->inputFrameRate = stoi(configValue);
	else if (configName == StringConstants::ReuseIntra)
		enCtx->reuseIntraMode = stoi(configValue);
	else if (configName == StringConstants::ReuseInter)
		enCtx->reuseInterMode = stoi(configValue);
	else if (configName == StringConstants::ReuseInterpolation)
		enCtx->reuseInterpolation = stoi(configValue);
	else if (configName == StringConstants::ReuseMode)
		enCtx->reuseMode = stoi(configValue);
	else if (configName == StringConstants::ReusePartmode)
		enCtx->reusePartMode = stoi(configValue);
	if (configName == StringConstants::ClusteredTQ)
		enCtx->clusteredTQ = stoi(configValue) != 0;
}

void ConfigurationManager::validateContextForEncoder(EncodingContext* enCtx)
{
	cout << "-------ENCODING CONFIGURATION---------" << endl;
	validatEncodingParameters(enCtx);
	validateGOPStructure(enCtx);
	validateIO(enCtx);
	validateParallelization(enCtx);
	validateBufferSizes(enCtx);
	validateEncodingFlags(enCtx);
	validateMotionEstimationConfiguration(enCtx);
	validateArchFlagsConfiguration(enCtx);
	validateTileConfiguration(enCtx);
	validatePolicy(enCtx);

	setFixedValues(enCtx);
	cout << "------------------------------------" << endl;
}

void ConfigurationManager::validateContextForDecoder(EncodingContext* decCtx)
{
	cout << "-------DECODING CONFIGURATION---------" << endl;
	validateIO(decCtx);
	validateParallelization(decCtx);
	validateArchFlagsConfiguration(decCtx);
	validateBufferSizes(decCtx);
	validateTileConfiguration(decCtx);

	setFixedValues(decCtx);
	cout << "------------------------------------" << endl;
}

void ConfigurationManager::validateContextForTranscoder(EncodingContext* enCtx, EncodingContext* decCtx)
{
	cout << "-------TRANSCODING CONFIGURATION (ENCODER + DECODER)---------" << endl;
	validateContextForEncoder(enCtx);
	validateContextForDecoder(decCtx);
	cout << "------------------------------------" << endl;
}

void ConfigurationManager::validateGOPStructure(EncodingContext* enCtx)
{
	if (enCtx->GOP == "")
	{
		cout << "No GOP specified. Using All Intra GOP." << endl;
		enCtx->GOP = "I";
	}
	else
	{
		int i;
		if (enCtx->GOP.compare("LD") == 0)
		{
			cout << " Using Low delay configuration" << endl;
		}
		else
		{
			for (i = 0; i < enCtx->GOP.size(); i++)
			{
				int type = toupper(enCtx->GOP[i]);
				if (type != StringConstants::FRAME_I && type != StringConstants::FRAME_P && type != StringConstants::FRAME_B)
				{
					cout << "Invalid GOP structure. Frame cannot be of type " << (char)type << " (Please use I, B or P frame type)" << endl;
					exit(EXIT_FAILURE);
				}
			}
		}
	}


}

void ConfigurationManager::validateTileConfiguration(EncodingContext* enCtx)
{
	if (enCtx->appType == Encoder || enCtx->appType == Transcoder)
	{
		if (enCtx->tilesEnabled)
		{
			cout << "Tiles enabled" << endl;

			int numberOfSeparatorsInColumn = (int)std::count(enCtx->tilesInColumnAsString.begin(), enCtx->tilesInColumnAsString.end(), ';');
			if (numberOfSeparatorsInColumn == 0)
			{
				enCtx->isUniform = true;
				enCtx->numberOfTilesInColumn = stoi(enCtx->tilesInColumnAsString);
				enCtx->columnWidths = new int[enCtx->numberOfTilesInColumn];
			}
			else
			{
				enCtx->isUniform = false;
				enCtx->numberOfTilesInColumn = numberOfSeparatorsInColumn + 1;
				enCtx->columnWidths = new int[enCtx->numberOfTilesInColumn];
				string substring = enCtx->tilesInColumnAsString;
				for (int i = 0; i < enCtx->numberOfTilesInColumn; i++)
				{
					enCtx->columnWidths[i] = stoi(substring.substr(0, substring.find(';')));
					substring = substring.substr(substring.find(';') + 1, substring.length());
				}
			}

			int numberOfSeparatorsInRow = (int)std::count(enCtx->tilesInRowAsString.begin(), enCtx->tilesInRowAsString.end(), ';');
			if (numberOfSeparatorsInRow == 0)
			{
				enCtx->isUniform = true;
				enCtx->numberOfTilesInRow = stoi(enCtx->tilesInRowAsString);
				enCtx->rowHeights = new int[enCtx->numberOfTilesInRow];
			}
			else
			{
				enCtx->isUniform = false;
				enCtx->numberOfTilesInRow = numberOfSeparatorsInRow + 1;
				enCtx->rowHeights = new int[enCtx->numberOfTilesInRow];
				string substring = enCtx->tilesInRowAsString;
				for (int i = 0; i < enCtx->numberOfTilesInRow; i++)
				{
					enCtx->rowHeights[i] = stoi(substring.substr(0, substring.find(';')));
					substring = substring.substr(substring.find(';') + 1, substring.length());
				}
			}

			if (enCtx->isUniform)
				cout << "Using uniform tiles" << endl;
			else
				cout << "Custom tile separation aplied" << endl;

			if (enCtx->numberOfTilesInRow == -1)
			{
				cout << "Number of tiles in a row should be defined" << endl;
				exit(EXIT_FAILURE);
			}
			else
			{
				cout << "Using " << enCtx->numberOfTilesInRow << " tiles in a row" << endl;

				if (enCtx->isUniform == false)
				{
					int overallCTUsInRow = 0;
					for (int i = 0; i < enCtx->numberOfTilesInRow; i++)
					{
						overallCTUsInRow += enCtx->rowHeights[i];
					}
					int neededCUTs = (int)ceil((double)enCtx->height / enCtx->ctbSize);
					if (overallCTUsInRow != neededCUTs)
					{
						cout << "Wrong configuration of TilesInRow property. Overal number of CTUs in frame does not match the sum of CTUs set in configuration" << endl;
						cout << "Needed number of CTUs defined: " << overallCTUsInRow << endl;
						cout << "Needed number of CTUs in rows: " << neededCUTs << endl;
						exit(EXIT_FAILURE);
					}
				}

			}
			if (enCtx->numberOfTilesInColumn == -1)
			{
				cout << "Number of tiles in a column should be defined" << endl;
				exit(EXIT_FAILURE);
			}
			else
			{
				cout << "Using " << enCtx->numberOfTilesInColumn << " tiles in a column" << endl;


				if (enCtx->isUniform == false)
				{
					int overallCTUsInColumn = 0;
					for (int i = 0; i < enCtx->numberOfTilesInColumn; i++)
					{
						overallCTUsInColumn += enCtx->columnWidths[i];
					}
					int neededCUTs = (int)ceil((double)enCtx->width / enCtx->ctbSize);
					if (overallCTUsInColumn != neededCUTs)
					{
						cout << "Wrong configuration of TilesInColumn property. Overal number of CTUs in frame does not match the sum of CTUs set in configuration" << endl;
						cout << "Needed number of CTUs defined: " << overallCTUsInColumn << endl;
						cout << "Needed number of CTUs in columns: " << neededCUTs << endl;
						exit(EXIT_FAILURE);
					}
				}
			}
			if (enCtx->numberOfTilesInRow == 1 && enCtx->numberOfTilesInColumn == 1)
			{
				cout << "Invalid number of tiles set (Number of tiles should not be 1x1!). Continuing process without tiles" << endl;
				enCtx->tilesEnabled = false;
			}

		}
		else
		{
			cout << "Tiles disabled" << endl;
		}
	}

	if (enCtx->tileLoadBalancingAlgorithm == -1)
	{
		enCtx->tileLoadBalancingAlgorithm = TileLoadBalancing::Simple;
		cout << "Default tile load balancing algorithm : Simple." << endl;
	}
	else
	{
		if (enCtx->tileLoadBalancingAlgorithm == TileLoadBalancing::Simple)
		{
			cout << "Tile load balancing algorithm: Simple." << endl;
		}
		else if (enCtx->tileLoadBalancingAlgorithm == TileLoadBalancing::MaxAndMin)
		{
			cout << "Tile load balancing algorithm: Combine max and min." << endl;
		}
		else if (enCtx->tileLoadBalancingAlgorithm == TileLoadBalancing::SortSnake)
		{
			cout << "Tile load balancing algorithm: Sort snake." << endl;
		}
		else if (enCtx->tileLoadBalancingAlgorithm == TileLoadBalancing::SortSnakeBits)
		{
			cout << "Tile load balancing algorithm: Sort snake by bits per tile." << endl;
		}
	}

	if (enCtx->tileLoadBalancingInterval == -1 || enCtx->tileLoadBalancingInterval == 0)
	{
		enCtx->tileLoadBalancingInterval = 1;

		if (enCtx->tileLoadBalancingInterval == 0)
			cout << "Wrong tile load balancing interval (cannot be zero) ! Switching to default" << endl;

		cout << "Default tile load balancing interval used: 1 (updating every frame)" << endl;
	}
	else
	{
		cout << "Tile load balancing interval is: " << enCtx->tileLoadBalancingInterval << endl;
	}

}

void ConfigurationManager::validateMotionEstimationConfiguration(EncodingContext* enCtx)
{
	if (enCtx->searchAlgorithm == -1)
	{
		enCtx->searchAlgorithm = SearchAlgorithm::TSS;
		enCtx->searchArea = 6;
		cout << "Default search algorithm in P frames: Three Step Search." << endl;
		cout << "Overriding search area: " << enCtx->searchArea << endl;

	}
	else
	{
		if (enCtx->searchAlgorithm == SearchAlgorithm::TSS)
		{
			cout << "Search algorithm in P frames: Three Step Search." << endl;
			if (enCtx->searchArea <= 0 && enCtx->searchArea % 6 != 0)
			{
				enCtx->searchArea = 6;
				cout << "Overriding search area: " << enCtx->searchArea << endl;
			}
			else
			{
				cout << "Search area used: " << enCtx->searchArea << endl;
			}

		}
		else if (enCtx->searchAlgorithm == SearchAlgorithm::FullIntSearch)
		{
			cout << "Search algorithm in P frames: Full integer Search." << endl;
			if (enCtx->searchArea == -1)
			{
				enCtx->searchArea = 6;
				cout << "No search area specified. Default search area used:  " << enCtx->searchArea << endl;
			}
			else
			{
				cout << "Search area used:  " << enCtx->searchArea << endl;
			}
		}
		else if (enCtx->searchAlgorithm == SearchAlgorithm::FullFractionalSearch)
		{
			cout << "Search algorithm in P frames: Full fractional Search." << endl;
			if (enCtx->searchArea == -1)
			{
				enCtx->searchArea = 2;
				cout << "No search area specified. Default search area used:  " << enCtx->searchArea << endl;
			}
			else
			{
				cout << "Search area used:  " << enCtx->searchArea << endl;
			}
		}
		else if (enCtx->searchAlgorithm == SearchAlgorithm::ZeroVector)
		{
			cout << "Search algorithm in P frames: No Search." << endl;
			enCtx->searchArea = 0;
			cout << "No search area for this search algorithm" << endl;
		}
		else if (enCtx->searchAlgorithm == SearchAlgorithm::TRANSWeightedMV)
		{
			if (enCtx->appType == Transcoder)
			{
				if (enCtx->reuseData)
				{
					cout << "Reusing data from decoded frame to find motion vectors. Weighted MV constructions is used!" << endl;
				}
				else
				{
					cout << "Selected motion estimation algorithm is only available when ReuseDecodedData flag is set to 1. Switching to TSS with search area = 6." << endl;
					enCtx->searchAlgorithm = TSS;
					enCtx->searchArea = 6;
				}
			}
			else
			{
				cout << "This search algorithm is only available for transcoder. Setting search algorithm to TSS with search area equal to 6" << endl;
				enCtx->searchAlgorithm = SearchAlgorithm::TSS;
				enCtx->searchArea = 6;
			}
		}
	}

	if (enCtx->interpolationAlgorithm == -1)
	{
		enCtx->interpolationAlgorithm = InterpolationAlgorithm::HEVCStandard;
		cout << "Default interpolation algorithm: Interpolation by HEVC standard." << endl;
	}
	else
	{
		if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::HEVCStandard)
		{
			cout << "Interpolation algorithm in search: HEVC standard." << endl;
		}
		else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::HEVCSF5)
		{
			cout << "Interpolation algorithm in search: HEVC standard with SF = 5." << endl;
		}
		else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::AVCStandard)
		{
			cout << "Interpolation algorithm in search: AVC standard." << endl;
		}
		else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::Bilinear)
		{
			cout << "Interpolation algorithm in search: Bilinear." << endl;
		}
		else if (enCtx->interpolationAlgorithm == InterpolationAlgorithm::FourTap)
		{
			cout << "Interpolation algorithm in search: Four tap." << endl;
		}
	}

	if (enCtx->blockMatching == -1)
	{
		enCtx->blockMatching = BlockMatchingCriteria::SAD;
		cout << "Default block matching criteria: Sum of absolute differences." << endl;
	}
	else
	{
		if (enCtx->blockMatching == BlockMatchingCriteria::SAD)
		{
			cout << "Block matching criteria: Sum of absolute differences." << endl;
		}
		else if (enCtx->blockMatching == BlockMatchingCriteria::SATD)
		{
			if (enCtx->useAMP || enCtx->useSMP)
			{
				cout << "Cannot use SATD for matrices that are not NxN, where N is power of 2. Using SAD instead." << endl;
				enCtx->blockMatching = BlockMatchingCriteria::SAD;
			}
			else
			{
				cout << "Block matching criteria: Sum of absolute transform differences (based on Fast Welsh-Hadamard transformation)." << endl;
			}
		}
		else if (enCtx->blockMatching == BlockMatchingCriteria::MSE)
		{
			cout << "Block matching criteria: Mean square error." << endl;
		}
	}

}

void ConfigurationManager::validateArchFlagsConfiguration(EncodingContext* enCtx)
{

	if (enCtx->AVX == false)
	{
		cout << "AVX disabled. (Could be inferred by compiler)" << endl;
	}
	else
	{
#ifdef __AVX2__
		cout << "AVX enabled." << endl;
#else
		cout << "AVX enabled, but not supported by architecture and/or compiler. Continuing without using AVX" << endl;
#endif
	}
	if (enCtx->NEON == false)
	{
		cout << "NEON disabled. (Could be inferred by compiler)" << endl;
	}
	else
	{
#ifdef __ARM_NEON
		cout << "NEON enabled." << endl;
#else
		cout << "NEON enabled, but not supported by architecture and/or compiler. Continuing without using NEON" << endl;
#endif


	}
	if (enCtx->SVE == false)
	{
		cout << "SVE disabled. (Could be inferred by compiler)" << endl;
	}
	else
	{
#ifdef __ARM_FEATURE_SVE
		cout << "SVE enabled." << endl;
#else
		cout << "SVE enabled, but not supported by architecture and/or compiler. Continuing without using SVE" << endl;
#endif


	}
	if (enCtx->EPI_RISCV == false)
	{
		cout << "EPI RISCV disabled." << endl;
	}
	else
	{
		cout << "EPI RISCV enabled." << endl;
	}

	if (enCtx->EPI_RISCV_AUTO == false)
	{
		cout << "EPI RISCV Autovectorization disabled." << endl;
	}
	else
	{
		cout << "EPI RISCV Autovectorization enabled." << endl;
	}

	if (enCtx->MANGO_GN == false)
	{
		cout << "Mango GN disabled." << endl;
	}
	else
	{
		cout << "Mango GN enabled." << endl;
	}

	if (enCtx->MANGO_NUP == false)
	{
		cout << "Mango NUP disabled." << endl;
	}
	else
	{
		cout << "Mango NUP enabled." << endl;
	}
	if (enCtx->MANGO_HW == false)
	{
		cout << "Mango HW disabled." << endl;
	}
	else
	{
		cout << "Mango HW enabled." << endl;
	}
	if (enCtx->MANGO_GPU == false)
	{
		cout << "Mango GPU disabled." << endl;
	}
	else
	{
		cout << "Mango GPU enabled." << endl;
	}
	if (enCtx->MANGO_PEAK == false)
	{
		cout << "Mango PEAK disabled." << endl;
	}
	else
	{
		cout << "Mango PEAK enabled." << endl;
	}

	if (enCtx->MEEP == false)
	{
		cout << "MEEP SA accelerator disabled." << endl;
	}
	else
	{
		cout << "MEEP SA accelerator enabled." << endl;
		cout << "NOTE: MEEP SA accelerator is only being used in ClusteredTQ mode. Make sure to enable ClusteredTQ in the configuration" << endl;
	}
}

void ConfigurationManager::validateEncodingFlags(EncodingContext* enCtx)
{
	if (enCtx->disableDeblockingFilter)
		cout << "Deblocking filter disabled." << endl;
	else
		cout << "Deblocking filter enabled." << endl;

	if (enCtx->saoEnabled == 1)
		cout << "SAO filter enabled." << endl;
	else
		cout << "SAO filter disabled." << endl;

	if (enCtx->useAMP == 1)
		cout << "Asymetric PUs enabled." << endl;
	else
		cout << "Asymetric PUs disabled." << endl;

	if (enCtx->useSMP == 1)
		cout << "Symetric PUs enabled." << endl;
	else
		cout << "Symetric PUs disabled." << endl;

	if (enCtx->useIntraTU == 1)
		cout << "Using Intra TUs." << endl;
	else
		cout << "Not using Intra TUs." << endl;

	if (enCtx->testAllModes)
		cout << "Evaluating both modes in P frames (INTRA and INTER)" << endl;
	else
		cout << "Evaluating only INTER mode in P frames" << endl;

	if (enCtx->clusteredTQ)
	{
		cout << "Calculating Transform and Quantization in CLUSTERED MODE" << endl;
		cout << "NOTE: Clustered mode is only available in P frames, and only in case when INTRA mode is not being tested" << endl;
	}
	else
		cout << "Calculating Transform and Quantization regularly" << endl;

}

void ConfigurationManager::validatEncodingParameters(EncodingContext* enCtx)
{
	

	if (enCtx->frameRate == -1)
	{
		enCtx->frameRate = 30;
		cout << "Framerate not specified. " << enCtx->frameRate << " will be used." << endl;
	}
	else
		cout << "Framerate: " << enCtx->frameRate << " fps" << endl;

	if (enCtx->width == -1)
	{
		cout << "Picture width is not specified." << endl;
		exit(EXIT_FAILURE);
	}
	else
	{
		cout << "Picture width: " << enCtx->width << " px" << endl;
	}

	if (enCtx->height == -1)
	{
		cout << "Picture height is not specified." << endl;
		exit(EXIT_FAILURE);
	}
	else
	{
		cout << "Picture height: " << enCtx->height << " px" << endl;
	}

	if (enCtx->quantizationParameter == -1)
	{
		enCtx->initQP = 24;
		enCtx->quantizationParameter = enCtx->initQP;

		cout << "Quantization parameter is not specified. Default QP: " << enCtx->initQP << endl;
	}
	else
	{
		enCtx->initQP = enCtx->quantizationParameter;

		if (enCtx->initQP < 1 || enCtx->initQP > 51)
		{
			cout << "Invalid value for Quantization parameter. QP has to be 0 - 51" << endl;
			exit(EXIT_FAILURE);
		}
		cout << "QP: " << enCtx->initQP << endl;
	}

	if (enCtx->bitNumber == -1)
	{
		enCtx->bitNumber = 8;
		cout << "Number of bits per pixel (BitNumber)is not specified. Using default bit number: " << enCtx->initQP << endl;
	}
	else
	{
		if (enCtx->bitNumber != 8)
		{
			cout << "HEVC standard supports only 8-bits or 10-bits per pixel. However Bolt65 currently supports only 8-bit. Continuing with 8-bit respresentation" << endl;
			enCtx->bitNumber = 8;
		}
		cout << "Bit number : " << enCtx->bitNumber << endl;
	}



	if (enCtx->ctbLog2SizeY == -1)
	{
		enCtx->ctbLog2SizeY = 6;
		cout << "Default value for CTB size used: " << enCtx->ctbLog2SizeY << endl;
		enCtx->ctbSize = 1 << enCtx->ctbLog2SizeY;
	}
	else
	{
		if (enCtx->ctbLog2SizeY < 4 || enCtx->ctbLog2SizeY > 6)
		{
			cout << "Invalid value for CTB size (ctbLog2SizeY). CTB can be 16x16, 32x32 or 64x64" << endl;
			exit(EXIT_FAILURE);
		}
		cout << "CTB size: " << enCtx->ctbLog2SizeY << endl;
		enCtx->ctbSize = 1 << enCtx->ctbLog2SizeY;
	}
}

void ConfigurationManager::validateBufferSizes(EncodingContext* enCtx)
{
	if (enCtx->inputBufSize == -1)
	{
		enCtx->inputBufSize = 30;
		cout << "Input buffer size not set. Using default value: " << enCtx->inputBufSize << endl;

	}
	else
	{
		cout << "Input buffer size: " << enCtx->inputBufSize << endl;
	}
	if (enCtx->outputBufSize == -1)
	{
		enCtx->outputBufSize = 10;
		cout << "Output buffer size not set. Using default value: " << enCtx->inputBufSize << endl;

	}
	else
	{
		cout << "Output buffer size: " << enCtx->outputBufSize << endl;
	}
}

void ConfigurationManager::validateIO(EncodingContext* enCtx)
{
	if (enCtx->inputFilePath == "")
	{
		cout << "No input specified." << endl;
		exit(EXIT_FAILURE);
	}
	else
	{
		cout << "Input file name: " << enCtx->inputFilePath << endl;
	}

	if (enCtx->outputFilePath == "")
	{
		cout << "No output specified." << endl;
		if (enCtx->appType == Encoder)
		{
			enCtx->outputFilePath = (enCtx->inputFilePath.substr(0, enCtx->inputFilePath.size() - 4)).append("_encoded.hevc");
		}
		else
		{
			enCtx->outputFilePath = (enCtx->inputFilePath.substr(0, enCtx->inputFilePath.size() - 5)).append("_decoded.yuv");
		}
		cout << "Default output file name: " << enCtx->outputFilePath << endl;
	}
	else
	{
		cout << "Output file name: " << enCtx->outputFilePath << endl;
	}
	if (enCtx->numOfFrames == -1)
	{
		enCtx->numOfFrames = 1;
		cout << "Numbers of frame not specified. " << enCtx->numOfFrames << " frames will be processed." << endl;
	}
	else
	{
		cout << "Number of frames to be processed: " << enCtx->numOfFrames << endl;

	}
}

void ConfigurationManager::validateParallelization(EncodingContext* enCtx)
{
	if (enCtx->threads < 2)
	{
		enCtx->threads = 1;
		cout << "Using 1 thread." << endl;
	}
	else
	{
		cout << "Using " << enCtx->threads << " threads." << endl;

	}
}

void ConfigurationManager::validatePolicy(EncodingContext* enCtx)
{

	if (enCtx->usePolicy && (!enCtx->statMode || !enCtx->calculatePSNR || !enCtx->calculateBits || !enCtx->calculateTime))
	{
		cout << endl;
		cout << "WARNING:" << endl;
		cout << "To use policy for dynamic configuration changes following options must be turned on: StatMode, CalculatePSNR, CalculateBitsPerFrame and CalculateProcessingTime" << endl;
		cout << "Policy will not be used!" << endl;
		cout << endl;

		enCtx->usePolicy = false;
	}
}

void ConfigurationManager::setFixedValues(EncodingContext* enCtx)
{
	//fixed values currently used:

	if (enCtx->appType == Encoder)
	{
		enCtx->dpbSize = 1;
		cout << "FIXED DPB size: " << enCtx->dpbSize << endl;
		enCtx->minCbLog2SizeY = 3;
		cout << "FIXED MinCbLog2SizeY: " << enCtx->minCbLog2SizeY << endl;
		enCtx->minTbLog2SizeY = 2;
		cout << "FIXED MinTbLog2SizeY: " << enCtx->minTbLog2SizeY << endl;
		enCtx->maxTbLog2SizeY = 5;
		cout << "FIXED MaxTbLog2SizeY: " << enCtx->maxTbLog2SizeY << endl;
		enCtx->highestTid = 0;
		cout << "FIXED HighestTid: " << enCtx->highestTid << endl;
		enCtx->useTemporalAMVP = 0;
		cout << "FIXED Not using temporal candidates in AMVP." << endl;
		enCtx->log2MaxPicOrderLst = 8;
		cout << "FIXED Log2MaxPicOrderLst: " << enCtx->log2MaxPicOrderLst << endl;
		enCtx->bitNumber = 8;
		cout << "FIXED Bit number: " << enCtx->bitNumber << endl;
		enCtx->highestTid = 0;
		cout << "FIXED Highest temporal ID: " << enCtx->highestTid << endl;
	}
	else if (enCtx->appType == Decoder)
	{
		enCtx->dpbSize = 1;
		cout << "FIXED DPB size: " << enCtx->dpbSize << endl;
	}
	else if (enCtx->appType == Transcoder)
	{
	}
}

ConfigurationManager::ConfigurationManager()
{
}

ConfigurationManager::~ConfigurationManager()
{
}
