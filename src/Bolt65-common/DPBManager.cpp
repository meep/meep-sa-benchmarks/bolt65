/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "DPBManager.h"
#include "DPBManager.h"



void DPBManager::AddFrameToDPB(Frame *frame)
{
	//TODO Additional logic for moving and deleting frames in DPB is required

	int i = frame->poc % dpb->DPBSize;

	if (dpb->frames[i] != nullptr)
		delete dpb->frames[i];

	dpb->frames[i] = frame;
}

void DPBManager::Init(int dpbSize)
{
	dpb = new DPB(dpbSize);
}

DPBManager::DPBManager()
{
}
void DPBManager::ClearDPB()
{
	delete dpb;
}


DPBManager::~DPBManager()
{
}

