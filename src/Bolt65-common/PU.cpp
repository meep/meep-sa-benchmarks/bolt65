/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "PU.h"



PU::PU(PB pbY, PB pbU, PB pbV)
{
	this->pbY = pbY;
	this->pbU = pbU;
	this->pbV = pbV;

	predictionMode = Unknown;
}

PU::PU()
{
}


PU::~PU()
{
}

void PU::sort() // sort vector
{
	for (int i = 1; i < transMappedPUs.size(); i++)
	{
		for (int j = 0; j < transMappedPUs.size() - i; j++)
		{
			if (transMappedPUs.at(j).first.RatioInCU < transMappedPUs.at(j + 1).first.RatioInCU)
			{
				std::pair<RatioCU, PU*> temp = transMappedPUs.at(j);
				transMappedPUs.at(j) = transMappedPUs.at(j + 1);
				transMappedPUs.at(j + 1) = temp;
			}
		}
	}
}
