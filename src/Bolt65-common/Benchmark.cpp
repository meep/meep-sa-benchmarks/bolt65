/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Benchmark.h"

Benchmark::Benchmark()
{
	enCtx = nullptr;
	picture = nullptr;
}

Benchmark::~Benchmark()
{

}

void Benchmark::Run(string path, EncodingContext *_enCtx)
{
	enCtx = _enCtx;
	ifstream benchFile;
	benchFile.open(path, ios::in | ios::binary);

	if (benchFile.is_open())
	{
		int FSize = 8294400;
		picture = new char[FSize];
		benchFile.read(picture, FSize);
		benchFile.close();
	}
	else 
	{
		cout << "Error opening input file for benchmark." << endl;
		exit(EXIT_FAILURE);
	}


	Bench_tnq();
}

void Benchmark::Bench_tnq()
{
	TQController tqc = TQController();
	tqc.Init(enCtx);

	int blockSize[4] = { 32, 16, 8, 4 };
	for (int bsIndex = 0; bsIndex < 4; ++bsIndex)
	{
		clock_t begin = clock();
		cout << "Benchmarking " << blockSize[bsIndex] << "x" << blockSize[bsIndex] << " blocks." << endl;
		int16_t* block = new int16_t[blockSize[bsIndex] * blockSize[bsIndex]];
		int16_t* transformedBlock = new int16_t[blockSize[bsIndex] * blockSize[bsIndex]];
		BlockPartition::get1dIntBlockByStartingIndex(picture, block, (0 * 2160 + 0), blockSize[bsIndex], 3840, 2160);

		for (int i = 0; i < 2160 - blockSize[bsIndex]; ++i)
		{
			for (int j = 0; j < 3840 - blockSize[bsIndex]; ++j)
			{
				bool isZeroMatrix = false;
				tqc.PerformTransformAndQuantization(22, block, blockSize[bsIndex], &isZeroMatrix, transformedBlock, 0, PredMode::MODE_INTRA);
			}
		}

		delete[] block;
		delete[] transformedBlock;
		clock_t end = clock();
		std::cout << " Processing time: " << (double)(end - begin) / CLOCKS_PER_SEC << " s " << endl;
	}
}
