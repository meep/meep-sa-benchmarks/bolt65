/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "LoadBalancing.h"

void LoadBalancing::performTileLoadBalancing(Frame* frame, int** tilesPerThread, int numOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, Statistics* stats, bool& addNewPPS)
{
	if (enCtx->tileLoadBalancingAlgorithm == Simple)
		simplestTilePerThreadLB(frame, tilesPerThread, enCtx->threads, numOfPartitions, referenceFrame, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == MaxAndMin)
		takeMaxAndMin(frame, tilesPerThread, enCtx->threads, numOfPartitions, referenceFrame, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == SortSnake)
		sortSnake(frame, tilesPerThread, enCtx->threads, numOfPartitions, referenceFrame, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == SortSnakeBits)
		sortSnakeBits(frame, tilesPerThread, enCtx->threads, numOfPartitions, referenceFrame, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == ShiftBordersBits)
		shiftBorderBits(frame, tilesPerThread, numOfPartitions, referenceFrame, enCtx, addNewPPS, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == ShiftBordersTimes)
		shiftBorderTimes(frame, tilesPerThread, numOfPartitions, referenceFrame, enCtx, addNewPPS, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == ShiftBordersBitsSeachPoints)
		shiftBorderBitsSearchPoints(frame, tilesPerThread, numOfPartitions, referenceFrame, enCtx, addNewPPS, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == ShiftBordersBitsSeachPoints2)
		shiftBorderBitsSearchPoints2(frame, tilesPerThread, numOfPartitions, referenceFrame, enCtx, addNewPPS, stats);
	else if (enCtx->tileLoadBalancingAlgorithm == ArticleAlgorithm)
		articleAlgorithm(frame, tilesPerThread, numOfPartitions, referenceFrame, enCtx, addNewPPS, stats);
	else
		simplestTilePerThreadLB(frame, tilesPerThread, enCtx->threads, numOfPartitions, referenceFrame, stats);
}


/*
Example: tiles are sorted only by tileId (4x4 tiles on 4 threads)
tileId:								0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
associated threadId:				0 1	2 3 0 1 2 3 0 1 2  3  0  1  2  3

THis is done only on the first frame because every frame has same arragment of tiles
*/
void LoadBalancing::simplestTilePerThreadLB(Frame* frame, int** tilesPerThread, int numOfThreads, int numOfPartitions, Frame* referenceFrame, Statistics* stats)
{
	if (frame->poc == 0)
	{
		for (int i = 0; i < numOfThreads; i++)
		{
			tilesPerThread[i] = new int[numOfPartitions + 1];
			for (int j = 0; j < numOfPartitions + 1; j++)
			{
				tilesPerThread[i][j] = -1;
			}
		}
	}

	int* counter = new int[numOfThreads] { 0 };
	float* bitPerThread = new float[numOfThreads] { 0.0 };

	for (int j = 0; j < numOfPartitions; j++)
	{
		int threadId = j % numOfThreads;
		tilesPerThread[threadId][counter[threadId]++] = j;

		if (frame->poc != 0 && stats->showTestTileStats)
			bitPerThread[threadId] += referenceFrame->tiles[j]->bitSize;
	}

	if (frame->poc != 0 && stats->showTestTileStats)
		stats->calculateTileLBEfficiency(bitPerThread, numOfThreads);

	delete[] counter;
	delete[] bitPerThread;
}

/*
Example: tiles are sorted by processing time - first one is with minimum processing time (4x4 tiles on 4 threads)
tileId:								0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
associated threadId:				0 0 1 1 2 2 3 3 3 3 2  2  1  1  0  0
*/
void LoadBalancing::takeMaxAndMin(Frame* frame, int** tilesPerThread, int numOfThreads, int numOfPartitions, Frame* referenceFrame, Statistics* stats)
{

	if (frame->poc == 0)
	{
		simplestTilePerThreadLB(frame, tilesPerThread, numOfThreads, numOfPartitions, referenceFrame, stats);
	}
	else
	{
		reinitializeTilesPerThread(tilesPerThread, numOfThreads, numOfPartitions);

		vector<pair<float, int> > processingTimes;
		for (int i = 0; i < numOfPartitions; i++)
			processingTimes.push_back(make_pair(referenceFrame->tiles[i]->proccesingTime, i));

		sort(processingTimes.begin(), processingTimes.end());


		float* threadTime = new float[numOfThreads] { 0.0 };
		int* counter = new int[numOfThreads] { 0 };
		int threadId = 0;

		for (int j = 0; j < numOfPartitions; j++)
		{
			if (j < numOfPartitions / 2)
				threadId = j / (numOfThreads / 2);
			else
				threadId = (numOfPartitions - 1 - j) / (numOfThreads / 2);

			//just to make sure it works for all numbers of tiles, this option does not make sense anyway if not 4x4 on 4 threads configuration
			threadId = threadId % numOfThreads;

			tilesPerThread[threadId][counter[threadId]++] = processingTimes[j].second;
			if (stats->showTestTileStats)
				threadTime[threadId] += processingTimes[j].first;
		}

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(threadTime, numOfThreads);

		delete[] counter;
		delete[] threadTime;
	}

}

/*
Example: tiles are sorted by processing time - first one is with minimum processing time (4x4 tiles on 4 threads)
tileId:								0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
associated threadId:				0 1 2 3 3 2 1 0 0 1 2  3  3  2  1  0
*/
void LoadBalancing::sortSnake(Frame* frame, int** tilesPerThread, int numOfThreads, int numOfPartitions, Frame* referenceFrame, Statistics* stats)
{

	if (frame->poc == 0)
	{
		simplestTilePerThreadLB(frame, tilesPerThread, numOfThreads, numOfPartitions, referenceFrame, stats);
	}
	else
	{
		reinitializeTilesPerThread(tilesPerThread, numOfThreads, numOfPartitions);

		vector<pair<float, int> > processingTimes;
		for (int i = 0; i < numOfPartitions; i++)
			processingTimes.push_back(make_pair(referenceFrame->tiles[i]->proccesingTime, i));

		sort(processingTimes.begin(), processingTimes.end());


		int* counter = new int[numOfThreads] { 0 };
		float* threadTime = new float[numOfThreads] { 0.0 };
		int threadId = 0;



		for (int j = 0; j < numOfPartitions; j++)
		{
			if ((j / numOfThreads) % 2 == 0)
				threadId = j % numOfThreads;
			else
				threadId = (numOfThreads - 1) - j % numOfThreads;

			tilesPerThread[threadId][counter[threadId]++] = processingTimes[j].second;

			if (stats->showTestTileStats)
				threadTime[threadId] += processingTimes[j].first;

		}
		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(threadTime, numOfThreads);

		delete[] counter;
		delete[] threadTime;

	}

}

/*
Example: tiles are sorted by number of bits - first one is with minimum bits (4x4 tiles on 4 threads)
tileId:								0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
associated threadId:				0 1 2 3 3 2 1 0 0 1 2  3  3  2  1  0
*/
void LoadBalancing::sortSnakeBits(Frame* frame, int** tilesPerThread, int numOfThreads, int numOfPartitions, Frame* referenceFrame, Statistics* stats)
{

	if (frame->poc == 0)
	{
		simplestTilePerThreadLB(frame, tilesPerThread, numOfThreads, numOfPartitions, referenceFrame, stats);
	}
	else
	{
		reinitializeTilesPerThread(tilesPerThread, numOfThreads, numOfPartitions);

		vector<pair<long long, int> > bits;
		for (int i = 0; i < numOfPartitions; i++)
			bits.push_back(make_pair(referenceFrame->tiles[i]->bitSize, i));

		sort(bits.begin(), bits.end());


		int* counter = new int[numOfThreads] { 0 };
		float* bitPerThread = new float[numOfThreads] { 0.0 };
		int threadId = 0;



		for (int j = 0; j < numOfPartitions; j++)
		{
			if ((j / numOfThreads) % 2 == 0)
				threadId = j % numOfThreads;
			else
				threadId = (numOfThreads - 1) - j % numOfThreads;

			tilesPerThread[threadId][counter[threadId]++] = bits[j].second;

			if (stats->showTestTileStats)
				bitPerThread[threadId] += bits[j].first;

		}

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(bitPerThread, numOfThreads);

		delete[] counter;
		delete[] bitPerThread;

	}

}

void  LoadBalancing::shiftBorderBitsSearchPoints(Frame* frame, int** tilesPerThread, int numOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, bool& addNewPPS, Statistics* stats)
{


	if (numOfPartitions != enCtx->threads)
	{
		cout << endl << "For this type of load balancing, for now, number of tiles has to be same as number of threads" << endl;
		exit(EXIT_FAILURE);
	}

	if (frame->poc < 2)
	{
		//default situation...each tile to thread..number of threads should be equal to number of number of Tiles
		for (int i = 0; i < enCtx->threads; i++)
		{
			tilesPerThread[i] = new int[numOfPartitions + 1];
			tilesPerThread[i][0] = i;
			tilesPerThread[i][1] = -1;
		}
	}
	else if (frame->poc == 1 || frame->poc % enCtx->tileLoadBalancingInterval == 0)
	{
		/*First version*/
		long long* rowSums = new long long[enCtx->numberOfTilesInRow] { 0 };
		long long* columnSums = new long long[enCtx->numberOfTilesInColumn] { 0 };

		float* sumPerThread = new float[enCtx->threads] { 0.0 };

		float sumBits = 0;
		float sumSPs = 0;
		for (int i = 0; i < numOfPartitions; i++)
		{
			sumBits += referenceFrame->tiles[i]->bitSize;
			sumSPs += referenceFrame->tiles[i]->searchPoints;
		}

		double qpCoeff = 0.0;
		if (enCtx->quantizationParameter == 22)
			qpCoeff = 0.4;
		else if (enCtx->quantizationParameter == 27)
			qpCoeff = 0.6;
		else if (enCtx->quantizationParameter == 32)
			qpCoeff = 0.8;
		else if (enCtx->quantizationParameter == 37)
			qpCoeff = 1.3;

		double spCoeff = qpCoeff / (sumSPs / sumBits);
		//spCoeff = 1.00;

		//float toEqualCoeff = 1.00 / (sumSPs / sumBits);
		//float spCoeff = toEqualCoeff * 3;

		for (int i = 0; i < numOfPartitions; i++)
		{
			/*Overall cost = Cost(entropy) + cost(scanning) + cost(prediction)
			Cost (prediction) = n (SPs)
			Cost (scanning) = n (SPs)
			Cost (entropy) = n (bits)

			Overall cost = C1 * n (bits) + C2 * n (SPs)
			*/

			double spCostShare = referenceFrame->tiles[i]->searchPoints / sumSPs;
			double bitsCostShare = referenceFrame->tiles[i]->bitSize / sumBits;

			int cost = (int)(qpCoeff * spCostShare + bitsCostShare) * 1000;

			rowSums[(int)(i / enCtx->numberOfTilesInColumn)] += cost;
			columnSums[(int)(i % enCtx->numberOfTilesInColumn)] += cost;

			if (stats->showTestTileStats)
				sumPerThread[i] += cost;
		}


		//adapt vertical lines
		for (int i = 0; i < enCtx->numberOfTilesInColumn - 1; i++)
		{
			int largerIndex = columnSums[i] >= columnSums[i + 1] ? i : i + 1;
			int columnDiff = (int)abs(columnSums[i] - columnSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				int newDiff = (int)(columnDiff - 2 * columnSums[largerIndex] / enCtx->columnWidths[largerIndex]);
				if (newDiff > 0)
				{
					ctuShift++;
					columnDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < columnDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->columnWidths[i] -= ctuShift;
				enCtx->columnWidths[i + 1] += ctuShift;
			}
			else
			{
				enCtx->columnWidths[i] += ctuShift;
				enCtx->columnWidths[i + 1] -= ctuShift;
			}
		}

		//adapt horizontal lines
		for (int i = 0; i < enCtx->numberOfTilesInRow - 1; i++)
		{
			int largerIndex = rowSums[i] >= rowSums[i + 1] ? i : i + 1;
			int rowDiff = (int)abs(rowSums[i] - rowSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				int newDiff = (int)(rowDiff - 2 * rowSums[largerIndex] / enCtx->rowHeights[largerIndex]);
				if (newDiff > 0)
				{
					ctuShift++;
					rowDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < rowDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->rowHeights[i] -= ctuShift;
				enCtx->rowHeights[i + 1] += ctuShift;
			}
			else
			{
				enCtx->rowHeights[i] += ctuShift;
				enCtx->rowHeights[i + 1] -= ctuShift;
			}
		}

		enCtx->isUniform = false;
		enCtx->tileChanged = true;
		addNewPPS = true;

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(sumPerThread, enCtx->threads);

		delete[] rowSums;
		delete[] columnSums;
		delete[] sumPerThread;
	}

}

void LoadBalancing::shiftBorderBitsSearchPoints2(Frame* frame, int** tilesPerThread, int numOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, bool& addNewPPS, Statistics* stats)
{
	double spCoeff = 1.85;
	if (numOfPartitions != enCtx->threads)
	{
		cout << endl << "For this type of load balancing, for now, number of tiles has to be same as number of threads" << endl;
		exit(EXIT_FAILURE);
	}

	if (frame->poc < 2)
	{
		//default situation...each tile to thread..number of threads should be equal to number of number of Tiles
		for (int i = 0; i < enCtx->threads; i++)
		{
			tilesPerThread[i] = new int[numOfPartitions + 1];
			tilesPerThread[i][0] = i;
			tilesPerThread[i][1] = -1;
		}
	}
	else if (frame->poc == 1 || frame->poc % enCtx->tileLoadBalancingInterval == 0)
	{
		long long* rowSums = new long long[enCtx->numberOfTilesInRow] { 0 };
		long long* columnSums = new long long[enCtx->numberOfTilesInColumn] { 0 };

		int* ctuColumnSum = new int[frame->numberOfCTUsInRow];
		int* ctuRowSum = new int[frame->numberOfCTUsInColumn];

		float* sumPerThread = new float[enCtx->threads] { 0.0 };
		int sumAll = 0;

		for (int i = 0; i < numOfPartitions; i++)
		{
			int value = (int)(referenceFrame->tiles[i]->bitSize + spCoeff * referenceFrame->tiles[i]->searchPoints);

			rowSums[i / enCtx->numberOfTilesInColumn] += value;
			columnSums[i % enCtx->numberOfTilesInColumn] += value;
			sumAll += value;

			if (stats->showTestTileStats)
				sumPerThread[i] += value;
		}

		//adapt vertical lines
		int ctuColumnIndex = 0;
		for (int i = 0; i < enCtx->numberOfTilesInColumn; i++)
		{
			for (int j = 0; j < enCtx->columnWidths[i]; j++)
			{
				ctuColumnSum[ctuColumnIndex] = (int)(columnSums[i] / enCtx->columnWidths[i]);
				ctuColumnIndex++;
			}
		}

		int idealValue = sumAll / enCtx->numberOfTilesInRow;
		int newSum = 0;
		int startingCTUColumn = 0;
		for (int i = 0; i < enCtx->numberOfTilesInColumn; i++)
		{
			int tileSum = 0;
			int tileColumns = 0;

			if (i == enCtx->numberOfTilesInColumn - 1)
			{
				enCtx->columnWidths[i] = frame->numberOfCTUsInRow - startingCTUColumn;
			}
			else
			{
				for (int j = startingCTUColumn; j < frame->numberOfCTUsInRow; j++)
				{
					tileSum += ctuColumnSum[j];
					tileColumns++;
					if (idealValue - tileSum < ctuColumnSum[j + 1])
					{
						startingCTUColumn = j + 1;
						if ((tileSum + ctuColumnSum[j + 1] - idealValue) < (idealValue - tileSum))
						{
							tileColumns++;
							startingCTUColumn++;
						}

						break;
					}
				}
				enCtx->columnWidths[i] = tileColumns;
			}
		}

		//adapt horizontal lines
		int ctuRowIndex = 0;
		for (int i = 0; i < enCtx->numberOfTilesInRow; i++)
		{
			for (int j = 0; j < enCtx->rowHeights[i]; j++)
			{
				ctuRowSum[ctuRowIndex] = (int)(rowSums[i] / enCtx->rowHeights[i]);
				ctuRowIndex++;
			}
		}

		idealValue = sumAll / enCtx->numberOfTilesInColumn;
		newSum = 0;
		int startingCTURow = 0;
		for (int i = 0; i < enCtx->numberOfTilesInRow; i++)
		{
			int tileSum = 0;
			int tileRows = 0;

			if (i == enCtx->numberOfTilesInRow - 1)
			{
				enCtx->rowHeights[i] = frame->numberOfCTUsInColumn - startingCTURow;
			}
			else
			{
				for (int j = startingCTURow; j < frame->numberOfCTUsInColumn; j++)
				{
					tileSum += ctuRowSum[j];
					tileRows++;
					if (idealValue - tileSum < ctuRowSum[j + 1])
					{
						startingCTURow = j + 1;
						if ((tileSum + ctuRowSum[j + 1] - idealValue) < (idealValue - tileSum))
						{
							tileRows++;
							startingCTURow++;
						}

						break;
					}
				}
				enCtx->rowHeights[i] = tileRows;
			}

		}

		enCtx->isUniform = false;
		enCtx->tileChanged = true;
		addNewPPS = true;

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(sumPerThread, enCtx->threads);

		delete[] rowSums;
		delete[] columnSums;
		delete[] ctuColumnSum;
		delete[] ctuRowSum;
		delete[] sumPerThread;


	}
}

void  LoadBalancing::articleAlgorithm(Frame* frame, int** tilesPerThread, int numOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, bool& addNewPPS, Statistics* stats)
{


	if (numOfPartitions != enCtx->threads)
	{
		cout << endl << "For this type of load balancing, for now, number of tiles has to be same as number of threads" << endl;
		exit(EXIT_FAILURE);
	}

	if (frame->poc < 2)
	{
		//default situation...each tile to thread..number of threads should be equal to number of number of Tiles
		for (int i = 0; i < enCtx->threads; i++)
		{
			tilesPerThread[i] = new int[numOfPartitions + 1];
			tilesPerThread[i][0] = i;
			tilesPerThread[i][1] = -1;
		}
	}
	else if (frame->poc % enCtx->tileLoadBalancingInterval == 0 && referenceFrame->sliceType != I)
	{
		/*First version*/
		long long* rowSums = new long long[enCtx->numberOfTilesInRow] { 0 };
		long long* columnSums = new long long[enCtx->numberOfTilesInColumn] { 0 };

		/*long long *rowSumsTest = new long long[enCtx->numberOfTilesInRow]{ 0 };
		long long *columnSumsTest = new long long[enCtx->numberOfTilesInColumn]{ 0 };*/

		float* sumPerThread = new float[enCtx->threads] { 0.0 };

		float sumBits = 0;
		float sumTransforms = 0;
		float sumPredictions = 0;
		float sumBlocks = 0;

		int bCoeff = 10600000;
		int tCoeff = 39300000;
		int pCoeff = 854568000;
		int blCoeff = 5823000;

		/*int bCoeff = 9361430;
		int tCoeff = 39024625;
		int pCoeff = 774571238;
		int blCoeff = 6197271;*/

		float bitCoeff = 1, transformCoeff = 0, predictionCoeff = 0, blockCoeff = 0;

		for (int i = 0; i < numOfPartitions; i++)
		{
			for (int j = 0; j < referenceFrame->tiles[i]->numberOfCTUs; j++)
			{
				referenceFrame->tiles[i]->transformCost += referenceFrame->tiles[i]->ctuTree[j]->transformCost;
				referenceFrame->tiles[i]->predictionCost += referenceFrame->tiles[i]->ctuTree[j]->predictionCost;
				referenceFrame->tiles[i]->blockCost += referenceFrame->tiles[i]->ctuTree[j]->blockCost;
			}
			sumBlocks += referenceFrame->tiles[i]->blockCost;
			sumPredictions += referenceFrame->tiles[i]->predictionCost;
			sumTransforms += referenceFrame->tiles[i]->transformCost;
			sumBits += referenceFrame->tiles[i]->bitSize;
		}


		for (int i = 0; i < numOfPartitions; i++)
		{
			/*float bitsCostShare = (float)referenceFrame->tiles[i]->bitSize / sumBits;
			float transformCostShare = (float)referenceFrame->tiles[i]->transformCost / sumTransforms;
			float predictionCostShare = (float)referenceFrame->tiles[i]->predictionCost / sumPredictions;
			float blockCostShare = (float)referenceFrame->tiles[i]->blockCost / sumBlocks;*/

			//int cost = (bitCoeff * bitsCostShare + transformCoeff * transformCostShare + predictionCoeff * predictionCostShare + blockCoeff * blockCostShare) * 1000000;
			int cost = (int)(((float)referenceFrame->tiles[i]->bitSize / bCoeff)
				+ ((float)referenceFrame->tiles[i]->transformCost / tCoeff)
				+ ((float)referenceFrame->tiles[i]->predictionCost / pCoeff)
				+ ((float)referenceFrame->tiles[i]->blockCost / blCoeff)
				) * 1000000000;

			/*int costTest = ((float)referenceFrame->tiles[i]->proccesingTime) * 10000;
			cost = costTest;*/

			rowSums[i / enCtx->numberOfTilesInColumn] += cost;
			columnSums[i % enCtx->numberOfTilesInColumn] += cost;

			/*rowSumsTest[i / enCtx->numberOfTilesInColumn] += costTest;
			columnSumsTest[i % enCtx->numberOfTilesInColumn] += costTest;*/

			if (stats->showTestTileStats)
				sumPerThread[i] += cost;
		}

		//adapt vertical lines
		for (int i = 0; i < enCtx->numberOfTilesInColumn - 1; i++)
		{
			int largerIndex = columnSums[i] >= columnSums[i + 1] ? i : i + 1;
			int columnDiff = (int)abs(columnSums[i] - columnSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				int newDiff = (int)(columnDiff - 2 * columnSums[largerIndex] / enCtx->columnWidths[largerIndex]);
				if (newDiff > 0)
				{
					ctuShift++;
					columnDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < columnDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->columnWidths[i] -= ctuShift;
				enCtx->columnWidths[i + 1] += ctuShift;
			}
			else
			{
				enCtx->columnWidths[i] += ctuShift;
				enCtx->columnWidths[i + 1] -= ctuShift;
			}
		}

		//adapt horizontal lines
		for (int i = 0; i < enCtx->numberOfTilesInRow - 1; i++)
		{
			int largerIndex = rowSums[i] >= rowSums[i + 1] ? i : i + 1;
			int rowDiff = (int)abs(rowSums[i] - rowSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				int newDiff = (int)(rowDiff - 2 * rowSums[largerIndex] / enCtx->rowHeights[largerIndex]);
				if (newDiff > 0)
				{
					ctuShift++;
					rowDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < rowDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->rowHeights[i] -= ctuShift;
				enCtx->rowHeights[i + 1] += ctuShift;
			}
			else
			{
				enCtx->rowHeights[i] += ctuShift;
				enCtx->rowHeights[i + 1] -= ctuShift;
			}
		}

		enCtx->isUniform = false;
		enCtx->tileChanged = true;
		addNewPPS = true;

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(sumPerThread, enCtx->threads);

		delete[] rowSums;
		delete[] columnSums;
		delete[] sumPerThread;
	}

}


//NOT YET IMPLEMENTED !!!!
void  LoadBalancing::transcodingBalance(Frame* decFrame, int** tilesPerThread, int decNumOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, bool& addNewPPS, Statistics* stats)
{
	long long* rowSums = new long long[enCtx->numberOfTilesInRow] { 0 };
	long long* columnSums = new long long[enCtx->numberOfTilesInColumn] { 0 };

	int numberOfAvailableCores = thread::hardware_concurrency();

	//if decoded frame is I frame, use just bits
	if (decFrame->sliceType == I)
	{

	}
	else
	{

	}

	//adapt vertical lines
	for (int i = 0; i < enCtx->numberOfTilesInColumn - 1; i++)
	{
		int largerIndex = columnSums[i] >= columnSums[i + 1] ? i : i + 1;
		int columnDiff = (int)abs(columnSums[i] - columnSums[i + 1]);

		bool stop = false;
		int ctuShift = 0;
		while (stop != true)
		{
			int newDiff = (int)(columnDiff - 2 * columnSums[largerIndex] / enCtx->columnWidths[largerIndex]);
			if (newDiff > 0)
			{
				ctuShift++;
				columnDiff = newDiff;
			}
			else
			{
				stop = true;
				if (abs(newDiff) < columnDiff)
					ctuShift++;
			}
		}

		if (largerIndex == i)
		{
			enCtx->columnWidths[i] -= ctuShift;
			enCtx->columnWidths[i + 1] += ctuShift;
		}
		else
		{
			enCtx->columnWidths[i] += ctuShift;
			enCtx->columnWidths[i + 1] -= ctuShift;
		}
	}

	//adapt horizontal lines
	for (int i = 0; i < enCtx->numberOfTilesInRow - 1; i++)
	{
		int largerIndex = rowSums[i] >= rowSums[i + 1] ? i : i + 1;
		int rowDiff = (int)abs(rowSums[i] - rowSums[i + 1]);

		bool stop = false;
		int ctuShift = 0;
		while (stop != true)
		{
			int newDiff = (int)(rowDiff - 2 * rowSums[largerIndex] / enCtx->rowHeights[largerIndex]);
			if (newDiff > 0)
			{
				ctuShift++;
				rowDiff = newDiff;
			}
			else
			{
				stop = true;
				if (abs(newDiff) < rowDiff)
					ctuShift++;
			}
		}

		if (largerIndex == i)
		{
			enCtx->rowHeights[i] -= ctuShift;
			enCtx->rowHeights[i + 1] += ctuShift;
		}
		else
		{
			enCtx->rowHeights[i] += ctuShift;
			enCtx->rowHeights[i + 1] -= ctuShift;
		}
	}

	enCtx->isUniform = false;
	enCtx->tileChanged = true;
	addNewPPS = true;

	delete[] rowSums;
	delete[] columnSums;
}

void  LoadBalancing::shiftBorderTimes(Frame* frame, int** tilesPerThread, int numOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, bool& addNewPPS, Statistics* stats)
{
	if (numOfPartitions != enCtx->threads)
	{
		cout << endl << "For this type of load balancing, for now, number of tiles has to be same as number of threads" << endl;
		exit(EXIT_FAILURE);
	}

	if (frame->poc == 0)
	{
		//default situation...each tile to thread..number of threads should be equal to number of number of Tiles
		for (int i = 0; i < enCtx->threads; i++)
		{
			tilesPerThread[i] = new int[numOfPartitions + 1];
			tilesPerThread[i][0] = i;
			tilesPerThread[i][1] = -1;
		}
	}
	else if (frame->poc == 1 || frame->poc % enCtx->tileLoadBalancingInterval == 0)
	{
		/*First version*/
		float* rowSums = new float[enCtx->numberOfTilesInRow] { 0 };
		float* columnSums = new float[enCtx->numberOfTilesInColumn] { 0 };

		float* processingTimes = new float[enCtx->threads] { 0.0 };

		for (int i = 0; i < numOfPartitions; i++)
		{
			rowSums[i / enCtx->numberOfTilesInColumn] += referenceFrame->tiles[i]->proccesingTime;
			columnSums[i % enCtx->numberOfTilesInColumn] += referenceFrame->tiles[i]->proccesingTime;

			if (stats->showTestTileStats)
				processingTimes[i] += referenceFrame->tiles[i]->proccesingTime;
		}


		//adapt vertical lines
		for (int i = 0; i < enCtx->numberOfTilesInColumn - 1; i++)
		{
			float ctuRowAvgLeft = columnSums[i] / enCtx->columnWidths[i];
			float ctuRowAvgRight = columnSums[i + 1] / enCtx->columnWidths[i + 1];

			int largerIndex = columnSums[i] >= columnSums[i + 1] ? i : i + 1;
			float columnDiff = abs(columnSums[i] - columnSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				float newDiff = columnDiff - 2 * columnSums[largerIndex] / enCtx->columnWidths[largerIndex];
				if (newDiff > 0)
				{
					ctuShift++;
					columnDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < columnDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->columnWidths[i] -= ctuShift;
				enCtx->columnWidths[i + 1] += ctuShift;
			}
			else
			{
				enCtx->columnWidths[i] += ctuShift;
				enCtx->columnWidths[i + 1] -= ctuShift;
			}
		}

		//adapt horizontal lines
		for (int i = 0; i < enCtx->numberOfTilesInRow - 1; i++)
		{
			float ctuRowAvgUp = rowSums[i] / enCtx->rowHeights[i];
			float ctuRowAvgBottom = rowSums[i + 1] / enCtx->rowHeights[i + 1];

			int largerIndex = rowSums[i] >= rowSums[i + 1] ? i : i + 1;
			float rowDiff = abs(rowSums[i] - rowSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				float newDiff = rowDiff - 2 * rowSums[largerIndex] / enCtx->rowHeights[largerIndex];
				if (newDiff > 0)
				{
					ctuShift++;
					rowDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < rowDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->rowHeights[i] -= ctuShift;
				enCtx->rowHeights[i + 1] += ctuShift;
			}
			else
			{
				enCtx->rowHeights[i] += ctuShift;
				enCtx->rowHeights[i + 1] -= ctuShift;
			}
		}

		enCtx->isUniform = false;
		enCtx->tileChanged = true;
		addNewPPS = true;

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(processingTimes, enCtx->threads);

		delete[] rowSums;
		delete[] columnSums;
	}

}

void LoadBalancing::shiftBorderBits(Frame* frame, int** tilesPerThread, int numOfPartitions, Frame* referenceFrame, EncodingContext* enCtx, bool& addNewPPS, Statistics* stats)
{
	if (numOfPartitions != enCtx->threads)
	{
		cout << endl << "For this type of load balancing, for now, number of tiles has to be same as number of threads" << endl;
		exit(EXIT_FAILURE);
	}

	if (frame->poc == 0)
	{
		//default situation...each tile to thread..number of threads should be equal to number of number of Tiles
		for (int i = 0; i < enCtx->threads; i++)
		{
			tilesPerThread[i] = new int[numOfPartitions + 1];
			tilesPerThread[i][0] = i;
			tilesPerThread[i][1] = -1;
		}
	}
	else if (frame->poc == 1 || frame->poc % enCtx->tileLoadBalancingInterval == 0)
	{
		/*First version*/
		long long* rowSums = new long long[enCtx->numberOfTilesInRow] { 0 };
		long long* columnSums = new long long[enCtx->numberOfTilesInColumn] { 0 };

		float* bitPerThread = new float[enCtx->threads] { 0.0 };

		for (int i = 0; i < numOfPartitions; i++)
		{
			rowSums[i / enCtx->numberOfTilesInColumn] += referenceFrame->tiles[i]->bitSize;
			columnSums[i % enCtx->numberOfTilesInColumn] += referenceFrame->tiles[i]->bitSize;

			if (stats->showTestTileStats)
				bitPerThread[i] += referenceFrame->tiles[i]->bitSize;
		}


		//adapt vertical lines
		for (int i = 0; i < enCtx->numberOfTilesInColumn - 1; i++)
		{
			int ctuRowAvgLeft = (int)(columnSums[i] / enCtx->columnWidths[i]);
			int ctuRowAvgRight = (int)(columnSums[i + 1] / enCtx->columnWidths[i + 1]);

			int largerIndex = columnSums[i] >= columnSums[i + 1] ? i : i + 1;
			int columnDiff = (int)abs(columnSums[i] - columnSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				int newDiff = (int)(columnDiff - 2 * columnSums[largerIndex] / enCtx->columnWidths[largerIndex]);
				if (newDiff > 0)
				{
					ctuShift++;
					columnDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < columnDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->columnWidths[i] -= ctuShift;
				enCtx->columnWidths[i + 1] += ctuShift;
			}
			else
			{
				enCtx->columnWidths[i] += ctuShift;
				enCtx->columnWidths[i + 1] -= ctuShift;
			}
		}

		//adapt horizontal lines
		for (int i = 0; i < enCtx->numberOfTilesInRow - 1; i++)
		{
			int ctuRowAvgUp = (int)(rowSums[i] / enCtx->rowHeights[i]);
			int ctuRowAvgBottom = (int)(rowSums[i + 1] / enCtx->rowHeights[i + 1]);

			int largerIndex = rowSums[i] >= rowSums[i + 1] ? i : i + 1;
			int rowDiff = (int)abs(rowSums[i] - rowSums[i + 1]);

			bool stop = false;
			int ctuShift = 0;
			while (stop != true)
			{
				int newDiff = (int)(rowDiff - 2 * rowSums[largerIndex] / enCtx->rowHeights[largerIndex]);
				if (newDiff > 0)
				{
					ctuShift++;
					rowDiff = newDiff;
				}
				else
				{
					stop = true;
					if (abs(newDiff) < rowDiff)
						ctuShift++;
				}
			}

			if (largerIndex == i)
			{
				enCtx->rowHeights[i] -= ctuShift;
				enCtx->rowHeights[i + 1] += ctuShift;
			}
			else
			{
				enCtx->rowHeights[i] += ctuShift;
				enCtx->rowHeights[i + 1] -= ctuShift;
			}
		}

		enCtx->isUniform = false;
		enCtx->tileChanged = true;
		addNewPPS = true;

		if (stats->showTestTileStats)
			stats->calculateTileLBEfficiency(bitPerThread, enCtx->threads);

		delete[] rowSums;
		delete[] columnSums;
	}
}

void LoadBalancing::reinitializeTilesPerThread(int** tilesPerThread, int numOfThreads, int numOfPartitions)
{
	for (int i = 0; i < numOfThreads; i++)
		delete tilesPerThread[i];

	for (int i = 0; i < numOfThreads; i++)
	{
		tilesPerThread[i] = new int[numOfPartitions + 1];
		for (int j = 0; j < numOfPartitions + 1; j++)
		{
			tilesPerThread[i][j] = -1;
		}
	}
}

LoadBalancing::LoadBalancing()
{
}

LoadBalancing::~LoadBalancing()
{
}

