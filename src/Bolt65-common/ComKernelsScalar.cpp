/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "ComKernelsScalar.h"


ComKernelsScalar::ComKernelsScalar()
{
}

int ComKernelsScalar::SAD(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight)
{
	int result = 0;

	for (int i = 0; i < puHeight; ++i)
	{
		for (int j = 0; j < puWidth; ++j)
		{
			result += abs(block_1[i * puWidth + j] - block_2[i * puWidth + j]);
		}
	}

	return result;
}

void ComKernelsScalar::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	for (int i = 0; i < blockSize * blockSize; i++)
	{
		residualBlock[i] = rawBlock[i] - predictedBlock[i];
	}
}
