/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __EPI_RISCV

#include "TransformAndQuantizationRISCVAuto.h"

TransformAndQuantizationRISCVAuto::TransformAndQuantizationRISCVAuto()
{
}


void TransformAndQuantizationRISCVAuto::multiply(int16_t *A, int16_t* B, int N, int16_t *C, int bdShift)
{
	for (int r = 0; r < N; r++) {
		for (int c = 0; c < N; c++) {
			int tmp = 0;
			#pragma clang loop vectorize(enable)
			for (int p = 0; p < N; p++) {
				tmp = tmp + A[r * N + p] * B[p * N + c];
			}
			C[r * N + c] = (tmp + (1 << (bdShift - 1))) >> bdShift;
		}
	}
}

void TransformAndQuantizationRISCVAuto::quantization(int16_t *A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int mScalingFactor = 16;
	int i;
	*is_zero_matrix = true;

	for (i = 0; i < N; i++)
	{
		#pragma clang loop vectorize(enable)
		for (int j = 0; j < N; j++)
		{
			int sign = 1;
			if (*(A + i * N + j) < 0)
				sign = -1;

			*(C + i * N + j) = (((abs(*(A + i * N + j)) * f[QP % 6] + (1 << (bdShift - 1))) >> QP / 6) >> bdShift);
			*(C + i * N + j) = ComUtil::clip3(coeffMin, coeffMax, *(C + i * N + j) * sign);

			if (*(C + i * N + j) != 0)
				*is_zero_matrix = false;
		}
	}


}

void TransformAndQuantizationRISCVAuto::dequantization(int16_t* A, int QP, int bitDepth, int N, int16_t* C)
{
	int log2TransformRange = 15;
	int bdShift = bitDepth + ComUtil::logarithm2(N) + 10 - log2TransformRange;
	int coeffMin = -32768;
	int coeffMax = 32767;
	int mScalingFactor = 16;
	int i;

	for (i = 0; i < N; i++)
	{
		#pragma clang loop vectorize(enable)
		for (int j = 0; j < N; j++)
		{
			*(C + i * N + j) = ComUtil::clip3(coeffMin, coeffMax, ((*(A + i * N + j) * mScalingFactor * g[QP % 6] << (QP / 6)) + (1 << (bdShift - 1))) >> bdShift);
		}
	}
}
#endif
