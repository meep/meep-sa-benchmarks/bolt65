/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Binarizer.h"

Binarizer::Binarizer()
{
}

Binarizer::~Binarizer()
{
}

Bins Binarizer::binarizeElement(syntaxElemEnum elementId, int elementValue, SyntaxElementInfo *elementInfo, ContextMemory *ctxMem)
{
	Bins b = Bins();
	unsigned int value = elementValue;
	unsigned int cMax = elementInfo->cMax;
	unsigned int cRiceParameter = elementInfo->cRiceParam;
	unsigned int k = elementInfo->k;


	//Check if there is values that have to be precalculated before Binarization
	if (elementInfo->addOperations[0] == PrecalculateValue || elementInfo->addOperations[1] == PrecalculateValue)
		value = precalculateValue(elementId, elementValue);

	if (elementInfo->addOperations[0] == PrecalculateParameter || elementInfo->addOperations[1] == PrecalculateParameter)
	{
		if (elementInfo->binType == FixedLength || elementInfo->binType == TruncatedUnary || elementInfo->binType == TruncatedRice)
			cMax = precalculateParameter(elementId, elementValue, ctxMem);
		else if (elementInfo->binType == ExpGolomb)
			k = precalculateParameter(elementId, elementValue, ctxMem);
	}

	//Perform binarization
	if (elementInfo->binType == TruncatedUnary)
	{
		b = bin_TrU(value, cMax);
	}
	else if (elementInfo->binType == TruncatedRice)
	{
		b = bin_TRk(value, cRiceParameter, cMax);
	}
	else if (elementInfo->binType == ExpGolomb)
	{
		b = bin_EGk(value, k);
	}
	else if (elementInfo->binType == FixedLength)
	{
		b = bin_FL(value, cMax);
	}
	else if (elementInfo->binType == Special)
	{
		b = bin_special(elementId, elementValue, ctxMem);

	}

	//Check if there is a need for Context refresh
	if (elementInfo->addOperations[0] == RefreshContext || elementInfo->addOperations[1] == RefreshContext)
	{
		refreshContext(elementId, elementValue, ctxMem);
	}

	return b;
}

int Binarizer::precalculateValue(syntaxElemEnum elementId, int elementValue)
{
	//for now only one version of precalculated value is possible 
	int x = 0;
	if (elementValue > 0)
		x = (2 * elementValue) - 1;
	else if (elementValue < 0)
		x = -(2 * elementValue);

	return x;
}

int Binarizer::precalculateParameter(syntaxElemEnum elementId, int elementValue, ContextMemory *ctxMem)
{
	int x = 0;
	if (elementId == lt_ref_pic_poc_lsb_sps || elementId == slice_pic_order_cnt_lsb || elementId == poc_lsb_lt)
	{
		x = (1 << (ctxMem->log2_max_pic_order_cnt_lsb_minus4 + 4)) - 1;
	}
	else if (elementId == slice_segment_address)
	{
		int MinCbLog2SizeY = ctxMem->log2_min_luma_coding_block_size_minus3 + 3;
		int CtbLog2SizeY = MinCbLog2SizeY + ctxMem->log2_diff_max_min_luma_coding_block_size;
		int CtbSizeY = 1 << CtbLog2SizeY;
		int PicWidthInCtbsY = (int)std::ceil(ctxMem->pic_width_in_luma_samples / CtbSizeY);
		int PicHeightInCtbsY = (int)std::ceil(ctxMem->pic_height_in_luma_samples / CtbSizeY);
		int PicSizeInCtbsY = PicWidthInCtbsY * PicHeightInCtbsY;
		int numOfBits = (int)std::ceil(ComUtil::logarithm2(PicSizeInCtbsY));
		x = (1 << numOfBits) - 1;
	}
	else if (elementId == short_term_ref_pic_set_idx)
	{
		int numOfBits = (int)std::ceil(ComUtil::logarithm2(ctxMem->num_short_term_ref_pic_sets));
		x = (1 << numOfBits) - 1;
	}
	else if (elementId == lt_idx_sps)
	{
		int numOfBits = (int)std::ceil(ComUtil::logarithm2(ctxMem->num_long_term_ref_pics_sps));
		x = (1 << numOfBits) - 1;
	}
	else if (elementId == entry_point_offset_minus1)
	{
		x = (1 << (ctxMem->offset_len_minus1 + 1)) - 1;
	}
	else if (elementId == pcm_sample_luma)
	{
		x = (1 << (ctxMem->pcm_sample_bit_depth_luma_minus1 + 1)) - 1;
	}
	else if (elementId == pcm_sample_chroma)
	{
		x = (1 << (ctxMem->pcm_sample_bit_depth_chroma_minus1 + 1)) - 1;
	}
	else if (elementId == sao_offset_abs)
	{
		x = (1 << (std::min(8, 10) - 5)) - 1;
	}
	else if (elementId == merge_idx)
	{
		int IvDiMcEnabledFlag = 0;
		int TexMcEnabledFlag = 0;
		int VspMcEnabledFlag = 0;

		int NumExtraMergeCand = (IvDiMcEnabledFlag + TexMcEnabledFlag + VspMcEnabledFlag) > 0 ? 1 : 0;
		int MaxNumMergeCand = 5 + NumExtraMergeCand - ctxMem->five_minus_max_num_merge_cand;

		x = MaxNumMergeCand - 1;
	}
	else if (elementId == ref_idx_l0)
	{
		x = ctxMem->num_ref_idx_l0_active_minus1;
	}
	else if (elementId == ref_idx_l1)
	{
		x = ctxMem->num_ref_idx_l1_active_minus1;
	}
	else if (elementId == cu_chroma_qp_offset_idx)
	{
		x = ctxMem->chroma_qp_offset_list_len_minus1;
	}
	else if (elementId == last_sig_coeff_x_prefix || elementId == last_sig_coeff_y_prefix)
	{
		x = (ctxMem->log2TrafoSize << 1) - 1;
	}
	else if (elementId == last_sig_coeff_x_suffix)
	{
		x = (1 << ((ctxMem->last_sig_coeff_x_prefix >> 1) - 1)) - 1;
	}
	else if (elementId == last_sig_coeff_y_suffix)
	{
		x = (1 << ((ctxMem->last_sig_coeff_y_prefix >> 1) - 1)) - 1;
	}

	return x;
}

void Binarizer::refreshContext(syntaxElemEnum elementId, int elementValue, ContextMemory * ctxMem)
{
	//check if some of these variables acctualy have to be checked since they do not change (or are updated in EncodingContext)?
	if (elementId == log2_max_pic_order_cnt_lsb_minus4)
	{
		ctxMem->log2_max_pic_order_cnt_lsb_minus4 = elementValue;
	}
	else if (elementId == pic_width_in_luma_samples)
	{
		ctxMem->pic_width_in_luma_samples = elementValue;
	}
	else if (elementId == pic_height_in_luma_samples)
	{
		ctxMem->pic_height_in_luma_samples = elementValue;
	}
	else if (elementId == log2_diff_max_min_luma_coding_block_size)
	{
		ctxMem->log2_diff_max_min_luma_coding_block_size = elementValue;
	}
	else if (elementId == log2_min_luma_coding_block_size_minus3)
	{
		ctxMem->log2_min_luma_coding_block_size_minus3 = elementValue;
	}
	else if (elementId == num_short_term_ref_pic_sets)
	{
		ctxMem->num_short_term_ref_pic_sets = elementValue;
	}
	else if (elementId == num_long_term_ref_pics_sps)
	{
		ctxMem->num_long_term_ref_pics_sps = elementValue;
	}
	else if (elementId == offset_len_minus1)
	{
		ctxMem->offset_len_minus1 = elementValue;
	}
	else if (elementId == pcm_sample_bit_depth_luma_minus1)
	{
		ctxMem->pcm_sample_bit_depth_luma_minus1 = elementValue;
	}
	else if (elementId == amp_enabled_flag)
	{
		ctxMem->amp_enabled_flag = elementValue == 1;
	}
	else if (elementId == num_ref_idx_l0_active_minus1)
	{
		ctxMem->num_ref_idx_l0_active_minus1 = elementValue;
	}
	else if (elementId == num_ref_idx_l1_active_minus1)
	{
		ctxMem->num_ref_idx_l1_active_minus1 = elementValue;
	}
	else if (elementId == chroma_qp_offset_list_len_minus1)
	{
		ctxMem->chroma_qp_offset_list_len_minus1 = elementValue;
	}
	else if (elementId == persistent_rice_adaptation_enabled_flag)
	{
		ctxMem->persistent_rice_adaptation_enabled_flag = elementValue == 1;
	}
	else if (elementId == extended_precision_processing_flag)
	{
		ctxMem->extended_precision_processing_flag = elementValue == 1;
	}
	else if (elementId == cu_transquant_bypass_flag)
	{
		ctxMem->cu_transquant_bypass_flag = elementValue == 1;
	}
	else if (elementId == pred_mode_flag)
	{
		ctxMem->pred_mode_flag = elementValue;
	}
	else if (elementId == transform_skip_flag)
	{
		ctxMem->transform_skip_flag = elementValue == 1;
	}
	else if (elementId == last_sig_coeff_x_prefix)
	{
		ctxMem->last_sig_coeff_x_prefix = elementValue;
	}
	else if (elementId == last_sig_coeff_y_prefix)
	{
		ctxMem->last_sig_coeff_y_prefix = elementValue;
	}
	else if (elementId == coeff_abs_level_remaining)
	{
		ctxMem->first_time_invoke = false;
	}
}

Bins Binarizer::bin_special(syntaxElemEnum elementId, int elementValue, ContextMemory * ctxMem)
{
	Bins b = Bins();
	if (elementId == part_mode)
	{
		bool CuPredMode = ctxMem->pred_mode_flag == 0 ? false : true;
		b = bin_part_mode(elementValue, CuPredMode, ctxMem->cbSize, ctxMem->log2_min_luma_coding_block_size_minus3 + 3, ctxMem->amp_enabled_flag);
	}
	else if (elementId == intra_chroma_pred_mode)
	{
		b = bin_intra_chroma_pred_mode(elementValue);
	}
	else if (elementId == inter_pred_idc)
	{
		b = bin_inter_pred_idc(elementValue, ctxMem->nPbW, ctxMem->nPbH);
	}
	else if (elementId == cu_qp_delta_abs)
	{
		b = bin_cu_qp_delta_abs(elementValue);
	}
	else if (elementId == coeff_abs_level_remaining)
	{
		int baseLevel = 1 + ctxMem->coeff_abs_level_greater1_flag + ctxMem->coeff_abs_level_greater2_flag;
		b = bin_coeff_abs_level_remaining(elementValue, baseLevel, ctxMem->colorIdx, ctxMem->persistent_rice_adaptation_enabled_flag, ctxMem->transform_skip_flag,
			ctxMem->cu_transquant_bypass_flag, ctxMem->extended_precision_processing_flag, ctxMem->first_time_invoke, ctxMem->StatCoeff,
			ctxMem->cLastAbsLevel, ctxMem->cLastRiceParam, 8, 8);
	}

	return b;
}

Bins Binarizer::bin_TrU(unsigned int N, unsigned int cMax)
{
	Bins b = Bins();

	if (cMax < N)
		return b;

	b.bits = 0;
	b.length = N + 1;

	if (cMax == N)
		b.length = N;

	for (unsigned int i = 0; i < N; i++)
	{
		if (i == 0)
		{
			b.bits = 1;
		}
		else
		{
			b.bits++;
		}

		if (cMax > i + 1)
			b.bits <<= 1;
	}

	return b;
}

Bins Binarizer::bin_TRk(unsigned int N, unsigned int cRiceParam, unsigned int cMax)
{
	int truN = N >> cRiceParam;
	int truCMax = cMax >> cRiceParam;

	Bins b = bin_TrU(truN, truCMax);

	/*Possible bug here

	NOTE � For the input parameter cRiceParam = 0, the TR binarization is exactly a truncated unary binarization and it is always invoked with a cMax value equal to the largest possible value of the syntax element being decoded.

	*/

	if ((N < cMax) && (cRiceParam > 0))
	{
		unsigned long suffixVal = N - (truN << cRiceParam);

		int cMaxSuffix = (1 << cRiceParam) - 1;
		Bins b1 = bin_FL(suffixVal, cMaxSuffix);

		b = put(b1, b);
	}

	return b;
}

Bins Binarizer::bin_EGk(unsigned int N, unsigned int k)
{
	Bins b = Bins();

	int kIdx = k;
	int NVal = N;

	if (NVal < 0)
		NVal = -NVal;

	b.bits = 0;
	b.length = 0;

	bool trfl = true;

	if (k == 0)
		trfl = false;

	bool stopLoop = false;

	do
	{
		if (NVal >= (1 << kIdx))
		{
			b = put(trfl, b);
			NVal -= (1 << kIdx);
			kIdx++;
		}
		else
		{
			b = put(!trfl, b);
			while (kIdx > 0)
			{
				kIdx--;
				if (((NVal >> kIdx) & 1) == 1)
					b = put(true, b);
				else
					b = put(false, b);
			}
			stopLoop = true;
		}
	} while (!stopLoop);

	return b;
}

Bins Binarizer::bin_EGk_lim(unsigned int N, unsigned int riceParam, unsigned int cIdx, int BitDepthY, int BitDepthC)
{
	int log2TransformRange = cIdx ? std::max(15, BitDepthY + 6) : std::max(15, BitDepthC + 6);
	int maxPrefixExtensionLength = 28 - log2TransformRange;

	int codeValue = N >> riceParam;
	int PrefixExtensionLength = 0;
	Bins b = Bins();
	b.bits = 0;
	b.length = 0;

	while ((PrefixExtensionLength < maxPrefixExtensionLength) && (codeValue > ((2 << PrefixExtensionLength) - 2)))
	{
		PrefixExtensionLength++;
		b = put(true, b);
	}

	int escapeLength;

	if (PrefixExtensionLength == maxPrefixExtensionLength)
		escapeLength = log2TransformRange;
	else
	{
		escapeLength = PrefixExtensionLength + riceParam;
		b = put(false, b);
	}

	N -= ((1 << PrefixExtensionLength) - 1) << riceParam;

	while (escapeLength > 0)
	{
		escapeLength--;
		if (((N >> escapeLength) & 1) == 1)
			b = put(true, b);
		else
			b = put(false, b);
	}

	return b;
}

Bins Binarizer::bin_FL(unsigned int N, unsigned int cMax)
{
	Bins b = Bins();
	b.bits = N;

	//this is cMax is maximum integer, in that cace cMax+1 is going out of int scope and will be 0
	if (cMax == 4294967295)
		b.length = 32;
	else
		b.length = ComUtil::logarithm2(cMax + 1);

	return b;
}

Bins Binarizer::bin_part_mode(int part_mode, bool CuPredMode, int cbSize, int MinCbLog2SizeY, bool amp_enabled_flag)
{
	Bins b = Bins();

	b.length = 0;
	b.bits = 0;

	int log2CbSize = ComUtil::logarithm2(cbSize);

	if (CuPredMode)
	{
		// MODE_INTRA
		if (log2CbSize == MinCbLog2SizeY)
		{
			switch (part_mode)
			{
			case 0:	// PART_2Nx2N
				b = put(true, b);
				break;

			case 1: // PART_NxN
				b = put(false, b);
				break;

			}
		}
	}
	else
	{
		// if CuPredMode is false then prediction mode is MODE_INTER
		// AD_TODO: make binarization for part_mode if luma prediction mode is MODE_INTER (done)
		// AD_TODO: test binarization process if luma prediction mode is MODE_INTER
		switch (part_mode)
		{
		case 0:	// PART_2Nx2N
			b = put(true, b);
			break;

		case 1: // PART_2NxN
			b = put(false, b);
			b = put(true, b);

			if (log2CbSize > MinCbLog2SizeY && amp_enabled_flag)
				b = put(true, b);

			break;

		case 2: // PART_Nx2N
			b = put(false, b);
			b = put(false, b);

			if ((log2CbSize > MinCbLog2SizeY && amp_enabled_flag) ||
				(log2CbSize == MinCbLog2SizeY && log2CbSize > 3))
				b = put(true, b);

			break;

		case 3: // PART_NxN
			if (log2CbSize == MinCbLog2SizeY && log2CbSize > 3)
			{
				b = put(false, b);
				b = put(false, b);
				b = put(false, b);
			}

			break;

		case 4: // PART_2NxnU
			if (log2CbSize > MinCbLog2SizeY && amp_enabled_flag)
			{
				b = put(false, b);
				b = put(true, b);
				b = put(false, b);
				b = put(false, b);
			}

			break;

		case 5: // PART_2NxnD
			if (log2CbSize > MinCbLog2SizeY && amp_enabled_flag)
			{
				b = put(false, b);
				b = put(true, b);
				b = put(false, b);
				b = put(true, b);
			}
			break;

		case 6: // PART_nLx2N
			if (log2CbSize > MinCbLog2SizeY && amp_enabled_flag)
			{
				b = put(false, b);
				b = put(false, b);
				b = put(false, b);
				b = put(false, b);
			}
			break;

		case 7: // PART_nRx2N
			if (log2CbSize > MinCbLog2SizeY && amp_enabled_flag)
			{
				b = put(false, b);
				b = put(false, b);
				b = put(false, b);
				b = put(true, b);
			}
			break;
		}
	}

	return b;
}

Bins Binarizer::bin_intra_chroma_pred_mode(int intra_chroma_pred_mode)
{
	Bins b = Bins();

	switch (intra_chroma_pred_mode)
	{
	case 4:
		b.bits = 0b0;
		b.length = 1;
		break;

	case 0:
		b.bits = 0b100;
		b.length = 3;
		break;

	case 1:
		b.bits = 0b101;
		b.length = 3;
		break;

	case 2:
		b.bits = 0b110;
		b.length = 3;
		break;

	case 3:
		b.bits = 0b111;
		b.length = 3;
		break;
	}

	return b;
}

Bins Binarizer::bin_inter_pred_idc(int inter_pred_idc, int nPbW, int nPbH)
{
	Bins b = Bins();

	switch (inter_pred_idc)
	{
	case 0:
		// PRED_L0
		if (nPbW + nPbH != 12)
		{
			b.bits = 0b00;
			b.length = 2;
		}
		else
		{
			b.bits = 0b0;
			b.length = 1;
		}
		break;

	case 1:
		// PRED_L1
		if (nPbW + nPbH != 12)
		{
			b.bits = 0b01;
			b.length = 2;
		}
		else
		{
			b.bits = 0b1;
			b.length = 1;
		}
		break;

	case 2:
		// PRED_BI
		if (nPbW + nPbH != 12)
		{
			b.bits = 0b1;
			b.length = 1;
		}
		break;
	}
	return b;
}

Bins Binarizer::bin_cu_qp_delta_abs(int cu_qp_delta_abs)
{
	Bins b = Bins();

	int prefixVal = (cu_qp_delta_abs < 5) ? cu_qp_delta_abs : 5;
	b = bin_TRk(prefixVal, 0, 5);

	if (prefixVal > 4)
	{
		int suffixVal = cu_qp_delta_abs - 5;
		Bins bSuffix = bin_EGk(suffixVal, 0);

		// Concatenation of prefix and suffix bin string value
		b = put(bSuffix, b);
	}

	return b;
}

Bins Binarizer::bin_coeff_abs_level_remaining(int coeff_abs_level_remaining, int baseLevel, int cIdx,
	bool persistent_rice_adaptation_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag,
	bool extended_precision_processing_flag, bool first_time_invoke, unsigned short *StatCoeff, unsigned short& cLastAbsLevel, unsigned short& cLastRiceParam,
	unsigned short bitDepthYVar, unsigned short bitDepthCVar)
{
	Bins b = Bins();

	int initRiceValue;

	if (!persistent_rice_adaptation_enabled_flag)
	{
		initRiceValue = 0;
	}
	else
	{
		int sbType;
		if (!transform_skip_flag && !cu_transquant_bypass_flag)
			sbType = 2 * (cIdx == 0 ? 1 : 0);
		else
			sbType = 2 * (cIdx == 0 ? 1 : 0) + 1;

		initRiceValue = StatCoeff[sbType] / 4;

		// If this process is invoked for the first time in this sub-block
		if (first_time_invoke)
		{
			// Modify StatCoeff
			if (coeff_abs_level_remaining >= (3 << (StatCoeff[sbType] / 4)))
				StatCoeff[sbType]++;
			else if (2 * coeff_abs_level_remaining < (1 << (StatCoeff[sbType] / 4)) && StatCoeff[sbType] > 0)
				StatCoeff[sbType]--;
		}
	}

	int cAbsLevel;
	int cRiceParam;

	if (first_time_invoke)
	{
		cLastAbsLevel = 0;
		cLastRiceParam = initRiceValue;
	}

	cAbsLevel = baseLevel + coeff_abs_level_remaining;

	if (!persistent_rice_adaptation_enabled_flag)
	{
		cRiceParam = std::min(cLastRiceParam + ((cLastAbsLevel > (3 * (1 << cLastRiceParam))) ? 1 : 0), 4);
	}
	else
	{
		cRiceParam = cLastRiceParam + (cLastAbsLevel > (3 * (1 << cLastRiceParam)) ? 1 : 0);
	}

	cLastAbsLevel = cAbsLevel;
	cLastRiceParam = cRiceParam;

	int cMax = 4 << cRiceParam;

	int prefixVal = std::min(cMax, coeff_abs_level_remaining);

	b = bin_TRk(prefixVal, cRiceParam, cMax);

	// if prefix bin string have length of 4 with all bits equal to 1
	if (((b.bits & 0b1111) == 0b1111) && b.length == 4)
	{
		// Suffix bin string is present
		int suffixVal = coeff_abs_level_remaining - cMax;

		Bins suffixB = Bins();

		if (extended_precision_processing_flag == 0)
			suffixB = bin_EGk(suffixVal, cRiceParam + 1);
		else
			suffixB = bin_EGk_lim(suffixVal, cRiceParam + 1, cIdx, bitDepthYVar, bitDepthCVar);

		b = put(suffixB, b);
	}

	return b;
}

#pragma region "Decoder Part"

#pragma region "Standard Binarization Methods"

Value Binarizer::invBin_TrU(Bins B, unsigned int cMax)
{
	Value val = Value();
	int maskShift = (sizeof(long) * 8) - B.length;
	B.bits <<= maskShift;
	B.bits >>= maskShift;

	if (B.length == cMax)
	{
		val.value = B.length;

		if (B.bits == ((1 << B.length) - 1))
			val.possible = true;
		else if (B.bits == ((1 << B.length) - 2))
		{
			val.value--;
			val.possible = true;
		}
	}
	else
	{
		val.value = B.length - 1;

		if (B.bits == ((1 << B.length) - 2))
			val.possible = true;
	}

	return val;
}

Value Binarizer::invBin_TRk(Bins B, unsigned int cRiceParam, unsigned int cMax)
{
	Value val = Value();

	int maskShift = (sizeof(long) * 8) - B.length;
	B.bits <<= maskShift;
	B.bits >>= maskShift;

	int truCMax = cMax >> cRiceParam;
	bool cMaxFlag = false;
	int prefixLength = 0;
	int binIndex = B.length - 1;
	int maskGen = 0;
	int maskFlag = 1;

	while (maskFlag != 0 && binIndex >= 0 && cMaxFlag == false)
	{
		maskGen = 1 << binIndex;
		binIndex--;

		maskFlag = B.bits & maskGen;
		prefixLength++;

		if (prefixLength == truCMax)
			cMaxFlag = true;
	}


	Bins truB = Bins();
	truB.bits = B.bits >> (B.length - prefixLength);
	truB.length = prefixLength;

	Value truN = invBin_TrU(truB, truCMax);

	if (truN.possible)
	{
		int N = truN.value << cRiceParam;

		if ((N < (int)cMax) && (cRiceParam > 0))
		{
			if (B.length != (prefixLength + cRiceParam))
				return val;

			B.length = cRiceParam;

			int cMaxSuffix = (1 << cRiceParam) - 1;
			Value suffixVal = invBin_FL(B, cMaxSuffix);

			if (suffixVal.possible)
			{
				val.value = N + suffixVal.value;
				val.possible = true;
			}
		}
		else
		{
			val.value = N;
			val.possible = true;
		}
	}

	return val;
}

Value Binarizer::invBin_EGk(Bins B, unsigned int k)
{
	Value val = Value();

	val.value = 0;

	bool trfl = true;

	if (k == 0)
		trfl = false;

	int leadingBits = -1;
	bool bin;

	do
	{
		bin = get(B);

		if (B.length < 0)	// Not full bin string given
			return val;

		leadingBits++;

		if (bin == trfl)
			val.value += (1 << (k + leadingBits));

	} while (bin == trfl);

	if (B.length == leadingBits + k)
	{
		val.value += B.bits;
		val.possible = true;
	}
	else
	{
		return val;			// Not full bin string given
	}

	return val;
}

Value Binarizer::invBin_EGk_lim(Bins B, unsigned int riceParam, unsigned int cIdx, int BitDepthY, int BitDepthC)
{
	Value val = Value();
	return val;
}

Value Binarizer::invBin_FL(Bins B, unsigned int cMax)
{
	Value val = Value();

	int maskShift = (sizeof(long) * 8) - B.length;
	B.bits <<= maskShift;
	B.bits >>= maskShift;

	if (((1 << B.length) - 1) == cMax || B.length == 32)		// Check if bin string is full
	{
		val.value = B.bits;
		val.possible = true;
	}

	return val;
}

#pragma endregion

#pragma region "Binarization Methods for Special Cases"

Value Binarizer::invBin_part_mode(Bins part_mode, bool CuPredMode, int cbSize, int MinCbLog2SizeY, bool amp_enabled_flag)
{
	Value val = Value();

	int log2CbSize = ComUtil::logarithm2(cbSize);

	if (CuPredMode)
	{
		// MODE_INTRA
		if (log2CbSize == MinCbLog2SizeY)
		{
			if (part_mode.length == 1)
			{
				val.possible = true;
				if ((part_mode.bits & 1) == 1)
					val.value = 0;	// PART_2Nx2N
				else
					val.value = 1;	// PART_NxN

			}
		}
	}
	else
	{
		// MODE_INTER
		if (log2CbSize > MinCbLog2SizeY)
		{
			if (amp_enabled_flag)
			{
				if (part_mode.length == 1 && (part_mode.bits & 1) == 1)			// PART_2Nx2N
				{
					val.value = 0;
					val.possible = true;
				}
				else if (part_mode.length == 3 && (part_mode.bits & 7) == 3)	// PART_2NxN
				{
					val.value = 1;
					val.possible = true;
				}
				else if (part_mode.length == 3 && (part_mode.bits & 7) == 1)	// PART_Nx2N
				{
					val.value = 2;
					val.possible = true;
				}
				else if (part_mode.length == 4 && (part_mode.bits & 15) == 4)	// PART_2NxnU
				{
					val.value = 4;
					val.possible = true;
				}
				else if (part_mode.length == 4 && (part_mode.bits & 15) == 5)	// PART_2NxnD
				{
					val.value = 5;
					val.possible = true;
				}
				else if (part_mode.length == 4 && (part_mode.bits & 15) == 0)	// PART_nLx2N
				{
					val.value = 6;
					val.possible = true;
				}
				else if (part_mode.length == 4 && (part_mode.bits & 15) == 1)	// PART_nRx2N
				{
					val.value = 7;
					val.possible = true;
				}
			}
			else
			{
				if (part_mode.length == 1 && (part_mode.bits & 1) == 1)			// PART_2Nx2N
				{
					val.value = 0;
					val.possible = true;
				}
				else if (part_mode.length == 2 && (part_mode.bits & 7) == 1)	// PART_2NxN
				{
					val.value = 1;
					val.possible = true;
				}
				else if (part_mode.length == 2 && (part_mode.bits & 7) == 0)	// PART_Nx2N
				{
					val.value = 2;
					val.possible = true;
				}
			}
		}
		else if (log2CbSize == MinCbLog2SizeY)
		{
			if (log2CbSize == 3)
			{
				if (part_mode.length == 1 && (part_mode.bits & 1) == 1)			// PART_2Nx2N
				{
					val.value = 0;
					val.possible = true;
				}
				else if (part_mode.length == 2 && (part_mode.bits & 3) == 1)	// PART_2NxN
				{
					val.value = 1;
					val.possible = true;
				}
				else if (part_mode.length == 2 && (part_mode.bits & 3) == 0)	// PART_Nx2N
				{
					val.value = 2;
					val.possible = true;
				}
			}
			else if (log2CbSize > 3)
			{
				if (part_mode.length == 1 && (part_mode.bits & 1) == 1)			// PART_2Nx_2N
				{
					val.value = 0;
					val.possible = true;
				}
				else if (part_mode.length == 2 && (part_mode.bits & 3) == 1)	// PART_2NxN
				{
					val.value = 1;
					val.possible = true;
				}
				else if (part_mode.length == 3 && (part_mode.bits & 7) == 1)	// PART_Nx2N
				{
					val.value = 2;
					val.possible = true;
				}
				else if (part_mode.length == 3 && (part_mode.bits & 7) == 0)	// PART_NxN
				{
					val.value = 3;
					val.possible = true;
				}
			}
		}
	}

	return val;
}

Value Binarizer::invBin_intra_chroma_pred_mode(Bins intra_chroma_pred_mode)
{
	Value val = Value();

	if (intra_chroma_pred_mode.length == 1 && intra_chroma_pred_mode.bits == 0)
	{
		val.value = 4;
		val.possible = true;
	}
	else if (intra_chroma_pred_mode.length == 3 && intra_chroma_pred_mode.bits == 4)
	{
		val.value = 0;
		val.possible = true;
	}
	else if (intra_chroma_pred_mode.length == 3 && intra_chroma_pred_mode.bits == 5)
	{
		val.value = 1;
		val.possible = true;
	}
	else if (intra_chroma_pred_mode.length == 3 && intra_chroma_pred_mode.bits == 6)
	{
		val.value = 2;
		val.possible = true;
	}
	else if (intra_chroma_pred_mode.length == 3 && intra_chroma_pred_mode.bits == 7)
	{
		val.value = 3;
		val.possible = true;
	}

	return val;
}

Value Binarizer::invBin_inter_pred_idc(Bins inter_pred_idc, int nPbW, int nPbH)
{
	Value val = Value();

	if ((nPbW + nPbH) != 12)
	{
		if (inter_pred_idc.length == 2 && (inter_pred_idc.bits & 3) == 0)		// PRED_L0
		{
			val.value = 0;
			val.possible = true;
		}
		else if (inter_pred_idc.length == 2 && (inter_pred_idc.bits & 3) == 1)	// PRED_L1
		{
			val.value = 1;
			val.possible = true;
		}
		else if (inter_pred_idc.length == 1 && (inter_pred_idc.bits & 1) == 1)	// PRED_BI
		{
			val.value = 2;
			val.possible = true;
		}
	}
	else if ((nPbW + nPbH) == 12)
	{
		if (inter_pred_idc.length == 1 && (inter_pred_idc.bits & 1) == 0)		// PRED_L0
		{
			val.value = 0;
			val.possible = true;
		}
		else if (inter_pred_idc.length == 1 && (inter_pred_idc.bits & 1) == 1)	// PRED_L1
		{
			val.value = 1;
			val.possible = true;
		}
	}

	return val;
}

Value Binarizer::invBin_cu_qp_delta_abs(Bins cu_qp_delta_abs)
{
	Value val = Value();

	if (cu_qp_delta_abs.length > 5)
	{
		Bins TRPrefix = Bins();

		TRPrefix.length = 5;
		TRPrefix.bits = cu_qp_delta_abs.bits >> (cu_qp_delta_abs.length - 5);

		Value prefix = invBin_TRk(TRPrefix, 0, 5);

		if (prefix.value == 5 && prefix.possible)
		{
			int mask = 1 << (cu_qp_delta_abs.length - 5);
			mask -= 1;

			cu_qp_delta_abs.length -= 5;
			cu_qp_delta_abs.bits &= mask;

			Value suffix = invBin_EGk(cu_qp_delta_abs, 0);

			if (suffix.possible)
			{
				val.value = 5 + suffix.value;
				val.possible = true;
			}
		}
	}
	else
	{
		val = invBin_TRk(cu_qp_delta_abs, 0, 5);
	}

	return val;
}

Value Binarizer::invBin_coeff_abs_level_remaining(Bins coeff_abs_level_remaining, int baseLevel, int cIdx,
	bool persistent_rice_adaptation_enabled_flag, bool transform_skip_flag, bool cu_transquant_bypass_flag,
	bool extended_precision_processing_flag, bool first_time_invoke, unsigned short *StatCoeff, unsigned short& cLastAbsLevel, unsigned short& cLastRiceParam,
	unsigned short bitDepthYVar, unsigned short bitDepthCVar)
{
	Value val = Value();

	int initRiceValue;
	int sbType;

	if (!persistent_rice_adaptation_enabled_flag)
	{
		initRiceValue = 0;
	}
	else
	{
		if (!transform_skip_flag && !cu_transquant_bypass_flag)
			sbType = 2 * (cIdx == 0 ? 1 : 0);
		else
			sbType = 2 * (cIdx == 0 ? 1 : 0) + 1;

		initRiceValue = StatCoeff[sbType] / 4;
	}

	int cAbsLevel;
	int cRiceParam;

	if (first_time_invoke)
	{
		cLastAbsLevel = 0;
		cLastRiceParam = initRiceValue;
	}

	if (!persistent_rice_adaptation_enabled_flag)
	{
		cRiceParam = std::min(cLastRiceParam + ((cLastAbsLevel > (3 * (1 << cLastRiceParam))) ? 1 : 0), 4);
	}
	else
	{
		cRiceParam = cLastRiceParam + (cLastAbsLevel > (3 * (1 << cLastRiceParam)) ? 1 : 0);
	}

	int cMax = 4 << cRiceParam;

	Bins prefixVal = Bins();

	Bins testBins = Bins();

	testBins.length = coeff_abs_level_remaining.length;
	testBins.bits = coeff_abs_level_remaining.bits;

	if (testBins.length > 4)
	{
		testBins.bits >>= testBins.length - 4;
	}

	if (((testBins.bits & 0b1111) == 0b1111) && testBins.length >= 4)
	{
		prefixVal.length = 4;
		prefixVal.bits = coeff_abs_level_remaining.bits >> (coeff_abs_level_remaining.length - 4);

		val = invBin_TRk(prefixVal, cRiceParam, cMax);

		if (!val.possible)
			return val;

		int suffixMask = (1 << (coeff_abs_level_remaining.length - 4)) - 1;

		Bins suffixB = Bins();

		suffixB.length = coeff_abs_level_remaining.length - 4;
		suffixB.bits = coeff_abs_level_remaining.bits & suffixMask;

		Value suffixVal = Value();

		if (extended_precision_processing_flag == 0)
			suffixVal = invBin_EGk(suffixB, cRiceParam + 1);
		else
			suffixVal = invBin_EGk_lim(suffixB, cRiceParam + 1, cIdx, bitDepthYVar, bitDepthCVar);

		if (!suffixVal.possible)
			return suffixVal;

		val.value += suffixVal.value;
	}
	else
	{
		val = invBin_TRk(coeff_abs_level_remaining, cRiceParam, cMax);

		if (!val.possible)
			return val;
	}

	cAbsLevel = baseLevel + val.value;
	cLastAbsLevel = cAbsLevel;
	cLastRiceParam = cRiceParam;

	// If this process is invoked for the first time in this sub-block
	if (first_time_invoke && persistent_rice_adaptation_enabled_flag)
	{
		// Modify StatCoeff
		if (val.value >= (3 << (StatCoeff[sbType] / 4)))
			StatCoeff[sbType]++;
		else if (2 * val.value < (1 << (StatCoeff[sbType] / 4)) && StatCoeff[sbType] > 0)
			StatCoeff[sbType]--;
	}

	return val;
}

#pragma endregion

#pragma endregion


Bins Binarizer::put(bool val, Bins b)
{
	b.bits <<= 1;

	if (val == true)
		b.bits += 1;

	b.length += 1;

	return b;
}

Bins Binarizer::put(Bins addB, Bins b)
{
	b.bits <<= addB.length;
	b.length += addB.length;

	long mask = (1 << addB.length) - 1;
	addB.bits &= mask;

	b.bits += addB.bits;

	return b;
}

bool Binarizer::get(Bins& b)
{
	bool value = (b.bits >> (b.length - 1)) & 1;
	b.length -= 1;

	long mask = (1 << b.length) - 1;

	b.bits &= mask;

	return value;
}


