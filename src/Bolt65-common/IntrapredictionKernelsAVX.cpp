/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __AVX2__
#include "IntrapredictionKernelsAVX.h"

IntrapredictionKernelsAVX::IntrapredictionKernelsAVX()
{
}

void IntrapredictionKernelsAVX::ThreeTapRefFilter(unsigned char* refSamples, unsigned char* filteredRefSamples, int blockSize) {
	__m256i prev, prev_lo, prev_hi;
	__m256i cur, cur_lo, cur_hi;
	__m256i next, next_lo, next_hi;

	__m128i prev_lo_8, prev_hi_8;
	__m128i cur_lo_8, cur_hi_8;
	__m128i next_lo_8, next_hi_8;

	__m256i f, f_lo, f_hi;
	__m256i m2 = _mm256_set1_epi16(2);

	int N = blockSize * 4;

	for (int i = 1; i < N; i = i + 32)
	{
		prev = _mm256_loadu_si256((__m256i*) & refSamples[i - 1]);
		cur = _mm256_loadu_si256((__m256i*) & refSamples[i]);
		next = _mm256_loadu_si256((__m256i*) & refSamples[i + 1]);

		// separate to 2 parts
		prev_lo_8 = _mm256_extracti128_si256(prev, 0);
		prev_hi_8 = _mm256_extracti128_si256(prev, 1);

		cur_lo_8 = _mm256_extracti128_si256(cur, 0);
		cur_hi_8 = _mm256_extracti128_si256(cur, 1);

		next_lo_8 = _mm256_extracti128_si256(next, 0);
		next_hi_8 = _mm256_extracti128_si256(next, 1);


		// convert to 16 bit
		prev_lo = _mm256_cvtepu8_epi16(prev_lo_8);
		prev_hi = _mm256_cvtepu8_epi16(prev_hi_8);

		cur_lo = _mm256_cvtepu8_epi16(cur_lo_8);
		cur_hi = _mm256_cvtepu8_epi16(cur_hi_8);

		next_lo = _mm256_cvtepu8_epi16(next_lo_8);
		next_hi = _mm256_cvtepu8_epi16(next_hi_8);


		// get values
		f_lo = _mm256_mullo_epi16(m2, cur_lo);
		f_lo = _mm256_add_epi16(prev_lo, f_lo);
		f_lo = _mm256_add_epi16(f_lo, next_lo);
		f_lo = _mm256_add_epi16(f_lo, m2);
		f_lo = _mm256_srai_epi16(f_lo, 2);

		f_hi = _mm256_mullo_epi16(m2, cur_hi);
		f_hi = _mm256_add_epi16(prev_hi, f_hi);
		f_hi = _mm256_add_epi16(f_hi, next_hi);
		f_hi = _mm256_add_epi16(f_hi, m2);
		f_hi = _mm256_srai_epi16(f_hi, 2);


		// pack values back to 8 bit and store
		f = _mm256_packus_epi16(f_lo, f_hi);
		f = _mm256_permute4x64_epi64(f, 0xD8);
		_mm256_storeu_si256((__m256i*) & filteredRefSamples[i], f);
	}

	filteredRefSamples[0] = refSamples[0];
	filteredRefSamples[N] = refSamples[N];
}

void IntrapredictionKernelsAVX::PlanarPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int blockSize) {
	int shiftSize = ComUtil::logarithm2(blockSize) + 1;
	int i, j;

	__m256i i_seq_lo, i_seq_hi;
	__m256i j_seq_lo, j_seq_hi;
	__m256i add_seq = _mm256_set1_epi16((unsigned char)(32 / blockSize));

	__m256i predictedBlockH_lo, predictedBlockH_hi;
	__m256i predictedBlockV_lo, predictedBlockV_hi;
	__m256i result, result_lo, result_hi;

	__m256i m1_lo, m1_hi, m2_lo, m2_hi;
	__m256i f1_lo, f1_hi, f2_lo, f2_hi;
	__m256i bs = _mm256_set1_epi16(blockSize);

	unsigned char v1_base_1, v1_base_2, v2_base, v4_base;
	__m256i v2, v3, v4;
	__m256i v3_lo, v3_hi;
	__m128i v3_lo_8, v3_hi_8;

	v2_base = refSamples[3 * blockSize + 1];
	v2 = _mm256_set1_epi16(v2_base);

	v4_base = refSamples[blockSize - 1];
	v4 = _mm256_set1_epi16(v4_base);

	if (blockSize == 32)
	{
		__m256i v1;

		i_seq_lo = _mm256_set1_epi16((unsigned char)1);
		i_seq_hi = i_seq_lo;
		j_seq_lo = _mm256_set_epi16(16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
		j_seq_hi = _mm256_set_epi16(32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17);

		v3 = _mm256_loadu_si256((__m256i*) & refSamples[2 * blockSize + 1]);

		// separate to 2 parts
		v3_lo_8 = _mm256_extracti128_si256(v3, 0);
		v3_hi_8 = _mm256_extracti128_si256(v3, 1);

		// convert to 16 bit
		v3_lo = _mm256_cvtepu8_epi16(v3_lo_8);
		v3_hi = _mm256_cvtepu8_epi16(v3_hi_8);

		for (i = 0; i < blockSize; i++) // row
		{
			v1_base_1 = refSamples[2 * blockSize - 1 - i];
			v1 = _mm256_set1_epi16(v1_base_1);

			// m1 = blockSize - 1 - j
			m1_lo = _mm256_sub_epi16(bs, j_seq_lo);
			m1_hi = _mm256_sub_epi16(bs, j_seq_hi);

			// m2 = blockSize - 1 - i
			m2_lo = _mm256_sub_epi16(bs, i_seq_lo);
			m2_hi = _mm256_sub_epi16(bs, i_seq_hi);

			// f1 = m1 * v1
			f1_lo = _mm256_mullo_epi16(m1_lo, v1);
			f1_hi = _mm256_mullo_epi16(m1_hi, v1);

			// f2 = j_seq * v2
			f2_lo = _mm256_mullo_epi16(j_seq_lo, v2);
			f2_hi = _mm256_mullo_epi16(j_seq_hi, v2);

			predictedBlockH_lo = _mm256_add_epi16(f1_lo, f2_lo);
			predictedBlockH_hi = _mm256_add_epi16(f1_hi, f2_hi);


			// f1 = m2 * v3
			f1_lo = _mm256_mullo_epi16(m2_lo, v3_lo);
			f1_hi = _mm256_mullo_epi16(m2_hi, v3_hi);

			// f2 = i_seq * v4
			f2_lo = _mm256_mullo_epi16(i_seq_lo, v4);
			f2_hi = _mm256_mullo_epi16(i_seq_hi, v4);

			predictedBlockV_lo = _mm256_add_epi16(f1_lo, f2_lo);
			predictedBlockV_hi = _mm256_add_epi16(f1_hi, f2_hi);


			result_lo = _mm256_add_epi16(predictedBlockH_lo, predictedBlockV_lo);
			result_hi = _mm256_add_epi16(predictedBlockH_hi, predictedBlockV_hi);

			result_lo = _mm256_add_epi16(result_lo, bs);
			result_hi = _mm256_add_epi16(result_hi, bs);

			result_lo = _mm256_srai_epi16(result_lo, shiftSize);
			result_hi = _mm256_srai_epi16(result_hi, shiftSize);

			result = _mm256_packus_epi16(result_lo, result_hi);
			result = _mm256_permute4x64_epi64(result, 0xD8);

			_mm256_storeu_si256((__m256i*) & predictedBlock[i * blockSize], result);

			i_seq_lo = _mm256_add_epi16(i_seq_lo, add_seq);
			i_seq_hi = _mm256_add_epi16(i_seq_hi, add_seq);
		}
	}
	else if (blockSize == 16)
	{
		__m256i v1_lo, v1_hi;
		__m128i mask = _mm_setr_epi32(-1, -1, -1, -1);

		i_seq_lo = _mm256_set1_epi16((unsigned char)1);
		i_seq_hi = _mm256_set1_epi16((unsigned char)2);
		j_seq_lo = _mm256_set_epi16(16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
		j_seq_hi = j_seq_lo;

		// load 16 elements
		v3_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + 1], mask);

		// convert to 16 bit
		v3_lo = _mm256_cvtepu8_epi16(v3_lo_8);
		v3_hi = v3_lo;

		for (i = 0; i < blockSize; i = i + 2) // row
		{
			v1_base_1 = refSamples[2 * blockSize - 1 - i];
			v1_lo = _mm256_set1_epi16(v1_base_1);

			v1_base_2 = refSamples[2 * blockSize - 1 - i - 1];
			v1_hi = _mm256_set1_epi16(v1_base_2);

			// m1 = blockSize - 1 - j
			m1_lo = _mm256_sub_epi16(bs, j_seq_lo);
			m1_hi = _mm256_sub_epi16(bs, j_seq_hi);

			// m2 = blockSize - 1 - i
			m2_lo = _mm256_sub_epi16(bs, i_seq_lo);
			m2_hi = _mm256_sub_epi16(bs, i_seq_hi);

			// f1 = m1 * v1
			f1_lo = _mm256_mullo_epi16(m1_lo, v1_lo);
			f1_hi = _mm256_mullo_epi16(m1_hi, v1_hi);

			// f2 = j_seq * v2
			f2_lo = _mm256_mullo_epi16(j_seq_lo, v2);
			f2_hi = _mm256_mullo_epi16(j_seq_hi, v2);

			predictedBlockH_lo = _mm256_add_epi16(f1_lo, f2_lo);
			predictedBlockH_hi = _mm256_add_epi16(f1_hi, f2_hi);


			// f1 = m2 * v3
			f1_lo = _mm256_mullo_epi16(m2_lo, v3_lo);
			f1_hi = _mm256_mullo_epi16(m2_hi, v3_hi);

			// f2 = i_seq * v4
			f2_lo = _mm256_mullo_epi16(i_seq_lo, v4);
			f2_hi = _mm256_mullo_epi16(i_seq_hi, v4);

			predictedBlockV_lo = _mm256_add_epi16(f1_lo, f2_lo);
			predictedBlockV_hi = _mm256_add_epi16(f1_hi, f2_hi);


			result_lo = _mm256_add_epi16(predictedBlockH_lo, predictedBlockV_lo);
			result_hi = _mm256_add_epi16(predictedBlockH_hi, predictedBlockV_hi);

			result_lo = _mm256_add_epi16(result_lo, bs);
			result_hi = _mm256_add_epi16(result_hi, bs);

			result_lo = _mm256_srai_epi16(result_lo, shiftSize);
			result_hi = _mm256_srai_epi16(result_hi, shiftSize);

			result = _mm256_packus_epi16(result_lo, result_hi);
			result = _mm256_permute4x64_epi64(result, 0xD8);

			_mm256_storeu_si256((__m256i*) & predictedBlock[i * blockSize], result);

			i_seq_lo = _mm256_add_epi16(i_seq_lo, add_seq);
			i_seq_hi = _mm256_add_epi16(i_seq_hi, add_seq);
		}
	}
	else if (blockSize == 8)
	{
		__m256i v1_lo, v1_hi;
		__m128i mask = _mm_setr_epi32(-1, -1, 1, 1);

		i_seq_lo = _mm256_set_epi16(2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1);
		i_seq_hi = _mm256_set_epi16(4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3);
		j_seq_lo = _mm256_set_epi16(8, 7, 6, 5, 4, 3, 2, 1, 8, 7, 6, 5, 4, 3, 2, 1);
		j_seq_hi = j_seq_lo;

		// load 8 elements
		v3 = _mm256_cvtepu8_epi16(_mm_maskload_epi32((int*)&refSamples[2 * blockSize + 1], mask));

		// repeat the 8 elements two times in a single vector for both lo and hi part
		v3_lo = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(_mm256_packus_epi16(v3, v3), 0));
		v3_hi = v3_lo;

		for (i = 0; i < blockSize; i = i + 4) // row
		{
			v1_base_1 = refSamples[2 * blockSize - 1 - i];
			v1_base_2 = refSamples[2 * blockSize - 1 - i - 1];
			v1_lo = _mm256_set_epi16(v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1);

			v1_base_1 = refSamples[2 * blockSize - 1 - i - 2];
			v1_base_2 = refSamples[2 * blockSize - 1 - i - 3];
			v1_hi = _mm256_set_epi16(v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1, v1_base_1);

			// m1 = blockSize - 1 - j
			m1_lo = _mm256_sub_epi16(bs, j_seq_lo);
			m1_hi = _mm256_sub_epi16(bs, j_seq_hi);

			// m2 = blockSize - 1 - i
			m2_lo = _mm256_sub_epi16(bs, i_seq_lo);
			m2_hi = _mm256_sub_epi16(bs, i_seq_hi);

			// f1 = m1 * v1
			f1_lo = _mm256_mullo_epi16(m1_lo, v1_lo);
			f1_hi = _mm256_mullo_epi16(m1_hi, v1_hi);

			// f2 = j_seq * v2
			f2_lo = _mm256_mullo_epi16(j_seq_lo, v2);
			f2_hi = _mm256_mullo_epi16(j_seq_hi, v2);

			predictedBlockH_lo = _mm256_add_epi16(f1_lo, f2_lo);
			predictedBlockH_hi = _mm256_add_epi16(f1_hi, f2_hi);


			// f1 = m2 * v3
			f1_lo = _mm256_mullo_epi16(m2_lo, v3_lo);
			f1_hi = _mm256_mullo_epi16(m2_hi, v3_hi);

			// f2 = i_seq * v4
			f2_lo = _mm256_mullo_epi16(i_seq_lo, v4);
			f2_hi = _mm256_mullo_epi16(i_seq_hi, v4);

			predictedBlockV_lo = _mm256_add_epi16(f1_lo, f2_lo);
			predictedBlockV_hi = _mm256_add_epi16(f1_hi, f2_hi);


			result_lo = _mm256_add_epi16(predictedBlockH_lo, predictedBlockV_lo);
			result_hi = _mm256_add_epi16(predictedBlockH_hi, predictedBlockV_hi);

			result_lo = _mm256_add_epi16(result_lo, bs);
			result_hi = _mm256_add_epi16(result_hi, bs);

			result_lo = _mm256_srai_epi16(result_lo, shiftSize);
			result_hi = _mm256_srai_epi16(result_hi, shiftSize);

			result = _mm256_packus_epi16(result_lo, result_hi);
			result = _mm256_permute4x64_epi64(result, 0xD8);

			_mm256_storeu_si256((__m256i*) & predictedBlock[i * blockSize], result);

			i_seq_lo = _mm256_add_epi16(i_seq_lo, add_seq);
			i_seq_hi = _mm256_add_epi16(i_seq_hi, add_seq);
		}
	}
	else if (blockSize == 4)
	{
		__m256i v1_lo;
		int v1_base_3, v1_base_4;
		__m128i mask = _mm_setr_epi32(-1, 1, 1, 1);
		__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, -1, -1, -1, -1);

		i_seq_lo = _mm256_set_epi16(4, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2, 1, 1, 1, 1);
		j_seq_lo = _mm256_set_epi16(4, 3, 2, 1, 4, 3, 2, 1, 4, 3, 2, 1, 4, 3, 2, 1);

		// load 4 elements
		v3 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(_mm256_cvtepu8_epi16(_mm_maskload_epi32((int*)&refSamples[2 * blockSize + 1], mask)), 0));

		// repeat the 4 elements four times in a single vector
		v3_lo = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(_mm256_packus_epi16(v3, v3), 0));
		v3_lo = _mm256_packus_epi16(v3_lo, v3_lo);

		v1_base_1 = refSamples[2 * blockSize - 1];
		v1_base_2 = refSamples[2 * blockSize - 2];
		v1_base_3 = refSamples[2 * blockSize - 3];
		v1_base_4 = refSamples[2 * blockSize - 4];
		v1_lo = _mm256_set_epi16(v1_base_4, v1_base_4, v1_base_4, v1_base_4, v1_base_3, v1_base_3, v1_base_3, v1_base_3, v1_base_2, v1_base_2, v1_base_2, v1_base_2, v1_base_1, v1_base_1, v1_base_1, v1_base_1);


		// m1 = blockSize - 1 - j
		m1_lo = _mm256_sub_epi16(bs, j_seq_lo);

		// m2 = blockSize - 1 - i
		m2_lo = _mm256_sub_epi16(bs, i_seq_lo);

		// f1 = m1 * v1
		f1_lo = _mm256_mullo_epi16(m1_lo, v1_lo);

		// f2 = j_seq * v2
		f2_lo = _mm256_mullo_epi16(j_seq_lo, v2);

		predictedBlockH_lo = _mm256_add_epi16(f1_lo, f2_lo);


		// f1 = m2 * v3
		f1_lo = _mm256_mullo_epi16(m2_lo, v3_lo);

		// f2 = i_seq * v4
		f2_lo = _mm256_mullo_epi16(i_seq_lo, v4);

		predictedBlockV_lo = _mm256_add_epi16(f1_lo, f2_lo);


		result_lo = _mm256_add_epi16(predictedBlockH_lo, predictedBlockV_lo);
		result_lo = _mm256_add_epi16(result_lo, bs);
		result_lo = _mm256_srai_epi16(result_lo, shiftSize);

		result_lo = _mm256_packus_epi16(result_lo, result_lo);
		result_lo = _mm256_permute4x64_epi64(result_lo, 0xD8);
		_mm256_maskstore_epi32((int*)&predictedBlock[0], mask_s, result_lo);
	}
}

void IntrapredictionKernelsAVX::DCPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int cIdx, int blockSize)
{
	short i, j;
	short dcVal = 0;
	bool isFiltered = false;
	int shiftSize = ComUtil::logarithm2(blockSize) + 1;

	__m256i r, r_16;

	__m256i v, v_lo, v2, v3, f2;
	__m128i v_lo_8, v_hi_8;

	__m128i sum_lo, sum_hi;


	if (blockSize == 32)
	{
		for (i = blockSize; i < 2 * blockSize; i = i + 32)
		{
			v = _mm256_loadu_si256((__m256i*) & refSamples[i]);
			v_lo_8 = _mm256_extracti128_si256(v, 0);
			v_hi_8 = _mm256_extracti128_si256(v, 1);

			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			sum_hi = _mm_sad_epu8(v_hi_8, _mm_setzero_si128());

			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4) + _mm_extract_epi16(sum_hi, 0) + _mm_extract_epi16(sum_hi, 4);
		}

		for (i = 2 * blockSize + 1; i < 3 * blockSize + 1; i = i + 32)
		{
			v = _mm256_loadu_si256((__m256i*) & refSamples[i]);
			v_lo_8 = _mm256_extracti128_si256(v, 0);
			v_hi_8 = _mm256_extracti128_si256(v, 1);

			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			sum_hi = _mm_sad_epu8(v_hi_8, _mm_setzero_si128());

			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4) + _mm_extract_epi16(sum_hi, 0) + _mm_extract_epi16(sum_hi, 4);
		}

		dcVal = (dcVal + blockSize) >> shiftSize;
		r = _mm256_set1_epi8((unsigned char)dcVal);

		for (i = 0; i < blockSize * blockSize; i = i + 32)
			_mm256_storeu_si256((__m256i*) & predictedBlock[i], r);
	}

	else if (blockSize == 16)
	{
		__m128i mask = _mm_setr_epi32(-1, -1, -1, -1);

		for (i = blockSize; i < 2 * blockSize; i = i + 16)
		{
			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[i], mask);
			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4);
		}

		for (i = 2 * blockSize + 1; i < 3 * blockSize + 1; i = i + 16)
		{
			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[i], mask);
			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4);
		}

		dcVal = (dcVal + blockSize) >> shiftSize;
		r = _mm256_set1_epi8((unsigned char)dcVal);
		r_16 = _mm256_set1_epi16((unsigned char)dcVal);

		if (cIdx == 0)
		{
			v2 = _mm256_set1_epi16((unsigned char)2);
			v3 = _mm256_set1_epi16((unsigned char)3);

			f2 = _mm256_mullo_epi16(v3, r_16);
			f2 = _mm256_add_epi16(f2, v2);

			for (i = 0; i < blockSize * blockSize; i = i + 32)
			{
				if (i == 0) {
					v_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + 1 + i], mask);

					// convert to 16 bit
					v_lo = _mm256_cvtepu8_epi16(v_lo_8);

					v_lo = _mm256_add_epi16(v_lo, f2);
					v_lo = _mm256_srai_epi16(v_lo, 2);

					v = _mm256_packus_epi16(v_lo, r_16);
					v = _mm256_permute4x64_epi64(v, 0xD8);

					_mm256_storeu_si256((__m256i*) & predictedBlock[i], v);
				}
				else
					_mm256_storeu_si256((__m256i*) & predictedBlock[i], r);
			}

			predictedBlock[0] = (refSamples[2 * blockSize - 1] + 2 * dcVal + refSamples[2 * blockSize + 1] + 2) >> 2; 	// case when i = 0 i j = 0

			for (i = 1; i < blockSize; i++)
				predictedBlock[i * blockSize] = (refSamples[2 * blockSize - 1 - i] + 3 * dcVal + 2) >> 2;			 	// case when je j = 0						
		}
		else
		{
			for (i = 0; i < blockSize * blockSize; i = i + 32) // column
			{
				_mm256_storeu_si256((__m256i*) & predictedBlock[i], r);
			}
		}
	}

	else if (blockSize == 8)
	{
		__m128i mask = _mm_setr_epi32(-1, -1, 1, 1);
		__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, -1, -1, -1, -1);

		for (i = blockSize; i < 2 * blockSize; i = i + 8)
		{
			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[i], mask);
			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4);
		}

		for (i = 2 * blockSize + 1; i < 3 * blockSize + 1; i = i + 8)
		{
			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[i], mask);
			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4);
		}

		dcVal = (dcVal + blockSize) >> shiftSize;
		r = _mm256_set1_epi8((unsigned char)dcVal);
		r_16 = _mm256_set1_epi16((unsigned char)dcVal);

		if (cIdx == 0)
		{
			v2 = _mm256_set1_epi16((unsigned char)2);
			v3 = _mm256_set1_epi16((unsigned char)3);

			f2 = _mm256_mullo_epi16(v3, r_16);
			f2 = _mm256_add_epi16(f2, v2);

			for (i = 0; i < blockSize * blockSize; i = i + 32)
			{
				if (i == 0) {
					v_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + 1 + i], mask);

					// convert to 16 bit
					v_lo = _mm256_cvtepu8_epi16(v_lo_8);

					v_lo = _mm256_add_epi16(v_lo, f2);
					v_lo = _mm256_srai_epi16(v_lo, 2);
					v_lo = _mm256_packus_epi16(v_lo, r_16);

					_mm256_maskstore_epi32((int*)&predictedBlock[i], mask_s, v_lo);
					_mm256_maskstore_epi32((int*)&predictedBlock[i + 16], mask_s, r);
				}
				else
					_mm256_storeu_si256((__m256i*) & predictedBlock[i], r);
			}

			predictedBlock[0] = (refSamples[2 * blockSize - 1] + 2 * dcVal + refSamples[2 * blockSize + 1] + 2) >> 2; 	// case when i = 0 i j = 0

			for (i = 1; i < blockSize; i++)
				predictedBlock[i * blockSize] = (refSamples[2 * blockSize - 1 - i] + 3 * dcVal + 2) >> 2;			 	// case when je j = 0						
		}
		else
		{
			for (i = 0; i < blockSize * blockSize; i = i + 32) // column
			{
				_mm256_storeu_si256((__m256i*) & predictedBlock[i], r);
			}
		}
	}

	else if (blockSize == 4)
	{
		__m128i mask = _mm_setr_epi32(-1, 1, 1, 1);
		__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, 1, 1, -1, -1);
		__m256i mask_sr = _mm256_set_epi32(1, 1, 1, 1, 1, -1, -1, -1);
		__m256i mask_r = _mm256_set_epi32(1, 1, 1, 1, -1, -1, -1, -1);

		for (i = blockSize; i < 2 * blockSize; i = i + 4)
		{
			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[i], mask);
			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4);
		}

		for (i = 2 * blockSize + 1; i < 3 * blockSize + 1; i = i + 4)
		{
			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[i], mask);
			sum_lo = _mm_sad_epu8(v_lo_8, _mm_setzero_si128());
			dcVal += _mm_extract_epi16(sum_lo, 0) + _mm_extract_epi16(sum_lo, 4);
		}

		dcVal = (dcVal + blockSize) >> shiftSize;
		r = _mm256_set1_epi8((unsigned char)dcVal);
		r_16 = _mm256_set1_epi16((unsigned char)dcVal);

		if (cIdx == 0)
		{
			v2 = _mm256_set1_epi16((unsigned char)2);
			v3 = _mm256_set1_epi16((unsigned char)3);

			f2 = _mm256_mullo_epi16(v3, r_16);
			f2 = _mm256_add_epi16(f2, v2);

			v_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + 1], mask);

			// convert to 16 bit
			v_lo = _mm256_cvtepu8_epi16(v_lo_8);

			v_lo = _mm256_add_epi16(v_lo, f2);
			v_lo = _mm256_srai_epi16(v_lo, 2);
			v_lo = _mm256_packus_epi16(v_lo, r_16);

			_mm256_maskstore_epi32((int*)&predictedBlock[0], mask_s, v_lo);
			_mm256_maskstore_epi32((int*)&predictedBlock[4], mask_sr, r);


			predictedBlock[0] = (refSamples[2 * blockSize - 1] + 2 * dcVal + refSamples[2 * blockSize + 1] + 2) >> 2; 	// case when i = 0 i j = 0

			for (i = 1; i < blockSize; i++)
				predictedBlock[i * blockSize] = (refSamples[2 * blockSize - 1 - i] + 3 * dcVal + 2) >> 2;			 	// case when je j = 0						
		}
		else
			_mm256_maskstore_epi32((int*)&predictedBlock[0], mask_r, r);
	}
}

void IntrapredictionKernelsAVX::AngularPrediction(unsigned char* refSamples, unsigned char predMode, unsigned char* predictedBlock, int cIdx, int blockSize)
{
	short i, j;
	int iPar, fPar;
	int doubleBlockSize = 2 * blockSize;

	__m256i result, result_lo, result_hi;

	__m256i seq_16 = _mm256_set1_epi16((unsigned char)16);
	__m256i zeros = _mm256_set1_epi32(0);
	__m256i f1_lo, f1_hi;

	unsigned char v1_base;
	__m256i v2, v3, v4;
	__m256i v2_lo, v2_hi, v4_lo, v4_hi;
	__m128i v2_lo_8, v2_hi_8, v4_lo_8, v4_hi_8;

	if (blockSize == 32)
	{
		if (predMode < 16)	//if is horizontal
		{
			for (i = 0; i < blockSize; i++) // row
			{
				for (j = 0; j < blockSize; j++) // column
				{
					iPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
					fPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) & 31;


					if (fPar == 0)
						predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + 16) >> 5;
					else
						predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + fPar * refSamples[2 * blockSize - i - iPar - 2] + 16) >> 5;
				}
			}
		}

		else  // if is vertical
		{
			int sub_val;
			__m256i sub_val_v, fPar_v;

			for (i = 0; i < blockSize; i++) // row
			{
				iPar = ((i + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
				fPar = ((i + 1) * AngularIntrapredictionModes[predMode][0]) & 31;

				sub_val = 32 - fPar;
				sub_val_v = _mm256_set1_epi16(sub_val);
				fPar_v = _mm256_set1_epi16(fPar);

				v2 = _mm256_loadu_si256((__m256i*) & refSamples[2 * blockSize + iPar + 1]);

				// separate to 2 parts
				v2_lo_8 = _mm256_extracti128_si256(v2, 0);
				v2_hi_8 = _mm256_extracti128_si256(v2, 1);

				// convert to 16 bit
				v2_lo = _mm256_cvtepu8_epi16(v2_lo_8);
				v2_hi = _mm256_cvtepu8_epi16(v2_hi_8);

				result_lo = _mm256_mullo_epi16(sub_val_v, v2_lo);
				result_hi = _mm256_mullo_epi16(sub_val_v, v2_hi);

				if (fPar != 0)
				{
					v4 = _mm256_loadu_si256((__m256i*) & refSamples[2 * blockSize + iPar + 2]);

					// separate to 2 parts
					v4_lo_8 = _mm256_extracti128_si256(v4, 0);
					v4_hi_8 = _mm256_extracti128_si256(v4, 1);

					// convert to 16 bit
					v4_lo = _mm256_cvtepu8_epi16(v4_lo_8);
					v4_hi = _mm256_cvtepu8_epi16(v4_hi_8);

					f1_lo = _mm256_mullo_epi16(fPar_v, v4_lo);
					f1_hi = _mm256_mullo_epi16(fPar_v, v4_hi);

					result_lo = _mm256_add_epi16(result_lo, f1_lo);
					result_hi = _mm256_add_epi16(result_hi, f1_hi);
				}

				result_lo = _mm256_add_epi16(result_lo, seq_16);
				result_hi = _mm256_add_epi16(result_hi, seq_16);

				result_lo = _mm256_srai_epi16(result_lo, 5);
				result_hi = _mm256_srai_epi16(result_hi, 5);


				result = _mm256_packus_epi16(result_lo, result_hi);
				result = _mm256_permute4x64_epi64(result, 0xD8);

				_mm256_storeu_si256((__m256i*) & predictedBlock[i * blockSize], result);
			}
		}
	}
	else if (blockSize == 16)
	{
		unsigned char v3_base;
		__m256i v1;
		__m128i v_128;

		__m128i mask = _mm_setr_epi32(-1, -1, -1, -1);
		__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, -1, -1, -1, -1);

		v3_base = refSamples[doubleBlockSize];
		v3 = _mm256_set1_epi16(v3_base);

		if (predMode < 16) { //if is horizontal
			for (i = 0; i < blockSize; i++) // row
			{
				v1_base = refSamples[doubleBlockSize - 1 - i];
				v1 = _mm256_set1_epi16(v1_base);

				for (j = 0; j < blockSize; j = j + 1) // column
				{
					if (predMode == 8 && i == 0 && cIdx == 0) //when prediction mode is totally horizontal we need to filter the first row.
					{
						v_128 = _mm_maskload_epi32((int*)&refSamples[doubleBlockSize + 1], mask);
						v2_lo = _mm256_cvtepu8_epi16(v_128);

						result = _mm256_sub_epi16(v2_lo, v3);
						result = _mm256_srai_epi16(result, 1);
						result = _mm256_add_epi16(result, v1);

						result = _mm256_packus_epi16(result, zeros);
						result = _mm256_permute4x64_epi64(result, 0xD8);

						_mm256_maskstore_epi32((int*)&predictedBlock[i * blockSize + j], mask_s, result);
						j += 16;
					}
					else
					{
						iPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
						fPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) & 31;


						if (fPar == 0)
							predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + 16) >> 5;
						else
							predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + fPar * refSamples[2 * blockSize - i - iPar - 2] + 16) >> 5;
					}
				}
			}
		}

		else// if is vertical
		{
			int iPar_lo, iPar_hi, fPar_lo, fPar_hi;
			int sub_val_lo, sub_val_hi;

			__m256i sub_val_v_lo, sub_val_v_hi;
			__m256i fPar_v_lo, fPar_v_hi;

			int tmpPredSample;

			for (i = 0; i < blockSize; i = i + 2) // row
			{
				iPar_lo = ((i + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
				iPar_hi = ((i + 2) * AngularIntrapredictionModes[predMode][0]) >> 5;

				fPar_lo = ((i + 1) * AngularIntrapredictionModes[predMode][0]) & 31;
				fPar_hi = ((i + 2) * AngularIntrapredictionModes[predMode][0]) & 31;

				sub_val_lo = 32 - fPar_lo;
				sub_val_hi = 32 - fPar_hi;

				sub_val_v_lo = _mm256_set_epi16(sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo, sub_val_lo);
				sub_val_v_hi = _mm256_set_epi16(sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi, sub_val_hi);

				fPar_v_lo = _mm256_set1_epi16(fPar_lo);
				fPar_v_hi = _mm256_set1_epi16(fPar_hi);

				// extract the 2 parts
				v2_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo + 1], mask);
				v2_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi + 1], mask);

				// convert to 16 bit
				v2_lo = _mm256_cvtepu8_epi16(v2_lo_8);
				v2_hi = _mm256_cvtepu8_epi16(v2_hi_8);

				result_lo = _mm256_mullo_epi16(sub_val_v_lo, v2_lo);
				result_hi = _mm256_mullo_epi16(sub_val_v_hi, v2_hi);

				if (fPar_lo != 0)
				{
					v4_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo + 2], mask);
					v4_lo = _mm256_cvtepu8_epi16(v4_lo_8);

					f1_lo = _mm256_mullo_epi16(fPar_v_lo, v4_lo);
					result_lo = _mm256_add_epi16(result_lo, f1_lo);
				}

				if (fPar_hi != 0)
				{
					v4_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi + 2], mask);
					v4_hi = _mm256_cvtepu8_epi16(v4_hi_8);

					f1_hi = _mm256_mullo_epi16(fPar_v_hi, v4_hi);
					result_hi = _mm256_add_epi16(result_hi, f1_hi);
				}

				result_lo = _mm256_add_epi16(result_lo, seq_16);
				result_hi = _mm256_add_epi16(result_hi, seq_16);

				result_lo = _mm256_srai_epi16(result_lo, 5);
				result_hi = _mm256_srai_epi16(result_hi, 5);


				result = _mm256_packus_epi16(result_lo, result_hi);
				result = _mm256_permute4x64_epi64(result, 0xD8);

				_mm256_storeu_si256((__m256i*) & predictedBlock[i * blockSize], result);

				if (predMode == 24 && cIdx == 0) // if total vertical, needs to be filtered but only first column to the left.
				{
					tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - i] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[i * blockSize] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[i * blockSize] = 0;
					}
					else
					{
						predictedBlock[i * blockSize] = tmpPredSample;
					}

					tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - i - 1] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[(i + 1) * blockSize] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[(i + 1) * blockSize] = 0;
					}
					else
					{
						predictedBlock[(i + 1) * blockSize] = tmpPredSample;
					}
				}
			}
		}
	}
	else if (blockSize == 8)
	{
		unsigned char v3_base;
		__m256i v1_lo;
		__m128i v_128;

		__m128i mask = _mm_setr_epi32(-1, -1, 1, 1);

		v3_base = refSamples[doubleBlockSize];
		v3 = _mm256_set1_epi16(v3_base);

		if (predMode < 16) { //if is horizontal
			__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, 1, 1, -1, -1);

			for (i = 0; i < blockSize; i++) // row
			{
				v1_base = refSamples[doubleBlockSize - 1 - i];
				v1_lo = _mm256_set1_epi16(v1_base);

				for (j = 0; j < blockSize; j = j + 1) // column
				{
					if (predMode == 8 && i == 0 && cIdx == 0) //when prediction mode is totally horizontal we need to filter the first row.
					{
						v_128 = _mm_maskload_epi32((int*)&refSamples[doubleBlockSize + 1], mask);
						v2_lo = _mm256_cvtepu8_epi16(v_128);

						result = _mm256_sub_epi16(v2_lo, v3);
						result = _mm256_srai_epi16(result, 1);
						result = _mm256_add_epi16(result, v1_lo);

						result = _mm256_packus_epi16(result, zeros);
						_mm256_maskstore_epi32((int*)&predictedBlock[i * blockSize + j], mask_s, result);
						j += 8;
					}
					else
					{
						iPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
						fPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) & 31;


						if (fPar == 0)
							predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + 16) >> 5;
						else
							predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + fPar * refSamples[2 * blockSize - i - iPar - 2] + 16) >> 5;
					}
				}
			}
		}

		else// if is vertical
		{
			int iPar_lo_lo, iPar_lo_hi, iPar_hi_lo, iPar_hi_hi;
			int fPar_lo_lo, fPar_lo_hi, fPar_hi_lo, fPar_hi_hi;
			int sub_val_lo_lo, sub_val_lo_hi, sub_val_hi_lo, sub_val_hi_hi;

			__m256i sub_val_v_lo_lo, sub_val_v_lo_hi, sub_val_v_hi_lo, sub_val_v_hi_hi;
			__m256i fPar_v_lo_lo, fPar_v_lo_hi, fPar_v_hi_lo, fPar_v_hi_hi;

			__m256i v2_lo_lo, v2_lo_hi, v2_hi_lo, v2_hi_hi;
			__m128i v2_lo_lo_8, v2_lo_hi_8, v2_hi_lo_8, v2_hi_hi_8;

			__m256i result_lo_lo, result_lo_hi, result_hi_lo, result_hi_hi;

			int tmpPredSample;
			__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, -1, -1, -1, -1);

			for (i = 0; i < blockSize; i = i + 4) // row
			{
				iPar_lo_lo = ((i + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
				iPar_lo_hi = ((i + 2) * AngularIntrapredictionModes[predMode][0]) >> 5;
				iPar_hi_lo = ((i + 3) * AngularIntrapredictionModes[predMode][0]) >> 5;
				iPar_hi_hi = ((i + 4) * AngularIntrapredictionModes[predMode][0]) >> 5;

				fPar_lo_lo = ((i + 1) * AngularIntrapredictionModes[predMode][0]) & 31;
				fPar_lo_hi = ((i + 2) * AngularIntrapredictionModes[predMode][0]) & 31;
				fPar_hi_lo = ((i + 3) * AngularIntrapredictionModes[predMode][0]) & 31;
				fPar_hi_hi = ((i + 4) * AngularIntrapredictionModes[predMode][0]) & 31;

				sub_val_lo_lo = 32 - fPar_lo_lo;
				sub_val_lo_hi = 32 - fPar_lo_hi;
				sub_val_hi_lo = 32 - fPar_hi_lo;
				sub_val_hi_hi = 32 - fPar_hi_hi;

				sub_val_v_lo_lo = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo);
				sub_val_v_lo_hi = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi);
				sub_val_v_hi_lo = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo);
				sub_val_v_hi_hi = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi);

				fPar_v_lo_lo = _mm256_set1_epi16(fPar_lo_lo);
				fPar_v_lo_hi = _mm256_set1_epi16(fPar_lo_hi);
				fPar_v_hi_lo = _mm256_set1_epi16(fPar_hi_lo);
				fPar_v_hi_hi = _mm256_set1_epi16(fPar_hi_hi);

				// extract the 4 parts
				v2_lo_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_lo + 1], mask);
				v2_lo_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_hi + 1], mask);
				v2_hi_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_lo + 1], mask);
				v2_hi_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_hi + 1], mask);

				// convert to 16 bit
				v2_lo_lo = _mm256_cvtepu8_epi16(v2_lo_lo_8);
				v2_lo_hi = _mm256_cvtepu8_epi16(v2_lo_hi_8);
				v2_hi_lo = _mm256_cvtepu8_epi16(v2_hi_lo_8);
				v2_hi_hi = _mm256_cvtepu8_epi16(v2_hi_hi_8);

				result_lo_lo = _mm256_mullo_epi16(sub_val_v_lo_lo, v2_lo_lo);
				result_lo_hi = _mm256_mullo_epi16(sub_val_v_lo_hi, v2_lo_hi);
				result_hi_lo = _mm256_mullo_epi16(sub_val_v_hi_lo, v2_hi_lo);
				result_hi_hi = _mm256_mullo_epi16(sub_val_v_hi_hi, v2_hi_hi);

				if (fPar_lo_lo != 0)
				{
					v4_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_lo + 2], mask);
					v4_lo = _mm256_cvtepu8_epi16(v4_lo_8);

					f1_lo = _mm256_mullo_epi16(fPar_v_lo_lo, v4_lo);
					result_lo_lo = _mm256_add_epi16(result_lo_lo, f1_lo);
				}

				if (fPar_lo_hi != 0)
				{
					v4_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_hi + 2], mask);
					v4_lo = _mm256_cvtepu8_epi16(v4_lo_8);

					f1_lo = _mm256_mullo_epi16(fPar_v_lo_hi, v4_lo);
					result_lo_hi = _mm256_add_epi16(result_lo_hi, f1_lo);
				}

				if (fPar_hi_lo != 0)
				{
					v4_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_lo + 2], mask);
					v4_hi = _mm256_cvtepu8_epi16(v4_hi_8);

					f1_hi = _mm256_mullo_epi16(fPar_v_hi_lo, v4_hi);
					result_hi_lo = _mm256_add_epi16(result_hi_lo, f1_hi);
				}

				if (fPar_hi_hi != 0)
				{
					v4_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_hi + 2], mask);
					v4_hi = _mm256_cvtepu8_epi16(v4_hi_8);

					f1_hi = _mm256_mullo_epi16(fPar_v_hi_hi, v4_hi);
					result_hi_hi = _mm256_add_epi16(result_hi_hi, f1_hi);
				}

				result_lo_lo = _mm256_add_epi16(result_lo_lo, seq_16);
				result_lo_hi = _mm256_add_epi16(result_lo_hi, seq_16);
				result_hi_lo = _mm256_add_epi16(result_hi_lo, seq_16);
				result_hi_hi = _mm256_add_epi16(result_hi_hi, seq_16);

				result_lo_lo = _mm256_srai_epi16(result_lo_lo, 5);
				result_lo_hi = _mm256_srai_epi16(result_lo_hi, 5);
				result_hi_lo = _mm256_srai_epi16(result_hi_lo, 5);
				result_hi_hi = _mm256_srai_epi16(result_hi_hi, 5);

				result_lo = _mm256_packus_epi16(result_lo_lo, result_lo_hi);
				result_hi = _mm256_packus_epi16(result_hi_lo, result_hi_hi);

				_mm256_maskstore_epi32((int*)&predictedBlock[i * blockSize], mask_s, result_lo);
				_mm256_maskstore_epi32((int*)&predictedBlock[i * blockSize + 16], mask_s, result_hi);


				if (predMode == 24 && cIdx == 0) // if total vertical, needs to be filtered but only first column to the left.
				{
					tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - i] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[i * blockSize] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[i * blockSize] = 0;
					}
					else
					{
						predictedBlock[i * blockSize] = tmpPredSample;
					}

					tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - i - 1] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[(i + 1) * blockSize] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[(i + 1) * blockSize] = 0;
					}
					else
					{
						predictedBlock[(i + 1) * blockSize] = tmpPredSample;
					}

					tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - i - 2] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[(i + 2) * blockSize] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[(i + 2) * blockSize] = 0;
					}
					else
					{
						predictedBlock[(i + 2) * blockSize] = tmpPredSample;
					}

					tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - i - 3] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[(i + 3) * blockSize] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[(i + 3) * blockSize] = 0;
					}
					else
					{
						predictedBlock[(i + 3) * blockSize] = tmpPredSample;
					}
				}
			}
		}
	}
	else if (blockSize == 4)
	{
		unsigned char v3_base;
		__m256i v1_lo;
		__m128i v_128;

		__m128i mask = _mm_setr_epi32(-1, 1, 1, 1);

		v3_base = refSamples[doubleBlockSize];
		v3 = _mm256_set1_epi16(v3_base);

		if (predMode < 16) { //if is horizontal

			__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, 1, 1, -1, -1);

			for (i = 0; i < blockSize; i++) // row
			{
				v1_base = refSamples[doubleBlockSize - 1 - i];
				v1_lo = _mm256_set1_epi16(v1_base);

				for (j = 0; j < blockSize; j = j + 1) // column
				{
					if (predMode == 8 && i == 0 && cIdx == 0) //when prediction mode is totally horizontal we need to filter the first row.
					{
						v_128 = _mm_maskload_epi32((int*)&refSamples[doubleBlockSize + 1], mask);
						v2_lo = _mm256_cvtepu8_epi16(v_128);

						result = _mm256_sub_epi16(v2_lo, v3);
						result = _mm256_srai_epi16(result, 1);
						result = _mm256_add_epi16(result, v1_lo);

						result = _mm256_packus_epi16(result, zeros);
						_mm256_maskstore_epi32((int*)&predictedBlock[i * blockSize + j], mask_s, result);
						j += 4;
					}
					else
					{
						iPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
						fPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) & 31;


						if (fPar == 0)
							predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + 16) >> 5;
						else
							predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + fPar * refSamples[2 * blockSize - i - iPar - 2] + 16) >> 5;
					}
				}
			}
		}

		else// if is vertical
		{
			int iPar_lo_lo, iPar_lo_hi, iPar_hi_lo, iPar_hi_hi;
			int fPar_lo_lo, fPar_lo_hi, fPar_hi_lo, fPar_hi_hi;
			int sub_val_lo_lo, sub_val_lo_hi, sub_val_hi_lo, sub_val_hi_hi;

			__m256i sub_val_v_lo_lo, sub_val_v_lo_hi, sub_val_v_hi_lo, sub_val_v_hi_hi;
			__m256i fPar_v_lo_lo, fPar_v_lo_hi, fPar_v_hi_lo, fPar_v_hi_hi;

			__m256i v2_lo_lo, v2_lo_hi, v2_hi_lo, v2_hi_hi;
			__m128i v2_lo_lo_8, v2_lo_hi_8, v2_hi_lo_8, v2_hi_hi_8;

			__m256i result_lo_lo, result_lo_hi, result_hi_lo, result_hi_hi;

			int tmpPredSample;
			__m256i mask_s = _mm256_set_epi32(1, 1, 1, 1, 1, 1, 1, -1);

			iPar_lo_lo = (1 * AngularIntrapredictionModes[predMode][0]) >> 5;
			iPar_lo_hi = (2 * AngularIntrapredictionModes[predMode][0]) >> 5;
			iPar_hi_lo = (3 * AngularIntrapredictionModes[predMode][0]) >> 5;
			iPar_hi_hi = (4 * AngularIntrapredictionModes[predMode][0]) >> 5;

			fPar_lo_lo = (1 * AngularIntrapredictionModes[predMode][0]) & 31;
			fPar_lo_hi = (2 * AngularIntrapredictionModes[predMode][0]) & 31;
			fPar_hi_lo = (3 * AngularIntrapredictionModes[predMode][0]) & 31;
			fPar_hi_hi = (4 * AngularIntrapredictionModes[predMode][0]) & 31;

			sub_val_lo_lo = 32 - fPar_lo_lo;
			sub_val_lo_hi = 32 - fPar_lo_hi;
			sub_val_hi_lo = 32 - fPar_hi_lo;
			sub_val_hi_hi = 32 - fPar_hi_hi;

			sub_val_v_lo_lo = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo, sub_val_lo_lo);
			sub_val_v_lo_hi = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi, sub_val_lo_hi);
			sub_val_v_hi_lo = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo, sub_val_hi_lo);
			sub_val_v_hi_hi = _mm256_set_epi16(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi, sub_val_hi_hi);

			fPar_v_lo_lo = _mm256_set1_epi16(fPar_lo_lo);
			fPar_v_lo_hi = _mm256_set1_epi16(fPar_lo_hi);
			fPar_v_hi_lo = _mm256_set1_epi16(fPar_hi_lo);
			fPar_v_hi_hi = _mm256_set1_epi16(fPar_hi_hi);

			// extract the 4 parts
			v2_lo_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_lo + 1], mask);
			v2_lo_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_hi + 1], mask);
			v2_hi_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_lo + 1], mask);
			v2_hi_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_hi + 1], mask);

			// convert to 16 bit
			v2_lo_lo = _mm256_cvtepu8_epi16(v2_lo_lo_8);
			v2_lo_hi = _mm256_cvtepu8_epi16(v2_lo_hi_8);
			v2_hi_lo = _mm256_cvtepu8_epi16(v2_hi_lo_8);
			v2_hi_hi = _mm256_cvtepu8_epi16(v2_hi_hi_8);

			result_lo_lo = _mm256_mullo_epi16(sub_val_v_lo_lo, v2_lo_lo);
			result_lo_hi = _mm256_mullo_epi16(sub_val_v_lo_hi, v2_lo_hi);
			result_hi_lo = _mm256_mullo_epi16(sub_val_v_hi_lo, v2_hi_lo);
			result_hi_hi = _mm256_mullo_epi16(sub_val_v_hi_hi, v2_hi_hi);

			if (fPar_lo_lo != 0)
			{
				v4_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_lo + 2], mask);
				v4_lo = _mm256_cvtepu8_epi16(v4_lo_8);

				f1_lo = _mm256_mullo_epi16(fPar_v_lo_lo, v4_lo);
				result_lo_lo = _mm256_add_epi16(result_lo_lo, f1_lo);
			}

			if (fPar_lo_hi != 0)
			{
				v4_lo_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_lo_hi + 2], mask);
				v4_lo = _mm256_cvtepu8_epi16(v4_lo_8);

				f1_lo = _mm256_mullo_epi16(fPar_v_lo_hi, v4_lo);
				result_lo_hi = _mm256_add_epi16(result_lo_hi, f1_lo);
			}

			if (fPar_hi_lo != 0)
			{
				v4_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_lo + 2], mask);
				v4_hi = _mm256_cvtepu8_epi16(v4_hi_8);

				f1_hi = _mm256_mullo_epi16(fPar_v_hi_lo, v4_hi);
				result_hi_lo = _mm256_add_epi16(result_hi_lo, f1_hi);
			}

			if (fPar_hi_hi != 0)
			{
				v4_hi_8 = _mm_maskload_epi32((int*)&refSamples[2 * blockSize + iPar_hi_hi + 2], mask);
				v4_hi = _mm256_cvtepu8_epi16(v4_hi_8);

				f1_hi = _mm256_mullo_epi16(fPar_v_hi_hi, v4_hi);
				result_hi_hi = _mm256_add_epi16(result_hi_hi, f1_hi);
			}

			result_lo_lo = _mm256_add_epi16(result_lo_lo, seq_16);
			result_lo_hi = _mm256_add_epi16(result_lo_hi, seq_16);
			result_hi_lo = _mm256_add_epi16(result_hi_lo, seq_16);
			result_hi_hi = _mm256_add_epi16(result_hi_hi, seq_16);

			result_lo_lo = _mm256_srai_epi16(result_lo_lo, 5);
			result_lo_hi = _mm256_srai_epi16(result_lo_hi, 5);
			result_hi_lo = _mm256_srai_epi16(result_hi_lo, 5);
			result_hi_hi = _mm256_srai_epi16(result_hi_hi, 5);

			result_lo_lo = _mm256_packus_epi16(result_lo_lo, result_lo_lo); // moze i zeros
			result_lo_hi = _mm256_packus_epi16(result_lo_hi, result_lo_hi);
			result_hi_lo = _mm256_packus_epi16(result_hi_lo, result_hi_lo);
			result_hi_hi = _mm256_packus_epi16(result_hi_hi, result_hi_hi);

			_mm256_maskstore_epi32((int*)&predictedBlock[0], mask_s, result_lo_lo);
			_mm256_maskstore_epi32((int*)&predictedBlock[4], mask_s, result_lo_hi);
			_mm256_maskstore_epi32((int*)&predictedBlock[8], mask_s, result_hi_lo);
			_mm256_maskstore_epi32((int*)&predictedBlock[12], mask_s, result_hi_hi);

			if (predMode == 24 && cIdx == 0) // if total vertical, needs to be filtered but only first column to the left.
			{
				tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1] - refSamples[doubleBlockSize]) >> 1);
				if (tmpPredSample > 255)
				{
					predictedBlock[0] = 255;
				}
				else if (tmpPredSample < 0)
				{
					predictedBlock[0] = 0;
				}
				else
				{
					predictedBlock[0] = tmpPredSample;
				}

				tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - 1] - refSamples[doubleBlockSize]) >> 1);
				if (tmpPredSample > 255)
				{
					predictedBlock[blockSize] = 255;
				}
				else if (tmpPredSample < 0)
				{
					predictedBlock[blockSize] = 0;
				}
				else
				{
					predictedBlock[blockSize] = tmpPredSample;
				}

				tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - 2] - refSamples[doubleBlockSize]) >> 1);
				if (tmpPredSample > 255)
				{
					predictedBlock[2 * blockSize] = 255;
				}
				else if (tmpPredSample < 0)
				{
					predictedBlock[2 * blockSize] = 0;
				}
				else
				{
					predictedBlock[2 * blockSize] = tmpPredSample;
				}

				tmpPredSample = refSamples[doubleBlockSize + 1] + ((refSamples[doubleBlockSize - 1 - 3] - refSamples[doubleBlockSize]) >> 1);
				if (tmpPredSample > 255)
				{
					predictedBlock[3 * blockSize] = 255;
				}
				else if (tmpPredSample < 0)
				{
					predictedBlock[3 * blockSize] = 0;
				}
				else
				{
					predictedBlock[3 * blockSize] = tmpPredSample;
				}
			}
		}
	}
}
#endif
