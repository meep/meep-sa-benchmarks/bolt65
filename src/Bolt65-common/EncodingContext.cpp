/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/


#include "EncodingContext.h"

EncodingContext::EncodingContext()
{
	appType = ApplicationType::UnknownApp;

//IO and Encoding configuration
	inputFilePath = "";
	outputFilePath = "";
	width = -1;
	height = -1;
	frameRate = -1;
	inputFrameRate = -1; //for transcoder?
	numOfFrames = -1;

	externalConfig = false;
	externalConfigFileName = "";

	//Encoding information
	GOP = "";

	quantizationParameter = -1;
	initQP = -1;
	bitNumber = -1;

	maxCUsize = -1;
	ctbSize = -1;
	ctbLog2SizeY = -1;
	maxTbLog2SizeY = -1;
	minCbLog2SizeY = -1;
	minTbLog2SizeY = -1;

	nalType = TRAIL_N;
	currentVPSId = -1;
	currentSPSId = -1;
	currentPPSId = -1;

	highestTid = -1;

//FLAGS
	//Encoding options flags
	disableDeblockingFilter = true;
	saoEnabled = false;
	useSMP = false;
	useAMP = false;
	useIntraTU = false;

	//Architectural flags
	AVX = false;
	NEON = false;
	SVE = false;
	SVE2 = false;
	EPI_RISCV = false;
	EPI_RISCV_AUTO = false;
	MANGO_HW = false;
	MANGO_PEAK = false;
	MANGO_GN = false;
	MANGO_GPU = false;
	MANGO_NUP = false;
	MEEP = false;

	//Statistics flags
	calculatePSNR = false;
	calculateSSIM = false;
	calculateBits = false;
	calculateTime = false;
	showStatsPerFrame = false;
	showStatsPerTile = false;
	statMode = false;

	outputStatsToFile = false;
	outputStatsFileName = "";

	outputStatsTileToFile = false;
	outputStatsTileFileName = "";

	clusteredTQ = false;

//MOTION ESTIMATION 
	dpbSize = -1;
	searchAlgorithm = -1;
	searchArea = -1;
	interpolationAlgorithm = -1;
	blockMatching = -1;
	log2MaxPicOrderLst = -1;

	useTemporalAMVP = false;
	testAllModes = false;

//TILES and PARALLELISM
	threads = -1;

	tilesEnabled = false;
	isUniform = false;
	numberOfTilesInRow = -1;
	numberOfTilesInColumn = -1;
	rowHeights = nullptr;
	columnWidths = nullptr;
	tileOffsets = nullptr;
	tileLoadBalancingAlgorithm = -1;
	tileLoadBalancingInterval = -1;

	tilesInColumnAsString = "";
	tilesInRowAsString = "";

	useDynamicTiles = false;
	tileChanged = false;

//MEMORY RELATED OPTIONS (Decoder)
	inputBufSize = -1;
	outputBufSize = -1;

//ENTROPY CODING HELPERS
	num_short_term_ref_pic_sets = 0;
	cabac_init_present_flag = 0;
	cabac_init_flag = false;
	max_transform_hierarchy_depth_inter = 0;
	strong_intra_smoothing_enabled_flag = 0;
	initType = 0;

//TRANSCODER SPECIFIC FLAGS
	reuseData = false;
	reuseIntraMode = false;
	reuseInterMode = false;
	reuseInterpolation = false;
	reusePartMode = false;
	reuseMode = false;

//POLICY
	usePolicy = false;
	policyHost = "";
	policyPort = -1;

//OTHER
	command = "";
	version = "";
}

void EncodingContext::clearEncodingContext()
{
	if (rowHeights != nullptr)
		delete[] rowHeights;

	if (columnWidths != nullptr)
		delete[] columnWidths;

	if (tileOffsets != nullptr)
		delete[] tileOffsets;
}

EncodingContext::~EncodingContext()
{
}


