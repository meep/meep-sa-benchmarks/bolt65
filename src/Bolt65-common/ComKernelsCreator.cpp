
/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/
#include "ComKernelsCreator.h"

ComKernelsCreator::ComKernelsCreator()
{
}

ComKernelsCreator::~ComKernelsCreator()
{
}

IComKernels* ComKernelsCreator::Create(EncodingContext* enCtx)
{
#ifdef __EPI_RISCV
	if (enCtx->EPI_RISCV)
	{
		return new ComKernelsEPIRISCV();
	}
	else
	{
		return new ComKernelsScalar();
	}
#endif
#ifdef __AVX2__
	if (enCtx->AVX)
	{
		return new ComKernelsAVX();
	}
	else
	{
		return new ComKernelsScalar();
	}
#elif defined(__ARM_NEON) || defined(__ARM_FEATURE_SVE)
	//SVE has higher priority
	if (enCtx->SVE)
	{
#ifdef __ARM_FEATURE_SVE
		return new ComKernelsSVE();
#else
		return new ComKernelsScalar();
#endif
	}
	else if (enCtx->NEON)
	{
#ifdef __ARM_NEON
		return new ComKernelsNEON();
#else
		return new ComKernelsScalar();
#endif
	}
	else
	{
		return new ComKernelsScalar();
	}
#else
	return new ComKernelsScalar();
#endif 
}
