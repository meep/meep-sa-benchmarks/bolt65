/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Policy.h"

void Policy::initializePolicy(EncodingContext *enCtx)
{
	//Create socket with host and port defined in config file
	string host = enCtx->policyHost;
	int portNum = enCtx->policyPort;

	//Define server
	//Create socket

	//Send initial message with properties
	Policy::params_st videoParams;
	videoParams.resolution_w = enCtx->width;
	videoParams.resolution_h = enCtx->height;
	videoParams.frame_r = enCtx->frameRate;

	//write(socketId,&videoParams,sizeof(videoParams);

	//necessary values that will be used later on in getOptimumConfig (e.g. socketID) can be stored as private variable in EncodingChannel class (header file)
}

void Policy::getOptimumConfiguration( EncodingContext *enCtx)
{
	//Current performance data is stored in currentPerformance variable which is performance_st type (defined in Policy.h)
	//At this point currentPerformance is filled with vali data
	//double currentPSNRY = stats->currentPerformance.PSNRY;

	//Current configuration
	config_st currentConfiguration;
	currentConfiguration.QP = enCtx->quantizationParameter;
	currentConfiguration.SR = enCtx->searchArea;
	currentConfiguration.CU_size = 1 << enCtx->ctbLog2SizeY;
	currentConfiguration.GOP = (int)enCtx->GOP.size();


	//New config is stored in new variable which is config_st type (defined in Policy.h)
	config_st newConfig;

	/*

	Pseudocode:

	socket.write(sockId,&currentPerformance,sizeof(currentPerformance));
	socket.write(sockId,&currentConfiguration,sizeof(currentConfiguration));
	socket.read(sockId,&newConfig,sizeof(newCofig));

	*/

	//Dummy values just for testing
	newConfig.QP = rand() % 10 + 20;
	newConfig.SR = rand() % 5 + 2;
	newConfig.searchAlgorithm = rand() % 4;

	changeConfiguration(newConfig, enCtx);
}

void Policy::changeConfiguration(config_st newConfiguration, EncodingContext *enCtx)
{
	enCtx->quantizationParameter = newConfiguration.QP;
	enCtx->searchArea = newConfiguration.SR;
	enCtx->searchAlgorithm = newConfiguration.searchAlgorithm;
}
