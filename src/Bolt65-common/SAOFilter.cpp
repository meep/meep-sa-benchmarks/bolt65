/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "SAOFilter.h"

void SAOFilter::BandOffset(unsigned char * block)
{
	int bandShift = 3; //bitDepth-5
}

void SAOFilter::EdgeOffset(unsigned char * block, int blockSize, int saoEoClass, int saoOffsetAbs, int saoOffsetSign, int log2OffsetScale)
{
	int hP[2], vP[2];
	int *sampleLocations0 = new int[blockSize*blockSize];
	int *sampleLocations1 = new int[blockSize*blockSize];
	int edgeIdx;

	int i, j;
	for (i = 0; i < 2; i++)
	{
		hP[i] = hPos[i][saoEoClass];
		vP[i] = vPos[i][saoEoClass];
	}

	for (i = 0; i < blockSize; i++)
	{
		for (j = 0; j < blockSize; j++)
		{
			sampleLocations0[i*blockSize + j] = (i + vP[0])*blockSize + j + hP[0];
			sampleLocations1[i*blockSize + j] = (i + vP[1])*blockSize + j + hP[1];
		}
	}

	//Check if idx goes out of picture and other conditions that make edgeIdx=0 (STANDARD page 181)
	//Here is where we use sample locations

	for (i = 0; i < blockSize; i++)
	{
		for (j = 0; j < blockSize; j++)
		{
			edgeIdx = 2 + ComUtil::sign(block[i*blockSize + j] - block[(i + vP[0])*blockSize + j + hP[0]])
				+ ComUtil::sign(block[i*blockSize + j] - block[(i + vP[1])*blockSize + j + hP[1]]);

			if (edgeIdx < 3)
				edgeIdx = edgeIdx == 2 ? 0 : edgeIdx + 1;

			block[i*blockSize + j] = ComUtil::clip3(0, 255, (1 - 2 * saoOffsetSign)*saoOffsetAbs << log2OffsetScale);
		}
	}
}

SAOFilter::SAOFilter()
{
}

SAOFilter::~SAOFilter()
{
}
