/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __EPI_RISCV
#include "TransformAndQuantizationEPIRISCV.h"

#ifdef __EPI_ONLY_LMUL1

TransformAndQuantizationEPIRISCV::TransformAndQuantizationEPIRISCV()
{
  // Under OnlyLMUL1 we need to underuse registers when using SEW=16, so let's
  // request the maximum number of elements of 32-bit we can fit.
  vl = __builtin_epi_vsetvlmax(__epi_e32, __epi_m1);
}

static inline __epi_2xi32 __builtin_epi_vwmul_2xi32(__epi_4xi16 a,
                                                    __epi_4xi16 b, long vl) {
  // Make room for a LMUL2 register.
  register __epi_2xi32 vdest_lo asm("v16");
  register __epi_2xi32 vdest_hi asm("v17");

  __asm__("vsetvli x0, %[gvl], e16,m1\n"
          "vwmul.vv %[vdest], %[va], %[vb]\n"
          : [ vdest ] "=&v"(vdest_lo), "=&v"(vdest_hi)
          : [ gvl ] "r"(vl), [ va ] "v"(a), [ vb ] "v"(b));

  return vdest_lo;
}

static inline __epi_4xi16
__builtin_epi_vnsra_4xi16_2xi32(__epi_2xi32 result_row_wide,
                                __epi_4xi16 tmp_shift, long vl) {
  // Ensure this is a LMUL2 valid input register.
  // The high part won't be used by the VPU.
  register __epi_2xi32 vin_lo asm("v16") = result_row_wide;

  __epi_4xi16 vdest;
  __asm__("vsetvli x0, %[gvl], e16,m1\n"
          "vnsra.vv %[vdest], %[va], %[vb]\n"
          : [ vdest ] "=&v"(vdest)
          :
          [ gvl ] "r"(vl), [ va ] "v"(vin_lo), [ vb ] "v"(tmp_shift));
  return vdest;
}

void TransformAndQuantizationEPIRISCV::multiply(int16_t *A, int16_t *B, int N, int16_t *C, int bdShift)
{
	int parts = (int)ceil(N / (double)vl);
	int step = vl;
	int offset = 0;

	int shiftOffset = 1 << (bdShift - 1);

	for (int p = 0; p < parts; p++)
	{

		for (int i = 0; i < N; i++)
		{
			__epi_2xi32 result_row_wide = __builtin_epi_vbroadcast_2xi32(0, N);

			for (int j = 0; j < N; j++)
			{
				__epi_4xi16 A_element = __builtin_epi_vbroadcast_4xi16(A[i*N + j], N);
				__epi_4xi16 B_row = __builtin_epi_vload_4xi16(B + (j*N) + offset, N);

				__epi_2xi32 tmp_wide = __builtin_epi_vwmul_2xi32(A_element, B_row, N);
				result_row_wide = __builtin_epi_vadd_2xi32(tmp_wide, result_row_wide, N);
			}
			__epi_2xi32 tmp_add = __builtin_epi_vbroadcast_2xi32(shiftOffset, N);
			result_row_wide = __builtin_epi_vadd_2xi32(tmp_add, result_row_wide, N);

			__epi_4xi16 tmp_shift = __builtin_epi_vbroadcast_4xi16(bdShift, N);
			__epi_4xi16 result_row = __builtin_epi_vnsra_4xi16_2xi32(result_row_wide, tmp_shift, N);
			__builtin_epi_vstore_4xi16(C + (i*N) + offset, result_row, N);

		}
		offset = +step;
	}
}

void TransformAndQuantizationEPIRISCV::quantization(int16_t * A, int QP, int NB, int N, bool * is_zero_matrix, int16_t * C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;

	int matrixSize = N * N;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	__epi_4xi16 mult = __builtin_epi_vbroadcast_4xi16(f[QP % 6], maxElements);
	__epi_2xi32 shiftOffset = __builtin_epi_vbroadcast_2xi32(1 << (bdShift - 1), maxElements);
	__epi_4xi16 shift = __builtin_epi_vbroadcast_4xi16(((QP / 6) + bdShift), maxElements);

	__epi_4xi16 zero_mask = __builtin_epi_vbroadcast_4xi16(0, maxElements);
	__epi_4xi16 ff_mask = __builtin_epi_vbroadcast_4xi16(0xFFFF, maxElements);
	__epi_4xi16 one_mask = __builtin_epi_vbroadcast_4xi16(1, maxElements);

	*is_zero_matrix = false;
	int zeroes = 0;

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_4xi16 coeffs = __builtin_epi_vload_4xi16(A + i, maxElements);

		//get absolute value
		__epi_4xi1 mask_negative = __builtin_epi_vmsgt_4xi16(zero_mask, coeffs, maxElements);
		coeffs = __builtin_epi_vxor_4xi16_mask(coeffs, coeffs, ff_mask, mask_negative, maxElements);
		coeffs = __builtin_epi_vadd_4xi16_mask(coeffs, coeffs, one_mask, mask_negative, maxElements);

		//multiply, add, shift to calculate quantization coeffs
		__epi_2xi32 coeff_wide = __builtin_epi_vwmul_2xi32(coeffs, mult, maxElements);
		coeff_wide = __builtin_epi_vadd_2xi32(coeff_wide, shiftOffset, maxElements);
		coeffs = __builtin_epi_vnsra_4xi16_2xi32(coeff_wide, shift, maxElements);

		//return the sign
		coeffs = __builtin_epi_vxor_4xi16_mask(coeffs, coeffs, ff_mask, mask_negative, maxElements);
		coeffs = __builtin_epi_vadd_4xi16_mask(coeffs, coeffs, one_mask, mask_negative, maxElements);

		//Compare all elements with zeroes
		__epi_4xi1  are_zeroes = __builtin_epi_vmseq_4xi16(coeffs, zero_mask, maxElements);
		zeroes += __builtin_epi_vmpopc_4xi1(are_zeroes, maxElements);

		__builtin_epi_vstore_4xi16(C + i, coeffs, maxElements);
	}

	if (zeroes == matrixSize)
		*is_zero_matrix = true;
}
void TransformAndQuantizationEPIRISCV::dequantization(int16_t * A, int QP, int bitDepth, int N, int16_t * C)
{
	int M = ComUtil::logarithm2(N);
	int log2TransformRange = 15;
	int bdShift = bitDepth + M + 10 - log2TransformRange;
	int mScalingFactor = 16;

	int matrixSize = N * N;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	__epi_4xi16 mult1 = __builtin_epi_vbroadcast_4xi16(mScalingFactor, maxElements);

	__epi_2xi32 mult2 = __builtin_epi_vbroadcast_2xi32(g[QP % 6], maxElements);
	__epi_2xi32 sh_mul2 = __builtin_epi_vbroadcast_2xi32(QP / 6, maxElements);
	mult2 = __builtin_epi_vsll_2xi32(mult2, sh_mul2, maxElements);

	__epi_2xi32 shiftOffset = __builtin_epi_vbroadcast_2xi32(1 << (bdShift - 1), maxElements);
	__epi_4xi16 shift = __builtin_epi_vbroadcast_4xi16(bdShift, maxElements);

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_4xi16 coeffs = __builtin_epi_vload_4xi16(A + i, maxElements);

		__epi_2xi32 coeffs_wide = __builtin_epi_vwmul_2xi32(coeffs, mult1, maxElements);
		coeffs_wide = __builtin_epi_vmul_2xi32(coeffs_wide, mult2, maxElements);
		coeffs_wide = __builtin_epi_vadd_2xi32(coeffs_wide, shiftOffset, maxElements);

		coeffs = __builtin_epi_vnsra_4xi16_2xi32(coeffs_wide, shift, maxElements);

		__builtin_epi_vstore_4xi16(C + i, coeffs, maxElements);
	}
}

#else

TransformAndQuantizationEPIRISCV::TransformAndQuantizationEPIRISCV()
{
	//ELEN in EPI is equal to 64, so 64 is hard-coded
	vl = __builtin_epi_vsetvl(64, __epi_e16, __epi_m1);
}

void TransformAndQuantizationEPIRISCV::multiply(int16_t *A, int16_t *B, int N, int16_t *C, int bdShift)
{
	int parts = (int)ceil(N / (double)vl);
	int step = vl;
	int offset = 0;

	int shiftOffset = 1 << (bdShift - 1);

	for (int p = 0; p < parts; p++)
	{

		for (int i = 0; i < N; i++)
		{
			__epi_4xi32 result_row_wide = __builtin_epi_vbroadcast_4xi32(0, N);

			for (int j = 0; j < N; j++)
			{
				__epi_4xi16 A_element = __builtin_epi_vbroadcast_4xi16(A[i*N + j], N);
				__epi_4xi16 B_row = __builtin_epi_vload_4xi16(B + (j*N) + offset, N);

				__epi_4xi32 tmp_wide = __builtin_epi_vwmul_4xi32(A_element, B_row, N);
				result_row_wide = __builtin_epi_vadd_4xi32(tmp_wide, result_row_wide, N);
			}
			__epi_4xi32 tmp_add = __builtin_epi_vbroadcast_4xi32(shiftOffset, N);
			result_row_wide = __builtin_epi_vadd_4xi32(tmp_add, result_row_wide, N);

			__epi_4xi16 tmp_shift = __builtin_epi_vbroadcast_4xi16(bdShift, N);
			__epi_4xi16 result_row = __builtin_epi_vnsra_4xi16(result_row_wide, tmp_shift, N);
			__builtin_epi_vstore_4xi16(C + (i*N) + offset, result_row, N);

		}
		offset = +step;
	}
}

void TransformAndQuantizationEPIRISCV::quantization(int16_t * A, int QP, int NB, int N, bool * is_zero_matrix, int16_t * C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;

	int matrixSize = N * N;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	__epi_4xi16 mult = __builtin_epi_vbroadcast_4xi16(f[QP % 6], maxElements);
	__epi_4xi32 shiftOffset = __builtin_epi_vbroadcast_4xi32(1 << (bdShift - 1), maxElements);
	__epi_4xi16 shift = __builtin_epi_vbroadcast_4xi16(((QP / 6) + bdShift), maxElements);

	__epi_4xi16 zero_mask = __builtin_epi_vbroadcast_4xi16(0, maxElements);
	__epi_4xi16 ff_mask = __builtin_epi_vbroadcast_4xi16(0xFFFF, maxElements);
	__epi_4xi16 one_mask = __builtin_epi_vbroadcast_4xi16(1, maxElements);

	*is_zero_matrix = false;
	int zeroes = 0;

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_4xi16 coeffs = __builtin_epi_vload_4xi16(A + i, maxElements);

		//get absolute value
		__epi_4xi1 mask_negative = __builtin_epi_vmsgt_4xi16(zero_mask, coeffs, maxElements);
		coeffs = __builtin_epi_vxor_4xi16_mask(coeffs, coeffs, ff_mask, mask_negative, maxElements);
		coeffs = __builtin_epi_vadd_4xi16_mask(coeffs, coeffs, one_mask, mask_negative, maxElements);

		//multiply, add, shift to calculate quantization coeffs
		__epi_4xi32 coeff_wide = __builtin_epi_vwmul_4xi32(coeffs, mult, maxElements);
		coeff_wide = __builtin_epi_vadd_4xi32(coeff_wide, shiftOffset, maxElements);
		coeffs = __builtin_epi_vnsra_4xi16(coeff_wide, shift, maxElements);

		//return the sign
		coeffs = __builtin_epi_vxor_4xi16_mask(coeffs, coeffs, ff_mask, mask_negative, maxElements);
		coeffs = __builtin_epi_vadd_4xi16_mask(coeffs, coeffs, one_mask, mask_negative, maxElements);

		//Compare all elements with zeroes
		__epi_4xi1  are_zeroes = __builtin_epi_vmseq_4xi16(coeffs, zero_mask, maxElements);
		zeroes += __builtin_epi_vmpopc_4xi1(are_zeroes, maxElements);

		__builtin_epi_vstore_4xi16(C + i, coeffs, maxElements);
	}

	if (zeroes == matrixSize)
		*is_zero_matrix = true;
}
void TransformAndQuantizationEPIRISCV::dequantization(int16_t * A, int QP, int bitDepth, int N, int16_t * C)
{
	int M = ComUtil::logarithm2(N);
	int log2TransformRange = 15;
	int bdShift = bitDepth + M + 10 - log2TransformRange;
	int mScalingFactor = 16;

	int matrixSize = N * N;
	int maxElements = vl > matrixSize ? matrixSize : vl;

	__epi_4xi16 mult1 = __builtin_epi_vbroadcast_4xi16(mScalingFactor, maxElements);

	__epi_4xi32 mult2 = __builtin_epi_vbroadcast_4xi32(g[QP % 6], maxElements);
	__epi_4xi32 sh_mul2 = __builtin_epi_vbroadcast_4xi32(QP / 6, maxElements);
	mult2 = __builtin_epi_vsll_4xi32(mult2, sh_mul2, maxElements);

	__epi_4xi32 shiftOffset = __builtin_epi_vbroadcast_4xi32(1 << (bdShift - 1), maxElements);
	__epi_4xi16 shift = __builtin_epi_vbroadcast_4xi16(bdShift, maxElements);

	for (int i = 0; i < matrixSize; i = i + maxElements)
	{
		__epi_4xi16 coeffs = __builtin_epi_vload_4xi16(A + i, maxElements);

		__epi_4xi32 coeffs_wide = __builtin_epi_vwmul_4xi32(coeffs, mult1, maxElements);
		coeffs_wide = __builtin_epi_vmul_4xi32(coeffs_wide, mult2, maxElements);
		coeffs_wide = __builtin_epi_vadd_4xi32(coeffs_wide, shiftOffset, maxElements);

		coeffs = __builtin_epi_vnsra_4xi16(coeffs_wide, shift, maxElements);

		__builtin_epi_vstore_4xi16(C + i, coeffs, maxElements);
	}
}

#endif

#endif