/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Tile.h"

Tile::Tile()
{
	partitionType = PartitionType::TILE;
}

Tile::Tile(EncodingContext * _enCtx, int tId):Partition(_enCtx)
{
	partitionType = PartitionType::TILE;
	tileId = tId;
}

Tile::~Tile()
{
	if (ctuTree != nullptr)
	{
		int i;
		for (i = 0; i < numberOfCTUs; i++)
			delete ctuTree[i];
		delete[] ctuTree;
	}
}


