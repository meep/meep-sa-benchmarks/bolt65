/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __ARM_NEON
#include "ComKernelsNEON.h"

ComKernelsNEON::ComKernelsNEON()
{
}

int ComKernelsNEON::SAD(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight)
{
	uint16x8_t sad_vector = vmovq_n_u16(0);

	for (int i = 0; i < puWidth * puHeight; i = i + 16)
	{
		uint8x16_t A = vld1q_u8(block_1 + i);
		uint8x16_t B = vld1q_u8(block_2 + i);

		sad_vector = vabal_u8(sad_vector, vget_low_u8(A), vget_low_u8(B));
		sad_vector = vabal_high_u8(sad_vector, A, B);
	}

	uint32_t final_sad = vaddlvq_u16(sad_vector);

	return (int)final_sad;
}

void ComKernelsNEON::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	uint8x16_t A, B;

	uint16x8_t A_low, A_high;
	uint16x8_t B_low, B_high;

	int16x8_t A_low_16, A_high_16;
	int16x8_t B_low_16, B_high_16;

	int16x8_t C_low, C_high;

	for (int i = 0; i < blockSize * blockSize; i = i + 16)
	{
		A = vld1q_u8((unsigned char*)rawBlock + i);
		B = vld1q_u8((unsigned char*)predictedBlock + i);

		A_low = vmovl_u8(vget_low_u8(A));
		A_high = vmovl_u8(vget_high_u8(A));

		B_low = vmovl_u8(vget_low_u8(B));
		B_high = vmovl_u8(vget_high_u8(B));

		A_low_16 = vreinterpretq_s16_u16(A_low);
		A_high_16 = vreinterpretq_s16_u16(A_high);

		B_low_16 = vreinterpretq_s16_u16(B_low);
		B_high_16 = vreinterpretq_s16_u16(B_high);

		C_low = vsubq_s16(A_low_16, B_low_16);
		C_high = vsubq_s16(A_high_16, B_high_16);

		vst1q_s16(residualBlock + i, C_low);
		vst1q_s16(residualBlock + i + 8, C_high);
	}
}
#endif
