/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __ARM_FEATURE_SVE
#include "ComKernelsSVE.h"

ComKernelsSVE::ComKernelsSVE()
{
}

int ComKernelsSVE::SAD(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight)
{
	size_t svewidth = svcntb();
	int maxNum = svewidth / sizeof(unsigned char);

	int matrixSize = puHeight * puWidth;

	if (matrixSize < maxNum)
		maxNum = matrixSize;

	svbool_t predicate = svwhilelt_b8_s32(0, maxNum);

	svuint8_t orig, pred, absdiff;
	int64_t sum = 0;

	for (int i = 0; i < matrixSize; i = i + maxNum)
	{
		if (i + maxNum > matrixSize)
			predicate = svwhilelt_b8_s32(0, matrixSize - i);

		orig = svld1_u8(predicate, block_1 + i);
		pred = svld1_u8(predicate, block_2 + i);

		absdiff = svabd_u8_x(predicate, orig, pred);

		sum = sum + svaddv_u8(predicate, absdiff);
	}

	return (int)sum;
}

void ComKernelsSVE::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	size_t svewidth = svcntb();
	int maxNum = svewidth / sizeof(int16_t);

	int matrixSize = blockSize * blockSize;

	if (matrixSize < maxNum)
		maxNum = matrixSize;

	svint16_t data_raw;
	svint16_t data_predicted;
	svint16_t data_result;
	svbool_t predicate = svwhilelt_b16_s32(0, maxNum);

	for (int i = 0; i < matrixSize; i = i + maxNum)
	{
		data_raw = svld1ub_s16(predicate, (uint8_t*)rawBlock + i);
		data_predicted = svld1ub_s16(predicate, (uint8_t*)predictedBlock + i);

		data_result = svsub_s16_x(predicate, data_raw, data_predicted);

		svst1_s16(predicate, residualBlock + i, data_result);
	}
}
#endif
