/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/


#include "Intraprediction.h"

Intraprediction::Intraprediction()
{
}


void Intraprediction::initialize(EncodingContext* enCtx)
{
	intrapredictionKernels = IntrapredictionKernelsCreator::Create(enCtx);
}

void Intraprediction::clean()
{
	delete intrapredictionKernels;
}


Intraprediction::~Intraprediction()
{
}

void Intraprediction::RefSub(unsigned char* refSamples, bool* refSamplesExists, int blockSize)
{
	unsigned char i;
	// If none of the reference sample are available, reference samples are subbed with nominal average sample value: 128 for 8-bit data
	unsigned char tmp = 128;
	bool noRef = true;

	for (i = 0; i < 4 * blockSize + 1; i++)
	{
		if (!refSamplesExists[i])
		{
			refSamples[i] = tmp;
		}
		else
		{
			tmp = refSamples[i];
			if (noRef)
			{
				//fill reference samples that were bypassed at the beginning because there were no reference samples available.
				std::fill(refSamples, refSamples + i, tmp);
				noRef = false;
			}
		}
	}
}

/*-----------------------------------------------------------------------------
Function name: LinearInterpolation32x32
Author/s: Leon
Current Version: 0.1

Version history:	0.1 - The method LinearInterpolation32x32 was created. (10.5.2016.)

Description:	Second method for smoothing the reference sample array. Used in case where 32x32 prediction block is used.
				This process is referred to as strong intra smoothing as it substitutes nearly all the original reference samples.
				It is turned off or on using strong_intra_smoothing_enabled_flag.

Parameters:
Inputs:		refSamples - reference sample array to be smoothed. Input format is 1D array p[2N-1][-1] to p[-1][2N-1].

Outputs:	refSamples - smoothed reference sample array.

Returns:	Void

Comments:	Can be optimized. It is not necessary to pass the whole array. Also, the reference samples can be calculated in parallel.

-----------------------------------------------------------------------------*/

void Intraprediction::LinearInterpolation32x32(unsigned char* refSamples, unsigned char* filteredRefSamples) {



	unsigned char i;
	unsigned char c_ul = refSamples[64];
	unsigned char c_b = refSamples[0];
	unsigned char c_ur = refSamples[128];
	filteredRefSamples[0] = refSamples[0];
	filteredRefSamples[64] = refSamples[64];
	filteredRefSamples[128] = refSamples[128];

	unsigned char xy;
	//skipping corners [-1][63], [63][-1], [-1][-1].
	for (i = 1; i < 128; i++) {
		if (i < 64)
		{
			xy = 63 - i;
			filteredRefSamples[i] = ((63 - xy) * c_ul + (xy + 1) * c_b + 32) >> 6;
		}

		else
		{
			if (i > 64)
			{
				xy = i - 65;
				filteredRefSamples[i] = ((63 - xy) * c_ul + (xy + 1) * c_ur + 32) >> 6;
			}
		}
	}
}
/*
Name: Reference sample row extension for negative angular predictions
Author: Leon
Current version: 0.1

Version history:
0.1 - The method ReferenceRowExtensionForNegativePrediction was created. (15.5.2016.)

Description:
This method is used when negative prediction directions are being used in angular prediction. It is necessary to extend the reference rows above
or to the left of the reference block. The method receives reference whole reference sample array (refSamples) and then substitutes the values needed for
negative prediction. The angular parameter is used for calculating new values of reference samples depending on the prediction mode(predMode)
and the block size (blockSize).

Comments:
*/
void Intraprediction::ReferenceRowExtensionForNegativePrediction(unsigned char* refSamples, unsigned char predMode, unsigned char blockSize) {

	signed char i;

	//number of reference samples that need to be recalculated based on the angle of the prediction.
	signed char maxRef = (blockSize * AngularIntrapredictionModes[predMode][0]) >> 5;
	if (predMode < 16) // if the prediction is horizontal, reference samples need to be extended above.
	{
		for (i = -1; i > maxRef; i--)
		{
			char tempRef = ((i * AngularIntrapredictionModes[predMode][1] + 128) >> 8);
			refSamples[2 * blockSize - i] = refSamples[2 * blockSize + tempRef];
		}
	}
	else // if the prediction is vertical, reference samples need to be extended to the left.
	{
		for (i = -1; i > maxRef; i--)
		{
			char tempRef = ((i * AngularIntrapredictionModes[predMode][1] + 128) >> 8);
			refSamples[2 * blockSize + i] = refSamples[2 * blockSize - tempRef];
		}
	}
}

void Intraprediction::FindBestPredictionIntra(CU* cu, Partition* partition, EncodingContext* enCtx)
{
	cu->PartMode = INTRA_PART_2Nx2N;
	cu->InitializeTransformTree(cu->cbY.blockSize);

	//check for implicit split
	if (cu->cbY.blockSize == 64)
	{
		//create tu split which is equal tu pu in intra mode.
		cu->transformTree->splitTU(cu->partitionWidth, cu->frameWidth);
		return;
	}
	else if (cu->cbY.blockSize == 8 && enCtx->useIntraTU)
	{
		cu->PartMode = INTRA_PART_2Nx2N;
		return;

		/*TODO */
		//When checking for the best candidates in INTA_NxN mode there has to be entire cycle of reconstruction implemented
		//The reason for that is that TUs with index 2 and 3 do not have valid referenceSamples if the TUs with index 0 and 1 are not completely reconsturcted
		//The time that is spent for finding all compinations of Intra samples (35) for all 4 TUs is not worth the effort, since INTRA_PART_NxN can occur only in CUs with smallest block size


		//Only for the smallest block there is possibility of INTRA_PART_NxN
		unsigned char* refSamplesY = new unsigned char[4 * 64 + 1];
		bool* refSamplesYExist = new bool[(4 * 64) + 1];
		unsigned char* predictedBlockY = new unsigned char[64 * 64];
		unsigned char* blockY = new unsigned char[64 * 64];

		//First check for INTRA PART_2Nx2N
		BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockY, cu->transformTree->tbY.startingIndexInFrame, cu->transformTree->tbY.transformBlockSize, cu->transformTree->tbY.transformBlockSize, enCtx->width, enCtx->height);
		BlockPartition::getReferenceSamples(cu->transformTree->tbY.startingIndexInFrame, cu->transformTree->tbY.startingIndex, cu->transformTree->tbY.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesY, refSamplesYExist);

		//find best candidate from intra prediction modes.
		int blockMatchingValue, bestBMC_2Nx2N, bestBMC_NxN, bestI;
		int bestBMC_NxN_perTU[4] = { -1,-1,-1,-1 };
		int bestI_NxN_perTU[4] = { -1,-1,-1,-1 };

		blockMatchingValue = 0;
		bestBMC_2Nx2N = -1;
		bestBMC_NxN = 0;
		bestI = -1;

		for (int i = 0; i < 35; i++)
		{
			// LUMA COMPONENT searches for best candidate
			GenerateIntraPredictedBlock(i, refSamplesY, refSamplesYExist, predictedBlockY, 0, cu->transformTree->tbY.transformBlockSize);
			int blockMatchingValue = ComUtil::CalculateBlockMatchingValue(blockY, predictedBlockY, cu->transformTree->tbY.transformBlockSize, cu->transformTree->tbY.transformBlockSize, enCtx);

			if (blockMatchingValue < bestBMC_2Nx2N || bestBMC_2Nx2N < 0)
			{
				bestBMC_2Nx2N = blockMatchingValue;
				bestI = i;
			}
		}

		cu->transformTree->splitTU(cu->partitionWidth, cu->frameWidth);
		blockMatchingValue = 0;

		for (int i = 0; i < 4; i++) //pretpostavljamo da je za intra pu = tu uvijek
		{

			BlockPartition::get1dBlockByStartingIndex((char*)partition->payload, blockY, cu->transformTree->children[i]->tbY.startingIndexInFrame, cu->transformTree->children[i]->tbY.transformBlockSize, cu->transformTree->children[i]->tbY.transformBlockSize, enCtx->width, enCtx->height);
			BlockPartition::getReferenceSamples(cu->transformTree->children[i]->tbY.startingIndexInFrame, cu->transformTree->children[i]->tbY.startingIndex, cu->transformTree->children[i]->tbY.transformBlockSize, enCtx->width, enCtx->height, partition->width, partition->height, partition->reconstructed_payload, partition->isReconstructed, refSamplesY, refSamplesYExist);

			for (int j = 0; j < 35; j++)
			{
				GenerateIntraPredictedBlock(j, refSamplesY, refSamplesYExist, predictedBlockY, 0, cu->transformTree->children[i]->tbY.transformBlockSize);
				int blockMatchingValue = ComUtil::CalculateBlockMatchingValue(blockY, predictedBlockY, cu->transformTree->children[i]->tbY.transformBlockSize, cu->transformTree->children[i]->tbY.transformBlockSize, enCtx);

				if (blockMatchingValue < bestBMC_NxN_perTU[i] || bestBMC_NxN_perTU[i] < 0)
				{
					bestBMC_NxN_perTU[i] = blockMatchingValue;
					bestI_NxN_perTU[i] = j;
				}
			}

			bestBMC_NxN += bestBMC_NxN_perTU[i];
		}

		delete[] refSamplesY;
		delete[] refSamplesYExist;
		delete[] blockY;
		delete[] predictedBlockY;

		if (bestBMC_NxN < bestBMC_2Nx2N)
			cu->PartMode = INTRA_PART_NxN;
		else
			cu->transformTree->cleanTU();
	}
}

void Intraprediction::GenerateIntraPredictedBlock(int predModeIntra, unsigned char* refSamples, bool* refSamplesExist, unsigned char* predictedBlock, int cIdx, int blockSize)
{
	int i = predModeIntra;
	bool filterFlag = true;
	unsigned char* filteredRefSamples = new unsigned char[4 * blockSize + 1];
	unsigned char* negExtendedRefSamples = new unsigned char[4 * blockSize + 1];

	RefSub(refSamples, refSamplesExist, blockSize);

	if (i == 0)
	{
		if (cIdx == 0 && blockSize != 4)
		{
			intrapredictionKernels->ThreeTapRefFilter(refSamples, filteredRefSamples, blockSize);
			intrapredictionKernels->PlanarPrediction(filteredRefSamples, predictedBlock, blockSize);
		}
		else
		{
			intrapredictionKernels->PlanarPrediction(refSamples, predictedBlock, blockSize);
		}
	}
	else if (i == 1)
	{
		intrapredictionKernels->DCPrediction(refSamples, predictedBlock, cIdx, blockSize);
	}
	else
	{
		if (blockSize == 32)
		{
			if (i == 10 || i == 26)
			{
				intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, 0, blockSize);
			}
			else
			{
				if (cIdx == 0)
				{
					intrapredictionKernels->ThreeTapRefFilter(refSamples, filteredRefSamples, blockSize);
					if (i > 10 && i < 26)
					{
						ReferenceRowExtensionForNegativePrediction(filteredRefSamples, i - 2, blockSize);
					}
					intrapredictionKernels->AngularPrediction(filteredRefSamples, i - 2, predictedBlock, cIdx, blockSize);
				}
				else
				{
					if (i > 10 && i < 26)
					{
						copy(refSamples, refSamples + 4 * blockSize + 1, negExtendedRefSamples);
						ReferenceRowExtensionForNegativePrediction(negExtendedRefSamples, i - 2, blockSize);
						intrapredictionKernels->AngularPrediction(negExtendedRefSamples, i - 2, predictedBlock, cIdx, blockSize);
					}
					else
						intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, cIdx, blockSize);
				}
			}
		}
		else if (blockSize == 16)
		{
			// Application of smoothing filter if it is not near-horizontal or near-vertical direction
			if (i == 9 || i == 10 || i == 11 || i == 25 || i == 26 || i == 27)
			{
				if (i > 10 && i < 26)
				{
					copy(refSamples, refSamples + 4 * blockSize + 1, negExtendedRefSamples);
					ReferenceRowExtensionForNegativePrediction(negExtendedRefSamples, i - 2, blockSize);
					intrapredictionKernels->AngularPrediction(negExtendedRefSamples, i - 2, predictedBlock, cIdx, blockSize);
				}
				else
					intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, cIdx, blockSize);
			}
			else
			{
				if (cIdx == 0)
				{
					intrapredictionKernels->ThreeTapRefFilter(refSamples, filteredRefSamples, blockSize);
					if (i > 10 && i < 26)
					{
						ReferenceRowExtensionForNegativePrediction(filteredRefSamples, i - 2, blockSize);
					}

					intrapredictionKernels->AngularPrediction(filteredRefSamples, i - 2, predictedBlock, cIdx, blockSize);
				}
				else
				{
					if (i > 10 && i < 26)
					{
						copy(refSamples, refSamples + 4 * blockSize + 1, negExtendedRefSamples);
						ReferenceRowExtensionForNegativePrediction(negExtendedRefSamples, i - 2, blockSize);
						intrapredictionKernels->AngularPrediction(negExtendedRefSamples, i - 2, predictedBlock, cIdx, blockSize);
					}
					else
						intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, cIdx, blockSize);
				}

			}
		}
		else if (blockSize == 8)
		{
			// Application of smoothing filter if it is near-horizontal or near-vertical direction
			if ((i >= 3 && i <= 17) || (i >= 19 && i <= 33))
			{
				if (i > 10 && i < 26)
				{
					copy(refSamples, refSamples + 4 * blockSize + 1, negExtendedRefSamples);
					ReferenceRowExtensionForNegativePrediction(negExtendedRefSamples, i - 2, blockSize);
					intrapredictionKernels->AngularPrediction(negExtendedRefSamples, i - 2, predictedBlock, cIdx, blockSize);
				}
				else
					intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, cIdx, blockSize);
			}
			else
			{
				if (cIdx == 0)
				{
					intrapredictionKernels->ThreeTapRefFilter(refSamples, filteredRefSamples, blockSize);
					if (i > 10 && i < 26)
					{
						ReferenceRowExtensionForNegativePrediction(filteredRefSamples, i - 2, blockSize);
					}
					intrapredictionKernels->AngularPrediction(filteredRefSamples, i - 2, predictedBlock, cIdx, blockSize);
				}
				else
				{
					if (i > 10 && i < 26)
					{
						copy(refSamples, refSamples + 4 * blockSize + 1, negExtendedRefSamples);
						ReferenceRowExtensionForNegativePrediction(negExtendedRefSamples, i - 2, blockSize);
						intrapredictionKernels->AngularPrediction(negExtendedRefSamples, i - 2, predictedBlock, cIdx, blockSize);
					}
					else
						intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, cIdx, blockSize);
				}

			}
		}
		else if (blockSize == 4)
		{
			// Application of smoothing filter if it is near-horizontal or near-vertical direction
			//There is no smoothing filter on 4x4 blocks
			if (i > 10 && i < 26)
			{
				copy(refSamples, refSamples + 4 * blockSize + 1, negExtendedRefSamples);
				ReferenceRowExtensionForNegativePrediction(negExtendedRefSamples, i - 2, blockSize);
				intrapredictionKernels->AngularPrediction(negExtendedRefSamples, i - 2, predictedBlock, cIdx, blockSize);
			}
			else
				intrapredictionKernels->AngularPrediction(refSamples, i - 2, predictedBlock, cIdx, blockSize);
		}
	}

	delete[] filteredRefSamples;
	delete[] negExtendedRefSamples;
}

void Intraprediction::DeriveCandidatesForIntraPrediction(int* candModeList, CU* cu, int partitionWidth)
{
	bool leftIsAvailable = cu->isLeftCodingUnitAvailable(partitionWidth);
	bool aboveIsAvailable = cu->isAboveCodingUnitAvailable(partitionWidth);
	CU* A = nullptr;
	CU* B = nullptr;

	if (leftIsAvailable)
		A = cu->getLeftCodingUnit();
	if (aboveIsAvailable)
		B = cu->getAboveCodingUnit();

	int candModeA;
	int candModeB;

	if (aboveIsAvailable)
	{
		if (B->CuPredMode != MODE_INTRA || B->PcmFlag == true || BlockPartition::isCornerUpperInCTU(cu))
		{
			candModeB = 1;
		}
		else
		{
			candModeB = B->IntraPredModeY[0];
		}


	}
	else
	{
		candModeB = 1;
	}

	if (leftIsAvailable)
	{
		if (A->CuPredMode != MODE_INTRA || A->PcmFlag == true)
		{
			candModeA = 1;
		}
		else
		{
			candModeA = A->IntraPredModeY[0];
		}
	}
	else
	{
		candModeA = 1;
	}


	if (candModeA == candModeB)
	{
		if (candModeA < 2)
		{
			candModeList[0] = 0;
			candModeList[1] = 1;
			candModeList[2] = 26;
		}
		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = 2 + ((candModeA + 29) % 32);
			candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
		}
	}

	else
	{
		candModeList[0] = candModeA;
		candModeList[1] = candModeB;

		if (candModeList[0] != 0 && candModeList[1] != 0)
		{
			candModeList[2] = 0;
		}
		else
		{
			if (candModeList[0] != 1 && candModeList[1] != 1)
			{
				candModeList[2] = 1;
			}
			else
			{
				candModeList[2] = 26;
			}
		}
	}
}

void Intraprediction::DeriveCandidatesForIntraPredictionPARTNxN(int* candModeList, CU* cu, int part, int partitionWidth)
{
	if (part == 0)
	{
		bool leftIsAvailable = cu->isLeftCodingUnitAvailable(partitionWidth);
		bool aboveIsAvailable = cu->isAboveCodingUnitAvailable(partitionWidth);
		CU* A = nullptr;
		CU* B = nullptr;

		if (leftIsAvailable)
			A = cu->getLeftCodingUnit();
		if (aboveIsAvailable)
			B = cu->getAboveCodingUnit();

		int candModeA;
		int candModeB;

		if (aboveIsAvailable)
		{
			if (B->CuPredMode != MODE_INTRA || B->PcmFlag == true || BlockPartition::isCornerUpperInCTU(cu))
			{
				candModeB = 1;
			}
			else
			{
				if (B->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeB = B->IntraPredModeY[2];
				}
				else
					candModeB = B->IntraPredModeY[0];
			}


		}
		else
		{
			candModeB = 1;
		}

		if (leftIsAvailable)
		{
			if (A->CuPredMode != MODE_INTRA || A->PcmFlag == true)
			{
				candModeA = 1;
			}
			else
			{
				if (A->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeA = A->IntraPredModeY[2];
				}
				else
					candModeA = A->IntraPredModeY[0];
			}
		}
		else
		{
			candModeA = 1;
		}


		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}

	}
	else if (part == 1)
	{
		bool leftIsAvailable = true;
		bool aboveIsAvailable = cu->isAboveCodingUnitAvailable(partitionWidth);
		CU* B = nullptr;

		if (aboveIsAvailable)
			B = cu->getAboveCodingUnit();

		int candModeA;
		int candModeB;

		if (aboveIsAvailable)
		{
			if (B->CuPredMode != MODE_INTRA || B->PcmFlag == true || BlockPartition::isCornerUpperInCTU(cu))
			{
				candModeB = 1;
			}
			else
			{
				if (B->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeB = B->IntraPredModeY[3];
				}
				else
					candModeB = B->IntraPredModeY[0];
			}


		}
		else
		{
			candModeB = 1;
		}

		candModeA = cu->IntraPredModeY[0];

		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}
	}
	else if (part == 2)
	{
		bool leftIsAvailable = cu->isLeftCodingUnitAvailable(partitionWidth);
		bool aboveIsAvailable = true;
		CU* A = nullptr;

		if (leftIsAvailable)
			A = cu->getLeftCodingUnit();


		int candModeA;
		int candModeB;


		candModeB = cu->IntraPredModeY[0];


		if (leftIsAvailable)
		{
			if (A->CuPredMode != MODE_INTRA || A->PcmFlag == true)
			{
				candModeA = 1;
			}
			else
			{
				if (A->PartMode == PartitionModeIntra::INTRA_PART_NxN)
				{
					candModeA = A->IntraPredModeY[3];
				}
				else
					candModeA = A->IntraPredModeY[0];
			}
		}
		else
		{
			candModeA = 1;
		}


		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}
	}
	else
	{
		int candModeA;
		int candModeB;

		candModeB = cu->IntraPredModeY[1];

		candModeA = cu->IntraPredModeY[2];

		if (candModeA == candModeB)
		{
			if (candModeA < 2)
			{
				candModeList[0] = 0;
				candModeList[1] = 1;
				candModeList[2] = 26;
			}
			else
			{
				candModeList[0] = candModeA;
				candModeList[1] = 2 + ((candModeA + 29) % 32);
				candModeList[2] = 2 + ((candModeA - 2 + 1) % 32);
			}
		}

		else
		{
			candModeList[0] = candModeA;
			candModeList[1] = candModeB;

			if (candModeList[0] != 0 && candModeList[1] != 0)
			{
				candModeList[2] = 0;
			}
			else
			{
				if (candModeList[0] != 1 && candModeList[1] != 1)
				{
					candModeList[2] = 1;
				}
				else
				{
					candModeList[2] = 26;
				}
			}
		}
	}

}

