/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "TransformAndQuantizationCPU.h"

TransformAndQuantizationCPU::TransformAndQuantizationCPU()
{
}

void TransformAndQuantizationCPU::transpose(int16_t* A, int N, int16_t* C)
{
	for (int i = 0; i < N; i++) {
		for (int j = i; j < N; j++) {
			*(C + (N * i) + j) = *(A + (N * j) + i);
			*(C + (N * j) + i) = *(A + (N * i) + j);
		}
	}
}

void TransformAndQuantizationCPU::multiply(int16_t *A, int16_t* B, int N, int16_t *C, int bdShift)
{
	for (int r = 0; r < N; r++) {
		for (int c = 0; c < N; c++) {
			int tmp = 0;
			for (int p = 0; p < N; p++) {
				tmp = tmp + A[r * N + p] * B[p * N + c];
			}
			C[r * N + c] = (tmp + (1 << (bdShift - 1))) >> bdShift;
		}
	}
}

void TransformAndQuantizationCPU::add(int* A, int scalar, int N, int* C)
{
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			*(C + i * N + j) = *(A + i * N + j) + scalar;

}

void TransformAndQuantizationCPU::quantization(int16_t *A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int mScalingFactor = 16;
	int i;
	*is_zero_matrix = true;

	for (i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			int sign = 1;
			if (*(A + i * N + j) < 0)
				sign = -1;

			*(C + i * N + j) = (((abs(*(A + i * N + j)) * f[QP % 6] + (1 << (bdShift - 1))) >> QP / 6) >> bdShift);
			*(C + i * N + j) = ComUtil::clip3(coeffMin, coeffMax, *(C + i * N + j) * sign);

			if (*(C + i * N + j) != 0)
				*is_zero_matrix = false;
		}
	}


}

void TransformAndQuantizationCPU::dequantization(int16_t* A, int QP, int bitDepth, int N, int16_t* C)
{
	int log2TransformRange = 15;
	int bdShift = bitDepth + ComUtil::logarithm2(N) + 10 - log2TransformRange;
	int coeffMin = -32768;
	int coeffMax = 32767;
	int mScalingFactor = 16;
	int i;

	for (i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			*(C + i * N + j) = ComUtil::clip3(coeffMin, coeffMax, ((*(A + i * N + j) * mScalingFactor * g[QP % 6] << (QP / 6)) + (1 << (bdShift - 1))) >> bdShift);
		}
	}
}

void TransformAndQuantizationCPU::dct_ii(int N, int16_t *in, int16_t* X) {

	const double PI = 3.1415926535897;
	double temp;
	int i, k, y, x;
	for (i = 0; i < N; ++i)
	{
		for (k = 0; k < N; ++k)
		{
			double sum = 0.;
			for (y = 0; y < N; y++)
			{
				for (x = 0; x < N; x++)
				{
					sum = sum + (in[y*N + x] * (cos((PI * (2. * y + 1.) * i) / (2. * N)) * (cos((PI * (2. * x + 1.) * k) / (2. * N)))));
				}
			}

			double s = (k == 0) ? 1. / sqrt(N) : sqrt(2. / N);
			double t = (i == 0) ? 1. / sqrt(N) : sqrt(2. / N);
			temp = s * t * sum * 4;
			if (temp > 0)
				X[i*N + k] = (int)(temp + 0.5);
			else if (temp < 0)
				X[i*N + k] = (int)(temp - 0.5);

		}
	}
}
