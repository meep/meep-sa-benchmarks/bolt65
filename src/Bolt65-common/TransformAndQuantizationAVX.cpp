/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __AVX2__

#include "TransformAndQuantizationAVX.h"

TransformAndQuantizationAVX::TransformAndQuantizationAVX()
{
}

void TransformAndQuantizationAVX::quantization(int16_t *A, int QP, int NB, int N, bool* is_zero_matrix, int16_t *C)
{
	int M = ComUtil::logarithm2(N);

	int bdShift = 29 - M - NB;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int mScalingFactor = 16;
	int i;
	*is_zero_matrix = true;

	__m256i mult, offset, temp0, temp1, all_1;


	mult = _mm256_set1_epi32(f[QP % 6]);
	offset = _mm256_set1_epi32(1 << (bdShift - 1));
	all_1 = _mm256_set1_epi32(UINT32_MAX);

	for (i = 0; i < N*N; i = i + 16)
	{
		__m256i _a0 = _mm256_loadu_si256((__m256i *) &A[i]);
		__m256i _a1;

		__m128i _a_lo = _mm256_extracti128_si256(_a0, 0);
		__m128i _a_hi = _mm256_extracti128_si256(_a0, 1);

		_a0 = _mm256_cvtepi16_epi32(_a_lo);
		_a1 = _mm256_cvtepi16_epi32(_a_hi);

		__m256i _b0 = _mm256_sign_epi32(_a0, _a0);
		__m256i _b1 = _mm256_sign_epi32(_a1, _a1);


		temp0 = _mm256_mullo_epi32(mult, _b0);
		temp0 = _mm256_add_epi32(offset, temp0);
		temp0 = _mm256_srai_epi32(temp0, (QP / 6) + bdShift);
		temp0 = _mm256_sign_epi32(temp0, _a0);

		temp1 = _mm256_mullo_epi32(mult, _b1);
		temp1 = _mm256_add_epi32(offset, temp1);
		temp1 = _mm256_srai_epi32(temp1, (QP / 6) + bdShift);
		temp1 = _mm256_sign_epi32(temp1, _a1);

		if (_mm256_testz_si256(all_1, temp0) == 0)
			*is_zero_matrix = false;

		if (_mm256_testz_si256(all_1, temp1) == 0)
			*is_zero_matrix = false;

		temp0 = _mm256_packs_epi32(temp0, temp1);
		temp0 = _mm256_permute4x64_epi64(temp0, 0xD8);

		_mm256_storeu_si256((__m256i *) &C[i], temp0);

	}


}

void TransformAndQuantizationAVX::dequantization(int16_t* A, int QP, int bitDepth, int N, int16_t* C)
{
	int M = ComUtil::logarithm2(N);

	int log2TransformRange = 15;
	int bdShift = bitDepth + M + 10 - log2TransformRange;
	int coeffMin = -32768;
	int coeffMax = 32767;
	int mScalingFactor = 16;
	int i;

	__m256i mult, mult2, offset, temp0, temp1;


	mult = _mm256_set1_epi32(mScalingFactor);
	mult2 = _mm256_set1_epi32(g[QP % 6]);
	mult2 = _mm256_slli_epi32(mult2, QP / 6);
	offset = _mm256_set1_epi32(1 << (bdShift - 1));

	for (i = 0; i < N*N; i = i + 16)
	{
		__m256i _a0 = _mm256_loadu_si256((__m256i *) &A[i]);
		__m256i _a1;

		__m128i _a_lo = _mm256_extracti128_si256(_a0, 0);
		__m128i _a_hi = _mm256_extracti128_si256(_a0, 1);

		_a0 = _mm256_cvtepi16_epi32(_a_lo);
		_a1 = _mm256_cvtepi16_epi32(_a_hi);

		temp0 = _mm256_mullo_epi32(mult, _a0);
		temp0 = _mm256_mullo_epi32(mult2, temp0);
		temp0 = _mm256_add_epi32(offset, temp0);
		temp0 = _mm256_srai_epi32(temp0, bdShift);

		temp1 = _mm256_mullo_epi32(mult, _a1);
		temp1 = _mm256_mullo_epi32(mult2, temp1);
		temp1 = _mm256_add_epi32(offset, temp1);
		temp1 = _mm256_srai_epi32(temp1, bdShift);

		temp0 = _mm256_packs_epi32(temp0, temp1);
		temp0 = _mm256_permute4x64_epi64(temp0, 0xD8);

		_mm256_storeu_si256((__m256i *) &C[i], temp0);

	}

}

void TransformAndQuantizationAVX::multiply(int16_t* A, int16_t* B, int N, int16_t* C, int bdShift)
{

	int shiftOffset = 1 << (bdShift - 1);
	if (N == 4)
	{
		__m256i temp;
		__m256i r_32[4];
		r_32[0] = _mm256_loadu_si256((__m256i *) B);
		r_32[1] = _mm256_permute4x64_epi64(r_32[0], 0xB1);

		// 22 44
		temp = _mm256_blend_epi32(r_32[0], r_32[1], 0x33);

		//22
		__m128i r0_16 = _mm256_extracti128_si256(temp, 0);

		//44
		__m128i r1_16 = _mm256_extracti128_si256(temp, 1);

		// 11 33
		temp = _mm256_blend_epi32(r_32[0], r_32[1], 0xCC);

		r_32[1] = _mm256_cvtepi16_epi32(r0_16);
		r_32[3] = _mm256_cvtepi16_epi32(r1_16);

		// 11
		r0_16 = _mm256_extracti128_si256(temp, 0);

		// 33
		r1_16 = _mm256_extracti128_si256(temp, 1);

		r_32[0] = _mm256_cvtepi16_epi32(r0_16);
		r_32[2] = _mm256_cvtepi16_epi32(r1_16);

		__m256i c[2];
		c[0] = _mm256_set1_epi32(0);
		c[1] = _mm256_set1_epi32(0);

		for (int i = 0; i < N; i = i + 2)
		{
			__m256i mult, mult1;
			__m128i mult0;

			mult0 = _mm_set1_epi32(A[i * N]);
			mult1 = _mm256_set1_epi32(A[(i + 1) * N]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[0]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);

			mult0 = _mm_set1_epi32(A[i * N + 1]);
			mult1 = _mm256_set1_epi32(A[(i + 1) * N + 1]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[1]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);

			mult0 = _mm_set1_epi32(A[i * N + 2]);
			mult1 = _mm256_set1_epi32(A[((i + 1) * N + 2)]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[2]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);

			mult0 = _mm_set1_epi32(A[i  * N + 3]);
			mult1 = _mm256_set1_epi32(A[((i + 1) * N + 3)]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[3]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);
		}

		temp = _mm256_set1_epi32(shiftOffset);
		c[0] = _mm256_add_epi32(c[0], temp);
		c[0] = _mm256_srai_epi32(c[0], bdShift);

		c[1] = _mm256_add_epi32(c[1], temp);
		c[1] = _mm256_srai_epi32(c[1], bdShift);

		c[0] = _mm256_packs_epi32(c[0], c[1]);
		c[0] = _mm256_permute4x64_epi64(c[0], 0xD8);
		_mm256_storeu_si256((__m256i *) C, c[0]);

	}

	if (N == 8)
	{
		//first 2 rows 16 bit.
		__m256i r_32[8];

		//load B matrix into registers
		for (int i = 0; i < N; i = i + 2)
		{
			r_32[i] = _mm256_loadu_si256((__m256i *) &B[i * 8]);
			__m128i r0 = _mm256_extracti128_si256(r_32[i], 0);
			__m128i r1 = _mm256_extracti128_si256(r_32[i], 1);
			r_32[i] = _mm256_cvtepi16_epi32(r0);
			r_32[i + 1] = _mm256_cvtepi16_epi32(r1);
		}


		for (int i = 0; i < N; i = i + 2)
		{
			__m256i temp;
			__m256i c0 = _mm256_set1_epi32(0);
			__m256i c1 = _mm256_set1_epi32(0);

			for (int j = 0; j < N; j++)
			{

				__m256i mult;



				mult = _mm256_set1_epi32(A[i * N + j]);
				temp = _mm256_mullo_epi32(mult, r_32[j]);
				c0 = _mm256_add_epi32(c0, temp);

				mult = _mm256_set1_epi32(A[(i + 1) * N + j]);
				temp = _mm256_mullo_epi32(mult, r_32[j]);
				c1 = _mm256_add_epi32(c1, temp);
			}

			temp = _mm256_set1_epi32(shiftOffset);
			c0 = _mm256_add_epi32(c0, temp);
			c0 = _mm256_srai_epi32(c0, bdShift);

			c1 = _mm256_add_epi32(c1, temp);
			c1 = _mm256_srai_epi32(c1, bdShift);

			c0 = _mm256_packs_epi32(c0, c1);
			c0 = _mm256_permute4x64_epi64(c0, 0xD8);

			_mm256_storeu_si256((__m256i *) &C[i * 8], c0);
		}
	}

	else if (N == 16)
	{

		for (int i = 0; i < N; i++)
		{
			__m256i c0 = _mm256_set1_epi32(0);
			__m256i c1 = _mm256_set1_epi32(0);

			__m256i mult;
			__m256i temp0, temp1;


			for (int j = 0; j < N; j++)
			{

				mult = _mm256_set1_epi32(A[i * 16 + j]);

				__m256i _b0 = _mm256_loadu_si256((__m256i *) &B[j * 16]);
				__m256i _b1;

				//lower 128 bits
				__m128i lo = _mm256_extracti128_si256(_b0, 0);

				//higher 128 bits
				__m128i hi = _mm256_extracti128_si256(_b0, 1);

				_b0 = _mm256_cvtepi16_epi32(lo);
				temp0 = _mm256_mullo_epi32(mult, _b0);
				c0 = _mm256_add_epi32(c0, temp0);

				_b1 = _mm256_cvtepi16_epi32(hi);
				temp1 = _mm256_mullo_epi32(mult, _b1);
				c1 = _mm256_add_epi32(c1, temp1);
			}

			temp0 = _mm256_set1_epi32(shiftOffset);
			temp1 = _mm256_set1_epi32(shiftOffset);
			c0 = _mm256_add_epi32(c0, temp0);
			c0 = _mm256_srai_epi32(c0, bdShift);

			c1 = _mm256_add_epi32(c1, temp1);
			c1 = _mm256_srai_epi32(c1, bdShift);

			c0 = _mm256_packs_epi32(c0, c1);

			c0 = _mm256_permute4x64_epi64(c0, 0xD8);

			_mm256_storeu_si256((__m256i *) &C[i * 16], c0);

		}

	}

	else if (N == 32)
	{

		for (int i = 0; i < N; i++)
		{
			__m256i c0 = _mm256_set1_epi32(0);
			__m256i c1 = _mm256_set1_epi32(0);
			__m256i c2 = _mm256_set1_epi32(0);
			__m256i c3 = _mm256_set1_epi32(0);

			__m256i mult;
			__m256i temp0, temp1, temp2, temp3;

			for (int j = 0; j < N; j++)
			{
				__m256i _b0 = _mm256_loadu_si256((__m256i *) &B[j * 32]);
				__m256i _b1;
				__m256i _b2 = _mm256_loadu_si256((__m256i *) &B[j * 32 + 16]);
				__m256i _b3;
				//lower 128 bits
				__m128i lo = _mm256_extracti128_si256(_b0, 0);

				//higher 128 bits
				__m128i hi = _mm256_extracti128_si256(_b0, 1);

				_b0 = _mm256_cvtepi16_epi32(lo);
				_b1 = _mm256_cvtepi16_epi32(hi);

				lo = _mm256_extracti128_si256(_b2, 0);
				hi = _mm256_extracti128_si256(_b2, 1);

				_b2 = _mm256_cvtepi16_epi32(lo);
				_b3 = _mm256_cvtepi16_epi32(hi);

				mult = _mm256_set1_epi32(A[i * 32 + j]);

				temp0 = _mm256_mullo_epi32(mult, _b0);
				temp1 = _mm256_mullo_epi32(mult, _b1);
				temp2 = _mm256_mullo_epi32(mult, _b2);
				temp3 = _mm256_mullo_epi32(mult, _b3);

				c0 = _mm256_add_epi32(c0, temp0);
				c1 = _mm256_add_epi32(c1, temp1);
				c2 = _mm256_add_epi32(c2, temp2);
				c3 = _mm256_add_epi32(c3, temp3);
			}

			temp0 = _mm256_set1_epi32(shiftOffset);
			temp1 = _mm256_set1_epi32(shiftOffset);
			temp2 = _mm256_set1_epi32(shiftOffset);
			temp3 = _mm256_set1_epi32(shiftOffset);

			c0 = _mm256_add_epi32(c0, temp0);
			c0 = _mm256_srai_epi32(c0, bdShift);

			c1 = _mm256_add_epi32(c1, temp1);
			c1 = _mm256_srai_epi32(c1, bdShift);

			c2 = _mm256_add_epi32(c2, temp2);
			c2 = _mm256_srai_epi32(c2, bdShift);

			c3 = _mm256_add_epi32(c3, temp3);
			c3 = _mm256_srai_epi32(c3, bdShift);

			c0 = _mm256_packs_epi32(c0, c1);
			c2 = _mm256_packs_epi32(c2, c3);

			c0 = _mm256_permute4x64_epi64(c0, 0xD8);
			c2 = _mm256_permute4x64_epi64(c2, 0xD8);

			_mm256_storeu_si256((__m256i *) &C[i * 32], c0);
			_mm256_storeu_si256((__m256i *) &C[i * 32 + 16], c2);
		}

	}
}

// transposing working only for 32-bit samples
void TransformAndQuantizationAVX::transpose4x4(int* mat, int* matT) {
	__m256i  r0, r1;
	__m256i  t0, t1;

	r0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 4])), _mm_loadu_si128((__m128i*)&mat[2 * 4]), 1);
	r1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 4])), _mm_loadu_si128((__m128i*)&mat[3 * 4]), 1);

	t0 = _mm256_unpacklo_epi32(r0, r1);
	t1 = _mm256_unpackhi_epi32(r0, r1);

	r0 = _mm256_permute4x64_epi64(t0, 0xD8);
	r1 = _mm256_permute4x64_epi64(t1, 0xD8);

	_mm256_storeu_si256((__m256i*)&matT[0 * 8], r0);
	_mm256_storeu_si256((__m256i*)&matT[1 * 8], r1);


}

void TransformAndQuantizationAVX::transpose8x8(int* mat, int* matT) {
	__m256i  r0, r1, r2, r3, r4, r5, r6, r7;
	__m256i  t0, t1, t2, t3, t4, t5, t6, t7;

	r0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 8 + 0])), _mm_loadu_si128((__m128i*)&mat[4 * 8 + 0]), 1);
	r1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 8 + 0])), _mm_loadu_si128((__m128i*)&mat[5 * 8 + 0]), 1);
	r2 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[2 * 8 + 0])), _mm_loadu_si128((__m128i*)&mat[6 * 8 + 0]), 1);
	r3 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[3 * 8 + 0])), _mm_loadu_si128((__m128i*)&mat[7 * 8 + 0]), 1);
	r4 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 8 + 4])), _mm_loadu_si128((__m128i*)&mat[4 * 8 + 4]), 1);
	r5 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 8 + 4])), _mm_loadu_si128((__m128i*)&mat[5 * 8 + 4]), 1);
	r6 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[2 * 8 + 4])), _mm_loadu_si128((__m128i*)&mat[6 * 8 + 4]), 1);
	r7 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[3 * 8 + 4])), _mm_loadu_si128((__m128i*)&mat[7 * 8 + 4]), 1);

	t0 = _mm256_unpacklo_epi32(r0, r1);
	t1 = _mm256_unpackhi_epi32(r0, r1);
	t2 = _mm256_unpacklo_epi32(r2, r3);
	t3 = _mm256_unpackhi_epi32(r2, r3);
	t4 = _mm256_unpacklo_epi32(r4, r5);
	t5 = _mm256_unpackhi_epi32(r4, r5);
	t6 = _mm256_unpacklo_epi32(r6, r7);
	t7 = _mm256_unpackhi_epi32(r6, r7);

	__m256 v;

	//r0 = _mm256_shuffle_ps(t0,t2, 0x44);
	//r1 = _mm256_shuffle_ps(t0,t2, 0xEE);  
	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t0), _mm256_castsi256_ps(t2), 0x4E);
	r0 = _mm256_blend_epi32(t0, _mm256_castps_si256(v), 0xCC);
	r1 = _mm256_blend_epi32(t2, _mm256_castps_si256(v), 0x33);

	//r2 = _mm256_shuffle_ps(t1,t3, 0x44);
	//r3 = _mm256_shuffle_ps(t1,t3, 0xEE);
	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t1), _mm256_castsi256_ps(t3), 0x4E);
	r2 = _mm256_blend_epi32(t1, _mm256_castps_si256(v), 0xCC);
	r3 = _mm256_blend_epi32(t3, _mm256_castps_si256(v), 0x33);

	//r4 = _mm256_shuffle_ps(t4,t6, 0x44);
	//r5 = _mm256_shuffle_ps(t4,t6, 0xEE);
	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t4), _mm256_castsi256_ps(t6), 0x4E);
	r4 = _mm256_blend_epi32(t4, _mm256_castps_si256(v), 0xCC);
	r5 = _mm256_blend_epi32(t6, _mm256_castps_si256(v), 0x33);

	//r6 = _mm256_shuffle_ps(t5,t7, 0x44);
	//r7 = _mm256_shuffle_ps(t5,t7, 0xEE);
	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t5), _mm256_castsi256_ps(t7), 0x4E);
	r6 = _mm256_blend_epi32(t5, _mm256_castps_si256(v), 0xCC);
	r7 = _mm256_blend_epi32(t7, _mm256_castps_si256(v), 0x33);

	_mm256_storeu_si256((__m256i*)&matT[0 * 8], r0);
	_mm256_storeu_si256((__m256i*)&matT[1 * 8], r1);
	_mm256_storeu_si256((__m256i*)&matT[2 * 8], r2);
	_mm256_storeu_si256((__m256i*)&matT[3 * 8], r3);
	_mm256_storeu_si256((__m256i*)&matT[4 * 8], r4);
	_mm256_storeu_si256((__m256i*)&matT[5 * 8], r5);
	_mm256_storeu_si256((__m256i*)&matT[6 * 8], r6);
	_mm256_storeu_si256((__m256i*)&matT[7 * 8], r7);
}

void TransformAndQuantizationAVX::transpose16x16(int* mat, int* matT) {
	__m256i  r0_0, r1_0, r2_0, r3_0, r4_0, r5_0, r6_0, r7_0, r8_0, r9_0, r10_0, r11_0, r12_0, r13_0, r14_0, r15_0,
		r0_1, r1_1, r2_1, r3_1, r4_1, r5_1, r6_1, r7_1, r8_1, r9_1, r10_1, r11_1, r12_1, r13_1, r14_1, r15_1;
	__m256i   t0_0, t1_0, t2_0, t3_0, t4_0, t5_0, t6_0, t7_0, t8_0, t9_0, t10_0, t11_0, t12_0, t13_0, t14_0, t15_0,
		t0_1, t1_1, t2_1, t3_1, t4_1, t5_1, t6_1, t7_1, t8_1, t9_1, t10_1, t11_1, t12_1, t13_1, t14_1, t15_1;

	r0_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[4 * 16 + 0]), 1);
	r1_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[5 * 16 + 0]), 1);
	r2_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[2 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[6 * 16 + 0]), 1);
	r3_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[3 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[7 * 16 + 0]), 1);
	r4_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[4 * 16 + 4]), 1);
	r5_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[5 * 16 + 4]), 1);
	r6_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[2 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[6 * 16 + 4]), 1);
	r7_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[3 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[7 * 16 + 4]), 1);

	r8_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[8 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[12 * 16 + 0]), 1);
	r9_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[9 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[13 * 16 + 0]), 1);
	r10_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[10 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[14 * 16 + 0]), 1);
	r11_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[11 * 16 + 0])), _mm_loadu_si128((__m128i*)&mat[15 * 16 + 0]), 1);
	r12_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[8 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[12 * 16 + 4]), 1);
	r13_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[9 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[13 * 16 + 4]), 1);
	r14_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[10 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[14 * 16 + 4]), 1);
	r15_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[11 * 16 + 4])), _mm_loadu_si128((__m128i*)&mat[15 * 16 + 4]), 1);

	r0_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[4 * 16 + 8]), 1);
	r1_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[5 * 16 + 8]), 1);
	r2_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[2 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[6 * 16 + 8]), 1);
	r3_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[3 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[7 * 16 + 8]), 1);
	r4_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[0 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[4 * 16 + 12]), 1);
	r5_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[1 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[5 * 16 + 12]), 1);
	r6_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[2 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[6 * 16 + 12]), 1);
	r7_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[3 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[7 * 16 + 12]), 1);

	r8_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[8 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[12 * 16 + 8]), 1);
	r9_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[9 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[13 * 16 + 8]), 1);
	r10_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[10 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[14 * 16 + 8]), 1);
	r11_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[11 * 16 + 8])), _mm_loadu_si128((__m128i*)&mat[15 * 16 + 8]), 1);
	r12_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[8 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[12 * 16 + 12]), 1);
	r13_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[9 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[13 * 16 + 12]), 1);
	r14_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[10 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[14 * 16 + 12]), 1);
	r15_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[11 * 16 + 12])), _mm_loadu_si128((__m128i*)&mat[15 * 16 + 12]), 1);


	t0_0 = _mm256_unpacklo_epi32(r0_0, r1_0);
	t1_0 = _mm256_unpackhi_epi32(r0_0, r1_0);
	t2_0 = _mm256_unpacklo_epi32(r2_0, r3_0);
	t3_0 = _mm256_unpackhi_epi32(r2_0, r3_0);
	t4_0 = _mm256_unpacklo_epi32(r4_0, r5_0);
	t5_0 = _mm256_unpackhi_epi32(r4_0, r5_0);
	t6_0 = _mm256_unpacklo_epi32(r6_0, r7_0);
	t7_0 = _mm256_unpackhi_epi32(r6_0, r7_0);

	t8_0 = _mm256_unpacklo_epi32(r8_0, r9_0);
	t9_0 = _mm256_unpackhi_epi32(r8_0, r9_0);
	t10_0 = _mm256_unpacklo_epi32(r10_0, r11_0);
	t11_0 = _mm256_unpackhi_epi32(r10_0, r11_0);
	t12_0 = _mm256_unpacklo_epi32(r12_0, r13_0);
	t13_0 = _mm256_unpackhi_epi32(r12_0, r13_0);
	t14_0 = _mm256_unpacklo_epi32(r14_0, r15_0);
	t15_0 = _mm256_unpackhi_epi32(r14_0, r15_0);

	t0_1 = _mm256_unpacklo_epi32(r0_1, r1_1);
	t1_1 = _mm256_unpackhi_epi32(r0_1, r1_1);
	t2_1 = _mm256_unpacklo_epi32(r2_1, r3_1);
	t3_1 = _mm256_unpackhi_epi32(r2_1, r3_1);
	t4_1 = _mm256_unpacklo_epi32(r4_1, r5_1);
	t5_1 = _mm256_unpackhi_epi32(r4_1, r5_1);
	t6_1 = _mm256_unpacklo_epi32(r6_1, r7_1);
	t7_1 = _mm256_unpackhi_epi32(r6_1, r7_1);

	t8_1 = _mm256_unpacklo_epi32(r8_1, r9_1);
	t9_1 = _mm256_unpackhi_epi32(r8_1, r9_1);
	t10_1 = _mm256_unpacklo_epi32(r10_1, r11_1);
	t11_1 = _mm256_unpackhi_epi32(r10_1, r11_1);
	t12_1 = _mm256_unpacklo_epi32(r12_1, r13_1);
	t13_1 = _mm256_unpackhi_epi32(r12_1, r13_1);
	t14_1 = _mm256_unpacklo_epi32(r14_1, r15_1);
	t15_1 = _mm256_unpackhi_epi32(r14_1, r15_1);

	__m256 v;
	//first part
	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t0_0), _mm256_castsi256_ps(t2_0), 0x4E);
	r0_0 = _mm256_blend_epi32(t0_0, _mm256_castps_si256(v), 0xCC);
	r1_0 = _mm256_blend_epi32(t2_0, _mm256_castps_si256(v), 0x33);

	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t1_0), _mm256_castsi256_ps(t3_0), 0x4E);
	r2_0 = _mm256_blend_epi32(t1_0, _mm256_castps_si256(v), 0xCC);
	r3_0 = _mm256_blend_epi32(t3_0, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t4_0), _mm256_castsi256_ps(t6_0), 0x4E);
	r4_0 = _mm256_blend_epi32(t4_0, _mm256_castps_si256(v), 0xCC);
	r5_0 = _mm256_blend_epi32(t6_0, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t5_0), _mm256_castsi256_ps(t7_0), 0x4E);
	r6_0 = _mm256_blend_epi32(t5_0, _mm256_castps_si256(v), 0xCC);
	r7_0 = _mm256_blend_epi32(t7_0, _mm256_castps_si256(v), 0x33);

	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t8_0), _mm256_castsi256_ps(t10_0), 0x4E);
	r8_0 = _mm256_blend_epi32(t8_0, _mm256_castps_si256(v), 0xCC);
	r9_0 = _mm256_blend_epi32(t10_0, _mm256_castps_si256(v), 0x33);

	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t9_0), _mm256_castsi256_ps(t11_0), 0x4E);
	r10_0 = _mm256_blend_epi32(t9_0, _mm256_castps_si256(v), 0xCC);
	r11_0 = _mm256_blend_epi32(t11_0, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t12_0), _mm256_castsi256_ps(t14_0), 0x4E);
	r12_0 = _mm256_blend_epi32(t12_0, _mm256_castps_si256(v), 0xCC);
	r13_0 = _mm256_blend_epi32(t14_0, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t13_0), _mm256_castsi256_ps(t15_0), 0x4E);
	r14_0 = _mm256_blend_epi32(t13_0, _mm256_castps_si256(v), 0xCC);
	r15_0 = _mm256_blend_epi32(t15_0, _mm256_castps_si256(v), 0x33);


	// second part
	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t0_1), _mm256_castsi256_ps(t2_1), 0x4E);
	r0_1 = _mm256_blend_epi32(t0_1, _mm256_castps_si256(v), 0xCC);
	r1_1 = _mm256_blend_epi32(t2_1, _mm256_castps_si256(v), 0x33);

	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t1_1), _mm256_castsi256_ps(t3_1), 0x4E);
	r2_1 = _mm256_blend_epi32(t1_1, _mm256_castps_si256(v), 0xCC);
	r3_1 = _mm256_blend_epi32(t3_1, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t4_1), _mm256_castsi256_ps(t6_1), 0x4E);
	r4_1 = _mm256_blend_epi32(t4_1, _mm256_castps_si256(v), 0xCC);
	r5_1 = _mm256_blend_epi32(t6_1, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t5_1), _mm256_castsi256_ps(t7_1), 0x4E);
	r6_1 = _mm256_blend_epi32(t5_1, _mm256_castps_si256(v), 0xCC);
	r7_1 = _mm256_blend_epi32(t7_1, _mm256_castps_si256(v), 0x33);

	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t8_1), _mm256_castsi256_ps(t10_1), 0x4E);
	r8_1 = _mm256_blend_epi32(t8_1, _mm256_castps_si256(v), 0xCC);
	r9_1 = _mm256_blend_epi32(t10_1, _mm256_castps_si256(v), 0x33);

	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t9_1), _mm256_castsi256_ps(t11_1), 0x4E);
	r10_1 = _mm256_blend_epi32(t9_1, _mm256_castps_si256(v), 0xCC);
	r11_1 = _mm256_blend_epi32(t11_1, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t12_1), _mm256_castsi256_ps(t14_1), 0x4E);
	r12_1 = _mm256_blend_epi32(t12_1, _mm256_castps_si256(v), 0xCC);
	r13_1 = _mm256_blend_epi32(t14_1, _mm256_castps_si256(v), 0x33);


	v = _mm256_shuffle_ps(_mm256_castsi256_ps(t13_1), _mm256_castsi256_ps(t15_1), 0x4E);
	r14_1 = _mm256_blend_epi32(t13_1, _mm256_castps_si256(v), 0xCC);
	r15_1 = _mm256_blend_epi32(t15_1, _mm256_castps_si256(v), 0x33);

	_mm256_storeu_si256((__m256i*)&matT[0 * 16], r0_0);
	_mm256_storeu_si256((__m256i*)&matT[8 * 16], r0_1);
	_mm256_storeu_si256((__m256i*)&matT[1 * 16], r1_0);
	_mm256_storeu_si256((__m256i*)&matT[9 * 16], r1_1);
	_mm256_storeu_si256((__m256i*)&matT[2 * 16], r2_0);
	_mm256_storeu_si256((__m256i*)&matT[10 * 16], r2_1);
	_mm256_storeu_si256((__m256i*)&matT[3 * 16], r3_0);
	_mm256_storeu_si256((__m256i*)&matT[11 * 16], r3_1);
	_mm256_storeu_si256((__m256i*)&matT[4 * 16], r4_0);
	_mm256_storeu_si256((__m256i*)&matT[12 * 16], r4_1);
	_mm256_storeu_si256((__m256i*)&matT[5 * 16], r5_0);
	_mm256_storeu_si256((__m256i*)&matT[13 * 16], r5_1);
	_mm256_storeu_si256((__m256i*)&matT[6 * 16], r6_0);
	_mm256_storeu_si256((__m256i*)&matT[14 * 16], r6_1);
	_mm256_storeu_si256((__m256i*)&matT[7 * 16], r7_0);
	_mm256_storeu_si256((__m256i*)&matT[15 * 16], r7_1);
	_mm256_storeu_si256((__m256i*)&matT[0 * 16 + 8], r8_0);
	_mm256_storeu_si256((__m256i*)&matT[8 * 16 + 8], r8_1);
	_mm256_storeu_si256((__m256i*)&matT[1 * 16 + 8], r9_0);
	_mm256_storeu_si256((__m256i*)&matT[9 * 16 + 8], r9_1);
	_mm256_storeu_si256((__m256i*)&matT[2 * 16 + 8], r10_0);
	_mm256_storeu_si256((__m256i*)&matT[10 * 16 + 8], r10_1);
	_mm256_storeu_si256((__m256i*)&matT[3 * 16 + 8], r11_0);
	_mm256_storeu_si256((__m256i*)&matT[11 * 16 + 8], r11_1);
	_mm256_storeu_si256((__m256i*)&matT[4 * 16 + 8], r12_0);
	_mm256_storeu_si256((__m256i*)&matT[12 * 16 + 8], r12_1);
	_mm256_storeu_si256((__m256i*)&matT[5 * 16 + 8], r13_0);
	_mm256_storeu_si256((__m256i*)&matT[13 * 16 + 8], r13_1);
	_mm256_storeu_si256((__m256i*)&matT[6 * 16 + 8], r14_0);
	_mm256_storeu_si256((__m256i*)&matT[14 * 16 + 8], r14_1);
	_mm256_storeu_si256((__m256i*)&matT[7 * 16 + 8], r15_0);
	_mm256_storeu_si256((__m256i*)&matT[15 * 16 + 8], r15_1);
}

void TransformAndQuantizationAVX::transpose32x32(int* mat, int* matT) {
	__m256i  r0_0, r1_0, r2_0, r3_0, r4_0, r5_0, r6_0, r7_0, r8_0, r9_0, r10_0, r11_0, r12_0, r13_0, r14_0, r15_0,
		r0_1, r1_1, r2_1, r3_1, r4_1, r5_1, r6_1, r7_1, r8_1, r9_1, r10_1, r11_1, r12_1, r13_1, r14_1, r15_1;
	__m256i   t0_0, t1_0, t2_0, t3_0, t4_0, t5_0, t6_0, t7_0, t8_0, t9_0, t10_0, t11_0, t12_0, t13_0, t14_0, t15_0,
		t0_1, t1_1, t2_1, t3_1, t4_1, t5_1, t6_1, t7_1, t8_1, t9_1, t10_1, t11_1, t12_1, t13_1, t14_1, t15_1;

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			r0_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 0 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 4 * 32 + 0 + (j * 16)]), 1);
			r1_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 1 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 5 * 32 + 0 + (j * 16)]), 1);
			r2_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 2 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 6 * 32 + 0 + (j * 16)]), 1);
			r3_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 3 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 7 * 32 + 0 + (j * 16)]), 1);
			r4_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 0 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 4 * 32 + 4 + (j * 16)]), 1);
			r5_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 1 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 5 * 32 + 4 + (j * 16)]), 1);
			r6_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 2 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 6 * 32 + 4 + (j * 16)]), 1);
			r7_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 3 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 7 * 32 + 4 + (j * 16)]), 1);

			r8_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 8 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 12 * 32 + 0 + (j * 16)]), 1);
			r9_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 9 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 13 * 32 + 0 + (j * 16)]), 1);
			r10_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 10 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 14 * 32 + 0 + (j * 16)]), 1);
			r11_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 11 * 32 + 0 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 15 * 32 + 0 + (j * 16)]), 1);
			r12_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 8 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 12 * 32 + 4 + (j * 16)]), 1);
			r13_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 9 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 13 * 32 + 4 + (j * 16)]), 1);
			r14_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 10 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 14 * 32 + 4 + (j * 16)]), 1);
			r15_0 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 11 * 32 + 4 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 15 * 32 + 4 + (j * 16)]), 1);

			r0_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 0 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 4 * 32 + 8 + (j * 16)]), 1);
			r1_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 1 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 5 * 32 + 8 + (j * 16)]), 1);
			r2_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 2 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 6 * 32 + 8 + (j * 16)]), 1);
			r3_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 3 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 7 * 32 + 8 + (j * 16)]), 1);
			r4_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 0 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 4 * 32 + 12 + (j * 16)]), 1);
			r5_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 1 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 5 * 32 + 12 + (j * 16)]), 1);
			r6_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 2 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 6 * 32 + 12 + (j * 16)]), 1);
			r7_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 3 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 7 * 32 + 12 + (j * 16)]), 1);

			r8_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 8 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 12 * 32 + 8 + (j * 16)]), 1);
			r9_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 9 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 13 * 32 + 8 + (j * 16)]), 1);
			r10_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 10 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 14 * 32 + 8 + (j * 16)]), 1);
			r11_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 11 * 32 + 8 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 15 * 32 + 8 + (j * 16)]), 1);
			r12_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 8 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 12 * 32 + 12 + (j * 16)]), 1);
			r13_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 9 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 13 * 32 + 12 + (j * 16)]), 1);
			r14_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 10 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 14 * 32 + 12 + (j * 16)]), 1);
			r15_1 = _mm256_insertf128_si256(_mm256_castsi128_si256(_mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 11 * 32 + 12 + (j * 16)])), _mm_loadu_si128((__m128i*)&mat[(i * 16 * 32) + 15 * 32 + 12 + (j * 16)]), 1);


			t0_0 = _mm256_unpacklo_epi32(r0_0, r1_0);
			t1_0 = _mm256_unpackhi_epi32(r0_0, r1_0);
			t2_0 = _mm256_unpacklo_epi32(r2_0, r3_0);
			t3_0 = _mm256_unpackhi_epi32(r2_0, r3_0);
			t4_0 = _mm256_unpacklo_epi32(r4_0, r5_0);
			t5_0 = _mm256_unpackhi_epi32(r4_0, r5_0);
			t6_0 = _mm256_unpacklo_epi32(r6_0, r7_0);
			t7_0 = _mm256_unpackhi_epi32(r6_0, r7_0);

			t8_0 = _mm256_unpacklo_epi32(r8_0, r9_0);
			t9_0 = _mm256_unpackhi_epi32(r8_0, r9_0);
			t10_0 = _mm256_unpacklo_epi32(r10_0, r11_0);
			t11_0 = _mm256_unpackhi_epi32(r10_0, r11_0);
			t12_0 = _mm256_unpacklo_epi32(r12_0, r13_0);
			t13_0 = _mm256_unpackhi_epi32(r12_0, r13_0);
			t14_0 = _mm256_unpacklo_epi32(r14_0, r15_0);
			t15_0 = _mm256_unpackhi_epi32(r14_0, r15_0);

			t0_1 = _mm256_unpacklo_epi32(r0_1, r1_1);
			t1_1 = _mm256_unpackhi_epi32(r0_1, r1_1);
			t2_1 = _mm256_unpacklo_epi32(r2_1, r3_1);
			t3_1 = _mm256_unpackhi_epi32(r2_1, r3_1);
			t4_1 = _mm256_unpacklo_epi32(r4_1, r5_1);
			t5_1 = _mm256_unpackhi_epi32(r4_1, r5_1);
			t6_1 = _mm256_unpacklo_epi32(r6_1, r7_1);
			t7_1 = _mm256_unpackhi_epi32(r6_1, r7_1);

			t8_1 = _mm256_unpacklo_epi32(r8_1, r9_1);
			t9_1 = _mm256_unpackhi_epi32(r8_1, r9_1);
			t10_1 = _mm256_unpacklo_epi32(r10_1, r11_1);
			t11_1 = _mm256_unpackhi_epi32(r10_1, r11_1);
			t12_1 = _mm256_unpacklo_epi32(r12_1, r13_1);
			t13_1 = _mm256_unpackhi_epi32(r12_1, r13_1);
			t14_1 = _mm256_unpacklo_epi32(r14_1, r15_1);
			t15_1 = _mm256_unpackhi_epi32(r14_1, r15_1);

			__m256 v;
			//first part
			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t0_0), _mm256_castsi256_ps(t2_0), 0x4E);
			r0_0 = _mm256_blend_epi32(t0_0, _mm256_castps_si256(v), 0xCC);
			r1_0 = _mm256_blend_epi32(t2_0, _mm256_castps_si256(v), 0x33);

			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t1_0), _mm256_castsi256_ps(t3_0), 0x4E);
			r2_0 = _mm256_blend_epi32(t1_0, _mm256_castps_si256(v), 0xCC);
			r3_0 = _mm256_blend_epi32(t3_0, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t4_0), _mm256_castsi256_ps(t6_0), 0x4E);
			r4_0 = _mm256_blend_epi32(t4_0, _mm256_castps_si256(v), 0xCC);
			r5_0 = _mm256_blend_epi32(t6_0, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t5_0), _mm256_castsi256_ps(t7_0), 0x4E);
			r6_0 = _mm256_blend_epi32(t5_0, _mm256_castps_si256(v), 0xCC);
			r7_0 = _mm256_blend_epi32(t7_0, _mm256_castps_si256(v), 0x33);

			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t8_0), _mm256_castsi256_ps(t10_0), 0x4E);
			r8_0 = _mm256_blend_epi32(t8_0, _mm256_castps_si256(v), 0xCC);
			r9_0 = _mm256_blend_epi32(t10_0, _mm256_castps_si256(v), 0x33);

			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t9_0), _mm256_castsi256_ps(t11_0), 0x4E);
			r10_0 = _mm256_blend_epi32(t9_0, _mm256_castps_si256(v), 0xCC);
			r11_0 = _mm256_blend_epi32(t11_0, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t12_0), _mm256_castsi256_ps(t14_0), 0x4E);
			r12_0 = _mm256_blend_epi32(t12_0, _mm256_castps_si256(v), 0xCC);
			r13_0 = _mm256_blend_epi32(t14_0, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t13_0), _mm256_castsi256_ps(t15_0), 0x4E);
			r14_0 = _mm256_blend_epi32(t13_0, _mm256_castps_si256(v), 0xCC);
			r15_0 = _mm256_blend_epi32(t15_0, _mm256_castps_si256(v), 0x33);


			// second part
			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t0_1), _mm256_castsi256_ps(t2_1), 0x4E);
			r0_1 = _mm256_blend_epi32(t0_1, _mm256_castps_si256(v), 0xCC);
			r1_1 = _mm256_blend_epi32(t2_1, _mm256_castps_si256(v), 0x33);

			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t1_1), _mm256_castsi256_ps(t3_1), 0x4E);
			r2_1 = _mm256_blend_epi32(t1_1, _mm256_castps_si256(v), 0xCC);
			r3_1 = _mm256_blend_epi32(t3_1, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t4_1), _mm256_castsi256_ps(t6_1), 0x4E);
			r4_1 = _mm256_blend_epi32(t4_1, _mm256_castps_si256(v), 0xCC);
			r5_1 = _mm256_blend_epi32(t6_1, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t5_1), _mm256_castsi256_ps(t7_1), 0x4E);
			r6_1 = _mm256_blend_epi32(t5_1, _mm256_castps_si256(v), 0xCC);
			r7_1 = _mm256_blend_epi32(t7_1, _mm256_castps_si256(v), 0x33);

			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t8_1), _mm256_castsi256_ps(t10_1), 0x4E);
			r8_1 = _mm256_blend_epi32(t8_1, _mm256_castps_si256(v), 0xCC);
			r9_1 = _mm256_blend_epi32(t10_1, _mm256_castps_si256(v), 0x33);

			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t9_1), _mm256_castsi256_ps(t11_1), 0x4E);
			r10_1 = _mm256_blend_epi32(t9_1, _mm256_castps_si256(v), 0xCC);
			r11_1 = _mm256_blend_epi32(t11_1, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t12_1), _mm256_castsi256_ps(t14_1), 0x4E);
			r12_1 = _mm256_blend_epi32(t12_1, _mm256_castps_si256(v), 0xCC);
			r13_1 = _mm256_blend_epi32(t14_1, _mm256_castps_si256(v), 0x33);


			v = _mm256_shuffle_ps(_mm256_castsi256_ps(t13_1), _mm256_castsi256_ps(t15_1), 0x4E);
			r14_1 = _mm256_blend_epi32(t13_1, _mm256_castps_si256(v), 0xCC);
			r15_1 = _mm256_blend_epi32(t15_1, _mm256_castps_si256(v), 0x33);

			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 0 * 32], r0_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 8 * 32], r0_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 1 * 32], r1_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 9 * 32], r1_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 2 * 32], r2_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 10 * 32], r2_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 3 * 32], r3_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 11 * 32], r3_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 4 * 32], r4_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 12 * 32], r4_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 5 * 32], r5_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 13 * 32], r5_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 6 * 32], r6_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 14 * 32], r6_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 7 * 32], r7_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 15 * 32], r7_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 0 * 32 + 8], r8_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 8 * 32 + 8], r8_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 1 * 32 + 8], r9_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 9 * 32 + 8], r9_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 2 * 32 + 8], r10_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 10 * 32 + 8], r10_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 3 * 32 + 8], r11_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 11 * 32 + 8], r11_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 4 * 32 + 8], r12_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 12 * 32 + 8], r12_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 5 * 32 + 8], r13_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 13 * 32 + 8], r13_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 6 * 32 + 8], r14_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 14 * 32 + 8], r14_1);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 7 * 32 + 8], r15_0);
			_mm256_storeu_si256((__m256i*)&matT[(j * 16 * 32) + (i * 16) + 15 * 32 + 8], r15_1);
		}


	}

}

#endif