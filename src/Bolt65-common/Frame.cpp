/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Frame.h"

Frame::Frame()
{
	tiles = nullptr;
}

Frame::Frame(EncodingContext *_enCtx) :Partition(_enCtx)
{
	partitionType = PartitionType::FRAME;
	tiles = nullptr;

	width = enCtx->width;
	height = enCtx->height;

	//TODO this calculation depends on Color format
	size = width * height * 3 / 2;

	numberOfCTUsInRow = (int)ceil((double)enCtx->width / enCtx->ctbSize);
	numberOfCTUsInColumn = (int)ceil((double)enCtx->height / enCtx->ctbSize);

	numberOfCTUs = numberOfCTUsInRow * numberOfCTUsInColumn;
	isLastPartitionInFrame = true;
}
Frame::Frame(EncodingContext *_enCtx, int frameIndex) :Partition(_enCtx)
{
	partitionType = PartitionType::FRAME;
	tiles = nullptr;
	numberOfTiles = -1;

	width = enCtx->width;
	height = enCtx->height;

	//TODO this calculation depends on Color format
	size = width * height * 3 / 2;

	numberOfCTUsInRow = (int)ceil((double)enCtx->width / enCtx->ctbSize);
	numberOfCTUsInColumn = (int)ceil((double)enCtx->height / enCtx->ctbSize);

	numberOfCTUs = numberOfCTUsInRow * numberOfCTUsInColumn;

	poc = frameIndex;
	decodingOrder = frameIndex;
	isLastPartitionInFrame = true;
}
Frame::Frame(EncodingContext *_enCtx, int frameIndex, char type) :Partition(_enCtx)
{
	partitionType = PartitionType::FRAME;
	tiles = nullptr;
	numberOfTiles = -1;

	width = enCtx->width;
	height = enCtx->height;

	//TODO this calculation depends on Color format
	size = width * height * 3 / 2;

	numberOfCTUsInRow = (int)ceil((double)enCtx->width / enCtx->ctbSize);;
	numberOfCTUsInColumn = (int)ceil((double)enCtx->height / enCtx->ctbSize);

	numberOfCTUs = numberOfCTUsInRow * numberOfCTUsInColumn;

	reconstructed_payload = new unsigned char[size];
	isReconstructed = new bool[size] {false};

	poc = frameIndex;
	decodingOrder = frameIndex;

	this->type = toupper(type);
	if (type == 'I')
		sliceType = I;
	else if (type == 'P')
		sliceType = P;
	else if (type == 'B')
		sliceType = B;

	isLastPartitionInFrame = true;
}

Frame::Frame(EncodingContext *_enCtx, int frameIndex, SliceType type) :Partition(_enCtx)
{
	partitionType = PartitionType::FRAME;
	tiles = nullptr;
	numberOfTiles = -1;

	width = enCtx->width;
	height = enCtx->height;

	//TODO this calculation depends on Color format
	size = width * height * 3 / 2;

	numberOfCTUsInRow = (int)ceil((double)enCtx->width / enCtx->ctbSize);;
	numberOfCTUsInColumn = (int)ceil((double)enCtx->height / enCtx->ctbSize);

	numberOfCTUs = numberOfCTUsInRow * numberOfCTUsInColumn;

	reconstructed_payload = new unsigned char[size];
	isReconstructed = new bool[size] {false};

	startingIndexU = width * height;
	startingIndexV = width * height + (width*height / 4);

	startingIndexUFrame = width*height;
	startingIndexVFrame = width*height + (width*height / 4);

	isWidthDividible = width % enCtx->ctbSize == 0;
	isHeightDividible = height % enCtx->ctbSize == 0;

	startingTileRow = startingIndexInFrame / width / enCtx->ctbSize;
	startingTileColumn = startingIndexInFrame % width /enCtx->ctbSize;

	poc = frameIndex;
	decodingOrder = frameIndex;
	sliceType = type;

	if (sliceType == I)
		this->type = 'I';
	else if (sliceType == P)
		this->type = 'P';
	else if (sliceType == B)
		this->type = 'B';

	isLastPartitionInFrame = true;
}

void Frame::splitToTiles()
{
	if (enCtx->isUniform)
		splitUniformTiles();

	createTiles();
	findTileStartingIndexes();
	//copyPayloadToTiles();

}


void Frame::findTileStartingIndexes()
{
	if (numberOfTiles > 0)
	{
		int CTUsRowBefore = 0, CTUsColumnBefore = 0, tileId = 0;

		for (int i = 0; i < enCtx->numberOfTilesInRow; i++)
		{
			CTUsRowBefore = 0;
			for (int j = 0; j < enCtx->numberOfTilesInColumn; j++)
			{
				tiles[tileId]->startingIndexInFrame = CTUsRowBefore * enCtx->ctbSize + CTUsColumnBefore * enCtx->ctbSize*width;
				tiles[tileId]->startingTileRow = tiles[tileId]->startingIndexInFrame / width / enCtx->ctbSize;
				tiles[tileId]->startingTileColumn = tiles[tileId]->startingIndexInFrame % width / enCtx->ctbSize;
				tileId++;
				CTUsRowBefore += enCtx->columnWidths[j];
			}
			CTUsColumnBefore += enCtx->rowHeights[i];
		}
	}
}


void Frame::reconstructFrameFromTiles()
{
	int lumaSize = width * height;
	int row, column, componentWidth, componentHeight, componentCtuSize, tileRow, tileColumn, tileId;
	int* currentTileIndex = new int[numberOfTiles] {0};

	for (int i = 0; i < size; i++)
	{
		//Y component
		if (i < lumaSize)
		{
			componentWidth = width;
			componentHeight = height;
			row = i / componentWidth;
			column = i % componentWidth;
			componentCtuSize = enCtx->ctbSize;
		}
		//U component
		else if (i >= lumaSize && i < (lumaSize + lumaSize / 4))
		{
			componentWidth = width / 2;
			componentHeight = height / 2;
			row = (i - lumaSize) / componentWidth;
			column = (i - lumaSize) % componentWidth;
			componentCtuSize = enCtx->ctbSize / 2;
		}
		//V component
		else
		{
			componentWidth = width / 2;
			componentHeight = height / 2;
			row = (i - lumaSize - (lumaSize / 4)) / componentWidth;
			column = (i - lumaSize - (lumaSize / 4)) % componentWidth;
			componentCtuSize = enCtx->ctbSize / 2;
		}

		int columnBoundary = 0;
		int rowBoundary = 0;
		for (int j = 0; j < enCtx->numberOfTilesInColumn; j++)
		{
			columnBoundary += enCtx->columnWidths[j] * componentCtuSize;
			if (column < columnBoundary)
			{
				tileColumn = j;
				break;
			}
		}

		for (int j = 0; j < enCtx->numberOfTilesInRow; j++)
		{
			rowBoundary += enCtx->rowHeights[j] * componentCtuSize;
			if (row < rowBoundary)
			{
				tileRow = j;
				break;
			}
		}

		tileId = tileRow * enCtx->numberOfTilesInColumn + tileColumn;

		reconstructed_payload[i] = tiles[tileId]->payload[currentTileIndex[tileId]];
		currentTileIndex[tileId]++;
	}
}

void Frame::scanTileCTUs()
{
	if (numberOfTiles > 0)
	{
		int ctuFrameIndex = 0;
		ctuTree = new CU*[numberOfCTUs];

		for (int i = 0; i < numberOfTiles; i++)
		{
			for (int j = 0; j < tiles[i]->numberOfCTUs; j++)
			{
				ctuTree[tiles[i]->ctuTree[j]->indexInFrame] = tiles[i]->ctuTree[j];
			}
		}
	}
}

void Frame::createTiles()
{
	numberOfTiles = enCtx->numberOfTilesInRow * enCtx->numberOfTilesInColumn;
	tiles = new Tile*[numberOfTiles];
	int ctuInFrameIndex = 0;

	for (int i = 0; i < numberOfTiles; i++)
	{
		tiles[i] = new Tile(enCtx, i);

		int tileRow = i / enCtx->numberOfTilesInColumn;
		int tileColumn = i % enCtx->numberOfTilesInColumn;

		//default values for tiles (if width and height are perfectly dividable)
		tiles[i]->numberOfCTUsInRow = enCtx->columnWidths[tileColumn];
		tiles[i]->numberOfCTUsInColumn = enCtx->rowHeights[tileRow];
		tiles[i]->height = tiles[i]->numberOfCTUsInColumn *enCtx->ctbSize;
		tiles[i]->width = tiles[i]->numberOfCTUsInRow  * enCtx->ctbSize;

		//check the height of the tiles on the bottom border
		if (i / enCtx->numberOfTilesInColumn == enCtx->numberOfTilesInRow - 1 && height%enCtx->ctbSize != 0)
		{
			tiles[i]->height = tiles[i]->height - (enCtx->ctbSize - height % enCtx->ctbSize);
		}

		//check the width of the tiles on the right border
		if ((i + 1) % enCtx->numberOfTilesInColumn == 0 && width%enCtx->ctbSize != 0)
		{
			tiles[i]->width = tiles[i]->width - (enCtx->ctbSize - width % enCtx->ctbSize);
		}


		tiles[i]->numberOfCTUs = tiles[i]->numberOfCTUsInRow*tiles[i]->numberOfCTUsInColumn;
		tiles[i]->size = tiles[i]->width*tiles[i]->height * 3 / 2;
		tiles[i]->startingCTUInFrame = ctuInFrameIndex;
		tiles[i]->type = type;
		tiles[i]->sliceType = sliceType;

		ctuInFrameIndex += tiles[i]->numberOfCTUs;

		tiles[i]->payload = payload;
		tiles[i]->reconstructed_payload = reconstructed_payload;
		tiles[i]->isReconstructed = isReconstructed;
		tiles[i]->isCTUProcessed = isCTUProcessed;
		tiles[i]->framePoc = poc;

		if (i == numberOfTiles - 1)
			tiles[i]->isLastPartitionInFrame = true;
		else
			tiles[i]->isLastPartitionInFrame = false;

		tiles[i]->startingIndexU = tiles[i]->width * tiles[i]->height;
		tiles[i]->startingIndexV = tiles[i]->width * tiles[i]->height + (tiles[i]->width*tiles[i]->height / 4);;

		tiles[i]->startingIndexUFrame = width * height;
		tiles[i]->startingIndexVFrame = width * height + (width*height / 4);

		tiles[i]->isWidthDividible = tiles[i]->width % enCtx->ctbSize == 0;
		tiles[i]->isHeightDividible = tiles[i]->height % enCtx->ctbSize == 0;

		
	}
}

void Frame::copyPayloadToTiles()
{
	int lumaSize = width * height;
	int row, column, componentWidth, componentHeight, componentCtuSize, tileRow, tileColumn, tileId;
	int* currentTileIndex = new int[numberOfTiles] {0};

	for (int i = 0; i < size; i++)
	{
		//Y component
		if (i < lumaSize)
		{
			componentWidth = width;
			componentHeight = height;
			row = i / componentWidth;
			column = i % componentWidth;
			componentCtuSize = enCtx->ctbSize;
		}
		//U component
		else if (i >= lumaSize && i < (lumaSize + lumaSize / 4))
		{
			componentWidth = width / 2;
			componentHeight = height / 2;
			row = (i - lumaSize) / componentWidth;
			column = (i - lumaSize) % componentWidth;
			componentCtuSize = enCtx->ctbSize / 2;
		}
		//V component
		else
		{
			componentWidth = width / 2;
			componentHeight = height / 2;
			row = (i - lumaSize - (lumaSize / 4)) / componentWidth;
			column = (i - lumaSize - (lumaSize / 4)) % componentWidth;
			componentCtuSize = enCtx->ctbSize / 2;
		}

		int columnBoundary = 0;
		int rowBoundary = 0;
		for (int j = 0; j < enCtx->numberOfTilesInColumn; j++)
		{
			columnBoundary += enCtx->columnWidths[j] * componentCtuSize;
			if (column < columnBoundary)
			{
				tileColumn = j;
				break;
			}
		}

		for (int j = 0; j < enCtx->numberOfTilesInRow; j++)
		{
			rowBoundary += enCtx->rowHeights[j] * componentCtuSize;
			if (row < rowBoundary)
			{
				tileRow = j;
				break;
			}
		}

		tileId = tileRow * enCtx->numberOfTilesInColumn + tileColumn;

		tiles[tileId]->payload[currentTileIndex[tileId]] = payload[i];
		currentTileIndex[tileId]++;

	}
}

void Frame::splitUniformTiles()
{
	for (int i = 0; i < enCtx->numberOfTilesInColumn; i++)
		enCtx->columnWidths[i] = ((i + 1)*numberOfCTUsInRow) / enCtx->numberOfTilesInColumn - i * numberOfCTUsInRow / enCtx->numberOfTilesInColumn;

	for (int i = 0; i < enCtx->numberOfTilesInRow; i++)
		enCtx->rowHeights[i] = ((i + 1)*numberOfCTUsInColumn) / enCtx->numberOfTilesInRow - i * numberOfCTUsInColumn / enCtx->numberOfTilesInRow;
}


Frame::~Frame()
{
	//TODO delete Tiles if they exist - after efficient Entropy coding is implemented (problem is that frame CTU's are mapped to Tile CTU's
	if (enCtx->tilesEnabled && numberOfTiles > 1)
	{
		for (int i = 0; i < numberOfTiles; i++)
			delete tiles[i];
		delete[] tiles;
	}
	else
	{
		if (ctuTree != nullptr)
		{
			for (int i = 0; i < numberOfCTUs; i++)
				delete ctuTree[i];
			delete[] ctuTree;
		}
	}

	if (reconstructed_payload != nullptr)
		delete[] reconstructed_payload;

	if (isReconstructed != nullptr)
		delete[] isReconstructed;

	if (payload != nullptr)
		delete[] payload;

	if (isCTUProcessed != nullptr)
		delete[] isCTUProcessed;



}


