/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Constants.h"


const std::string StringConstants::InputFile = "InputFile";
const std::string StringConstants::OutputFile = "OutputFile";
const std::string StringConstants::NumberOfFrames = "NumberOfFrames";
const std::string StringConstants::QP = "QP";
const std::string StringConstants::BitNumber = "BitNumber";
const std::string StringConstants::HighestTid = "HighestTid";
const std::string StringConstants::CtbLog2SizeY = "CtbLog2SizeY";
const std::string StringConstants::MinCbLog2SizeY = "MinCbLog2SizeY";
const std::string StringConstants::MinTbLog2SizeY = "MinTbLog2SizeY";
const std::string StringConstants::MaxTbLog2SizeY = "MaxTbLog2SizeY";
const std::string StringConstants::InputPictureWidth = "InputPictureWidth";
const std::string StringConstants::InputPictureHeight = "InputPictureHeight";
const std::string StringConstants::FrameRate = "FrameRate";
const std::string StringConstants::DPBSize = "DPBSize";
const std::string StringConstants::GOP = "GOP";
const std::string StringConstants::AMVPUseTemporalCandidates = "AMVPUseTemporalCandidates";
const std::string StringConstants::DeblockingFilter = "DeblockingFilter";
const std::string StringConstants::SAOFilter = "SAOFilter";
const std::string StringConstants::Log2MaxPicOrderLst = "Log2MaxPicOrderLst";
const std::string StringConstants::SearchAlgorithm = "SearchAlgorithm";
const std::string StringConstants::SearchArea = "SearchArea";

const std::string StringConstants::StatMode = "StatMode";
const std::string StringConstants::CalculatePSNR = "CalculatePSNR";
const std::string StringConstants::CalculateSSIM = "CalculateSSIM";
const std::string StringConstants::CalculateBitsPerFrame = "CalculateBitsPerFrame";
const std::string StringConstants::CalculateProcessingTime = "CalculateProcessingTime";
const std::string StringConstants::ShowStatisticsPerFrame = "ShowStatisticsPerFrame";
const std::string StringConstants::ShowStatisticsPerTile = "ShowStatisticsPerTile";

const std::string StringConstants::UsePolicy = "UsePolicy";
const std::string StringConstants::PolicySocketHost = "PolicySocketHost";
const std::string StringConstants::PolicySocketPortNumber = "PolicySocketPortNumber";

const std::string StringConstants::InterpolateAlgorithm = "InterpolateAlgorithm";
const std::string StringConstants::BlockMatching = "BlockMatching";
const std::string StringConstants::CSV = "CSV";
const std::string StringConstants::CSVTiles = "CSVTiles";
const std::string StringConstants::AVX = "AVX";
const std::string StringConstants::NEON = "NEON";
const std::string StringConstants::SVE = "SVE";
const std::string StringConstants::SVE2 = "SVE2";
const std::string StringConstants::EPI_RISCV = "EPI_RISCV";
const std::string StringConstants::EPI_RISCV_AUTO = "EPI_RISCV_AUTO";
const std::string StringConstants::MANGO_HW = "MANGO_HW";
const std::string StringConstants::MANGO_NUP = "MANGO_NUP";
const std::string StringConstants::MANGO_PEAK = "MANGO_PEAK";
const std::string StringConstants::MANGO_GN = "MANGO_GN";
const std::string StringConstants::MANGO_GPU = "MANGO_GPU";
const std::string StringConstants::MEEP = "MEEP";
const std::string StringConstants::Threads = "Threads";

const std::string StringConstants::TilesEnabled = "TilesEnabled";
const std::string StringConstants::TilesInRow = "TilesInRow";
const std::string StringConstants::TilesInColumn = "TilesInColumn";
const std::string StringConstants::DynamicTiles = "DynamicTiles";
const std::string StringConstants::TileLoadBalancingAlgorithm = "TileLoadBalancingAlgorithm";
const std::string StringConstants::TileLoadBalancingInterval = "TileLoadBalancingInterval";
const std::string StringConstants::InputBufferSize = "InputBufferSize";
const std::string StringConstants::OutputBufferSize = "OutputBufferSize";

const std::string StringConstants::SMPEnabled = "SMPEnabled";
const std::string StringConstants::AMPEnabled = "AMPEnabled";
const std::string StringConstants::IntraTUEnabled = "IntraTUEnabled";

const std::string StringConstants::ClusteredTQ = "ClusteredTQ";

const std::string StringConstants::ReuseDecodedData = "ReuseDecodedData";
const std::string StringConstants::ReuseIntra = "ReuseIntra";
const std::string StringConstants::ReuseInter = "ReuseInter";
const std::string StringConstants::ReuseInterpolation = "ReuseInterpolation";
const std::string StringConstants::ReuseMode = "ReuseMode";
const std::string StringConstants::ReusePartmode = "ReusePartmode";

const std::string StringConstants::TestAllModes = "TestAllModes";
const std::string StringConstants::InputFrameRate = "InputFrameRate";

const char StringConstants::FRAME_I = 'I';
const char StringConstants::FRAME_P = 'P';
const char StringConstants::FRAME_B = 'B';

