/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "TU.h"

TU::TU(TB tbY, TB tbU, TB tbV)
{
	this->tbY = tbY;
	this->tbU = tbU;
	this->tbV = tbV;

	splitTransformFlag = false;
	trafoDepth = -1;
	hasChildren = false;
	*children = nullptr;
}


TU::TU()
{
	*children = nullptr;
}


TU::~TU()
{
	if (*children != nullptr)
	{
		delete children[0];
		delete children[1];
		delete children[2];
		delete children[3];
	}

}

void TU::splitTU(int width, int frameWidth)
{
	splitTransformFlag = true;
	hasChildren = true;

	int i;
	int blockSize = tbY.transformBlockSize;
	for (i = 0; i < 4; i++)
	{
		int lumaPixelOffsetFrame = (i / 2)* (blockSize / 2 * frameWidth) + (i % 2)*blockSize / 2;
		int chromaPixelOffsetFrame = (i / 2)*(blockSize / 4 * frameWidth / 2) + (i % 2)*blockSize / 4;

		int lumaPixelOffset = (i / 2)* (blockSize / 2 * width) + (i % 2)*blockSize / 2;
		int chromaPixelOffset = (i / 2)*(blockSize / 4 * width / 2) + (i % 2)*blockSize / 4;

		TB tbY_child = TB(tbY.startingIndex + lumaPixelOffset, tbY.startingIndexInFrame + lumaPixelOffsetFrame, blockSize / 2, 0);
		TB tbU_child = TB(tbU.startingIndex + chromaPixelOffset, tbU.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, 1);
		TB tbV_child = TB(tbV.startingIndex + chromaPixelOffset, tbV.startingIndexInFrame + chromaPixelOffsetFrame, blockSize / 4, 2);

		children[i] = new TU(tbY_child, tbU_child, tbV_child);
		children[i]->index = i;

		children[i]->trafoDepth = trafoDepth + 1;
		children[i]->parent = this;
		*children[i]->children = nullptr;
		children[i]->hasChildren = false;
		children[i]->splitTransformFlag = false;
	}
}

void TU::cleanTU()
{
	splitTransformFlag = false;
	hasChildren = false;

	if (*children != nullptr)
	{
		delete children[0];
		delete children[1];
		delete children[2];
		delete children[3];
	}

	*children = nullptr;
}


