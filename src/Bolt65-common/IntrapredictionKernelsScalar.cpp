﻿/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "IntrapredictionKernelsScalar.h"


IntrapredictionKernelsScalar::IntrapredictionKernelsScalar()
{
}

void IntrapredictionKernelsScalar::ThreeTapRefFilter(unsigned char* refSamples, unsigned char* filteredRefSamples, int blockSize) {
	int i;
	int N = blockSize * 4;

	//skipping corners p[-1][2N-1], p[2N-1][-1].
	filteredRefSamples[0] = refSamples[0];
	filteredRefSamples[N] = refSamples[N];

	for (i = 1; i < N; i++)
	{
		filteredRefSamples[i] = (refSamples[i - 1] + 2 * refSamples[i] + refSamples[i + 1] + 2) >> 2;
	}
}

void IntrapredictionKernelsScalar::PlanarPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int blockSize) {
	short i, j;
	unsigned int predictedBlockH;
	unsigned int predictedBlockV;
	int shiftSize = ComUtil::logarithm2(blockSize) + 1;

	for (i = 0; i < blockSize; i++) //row
	{
		for (j = 0; j < blockSize; j++) //column
		{
			predictedBlockH = (blockSize - 1 - j) * refSamples[2 * blockSize - 1 - i] + (j + 1) * refSamples[3 * blockSize + 1];
			predictedBlockV = (blockSize - 1 - i) * refSamples[2 * blockSize + 1 + j] + (i + 1) * refSamples[blockSize - 1];


			// Averaging horizontal and vertical linear prediction
			predictedBlock[i * blockSize + j] = (predictedBlockH + predictedBlockV + blockSize) >> shiftSize;

		}
	}
}

void IntrapredictionKernelsScalar::DCPrediction(unsigned char* refSamples, unsigned char* predictedBlock, int cIdx, int blockSize)
{
	short i, j;
	short dcVal = 0;
	bool isFiltered = false;
	int shiftSize = ComUtil::logarithm2(blockSize) + 1;

	for (i = blockSize; i < 2 * blockSize; i++)
		dcVal += refSamples[i];

	for (i = 2 * blockSize + 1; i < 3 * blockSize + 1; i++)
		dcVal += refSamples[i];

	dcVal = (dcVal + blockSize) >> shiftSize;

	if (blockSize == 32)
	{
		for (i = 0; i < blockSize * blockSize; i++)
			predictedBlock[i] = (unsigned char)dcVal;
	}

	else
	{

		if (cIdx == 0)
			isFiltered = true;

		for (i = 0; i < blockSize; i++) // row
		{
			for (j = 0; j < blockSize; j++) // column
			{
				if (isFiltered)
				{
					if (i == 0 && j == 0)
					{
						predictedBlock[i * blockSize + j] = (refSamples[2 * blockSize - 1] + 2 * dcVal + refSamples[2 * blockSize + 1] + 2) >> 2;
					}

					else if (j == 0)
					{
						predictedBlock[i * blockSize + j] = (refSamples[2 * blockSize - 1 - i] + 3 * dcVal + 2) >> 2;
					}
					else if (i == 0)
					{
						predictedBlock[i * blockSize + j] = (refSamples[2 * blockSize + 1 + j] + 3 * dcVal + 2) >> 2;
					}

					else
					{
						predictedBlock[i * blockSize + j] = (unsigned char)dcVal;
					}
				}

				else
				{
					predictedBlock[i * blockSize + j] = (unsigned char)dcVal;
				}
			}
		}
	}
}

void IntrapredictionKernelsScalar::AngularPrediction(unsigned char* refSamples, unsigned char predMode, unsigned char* predictedBlock, int cIdx, int blockSize)
{
	short i, j;
	int iPar, fPar;
	int tmpPredSample;
	int doubleBlockSize = 2 * blockSize;

	if (predMode < 16) { //if is horizontal
		for (i = 0; i < blockSize; i++) // row
		{
			for (j = 0; j < blockSize; j++) // column
			{
				if (predMode == 8 && i == 0 && cIdx == 0 && blockSize != 32) //when prediction mode is totally horizontal we need to filter the first row.
				{
					tmpPredSample = refSamples[doubleBlockSize - 1 - i] + ((refSamples[doubleBlockSize + 1 + j] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[i * blockSize + j] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[i * blockSize + j] = 0;
					}
					else
					{
						predictedBlock[i * blockSize + j] = tmpPredSample;
					}
				}
				else
				{
					iPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
					fPar = ((j + 1) * AngularIntrapredictionModes[predMode][0]) & 31;


					if (fPar == 0)
						predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + 16) >> 5;
					else
						predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize - i - iPar - 1] + fPar * refSamples[2 * blockSize - i - iPar - 2] + 16) >> 5;
				}
			}
		}
	}

	else// if is vertical
	{
		for (i = 0; i < blockSize; i++) // row
		{
			for (j = 0; j < blockSize; j++) // column
			{
				if (predMode == 24 && j == 0 && cIdx == 0 && blockSize != 32) // if total vertical, needs to be filtered but only first column to the left.
				{
					tmpPredSample = refSamples[doubleBlockSize + 1 + j] + ((refSamples[doubleBlockSize - 1 - i] - refSamples[doubleBlockSize]) >> 1);
					if (tmpPredSample > 255)
					{
						predictedBlock[i * blockSize + j] = 255;
					}
					else if (tmpPredSample < 0)
					{
						predictedBlock[i * blockSize + j] = 0;
					}
					else
					{
						predictedBlock[i * blockSize + j] = tmpPredSample;
					}
				}
				else
				{
					iPar = ((i + 1) * AngularIntrapredictionModes[predMode][0]) >> 5;
					fPar = ((i + 1) * AngularIntrapredictionModes[predMode][0]) & 31;
					if (fPar == 0)
						predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize + j + iPar + 1] + 16) >> 5;
					else
						predictedBlock[i * blockSize + j] = ((32 - fPar) * refSamples[2 * blockSize + j + iPar + 1] + fPar * refSamples[2 * blockSize + j + iPar + 2] + 16) >> 5;
				}

			}
		}
	}
}