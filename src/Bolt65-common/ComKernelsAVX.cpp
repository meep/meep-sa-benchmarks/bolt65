/*
� FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#ifdef __AVX2__
#include "ComKernelsAVX.h"

ComKernelsAVX::ComKernelsAVX()
{
}

int ComKernelsAVX::SAD(unsigned char* block_1, unsigned char* block_2, int puWidth, int puHeight)
{
	__m256i a, b, sad, sadtemp;
	sad = _mm256_set1_epi32(0);


	for (int i = 0; i < puWidth * puHeight; i = i + 32)
	{
		a = _mm256_loadu_si256((__m256i*) & block_1[i]);
		b = _mm256_loadu_si256((__m256i*) & block_2[i]);

		sadtemp = _mm256_sad_epu8(a, b);
		sad = _mm256_add_epi64(sadtemp, sad);
	}


	int res = ((int*)&sad)[0] + ((int*)&sad)[2] + ((int*)&sad)[4] + ((int*)&sad)[6];

	return res;
}
void ComKernelsAVX::SubResidual(unsigned char* rawBlock, unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	__m256i a, b, c, a_low, a_high, b_low, b_high, c_low, c_high;

	//AVX has 256 bits, meaning that 32 8-bit elements can fit
	int increment = 32;

	//If we have 4x4 block, we cannot increment loop by 32
	if (blockSize == 4)
		increment = 16;

	for (int i = 0; i < blockSize * blockSize; i = i + increment)
	{
		a = _mm256_loadu_si256((__m256i*) & rawBlock[i]);
		b = _mm256_loadu_si256((__m256i*) & predictedBlock[i]);

		a_low = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(a, 0));
		a_high = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(a, 1));

		b_low = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(b, 0));
		b_high = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(b, 1));

		c_low = _mm256_subs_epi16(a_low, b_low);
		c_high = _mm256_subs_epi16(a_high, b_high);

		_mm256_storeu_si256((__m256i*) & residualBlock[i], c_low);
		if (increment > 16)
			_mm256_storeu_si256((__m256i*) & residualBlock[i + 16], c_high);
	}
}
#endif
