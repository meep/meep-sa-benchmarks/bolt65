/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/
#ifdef __MEEP
#include "TransformAndQuantizationMEEP.h"

TransformAndQuantizationMEEP::TransformAndQuantizationMEEP()
{
}

void TransformAndQuantizationMEEP::multiply(int16_t* A, int16_t* B, int N, int16_t* C, int bdShift)
{
	for (int r = 0; r < N; r++) {
		for (int c = 0; c < N; c++) {
			int tmp = 0;
			for (int p = 0; p < N; p++) {
				tmp = tmp + A[r * N + p] * B[p * N + c];
			}
			C[r * N + c] = (tmp + (1 << (bdShift - 1))) >> bdShift;
		}
	}
}

void TransformAndQuantizationMEEP::quantization(int16_t *A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C)
{
	int M = ComUtil::logarithm2(N);
	int bdShift = 29 - M - NB;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int mScalingFactor = 16;
	int i;
	*is_zero_matrix = true;

	for (i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			int sign = 1;
			if (*(A + i * N + j) < 0)
				sign = -1;

			*(C + i * N + j) = (((abs(*(A + i * N + j)) * f[QP % 6] + (1 << (bdShift - 1))) >> QP / 6) >> bdShift);
			*(C + i * N + j) = ComUtil::clip3(coeffMin, coeffMax, *(C + i * N + j) * sign);

			if (*(C + i * N + j) != 0)
				*is_zero_matrix = false;
		}
	}


}

void TransformAndQuantizationMEEP::dequantization(int16_t* A, int QP, int bitDepth, int N, int16_t* C)
{
	int log2TransformRange = 15;
	int bdShift = bitDepth + ComUtil::logarithm2(N) + 10 - log2TransformRange;
	int coeffMin = -32768;
	int coeffMax = 32767;
	int mScalingFactor = 16;
	int i;

	for (i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			*(C + i * N + j) = ComUtil::clip3(coeffMin, coeffMax, ((*(A + i * N + j) * mScalingFactor * g[QP % 6] << (QP / 6)) + (1 << (bdShift - 1))) >> bdShift);
		}
	}
}

void TransformAndQuantizationMEEP::performTQandIQTOnCluster(int QP, int bitNumber, int16_t* residualBlock, int residualSize, int blockSize, int16_t* qtBlock, int colorIdx, PredMode predMode, int16_t* iqtBlock)
{
	int mode = -1;

	if (blockSize == 4)
		mode = 0;
	else if (blockSize == 8)
		mode = 1;
	else if (blockSize == 16)
		mode = 2;
	else if (blockSize == 32)
		mode = 3;

	__asm__(
		"sa.setssr.0 0,%[m]\n"
		"sa.setssr.0 1,%[qp]\n"
		"sa.setoplen.0.0 x0, %[length]\n" // sets the operational length - (number_of_matrices * block_size * block_size)
		"sa.load1d0.0.1 sa.0, (%[a])\n"   // loads first operand ("data_size" elements of 16-bit)
		"sa.op22.0.0 sa.2, sa.1, sa.0, sa.3\n"  // performs the operation, No op21 instruction! We need one input, two outputs. Or use op22 with empty src2?
		"sa.store1d0.0.1 sa.1, (%[b])\n"  // stores data_size element of 16 bit
		"sa.store1d0.0.1 sa.2, (%[c])\n"  // stores data_size element of 16 bit
		: /* no register outputs */
	: [qp] "r" (QP), [m] "r" (mode), [length] "r"(residualSize), [a] "r"(residualBlock), [b] "r"(qtBlock), [c] "r"(iqtBlock)
		: "memory" /* tell the compiler we will update the memory */);
}
#endif
