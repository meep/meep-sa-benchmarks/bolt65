/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "BlockPartition.h"
#include<math.h>

BlockPartition::BlockPartition()
{
}

#pragma region Used in prediction prediction

PU* BlockPartition::getCandidateBy2DCoordinates(CU** ctuTree, int pixelRow, int pixelColumn, int partitionWidth, int partitionHeight, int ctbSize, bool& isAvailable)
{
	isAvailable = true;

	if (pixelRow < 0 || pixelColumn < 0)
		isAvailable = false;

	if (pixelRow >= partitionHeight || pixelColumn >= partitionWidth)
		isAvailable = false;

	int pixelStartingIndex = pixelRow * partitionWidth + pixelColumn;


	CU* cu = getCodingUnitByLumaPixelIndex(ctuTree, pixelStartingIndex, isAvailable, ctbSize, partitionWidth, partitionHeight);

	if (cu->CuPredMode == MODE_INTRA || isAvailable == false)
		isAvailable = false;

	if (cu->numOfPUs > 1)
	{
		int cuRow = cu->cbY.startingIndexInFrame / partitionWidth;
		int cuColumn = cu->cbY.startingIndexInFrame % partitionWidth;

		if (cu->PartMode == PART_2NxN || cu->PartMode == PART_2NxnU || cu->PartMode == PART_2NxnD)
		{
			int rowDiff = pixelRow - cuRow;

			if (rowDiff < cu->predictionUnits[0].pbY.height)
			{
				if (!cu->predictionUnits[0].isProcessed)
					isAvailable = false;

				return &cu->predictionUnits[0];

			}
			else
			{
				if (!cu->predictionUnits[1].isProcessed)
					isAvailable = false;

				return &cu->predictionUnits[1];
			}
		}
		else if (cu->PartMode == PART_Nx2N || cu->PartMode == PART_nLx2N || cu->PartMode == PART_nRx2N)
		{
			int columnDiff = pixelColumn - cuColumn;

			if (columnDiff < cu->predictionUnits[0].pbY.width)
			{
				if (!cu->predictionUnits[0].isProcessed)
					isAvailable = false;

				return &cu->predictionUnits[0];
			}
			else
			{
				if (!cu->predictionUnits[1].isProcessed)
					isAvailable = false;

				return &cu->predictionUnits[1];
			}
		}
		else
		{
			int rowDiff = pixelRow - cuRow;
			int columnDiff = pixelColumn - cuColumn;

			if (rowDiff < cu->predictionUnits[0].pbY.height)
			{
				if (columnDiff < cu->predictionUnits[0].pbY.width)
				{
					if (!cu->predictionUnits[0].isProcessed)
						isAvailable = false;

					return &cu->predictionUnits[0];
				}
				else
				{
					if (!cu->predictionUnits[1].isProcessed)
						isAvailable = false;

					return &cu->predictionUnits[1];
				}
			}
			else
			{
				if (columnDiff < cu->predictionUnits[0].pbY.width)
				{
					if (!cu->predictionUnits[2].isProcessed)
						isAvailable = false;
					return &cu->predictionUnits[2];
				}
				else
				{
					if (!cu->predictionUnits[3].isProcessed)
						isAvailable = false;

					return &cu->predictionUnits[3];
				}
			}
		}
	}
	else
	{
		if (!cu->predictionUnits[0].isProcessed)
			isAvailable = false;

		return &cu->predictionUnits[0];

	}

	return nullptr;

}

//getCandidateBy2DCoordinates(x-1, y-1)
bool BlockPartition::getCandidateBy2DCoordinates(CU** ctuTree, int pixelRow, int pixelColumn, int partitionWidth, int partitionHeight, int ctbSize, PU& pu)
{
	if (pixelRow < 0 || pixelColumn < 0)
		return false;

	if (pixelRow >= partitionHeight || pixelColumn >= partitionWidth)
		return false;

	int pixelStartingIndex = pixelRow * partitionWidth + pixelColumn;

	bool isAvailable = true;
	CU* cu = getCodingUnitByLumaPixelIndex(ctuTree, pixelStartingIndex, isAvailable, ctbSize, partitionWidth, partitionHeight);

	if (cu->CuPredMode == MODE_INTRA || cu->CuPredMode == UNKNOWN || isAvailable == false)
		return false;

	if (cu->numOfPUs > 1)
	{
		int cuRow = cu->cbY.startingIndex / partitionWidth;
		int cuColumn = cu->cbY.startingIndex % partitionWidth;

		if (cu->PartMode == PART_2NxN || cu->PartMode == PART_2NxnU || cu->PartMode == PART_2NxnD)
		{
			int rowDiff = pixelRow - cuRow;

			if (rowDiff < cu->predictionUnits[0].pbY.height)
			{
				if (!cu->predictionUnits[0].isProcessed)
					return false;

				pu = cu->predictionUnits[0];

			}
			else
			{
				if (!cu->predictionUnits[1].isProcessed)
					return false;

				pu = cu->predictionUnits[1];
			}
		}
		else if (cu->PartMode == PART_Nx2N || cu->PartMode == PART_nLx2N || cu->PartMode == PART_nRx2N)
		{
			int columnDiff = pixelColumn - cuColumn;

			if (columnDiff < cu->predictionUnits[0].pbY.width)
			{
				if (!cu->predictionUnits[0].isProcessed)
					return false;

				pu = cu->predictionUnits[0];
			}
			else
			{
				if (!cu->predictionUnits[1].isProcessed)
					return false;

				pu = cu->predictionUnits[1];
			}
		}
		else
		{
			int rowDiff = pixelRow - cuRow;
			int columnDiff = pixelColumn - cuColumn;

			if (rowDiff < cu->predictionUnits[0].pbY.height)
			{
				if (columnDiff < cu->predictionUnits[0].pbY.width)
				{
					if (!cu->predictionUnits[0].isProcessed)
						return false;

					pu = cu->predictionUnits[0];
				}
				else
				{
					if (!cu->predictionUnits[1].isProcessed)
						return false;

					pu = cu->predictionUnits[1];
				}
			}
			else
			{
				if (columnDiff < cu->predictionUnits[0].pbY.width)
				{
					if (!cu->predictionUnits[2].isProcessed)
						return false;
					pu = cu->predictionUnits[2];
				}
				else
				{
					if (!cu->predictionUnits[3].isProcessed)
						return false;

					pu = cu->predictionUnits[3];
				}
			}
		}
	}
	else
	{
		if (!cu->predictionUnits[0].isProcessed)
			return false;

		pu = cu->predictionUnits[0];

	}

	return isAvailable;

}



void BlockPartition::get2DCoordinatesFromStartingIndex(int startingIdx, int partitionWidth, int& row, int& column) {
	row = startingIdx / partitionWidth;
	column = startingIdx % partitionWidth;
}


/*-----------------------------------------------------------------------------
Function name: get1dBlockByStartingIndex
Description: Extracts block of data from starting index in frame
Parameters:
	-	data - raw frame data
	-	resultBlock - result block extracted from frame
	-	startingIndex - starting index of block inside frame
	-	blockSize - size of the block that has to be extracted
Returns: resultBlock

Author: Piljic
Current version: v0.1
Version history:

	-	v 0.1. - Fetches block (with defined size) from frame

Comments: Fetching result block depends on color component (luma or chroma). For now that component is recognized by starting index
-----------------------------------------------------------------------------*/
void BlockPartition::get1dBlockByStartingIndex(char* data, unsigned char* resultBlock, int startingIndex, int blockWidth, int blockHeight, int frameWidth, int frameHeight)
{
	int v_starting_index = frameWidth * frameHeight + (frameWidth * frameHeight / 4);
	int u_starting_index = frameWidth * frameHeight;

	int componentWidth, componentHeight, componentStartingIndex;

	if (startingIndex >= v_starting_index)
	{
		componentWidth = frameWidth / 2;
		componentHeight = frameHeight / 2;
		componentStartingIndex = v_starting_index;
	}
	else if (startingIndex >= u_starting_index)
	{
		componentWidth = frameWidth / 2;
		componentHeight = frameHeight / 2;
		componentStartingIndex = u_starting_index;
	}
	else
	{
		componentWidth = frameWidth;
		componentHeight = frameHeight;
		componentStartingIndex = 0;
	}

	int blockX = ((startingIndex - componentStartingIndex) / componentWidth);
	int blockY = ((startingIndex - componentStartingIndex) % componentWidth);

	if (startingIndex < 0)
	{
		blockX--;
		blockY = componentWidth + blockY;
	}

	//THis means that motion vector went beyonde the left edge of the frame
	if (blockY == componentWidth - 1)
	{
		blockY = -1;
		blockX++;
	}

	for (int i = 0; i < blockHeight; i++)
	{
		for (int j = 0; j < blockWidth; j++)
		{
			int pixelColumn = blockY + j;
			int pixelRow = blockX + i;

			if (pixelRow >= componentHeight)
				pixelRow = componentHeight - 1;

			if (pixelColumn >= componentWidth)
				pixelColumn = componentWidth - 1;

			if (pixelRow < 0)
				pixelRow = 0;

			if (pixelColumn < 0)
				pixelColumn = 0;


			resultBlock[i * blockWidth + j] = data[componentStartingIndex + pixelRow * componentWidth + pixelColumn];
		}
	}

}

void BlockPartition::get1dBlockByStartingIndexWithOffset(char* data, unsigned char* resultBlock, int yOffset, int xOffset, int startingIndex, int cuBlockSize, int frameWidth, int frameHeight, int puWidth, int puHeight, int puOffset)
{
	int rowOffset, colOffset;
	if (puWidth == cuBlockSize)
	{
		rowOffset = puOffset % 2 * (cuBlockSize - puHeight);
		colOffset = 0;
	}
	else
	{
		rowOffset = puOffset / 2 * (cuBlockSize - puHeight);
		colOffset = puOffset % 2 * (cuBlockSize - puWidth);
	}

	int v_starting_index = frameWidth * frameHeight + (frameWidth * frameHeight / 4);
	int u_starting_index = frameWidth * frameHeight;

	int componentWidth, componentHeight, componentStartingIndex;

	if (startingIndex >= v_starting_index)
	{
		componentWidth = frameWidth / 2;
		componentHeight = frameHeight / 2;
		componentStartingIndex = v_starting_index;
	}
	else if (startingIndex >= u_starting_index)
	{
		componentWidth = frameWidth / 2;
		componentHeight = frameHeight / 2;
		componentStartingIndex = u_starting_index;
	}
	else
	{
		componentWidth = frameWidth;
		componentHeight = frameHeight;
		componentStartingIndex = 0;
	}

	int blockX = ((startingIndex - componentStartingIndex) / componentWidth) + xOffset;
	int blockY = ((startingIndex - componentStartingIndex) % componentWidth) + yOffset;

	for (int i = 0; i < puHeight; i++)
	{
		for (int j = 0; j < puWidth; j++)
		{
			int pixelColumn = blockY + j;
			int pixelRow = blockX + i;

			if (pixelRow >= componentHeight)
				pixelRow = componentHeight - 1;

			if (pixelColumn >= componentWidth)
				pixelColumn = componentWidth - 1;

			if (pixelRow < 0)
				pixelRow = 0;

			if (pixelColumn < 0)
				pixelColumn = 0;

			resultBlock[(i + rowOffset) * cuBlockSize + j + colOffset] = data[componentStartingIndex + pixelRow * componentWidth + pixelColumn];
		}
	}
}

/*-----------------------------------------------------------------------------
Function name: fill1dBlockByStartingIndex
Description: Does the opposit of the method get1dBlockByStartingIndex. Takes one block and adds it to frame, depending on starting index
Parameters:
	-	block - block that will be added to frame
	-	startingIndex - starting index in frame
	-	blockSize - size of the block
	-	data - frame data container where block is to be put

Returns: fills frame container (data variable)

Author: Piljic
Current version: v0.1
Version history:

	-	v 0.1. - Adds one block to frame data container

Comments: Adds block depending on color component (luma or chroma). For now that component is recognized by starting index
-----------------------------------------------------------------------------*/
void BlockPartition::fill1dBlockByStartingIndex(unsigned char* block, int startingIndex, int blockSize, unsigned char* data, bool* dataIsValid, int frameWidth, int frameHeight)
{
	int v_starting_index = frameWidth * frameHeight + (frameWidth * frameHeight / 4);
	int u_starting_index = frameWidth * frameHeight;

	int wid, hei, row, column;
	if (startingIndex >= v_starting_index)
	{
		wid = frameWidth / 2;
		hei = frameHeight / 2;
		row = ((startingIndex - v_starting_index) / wid) / blockSize;
		column = ((startingIndex - v_starting_index) % wid) / blockSize;
	}
	else if (startingIndex >= u_starting_index)
	{
		wid = frameWidth / 2;
		hei = frameHeight / 2;
		row = ((startingIndex - u_starting_index) / wid) / blockSize;
		column = ((startingIndex - u_starting_index) % wid) / blockSize;
	}
	else
	{
		wid = frameWidth;
		hei = frameHeight;
		row = (startingIndex / wid) / blockSize;
		column = (startingIndex % wid) / blockSize;
	}


	int x_coordinate = blockSize * row;
	int y_coordinate = column * blockSize;

	int data_index = startingIndex;

	for (int i = 0; i < blockSize; i++)
	{
		for (int j = 0; j < blockSize; j++)
		{
			int index = data_index + wid * i + j;
			if (x_coordinate + i >= hei || y_coordinate + j >= wid)
			{
			}
			else
			{
				data[index] = block[i * blockSize + j];
				dataIsValid[index] = true;
			}
		}
	}
}



/*-----------------------------------------------------------------------------
Function name: getReferenceSamples
Description: extracts reference samples for certain CU (depending on starting index and blockSize)
Parameters:
	-	startingIndex - starting index of the CU for which reference samples are extracted
	-	blockSize -	block size of the CU for which reference samples are extracted
	-	frameWidth -
	-	frameHeight -
	-	payload - raw payload (usually
	picture)
	-	isValid - array that indicates wheter certain pixel has been reconstructed, it is the same size as payload
	-	refSamples	- out parameter where reference samples are stored
	-	refSamplesValid - out parameter that indicates whether certain reference sample is available
Returns: refSamples, refSamplesValid. Size of output parameters is 2 * block_size(left) + 1(top left) + 2 * block_size(above)

Author: Piljic
Current version: v0.1
Version history:

	-	v 0.1. - Reference samples are fetched in following order: first left samples are fetched, then left top sample, then to samples

Comments: Fetching reference samples depends on the color component, which is now recognized by starting index
-----------------------------------------------------------------------------*/
void BlockPartition::getReferenceSamples(int startingIndex, int startingIndexTile, int blockSize, int frameWidth, int frameHeight, int tileWidth, int tileHeight, unsigned char* payload, bool* isValid, unsigned char* refSamples, bool* refSamplesValid)
{

	//detecting if it is Y,U or V component 
	int uStartingIndex = frameWidth * frameHeight;
	int vStartingIndex = frameWidth * frameHeight + (frameWidth * frameHeight / 4);

	int uStartingIndexTile = tileWidth * tileHeight;
	int vStartingIndexTile = tileWidth * tileHeight + (tileWidth * tileHeight / 4);

	int wid, hei, x_coordinate, y_coordinate;
	bool isLeftCorner, isUpper, isBottomInTile, isRightInTile;
	int indexOffset;
	if (startingIndex >= vStartingIndex)
	{
		wid = frameWidth / 2;
		hei = frameHeight / 2;
		x_coordinate = (startingIndex - vStartingIndex) % wid;
		y_coordinate = (startingIndex - vStartingIndex) / wid;
		isLeftCorner = (startingIndexTile - vStartingIndexTile) % (tileWidth / 2) == 0;
		isBottomInTile = ((startingIndexTile - vStartingIndexTile) / (tileWidth / 2)) + blockSize == tileHeight / 2;
		isRightInTile = ((startingIndexTile - vStartingIndexTile) % (tileWidth / 2)) + blockSize == tileWidth / 2;
		isUpper = (startingIndexTile - vStartingIndexTile) < (tileWidth / 2);
		indexOffset = vStartingIndex;
	}
	else if (startingIndex >= uStartingIndex)
	{
		wid = frameWidth / 2;
		hei = frameHeight / 2;
		x_coordinate = (startingIndex - uStartingIndex) % wid;
		y_coordinate = (startingIndex - uStartingIndex) / wid;
		isLeftCorner = (startingIndexTile - uStartingIndexTile) % (tileWidth / 2) == 0;
		isBottomInTile = ((startingIndexTile - uStartingIndexTile) / (tileWidth / 2)) + blockSize == tileHeight / 2;
		isRightInTile = ((startingIndexTile - uStartingIndexTile) % (tileWidth / 2)) + blockSize == tileWidth / 2;
		isUpper = (startingIndexTile - uStartingIndexTile) < (tileWidth / 2);
		indexOffset = uStartingIndex;
	}
	else
	{
		wid = frameWidth;
		hei = frameHeight;
		x_coordinate = startingIndex % wid;
		y_coordinate = startingIndex / wid;
		isLeftCorner = startingIndexTile % tileWidth == 0;
		isBottomInTile = (startingIndexTile / tileWidth) + blockSize == tileHeight;
		isRightInTile = (startingIndexTile % tileWidth) + blockSize == tileWidth;
		isUpper = startingIndexTile < tileWidth;
		indexOffset = 0;
	}


	int i = 0, sample_iterator = 0, index;
	bool isPixelAvailable = false;

	//Search for left pixels
	for (i = blockSize * 2 - 1; i >= 0; i--)
	{
		index = indexOffset + (y_coordinate + i) * wid + (x_coordinate - 1);
		isPixelAvailable = !(isBottomInTile && i >= blockSize) && (!isLeftCorner) && ((y_coordinate + i) < hei) && isValid[index];

		refSamplesValid[sample_iterator] = isPixelAvailable;

		if (isPixelAvailable)
		{
			refSamples[sample_iterator] = payload[index];
		}
		else
		{
			refSamples[sample_iterator] = 0;
		}

		sample_iterator++;
	}

	//Search for left upper pixel
	index = indexOffset + (y_coordinate - 1) * wid + (x_coordinate - 1);
	isPixelAvailable = (!isLeftCorner) && (!isUpper) && ((y_coordinate - 1) < hei) && ((x_coordinate - 1) < wid) && isValid[index];
	refSamplesValid[sample_iterator] = isPixelAvailable;
	if (isPixelAvailable)
	{
		refSamples[sample_iterator] = payload[index];
	}
	else
	{
		refSamples[sample_iterator] = 0;
	}
	sample_iterator++;

	//Search for upper pixels
	for (i = 0; i < blockSize * 2; i++)
	{
		index = indexOffset + (y_coordinate - 1) * wid + (x_coordinate + i);
		isPixelAvailable = !(isRightInTile && i >= blockSize) && (!isUpper) && ((x_coordinate + i) < wid) && isValid[index];

		refSamplesValid[sample_iterator] = isPixelAvailable;

		if (isPixelAvailable)
		{
			refSamples[sample_iterator] = payload[index];
		}
		else
		{
			refSamples[sample_iterator] = 0;
		}

		sample_iterator++;
	}
}

/*-----------------------------------------------------------------------------
Function name: isCornerLeft
Description: checks if CU/CTU is corner left in frame
Parameters:
	-	startingIndex - starting index of CU/CTU
	-	frameWidth

Returns: true if CU is on the left corner in frame

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerLeft(int startingIndex, int frameWidth)
{
	return startingIndex % frameWidth == 0;
}

/*-----------------------------------------------------------------------------
Function name: isCornerRight
Description: checks if CU/CTU is corner right in frame
Parameters:
	-	startingIndex - starting index of CU/CTU
	-	frameWidth
	-	blockSize - block size of CU/CTU

Returns: true if CU is on the right corner in frame

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerRight(int startingIndex, int frameWidth, int blockSize)
{
	return (startingIndex % frameWidth) + blockSize >= frameWidth;
}

/*-----------------------------------------------------------------------------
Function name: isCornerUpper
Description: checks if CU/CTU is corner upper in frame
Parameters:
	-	startingIndex - starting index of CU/CTU
	-	frameWidth

Returns: true if CU is on the upper corner in frame

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerUpper(int startingIndex, int frameWidth)
{
	return startingIndex < frameWidth;
}

/*-----------------------------------------------------------------------------
Function name: isCornerBottom
Description: checks if CU/CTU is corner bottom in frame
Parameters:
-	startingIndex - starting index of CU/CTU
-	frameWidth

Returns: true if CU is on the bottom corner in frame

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerBottom(int startingIndex, int blockSize, int frameWidth, int frameHeight)
{
	return startingIndex + blockSize * frameWidth >= frameWidth * frameHeight;
}

/*-----------------------------------------------------------------------------
Function name: isCornerLeftInCTU
Description: checks if CU is corner left in parent CTU
Parameters:
-	cu - current cu for which check is done
-	frameWidth

Returns: true if CU is on the left corner in  parent CTU

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerLeftInCTU(CU* cu, int frameWidth)
{
	CU* ctu_parent = getParentCTU(cu);

	return (cu->cbY.startingIndex - ctu_parent->cbY.startingIndex) % frameWidth == 0;
}

/*-----------------------------------------------------------------------------
Function name: isCornerLeftInCTU
Description: checks if CU is corner left in parent CTU
Parameters:
-	cu - current cu for which check is done
-	frameWidth

Returns: true if CU is on the left corner in  parent CTU

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerRightInCTU(CU* cu, int frameWidth)
{
	CU* ctu_parent = getParentCTU(cu);

	int cuSI = cu->cbY.startingIndex % frameWidth;
	int ctuSI = ctu_parent->cbY.startingIndex % frameWidth;

	return (cuSI - ctuSI) + cu->cbY.blockSize == ctu_parent->cbY.blockSize;
}

/*-----------------------------------------------------------------------------
Function name: isCornerUpperInCTU
Description: checks if CU is corner upper in parent CTU
Parameters:
	-	cu - current cu for which check is done

Returns: true if CU is on the upper corner in  parent CTU

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerUpperInCTU(CU* cu)
{
	CU* ctu_parent = getParentCTU(cu);

	return (cu->cbY.startingIndex - ctu_parent->cbY.startingIndex) < ctu_parent->cbY.blockSize;

}

/*-----------------------------------------------------------------------------
Function name: isCornerBottomInCTU
Description: checks if CU is corner bottom in parent CTU
Parameters:
-	cu - current cu for which check is done

Returns: true if CU is on the bottom corner in  parent CTU

Author: Piljic
Current version: v0.1
-----------------------------------------------------------------------------*/
bool BlockPartition::isCornerBottomInCTU(CU* cu, int frameWidth)
{
	CU* ctu_parent = getParentCTU(cu);

	int cuO = cu->cbY.startingIndex + cu->cbY.blockSize * frameWidth;
	int ctuO = ctu_parent->cbY.startingIndex + ctu_parent->cbY.blockSize * frameWidth;

	return cuO >= ctuO;

}

/*-----------------------------------------------------------------------------
Function name: getParentCTU
Description: methods returns CTU in which current CU is
Parameters:
	-	cu - current cu for which check is done
Returns:

Author: Piljic
Current version: v0.2.

	-	v0.1. - method returning CTU parent
	-	v0.2. - if CTU is passed as an argument, that CTU is returned

Comments: CTU does not have to be first parent of current cu, CTU is the last CU and does not have parent
-----------------------------------------------------------------------------*/
CU* BlockPartition::getParentCTU(CU* cu)
{

	int cuDepth = cu->depth;

	if (cuDepth == 0)
		return cu;

	CU* ctu_parent = cu->parent;
	CU* current = cu;

	while (cuDepth != 1)
	{
		current = current->parent;
		cuDepth = current->depth;
	}

	CU* ctu = current->parent;

	return ctu;
}

/*-----------------------------------------------------------------------------
Function name: getCUPath
Description: finds the unique path from itself to CTU (indexes)
Parameters:
	-	cu - current cu for which path is required
	-	indexes - out parameter, array of indexes from lower to upper
	-	depth - depth of current CU
Returns: array of CU indexes that uniquely define each CU

Author: Piljic
Current version: v 0.1
TODO: Extract depth from CU, instead of passing it as parameter
-----------------------------------------------------------------------------*/
void BlockPartition::getCUPath(CU* cu, int* indexes, int depth)
{

	int i = 0;
	do
	{
		indexes[i] = cu->index;

		if (depth != 1)
			cu = cu->parent;

		i++;
		depth--;

	} while (depth != 0);

}

/*-----------------------------------------------------------------------------
Function name: getCUPathWithCTU
Description: finds the unique path from itself to CTU (indexes) including CTU i ndex in frame
Parameters:
-	cu - current cu for which path is required
-	indexes - out parameter, array of indexes from lower to upper
-	depth - depth of current CU
Returns: array of CU indexes that uniquely define each CU

Author: Piljic
Current version: v 0.1
TODO: Extract depth from CU, instead of passing it as parameter
-----------------------------------------------------------------------------*/
void BlockPartition::getCUPathWithCTU(CU* cu, int* indexes, int depth)
{

	int i = 0;
	do
	{
		indexes[i] = cu->index;

		if (depth != 0)
			cu = cu->parent;

		i++;
		depth--;

	} while (depth >= 0);

}

/*-----------------------------------------------------------------------------
Function name: getTopLeftChild
Description: finds the most top left child in CU
Parameters:
-	cu - CU in which top left child is extracted
Returns: most top and most left CU child

Author: Piljic
Current version: v0.1.
Comments: It depends how many times CU has been split, but function will always return most top and most left child
-----------------------------------------------------------------------------*/
CU* BlockPartition::getTopLeftChild(CU* cu)
{
	while (cu->hasChildren)
	{
		cu = cu->children[0];
	}

	return cu;
}

/*-----------------------------------------------------------------------------
Function name: getTopRightChild
Description: finds the most top right child in CU
Parameters:
	-	cu - CU in which top right child is extracted
Returns: most top and most right CU child

Author: Piljic
Current version: v0.1.
Comments: It depends how many times CU has been split, but function will always return most top and most right child
-----------------------------------------------------------------------------*/
CU* BlockPartition::getTopRightChild(CU* cu)
{
	while (cu->hasChildren)
	{
		cu = cu->children[1];
	}

	return cu;
}

/*-----------------------------------------------------------------------------
Function name: getBottomLeftChild
Description: finds the most bottom left child in CU
Parameters:
-	cu - CU in which bottom left child is extracted
Returns: most bottom and most left CU child

Author: Piljic
Current version: v0.1.
Comments: It depends how many times CU has been split, but function will always return most bottom and most left child
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomLeftChild(CU* cu)
{
	while (cu->hasChildren)
	{
		cu = cu->children[2];
	}

	return cu;
}

/*-----------------------------------------------------------------------------
Function name: getBottomRightChild
Description: finds the most bottom right child in CU
Parameters:
-	cu - CU in which bottom right child is extracted
Returns: most bottom and most right CU child

Author: Piljic
Current version: v0.1.
Comments: It depends how many times CU has been split, but function will always return most bottom and most right child
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomRightChild(CU* cu)
{
	while (cu->hasChildren)
	{
		cu = cu->children[3];
	}

	return cu;
}

/*-----------------------------------------------------------------------------
Function name: getLeftBrotherInSameParent
Description: finds the left brother in same parent, not matter if the parent is CU or CTU
Parameters:
	-	cu - current CU for which left borhter is fetched
	-	index - index of current CU in parent (can be 0,1,2 or 3)
Returns: CU that is left of current CU, but in same parent CU

Author: Piljic
Current version: v0.1.
Comments: Index can be 1 or 3 ! otherwise there is no left brother
TODO: Make check if left brother exists
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftBrotherInSameParent(CU* cu, int index)
{
	return cu->parent->children[index - 1];
}

/*-----------------------------------------------------------------------------
Function name: getRightBrotherInSameParent
Description: finds the right brother in same parent, not matter if the parent is CU or CTU
Parameters:
-	cu - current CU for which right borhter is fetched
-	index - index of current CU in parent (can be 0,1,2 or 3)
Returns: CU that is right of current CU, but in same parent CU

Author: Piljic
Current version: v0.1.
Comments: Index can be 0 or 2 ! otherwise there is no left brother
TODO: Make check if right brother exists
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightBrotherInSameParent(CU* cu, int index)
{
	return cu->parent->children[index + 1];
}

/*-----------------------------------------------------------------------------
Function name: getAboveBrotherInSameParent
Description: finds the above brother in same parent, not matter if the parent is CU or CTU
Parameters:
-	cu - current CU for which above brotheris fetched
-	index - index of current CU in parent (can be 0,1,2 or 3)
Returns: CU that is above of current CU, but in same parent CU

Author: Piljic
Current version: v0.1.
Comments: Index can be 2 or 3 ! otherwise there is no above brother
TODO: Make check if above brother exists
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveBrotherInSameParent(CU* cu, int index)
{
	return cu->parent->children[index - 2];
}

/*-----------------------------------------------------------------------------
Function name: getBottomBrotherInSameParent
Description: finds the bottom brother in same parent, not matter if the parent is CU or CTU
Parameters:
-	cu - current CU for which bottom brotheris fetched
-	index - index of current CU in parent (can be 0,1,2 or 3)
Returns: CU that is bottom of current CU, but in same parent CU

Author: Piljic
Current version: v0.1.
Comments: Index can be 0 or 1 ! otherwise there is no above brother
TODO: Make check if bottom brother exists
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomBrotherInSameParent(CU* cu, int index)
{
	return cu->parent->children[index + 2];
}

/*-----------------------------------------------------------------------------
Function name: getLeftNeighbourFromRoot
Description: finds left neigbour of cu with specified indexes
Parameters:
	-	cu - root from which neighbour is fetched
	-	indexes - path of CU for which left neigbour is being fetched
	-	z - length of indexes array
Returns: CU that is leftNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
	-	if the index in indexes (path of CU in question) is 0 then its neighbour is surely in CU with index 1,
		same goes if index is 2, then its neighbour is surely in CU with index 3
		NOTE: indexes are received from deppest upwards, now, we go from root deeper
	-	when the end is reached, top right child of that CU is fetched, in case that CU is divided more times than CU in question
	-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 1 or 3 all others should be 0 or 2*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index + 1];
	}
	return getTopRightChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getRightNeighbourFromRoot
Description: finds right neigbour of cu with specified indexes
Parameters:
-	cu - root from which neighbour is fetched
-	indexes - path of CU for which right neigbour is being fetched
-	z - length of indexes array
Returns: CU that is rightNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
-	if the index in indexes (path of CU in question) is 0 then its neighbour is surely in CU with index 1,
same goes if index is 2, then its neighbour is surely in CU with index 3
NOTE: indexes are received from deppest upwards, now, we go from root deeper
-	when the end is reached, bottom right child of that CU is fetched, in case that CU is divided more times than CU in question
-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 1 or 3 all others should be 0 or 2*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index - 1];
	}
	return getTopLeftChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getLeftBottomNeighbourFromRoot
Description: finds left bottom neigbour of cu with specified indexes
Parameters:
-	cu - root from which neighbour is fetched
-	indexes - path of CU for which left bottom neigbour is being fetched
-	z - length of indexes array
Returns: CU that is leftNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
-	if the index in indexes (path of CU in question) is 0 then its neighbour is surely in CU with index 1,
same goes if index is 2, then its neighbour is surely in CU with index 3
NOTE: indexes are received from deppest upwards, now, we go from root deeper
-	when the end is reached, bottom right child of that CU is fetched, in case that CU is divided more times than CU in question
-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftBottomNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 1 or 3 all others should be 0 or 2*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index + 1];
	}
	return getBottomRightChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getRightBottomNeighbourFromRoot
Description: finds right bottom neigbour of cu with specified indexes
Parameters:
-	cu - root from which neighbour is fetched
-	indexes - path of CU for which right bottom neigbour is being fetched
-	z - length of indexes array
Returns: CU that is rightNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
-	if the index in indexes (path of CU in question) is 0 then its neighbour is surely in CU with index 1,
same goes if index is 2, then its neighbour is surely in CU with index 3
NOTE: indexes are received from deppest upwards, now, we go from root deeper
-	when the end is reached, bottom right child of that CU is fetched, in case that CU is divided more times than CU in question
-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightBottomNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 1 or 3 all others should be 0 or 2*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index - 1];
	}
	return getBottomLeftChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getAboveNeighbourFromRoot
Description: finds above neigbour of cu with specified indexes
Parameters:
	-	cu - root from which neighbour is fetched
	-	indexes - path of CU for which above neigbour is being fetched
	-	z - length of indexes array
Returns: CU that is aboveNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
	-	if the index in indexes (path of CU in question) is 0 then its neighbour is surely in CU with index 2,
		same goes if index is 1, then its neighbour is surely in CU with index 3
		NOTE: indexes are received from deppest upwards, now, we go from root deeper
	-	when the end is reached, bottom left child of that CU is fetched, in case that CU is divided more times than CU in question
	-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 2 or 3 all others should be 0 or 1*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index + 2];
	}
	return getBottomLeftChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getAboveRightNeighbourFromRoot
Description: finds above right neigbour of cu with specified indexes
Parameters:
-	cu - root from which neighbour is fetched
-	indexes - path of CU for which above right neigbour is being fetched
-	z - length of indexes array
Returns: CU that is aboveNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
-	if the index in indexes (path of CU in question) is 0 then its neighbour is surely in CU with index 2,
same goes if index is 1, then its neighbour is surely in CU with index 3
NOTE: indexes are received from deppest upwards, now, we go from root deeper
-	when the end is reached, bottom right child of that CU is fetched, in case that CU is divided more times than CU in question
-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveRightNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 2 or 3 all others should be 0 or 1*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index + 2];
	}
	return getBottomRightChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getBottomNeighbourFromRoot
Description: finds bottom neigbour of cu with specified indexes
Parameters:
-	cu - root from which neighbour is fetched
-	indexes - path of CU for which bottom neigbour is being fetched
-	z - length of indexes array
Returns: CU that is bottomNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
-	if the index in indexes (path of CU in question) is 2 then its neighbour is surely in CU with index 0,
same goes if index is 3, then its neighbour is surely in CU with index 1
NOTE: indexes are received from deppest upwards, now, we go from root deeper
-	when the end is reached, bottom right child of that CU is fetched, in case that CU is divided more times than CU in question
-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 0 or  all others should be 2 or 3*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index - 2];
	}
	return getTopLeftChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getBottomRightNeighbourFromRoot
Description: finds bottom right neigbour of cu with specified indexes
Parameters:
-	cu - root from which neighbour is fetched
-	indexes - path of CU for which above right neigbour is being fetched
-	z - length of indexes array
Returns: CU that is bottomNeighbour of CU with defined indexes

Author: Piljic
Current version: v0.1.
Comments:
-	if the index in indexes (path of CU in question) is 2 then its neighbour is surely in CU with index 0,
same goes if index is 3, then its neighbour is surely in CU with index 1
NOTE: indexes are received from deppest upwards, now, we go from root deeper
-	when the end is reached, bottom right child of that CU is fetched, in case that CU is divided more times than CU in question
-	in case neighbour is split less times, this will still work
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomRightNeighbourFromRoot(CU* cu, int* indexes, int z)
{
	/*Only first index in indexes is 0 or  all others should be 2 or 3*/
	int index;
	while (z != 0 && cu->hasChildren)
	{
		z--;
		index = indexes[z];
		cu = cu->children[index - 2];
	}
	return getTopRightChild(cu);
}

/*-----------------------------------------------------------------------------
Function name: getLeftInSameCTU
Description: gets left CU neigbour if they are in same CTU
Parameters:
	-	cu - current CU for whic left neigbouhr is fetched
	-	indexes - array that uniquely descibes cu and its path to the parent that has its left neighbour
Returns: CU that is left neighbout in same CTU

Author: Piljic
Current version: v0.1.
Comments:
	-	indexes are calculated until first parent has index 1 or 3, that means that his left CU in same parent contains left neighbour
	-	left CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 1 && index != 3)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getLeftBrotherInSameParent(root, index);

	return getLeftNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getRightInSameCTU
Description: gets right CU neigbour if they are in same CTU
Parameters:
-	cu - current CU for whic right neigbouhr is fetched
-	indexes - array that uniquely descibes cu and its path to the parent that has its left neighbour
Returns: CU that is right neighbout in same CTU

Author: Piljic
Current version: v0.1.
Comments:
-	indexes are calculated until first parent has index 1 or 3, that means that his left CU in same parent contains left neighbour
-	left CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 0 && index != 2)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getRightBrotherInSameParent(root, index);

	return getRightNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getLeftBottomInSameCTU
Description: gets left-bottom CU neigbour if they are in same CTU
Parameters:
-	cu - current CU for which left bottom neigbour is fetched
-	indexes - array that uniquely descibes cu and its path to the parent that has its left bottom neighbour
Returns: CU that is left bottom neighbout in same CTU

Author: Piljic
Current version: v0.1.
Comments:
-	indexes are calculated until first parent has index 1 or 3, that means that his left CU in same parent contains left neighbour
-	left CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftBottomInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 1 && index != 3)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getLeftBrotherInSameParent(root, index);

	return getLeftBottomNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getRightBottomInSameCTU
Description: gets right-bottom CU neigbour if they are in same CTU
Parameters:
-	cu - current CU for which right bottom neigbour is fetched
-	indexes - array that uniquely descibes cu and its path to the parent that has its left bottom neighbour
Returns: CU that is left bottom neighbout in same CTU

Author: Piljic
Current version: v0.1.
Comments:
-	indexes are calculated until first parent has index 0 or 2, that means that his right CU in same parent contains right neighbour
-	right CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightBottomInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 0 && index != 2)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getRightBrotherInSameParent(root, index);

	return getRightBottomNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getAboveInSameCTU
Description: gets above CU neigbour if they are in same CTU
Parameters:
	-	cu - current CU for which above neigbour is fetched
	-	indexes - array that uniquely descibes CU and its path to the parent that has its above neighbour
Returns: CU that is above neighbour in same CTU

Author: Piljic
Current version: v0.1.
Comments:
	-	indexes are calculated until first parent has index 2 or 3, that means that his above CU in same parent contains above neighbour
	-	above CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 2 && index != 3)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getAboveBrotherInSameParent(root, index);

	return getAboveNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getAboveRightInSameCTU
Description: gets above right CU neigbour if they are in same CTU
Parameters:
-	cu - current CU for which above right neigbour is fetched
-	indexes - array that uniquely descibes CU and its path to the parent that has its above right neighbour
Returns: CU that is above right neighbour in same CTU

Author: Piljic
Current version: v0.1.
Comments:
-	indexes are calculated until first parent has index 2 or 3, that means that his above CU in same parent contains above right neighbour
-	above CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveRightInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 2 && index != 3)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getAboveBrotherInSameParent(root, index);

	return getAboveRightNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getBottomInSameCTU
Description: gets bottom  CU neigbour if they are in same CTU
Parameters:
-	cu - current CU for which bottom  neigbour is fetched
-	indexes - array that uniquely descibes CU and its path to the parent that has its bottom  neighbour
Returns: CU that is bottom right neighbour in same CTU

Author: Piljic
Current version: v0.1.
Comments:
-	indexes are calculated until first parent has index 2 or 3, that means that his above CU in same parent contains above right neighbour
-	above CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 0 && index != 1)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getBottomBrotherInSameParent(root, index);

	return getBottomNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getBottomRightInSameCTU
Description: gets bottom right CU neigbour if they are in same CTU
Parameters:
-	cu - current CU for which bottom right neigbour is fetched
-	indexes - array that uniquely descibes CU and its path to the parent that has its bottom right neighbour
Returns: CU that is bottom right neighbour in same CTU

Author: Piljic
Current version: v0.1.
Comments:
-	indexes are calculated until first parent has index 2 or 3, that means that his above CU in same parent contains above right neighbour
-	above CU in same parent is then passed as a root to find neigbour
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomRightInSameCTU(CU* cu, int* indexes)
{
	int z = 0;
	int index = indexes[z];
	CU* root = cu;

	while (index != 0 && index != 1)
	{
		z++;
		index = indexes[z];

		root = root->parent;
	}

	CU* neighbour = getBottomBrotherInSameParent(root, index);

	return getBottomRightNeighbourFromRoot(neighbour, indexes, z);
}

/*-----------------------------------------------------------------------------
Function name: getLeftCodingUnit
Description: gets left CU of current CU in frame (see sketch)
Parameters:
	-	ctuTree - CTU Tree of entire frame
	-	current - current CU for which left neihbour is fetched
	-	isAvailable - out parameter showing if current Cu has left neihgbour
	-	frameWidth
Returns: left CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is left CU in same CTU as current CU or it is contained in different (left) CTU

-- searching for X --

+---+-----------+
| X |           |
+---+           |
	|  current  |
	|			|
	|			|
	+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{
	bool isCornerLeftInFrame = isCornerLeft(current->cbY.startingIndex, frameWidth);

	if (isCornerLeftInFrame)
	{
		isAvailable = false;
		return current;
	}

	isAvailable = true;
	if (current->depth == 0)
	{
		return getTopRightChild(ctuTree[current->index - 1]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];

	bool isCornerLeftInsideCTU = isCornerLeftInCTU(current, frameWidth);

	getCUPath(current, indexes, currentCuDepth);
	CU* lBlock;

	if (!isCornerLeftInsideCTU)
	{
		lBlock = getLeftInSameCTU(current, indexes);
	}
	else
	{
		CU* ctu_root = ctuTree[getParentCTU(current)->index - 1];
		lBlock = getLeftNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return lBlock;
}

/*-----------------------------------------------------------------------------
Function name: getRightCodingUnit
Description: gets right CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which right neihbour is fetched
-	isAvailable - out parameter showing if current Cu has right neihgbour
-	frameWidth
Returns: right CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is right CU in same CTU as current CU or it is contained in different (left) CTU

-- searching for X --

+-----------+---+
|           | X |
|           +---+
|  current  |
|			|
|			|
+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{

	bool isCornerRightInFrame = isCornerRight(current->cbY.startingIndex, frameWidth, current->cbY.blockSize);

	if (isCornerRightInFrame)
	{
		isAvailable = false;
		return current;
	}

	isAvailable = true;

	if (current->depth == 0)
	{
		return getTopLeftChild(ctuTree[current->index + 1]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];

	bool isCornerRightInsideCTU = isCornerRightInCTU(current, frameWidth);

	getCUPath(current, indexes, currentCuDepth);
	CU* rBlock;

	if (!isCornerRightInsideCTU)
	{
		rBlock = getRightInSameCTU(current, indexes);
	}
	else
	{
		CU* ctu_root = ctuTree[getParentCTU(current)->index + 1];
		rBlock = getRightNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return rBlock;
}

/*-----------------------------------------------------------------------------
Function name: getAboveCodingUnit
Description: gets above CU of current CU in frame (see sketch)
Parameters:
	-	ctuTree - CTU Tree of entire frame
	-	current - current CU for which above neighbour is fetched
	-	isAvailable - out parameter showing if current CU has above neihgbour
	-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --
+---+
| X |
+---+-------+
|           |
|           |
|  current  |
|			|
|			|
+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{
	bool isCornerAboveInFrame = isCornerUpper(current->cbY.startingIndex, frameWidth);

	if (isCornerAboveInFrame)
	{
		isAvailable = false;
		return current; //just so we can return anything...
	}

	isAvailable = true;
	int number_of_blocks_row = (int)ceil((double)frameWidth / ctuTree[0]->cbY.blockSize);

	if (current->depth == 0)
	{
		return getBottomLeftChild(ctuTree[current->index - number_of_blocks_row]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];


	bool isCorneAboveInsideCTU = isCornerUpperInCTU(current);

	getCUPath(current, indexes, currentCuDepth);
	CU* aBlock;

	if (!isCorneAboveInsideCTU)
	{
		aBlock = getAboveInSameCTU(current, indexes);
	}
	else
	{
		CU* parent = getParentCTU(current);
		number_of_blocks_row = (int)ceil((double)frameWidth / parent->cbY.blockSize);
		CU* ctu_root = ctuTree[parent->index - number_of_blocks_row];
		aBlock = getAboveNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return aBlock;

}

/*-----------------------------------------------------------------------------
Function name: getBottomCodingUnit
Description: gets bottomCU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which bottom neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is bottom CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

+-----------+
|           |
|           |
|  current  |
|			|
|			|
+---+-------+
| X |
+---+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth, int frameHeight)
{



	bool isCornerBottomInFrame = isCornerBottom(current->cbY.startingIndex, current->cbY.blockSize, frameWidth, frameHeight);

	if (isCornerBottomInFrame)
	{
		isAvailable = false;
		return current;
	}

	isAvailable = true;
	int number_of_blocks_row = (int)ceil((double)frameWidth / ctuTree[0]->cbY.blockSize);

	if (current->depth == 0)
	{
		return getTopLeftChild(ctuTree[current->index + number_of_blocks_row]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];

	bool isCorneBottomInsideCTU = isCornerBottomInCTU(current, frameWidth);

	getCUPath(current, indexes, currentCuDepth);
	CU* bBlock;

	if (!isCorneBottomInsideCTU)
	{
		bBlock = getBottomInSameCTU(current, indexes);
	}
	else
	{
		CU* parent = getParentCTU(current);
		number_of_blocks_row = (int)ceil((double)frameWidth / parent->cbY.blockSize);
		CU* ctu_root = ctuTree[parent->index + number_of_blocks_row];
		bBlock = getBottomNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return bBlock;
}

/*-----------------------------------------------------------------------------
Function name: getLeftBottomCodingUnit
Description: gets left, but at bottom  CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments:

-- searching for X --

	+-----------+
	|           |
	|           |
	|  current  |
+---+			|
| X	|			|
+---+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftBottomCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{
	bool isCornerLeftInFrame = isCornerLeft(current->cbY.startingIndex, frameWidth);

	if (isCornerLeftInFrame)
	{
		isAvailable = false;
		return current;
	}

	isAvailable = true;

	if (current->depth == 0)
	{
		return getBottomRightChild(ctuTree[current->index - 1]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];

	bool isCornerLeftInsideCTU = isCornerLeftInCTU(current, frameWidth);

	getCUPath(current, indexes, currentCuDepth);
	CU* lBlock;

	if (!isCornerLeftInsideCTU)
	{
		lBlock = getLeftBottomInSameCTU(current, indexes);
	}
	else
	{
		CU* ctu_root = ctuTree[getParentCTU(current)->index - 1];
		lBlock = getLeftBottomNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return lBlock;
}

/*-----------------------------------------------------------------------------
Function name: getLeftDownCodingUnit
Description: gets left, but under CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

	+-----------+
	|           |
	|           |
	|  current  |
	|			|
	|			|
+---+-----------+
| X |
+---+

-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftDownCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth, int frameHeight)
{
	CU* left_bottom = getLeftBottomCodingUnit(ctuTree, current, isAvailable, frameWidth);

	if (!isAvailable)
		return current;

	int cuEnd = current->cbY.startingIndex + current->cbY.blockSize * frameWidth;
	int leftBottomEnd = left_bottom->cbY.startingIndex + left_bottom->cbY.blockSize * frameWidth;

	if (left_bottom->cbY.blockSize > current->cbY.blockSize && cuEnd < leftBottomEnd)
		return left_bottom;

	CU* leftDown = getBottomRightCodingUnit(ctuTree, left_bottom, isAvailable, frameWidth, frameHeight);

	if (!isAvailable)
		return current;

	return leftDown;
}

/*-----------------------------------------------------------------------------
Function name: getLeftUpCodingUnit
Description: gets left and above CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --
+---+
| X |
+---+-----------+
	|           |
	|           |
	|  current  |
	|			|
	|			|
	+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getLeftUpCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{

	CU* left = getLeftCodingUnit(ctuTree, current, isAvailable, frameWidth);

	if (!isAvailable)
		return current;

	if (left->cbY.blockSize > current->cbY.blockSize &&
		current->cbY.startingIndex - left->cbY.startingIndex > frameWidth)
		return left;

	CU* leftUp = getAboveRightCodingUnit(ctuTree, left, isAvailable, frameWidth);

	if (!isAvailable)
		return current;

	return leftUp;
}

/*-----------------------------------------------------------------------------
Function name: getAboveRightCodingUnit
Description: gets above right CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --
		+---+
		| X |
+-------+---+
|           |
|           |
|  current  |
|			|
|			|
+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getAboveRightCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{
	bool isCornerAboveInFrame = isCornerUpper(current->cbY.startingIndex, frameWidth);

	if (isCornerAboveInFrame)
	{
		isAvailable = false;
		return current; //just so we can return anything...
	}

	isAvailable = true;
	int number_of_blocks_row = (int)ceil((double)frameWidth / ctuTree[0]->cbY.blockSize);

	if (current->depth == 0)
	{
		return getBottomRightChild(ctuTree[current->index - number_of_blocks_row]);
	}

	int currentCuDepth = current->depth;
	int* indexes = new int[currentCuDepth];

	bool isCorneAboveInsideCTU = isCornerUpperInCTU(current);

	getCUPath(current, indexes, currentCuDepth);
	CU* aBlock;

	if (!isCorneAboveInsideCTU)
	{
		aBlock = getAboveRightInSameCTU(current, indexes);
	}
	else
	{
		CU* parent = getParentCTU(current);
		number_of_blocks_row = (int)ceil((double)frameWidth / parent->cbY.blockSize);
		CU* ctu_root = ctuTree[parent->index - number_of_blocks_row];
		aBlock = getAboveRightNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return aBlock;
}

/*-----------------------------------------------------------------------------
Function name: getRightUpCodingUnit
Description: gets above, but right CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

			+---+
			| X |
+-----------+---+
|           |
|           |
|  current  |
|			|
|			|
+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightUpCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{
	CU* above_right = getAboveRightCodingUnit(ctuTree, current, isAvailable, frameWidth);

	if (!isAvailable)
		return current;

	int arEnd = above_right->cbY.startingIndex + above_right->cbY.blockSize;
	int currEnd = current->cbY.startingIndex + current->cbY.blockSize;

	if (above_right->cbY.blockSize > current->cbY.blockSize &&
		(arEnd - currEnd) % frameWidth != 0)
		return above_right;

	CU* rightUp = getRightBottomCodingUnit(ctuTree, above_right, isAvailable, frameWidth);

	if (!isAvailable)
		return current;

	return rightUp;
}

/*-----------------------------------------------------------------------------
Function name: getRightDownCodingUnit
Description: gets right, but under CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has right neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

+-----------+
|           |
|           |
|  current  |
|			|
|			|
+-----------+---+
			| X |
			+---+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightDownCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth, int frameHeight)
{
	CU* right_bottom = getRightBottomCodingUnit(ctuTree, current, isAvailable, frameWidth);

	if (!isAvailable)
		return current;

	int cuEnd = current->cbY.startingIndex + current->cbY.blockSize * frameWidth;
	int rightBottomEnd = right_bottom->cbY.startingIndex + right_bottom->cbY.blockSize * frameWidth;

	if (right_bottom->cbY.blockSize > current->cbY.blockSize && rightBottomEnd - cuEnd != current->cbY.blockSize)
		return right_bottom;

	CU* rightDown = getBottomCodingUnit(ctuTree, right_bottom, isAvailable, frameWidth, frameHeight);

	if (!isAvailable)
		return current;

	return rightDown;
}

/*-----------------------------------------------------------------------------
Function name: getBottomRightCodingUnit
Description: gets bottom, but right CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which bottom, right neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

+-----------+
|           |
|           |
|  current  |
|			|
|			|
+-------+---+
		| X |
		+---+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getBottomRightCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth, int frameHeight)
{



	bool isCornerBottomInFrame = isCornerBottom(current->cbY.startingIndex, current->cbY.blockSize, frameWidth, frameHeight);

	if (isCornerBottomInFrame)
	{
		isAvailable = false;
		return current; //just so we can return anything...
	}

	isAvailable = true;
	int number_of_blocks_row = (int)ceil((double)frameWidth / ctuTree[0]->cbY.blockSize);
	if (current->depth == 0)
	{
		return getTopRightChild(ctuTree[current->index + number_of_blocks_row]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];

	bool isCorneBottomInsideCTU = isCornerBottomInCTU(current, frameWidth);

	getCUPath(current, indexes, currentCuDepth);
	CU* bBlock;

	if (!isCorneBottomInsideCTU)
	{
		bBlock = getBottomRightInSameCTU(current, indexes);
	}
	else
	{
		CU* parent = getParentCTU(current);
		number_of_blocks_row = (int)ceil((double)frameWidth / parent->cbY.blockSize);
		CU* ctu_root = ctuTree[parent->index + number_of_blocks_row];
		bBlock = getBottomRightNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return bBlock;
}

/*-----------------------------------------------------------------------------
Function name: getRightBottomCodingUnit
Description: gets right, but bottom CU of current CU in frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which right, but bottom neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

+-----------+
|           |
|           |
|  current  |
|			+---+
|			| X |
+-----------+---+

-----------------------------------------------------------------------------*/
CU* BlockPartition::getRightBottomCodingUnit(CU** ctuTree, CU* current, bool& isAvailable, int frameWidth)
{


	bool isCornerRightInFrame = isCornerRight(current->cbY.startingIndex, frameWidth, current->cbY.blockSize);

	if (isCornerRightInFrame)
	{
		isAvailable = false;
		return current;
	}

	isAvailable = true;

	if (current->depth == 0)
	{
		return getBottomLeftChild(ctuTree[current->index + 1]);
	}

	int currentCuDepth = current->depth;
	//int indexes[5];
	int* indexes = new int[currentCuDepth];

	bool isCornerRightInsideCTU = isCornerRightInCTU(current, frameWidth);

	getCUPath(current, indexes, currentCuDepth);
	CU* rBlock;

	if (!isCornerRightInsideCTU)
	{
		rBlock = getRightBottomInSameCTU(current, indexes);
	}
	else
	{
		CU* ctu_root = ctuTree[getParentCTU(current)->index + 1];
		rBlock = getRightBottomNeighbourFromRoot(ctu_root, indexes, currentCuDepth);
	}

	delete[] indexes;

	return rBlock;
}

/*-----------------------------------------------------------------------------
Function name: getRefCenterCodingUnit
Description: gets center coding unit from reference frame (see sketch)
Parameters:
-	ctuTree - CTU Tree of entire frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has above neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

+-----------+
|  current  |
|           |
|     +---+ |
|	  | X |	|
|	  +---+ |
+-----------+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRefCenterCodingUnit(CU** refCtuTree, CU* current)
{
	int indexesSize = current->depth + 1;
	//int indexes[5];
	int* indexes = new int[indexesSize];

	getCUPathWithCTU(current, indexes, current->depth);

	int index = indexesSize - 1;
	CU* centerCU = refCtuTree[indexes[index]];
	bool found = false;

	//Folloe the same path in refFrame 
	while (index != 0)
	{
		if (centerCU->hasChildren)
		{
			index--;
			centerCU = centerCU->children[indexes[index]];
		}
		else
		{
			//THis means that in ref CU is bigger 
			found = true;
			break;
		}
	}

	delete[] indexes;

	if (found || !centerCU->hasChildren)
	{
		//if CU is the same or bigger
		return centerCU;
	}
	else
	{
		//if CU is the smaller first get the center block (bottom-right)
		centerCU = centerCU->children[3];

		//of that child has further children then center block is the one on top left 
		if (centerCU->hasChildren)
			return getTopLeftChild(centerCU);
		else
			return centerCU;
	}
}

/*-----------------------------------------------------------------------------
Function name: getRefRightDownCodingUnitByIndex
Description: gets right, but under CU of current CU in reference frame (see sketch)
Parameters:
-	refCtuTree - CTU Tree of entire reference frame
-	current - current CU for which above neighbour is fetched
-	isAvailable - out parameter showing if current CU has right neihgbour
-	frameWidth
Returns: above CU and isAvailable flag

Author: Piljic
Current version: v0.1
Comments: Two this are sepratated here, is above CU  in same CTU as current CU or it is contained in different (above) CTU

-- searching for X --

+-----------+
|           |
|           |
|  current  |
|	(ref.)	|
|			|
+-----------+---+
			| X |
			+---+
-----------------------------------------------------------------------------*/
CU* BlockPartition::getRefRightDownCodingUnitByIndex(CU** refCtuTree, CU* current, bool& isAvailable, int frameWidth, int frameHeight)
{
	bool isCornerRightInFrame = isCornerRight(current->cbY.startingIndexInFrame, frameWidth, current->cbY.blockSize);
	bool isCornerBottomInFrame = isCornerBottom(current->cbY.startingIndexInFrame, current->cbY.blockSize, frameWidth, frameHeight);

	if (isCornerBottomInFrame || isCornerRightInFrame)
	{
		isAvailable = false;
		return current;
	}
	else
	{
		CU* originalParent = getParentCTU(current);
		int pixelIndex = current->cbY.startingIndexInFrame + (current->cbY.blockSize * frameWidth) + current->cbY.blockSize;
		CU* refRightDown = getCodingUnitByLumaPixelIndex(refCtuTree, pixelIndex, isAvailable, originalParent->cbY.blockSize, frameWidth, frameHeight);

		//There is limitation 
		//If refRightDown's CTU is in the same row as CTU of original CU it is considered as unavailable
		CU* refParent = getParentCTU(refRightDown);
		if (refParent->index - originalParent->index > 1)
		{
			isAvailable = false;
			return current;
		}
		else
		{
			isAvailable = true;
			return refRightDown;
		}
	}
}


/*-----------------------------------------------------------------------------
Function name: assignAllNeighbours
Description: assigns all left and above CU's for current CU and its children
Parameters:
	-	ctuTree - CTU Tree of entire frame
	-	current - current CU for which action is done
	-	frameWidth
Returns: just fills CU data

Author: Piljic
-----------------------------------------------------------------------------*/
void BlockPartition::assignAllNeighbours(CU** ctuTree, CU* current, int frameWidth)
{

	//TODO Check if it is neccessary to do this for all CU's
	//This was implemented when we had all INTRA 
	//for example, if CU is Inter predicted leftCu and aboveCu are not important 

	/*bool leftIsAvailable;
	bool aboveIsAvailable;
*/
	current->isLeftCodingUnitAvailable(frameWidth);
	current->isAboveCodingUnitAvailable(frameWidth);
	//current->leftCU = getLeftCodingUnit(ctuTree, current, current->isLeftAvailable, frameWidth);
	//current->aboveCU = getAboveCodingUnit(ctuTree, current, current->isAboveAvailable, frameWidth);

	if (current->hasChildren)
	{
		if (current->children[0]->isInFrame)
			assignAllNeighbours(ctuTree, current->children[0], frameWidth);

		if (current->children[1]->isInFrame)
			assignAllNeighbours(ctuTree, current->children[1], frameWidth);

		if (current->children[2]->isInFrame)
			assignAllNeighbours(ctuTree, current->children[2], frameWidth);

		if (current->children[3]->isInFrame)
			assignAllNeighbours(ctuTree, current->children[3], frameWidth);
	}
}

#pragma endregion


BlockPartition::~BlockPartition()
{
}


void BlockPartition::get1dIntBlockByStartingIndex(char* data, int16_t* resultBlock, int startingIndex, int blockSize, int frameWidth, int frameHeight)
{
	int v_starting_index = frameWidth * frameHeight + (frameWidth * frameHeight / 4);
	int u_starting_index = frameWidth * frameHeight;

	int wid, hei, row, column;
	if (startingIndex >= v_starting_index)
	{
		wid = frameWidth / 2;
		hei = frameHeight / 2;
		row = ((startingIndex - v_starting_index) / wid) / blockSize;
		column = ((startingIndex - v_starting_index) % wid) / blockSize;
	}
	else if (startingIndex >= u_starting_index)
	{
		wid = frameWidth / 2;
		hei = frameHeight / 2;
		row = ((startingIndex - u_starting_index) / wid) / blockSize;
		column = ((startingIndex - u_starting_index) % wid) / blockSize;
	}
	else
	{
		wid = frameWidth;
		hei = frameHeight;
		row = (startingIndex / wid) / blockSize;
		column = (startingIndex % wid) / blockSize;
	}


	int x_coordinate = blockSize * row;
	int y_coordinate = column * blockSize;

	int data_index = startingIndex;

	for (int i = 0; i < blockSize; i++)
	{
		for (int j = 0; j < blockSize; j++)
		{
			int index = data_index + wid * i + j;
			if (x_coordinate + i >= hei || y_coordinate + j >= wid)
			{
				//Block out of range 
				//TODO what happens when block is out of range ? 
				resultBlock[i * blockSize + j] = 0;
			}
			else
			{
				resultBlock[i * blockSize + j] = data[index];
			}
		}
	}

}

void BlockPartition::get1dInt8BlockByStartingIndex(int8_t* data, int8_t* resultBlock, int startingIndex, int blockSize, int frameWidth, int frameHeight)
{

	int wid, hei, row, column;
	wid = frameWidth;
	hei = frameHeight;
	row = (startingIndex / wid) / blockSize;
	column = (startingIndex % wid) / blockSize;

	int x_coordinate = blockSize * row;
	int y_coordinate = column * blockSize;

	int data_index = startingIndex;

	for (int i = 0; i < blockSize; i++)
	{
		for (int j = 0; j < blockSize; j++)
		{
			int index = data_index + wid * i + j;
			if (x_coordinate + i >= hei || y_coordinate + j >= wid)
			{
				//Block out of range 
				//TODO what happens when block is out of range ? 
				resultBlock[i * blockSize + j] = 0;
			}
			else
			{
				resultBlock[i * blockSize + j] = data[index];
			}
		}
	}
}

void BlockPartition::get1dInt16BlockByStartingIndex(int16_t* data, int16_t* resultBlock, int startingIndex, int blockSize, int frameWidth, int frameHeight)
{

	int wid, hei, row, column;
	wid = frameWidth;
	hei = frameHeight;
	row = (startingIndex / wid) / blockSize;
	column = (startingIndex % wid) / blockSize;

	int x_coordinate = blockSize * row;
	int y_coordinate = column * blockSize;

	int data_index = startingIndex;

	for (int i = 0; i < blockSize; i++)
	{
		for (int j = 0; j < blockSize; j++)
		{
			int index = data_index + wid * i + j;
			if (x_coordinate + i >= hei || y_coordinate + j >= wid)
			{
				//Block out of range 
				//TODO what happens when block is out of range ? 
				resultBlock[i * blockSize + j] = 0;
			}
			else
			{
				resultBlock[i * blockSize + j] = data[index];
			}
		}
	}
}

/*-----------------------------------------------------------------------------
Function name: getCodingUnitByLumaPixelIndex
Description: finds in which CU pixel is located in
Parameters:
-	ctuTree - CTU Tree of entire reference frame
-	pixelIndex
-	isAvailable
-	frameWidth
-	frameHeight
Returns: CU in which pixel is located

Author: Piljic
Current version: v0.1
Comments: This method does not check if CU is out of picture boundaries (for example if we are looking for right block of most right CU)
This sholud be done on level above this
Only check is if index is larger that width*height

-----------------------------------------------------------------------------*/
CU* BlockPartition::getCodingUnitByLumaPixelIndex(CU** ctuTree, int pixelIndex, bool& isAvailable, int ctuSize, int frameWidth, int frameHeight)
{
	if (pixelIndex >= frameWidth * frameHeight)
	{
		isAvailable = false;
		return nullptr;
	}
	int blocksInRow = (int)ceil((double)frameWidth / ctuSize);
	int blocksInColumn = (int)ceil((double)frameHeight / ctuSize);

	//First find CTU where pixel belongs (since it is only thing in common for all frames)
	int ctu_column = (pixelIndex % frameWidth) / ctuSize;
	int ctu_row = (pixelIndex / frameWidth) / ctuSize;

	int ctu_index = ctu_row * blocksInRow + ctu_column;

	return getCodingUnitInCUByLumaPixelIndex(ctuTree[ctu_index], pixelIndex, frameWidth);
}

PU* BlockPartition::getPredictionUnitByLumaPixelIndex(CU** ctuTree, int pixelIndex, bool& isAvailable, int ctuSize, int frameWidth, int frameHeight)
{
	CU* cu = getCodingUnitByLumaPixelIndex(ctuTree, pixelIndex, isAvailable, ctuSize, frameWidth, frameHeight);

	if (cu == nullptr || cu->numOfPUs == 0 || cu->CuPredMode != MODE_INTER)
		return nullptr;

	//if there is no split to prediction units
	if (cu->numOfPUs == 1)
		return &cu->predictionUnits[0];

	int widthInCU = pixelIndex % frameWidth - cu->cbY.startingIndex % frameWidth;
	int heightInCu = (int)(pixelIndex / frameWidth) - (int)(cu->cbY.startingIndex / frameWidth);

	int indexVertical = widthInCU < cu->predictionUnits[0].pbY.width ? 0 : 1;
	int indexHorizontal = heightInCu < cu->predictionUnits[0].pbY.height ? 0 : 1;


	//vertical PU split
	if (cu->PartMode == PartitionModeInter::PART_Nx2N || cu->PartMode == PartitionModeInter::PART_nRx2N || cu->PartMode == PartitionModeInter::PART_nLx2N)
	{
		return &cu->predictionUnits[indexVertical];
	}
	//horizontal PU split
	else if (cu->PartMode == PartitionModeInter::PART_2NxN || cu->PartMode == PartitionModeInter::PART_2NxnD || cu->PartMode == PartitionModeInter::PART_2NxnU)
	{
		return &cu->predictionUnits[indexHorizontal];
	}
	//Split to 4 PUs
	else
	{
		return &cu->predictionUnits[indexVertical * 2 + indexHorizontal];
	}
}

TU* BlockPartition::getTransformUnitByLumaPixelIndex(CU** ctuTree, int pixelIndex, bool& isAvailable, int ctuSize, int frameWidth, int frameHeight)
{
	CU* cu = getCodingUnitByLumaPixelIndex(ctuTree, pixelIndex, isAvailable, ctuSize, frameWidth, frameHeight);

	if (cu == nullptr || !cu->hasTransformTree)
		return nullptr;

	if (!cu->transformTree->splitTransformFlag)
	{
		return cu->transformTree;
	}
	else
	{
		int widthInCU = pixelIndex % frameWidth - cu->cbY.startingIndex % frameWidth;
		int heightInCu = (int)(pixelIndex / frameWidth) - (int)(cu->cbY.startingIndex / frameWidth);

		int indexVertical = widthInCU < cu->transformTree->tbY.transformBlockSize / 2 ? 0 : 1;
		int indexHorizontal = heightInCu < cu->transformTree->tbY.transformBlockSize / 2 ? 0 : 1;

		return cu->transformTree->children[indexVertical + 2 * indexHorizontal];
	}


}

CU* BlockPartition::getCodingUnitInCUByLumaPixelIndex(CU* parent, int pixelIndex, int frameWidth)
{
	if (parent && parent->hasChildren)
	{
		int child_column = ((pixelIndex % frameWidth) - (parent->cbY.startingIndex % frameWidth)) / (parent->cbY.blockSize / 2);
		int child_row = ((pixelIndex / frameWidth) - (parent->cbY.startingIndex / frameWidth)) / (parent->cbY.blockSize / 2);

		return getCodingUnitInCUByLumaPixelIndex(parent->children[child_row * 2 + child_column], pixelIndex, frameWidth);

	}
	else
	{
		return parent;
	}
}

void BlockPartition::getCuPuTuByLumaPixelIndex(Partition* partition, int pixelIndex, int ctbSize, CU*& cu, PU*& pu, TU*& tu)
{
	bool isAvailable;

	cu = getCodingUnitByLumaPixelIndex(partition->ctuTree, pixelIndex, isAvailable, ctbSize, partition->width, partition->height);

	if (cu == nullptr)
		return;

	//find prediction unit
	if (cu->numOfPUs > 0 && cu->CuPredMode == MODE_INTER)
	{
		//if there is no split to prediction units
		if (cu->numOfPUs == 1)
			pu = &cu->predictionUnits[0];

		int widthInCU = pixelIndex % partition->width - cu->cbY.startingIndex % partition->width;
		int heightInCu = (int)(pixelIndex / partition->width) - (int)(cu->cbY.startingIndex / partition->width);

		int indexVertical = widthInCU < cu->predictionUnits[0].pbY.width ? 0 : 1;
		int indexHorizontal = heightInCu < cu->predictionUnits[0].pbY.height ? 0 : 1;


		//vertical PU split
		if (cu->PartMode == PartitionModeInter::PART_Nx2N || cu->PartMode == PartitionModeInter::PART_nRx2N || cu->PartMode == PartitionModeInter::PART_nLx2N)
		{
			pu = &cu->predictionUnits[indexVertical];
		}
		//horizontal PU split
		else if (cu->PartMode == PartitionModeInter::PART_2NxN || cu->PartMode == PartitionModeInter::PART_2NxnD || cu->PartMode == PartitionModeInter::PART_2NxnU)
		{
			pu = &cu->predictionUnits[indexHorizontal];
		}
		//Split to 4 PUs
		else
		{
			pu = &cu->predictionUnits[indexVertical * 2 + indexHorizontal];
		}
	}


	//find transform unit
	if (cu->hasTransformTree)
	{
		if (!cu->transformTree->splitTransformFlag)
		{
			tu = cu->transformTree;
		}
		else
		{
			int widthInCU = pixelIndex % partition->width - cu->cbY.startingIndex % partition->width;
			int heightInCu = (int)(pixelIndex / partition->width) - (int)(cu->cbY.startingIndex / partition->width);

			int indexVertical = widthInCU < cu->transformTree->tbY.transformBlockSize / 2 ? 0 : 1;
			int indexHorizontal = heightInCu < cu->transformTree->tbY.transformBlockSize / 2 ? 0 : 1;

			tu = cu->transformTree->children[indexVertical + 2 * indexHorizontal];
		}
	}


}
