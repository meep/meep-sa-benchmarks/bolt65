/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "TransformAndQuantizationCreator.h"

TransformAndQuantizationCreator::TransformAndQuantizationCreator()
{

}

TransformAndQuantizationCreator::~TransformAndQuantizationCreator()
{

}

ITransformAndQuantization* TransformAndQuantizationCreator::Create(EncodingContext *enCtx)
{
#ifdef __MEEP
	if (enCtx->MEEP)
	{
		return new TransformAndQuantizationMEEP();
	}
	else
	{
		return new TransformAndQuantizationScalar();
	}
#endif
#ifdef __EPI_RISCV
	if (enCtx->EPI_RISCV)
	{
		return new TransformAndQuantizationEPIRISCV();
	}
	else if(enCtx->EPI_RISCV_AUTO)
	{
		return new TransformAndQuantizationRISCVAuto();
	}
	else
	{
		return new TransformAndQuantizationScalar();
	}
#endif
#ifdef __AVX2__
	if (enCtx->AVX)
	{
		return new TransformAndQuantizationAVX();
	}
	else
	{
		return new TransformAndQuantizationScalar();
	}
#elif defined(__ARM_NEON) || defined(__ARM_FEATURE_SVE)
	//SVE has higher priority
	if (enCtx->SVE)
	{
#ifdef __ARM_FEATURE_SVE
		return new TransformAndQuantizationSVE();
#else
		return new TransformAndQuantizationScalar();
#endif
	}
	else if (enCtx->NEON)
	{
#ifdef __ARM_NEON
		return new TransformAndQuantizationNEON();
#else
		return new TransformAndQuantizationScalar();
#endif
	}
	else
	{
		return new TransformAndQuantizationScalar();
	}
#else
	return new TransformAndQuantizationScalar();
#endif 

}