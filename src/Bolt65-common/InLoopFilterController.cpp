/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK,
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "InLoopFilterController.h"


InLoopFilterController::InLoopFilterController()
{
}

InLoopFilterController::~InLoopFilterController()
{
}

void InLoopFilterController::Init(EncodingContext* _enCtx)
{
	enCtx = _enCtx;

}

void InLoopFilterController::PerformDeblockingFilter(Partition* partition)
{
	if (!enCtx->disableDeblockingFilter)
	{
		PerformVerticalDeblockingFilterForPartition(partition);
		PerformHorizontalDeblockingFilterForPartition(partition);
	}
}

void InLoopFilterController::PerformVerticalDeblockingFilterForPartition(Partition* partition)
{
	int i, j;
	int lumaSize = partition->width * partition->height;
	int chromaSize = lumaSize / 4;

	//split partition to 8x8 grid and check every boundary 
	for (i = 0; i < partition->height; i = i + 8)
	{
		for (j = 0; j < partition->width; j = j + 8)
		{
			int leftCuBlockIndex = i * partition->width + j;
			int rightCuBlockIndex = i * partition->width + 8 + j;

			bool needsDeblockingFilter = false;
			bool needsSplitCheck = false;
			int boundariesStrenght = -1;

			CheckVerticalBoundary(partition, leftCuBlockIndex, rightCuBlockIndex, needsDeblockingFilter, needsSplitCheck, boundariesStrenght, 8);

			if (needsSplitCheck)
			{
				//we need to check for each of the 4x4 blocks in 8x8 block separately and since we are talking about vertical boundaries, we have to check two rightmost segments
				int leftCuBlockIndexPart1 = leftCuBlockIndex + 4;
				int rightCuBlockIndexPart1 = rightCuBlockIndex;

				int leftCuBlockIndexPart2 = leftCuBlockIndex + 4 + partition->width * 4;
				int rightCuBlockIndexPart2 = rightCuBlockIndex + partition->width * 4;

				CheckVerticalBoundary(partition, leftCuBlockIndexPart1, rightCuBlockIndexPart1, needsDeblockingFilter, needsSplitCheck, boundariesStrenght, 4);
				if (needsDeblockingFilter)
				{
					if (boundariesStrenght > 0)
					{
						int leftBlockFrameIndex1 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, leftCuBlockIndexPart1);
						int rightBlockFrameIndex1 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, rightCuBlockIndexPart1);
						PerformDeblockingFilterForBlock(partition, leftBlockFrameIndex1, rightBlockFrameIndex1, VERTICAL_DEB, 4, Luma);
					}
				}

				CheckVerticalBoundary(partition, leftCuBlockIndexPart2, rightCuBlockIndexPart2, needsDeblockingFilter, needsSplitCheck, boundariesStrenght, 4);
				if (needsDeblockingFilter)
				{
					if (boundariesStrenght > 0)
					{
						int leftBlockFrameIndex2 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, leftCuBlockIndexPart2);
						int rightBlockFrameIndex2 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, rightCuBlockIndexPart2);
						PerformDeblockingFilterForBlock(partition, leftBlockFrameIndex2, rightBlockFrameIndex2, VERTICAL_DEB, 4, Luma);
					}
				}
			}
			else
			{
				if (needsDeblockingFilter)
				{
					//filter chroma
					if (boundariesStrenght > 1)
					{

						//since we check only 8x8 grid at chroma only, it is 16x16 in luma samples
						if (j % 16 == 8)
						{
							int uStartingIndexLeft = lumaSize + (i / 2 * partition->width / 2) + j / 2;
							int uStartingIndexRight = uStartingIndexLeft + 4;

							int vStartingIndexLeft = lumaSize + chromaSize + (i / 2 * partition->width / 2) + j / 2;
							int vStartingIndexRight = vStartingIndexLeft + 4;

							int leftBlockFrameIndexU = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, uStartingIndexLeft);
							int rightBlockFrameIndexU = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, uStartingIndexRight);

							int leftBlockFrameIndexV = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, vStartingIndexLeft);
							int rightBlockFrameIndexV = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, vStartingIndexRight);

							PerformDeblockingFilterForBlock(partition, leftBlockFrameIndexU, rightBlockFrameIndexU, VERTICAL_DEB, 4, ChromaCb);
							PerformDeblockingFilterForBlock(partition, leftBlockFrameIndexV, rightBlockFrameIndexV, VERTICAL_DEB, 4, ChromaCr);
						}
					}
					//filter luma 
					if (boundariesStrenght > 0)
					{
						int leftBlockFrameIndex = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, leftCuBlockIndex);
						int rightBlockFrameIndex = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, rightCuBlockIndex);
						PerformDeblockingFilterForBlock(partition, leftBlockFrameIndex, rightBlockFrameIndex, VERTICAL_DEB, 8, Luma);
					}
				}
			}
		}

	}
}

void InLoopFilterController::PerformHorizontalDeblockingFilterForPartition(Partition* partition)
{
	int i, j;
	int lumaSize = partition->width * partition->height;
	int chromaSize = lumaSize / 4;

	//split partition to 8x8 grid and check every boundary 
	for (i = 0; i < partition->height; i = i + 8)
	{
		for (j = 0; j < partition->width; j = j + 8)
		{
			int upCuBlockIndex = i * partition->width + j;
			int downCuBlockIndex = (i + 8) * partition->width + j;

			bool needsDeblockingFilter = false;
			bool needsSplitCheck = false;
			int boundariesStrenght = -1;

			CheckHorizontalBoundary(partition, upCuBlockIndex, downCuBlockIndex, needsDeblockingFilter, needsSplitCheck, boundariesStrenght, 8);

			if (needsSplitCheck)
			{
				//we need to check for each of the 4x4 blocks in 8x8 block separately and since we are talking about horizontal boundaries, we have to check two downmost segments
				int upCuBlockIndexpart1 = upCuBlockIndex + partition->width * 4;
				int downCuBlockIndexPart1 = downCuBlockIndex;

				int upCuBlockIndexPart2 = upCuBlockIndexpart1 + 4;
				int downCuBlockIndexPart2 = downCuBlockIndex + 4;

				CheckHorizontalBoundary(partition, upCuBlockIndexpart1, downCuBlockIndexPart1, needsDeblockingFilter, needsSplitCheck, boundariesStrenght, 4);
				if (needsDeblockingFilter)
				{
					if (boundariesStrenght > 0)
					{
						int upBlockFrameIndex1 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, upCuBlockIndexpart1);
						int downBlockFrameIndex1 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, downCuBlockIndexPart1);
						PerformDeblockingFilterForBlock(partition, upBlockFrameIndex1, downBlockFrameIndex1, HORIZONTAL_DEB, 4, Luma);
					}
				}

				CheckHorizontalBoundary(partition, upCuBlockIndexPart2, downCuBlockIndexPart2, needsDeblockingFilter, needsSplitCheck, boundariesStrenght, 4);
				if (needsDeblockingFilter)
				{
					if (boundariesStrenght > 0)
					{
						int upBlockFrameIndex2 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, upCuBlockIndexPart2);
						int downBlockFrameIndex2 = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, downCuBlockIndexPart2);
						PerformDeblockingFilterForBlock(partition, upBlockFrameIndex2, downBlockFrameIndex2, HORIZONTAL_DEB, 4, Luma);
					}
				}
			}
			else
			{
				if (needsDeblockingFilter)
				{
					//filter chroma
					if (boundariesStrenght > 1)
					{
						//since we check only 8x8 grid at chroma only, it is 16x16 in luma samples
						if (i % 16 == 8)
						{
							int uStartingIndexUp = lumaSize + (i / 2 * partition->width / 2) + j / 2;
							int uStartingIndexDown = uStartingIndexUp + 4 * partition->width / 2;

							int vStartingIndexUp = lumaSize + chromaSize + (i / 2 * partition->width / 2) + j / 2;
							int vStartingIndexDown = vStartingIndexUp + 4 * partition->width / 2;;

							int upBlockFrameIndexU = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, uStartingIndexUp);
							int downBlockFrameIndexU = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, uStartingIndexDown);

							int upBlockFrameIndexV = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, vStartingIndexUp);
							int downBlockFrameIndexV = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, vStartingIndexDown);

							PerformDeblockingFilterForBlock(partition, upBlockFrameIndexU, downBlockFrameIndexU, HORIZONTAL_DEB, 4, ChromaCb);
							PerformDeblockingFilterForBlock(partition, upBlockFrameIndexV, downBlockFrameIndexV, HORIZONTAL_DEB, 4, ChromaCr);
						}
					}
					//filter luma 
					if (boundariesStrenght > 0)
					{
						int upBlockFrameIndex = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, upCuBlockIndex);
						int downBlockFrameIndex = ComUtil::getFrameIndexByPartitionIndex(partition->width, partition->height, partition->startingIndexInFrame, partition->partitionType, enCtx, downCuBlockIndex);
						PerformDeblockingFilterForBlock(partition, upBlockFrameIndex, downBlockFrameIndex, HORIZONTAL_DEB, 8, Luma);
					}
				}
			}
		}

	}
}

void InLoopFilterController::PerformDeblockingFilterForBlock(Partition* partition, int startingIndexFirst, int startingIndexSecond, DeblockinFilterType type, int blockSize, ColorIdx colorIdx)
{
	//Block size is alwasy 8, since we check the frame per 8 block basis
	unsigned char* blockP = new unsigned char[blockSize * blockSize];
	unsigned char* blockQ = new unsigned char[blockSize * blockSize];

	BlockPartition::get1dBlockByStartingIndex((char*)partition->reconstructed_payload, blockP, startingIndexFirst, blockSize, blockSize, enCtx->width, enCtx->height);
	BlockPartition::get1dBlockByStartingIndex((char*)partition->reconstructed_payload, blockQ, startingIndexSecond, blockSize, blockSize, enCtx->width, enCtx->height);

	if (colorIdx == Luma)
	{
		DeblockingFilter::DeblockingFilterBlock(blockP, blockQ, blockSize, enCtx->quantizationParameter, type);
	}
	else
	{
		DeblockingFilter::DeblockingFilterBlockChroma(blockP, blockQ, blockSize, enCtx->quantizationParameter, type);
	}


	BlockPartition::fill1dBlockByStartingIndex(blockP, startingIndexFirst, blockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);
	BlockPartition::fill1dBlockByStartingIndex(blockQ, startingIndexSecond, blockSize, partition->reconstructed_payload, partition->isReconstructed, enCtx->width, enCtx->height);

	delete[] blockP;
	delete[] blockQ;
}



void InLoopFilterController::CheckVerticalBoundary(Partition* partition, int leftIndex, int rightIndex, bool& needsDeblockingFilter, bool& needsSplitCheck, int& Bs, int blockSize)
{
	CU* leftCu = nullptr;
	CU* rightCu = nullptr;
	PU* leftPu = nullptr;
	PU* rightPu = nullptr;
	TU* leftTu = nullptr;
	TU* rightTu = nullptr;

	BlockPartition::getCuPuTuByLumaPixelIndex(partition, leftIndex + blockSize - 1, enCtx->ctbSize, leftCu, leftPu, leftTu);
	BlockPartition::getCuPuTuByLumaPixelIndex(partition, rightIndex, enCtx->ctbSize, rightCu, rightPu, rightTu);

	//In some cases there is possibility that there is a different BS in different segments
	//i.e. the PU or TU boundary is within 8x8 block, so one part od the boundary can have different BS than another
	if (blockSize == 8)
	{
		if (
			(leftTu != nullptr && leftTu->tbY.transformBlockSize == 4) ||
			(rightTu != nullptr && rightTu->tbY.transformBlockSize == 4) ||
			(leftPu != nullptr && (leftPu->pbY.height == 4 || (leftPu->pbY.height == 12 && leftIndex / partition->width != leftCu->cbY.startingIndex / partition->width))) ||
			(rightPu != nullptr && (rightPu->pbY.height == 4 || (rightPu->pbY.height == 12 && rightIndex / partition->width != rightCu->cbY.startingIndex / partition->width)))
			)
		{
			//if one of the Cu is INTRA then BS has to be 2 at whole boundary
			if (leftCu != nullptr && leftCu->CuPredMode != MODE_INTRA && rightCu != nullptr && rightCu->CuPredMode != MODE_INTRA)
			{
				needsSplitCheck = true;
				return;
			}
		}
	}

	//is everything valid
	if (leftCu != nullptr && rightCu != nullptr && rightIndex % partition->width != 0)
	{
		// If two 8x8 blocks do not belong in same CU
		if (leftCu != rightCu)
		{
			needsDeblockingFilter = true;
		}
		//if they belong in same CU, but different PU 
		else if (leftCu->numOfPUs > 1)
		{
			//if vertical partitioning
			if (leftPu != rightPu)
				needsDeblockingFilter = true;
		}

		//if they belong to same CU and PU, check if they belong to same TU
		//TODO: for now it checks only if TU is split once
		if (leftCu->hasTransformTree && leftCu->transformTree->splitTransformFlag)
		{
			if (leftTu != rightTu)
				needsDeblockingFilter = true;
		}

	}

	if (needsDeblockingFilter)
		CalculateBoundaryStrength(leftCu, rightCu, leftPu, rightPu, leftTu, rightTu, Bs);

}

void InLoopFilterController::CheckHorizontalBoundary(Partition* partition, int upIndex, int downIndex, bool& needsDeblockingFilter, bool& needsSplitCheck, int& Bs, int blockSize)
{

	CU* upCu = nullptr;
	CU* downCu = nullptr;
	PU* upPu = nullptr;
	PU* downPu = nullptr;
	TU* upTu = nullptr;
	TU* downTu = nullptr;

	BlockPartition::getCuPuTuByLumaPixelIndex(partition, upIndex + (blockSize - 1) * partition->width, enCtx->ctbSize, upCu, upPu, upTu);
	BlockPartition::getCuPuTuByLumaPixelIndex(partition, downIndex, enCtx->ctbSize, downCu, downPu, downTu);

	if (blockSize == 8)
	{
		if (
			(upTu != nullptr && upTu->tbY.transformBlockSize == 4) ||
			(downTu != nullptr && downTu->tbY.transformBlockSize == 4) ||
			(upPu != nullptr && (upPu->pbY.width == 4 || (upPu->pbY.width == 12 && upIndex % partition->width != upCu->cbY.startingIndex % partition->width))) ||
			(downPu != nullptr && (downPu->pbY.width == 4 || (downPu->pbY.width == 12 && downIndex % partition->width != downCu->cbY.startingIndex % partition->width)))
			)
		{
			if (upCu != nullptr && upCu->CuPredMode != MODE_INTRA && downCu != nullptr && downCu->CuPredMode != MODE_INTRA)
			{
				needsSplitCheck = true;
				return;
			}
		}
	}


	//is everything valid
	if (upCu != nullptr && downCu != nullptr && downIndex % partition->height != partition->height - 7)
	{
		// If two 8x8 blocks do not belong in same CU
		if (upCu != downCu)
		{
			needsDeblockingFilter = true;
		}
		//if they belong in same CU, but different PU 
		else if (upCu->numOfPUs > 1)
		{
			if (upPu != downPu)
				needsDeblockingFilter = true;
		}

		//if they belong to same CU and PU, check if they belong to same TU
		//TODO: for now it checks only if TU is split once
		if (upCu->hasTransformTree && upCu->transformTree->splitTransformFlag)
		{
			if (upTu != downTu)
				needsDeblockingFilter = true;
		}
	}

	if (needsDeblockingFilter)
		CalculateBoundaryStrength(upCu, downCu, upPu, downPu, upTu, downTu, Bs);
}

void InLoopFilterController::CalculateBoundaryStrength(CU* cuP, CU* cuQ, PU* puP, PU* puQ, TU* tuP, TU* tuQ, int& bs)
{
	//P or Q is intra
	if (cuP->CuPredMode == MODE_INTRA || cuQ->CuPredMode == MODE_INTRA)
		bs = 2;
	//P or Q has non-zero coefficients
	else if ((!tuP->tbY.isZeroMatrix || !tuQ->tbY.isZeroMatrix))
		bs = 1;
	//P and Q use different ref. pictures
	else if (puP->MVt1 != puQ->MVt1)
		bs = 1;
	//Abs diff. between P and Q's MV's is >=1
	else if (abs(puP->MVx1 - puQ->MVx1) + abs(puP->MVy1 - puQ->MVy1) >= 1)
		bs = 1;
	else
		bs = 0;
}