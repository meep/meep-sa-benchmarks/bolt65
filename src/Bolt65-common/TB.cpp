/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "TB.h"

TB::TB()
{
	this->QTResidual = nullptr;
	this->residual = nullptr;
	this->subBlocks = nullptr;
	this->isZeroMatrix = true;
	this->numOfSubblocks = -1;
}

TB::TB(int startingTransformIndex, int blockSize)
{
	this->startingIndexInFrame = startingTransformIndex;
	this->transformBlockSize = blockSize;
	this->QTResidual = nullptr;
	this->residual = nullptr;
	this->subBlocks = nullptr;
	this->isZeroMatrix = true;
	this->numOfSubblocks = -1;
}

TB::TB(int startingTransformIndex, int blockSize, int colorIdx)
{
	this->colorIdx = colorIdx;
	this->startingIndexInFrame = startingTransformIndex;
	this->transformBlockSize = blockSize;
	this->QTResidual = nullptr;
	this->residual = nullptr;
	this->subBlocks = nullptr;
	this->isZeroMatrix = true;
	this->numOfSubblocks = -1;
}

TB::TB(int startingIndex, int startingIndexFrame, int blockSize, int colorIdx)
{
	this->colorIdx = colorIdx;
	this->startingIndexInFrame = startingIndexFrame;
	this->startingIndex = startingIndex;
	this->transformBlockSize = blockSize;
	this->QTResidual = nullptr;
	this->residual = nullptr;
	this->subBlocks = nullptr;
	this->isZeroMatrix = true;
	this->numOfSubblocks = -1;
}


TB::~TB()
{
	if (QTResidual != nullptr)
		delete[] QTResidual;

	if (residual != nullptr)
		delete[] residual;

	if (subBlocks != nullptr)
		delete[] subBlocks;
}
