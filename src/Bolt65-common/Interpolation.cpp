/*
© FER, HPC Architecture and Application Research Center, All rights reserved

Use under License Agreement ONLY.

IF, PRIOR TO DOWNLOADING, STORING, INSTALLING, ACTIVATING OR USING THE WORK, 
(A) YOU DECIDE YOU ARE UNWILLING TO AGREE TO THE TERMS OF THE PROVIDED LICENSE AGREEMENT, or 
(B) YOU DID NOT RECEIVE OR OBTAIN THE LICENSE AGREEMENT, YOU HAVE NO RIGHT TO USE THE WORK AND YOU SHOULD PROMPTLY RETURN THE WORK TO FER, DELETE IT, OR DISABLE IT.
*/

#include "Interpolation.h"



Interpolation::Interpolation()
{

}

void Interpolation::Interpolate(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int cIdx, int absdX, int absdY, bool isdXNeg, bool isdYNeg)
{

	// shift1 Min(4, BitDepthY - 8)
	int shift1 = 0;
	int shift2 = 6;
	int shift3 = 12;

	int shift1Offset = 0;
	int shift2Offset = 32;
	int shift3Offset = 2048;

	int interpolatedArrayTmp = -1;
	int interpolatedArrayIntermediate[64 * (64 + 7)]{ 0 };

	if (cIdx == 0)
	{
		int j = startingIndex % frameWidth;
		int i = startingIndex / frameWidth;

		int intdX = absdX >> 2;
		int intdY = absdY >> 2;

		int fracdX = absdX & 3;
		int fracdY = absdY & 3;

		int row, column;
		if (isdXNeg)
		{
			column = j - intdX;

			if (fracdX != 0)
				column--;
			//intdX--;
			fracdX = (4 - fracdX) % 4;
		}
		else
		{
			column = j + intdX;
		}

		if (isdYNeg)
		{
			row = i - intdY;
			if (fracdY != 0)
				row--;
			//intdY--;
			fracdY = (4 - fracdY) % 4;
		}
		else
		{
			row = i + intdY;
		}



		if (fracdX == 0)
		{

			/*
			if (fracdY == 0)
			{
			// do nothing, no interpolation
			}
			*/

			// looking for d(i,j), only vertical movement, no need for left and right extension of pb
			if (fracdY == 1)
			{

				int currRow, currColumn, tmpRowMin3, tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;

				int beyondXNegative = 0, beyondXPositive = 0;;
				if (column < 0)
				{
					beyondXNegative = -column;
					if (beyondXNegative > nPbW)
						beyondXNegative = nPbW;
				}
				if (column + nPbW > frameWidth)
				{
					beyondXPositive = column + nPbW - frameWidth;
					if (beyondXPositive > nPbW)
						beyondXPositive = nPbW;
				}

				//This is the case when movement in X axis goes left of frame
				//if column is negative it means that integer X movement behind the frame happened !!!
				int beyondRow = row;
				if (beyondXPositive > 0)
				{
					for (int k = 0; k < nPbH; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, column);
						currRow = ComUtil::clip3(0, frameHeight - 1, beyondRow);
						tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 3);
						tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 2);
						tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 1);

						tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 3);
						tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 2);
						tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 1);


						for (int l = 0; l < beyondXPositive; l++)
						{
							interpolatedArrayTmp = ((-referencePicture[tmpRowMin3* frameWidth + frameWidth - 1] + 4 * referencePicture[tmpRowMin2 * frameWidth + frameWidth - 1] - 10 * referencePicture[tmpRowMin1 * frameWidth + frameWidth - 1] + 58 * referencePicture[currRow * frameWidth + frameWidth - 1] + 17 * referencePicture[tmpRowPlus1 * frameWidth + frameWidth - 1] - 5 * referencePicture[tmpRowPlus2 * frameWidth + frameWidth - 1] + referencePicture[tmpRowPlus3 * frameWidth + frameWidth - 1]) + shift2Offset) >> shift2;

							interpolatedArray[k*nPbW + nPbW - l - 1] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondRow;
					}
				}

				if (beyondXNegative > 0)
				{
					beyondRow = row;
					for (int k = 0; k < nPbH; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, column);
						currRow = ComUtil::clip3(0, frameHeight - 1, beyondRow);
						tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 3);
						tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 2);
						tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 1);

						tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 3);
						tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 2);
						tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 1);


						for (int l = 0; l < beyondXNegative; l++)
						{
							interpolatedArrayTmp = ((-referencePicture[tmpRowMin3* frameWidth + currColumn + 0] + 4 * referencePicture[tmpRowMin2 * frameWidth + currColumn + 0] - 10 * referencePicture[tmpRowMin1 * frameWidth + currColumn + 0] + 58 * referencePicture[currRow * frameWidth + currColumn + 0] + 17 * referencePicture[tmpRowPlus1 * frameWidth + currColumn + 0] - 5 * referencePicture[tmpRowPlus2 * frameWidth + currColumn + 0] + referencePicture[tmpRowPlus3 * frameWidth + currColumn + 0]) + shift2Offset) >> shift2;

							interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondRow;
					}
				}

				for (int k = 0; k < nPbH; k++)
				{


					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, row - 3);
					tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
					tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

					tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
					tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
					tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);


					for (int l = 0; l < nPbW - beyondXNegative - beyondXPositive; l++)
					{
						interpolatedArrayTmp = ((-referencePicture[tmpRowMin3* frameWidth + currColumn + l] + 4 * referencePicture[tmpRowMin2 * frameWidth + currColumn + l] - 10 * referencePicture[tmpRowMin1 * frameWidth + currColumn + l] + 58 * referencePicture[currRow * frameWidth + currColumn + l] + 17 * referencePicture[tmpRowPlus1 * frameWidth + currColumn + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + currColumn + l] + referencePicture[tmpRowPlus3 * frameWidth + currColumn + l]) + shift2Offset) >> shift2;

						interpolatedArray[k*nPbW + l + beyondXNegative] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++row;
				}
			}

			// looking for h(i,j), only vertical movement, no need for left and right extension of pb
			else if (fracdY == 2)
			{
				//if column is negative it means that integer X movement behind the frame happened !!!

				// shift1 Min(4, BitDepthY - 8)
				int currColumn, currRow, tmpRowMin3, tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpRowPlus4;
				int beyondXNegative = 0, beyondXPositive = 0;
				if (column < 0)
				{
					beyondXNegative = -column;
					if (beyondXNegative > nPbW)
						beyondXNegative = nPbW;
				}
				if (column + nPbW > frameWidth)
				{
					beyondXPositive = column + nPbW - frameWidth;
					if (beyondXPositive > nPbW)
						beyondXPositive = nPbW;
				}

				//This is the case when movement in X axis goes right of frame

				int beyondRow = row;
				if (beyondXPositive > 0)
				{
					for (int k = 0; k < nPbH; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, column);
						currRow = ComUtil::clip3(0, frameHeight - 1, beyondRow);
						tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 3);
						tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 2);
						tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 1);


						tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 4);
						tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 3);
						tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 2);
						tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 1);

						for (int l = 0; l < beyondXPositive; l++)
						{
							interpolatedArrayTmp = (-referencePicture[tmpRowMin3* frameWidth + frameWidth - 1] + 4 * referencePicture[tmpRowMin2 * frameWidth + frameWidth - 1] - 11 * referencePicture[tmpRowMin1 * frameWidth + frameWidth - 1] + 40 * referencePicture[currRow * frameWidth + frameWidth - 1] + 40 * referencePicture[tmpRowPlus1 * frameWidth + frameWidth - 1] - 11 * referencePicture[tmpRowPlus2 * frameWidth + frameWidth - 1] + 4 * referencePicture[tmpRowPlus3 * frameWidth + frameWidth - 1] - referencePicture[tmpRowPlus4 * frameWidth + frameWidth - 1] + shift2Offset) >> shift2;

							interpolatedArray[k*nPbW + nPbW - l - 1] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondRow;
					}
				}

				if (beyondXNegative > 0)
				{
					//This is the case when movement in X axis goes left of frame
					beyondRow = row;
					for (int k = 0; k < nPbH; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, column);
						currRow = ComUtil::clip3(0, frameHeight - 1, beyondRow);
						tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 3);
						tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 2);
						tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 1);


						tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 4);
						tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 3);
						tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 2);
						tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 1);

						for (int l = 0; l < beyondXNegative; l++)
						{
							interpolatedArrayTmp = (-referencePicture[tmpRowMin3* frameWidth + currColumn + 0] + 4 * referencePicture[tmpRowMin2 * frameWidth + currColumn + 0] - 11 * referencePicture[tmpRowMin1 * frameWidth + currColumn + 0] + 40 * referencePicture[currRow * frameWidth + currColumn + 0] + 40 * referencePicture[tmpRowPlus1 * frameWidth + currColumn + 0] - 11 * referencePicture[tmpRowPlus2 * frameWidth + currColumn + 0] + 4 * referencePicture[tmpRowPlus3 * frameWidth + currColumn + 0] - referencePicture[tmpRowPlus4 * frameWidth + currColumn + 0] + shift2Offset) >> shift2;

							interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondRow;
					}
				}

				for (int k = 0; k < nPbH; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, row - 3);
					tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
					tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);


					tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, row + 4);
					tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
					tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
					tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);

					for (int l = 0; l < nPbW - beyondXNegative - beyondXPositive; l++)
					{
						interpolatedArrayTmp = (-referencePicture[tmpRowMin3* frameWidth + currColumn + l] + 4 * referencePicture[tmpRowMin2 * frameWidth + currColumn + l] - 11 * referencePicture[tmpRowMin1 * frameWidth + currColumn + l] + 40 * referencePicture[currRow * frameWidth + currColumn + l] + 40 * referencePicture[tmpRowPlus1 * frameWidth + currColumn + l] - 11 * referencePicture[tmpRowPlus2 * frameWidth + currColumn + l] + 4 * referencePicture[tmpRowPlus3 * frameWidth + currColumn + l] - referencePicture[tmpRowPlus4 * frameWidth + currColumn + l] + shift2Offset) >> shift2;

						interpolatedArray[k*nPbW + l + beyondXNegative] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++row;
				}
			}

			// looking for n(i,j), only vertical movement, no need for left and right extension of pb
			else if (fracdY == 3)
			{
				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpRowPlus4;

				int beyondXNegative = 0, beyondXPositive = 0;
				if (column < 0)
				{
					beyondXNegative = -column;
					if (beyondXNegative > nPbW)
						beyondXNegative = nPbW;
				}
				if (column + nPbW > frameWidth)
				{
					beyondXPositive = column + nPbW - frameWidth;
					if (beyondXPositive > nPbW)
						beyondXPositive = nPbW;
				}


				//This is the case when movement in X axis goes left of frame
				int beyondRow = row;
				if (beyondXPositive > 0)
				{
					for (int k = 0; k < nPbH; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, column);
						currRow = ComUtil::clip3(0, frameHeight - 1, beyondRow);
						tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 2);
						tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 1);

						tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 4);
						tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 3);
						tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 2);
						tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 1);

						for (int l = 0; l < beyondXPositive; l++)
						{
							interpolatedArrayTmp = (referencePicture[tmpRowMin2 * frameWidth + frameWidth - 1] - 5 * referencePicture[tmpRowMin1 * frameWidth + frameWidth - 1] + 17 * referencePicture[currRow * frameWidth + frameWidth - 1] + 58 * referencePicture[tmpRowPlus1 * frameWidth + frameWidth - 1] - 10 * referencePicture[tmpRowPlus2 * frameWidth + frameWidth - 1] + 4 * referencePicture[tmpRowPlus3 * frameWidth + frameWidth - 1] - referencePicture[tmpRowPlus4 * frameWidth + frameWidth - 1] + shift2Offset) >> shift2;

							interpolatedArray[k*nPbW + nPbW - l - 1] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondRow;
					}
				}

				if (beyondXNegative > 0)
				{
					beyondRow = row;
					for (int k = 0; k < nPbH; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, column);
						currRow = ComUtil::clip3(0, frameHeight - 1, beyondRow);
						tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 2);
						tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, beyondRow - 1);

						tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 4);
						tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 3);
						tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 2);
						tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, beyondRow + 1);

						for (int l = 0; l < beyondXNegative; l++)
						{
							interpolatedArrayTmp = (referencePicture[tmpRowMin2 * frameWidth + currColumn + 0] - 5 * referencePicture[tmpRowMin1 * frameWidth + currColumn + 0] + 17 * referencePicture[currRow * frameWidth + currColumn + 0] + 58 * referencePicture[tmpRowPlus1 * frameWidth + currColumn + 0] - 10 * referencePicture[tmpRowPlus2 * frameWidth + currColumn + 0] + 4 * referencePicture[tmpRowPlus3 * frameWidth + currColumn + 0] - referencePicture[tmpRowPlus4 * frameWidth + currColumn + 0] + shift2Offset) >> shift2;

							interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondRow;
					}
				}

				for (int k = 0; k < nPbH; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
					tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

					tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, row + 4);
					tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
					tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
					tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);


					for (int l = 0; l < nPbW - beyondXNegative - beyondXPositive; l++)
					{
						interpolatedArrayTmp = (referencePicture[tmpRowMin2 * frameWidth + currColumn + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + currColumn + l] + 17 * referencePicture[currRow * frameWidth + currColumn + l] + 58 * referencePicture[tmpRowPlus1 * frameWidth + currColumn + l] - 10 * referencePicture[tmpRowPlus2 * frameWidth + currColumn + l] + 4 * referencePicture[tmpRowPlus3 * frameWidth + currColumn + l] - referencePicture[tmpRowPlus4 * frameWidth + currColumn + l] + shift2Offset) >> shift2;

						interpolatedArray[k*nPbW + l + beyondXNegative] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++row;
				}

			}
		}
		else if (fracdX == 1)
		{
			// looking for a(i,j), only horizontal movement, no need for top and bottom extension of pb
			if (fracdY == 0)
			{
				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;

				int beyondYPositive = 0, beyondYNegative = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > frameHeight)
				{
					beyondYPositive = row + nPbH - frameHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, frameHeight - 1, row);
						tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 3);
						tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 2);
						tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 1);

						tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 3);
						tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin3] + 4 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin2] - 10 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin1] + 58 * referencePicture[(frameHeight - 1) * frameWidth + currColumn] + 17 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus2] + referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus3] + shift2Offset) >> shift2;

							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;

					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, frameHeight - 1, row);
						tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 3);
						tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 2);
						tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 1);

						tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 3);
						tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-referencePicture[(currRow + 0) * frameWidth + tmpColumnMin3] + 4 * referencePicture[(currRow + 0) * frameWidth + tmpColumnMin2] - 10 * referencePicture[(currRow + 0) * frameWidth + tmpColumnMin1] + 58 * referencePicture[(currRow + 0) * frameWidth + currColumn] + 17 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus2] + referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus3] + shift2Offset) >> shift2;

							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}

				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-referencePicture[(currRow + l) * frameWidth + tmpColumnMin3] + 4 * referencePicture[(currRow + l) * frameWidth + tmpColumnMin2] - 10 * referencePicture[(currRow + l) * frameWidth + tmpColumnMin1] + 58 * referencePicture[(currRow + l) * frameWidth + currColumn] + 17 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus2] + referencePicture[(currRow + l) * frameWidth + tmpColumnPlus3] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for e(i,j), horizontal and vertical movement
			else if (fracdY == 1)
			{
				//first we calculate a[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 10 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 58 * referencePicture[tmpRow * frameWidth + currColumn] + 17 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3] + shift1Offset) >> shift1;

					}
					++column;

				}


				//now we calculate e[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (-interpolatedArrayIntermediate[k] + 4 * interpolatedArrayIntermediate[k + nPbW] - 10 * interpolatedArrayIntermediate[k + 2 * nPbW] + 58 * interpolatedArrayIntermediate[k + 3 * nPbW] + 17 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + interpolatedArrayIntermediate[k + 6 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}

			else if (fracdY == 2)
			{
				//first we calculate a[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 10 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 58 * referencePicture[tmpRow * frameWidth + currColumn] + 17 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3] + shift1Offset) >> shift1;

					}
					++column;

				}

				//now we calculate i[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (-interpolatedArrayIntermediate[k] + 4 * interpolatedArrayIntermediate[k + nPbW] - 11 * interpolatedArrayIntermediate[k + 2 * nPbW] + 40 * interpolatedArrayIntermediate[k + 3 * nPbW] + 40 * interpolatedArrayIntermediate[k + 4 * nPbW] - 11 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;
			}

			else if (fracdY == 3)
			{
				//first we calculate a[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 10 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 58 * referencePicture[tmpRow * frameWidth + currColumn] + 17 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3] + shift1Offset) >> shift1;
					}
					++column;

				}

				//now we calculate p[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 17 * interpolatedArrayIntermediate[k + 3 * nPbW] + 58 * interpolatedArrayIntermediate[k + 4 * nPbW] - 10 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;

			}





		}
		else if (fracdX == 2)
		{
			// looking for b(i,j), only horizontal movement, no need for top and bottom extension of pb
			if (fracdY == 0)
			{
				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > frameHeight)
				{
					beyondYPositive = row + nPbH - frameHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, frameHeight - 1, row);
						tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 3);
						tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 2);
						tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 1);

						tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 4);
						tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 3);
						tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = ((-referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin3] + 4 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin2] - 11 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin1] + 40 * referencePicture[(frameHeight - 1) * frameWidth + currColumn] + 40 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus1] - 11 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus2] + 4 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus3] - referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus4]) + +shift2Offset) >> shift2;

							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, frameHeight - 1, row);
						tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 3);
						tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 2);
						tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 1);

						tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 4);
						tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 3);
						tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = ((-referencePicture[(currRow + 0) * frameWidth + tmpColumnMin3] + 4 * referencePicture[(currRow + 0) * frameWidth + tmpColumnMin2] - 11 * referencePicture[(currRow + 0) * frameWidth + tmpColumnMin1] + 40 * referencePicture[(currRow + 0) * frameWidth + currColumn] + 40 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus1] - 11 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus2] + 4 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus3] - referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus4]) + +shift2Offset) >> shift2;

							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);



					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = ((-referencePicture[(currRow + l) * frameWidth + tmpColumnMin3] + 4 * referencePicture[(currRow + l) * frameWidth + tmpColumnMin2] - 11 * referencePicture[(currRow + l) * frameWidth + tmpColumnMin1] + 40 * referencePicture[(currRow + l) * frameWidth + currColumn] + 40 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus1] - 11 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus2] + 4 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus3] - referencePicture[(currRow + l) * frameWidth + tmpColumnPlus4]) + +shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}

			else if (fracdY == 1)
			{
				//first we calculate b[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 11 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 40 * referencePicture[tmpRow * frameWidth + currColumn] + 40 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 11 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;

					}
					++column;

				}

				//now we calculate f[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (-interpolatedArrayIntermediate[k] + 4 * interpolatedArrayIntermediate[k + nPbW] - 10 * interpolatedArrayIntermediate[k + 2 * nPbW] + 58 * interpolatedArrayIntermediate[k + 3 * nPbW] + 17 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + interpolatedArrayIntermediate[k + 6 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				/*delete[] interpolatedArrayIntermediate;*/
			}

			else if (fracdY == 2)
			{
				//first we calculate b[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 11 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 40 * referencePicture[tmpRow * frameWidth + currColumn] + 40 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 11 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;

					}
					++column;

				}

				//now we calculate j[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (-interpolatedArrayIntermediate[k] + 4 * interpolatedArrayIntermediate[k + nPbW] - 11 * interpolatedArrayIntermediate[k + 2 * nPbW] + 40 * interpolatedArrayIntermediate[k + 3 * nPbW] + 40 * interpolatedArrayIntermediate[k + 4 * nPbW] - 11 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;

			}
			else if (fracdY == 3)
			{
				//first we calculate b[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 11 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 40 * referencePicture[tmpRow * frameWidth + currColumn] + 40 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 11 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;
					}
					++column;

				}

				//now we calculate q[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 17 * interpolatedArrayIntermediate[k + 3 * nPbW] + 58 * interpolatedArrayIntermediate[k + 4 * nPbW] - 10 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 3)
		{
			// looking for c(i,j), only horizontal movement, no need for top and bottom extension of pb
			if (fracdY == 0)
			{
				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > frameHeight)
				{
					beyondYPositive = row + nPbH - frameHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, frameHeight - 1, row);
						tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 2);
						tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 1);

						tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 4);
						tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 3);
						tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnMin1] + 17 * referencePicture[(frameHeight - 1) * frameWidth + currColumn] + 58 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus1] - 10 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus2] + 4 * referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus3] - referencePicture[(frameHeight - 1) * frameWidth + tmpColumnPlus4] + shift2Offset) >> shift2;

							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, frameWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, frameHeight - 1, row);
						tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 2);
						tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn - 1);

						tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 4);
						tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 3);
						tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (referencePicture[(currRow + 0) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(currRow + 0) * frameWidth + tmpColumnMin1] + 17 * referencePicture[(currRow + 0) * frameWidth + currColumn] + 58 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus1] - 10 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus2] + 4 * referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus3] - referencePicture[(currRow + 0) * frameWidth + tmpColumnPlus4] + shift2Offset) >> shift2;

							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}



				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (referencePicture[(currRow + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(currRow + l) * frameWidth + tmpColumnMin1] + 17 * referencePicture[(currRow + l) * frameWidth + currColumn] + 58 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus1] - 10 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus2] + 4 * referencePicture[(currRow + l) * frameWidth + tmpColumnPlus3] - referencePicture[(currRow + l) * frameWidth + tmpColumnPlus4] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}

			}

			else if (fracdY == 1)
			{
				//first we calculate c[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 17 * referencePicture[tmpRow * frameWidth + currColumn] + 58 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 10 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;

					}
					++column;
				}

				//now we calculate g[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (-interpolatedArrayIntermediate[k] + 4 * interpolatedArrayIntermediate[k + nPbW] - 10 * interpolatedArrayIntermediate[k + 2 * nPbW] + 58 * interpolatedArrayIntermediate[k + 3 * nPbW] + 17 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + interpolatedArrayIntermediate[k + 6 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;
			}

			else if (fracdY == 2)
			{
				//first we calculate c[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 17 * referencePicture[tmpRow * frameWidth + currColumn] + 58 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 10 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;
					}
					++column;

				}

				//now we calculate k[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (-interpolatedArrayIntermediate[k] + 4 * interpolatedArrayIntermediate[k + nPbW] - 11 * interpolatedArrayIntermediate[k + 2 * nPbW] + 40 * interpolatedArrayIntermediate[k + 3 * nPbW] + 40 * interpolatedArrayIntermediate[k + 4 * nPbW] - 11 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;

			}

			else if (fracdY == 3)
			{
				//first we calculate c[nPbW][nPbH + 7]
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
				int currRow, currColumn, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
				int tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, frameWidth - 1, column);
					currRow = ComUtil::clip3(0, frameHeight - 1, row);
					tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
					tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

					tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
					tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
					tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


					for (int l = 0; l < nPbH + 7; l++)
					{
						tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 17 * referencePicture[tmpRow * frameWidth + currColumn] + 58 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 10 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 4 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;
					}
					++column;
				}

				//now we calculate r[nPbW][nPbH]
				for (int k = 0; k < nPbH * nPbW; k++)
				{
					interpolatedArrayTmp = (interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 17 * interpolatedArrayIntermediate[k + 3 * nPbW] + 58 * interpolatedArrayIntermediate[k + 4 * nPbW] - 10 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				//delete[] interpolatedArrayIntermediate;
			}
		}
	}
	if (cIdx > 0)
	{
		int frameSize = frameWidth * frameHeight;
		// TO DO: CHROMA SUBSAMPLING!!!!!!
		int chromaWidth = frameWidth / 2;
		int chromaHeight = frameHeight / 2;
		int chromaSize = chromaWidth * chromaHeight;
		int chromaOffset;



		int j;
		int i;
		if (cIdx == 1)
		{
			j = (startingIndex - frameSize) % chromaWidth;
			i = (startingIndex - frameSize) / chromaWidth;
			chromaOffset = frameSize;
		}
		else
		{
			j = (startingIndex - frameSize - chromaSize) % chromaWidth;
			i = (startingIndex - frameSize - chromaSize) / chromaWidth;
			chromaOffset = frameSize + chromaSize;
		}


		int intdX = absdX >> 3;
		int intdY = absdY >> 3;

		int fracdX = absdX & 7;
		int fracdY = absdY & 7;

		int row, column;
		if (isdXNeg)
		{
			column = j - intdX;
			if (fracdX != 0)
				column--;
			//intdX--;
			fracdX = (8 - fracdX) % 8;
		}
		else
		{
			column = j + intdX;
		}

		if (isdYNeg)
		{
			row = i - intdY;
			if (fracdY != 0)
				row--;
			//intdY--;
			fracdY = (8 - fracdY) % 8;
		}
		else
		{
			row = i + intdY;
		}

		//int interpolatedArrayIntermediate[32 * (32 + 7)];
		if (fracdX == 0)
		{

			/*
			if (fracdY == 0)
			{
			// do nothing, no interpolation
			}
			*/

			// looking for ba(i,j), only vertical movement, no need for left and right extension of pb

			int currRow, currColumn, tmpRowMin1, tmpRowPlus1, tmpRowPlus2;

			int beyondXNegative = 0, beyondXPositive = 0;

			if (column < 0)
			{
				beyondXNegative = -column;
				if (beyondXNegative > nPbW)
					beyondXNegative = nPbW;
			}
			if (column + nPbW > chromaWidth)
			{
				beyondXPositive = column + nPbW - chromaWidth;
				if (beyondXPositive > nPbW)
					beyondXPositive = nPbW;
			}


			int beyondRow = row;
			if (beyondXPositive > 0)
			{
				for (int k = 0; k < nPbH; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, beyondRow);

					tmpRowMin1 = ComUtil::clip3(0, chromaHeight - 1, beyondRow - 1);

					tmpRowPlus2 = ComUtil::clip3(0, chromaHeight - 1, beyondRow + 2);
					tmpRowPlus1 = ComUtil::clip3(0, chromaHeight - 1, beyondRow + 1);

					for (int l = 0; l < beyondXPositive; l++)
					{
						if (fracdY == 1)
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 58 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 10 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;
						else if (fracdY == 2)
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 54 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 16 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;
						else if (fracdY == 3)
							interpolatedArrayTmp = (-6 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 46 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 28 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;
						else if (fracdY == 4)
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 36 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 36 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;
						else if (fracdY == 5)
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 28 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 46 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 6 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;
						else if (fracdY == 6)
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 16 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 54 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;
						else if (fracdY == 7)
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + chromaWidth - 1] + 10 * referencePicture[chromaOffset + currRow * chromaWidth + chromaWidth - 1] + 58 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + chromaWidth - 1] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + chromaWidth - 1] + shift2Offset) >> shift2;

						interpolatedArray[k*nPbW + nPbW - l - 1] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++beyondRow;
				}
			}


			if (beyondXNegative > 0)
			{
				beyondRow = row;


				for (int k = 0; k < nPbH; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, beyondRow);

					tmpRowMin1 = ComUtil::clip3(0, chromaHeight - 1, beyondRow - 1);

					tmpRowPlus2 = ComUtil::clip3(0, chromaHeight - 1, beyondRow + 2);
					tmpRowPlus1 = ComUtil::clip3(0, chromaHeight - 1, beyondRow + 1);

					for (int l = 0; l < beyondXNegative; l++)
					{
						if (fracdY == 1)
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 58 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 10 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;
						else if (fracdY == 2)
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 54 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 16 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;
						else if (fracdY == 3)
							interpolatedArrayTmp = (-6 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 46 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 28 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;
						else if (fracdY == 4)
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 36 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 36 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;
						else if (fracdY == 5)
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 28 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 46 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 6 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;
						else if (fracdY == 6)
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 16 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 54 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;
						else if (fracdY == 7)
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + 0] + 10 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + 0] + 58 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + 0] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + 0] + shift2Offset) >> shift2;

						interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}
					++beyondRow;
				}

			}

			for (int k = 0; k < nPbH; k++)
			{

				currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
				currRow = ComUtil::clip3(0, chromaHeight - 1, row);

				tmpRowMin1 = ComUtil::clip3(0, chromaHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, chromaHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, chromaHeight - 1, row + 1);

				for (int l = 0; l < nPbW - beyondXNegative - beyondXPositive; l++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 58 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 10 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;
					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 54 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 16 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;
					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 46 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 28 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;
					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 36 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 36 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;
					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 28 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 46 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 6 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;
					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 16 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 54 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 4 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;
					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + tmpRowMin1 * chromaWidth + currColumn + l] + 10 * referencePicture[chromaOffset + currRow * chromaWidth + currColumn + l] + 58 * referencePicture[chromaOffset + tmpRowPlus1 * chromaWidth + currColumn + l] - 2 * referencePicture[chromaOffset + tmpRowPlus2 * chromaWidth + currColumn + l] + shift2Offset) >> shift2;

					interpolatedArray[k*nPbW + l + beyondXNegative] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}

				++row;
			}
		}
		else if (fracdX == 1)
		{
			// ab
			if (fracdY == 0)
			{

				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;

				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 58 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 10 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 58 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 10 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 58 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 10 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xb(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currColumn, currRow, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-2 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 58 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 10 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 2)
		{
			// ac
			if (fracdY == 0)
			{

				// shift1 Min(4, BitDepthY - 8)
				int currColumn, currRow, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 54 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 16 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 54 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 16 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 54 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 16 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xc(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-4 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 54 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 16 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 3)
		{
			// ad
			if (fracdY == 0)
			{

				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);

						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-6 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 46 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 28 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-6 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 46 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 28 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-6 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 46 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 28 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xd(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-6 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 46 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 28 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 4)
		{
			// ae
			if (fracdY == 0)
			{

				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);

						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 36 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 36 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 36 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 36 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 36 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 36 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xe(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-4 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 36 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 36 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 5)
		{
			// af
			if (fracdY == 0)
			{

				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 28 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 46 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 6 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 28 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 46 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 6 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-4 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 28 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 46 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 6 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xf(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-4 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 28 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 46 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 6 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 6)
		{
			// ag
			if (fracdY == 0)
			{
				// shift1 Min(4, BitDepthY - 8)
				int currColumn, currRow, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 16 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 54 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 16 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 54 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 16 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 54 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xg(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currColumn, currRow, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-2 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 16 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 54 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 4 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
		else if (fracdX == 7)
		{
			// ah
			if (fracdY == 0)
			{
				// shift1 Min(4, BitDepthY - 8)
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

				int beyondYNegative = 0, beyondYPositive = 0;
				if (row < 0)
				{
					beyondYNegative = -row;
					if (beyondYNegative > nPbH)
						beyondYNegative = nPbH;
				}
				if (row + nPbH > chromaHeight)
				{
					beyondYPositive = row + nPbH - chromaHeight;
					if (beyondYPositive > nPbH)
						beyondYPositive = nPbH;
				}

				int beyondColumn = column;
				if (beyondYPositive > 0)
				{
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYPositive; l++)
						{
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnMin1] + 10 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + currColumn] + 58 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (chromaHeight - 1) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[(nPbH - l - 1) *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				if (beyondYNegative > 0)
				{
					beyondColumn = column;
					for (int k = 0; k < nPbW; k++)
					{
						currColumn = ComUtil::clip3(0, chromaWidth - 1, beyondColumn);
						currRow = ComUtil::clip3(0, chromaHeight - 1, row);

						tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn - 1);

						tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 2);
						tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, beyondColumn + 1);


						for (int l = 0; l < beyondYNegative; l++)
						{
							interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnMin1] + 10 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + currColumn] + 58 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (currRow + 0) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;


							interpolatedArray[l *nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
						}

						++beyondColumn;
					}
				}

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH - beyondYPositive - beyondYNegative; l++)
					{
						interpolatedArrayTmp = (-2 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnMin1] + 10 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + currColumn] + 58 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + (currRow + l) * chromaWidth + tmpColumnPlus2] + shift2Offset) >> shift2;

						interpolatedArray[(l + beyondYNegative)*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
					}

					++column;
				}
			}
			// looking for Xh(i,j), horizontal and vertical movement
			else
			{
				//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 3)];
				int currRow, currColumn, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

				for (int k = 0; k < nPbW; k++)
				{
					currColumn = ComUtil::clip3(0, chromaWidth - 1, column);
					currRow = ComUtil::clip3(0, chromaHeight - 1, row);

					tmpColumnMin1 = ComUtil::clip3(0, chromaWidth - 1, column - 1);

					tmpColumnPlus2 = ComUtil::clip3(0, chromaWidth - 1, column + 2);
					tmpColumnPlus1 = ComUtil::clip3(0, chromaWidth - 1, column + 1);

					for (int l = 0; l < nPbH + 3; l++)
					{
						tmpRow = ComUtil::clip3(0, chromaHeight - 1, row - 1 + l);

						interpolatedArrayIntermediate[l*nPbW + k] = (-2 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnMin1] + 10 * referencePicture[chromaOffset + tmpRow * chromaWidth + currColumn] + 58 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus1] - 2 * referencePicture[chromaOffset + tmpRow * chromaWidth + tmpColumnPlus2] + shift1Offset) >> shift1;

					}
					++column;
				}

				for (int k = 0; k < nPbH * nPbW; k++)
				{
					if (fracdY == 1)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 58 * interpolatedArrayIntermediate[k + nPbW] + 10 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 2)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 54 * interpolatedArrayIntermediate[k + nPbW] + 16 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 3)
						interpolatedArrayTmp = (-6 * interpolatedArrayIntermediate[k] + 46 * interpolatedArrayIntermediate[k + nPbW] + 28 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 4)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 36 * interpolatedArrayIntermediate[k + nPbW] + 36 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 5)
						interpolatedArrayTmp = (-4 * interpolatedArrayIntermediate[k] + 28 * interpolatedArrayIntermediate[k + nPbW] + 46 * interpolatedArrayIntermediate[k + 2 * nPbW] - 6 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 6)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 16 * interpolatedArrayIntermediate[k + nPbW] + 54 * interpolatedArrayIntermediate[k + 2 * nPbW] - 4 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;

					else if (fracdY == 7)
						interpolatedArrayTmp = (-2 * interpolatedArrayIntermediate[k] + 10 * interpolatedArrayIntermediate[k + nPbW] + 58 * interpolatedArrayIntermediate[k + 2 * nPbW] - 2 * interpolatedArrayIntermediate[k + 3 * nPbW] + shift3Offset) >> shift3;


					interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArrayTmp);
				}
				//delete[] interpolatedArrayIntermediate;

			}
		}
	}

}

void Interpolation::InterpolateNew(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int cIdx, int dX, int dY)
{

	int shift6 = 6;
	int shift12 = 12;

	int shift6Offset = 32;
	int shift12Offset = 2048;

	unsigned char inputArray[(64 + 7) * (64 + 7)];
	int interpolatedArrayIntermediate[64 * (64 + 7)]{ 0 };
	int resultBlock[64 * 64]{ 0 };
	int columnOffset, rowOffset;
	int row, column;


	int fractionVector[3][8] =
	{
	{ -1, 4,-10, 58, 17, -5, 1, 0 },
	{-1,4,-11,40,40,-11,4,-1},
	{0,1,-5,17,58,-10,4,-1}
	};
	int chromaVector[7][4] =
	{
		{-2,58,10,-2},
		{-4,54,16,-2},
		{-6,46,28,-4},
		{-4,36,36,-4},
		{-4,28,46,-6},
		{-2,16,54,-4},
		{-2,10,58,-2}

	};
	int absdX = abs(dX);
	int absdY = abs(dY);

	if (cIdx == 0)
	{


		int j = startingIndex % frameWidth;
		int i = startingIndex / frameWidth;

		int intdX = absdX >> 2;
		int intdY = absdY >> 2;

		int fracdX = absdX & 3;
		int fracdY = absdY & 3;



		if (absdX != dX)
		{
			column = j - intdX;

			if (fracdX != 0)
				column--;
			//intdX--;
			fracdX = (4 - fracdX) % 4;
		}
		else
		{
			column = j + intdX;
		}

		if (absdY != dY)
		{
			row = i - intdY;
			if (fracdY != 0)
				row--;
			//intdY--;
			fracdY = (4 - fracdY) % 4;
		}
		else
		{
			row = i + intdY;
		}
		columnOffset = column - j - 3;
		rowOffset = row - i - 3;

		

		//dohva�am cijeli blok koji mi treba zajedno s popunjenim lokacijama izvan framea.
		BlockPartition::get1dBlockByStartingIndexWithOffset((char*)referencePicture, inputArray, columnOffset, rowOffset, startingIndex, (64 + 7), frameWidth, frameHeight, nPbW + 7, nPbH + 7, 0);


		if (fracdX == 0)
		{
			for (int i = 0; i < nPbH; i++)
			{
				for (int j = 0; j < nPbW; j++)
				{
					for (int k = 0; k < 8; k++)
					{
						resultBlock[i * nPbW + j] += inputArray[(i + k) * (64 + 7) + j + 3] * fractionVector[fracdY - 1][k];
					}
					interpolatedArray[i * nPbW + j] = ComUtil::clip3(0, 255, (resultBlock[i * nPbW + j] + shift6Offset) >> shift6);

				}
			}
		}
		else if (fracdY == 0)
		{
			for (int i = 0; i < nPbH; i++)
			{
				for (int j = 0; j < nPbW; j++)
				{
					for (int k = 0; k < 8; k++)
					{
						resultBlock[i * nPbW + j] += inputArray[(i + 3) * (64 + 7) + j + k] * fractionVector[fracdX - 1][k];
					}

					interpolatedArray[i * nPbW + j] = ComUtil::clip3(0, 255, (resultBlock[i * nPbW + j] + shift6Offset) >> shift6);

				}
			}
		}
		else
		{
			for (int i = 0; i < nPbH + 7; i++)
			{
				for (int j = 0; j < nPbW; j++) {

					for (int k = 0; k < 8; k++)
					{
						interpolatedArrayIntermediate[i * 64 + j] += inputArray[i * (64 + 7) + j + k] * fractionVector[fracdX - 1][k];
					}
					interpolatedArrayIntermediate[i * 64 + j] = interpolatedArrayIntermediate[i * 64 + j];
				}
			}

			for (int i = 0; i < nPbH; i++)
			{
				for (int j = 0; j < nPbW; j++) {

					for (int k = 0; k < 8; k++)
					{
						resultBlock[i * nPbW + j] += interpolatedArrayIntermediate[(i + k) * 64 + j] * fractionVector[fracdY - 1][k];
					}
					interpolatedArray[i * nPbW + j] = ComUtil::clip3(0, 255, (resultBlock[i * nPbW + j] + shift12Offset) >> shift12);
				}
			}


		}


	}
	else if (cIdx > 0)
	{
		int frameSize = frameWidth * frameHeight;

		int chromaWidth = frameWidth / 2;
		int chromaHeight = frameHeight / 2;
		int chromaSize = chromaWidth * chromaHeight;
		int chromaOffset;

		int j;
		int i;
		if (cIdx == 1)
		{
			j = (startingIndex - frameSize) % chromaWidth;
			i = (startingIndex - frameSize) / chromaWidth;
			chromaOffset = frameSize;
		}
		else
		{
			j = (startingIndex - frameSize - chromaSize) % chromaWidth;
			i = (startingIndex - frameSize - chromaSize) / chromaWidth;
			chromaOffset = frameSize + chromaSize;
		}


		int intdX = absdX >> 3;
		int intdY = absdY >> 3;

		int fracdX = absdX & 7;
		int fracdY = absdY & 7;

		if (dX != absdX)
		{
			column = j - intdX;
			if (fracdX != 0)
				column--;
			//intdX--;
			fracdX = (8 - fracdX) % 8;
		}
		else
		{
			column = j + intdX;
		}

		if (dY != absdY)
		{
			row = i - intdY;
			if (fracdY != 0)
				row--;
			fracdY = (8 - fracdY) % 8;
		}
		else
		{
			row = i + intdY;
		}

		columnOffset = column - j - 1;
		rowOffset = row - i - 1;
		

		BlockPartition::get1dBlockByStartingIndexWithOffset((char*)referencePicture, inputArray, columnOffset, rowOffset, startingIndex, (64 + 7), frameWidth, frameHeight, nPbW + 3, nPbH + 3, 0);

		if (fracdX == 0)
		{
			for (int i = 0; i < nPbH; i++)
			{
				for (int j = 0; j < nPbW; j++)
				{
					for (int k = 0; k < 4; k++)
					{
						resultBlock[i * nPbW + j] += inputArray[(i + k) * (64 + 7) + j + 1] * chromaVector[fracdY - 1][k];
					}
					interpolatedArray[i * nPbW + j] = ComUtil::clip3(0, 255, (resultBlock[i * nPbW + j] + shift6Offset) >> shift6);

				}
			}
		}
		else if (fracdY == 0)
		{
			for (int i = 0; i < nPbH; i++)
			{
				for (int j = 0; j < nPbW; j++)
				{
					for (int k = 0; k < 4; k++)
					{
						resultBlock[i * nPbW + j] += inputArray[(i + 1) * (64 + 7) + j + k] * chromaVector[fracdX - 1][k];
					}

					interpolatedArray[i * nPbW + j] = ComUtil::clip3(0, 255, (resultBlock[i * nPbW + j] + shift6Offset) >> shift6);

				}
			}
		}
		else
		{
			for (int i = 0; i < nPbH + 3; i++)
			{
				for (int j = 0; j < nPbW; j++) {

					for (int k = 0; k < 4; k++)
					{
						interpolatedArrayIntermediate[i * 64 + j] += inputArray[i * (64 + 7) + j + k] * chromaVector[fracdX - 1][k];
					}
					interpolatedArrayIntermediate[i * 64 + j] = interpolatedArrayIntermediate[i * 64 + j];
				}
			}

			for (int i = 0; i < nPbH; i++)
			{
				for (int j = 0; j < nPbW; j++) {

					for (int k = 0; k < 4; k++)
					{
						resultBlock[i * nPbW + j] += interpolatedArrayIntermediate[(i + k) * 64 + j] * chromaVector[fracdY - 1][k];
					}
					interpolatedArray[i * nPbW + j] = ComUtil::clip3(0, 255, (resultBlock[i * nPbW + j] + shift12Offset) >> shift12);
				}
			}
		}
	}
}

void Interpolation::InterpolateSF5(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg)
{

	// shift1 Min(4, BitDepthY - 8)
	int shift1 = 0;
	int shift2 = 5;
	int shift3 = 10;

	int shift1Offset = 0;
	int shift2Offset = 16;
	int shift3Offset = 512;


	int j = startingIndex % frameWidth;
	int i = startingIndex / frameWidth;

	int intdX = absdX >> 2;
	int intdY = absdY >> 2;

	int fracdX = absdX & 3;
	int fracdY = absdY & 3;

	int row, column;
	if (isdXNeg)
	{
		column = j - intdX;

		if (fracdX != 0)
			column--;
		//intdX--;
		fracdX = (4 - fracdX) % 4;
	}
	else
	{
		column = j + intdX;
	}

	if (isdYNeg)
	{
		row = i - intdY;
		if (fracdY != 0)
			row--;
		//intdY--;
		fracdY = (4 - fracdY) % 4;
	}
	else
	{
		row = i + intdY;
	}

	int interpolatedArrayIntermediate[64 * (64 + 7)]{ 0 };

	if (fracdX == 0)
	{

		/*
		if (fracdY == 0)
		{
		// do nothing, no interpolation
		}
		*/

		// looking for d(i,j), only vertical movement, no need for left and right extension of pb
		if (fracdY == 1)
		{

			int tmpRowMin3, tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, row - 3);
				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = ((-referencePicture[tmpRowMin3* frameWidth + column + l] + 2 * referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 29 * referencePicture[row * frameWidth + column + l] + 9 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 3 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + referencePicture[tmpRowPlus3 * frameWidth + column + l]) + shift2Offset) >> shift2;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++row;
			}
		}

		// looking for h(i,j), only vertical movement, no need for left and right extension of pb
		else if (fracdY == 2)
		{
			// shift1 Min(4, BitDepthY - 8)
			int tmpRowMin3, tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpRowPlus4;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin3 = ComUtil::clip3(0, frameHeight - 1, row - 3);
				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);


				tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, row + 4);
				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (-referencePicture[tmpRowMin3* frameWidth + column + l] + 2 * referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 20 * referencePicture[row * frameWidth + column + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + 2 * referencePicture[tmpRowPlus3 * frameWidth + column + l] - referencePicture[tmpRowPlus4 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++row;
			}
		}

		// looking for n(i,j), only vertical movement, no need for left and right extension of pb
		else if (fracdY == 3)
		{
			// shift1 Min(4, BitDepthY - 8)
			int tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpRowPlus4;

			for (int k = 0; k < nPbH; k++)
			{
				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus4 = ComUtil::clip3(0, frameHeight - 1, row + 4);
				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);


				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column + l] - 3 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 9 * referencePicture[row * frameWidth + column + l] + 29 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + 2 * referencePicture[tmpRowPlus3 * frameWidth + column + l] - referencePicture[tmpRowPlus4 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++row;
			}

		}
	}
	else if (fracdX == 1)
	{
		// looking for a(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;

			for (int k = 0; k < nPbW; k++)
			{

				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArray[l*nPbW + k] = (-referencePicture[(row + l) * frameWidth + tmpColumnMin3] + 2 * referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 29 * referencePicture[(row + l) * frameWidth + column] + 9 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 3 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3] + shift2Offset) >> shift2;

					interpolatedArray[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArray[l*nPbW + k]);
				}

				++column;
			}
		}
		// looking for e(i,j), horizontal and vertical movement
		else if (fracdY == 1)
		{
			//first we calculate a[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 29 * referencePicture[tmpRow * frameWidth + column] + 9 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 3 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3] + shift1Offset) >> shift1;

				}
				++column;

			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k] + 2 * interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 29 * interpolatedArrayIntermediate[k + 3 * nPbW] + 9 * interpolatedArrayIntermediate[k + 4 * nPbW] - 3 * interpolatedArrayIntermediate[k + 5 * nPbW] + interpolatedArrayIntermediate[k + 6 * nPbW]) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}
			//delete[] interpolatedArrayIntermediate;

		}

		else if (fracdY == 2)
		{
			//first we calculate a[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 29 * referencePicture[tmpRow * frameWidth + column] + 9 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 3 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3] + shift1Offset) >> shift1;

				}
				++column;

			}

			//now we calculate i[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k] + 2 * interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate[k + 3 * nPbW] + 20 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + 2 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;
		}

		else if (fracdY == 3)
		{
			//first we calculate a[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 29 * referencePicture[tmpRow * frameWidth + column] + 9 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 3 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3] + shift1Offset) >> shift1;
				}
				++column;

			}

			//now we calculate p[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 17 * interpolatedArrayIntermediate[k + 3 * nPbW] + 58 * interpolatedArrayIntermediate[k + 4 * nPbW] - 10 * interpolatedArrayIntermediate[k + 5 * nPbW] + 4 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;

		}





	}
	else if (fracdX == 2)
	{
		// looking for b(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);



				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArray[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin3] + 2 * referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + 2 * referencePicture[(row + l) * frameWidth + tmpColumnPlus3] - referencePicture[(row + l) * frameWidth + tmpColumnPlus4]) + +shift2Offset) >> shift2;

					interpolatedArray[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArray[l*nPbW + k]);
				}

				++column;
			}
		}

		else if (fracdY == 1)
		{
			//first we calculate b[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;

				}
				++column;

			}

			//now we calculate f[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k] + 2 * interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 29 * interpolatedArrayIntermediate[k + 3 * nPbW] + 9 * interpolatedArrayIntermediate[k + 4 * nPbW] - 3 * interpolatedArrayIntermediate[k + 5 * nPbW] + interpolatedArrayIntermediate[k + 6 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;
		}

		else if (fracdY == 2)
		{
			//first we calculate b[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k] + 2 * interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate[k + 3 * nPbW] + 20 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + 2 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;

		}
		else if (fracdY == 3)
		{
			//first we calculate b[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin3, tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin3 = ComUtil::clip3(0, frameWidth - 1, column - 3);
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (-referencePicture[tmpRow * frameWidth + tmpColumnMin3] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;
				}
				++column;

			}

			//now we calculate q[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k + nPbW] - 3 * interpolatedArrayIntermediate[k + 2 * nPbW] + 9 * interpolatedArrayIntermediate[k + 3 * nPbW] + 29 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + 2 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;

		}
	}
	else if (fracdX == 3)
	{
		// looking for c(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;

			for (int k = 0; k < nPbW; k++)
			{

				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArray[l*nPbW + k] = (referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 3 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 9 * referencePicture[(row + l) * frameWidth + column] + 29 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + 2 * referencePicture[(row + l) * frameWidth + tmpColumnPlus3] - referencePicture[(row + l) * frameWidth + tmpColumnPlus4] + shift2Offset) >> shift2;

					interpolatedArray[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArray[l*nPbW + k]);
				}

				++column;
			}

		}

		else if (fracdY == 1)
		{
			//first we calculate c[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 3 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 9 * referencePicture[tmpRow * frameWidth + column] + 29 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;

				}
				++column;
			}

			//now we calculate g[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k] + 2 * interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 29 * interpolatedArrayIntermediate[k + 3 * nPbW] + 9 * interpolatedArrayIntermediate[k + 4 * nPbW] - 3 * interpolatedArrayIntermediate[k + 5 * nPbW] + interpolatedArrayIntermediate[k + 6 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;
		}

		else if (fracdY == 2)
		{
			//first we calculate c[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 3 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 9 * referencePicture[tmpRow * frameWidth + column] + 29 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;
				}
				++column;

			}

			//now we calculate k[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k] + 2 * interpolatedArrayIntermediate[k + nPbW] - 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate[k + 3 * nPbW] + 20 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + 2 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;

		}

		else if (fracdY == 3)
		{
			//first we calculate c[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 7)];
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpColumnPlus4;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus4 = ComUtil::clip3(0, frameWidth - 1, column + 4);
				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 7; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 3 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = (referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 3 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 9 * referencePicture[tmpRow * frameWidth + column] + 29 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + 2 * referencePicture[tmpRow * frameWidth + tmpColumnPlus3] - referencePicture[tmpRow * frameWidth + tmpColumnPlus4] + shift1Offset) >> shift1;
				}
				++column;
			}

			//now we calculate r[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k + nPbW] - 3 * interpolatedArrayIntermediate[k + 2 * nPbW] + 9 * interpolatedArrayIntermediate[k + 3 * nPbW] + 29 * interpolatedArrayIntermediate[k + 4 * nPbW] - 5 * interpolatedArrayIntermediate[k + 5 * nPbW] + 2 * interpolatedArrayIntermediate[k + 6 * nPbW] - interpolatedArrayIntermediate[k + 7 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;
		}
	}

}

void Interpolation::AVCStandardInterpolate(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg)
{

	// shift1 Min(4, BitDepthY - 8)
	int shift1 = 0;
	int shift2 = 5;
	int shift3 = 10;

	int shift1Offset = 0;
	int shift2Offset = 16;
	int shift3Offset = 512;


	int j = startingIndex % frameWidth;
	int i = startingIndex / frameWidth;

	int intdX = absdX >> 2;
	int intdY = absdY >> 2;

	int fracdX = absdX & 3;
	int fracdY = absdY & 3;

	int row, column;
	if (isdXNeg)
	{
		column = j - intdX;

		if (fracdX != 0)
			column--;
		//intdX--;
		fracdX = (4 - fracdX) % 4;
	}
	else
	{
		column = j + intdX;
	}

	if (isdYNeg)
	{
		row = i - intdY;
		if (fracdY != 0)
			row--;
		//intdY--;
		fracdY = (4 - fracdY) % 4;
	}
	else
	{
		row = i + intdY;
	}

	int interpolatedArrayIntermediate[64 * (64 + 7)]{ 0 };
	int interpolatedArrayIntermediate2[64 * (64 + 7)]{ 0 };

	if (fracdX == 0)
	{

		/*
		if (fracdY == 0)
		{
		// do nothing, no interpolation
		}
		*/

		// looking for d(i,j), only vertical movement, no need for left and right extension of pb
		if (fracdY == 1)
		{

			//first we calculate h[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateRow = row;
			// shift1 Min(4, BitDepthY - 8)
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 20 * referencePicture[row * frameWidth + column + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + referencePicture[tmpRowPlus3 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;

			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH; k++)
			{
				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[intermediateRow * frameWidth + column + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}
			//delete[] interpolatedArrayIntermediate;
		}

		// looking for h(i,j), only vertical movement, no need for left and right extension of pb
		else if (fracdY == 2)
		{
			// shift1 Min(4, BitDepthY - 8)
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 20 * referencePicture[row * frameWidth + column + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + referencePicture[tmpRowPlus3 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++row;
			}
		}

		// looking for n(i,j), only vertical movement, no need for left and right extension of pb
		else if (fracdY == 3)
		{
			//first we calculate h[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateRow = row;
			// shift1 Min(4, BitDepthY - 8)
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 20 * referencePicture[row * frameWidth + column + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + referencePicture[tmpRowPlus3 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;

			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH; k++)
			{
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, intermediateRow + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[tmpRowPlus1 * frameWidth + column + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}
			//delete[] interpolatedArrayIntermediate;

		}
	}
	else if (fracdX == 1)
	{
		// looking for a(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateColumn = column;
			int intermediateRow = row;
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}


				++column;
			}

			for (int k = 0; k < nPbH; k++)
			{
				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[intermediateRow * frameWidth + intermediateColumn + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}
			//delete[] interpolatedArrayIntermediate;
		}
		// looking for e(i,j), horizontal and vertical movement
		else if (fracdY == 1)
		{
			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int column2 = column;
			int row2 = row;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}

			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH];
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row2 - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row2 - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row2 + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row2 + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row2 + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate2[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column2 + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column2 + l] + 20 * referencePicture[row2 * frameWidth + column2 + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column2 + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column2 + l] + referencePicture[tmpRowPlus3 * frameWidth + column2 + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate2[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[k*nPbW + l]);
				}

				++row2;
			}

			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}

			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;


		}

		//looking for i(i,j)
		else if (fracdY == 2)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //h
																   // shift1 Min(4, BitDepthY - 8)
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;
			int row2 = row;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 20 * referencePicture[row * frameWidth + column + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + referencePicture[tmpRowPlus3 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row2 - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3];

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				//finalize j 
				interpolatedArray[k] = (interpolatedArrayIntermediate2[k] - 5 * interpolatedArrayIntermediate2[k + nPbW] + 20 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate2[k + 3 * nPbW] - 5 * interpolatedArrayIntermediate2[k + 4 * nPbW] + interpolatedArrayIntermediate2[k + 5 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				//calculate i
				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;
		}

		//p(i,j)
		else if (fracdY == 3)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3;
			int row2 = row;
			int column2 = column;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + column + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + column + l] + 20 * referencePicture[row * frameWidth + column + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + column + l] + referencePicture[tmpRowPlus3 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;
			}

			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH]; //s

			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column2 - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column2 - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column2 + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column2 + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column2 + 1);

				tmpRow = ComUtil::clip3(0, frameHeight - 1, row2 + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate2[l*nPbW + k] = ((referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(tmpRow + l) * frameWidth + column2] + 20 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus2] + referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate2[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[l*nPbW + k]);
				}

				++column2;
			}

			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}


			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}

	}
	else if (fracdX == 2)
	{
		// looking for b(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);



				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArray[l*nPbW + k] = ((referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;

					interpolatedArray[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArray[l*nPbW + k]);
				}


				++column;
			}
		}

		//looking for f(i,j)
		else if (fracdY == 1)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //b
			// shift1 Min(4, BitDepthY - 8)
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int column2 = column;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);



				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column2 - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column2 - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column2 + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column2 + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column2 + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column2] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3];

				}
				++column2;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate2[k] - 5 * interpolatedArrayIntermediate2[k + nPbW] + 20 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate2[k + 3 * nPbW] - 5 * interpolatedArrayIntermediate2[k + 4 * nPbW] + interpolatedArrayIntermediate2[k + 5 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}

		//j(i,j)
		else if (fracdY == 2)
		{
			//first we calculate b[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 5)];
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 2 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3];

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] - 5 * interpolatedArrayIntermediate[k + nPbW] + 20 * interpolatedArrayIntermediate[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate[k + 3 * nPbW] - 5 * interpolatedArrayIntermediate[k + 4 * nPbW] + interpolatedArrayIntermediate[k + 5 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;

		}

		//q(i,j)
		else if (fracdY == 3)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //s

			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpRow;
			int column2 = column;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				tmpRow = ComUtil::clip3(0, frameHeight - 1, row + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(tmpRow + l) * frameWidth + column] + 20 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus2] + referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column2 - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column2 - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column2 + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column2 + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column2 + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column2] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3];

				}
				++column2;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate2[k] - 5 * interpolatedArrayIntermediate2[k + nPbW] + 20 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate2[k + 3 * nPbW] - 5 * interpolatedArrayIntermediate2[k + 4 * nPbW] + interpolatedArrayIntermediate2[k + 5 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}
	}
	else if (fracdX == 3)
	{
		// looking for c(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateColumn = column;
			int intermediateRow = row;
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}


				++column;
			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH; k++)
			{
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, intermediateColumn + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[intermediateRow * frameWidth + tmpColumnPlus1 + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}

			//delete[] interpolatedArrayIntermediate;

		}

		//looking for g(i,j)
		else if (fracdY == 1)
		{
			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int column2 = column;
			int row2 = row;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(row + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(row + l) * frameWidth + column] + 20 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus2] + referencePicture[(row + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}

			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH]; //m

			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpColumn;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row2 - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row2 - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row2 + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row2 + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row2 + 1);

				tmpColumn = ComUtil::clip3(0, frameWidth - 1, column2 + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate2[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + tmpColumn + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + tmpColumn + l] + 20 * referencePicture[row2 * frameWidth + tmpColumn + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + tmpColumn + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + tmpColumn + l] + referencePicture[tmpRowPlus3 * frameWidth + tmpColumn + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate2[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[k*nPbW + l]);
				}

				++row2;
			}


			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}

			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;


		}

		//loking for k(i,j)
		else if (fracdY == 2)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //m
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpColumn;
			int row2 = row;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);

				tmpColumn = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + tmpColumn + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + tmpColumn + l] + 20 * referencePicture[row * frameWidth + tmpColumn + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + tmpColumn + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + tmpColumn + l] + referencePicture[tmpRowPlus3 * frameWidth + tmpColumn + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j
			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row2 - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = referencePicture[tmpRow * frameWidth + tmpColumnMin2] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 20 * referencePicture[tmpRow * frameWidth + column] + 20 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus2] + referencePicture[tmpRow * frameWidth + tmpColumnPlus3];

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				//finalize j 
				interpolatedArray[k] = (interpolatedArrayIntermediate2[k] - 5 * interpolatedArrayIntermediate2[k + nPbW] + 20 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 20 * interpolatedArrayIntermediate2[k + 3 * nPbW] - 5 * interpolatedArrayIntermediate2[k + 4 * nPbW] + interpolatedArrayIntermediate2[k + 5 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				//calculate k
				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}

		else if (fracdY == 3)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //s

			int tmpColumnMin2, tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpColumnPlus3, tmpRow;
			int row2 = row;
			int column2 = column;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin2 = ComUtil::clip3(0, frameWidth - 1, column - 2);
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus3 = ComUtil::clip3(0, frameWidth - 1, column + 3);
				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				tmpRow = ComUtil::clip3(0, frameHeight - 1, row + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin2] - 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin1] + 20 * referencePicture[(tmpRow + l) * frameWidth + column] + 20 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus1] - 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus2] + referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus3]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}

			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH]; //m
			int  tmpRowMin2, tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpRowPlus3, tmpColumn;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin2 = ComUtil::clip3(0, frameHeight - 1, row2 - 2);
				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row2 - 1);

				tmpRowPlus3 = ComUtil::clip3(0, frameHeight - 1, row2 + 3);
				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row2 + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row2 + 1);

				tmpColumn = ComUtil::clip3(0, frameWidth - 1, column2 + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate2[k*nPbW + l] = (referencePicture[tmpRowMin2 * frameWidth + tmpColumn + l] - 5 * referencePicture[tmpRowMin1 * frameWidth + tmpColumn + l] + 20 * referencePicture[row2 * frameWidth + tmpColumn + l] + 20 * referencePicture[tmpRowPlus1 * frameWidth + tmpColumn + l] - 5 * referencePicture[tmpRowPlus2 * frameWidth + tmpColumn + l] + referencePicture[tmpRowPlus3 * frameWidth + tmpColumn + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate2[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[k*nPbW + l]);
				}

				++row2;
			}


			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}

			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;
		}
	}

}

void Interpolation::InterpolateBilinear(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg)
{

	// shift1 Min(4, BitDepthY - 8)
	int shift1 = 4;
	int shift1Offset = 8;


	int j = startingIndex % frameWidth;
	int i = startingIndex / frameWidth;

	int intdX = absdX >> 2;
	int intdY = absdY >> 2;

	int fracdX = absdX & 3;
	int fracdY = absdY & 3;

	int row, column;
	if (isdXNeg)
	{
		column = j - intdX;

		if (fracdX != 0)
			column--;
		//intdX--;
		fracdX = (4 - fracdX) % 4;
	}
	else
	{
		column = j + intdX;
	}

	if (isdYNeg)
	{
		row = i - intdY;
		if (fracdY != 0)
			row--;
		//intdY--;
		fracdY = (4 - fracdY) % 4;
	}
	else
	{
		row = i + intdY;
	}

	// shift1 Min(4, BitDepthY - 8)
	int  tmpRowPlus1, tmpColumnPlus1, index;

	for (int k = 0; k < nPbH; k++)
	{
		for (int l = 0; l < nPbW; l++)
		{
			tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + k + 1);
			tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + l + 1);
			index = k * nPbH + l;

			interpolatedArray[index] =
				(((4 - fracdX)*(4 - fracdY) * referencePicture[(row + k) * frameWidth + (column + l)] +
					fracdX * (4 - fracdY) * referencePicture[(row + k) * frameWidth + (tmpColumnPlus1)] +
					(4 - fracdX)*fracdY * referencePicture[(tmpRowPlus1)* frameWidth + (column + l)] +
					fracdX * fracdY * referencePicture[(tmpRowPlus1)* frameWidth + (tmpColumnPlus1)]) + shift1Offset) >> shift1;

			interpolatedArray[index] = ComUtil::clip3(0, 255, interpolatedArray[index]);
		}
	}



}

void Interpolation::InterpolateFourTap(int nPbH, int nPbW, unsigned char * referencePicture, unsigned char* interpolatedArray, int startingIndex, int frameWidth, int frameHeight, int absdX, int absdY, bool isdXNeg, bool isdYNeg)
{

	// shift1 Min(4, BitDepthY - 8)
	int shift1 = 0;
	int shift2 = 3;
	int shift3 = 6;

	int shift1Offset = 0;
	int shift2Offset = 4;
	int shift3Offset = 32;


	int j = startingIndex % frameWidth;
	int i = startingIndex / frameWidth;

	int intdX = absdX >> 2;
	int intdY = absdY >> 2;

	int fracdX = absdX & 3;
	int fracdY = absdY & 3;

	int row, column;
	if (isdXNeg)
	{
		column = j - intdX;

		if (fracdX != 0)
			column--;
		//intdX--;
		fracdX = (4 - fracdX) % 4;
	}
	else
	{
		column = j + intdX;
	}

	if (isdYNeg)
	{
		row = i - intdY;
		if (fracdY != 0)
			row--;
		//intdY--;
		fracdY = (4 - fracdY) % 4;
	}
	else
	{
		row = i + intdY;
	}

	int interpolatedArrayIntermediate[64 * (64 + 7)]{ 0 };
	int interpolatedArrayIntermediate2[64 * (64 + 7)]{ 0 };

	if (fracdX == 0)
	{

		/*
		if (fracdY == 0)
		{
		// do nothing, no interpolation
		}
		*/

		// looking for d(i,j), only vertical movement, no need for left and right extension of pb
		if (fracdY == 1)
		{

			//first we calculate h[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateRow = row;
			// shift1 Min(4, BitDepthY - 8)
			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);


				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + column + l] + 5 * referencePicture[row * frameWidth + column + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - referencePicture[tmpRowPlus2 * frameWidth + column + l] + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;

			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH; k++)
			{
				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[intermediateRow * frameWidth + column + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}
			//delete[] interpolatedArrayIntermediate;
		}

		// looking for h(i,j), only vertical movement, no need for left and right extension of pb
		else if (fracdY == 2)
		{
			// shift1 Min(4, BitDepthY - 8)
			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + column + l] + 5 * referencePicture[row * frameWidth + column + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - referencePicture[tmpRowPlus2 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++row;
			}
		}

		// looking for n(i,j), only vertical movement, no need for left and right extension of pb
		else if (fracdY == 3)
		{
			//first we calculate h[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateRow = row;
			// shift1 Min(4, BitDepthY - 8)
			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + column + l] + 5 * referencePicture[row * frameWidth + column + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - referencePicture[tmpRowPlus2 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;

			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH; k++)
			{
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, intermediateRow + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[tmpRowPlus1 * frameWidth + column + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}
			//delete[] interpolatedArrayIntermediate;

		}
	}
	else if (fracdX == 1)
	{
		// looking for a(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateColumn = column;
			int intermediateRow = row;
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(row + l) * frameWidth + column] + 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - referencePicture[(row + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}


				++column;
			}

			for (int k = 0; k < nPbH; k++)
			{
				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[intermediateRow * frameWidth + intermediateColumn + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}
			//delete[] interpolatedArrayIntermediate;
		}
		// looking for e(i,j), horizontal and vertical movement
		else if (fracdY == 1)
		{
			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;
			int column2 = column;
			int row2 = row;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(row + l) * frameWidth + column] + 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - referencePicture[(row + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}

			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH];
			int  tmpRowMin1, tmpRowPlus1, tmpRowPlus2;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row2 - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row2 + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row2 + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate2[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + column2 + l] + 5 * referencePicture[row2 * frameWidth + column2 + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + column2 + l] - referencePicture[tmpRowPlus2 * frameWidth + column2 + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate2[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[k*nPbW + l]);
				}

				++row2;
			}

			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}

			/*delete[] interpolatedArrayIntermediate;
			delete[] interpolatedArrayIntermediate2;*/


		}

		//looking for i(i,j)
		else if (fracdY == 2)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //h

			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2;
			int row2 = row;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + column + l] + 5 * referencePicture[row * frameWidth + column + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - referencePicture[tmpRowPlus2 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row2 - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = -referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 5 * referencePicture[tmpRow * frameWidth + column] + 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - referencePicture[tmpRow * frameWidth + tmpColumnPlus2];

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				//finalize j 
				interpolatedArray[k] = (-interpolatedArrayIntermediate2[k + nPbW] + 5 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 5 * interpolatedArrayIntermediate2[k + 3 * nPbW] - interpolatedArrayIntermediate2[k + 4 * nPbW] + +shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				//calculate i
				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			/*delete[] interpolatedArrayIntermediate;
			delete[] interpolatedArrayIntermediate2;*/
		}

		//p(i,j)
		else if (fracdY == 3)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2;
			int row2 = row;
			int column2 = column;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);



				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + column + l] + 5 * referencePicture[row * frameWidth + column + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + column + l] - referencePicture[tmpRowPlus2 * frameWidth + column + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;
			}

			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH]; //s

			int tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column2 - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column2 + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column2 + 1);

				tmpRow = ComUtil::clip3(0, frameHeight - 1, row2 + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate2[l*nPbW + k] = (-referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(tmpRow + l) * frameWidth + column2] + 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus1] - referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus2] + shift2Offset) >> shift2;
					interpolatedArrayIntermediate2[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[l*nPbW + k]);
				}

				++column2;
			}

			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}


			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}

	}
	else if (fracdX == 2)
	{
		// looking for b(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			// shift1 Min(4, BitDepthY - 8)
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);



				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArray[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(row + l) * frameWidth + column] + 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - referencePicture[(row + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;

					interpolatedArray[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArray[l*nPbW + k]);
				}


				++column;
			}
		}

		//looking for f(i,j)
		else if (fracdY == 1)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //b
																   // shift1 Min(4, BitDepthY - 8)
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;
			int column2 = column;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);



				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(row + l) * frameWidth + column] + 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - referencePicture[(row + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column2 - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column2 + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column2 + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = -referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 5 * referencePicture[tmpRow * frameWidth + column2] + 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - referencePicture[tmpRow * frameWidth + tmpColumnPlus2];

				}
				++column2;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate2[k + nPbW] + 5 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 5 * interpolatedArrayIntermediate2[k + 3 * nPbW] - interpolatedArrayIntermediate2[k + 4 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}

		//j(i,j)
		else if (fracdY == 2)
		{
			//first we calculate b[nPbW][nPbH + 7]
			//interpolatedArrayIntermediate = new int[nPbW  * (nPbH + 5)];
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 2 + l);

					interpolatedArrayIntermediate[l*nPbW + k] = -referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 5 * referencePicture[tmpRow * frameWidth + column] + 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - referencePicture[tmpRow * frameWidth + tmpColumnPlus2];

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate[k + nPbW] + 5 * interpolatedArrayIntermediate[k + 2 * nPbW] + 5 * interpolatedArrayIntermediate[k + 3 * nPbW] - interpolatedArrayIntermediate[k + 4 * nPbW] + shift3Offset) >> shift3;

				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);
			}

			//delete[] interpolatedArrayIntermediate;

		}

		//q(i,j)
		else if (fracdY == 3)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //s

			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;
			int column2 = column;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				tmpRow = ComUtil::clip3(0, frameHeight - 1, row + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(tmpRow + l) * frameWidth + column] + 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus1] - referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column2 - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column2 + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column2 + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = -referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 5 * referencePicture[tmpRow * frameWidth + column2] + 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - referencePicture[tmpRow * frameWidth + tmpColumnPlus2];

				}
				++column2;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				interpolatedArray[k] = (-interpolatedArrayIntermediate2[k + nPbW] + 5 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 5 * interpolatedArrayIntermediate2[k + 3 * nPbW] - interpolatedArrayIntermediate2[k + 4 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			/*delete[] interpolatedArrayIntermediate;
			delete[] interpolatedArrayIntermediate2;*/

		}
	}
	else if (fracdX == 3)
	{
		// looking for c(i,j), only horizontal movement, no need for top and bottom extension of pb
		if (fracdY == 0)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int intermediateColumn = column;
			int intermediateRow = row;
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(row + l) * frameWidth + column] + 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - referencePicture[(row + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}


				++column;
			}

			//now we calculate e[nPbW][nPbH]
			for (int k = 0; k < nPbH; k++)
			{
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, intermediateColumn + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArray[k*nPbW + l] = (referencePicture[intermediateRow * frameWidth + tmpColumnPlus1 + l] + interpolatedArrayIntermediate[k*nPbW + l] + 1) >> 1;

					interpolatedArray[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArray[k*nPbW + l]);
				}

				++intermediateRow;
			}

			//delete[] interpolatedArrayIntermediate;

		}

		//looking for g(i,j)
		else if (fracdY == 1)
		{
			// shift1 Min(4, BitDepthY - 8)
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH];
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;
			int column2 = column;
			int row2 = row;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(row + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(row + l) * frameWidth + column] + 5 * referencePicture[(row + l) * frameWidth + tmpColumnPlus1] - referencePicture[(row + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}

			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH]; //m

			int  tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpColumn;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row2 - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row2 + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row2 + 1);

				tmpColumn = ComUtil::clip3(0, frameWidth - 1, column2 + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate2[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + tmpColumn + l] + 5 * referencePicture[row2 * frameWidth + tmpColumn + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + tmpColumn + l] - referencePicture[tmpRowPlus2 * frameWidth + tmpColumn + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate2[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[k*nPbW + l]);
				}

				++row2;
			}


			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}

			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;


		}

		//loking for k(i,j)
		else if (fracdY == 2)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //m
			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpColumn;
			int row2 = row;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row + 1);

				tmpColumn = ComUtil::clip3(0, frameWidth - 1, column + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + tmpColumn + l] + 5 * referencePicture[row * frameWidth + tmpColumn + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + tmpColumn + l] - referencePicture[tmpRowPlus2 * frameWidth + tmpColumn + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[k*nPbW + l]);
				}

				++row;
			}


			//interpolatedArrayIntermediate2 = new int[nPbW  * (nPbH + 5)];//j
			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2;
			int tmpRow;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);


				for (int l = 0; l < nPbH + 5; l++)
				{
					tmpRow = ComUtil::clip3(0, frameHeight - 1, row2 - 2 + l);

					interpolatedArrayIntermediate2[l*nPbW + k] = -referencePicture[tmpRow * frameWidth + tmpColumnMin1] + 5 * referencePicture[tmpRow * frameWidth + column] + 5 * referencePicture[tmpRow * frameWidth + tmpColumnPlus1] - referencePicture[tmpRow * frameWidth + tmpColumnPlus2];

				}
				++column;

			}

			//now we calculate j[nPbW][nPbH]
			for (int k = 0; k < nPbH * nPbW; k++)
			{
				//finalize j 
				interpolatedArray[k] = (-interpolatedArrayIntermediate2[k + nPbW] + 5 * interpolatedArrayIntermediate2[k + 2 * nPbW] + 5 * interpolatedArrayIntermediate2[k + 3 * nPbW] - interpolatedArrayIntermediate2[k + 4 * nPbW] + shift3Offset) >> shift3;
				interpolatedArray[k] = ComUtil::clip3(0, 255, interpolatedArray[k]);

				//calculate k
				interpolatedArray[k] = (interpolatedArray[k] + interpolatedArrayIntermediate[k] + 1) >> 1;

			}



			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;

		}

		else if (fracdY == 3)
		{
			//interpolatedArrayIntermediate = new int[nPbW  * nPbH]; //s

			int  tmpColumnMin1, tmpColumnPlus1, tmpColumnPlus2, tmpRow;
			int row2 = row;
			int column2 = column;

			for (int k = 0; k < nPbW; k++)
			{
				tmpColumnMin1 = ComUtil::clip3(0, frameWidth - 1, column - 1);

				tmpColumnPlus2 = ComUtil::clip3(0, frameWidth - 1, column + 2);
				tmpColumnPlus1 = ComUtil::clip3(0, frameWidth - 1, column + 1);

				tmpRow = ComUtil::clip3(0, frameHeight - 1, row + 1);

				for (int l = 0; l < nPbH; l++)
				{
					interpolatedArrayIntermediate[l*nPbW + k] = ((-referencePicture[(tmpRow + l) * frameWidth + tmpColumnMin1] + 5 * referencePicture[(tmpRow + l) * frameWidth + column] + 5 * referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus1] - referencePicture[(tmpRow + l) * frameWidth + tmpColumnPlus2]) + shift2Offset) >> shift2;
					interpolatedArrayIntermediate[l*nPbW + k] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate[l*nPbW + k]);
				}

				++column;
			}

			//interpolatedArrayIntermediate2 = new int[nPbW  * nPbH]; //m
			int   tmpRowMin1, tmpRowPlus1, tmpRowPlus2, tmpColumn;

			for (int k = 0; k < nPbH; k++)
			{

				tmpRowMin1 = ComUtil::clip3(0, frameHeight - 1, row2 - 1);

				tmpRowPlus2 = ComUtil::clip3(0, frameHeight - 1, row2 + 2);
				tmpRowPlus1 = ComUtil::clip3(0, frameHeight - 1, row2 + 1);

				tmpColumn = ComUtil::clip3(0, frameWidth - 1, column2 + 1);

				for (int l = 0; l < nPbW; l++)
				{
					interpolatedArrayIntermediate2[k*nPbW + l] = (-referencePicture[tmpRowMin1 * frameWidth + tmpColumn + l] + 5 * referencePicture[row2 * frameWidth + tmpColumn + l] + 5 * referencePicture[tmpRowPlus1 * frameWidth + tmpColumn + l] - referencePicture[tmpRowPlus2 * frameWidth + tmpColumn + l] + shift2Offset) >> shift2;

					interpolatedArrayIntermediate2[k*nPbW + l] = ComUtil::clip3(0, 255, interpolatedArrayIntermediate2[k*nPbW + l]);
				}

				++row2;
			}


			for (int k = 0; k < nPbH*nPbW; k++)
			{
				interpolatedArray[k] = (interpolatedArrayIntermediate[k] + interpolatedArrayIntermediate2[k] + 1) >> 1;
			}

			//delete[] interpolatedArrayIntermediate;
			//delete[] interpolatedArrayIntermediate2;
		}
	}

}

Interpolation::~Interpolation()
{
}
